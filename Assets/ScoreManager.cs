﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Random = UnityEngine.Random;
using System.Collections.Generic;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
    //public static int score = 0;        // The player's score.
    //public static int bestScore = 0;

    private int score = 0;        // The player's score.
    private int bestScore = 0;

    //public TextMeshProUGUI text;                      // Reference to the Text component.
    public Text text;
    //public TextMeshProUGUI BestScoretext;
    public Text BestScoretext;

    public Button ClearScoreBtn;

    void Awake()
    {
        // Set up the reference.
        //text = GetComponent<TextMeshProUGUI>();

        // Reset the score.
        score = PlayerPrefs.GetInt("totalScore");
        bestScore = PlayerPrefs.GetInt("bestScore");

        //Set up Clear Score Btn
        ClearScoreBtn.onClick.AddListener(ClearScore);

    }


    void Update()
    {
        // Set the displayed text to be the word "Score" followed by the score value.
        text.text = "Total Points Collected - " + score;
        BestScoretext.text = "My Best Game Score - " + bestScore;
    }

    public void UpdateScore(int currentScore)
    {
        //int newScore = PlayerPrefs.GetInt("totalScore");

        score += currentScore;

        PlayerPrefs.SetInt("totalScore", score);

        //Compare the best score
        if (currentScore > bestScore)
        {
            bestScore = currentScore;
            PlayerPrefs.SetInt("bestScore", bestScore);
        }

    }

    public int getHighScore()
    {
        return PlayerPrefs.GetInt("totalScore");
    }

    private void ClearScore()
    {
        PlayerPrefs.SetInt("totalScore", 0);
        PlayerPrefs.SetInt("bestScore", 0);

        score = 0;
        bestScore = 0;  

        UpdateScore(0);
    }

}
