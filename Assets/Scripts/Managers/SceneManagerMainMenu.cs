﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneManagerMainMenu : MonoBehaviour {

    public Button quickStart, normalStartGameBtn, tutorial, exitButton, playGameBtn, backBtn, scroreBackBtn, scoreBtn , HomeBtn, BackFromQuickGameBtn;
 
    public GameObject MainButtons,quickGameMenu,NormalStartGameMenu,ScorePanel, LoadingPanel;
    public GUIManager gUIManager;

    public Slider diffSlider2 , exposureSlider2, timeSlider2;

    public Text ownerInfo;

    private AudioSource audioSource;

    private bool isQuickStart = false;

    private AsyncOperation operation;
	
	void Start ()
    {
        
        quickStart.onClick.AddListener(QuickStartGame);
        normalStartGameBtn.onClick.AddListener(StartGameButton);
        tutorial.onClick.AddListener(Tutorial);

        playGameBtn.gameObject.SetActive(false);
        playGameBtn.onClick.AddListener(PlayGame);

        backBtn.gameObject.SetActive(false);
        HomeBtn.gameObject.SetActive(false);
        BackFromQuickGameBtn.onClick.AddListener(HomeButtonPressed);
        HomeBtn.onClick.AddListener(HomeButtonPressed);


        audioSource = GetComponent<AudioSource>();

        ownerInfo.gameObject.SetActive(true);

        //Disable all other menues
        MainButtons.gameObject.SetActive(true);
        NormalStartGameMenu.SetActive(false);
        quickGameMenu.SetActive(false);
        LoadingPanel.SetActive(false);

        isQuickStart = true;

        //Exit Game
        exitButton.onClick.AddListener(() => Application.Quit());

        //Show Score
        scoreBtn.onClick.AddListener(openScorePanel);
        scroreBackBtn.onClick.AddListener(openScorePanel);
    }

    public void HomeButtonPressed()
    {
        MainButtons.gameObject.SetActive(true);
        NormalStartGameMenu.SetActive(false);
        quickGameMenu.SetActive(false);
        LoadingPanel.SetActive(false);
        playGameBtn.gameObject.SetActive(false);
        

        HomeBtn.gameObject.SetActive(false);

    }

    private void QuickStartGame()
    {

        //Disable all the buttons
        MainButtons.gameObject.SetActive(false);
        PlayButtonClickSound();
        ownerInfo.gameObject.SetActive(false);

        //Open Quick Start Menu
        quickGameMenu.SetActive(true);
        TurnOnPlayBtn();

        //backBtn.gameObject.SetActive(true);
        HomeBtn.gameObject.SetActive(false);
    }

    private void StartGameButton()
    {
        MainButtons.gameObject.SetActive(false);
        NormalStartGameMenu.SetActive(true);
        ownerInfo.gameObject.SetActive(false);

        PlayButtonClickSound();
        gUIManager.StartInstructions();

        HomeBtn.gameObject.SetActive(true);

    }

    private void Tutorial()
    {
        StartCoroutine( openScene(2));
        //SceneManager.LoadScene(2);
    }

    private void PlayGame()
    {
        if(isQuickStart)
        {
            PlayerPrefs.SetFloat("diffSlider", diffSlider2.value);
            PlayerPrefs.SetFloat("exposureSlider", exposureSlider2.value);
            PlayerPrefs.SetFloat("timeSlider", timeSlider2.value);
        }

        PlayButtonClickSound();

        StartCoroutine( openScene(1));

        
    }

    IEnumerator openScene(int num)
    {
        operation = SceneManager.LoadSceneAsync(num);

        while (!operation.isDone)
        {
            Loading();
            yield return null;
        }

        operation = null;


    }

    private void PlayButtonClickSound()
    {
        audioSource.Play();
    }

    public void TurnOnPlayBtn()
    {
        playGameBtn.gameObject.SetActive(true);
    }

    private void openScorePanel()
    {
        PlayButtonClickSound();

        if (ScorePanel.active)
        {
            ScorePanel.SetActive(false);
            MainButtons.SetActive(true);
            scroreBackBtn.gameObject.SetActive(false);
        }
        else
        {
            ScorePanel.SetActive(true);
            MainButtons.SetActive(false);
            scroreBackBtn.gameObject.SetActive(true);
        }

    }


    private void Loading()
    {
        LoadingPanel.SetActive(true);

        MainButtons.gameObject.SetActive(false);
        NormalStartGameMenu.SetActive(false);
        quickGameMenu.SetActive(false);
        
    }

}
