﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TutorialManager : MonoBehaviour
{
    public GameObject swimmer;
    public GameObject GlowSphere;

    public GameObject cage;

    public Material[] colours;
   
    public Button nextBtn, backBtn;
    public Button RepeatBtn, EndBtn;
    public Button HomeBtn, YesBtn, NoBtn;
    public GameObject quitTpanel;
    public Animator SoundController;

    //Text Messages
    public Text message;

    public AudioSource[] SoundEffects;

    private int tutorialProgress = 0;

    private List<GameObject> swimmers;
    private List<GameObject> cages;
    private int diffCount = 100;
    private Vector3[] spawnValues;

    private Vector3 cagePosition;
    private int cageSpawnCount = 0;
    private bool cageSpawn = false;

    private int SharksCought = 0;

    private void Start()
    {
        GenerateGrid();

        GlowSphere.GetComponent<GlowScript>().MakeGlowRed();
        GlowSphere.SetActive(true);

        swimmers = new List<GameObject>();
        cages = new List<GameObject>();

        SpawnSwimmers();

        //Button Handler
        nextBtn.onClick.AddListener(() => tutorialProgress++);
        backBtn.onClick.AddListener(() => tutorialProgress--);
        HomeBtn.onClick.AddListener(HomeButtonPressed);

        YesBtn.onClick.AddListener(() => SceneManager.LoadScene(0));
        NoBtn.onClick.AddListener(HomeButtonPressed);

        //Set up End and Repeat Btns
        EndBtn.onClick.AddListener(() => SceneManager.LoadScene(0));
        RepeatBtn.onClick.AddListener(() => SceneManager.LoadScene(2));

        EndBtn.gameObject.SetActive(false);
        RepeatBtn.gameObject.SetActive(false);
        quitTpanel.SetActive(false);

    }

    private void Update()
    {
        TutorialProccesses();
    }


    void GenerateGrid()
    {
        var camera = Camera.main;
        float aspectRatio = (float)camera.pixelWidth / (float)camera.pixelHeight;
        float screenHeight = Mathf.Tan(camera.fieldOfView / 2 * Mathf.Deg2Rad) * camera.transform.position.y * 2;
        float screenWidth = screenHeight * aspectRatio;
        float squareHeight = screenHeight * 0.86f / 5.0f;
        float squareWidth = screenHeight * 0.9f / 4.0f;

        camera.gameObject.transform.position = new Vector3(camera.gameObject.transform.position.x, camera.gameObject.transform.position.y, screenWidth / 2);

        cagePosition = new Vector3(squareHeight * 1.8f + camera.gameObject.transform.position.x * 0.5f, 6f, squareHeight * 0.9f); //squareWidth * 1.2f);
        GlowSphere.transform.position = cagePosition;

        int rows = 5;
        int cols = 4;
        int counter = 0;
        spawnValues = new Vector3[rows * cols];
        for (int row = 0; row < rows; row++)
        {
            for (int col = 0; col < cols; col++)
            {

                float xPosition = (squareHeight * row) + (squareHeight * 0.5f) + (camera.gameObject.transform.position.x * 0.5f) + (screenHeight * 0.07f);
                float zPosition = (squareWidth * col) + (squareWidth * 0.5f) + (screenWidth / 2 * 0.6f);
                Vector3 spawnPosition = new Vector3(xPosition, -2.25f, zPosition);
                spawnValues[counter] = spawnPosition;
                counter += 1;
            }
        }
    }

    private void SpawnSwimmers()
    {
        Quaternion spawnRotation = Quaternion.identity;
        for (int i = 0; i < 2; i++)
        {
            Material color = colours[i];
            Vector3 spawnPosition = spawnValues[i];
            GameObject newSwimmer = Instantiate(swimmer, spawnPosition, spawnRotation);
            newSwimmer.GetComponent<ColourGenerator>().SetSwimmerColour(color);
            swimmers.Add(newSwimmer);

            Vector3 decoyPosition = spawnValues[i + 2];
            GameObject decoySwimmer = Instantiate(swimmer, decoyPosition, spawnRotation);
            ColourGenerator cg = decoySwimmer.GetComponent<ColourGenerator>();
            cg.SetSwimmerColour(color);
            cg.RemoveShark(false);
            swimmers.Add(decoySwimmer);
        }

    }

    private void TutorialProccesses()
    {

       
        switch (tutorialProgress)
        {

            case 0:

                //Show Message
                message.text = "This is a tutorial. You will learn the basics of playing the game!";
                backBtn.gameObject.SetActive(false);
                break;

            case 1:

                message.text = "This game tests your memory for locations.";
                backBtn.gameObject.SetActive(true);

                break;

            case 2:

                message.text = "SOME SWIMMERS ARE SURROUNDED BY SHARKS. YOU HAVE TO REMEMBER THE LOCATION OF THOSE SWIMMERS AND PUT THE SHARKS INTO A GAGE!";

                if(!cageSpawn)
                {
                    SpawnCage();
                    cageSpawn = true;
                }
                cages[0].GetComponent<DragScript>().isDraggable = false;

                break;

            case 3:

                message.text = "You can decide the number of swimmers, how long you are able to view the swimmers, and the delay period before the cages become active.";

                break;

            case 4:

                message.text = "When the Circle in the bottom right corner turns green, you can drag the cage to capture the shark!";

                cages[0].GetComponent<DragScript>().isDraggable = true;
                GlowSphere.GetComponent<GlowScript>().MakeGlowGreen();


                break;

            case 5:

                message.text = "WHEN YOU GET CLOSE TO THE LOCATION OF A SWIMMER, YOU MAY HEAR A 'HELP' SOUND. IF YOU SUCCEED IN PUTTING THE SHARK IN A CAGE, YOU WILL HEAR A SPLASH SOUND AND APPLAUSE. ";
                                

                break;

            case 6:

                message.text = "If you go to the wrong swimmer, where there is no shark, YOU WILL HEAR A SPLASH AND A 'OOH' SOUND.";

                break;

            case 7:

                message.text = "NOW TRY CAGING THE SHARK BY DRAGGING THE CAGE FROM THE GREEN CIRCLE ON THE BOTTOM RIGHT OF THE SCREEN. AFTER YOU HAVE DONE THIS,  Try moving the cage to a swimmer where there is no shark.";

                nextBtn.gameObject.SetActive(false);
                backBtn.gameObject.SetActive(false);

                if (SharksCought == 1)
                {
                    tutorialProgress++;
                    nextBtn.gameObject.SetActive(true);
                }

                break;

            case 8:

                message.text = "End of Tutorial.";

                //nextBtn.GetComponentInChildren<Text>().text = "End";
                nextBtn.gameObject.SetActive(false);
                backBtn.gameObject.SetActive(false);
                EndBtn.gameObject.SetActive(true);
                RepeatBtn.gameObject.SetActive(true);


                break;

        }


    }

    private void HomeButtonPressed()
    {
        if(!quitTpanel.active)
        {
            quitTpanel.SetActive(true);
            HomeBtn.gameObject.SetActive(false);
            message.gameObject.SetActive(false);
        }
        else
        {
            quitTpanel.SetActive(false);
            HomeBtn.gameObject.SetActive(true);
            message.gameObject.SetActive(true);
        }
    }

    public void SpawnCage()
    {
        cageSpawnCount += 1;
        print("[SPAWN CAGES] Cage spawn count: " + cageSpawnCount + " Diff count: " + diffCount);
        if (cageSpawnCount > diffCount)
        {
            return;
        }

        Quaternion spawnRotation = Quaternion.identity;
        print("Spawn cage: " + cagePosition);
        GameObject newCage = Instantiate(cage, cagePosition, spawnRotation);
        newCage.GetComponent<CageAppearance>().SetColour(0);
        //  newCage.GetComponent<Animation>().Play();
        newCage.name = "Cage" + cageSpawnCount;
        print("Cage postion before: " + newCage.transform.position);
        newCage.transform.parent = this.transform;
        print("Cage postion after: " + newCage.transform.position);
        //bool dragCheck = cageTimer > totalCageTime;
        cages.Add(newCage);
        newCage.GetComponent<DragScript>().isDraggable = true;

        int remainingCount = diffCount - cageSpawnCount + 1;
        //cageTimerText.text = remainingCount + "\n" + cageColourText + " CAGE" + (remainingCount > 1 ? "S" : "") + "\nremaining";
    }

    public void PlaySplash()
    {
        //SoundController.SetTrigger("PlaySplash");
        SoundEffects[1].Play();
        //SpawnCage();
    }

    public void CageCollided()
    {
        SoundEffects[0].Play();
        SharksCought++;
    }

    public void PlayOOps()
    {
        SoundEffects[2].Play();
    }

}
