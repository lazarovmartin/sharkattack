﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GUIManager : MonoBehaviour
{
    public Animator UIController;
    public TextMeshProUGUI InstructionBtnTxt;
    public GameObject HighScoreImage;
    public Manager manager;
    public Button quickStartBtn;
    public Button normalStartBtn;
    public Button nextBtn;
    public Button backBtn;

    public Button TutorialBtn;

    public GameObject StartPanel;
    public GameObject StartPanelSelection;
    public GameObject ExposurePanel;
    public GameObject ExposureSelection;
    public GameObject TimePanel;
    public GameObject TimePanelSelection;
    public GameObject InstructionsPanel;

    public GameObject GameSetUpMenu;

    public GameObject SettingsPanel;
    public GameObject EndPanel;
    public GameObject ResetPanel;

    List<GameObject> allPanels = new List<GameObject>();

    public Slider diffSlider2;
    public Slider exposureSlider2;
    public Slider timeSlider2;

    public SceneManagerMainMenu sceneManagerMainMenu;

    private bool isQuickStart;

    private int stage = 0;

    private void Start()
    {
        /*quickStartBtn.onClick.AddListener(QuickStart);
        normalStartBtn.onClick.AddListener(NormalStart);
        

        nextBtn.gameObject.SetActive(false);
        backBtn.gameObject.SetActive(false);

        allPanels.Add(StartPanel);
        allPanels.Add(StartPanelSelection);
        allPanels.Add(ExposurePanel);
        allPanels.Add(ExposureSelection);
        allPanels.Add(TimePanel);
        allPanels.Add(TimePanelSelection);
        allPanels.Add(InstructionsPanel);*/
       
        //Set Resolution
        Screen.SetResolution(1024, 600, true);

        nextBtn.onClick.AddListener(Next);
        backBtn.onClick.AddListener(Back);


        //TutorialBtn.onClick.AddListener(() => SceneManager.LoadScene(1));

    }

    public void StartInstructions()
    {
        stage = 5;

    }

    private void Update()
    {

        switch (stage)
        {

            case 0:
                
                //StartPanel.SetActive(true);

                nextBtn.gameObject.SetActive(false);
                backBtn.gameObject.SetActive(false);
                //sceneManagerMainMenu.HomeButtonPressed();

                //TutorialBtn.gameObject.SetActive(true);

                break;

            #region QuickStart Game

            //Quick Start option
            case 1:

                StartPanel.SetActive(false);
                GameSetUpMenu.SetActive(true);

                nextBtn.gameObject.SetActive(false);
                backBtn.gameObject.SetActive(false);

                TutorialBtn.gameObject.SetActive(false);

                break;
            //    StartPanelSelection.SetActive(true);

            //    nextBtn.gameObject.SetActive(true);
            //    backBtn.gameObject.SetActive(true);

            //    break;

            //case 2:

                //    StartPanelSelection.SetActive(false);
                //    ExposureSelection.SetActive(true);

                //    nextBtn.gameObject.SetActive(true);
                //stage = 4;
                //break;

            //case 3:

            //    ExposureSelection.SetActive(false);
            //    TimePanelSelection.SetActive(true);

            //    break;

            case 4:

                //TimePanelSelection.SetActive(false);
                //GameSetUpMenu.SetActive(false);
                //InstructionsPanel.SetActive(true);

                nextBtn.gameObject.SetActive(false);
                backBtn.gameObject.SetActive(false);
                sceneManagerMainMenu.HomeBtn.gameObject.SetActive(false);


                PlayerPrefs.SetFloat("diffSlider", diffSlider2.value);
                PlayerPrefs.SetFloat("exposureSlider", exposureSlider2.value);
                PlayerPrefs.SetFloat("timeSlider", timeSlider2.value);



                sceneManagerMainMenu.TurnOnPlayBtn();

                break;

            #endregion

            case 5:

                //Update the dificulty slider
                //manager.diffSlider.value = diffSlider2.value;


                //StartPanel.SetActive(false);
                ExposurePanel.SetActive(false);
                StartPanelSelection.SetActive(true);

                nextBtn.gameObject.SetActive(true);
                backBtn.gameObject.SetActive(true);

                //TutorialBtn.gameObject.SetActive(false);

                break;

            case 6:

                ExposureSelection.SetActive(false);

                StartPanelSelection.SetActive(false);
                ExposurePanel.SetActive(true);

               

                break;

            case 7:

                TimePanel.SetActive(false);

                ExposurePanel.SetActive(false);
                ExposureSelection.SetActive(true);

                break;

            case 8:

                //Update the explosure Panel
                //manager.exposureSlider.value = exposureSlider2.value;

                TimePanelSelection.SetActive(false);

                ExposureSelection.SetActive(false);
                TimePanel.SetActive(true);

                break;

            case 9:

                //Update Timer slider
                //manager.timeSlider.value = timeSlider2.value;


                TimePanel.SetActive(false);
                TimePanelSelection.SetActive(true);

                stage = 4;

                break;

            case 10:

                TimePanelSelection.SetActive(false);
                //InstructionsPanel.SetActive(true);

                //stage = 4;

                break;

            case 100:
                //Game play

                GameSetUpMenu.SetActive(false);

                break;

        }

    }

    private void Next()
    {
        stage++; 
    }

    private void Back()
    {
        for(int i = 0; i < allPanels.Count;i++)
        {
            allPanels[i].SetActive(false);
        }
        if(stage > 0)
        {
            if (stage == 5)
                stage = 0;
            else
                stage--;
        }
            
    }

    private void QuickStart()
    {
        stage = 1;
    }

    private void NormalStart()
    {
        stage = 5;
    }

    public void StartGame()
    {
        InstructionsPanel.SetActive(false);
        nextBtn.gameObject.SetActive(false);
        backBtn.gameObject.SetActive(false);
        stage = 100;
        manager.startGame();
    }

    public void shwowEndPanel()
    {
        EndPanel.SetActive(true);
        ResetPanel.SetActive(true);
    }

    public void ResetScreen()
    {
        EndPanel.SetActive(false);
        ResetPanel.SetActive(false);
        stage = 0;
    }

    public void ReplayGame()
    {
        EndPanel.SetActive(false);
        ResetPanel.SetActive(false);
    }

    public void showSettings()
    {
        if (SettingsPanel.active)
            SettingsPanel.SetActive(false);
        else
            SettingsPanel.SetActive(true);
    }

    #region Button handling

    public void PressSwimmerInfoNext()
    {
        UIController.SetTrigger("ShowSwimmerSelection");
        isQuickStart = true;
    }

    public void NormalStartGame()
    {
        UIController.SetTrigger("ShowSwimmerSelection");
    }

    public void StartNewGameStep2()
    {
        UIController.SetTrigger("ShowExposureSelection");
    }

    public void StartNewGameStep3()
    {
        UIController.SetTrigger("ShowCageSelection");
        InstructionBtnTxt.text = "Start Game";
    }

    public void PressSwimmerSelectionNext()
    {
        if (isQuickStart)
            StartNewGameStep2();
        else
            UIController.SetTrigger("ShowExposureMenu");
    }

    public void PressExposureInfoNext()
    {
        UIController.SetTrigger("ShowExposureSelection");
    }

    public void PressExposureSelectionNext()
    {
        if (isQuickStart)
            StartNewGameStep3();
        else
            UIController.SetTrigger("ShowCageMenu");
    }

    public void PressCageInfoNext()
    {
        UIController.SetTrigger("ShowCageSelection");
        InstructionBtnTxt.text = "Instructions";
    }

    public void PressCageSelectionNext()
    {
        if (isQuickStart)
        {
            UIController.SetTrigger("PlayGame");
            manager.startGame();
        }
        else
        {
            HighScoreImage.SetActive(false);
            UIController.SetTrigger("ShowInstructions");
        }

    }

    public void PressInstructionsNext()
    {
        UIController.SetTrigger("PlayGame");//--------------------------------------------
        manager.startGame();
        HighScoreImage.SetActive(true);
    }


    #endregion

}
