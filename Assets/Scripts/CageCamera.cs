﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CageCamera : MonoBehaviour {

    public Camera cageCam;

    private Vector2 cagePosition;

	void Start ()
    {
		
	}
	
	
	void Update ()
    {

        cagePosition = new Vector2(this.transform.position.x, this.transform.position.y);

        cageCam.rect = new Rect(cagePosition.normalized, new Vector2(0.1f, 0.15f));
	}
}
