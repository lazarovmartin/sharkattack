﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeachMen : MonoBehaviour {

    Animator animator;

	// Use this for initialization
	void Start ()
    {
        animator = GetComponent<Animator>();
        
	}

    public void Scared()
    {
        animator.SetBool("Laying", false);
    }

}
