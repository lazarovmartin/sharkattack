﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using TMPro;
using System;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{
	private float swimmerShownDuration;
	private int scoreMultiplier;

    public Button surveyBtn;

    public Slider diffSlider;
	public Slider exposureSlider;
	public Slider timeSlider;
    public Button pauseButton;
	public GameObject swimmer;
	public GameObject cage;
	public GameObject Beach;
    public GameObject cageImage;
	public GameObject GlowSphere;
	public Animator UIController;
	public Animator SoundController;
	private Animator GameAnimator;

	public Text hintText;
    //public TextMeshProUGUI scoreText;
    public Text scoreText;
    public TextMeshProUGUI totalScoreText;
   // public GameObject meterTotalScore;
	public TextMeshProUGUI cageTimerText;

	public Material[] colours;

    public GameObject HighScoreImage;
   


	Vector3[] spawnValues;
	Vector3 cagePosition;

	string cageColourText;

	int diffCount = 1;
	int cageType = 1;
	int cageSpawnCount = 0;
	int usedCageCount = 0;
	int correctCages = 0;
    int startingScore = 0;
    int currentScore;

	float totalTime = 0f;
	float cageTimer = 0f;
	float totalCageTime = 0f;

	bool swimmersSpawned = false;
	bool gameStarted = false;
	bool cagesActive = false;

    private bool cageHintActive;

    private bool firstTimeTutorial = false;
    //public Transform Hand;
    private int handStage = 0;
    private bool tutorialDone;
    private float handSpeed = 30.0f;
    private GameObject tutorialTarget;
    private int tutorialRepeat = 0;

    string hintTextType;
    private int textType = 0;
    public GameObject GreenCircle;
    public GameObject GreenCicle2;
    public GameObject RedCircle;

    public GameObject[] beachPeople;
    public TextMeshProUGUI InstructionBtnTxt;
    public bool isDemo;
    public GUIManager gUIManager;
    public ScoreManager scoreManager;

    private bool isQuickStart;

    enum playerState
	{
		swimming,
		diving,
		surfacing,
		stationary
	};

	List<GameObject> swimmers;
	List<GameObject> cages;

	void Start ()
	{
		pauseButton.gameObject.SetActive (false);
		cageTimerText.gameObject.SetActive (false);
		GameAnimator = gameObject.GetComponent <Animator> ();
		GenerateGrid ();
        currentScore = startingScore;
        //set tutorial hand to false
        //Hand.gameObject.SetActive(false);
        tutorialTarget = new GameObject("Target");
        GreenCircle.SetActive(false);
        RedCircle.SetActive(false);
        GreenCicle2.SetActive(false);


        diffSlider.value = PlayerPrefs.GetFloat("diffSlider");
        exposureSlider.value = PlayerPrefs.GetFloat("exposureSlider");
        timeSlider.value = PlayerPrefs.GetFloat("timeSlider");

        startGame();

        surveyBtn.onClick.AddListener(() => Application.OpenURL("https://goo.gl/forms/uv9D66XdH361S3Ex2"));

    }

	void Update ()
	{
		if (ApplicationModel.isPaused) {
			print ("game paused");
			return;
		}

		if (swimmersSpawned) {
			totalTime += Time.deltaTime;
			if (totalTime < swimmerShownDuration) {
				hintText.text = "Remember where the sharks are!";
                cageImage.gameObject.SetActive(false);

                RedCircle.SetActive(false);
                GreenCicle2.SetActive(false);
                GreenCircle.SetActive(false);

			}
		}

		if (totalTime > swimmerShownDuration && !gameStarted) {
			print ("Make swimmers dive!!");
			gameStarted = true;
            //Game Started
			foreach (GameObject s in swimmers) {

                MovementScript mS = s.GetComponentInChildren<MovementScript>();
                if (mS != null)
                {
                    mS.Dive();
                }
			}
        }

		if (gameStarted) {
			cageTimer += Time.deltaTime;

            if (cageTimer > totalCageTime && !cagesActive) {
				print ("unlock cages");
                //hintText.text = "Drag the cage      to capture the shark"; //--------------------------------------------
                //new Code Change the hint text


                switch (cageType)
                {

                    default:

                        GreenCircle.SetActive(true);
                        hintText.text = hintTextType;

                        break;

                    case 2:

                        RedCircle.SetActive(true);
                        GreenCicle2.SetActive(true);
                        hintText.text = "Wait for the cages to unlock! " + hintTextType;
                        break;

                    case 3:

                        RedCircle.SetActive(true);
                        GreenCicle2.SetActive(true);
                        hintText.text = "Wait for the cages to unlock! " + hintTextType;

                        break;

                }


                //if (textType == 1)
                //{
                //    //GreenCircle.SetActive(true);
                //    //hintText.text = hintTextType;
                //}
                //else
                //{
                //    RedCircle.SetActive(true);
                //    GreenCicle2.SetActive(true);
                //    hintText.text = "Wait for the cages to unlock " + hintTextType;
                //}
                                    
                //cageImage.gameObject.SetActive(true);
                cagesActive = true;
				GlowSphere.GetComponent<GlowScript> ().MakeGlowGreen ();
				cageTimerText.gameObject.SetActive (true);
				foreach (GameObject c in cages) {
					print (c.name + " has a drag script? " + c.GetComponent<DragScript> ());
					c.GetComponent<DragScript> ().isDraggable = true;
				}

                turnOnBeachPeople();

			} 
			else if (!cagesActive) {
				
				cageTimerText.gameObject.SetActive (true);
				hintText.text = "Wait for the cages to unlock! " + hintTextType;
                RedCircle.SetActive(true);
                GreenCicle2.SetActive(true);

                cageImage.gameObject.SetActive(false);

                //RedCircle.SetActive(false);
                GreenCircle.SetActive(false);

                int remainingTime = Mathf.FloorToInt (totalCageTime - cageTimer + 1);
				cageTimerText.text = "Cages unlock in:\n" + remainingTime + " secs";
			} 
			else 
			{
				int remainingCount = diffCount - cageSpawnCount + 1;
				cageTimerText.text = remainingCount + "\n" + cageColourText + " CAGE" + (remainingCount > 1 ? "S" : "") + "\nremaining";
			}

            //if(!firstTimeTutorial)
            //    playTutorial();

        }


    }


	#region Button handling

	public void PressSwimmerInfoNext()
	{
		UIController.SetTrigger ("ShowSwimmerSelection");
        isQuickStart = true;
	}

    public void NormalStartGame()
    {
        UIController.SetTrigger("ShowSwimmerSelection");
    }

    public void StartNewGameStep2()
    {  
        UIController.SetTrigger("ShowExposureSelection");
    }

    public void StartNewGameStep3()
    {
        UIController.SetTrigger("ShowCageSelection");
        InstructionBtnTxt.text = "Start Game";
    }

    public void PressSwimmerSelectionNext()
	{
        if (isQuickStart)
            StartNewGameStep2();
        else
            UIController.SetTrigger ("ShowExposureMenu");
	}

	public void PressExposureInfoNext()
	{
        UIController.SetTrigger ("ShowExposureSelection");
	}

	public void PressExposureSelectionNext()
	{
        if (isQuickStart)
            StartNewGameStep3();
        else
            UIController.SetTrigger ("ShowCageMenu");
	}

	public void PressCageInfoNext()
	{
		UIController.SetTrigger ("ShowCageSelection");
        InstructionBtnTxt.text = "Instructions";
    }

	public void PressCageSelectionNext()
	{
        if (isQuickStart)
        {
            UIController.SetTrigger("PlayGame");
            startGame();
        }
        else
        {
            HighScoreImage.SetActive(false);
            UIController.SetTrigger("ShowInstructions");
        }
            
	}

	public void PressInstructionsNext()
	{
		UIController.SetTrigger ("PlayGame");//--------------------------------------------
		startGame ();
        HighScoreImage.SetActive(true);
	}


	public void PressedReplay()
	{
		UIController.SetTrigger ("ReplayGame");
		startGame ();
        gUIManager.ReplayGame();
	}

	public void PressedEndDone ()
	{
		GameAnimator.SetTrigger ("EndDrama");
		UIController.SetTrigger ("CloseEndMenu");
		resetGame ();
        gUIManager.ResetScreen();
        SceneManager.LoadScene(0);
    }

	public void showEndPanel ()
	{
        //UIController.SetTrigger ("EndGame");
        gUIManager.shwowEndPanel();
	}

    public void BackSwimmerMenu()
    {
        UIController.SetTrigger("BackSwimmerMenu");
    }

	public void PressPauseGame ()
	{
		ApplicationModel.isPaused = true;
		pauseButton.gameObject.SetActive (false);
		hintText.gameObject.SetActive (false);
        cageHintActive = cageImage.gameObject.activeSelf;
        cageImage.gameObject.SetActive(false);

        RedCircle.SetActive(false);
        GreenCicle2.SetActive(false);
        GreenCircle.SetActive(false);

		cageTimerText.gameObject.SetActive (false);
		UIController.SetTrigger ("ShowPauseMenu");

        gUIManager.showSettings();

	}

	public void PressResumeGame ()
	{
		UIController.SetTrigger ("ResumeGame");
		pauseButton.gameObject.SetActive (true);
		hintText.gameObject.SetActive (true);
        cageImage.gameObject.SetActive(cageHintActive);
        
        RedCircle.SetActive(cageHintActive);
        GreenCicle2.SetActive(cageHintActive);
        GreenCircle.SetActive(cageHintActive);

        cageTimerText.gameObject.SetActive (true);
		ApplicationModel.isPaused = false;

        gUIManager.showSettings();
	}

	public void PressQuitGame ()
	{
		GameAnimator.SetTrigger ("EndDrama");
        UIController.SetTrigger ("QuitGame");
		ApplicationModel.isPaused = false;
		resetGame ();
        gUIManager.ResetScreen();
        gUIManager.showSettings();
        SceneManager.LoadScene(0);
	}

	#endregion

    public void startGame()
	{
		resetGame ();
		diffCount = (int)diffSlider.value;
		swimmers = new List<GameObject> ();
		cages = new List<GameObject>();
		setExposureTime();
		setCageTime ();
		print ("Diff: " + diffCount + " exposure time: " + swimmerShownDuration + " Cage Time: " + cageType);
		Invoke ("SpawnCage", 0.5f);
		Invoke ("SpawnSwimmers", 2.0f);
		pauseButton.gameObject.SetActive (true);
		GlowSphere.GetComponent<GlowScript> ().MakeGlowRed ();
		GlowSphere.SetActive (true);
		GameAnimator.SetTrigger ("StartDrama");
        
    }

    void resetGame()
    {
        totalTime = 0f;
        cageTimer = 0f;
        totalCageTime = 0f;
        cageSpawnCount = 0;
        usedCageCount = 0;
        correctCages = 0;
        swimmersSpawned = false;
        gameStarted = false;
        cagesActive = false;
        //hintText.text = "";
        cageTimerText.text = "";
        cageTimerText.gameObject.SetActive(true);
        //clear cages 
        if (cages != null)
        {
            foreach (GameObject c in cages)
            {
                Destroy(c);
            }
        }
        //clear swimmers 
        if (swimmers != null)
        {
            foreach (GameObject s in swimmers)
            {
                Destroy(s);
            }
        }      
    }

    //----------------------------------------------
    void setCageTime ()
	{
		cageType = (int)timeSlider.value;

		switch (cageType) {
		case 2:
			totalCageTime = 5f;
			cageColourText = "<color=#999900>GOLD</color>"; //  #C9B200FF
                hintTextType = " When the red circle      turns green      drag a cage to the location of a shark";
                break;
		case 3:
			totalCageTime = 15f;
			cageColourText = "<color=#1E25B0FF>SAPPHIRE</color>";
            hintTextType = " When the red circle      turns green      drag a cage to the location of a shark";
			    break;
		default:
			totalCageTime = 0.0f;
			cageColourText = "<color=#909090FF>SILVER</color>";
            hintTextType = "Drag a cage form the green circle      to the location of shark";
            textType = 0;
			    break;
		}
		print ("cageTime: " + totalCageTime);
	}

	void setExposureTime()
	{
		swimmerShownDuration = (int)exposureSlider.value == 1 ? 11f : 4f;
		scoreMultiplier = (int)exposureSlider.value == 1 ? 1 : 2;
		print ("Level Exposure time: " + swimmerShownDuration + " score mulitplier: " + scoreMultiplier);
	}

	public void SpawnCage ()
	{
		cageSpawnCount += 1;
		print ("[SPAWN CAGES] Cage spawn count: " + cageSpawnCount + " Diff count: " + diffCount);
		if (cageSpawnCount > diffCount) {
			return;
		}
		
		Quaternion spawnRotation = Quaternion.identity;
		print ("Spawn cage: " + cagePosition);
		GameObject newCage = Instantiate (cage, cagePosition, spawnRotation);
		newCage.GetComponent<CageAppearance> ().SetColour (cageType - 1);
      //  newCage.GetComponent<Animation>().Play();
        newCage.name = "Cage" + cageSpawnCount;
		print ("Cage postion before: " + newCage.transform.position);
		newCage.transform.parent = this.transform;
		print ("Cage postion after: " + newCage.transform.position);
		bool dragCheck = cageTimer > totalCageTime;
		cages.Add(newCage);
		newCage.GetComponent<DragScript> ().isDraggable = dragCheck;

		int remainingCount = diffCount - cageSpawnCount + 1;
		cageTimerText.text = remainingCount + "\n" + cageColourText + " CAGE" + (remainingCount > 1 ? "S" : "") + "\nremaining";
	}

	void SpawnSwimmers ()
	{

        if(!isDemo)
        {
            Quaternion spawnRotation = Quaternion.identity;
            shuffle(spawnValues);
            shuffle(colours);
            for (int i = 0; i < diffCount; i++)
            {
                Material color = colours[i];
                Vector3 spawnPosition = spawnValues[i];
                GameObject newSwimmer = Instantiate(swimmer, spawnPosition, spawnRotation);
                newSwimmer.GetComponent<ColourGenerator>().SetSwimmerColour(color);
                swimmers.Add(newSwimmer);

                Vector3 decoyPosition = spawnValues[i + diffCount];
                GameObject decoySwimmer = Instantiate(swimmer, decoyPosition, spawnRotation);
                ColourGenerator cg = decoySwimmer.GetComponent<ColourGenerator>();
                cg.SetSwimmerColour(color);
                cg.RemoveShark(false);
                swimmers.Add(decoySwimmer);
            }
            swimmersSpawned = true;
        }
        else
        {
            Quaternion spawnRotation = Quaternion.identity;
            for (int i = 0; i < diffCount; i++)
            {
                Material color = colours[i];
                Vector3 spawnPosition = spawnValues[i];
                GameObject newSwimmer = Instantiate(swimmer, spawnPosition, spawnRotation);
                newSwimmer.GetComponent<ColourGenerator>().SetSwimmerColour(color);
                swimmers.Add(newSwimmer);

                Vector3 decoyPosition = spawnValues[i + diffCount];
                GameObject decoySwimmer = Instantiate(swimmer, decoyPosition, spawnRotation);
                ColourGenerator cg = decoySwimmer.GetComponent<ColourGenerator>();
                cg.SetSwimmerColour(color);
                cg.RemoveShark(false);
                swimmers.Add(decoySwimmer);
            }
            swimmersSpawned = true;
        }


    }

	void GenerateGrid ()
	{
		var camera = Camera.main;
		float aspectRatio = (float)camera.pixelWidth / (float)camera.pixelHeight;
		float screenHeight = Mathf.Tan (camera.fieldOfView / 2 * Mathf.Deg2Rad) * camera.transform.position.y * 2;
		float screenWidth = screenHeight * aspectRatio;
		float squareHeight = screenHeight * 0.86f / 5.0f;
		float squareWidth = screenHeight * 0.9f / 4.0f;

		camera.gameObject.transform.position = new Vector3 (camera.gameObject.transform.position.x, camera.gameObject.transform.position.y, screenWidth/2);

		cagePosition = new Vector3 (squareHeight * 1.8f + camera.gameObject.transform.position.x * 0.5f, 6f, squareHeight * 0.9f); //squareWidth * 1.2f);
		GlowSphere.transform.position = cagePosition;

		int rows = 5;
		int cols = 4;
		int counter = 0;
		spawnValues = new Vector3[rows * cols];
		for (int row = 0; row < rows; row++) {
			for (int col = 0; col < cols; col++) {

				float xPosition = (squareHeight * row) + (squareHeight * 0.5f) + (camera.gameObject.transform.position.x * 0.5f) + (screenHeight * 0.07f);
				float zPosition = (squareWidth * col) + (squareWidth * 0.5f) + (screenWidth/2 * 0.6f);
				Vector3 spawnPosition = new Vector3 (xPosition, -2.25f, zPosition);
				spawnValues [counter] = spawnPosition;
				counter += 1;
			}
		}
	}

	void shuffle<T> (T[] texts)
	{
		// Knuth shuffle algorithm :: courtesy of Wikipedia :)
		for (int t = 0; t < texts.Length; t++) {
			T tmp = texts [t];
			int r = Random.Range (t, texts.Length);
			texts [t] = texts [r];
			texts [r] = tmp;
		}
	}

	public void PlaySplash ()
	{
		SoundController.SetTrigger ("PlaySplash");
	}

	public void PlayClap ()
	{
		SoundController.SetTrigger ("PlayClap");
	}

	public void PlayGasp ()
	{
		SoundController.SetTrigger ("PlayGasp");
	}

	public void PlayHelp ()
	{
		SoundController.SetTrigger ("PlayHelp");
	}

	public void CageCollided ()
	{
		print ("Cage Collided :)");
		usedCageCount += 1;
		correctCages += 1;
        Invoke("PlayClap", 0.5f);
		checkAllCagesUsed ();
	}

	public void CageMissed()
	{
		print ("Cage Missed :(");
		usedCageCount += 1;
		PlayGasp ();
		checkAllCagesUsed ();
	}

	int gameScore ()
	{
		return correctCages * cageValueForType() * scoreMultiplier;
	}

	int cageValueForType()
	{
		switch (cageType) {
		case 2:
			return 25;
		case 3:
			return 50;
		default:
			return 10;
		}
	}

	void surfaceMissedSwimmers ()
	{
		foreach (GameObject s in swimmers) {
            MovementScript mS = s.GetComponentInChildren<MovementScript>();
            if (mS != null && !mS.sharkCaptured)
            {
                mS.Surface();
            }
			//if (!s.GetComponentInChildren<MovementScript> ().sharkCaptured) {
			//	s.GetComponentInChildren<MovementScript> ().Surface ();
			//}
		}
	}

	void checkAllCagesUsed ()
	{

		if (usedCageCount == diffCount) {
			print ("all cages used!! Correct: " + correctCages + "/" + diffCount);
			surfaceMissedSwimmers ();
			int newScore = gameScore ();

            int highScore = scoreManager.getHighScore();//PlayerPrefs.GetInt ("high_score");
			if (newScore > highScore) {
				//PlayerPrefs.SetInt ("high_score", newScore);
			}
            //string scoreTextStr = "<color=#00FF00FF>Your score in this game is: " + newScore + "</color>" + "<color=#FFFF00FF>\n (Max score in a game is 1000)";
            string scoreTextStr = "Your score in this game is: " + newScore + "\n(Max score in a game is 1000)";


            if (PlayerPrefs.HasKey ("prev_score")) {
                //scoreTextStr += "\n\nPrevious score: " + PlayerPrefs.GetInt ("prev_score");
                scoreTextStr += "\nPrevious score: " + PlayerPrefs.GetInt("prev_score");

            }
            PlayerPrefs.SetInt("prev_score", newScore);
            //if (PlayerPrefs.HasKey ("high_score")) {
            //	scoreTextStr += "\n\nHighest score: " + PlayerPrefs.GetInt ("high_score");
            //}




            scoreText.text = scoreTextStr;
			//PlayerPrefs.SetInt ("prev_score", newScore);
			pauseButton.gameObject.SetActive (false);
			cageTimerText.gameObject.SetActive (false);
			GlowSphere.SetActive (false);
			hintText.text = "";
            cageImage.gameObject.SetActive(false);

            RedCircle.SetActive(false);
            GreenCicle2.SetActive(false);
            GreenCircle.SetActive(false);

			Invoke ("showEndPanel", 1.0f);

           int currentScore = newScore;
            //ScoreManager.score += currentScore;
            scoreManager.UpdateScore(newScore);
           if (currentScore >= 1000)
            {

                totalScoreText.text = "Total Points Collected:" + currentScore + " YOU WIN!";
            }
         
          //  totalScoreText.text = "Your score is:" + currentScore;
        }
	}

    /*void playTutorial()
    {
        if(!tutorialDone)
        {

            switch (handStage)
            {
                //Scan for a target swimmer
                case 0:

                    //Show the hand
                    Hand.gameObject.SetActive(true);
                    Hand.position = new Vector3(81.3f, 17.5f, 14.1f);

                    //Target Swimmer 
                    tutorialTarget.transform.position = new Vector3(swimmers[0].transform.position.x - 6.0f, 17.5f, swimmers[0].transform.position.z + 3.0f);


                    if (tutorialRepeat < 3)
                    {
                        tutorialRepeat++;
                        handStage = 1;
                    }
                    else
                        handStage = 2;

                    break;

                //Move to target
                case 1:

                    //Drag the hand to the cage main position
                    float step = handSpeed * Time.deltaTime;

                    //Move towards the first target
                    Hand.position = Vector3.MoveTowards(Hand.position, tutorialTarget.transform.position, step);
                    if (Hand.position == tutorialTarget.transform.position)
                        handStage = 0;

                    break;

                case 2:
                    //End
                    tutorialDone = true;
                    Hand.gameObject.SetActive(false);

                    firstTimeTutorial = true;

                    break;

            }

            
        }


    }*/


    private void turnOnBeachPeople()
    {
        //StartCoroutine(startAnim());

        for (int i = 0; i < beachPeople.Length; i++)
        {

            beachPeople[i].GetComponent<BeachMen>().Scared();

        }

    }

    private IEnumerator startAnim()
    {
        
        for(int i = 0; i < beachPeople.Length;i++)
        {
            float randSec = Random.Range(0.0f, 3.0f);

            yield return new WaitForSeconds(randSec);

            beachPeople[i].GetComponent<BeachMen>().Scared();

        }

        StopCoroutine(startAnim());
       
    }


}
