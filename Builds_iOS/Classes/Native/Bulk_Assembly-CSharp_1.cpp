﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// TweenAnimation.Models.Curve/QuarticEaseInCurve
struct QuarticEaseInCurve_t3162537896;
// TweenAnimation.Models.Curve
struct Curve_t1446801592;
// TweenAnimation.Models.Curve/QuarticEaseInOutCurve
struct QuarticEaseInOutCurve_t1421613264;
// TweenAnimation.Models.Curve/QuarticEaseOutCurve
struct QuarticEaseOutCurve_t3969926341;
// TweenAnimation.Models.Curve/SineEaseInCurve
struct SineEaseInCurve_t3529713798;
// TweenAnimation.Models.Curve/SineEaseInOutCurve
struct SineEaseInOutCurve_t3024528697;
// TweenAnimation.Models.Curve/SineEaseOutCurve
struct SineEaseOutCurve_t349540267;
// TweenAnimation.Models.TweenAnimation
struct TweenAnimation_t1578776730;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Action
struct Action_t1264377477;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// TweenScript
struct TweenScript_t2995776179;
// System.Collections.Generic.List`1<TweenAnimation.Models.TweenAnimation>
struct List_1_t3050851472;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// System.Collections.Generic.IEnumerable`1<TweenAnimation.Models.TweenAnimation>
struct IEnumerable_1_t558629619;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2059959053;
// System.Func`2<TweenAnimation.Models.TweenAnimation,System.Boolean>
struct Func_2_t1832049457;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t3759279471;
// System.Action`1<TweenAnimation.Models.TweenAnimation>
struct Action_1_t1751244325;
// System.Action`1<System.Object>
struct Action_1_t3252573759;
// System.Char[]
struct CharU5BU5D_t3528271667;
// TweenAnimation.Models.TweenAnimation[]
struct TweenAnimationU5BU5D_t916709311;
// System.Void
struct Void_t1185182177;
// System.String
struct String_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;

extern RuntimeClass* Curve_t1446801592_il2cpp_TypeInfo_var;
extern const uint32_t QuarticEaseInCurve__ctor_m226203159_MetadataUsageId;
extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern const uint32_t QuarticEaseInCurve_Interpolate_m3318808311_MetadataUsageId;
extern RuntimeClass* Quaternion_t2301928331_il2cpp_TypeInfo_var;
extern const uint32_t QuarticEaseInCurve_Interpolate_m1834291297_MetadataUsageId;
extern const uint32_t QuarticEaseInOutCurve__ctor_m2652695060_MetadataUsageId;
extern const uint32_t QuarticEaseInOutCurve_Interpolate_m272778676_MetadataUsageId;
extern const uint32_t QuarticEaseInOutCurve_Interpolate_m37107443_MetadataUsageId;
extern const uint32_t QuarticEaseOutCurve__ctor_m1956297805_MetadataUsageId;
extern const uint32_t QuarticEaseOutCurve_Interpolate_m502579920_MetadataUsageId;
extern const uint32_t QuarticEaseOutCurve_Interpolate_m3600053911_MetadataUsageId;
extern const uint32_t SineEaseInCurve__ctor_m2069362206_MetadataUsageId;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern const uint32_t SineEaseInCurve_SineIn_m1117613811_MetadataUsageId;
extern const uint32_t SineEaseInCurve_Interpolate_m4184295870_MetadataUsageId;
extern const uint32_t SineEaseInCurve_Interpolate_m4158627042_MetadataUsageId;
extern const uint32_t SineEaseInOutCurve__ctor_m536029866_MetadataUsageId;
extern const uint32_t SineEaseInOutCurve_SineInOut_m1921845045_MetadataUsageId;
extern const uint32_t SineEaseInOutCurve_Interpolate_m586090789_MetadataUsageId;
extern const uint32_t SineEaseInOutCurve_Interpolate_m1920943708_MetadataUsageId;
extern const uint32_t SineEaseOutCurve__ctor_m970120592_MetadataUsageId;
extern const uint32_t SineEaseOutCurve_SineOut_m379528209_MetadataUsageId;
extern const uint32_t SineEaseOutCurve_Interpolate_m3953486845_MetadataUsageId;
extern const uint32_t SineEaseOutCurve_Interpolate_m4082407585_MetadataUsageId;
extern const uint32_t TweenAnimation__ctor_m1020063861_MetadataUsageId;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern const uint32_t TweenAnimation_Update_m1480098314_MetadataUsageId;
extern const uint32_t TweenAnimation_Finished_m338973551_MetadataUsageId;
extern RuntimeClass* List_1_t3050851472_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m465774663_RuntimeMethod_var;
extern const uint32_t TweenScript__ctor_m3663774949_MetadataUsageId;
extern RuntimeClass* TweenScript_t2995776179_il2cpp_TypeInfo_var;
extern const uint32_t TweenScript_get_Instance_m3414923116_MetadataUsageId;
extern const uint32_t TweenScript_set_Instance_m2955198855_MetadataUsageId;
extern String_t* _stringLiteral3417423516;
extern const uint32_t TweenScript_Awake_m157295781_MetadataUsageId;
extern RuntimeClass* Func_2_t1832049457_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_AddRange_m4287091714_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m2453644996_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m1204443697_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m673827862_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m353787894_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m2309435788_RuntimeMethod_var;
extern const RuntimeMethod* TweenScript_U3CUpdateU3Em__0_m25612604_RuntimeMethod_var;
extern const RuntimeMethod* Func_2__ctor_m2927733507_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_Where_TisTweenAnimation_t1578776730_m2727003430_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_ToList_TisTweenAnimation_t1578776730_m855476414_RuntimeMethod_var;
extern const uint32_t TweenScript_Update_m1366624147_MetadataUsageId;
extern const RuntimeMethod* List_1_Add_m702000797_RuntimeMethod_var;
extern const uint32_t TweenScript_Enqueue_m1389370299_MetadataUsageId;
extern const RuntimeMethod* List_1_Remove_m4213504351_RuntimeMethod_var;
extern const uint32_t TweenScript_Dequeue_m3155962551_MetadataUsageId;
extern RuntimeClass* Action_1_t1751244325_il2cpp_TypeInfo_var;
extern const RuntimeMethod* TweenScript_U3CDequeueU3Em__1_m1553395588_RuntimeMethod_var;
extern const RuntimeMethod* Action_1__ctor_m1160958587_RuntimeMethod_var;
extern const RuntimeMethod* List_1_ForEach_m2023082796_RuntimeMethod_var;
extern const RuntimeMethod* TweenScript_U3CDequeueU3Em__2_m512505057_RuntimeMethod_var;
extern const uint32_t TweenScript_Dequeue_m3048948346_MetadataUsageId;
extern const uint32_t TweenScript_U3CDequeueU3Em__1_m1553395588_MetadataUsageId;
extern const uint32_t TweenScript_U3CDequeueU3Em__2_m512505057_MetadataUsageId;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef TWEENANIMATION_T1578776730_H
#define TWEENANIMATION_T1578776730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.TweenAnimation
struct  TweenAnimation_t1578776730  : public RuntimeObject
{
public:
	// System.Int32 TweenAnimation.Models.TweenAnimation::frameCount
	int32_t ___frameCount_0;
	// TweenAnimation.Models.Curve TweenAnimation.Models.TweenAnimation::curve
	Curve_t1446801592 * ___curve_1;
	// System.Single TweenAnimation.Models.TweenAnimation::duration
	float ___duration_2;
	// System.Single TweenAnimation.Models.TweenAnimation::delay
	float ___delay_3;
	// System.Single TweenAnimation.Models.TweenAnimation::startTime
	float ___startTime_4;
	// UnityEngine.Transform TweenAnimation.Models.TweenAnimation::transform
	Transform_t3600365921 * ___transform_5;
	// System.Boolean TweenAnimation.Models.TweenAnimation::isFinished
	bool ___isFinished_6;
	// System.Action TweenAnimation.Models.TweenAnimation::finishCallback
	Action_t1264377477 * ___finishCallback_7;
	// System.Action TweenAnimation.Models.TweenAnimation::startCallback
	Action_t1264377477 * ___startCallback_8;

public:
	inline static int32_t get_offset_of_frameCount_0() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___frameCount_0)); }
	inline int32_t get_frameCount_0() const { return ___frameCount_0; }
	inline int32_t* get_address_of_frameCount_0() { return &___frameCount_0; }
	inline void set_frameCount_0(int32_t value)
	{
		___frameCount_0 = value;
	}

	inline static int32_t get_offset_of_curve_1() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___curve_1)); }
	inline Curve_t1446801592 * get_curve_1() const { return ___curve_1; }
	inline Curve_t1446801592 ** get_address_of_curve_1() { return &___curve_1; }
	inline void set_curve_1(Curve_t1446801592 * value)
	{
		___curve_1 = value;
		Il2CppCodeGenWriteBarrier((&___curve_1), value);
	}

	inline static int32_t get_offset_of_duration_2() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___duration_2)); }
	inline float get_duration_2() const { return ___duration_2; }
	inline float* get_address_of_duration_2() { return &___duration_2; }
	inline void set_duration_2(float value)
	{
		___duration_2 = value;
	}

	inline static int32_t get_offset_of_delay_3() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___delay_3)); }
	inline float get_delay_3() const { return ___delay_3; }
	inline float* get_address_of_delay_3() { return &___delay_3; }
	inline void set_delay_3(float value)
	{
		___delay_3 = value;
	}

	inline static int32_t get_offset_of_startTime_4() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___startTime_4)); }
	inline float get_startTime_4() const { return ___startTime_4; }
	inline float* get_address_of_startTime_4() { return &___startTime_4; }
	inline void set_startTime_4(float value)
	{
		___startTime_4 = value;
	}

	inline static int32_t get_offset_of_transform_5() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___transform_5)); }
	inline Transform_t3600365921 * get_transform_5() const { return ___transform_5; }
	inline Transform_t3600365921 ** get_address_of_transform_5() { return &___transform_5; }
	inline void set_transform_5(Transform_t3600365921 * value)
	{
		___transform_5 = value;
		Il2CppCodeGenWriteBarrier((&___transform_5), value);
	}

	inline static int32_t get_offset_of_isFinished_6() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___isFinished_6)); }
	inline bool get_isFinished_6() const { return ___isFinished_6; }
	inline bool* get_address_of_isFinished_6() { return &___isFinished_6; }
	inline void set_isFinished_6(bool value)
	{
		___isFinished_6 = value;
	}

	inline static int32_t get_offset_of_finishCallback_7() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___finishCallback_7)); }
	inline Action_t1264377477 * get_finishCallback_7() const { return ___finishCallback_7; }
	inline Action_t1264377477 ** get_address_of_finishCallback_7() { return &___finishCallback_7; }
	inline void set_finishCallback_7(Action_t1264377477 * value)
	{
		___finishCallback_7 = value;
		Il2CppCodeGenWriteBarrier((&___finishCallback_7), value);
	}

	inline static int32_t get_offset_of_startCallback_8() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___startCallback_8)); }
	inline Action_t1264377477 * get_startCallback_8() const { return ___startCallback_8; }
	inline Action_t1264377477 ** get_address_of_startCallback_8() { return &___startCallback_8; }
	inline void set_startCallback_8(Action_t1264377477 * value)
	{
		___startCallback_8 = value;
		Il2CppCodeGenWriteBarrier((&___startCallback_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENANIMATION_T1578776730_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef LIST_1_T3050851472_H
#define LIST_1_T3050851472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<TweenAnimation.Models.TweenAnimation>
struct  List_1_t3050851472  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TweenAnimationU5BU5D_t916709311* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3050851472, ____items_1)); }
	inline TweenAnimationU5BU5D_t916709311* get__items_1() const { return ____items_1; }
	inline TweenAnimationU5BU5D_t916709311** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TweenAnimationU5BU5D_t916709311* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3050851472, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3050851472, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3050851472_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	TweenAnimationU5BU5D_t916709311* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3050851472_StaticFields, ___EmptyArray_4)); }
	inline TweenAnimationU5BU5D_t916709311* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline TweenAnimationU5BU5D_t916709311** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(TweenAnimationU5BU5D_t916709311* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3050851472_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef CURVE_T1446801592_H
#define CURVE_T1446801592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve
struct  Curve_t1446801592  : public RuntimeObject
{
public:

public:
};

struct Curve_t1446801592_StaticFields
{
public:
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::Linear
	Curve_t1446801592 * ___Linear_0;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::CubicEaseIn
	Curve_t1446801592 * ___CubicEaseIn_1;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::CubicEaseOut
	Curve_t1446801592 * ___CubicEaseOut_2;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::CubicEaseInOut
	Curve_t1446801592 * ___CubicEaseInOut_3;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::QuarticEaseIn
	Curve_t1446801592 * ___QuarticEaseIn_4;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::QuarticEaseOut
	Curve_t1446801592 * ___QuarticEaseOut_5;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::QuarticEaseInOut
	Curve_t1446801592 * ___QuarticEaseInOut_6;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::QuadricEaseIn
	Curve_t1446801592 * ___QuadricEaseIn_7;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::QuadricEaseOut
	Curve_t1446801592 * ___QuadricEaseOut_8;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::QuadricEaseInOut
	Curve_t1446801592 * ___QuadricEaseInOut_9;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::BounceEaseOut
	Curve_t1446801592 * ___BounceEaseOut_10;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::BounceEaseIn
	Curve_t1446801592 * ___BounceEaseIn_11;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::BounceEaseInOut
	Curve_t1446801592 * ___BounceEaseInOut_12;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::BackEaseIn
	Curve_t1446801592 * ___BackEaseIn_13;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::BackEaseOut
	Curve_t1446801592 * ___BackEaseOut_14;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::BackEaseInOut
	Curve_t1446801592 * ___BackEaseInOut_15;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::SineEaseIn
	Curve_t1446801592 * ___SineEaseIn_16;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::SineEaseOut
	Curve_t1446801592 * ___SineEaseOut_17;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::SineEaseInOut
	Curve_t1446801592 * ___SineEaseInOut_18;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::ElasticEaseIn
	Curve_t1446801592 * ___ElasticEaseIn_19;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::ElasticEaseOut
	Curve_t1446801592 * ___ElasticEaseOut_20;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::ElasticEaseInOut
	Curve_t1446801592 * ___ElasticEaseInOut_21;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::CircularEaseIn
	Curve_t1446801592 * ___CircularEaseIn_22;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::CircularEaseOut
	Curve_t1446801592 * ___CircularEaseOut_23;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::CircularEaseInOut
	Curve_t1446801592 * ___CircularEaseInOut_24;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::ExponentialEaseIn
	Curve_t1446801592 * ___ExponentialEaseIn_25;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::ExponentialEaseOut
	Curve_t1446801592 * ___ExponentialEaseOut_26;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::ExponentialEaseInOut
	Curve_t1446801592 * ___ExponentialEaseInOut_27;

public:
	inline static int32_t get_offset_of_Linear_0() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___Linear_0)); }
	inline Curve_t1446801592 * get_Linear_0() const { return ___Linear_0; }
	inline Curve_t1446801592 ** get_address_of_Linear_0() { return &___Linear_0; }
	inline void set_Linear_0(Curve_t1446801592 * value)
	{
		___Linear_0 = value;
		Il2CppCodeGenWriteBarrier((&___Linear_0), value);
	}

	inline static int32_t get_offset_of_CubicEaseIn_1() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___CubicEaseIn_1)); }
	inline Curve_t1446801592 * get_CubicEaseIn_1() const { return ___CubicEaseIn_1; }
	inline Curve_t1446801592 ** get_address_of_CubicEaseIn_1() { return &___CubicEaseIn_1; }
	inline void set_CubicEaseIn_1(Curve_t1446801592 * value)
	{
		___CubicEaseIn_1 = value;
		Il2CppCodeGenWriteBarrier((&___CubicEaseIn_1), value);
	}

	inline static int32_t get_offset_of_CubicEaseOut_2() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___CubicEaseOut_2)); }
	inline Curve_t1446801592 * get_CubicEaseOut_2() const { return ___CubicEaseOut_2; }
	inline Curve_t1446801592 ** get_address_of_CubicEaseOut_2() { return &___CubicEaseOut_2; }
	inline void set_CubicEaseOut_2(Curve_t1446801592 * value)
	{
		___CubicEaseOut_2 = value;
		Il2CppCodeGenWriteBarrier((&___CubicEaseOut_2), value);
	}

	inline static int32_t get_offset_of_CubicEaseInOut_3() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___CubicEaseInOut_3)); }
	inline Curve_t1446801592 * get_CubicEaseInOut_3() const { return ___CubicEaseInOut_3; }
	inline Curve_t1446801592 ** get_address_of_CubicEaseInOut_3() { return &___CubicEaseInOut_3; }
	inline void set_CubicEaseInOut_3(Curve_t1446801592 * value)
	{
		___CubicEaseInOut_3 = value;
		Il2CppCodeGenWriteBarrier((&___CubicEaseInOut_3), value);
	}

	inline static int32_t get_offset_of_QuarticEaseIn_4() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___QuarticEaseIn_4)); }
	inline Curve_t1446801592 * get_QuarticEaseIn_4() const { return ___QuarticEaseIn_4; }
	inline Curve_t1446801592 ** get_address_of_QuarticEaseIn_4() { return &___QuarticEaseIn_4; }
	inline void set_QuarticEaseIn_4(Curve_t1446801592 * value)
	{
		___QuarticEaseIn_4 = value;
		Il2CppCodeGenWriteBarrier((&___QuarticEaseIn_4), value);
	}

	inline static int32_t get_offset_of_QuarticEaseOut_5() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___QuarticEaseOut_5)); }
	inline Curve_t1446801592 * get_QuarticEaseOut_5() const { return ___QuarticEaseOut_5; }
	inline Curve_t1446801592 ** get_address_of_QuarticEaseOut_5() { return &___QuarticEaseOut_5; }
	inline void set_QuarticEaseOut_5(Curve_t1446801592 * value)
	{
		___QuarticEaseOut_5 = value;
		Il2CppCodeGenWriteBarrier((&___QuarticEaseOut_5), value);
	}

	inline static int32_t get_offset_of_QuarticEaseInOut_6() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___QuarticEaseInOut_6)); }
	inline Curve_t1446801592 * get_QuarticEaseInOut_6() const { return ___QuarticEaseInOut_6; }
	inline Curve_t1446801592 ** get_address_of_QuarticEaseInOut_6() { return &___QuarticEaseInOut_6; }
	inline void set_QuarticEaseInOut_6(Curve_t1446801592 * value)
	{
		___QuarticEaseInOut_6 = value;
		Il2CppCodeGenWriteBarrier((&___QuarticEaseInOut_6), value);
	}

	inline static int32_t get_offset_of_QuadricEaseIn_7() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___QuadricEaseIn_7)); }
	inline Curve_t1446801592 * get_QuadricEaseIn_7() const { return ___QuadricEaseIn_7; }
	inline Curve_t1446801592 ** get_address_of_QuadricEaseIn_7() { return &___QuadricEaseIn_7; }
	inline void set_QuadricEaseIn_7(Curve_t1446801592 * value)
	{
		___QuadricEaseIn_7 = value;
		Il2CppCodeGenWriteBarrier((&___QuadricEaseIn_7), value);
	}

	inline static int32_t get_offset_of_QuadricEaseOut_8() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___QuadricEaseOut_8)); }
	inline Curve_t1446801592 * get_QuadricEaseOut_8() const { return ___QuadricEaseOut_8; }
	inline Curve_t1446801592 ** get_address_of_QuadricEaseOut_8() { return &___QuadricEaseOut_8; }
	inline void set_QuadricEaseOut_8(Curve_t1446801592 * value)
	{
		___QuadricEaseOut_8 = value;
		Il2CppCodeGenWriteBarrier((&___QuadricEaseOut_8), value);
	}

	inline static int32_t get_offset_of_QuadricEaseInOut_9() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___QuadricEaseInOut_9)); }
	inline Curve_t1446801592 * get_QuadricEaseInOut_9() const { return ___QuadricEaseInOut_9; }
	inline Curve_t1446801592 ** get_address_of_QuadricEaseInOut_9() { return &___QuadricEaseInOut_9; }
	inline void set_QuadricEaseInOut_9(Curve_t1446801592 * value)
	{
		___QuadricEaseInOut_9 = value;
		Il2CppCodeGenWriteBarrier((&___QuadricEaseInOut_9), value);
	}

	inline static int32_t get_offset_of_BounceEaseOut_10() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___BounceEaseOut_10)); }
	inline Curve_t1446801592 * get_BounceEaseOut_10() const { return ___BounceEaseOut_10; }
	inline Curve_t1446801592 ** get_address_of_BounceEaseOut_10() { return &___BounceEaseOut_10; }
	inline void set_BounceEaseOut_10(Curve_t1446801592 * value)
	{
		___BounceEaseOut_10 = value;
		Il2CppCodeGenWriteBarrier((&___BounceEaseOut_10), value);
	}

	inline static int32_t get_offset_of_BounceEaseIn_11() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___BounceEaseIn_11)); }
	inline Curve_t1446801592 * get_BounceEaseIn_11() const { return ___BounceEaseIn_11; }
	inline Curve_t1446801592 ** get_address_of_BounceEaseIn_11() { return &___BounceEaseIn_11; }
	inline void set_BounceEaseIn_11(Curve_t1446801592 * value)
	{
		___BounceEaseIn_11 = value;
		Il2CppCodeGenWriteBarrier((&___BounceEaseIn_11), value);
	}

	inline static int32_t get_offset_of_BounceEaseInOut_12() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___BounceEaseInOut_12)); }
	inline Curve_t1446801592 * get_BounceEaseInOut_12() const { return ___BounceEaseInOut_12; }
	inline Curve_t1446801592 ** get_address_of_BounceEaseInOut_12() { return &___BounceEaseInOut_12; }
	inline void set_BounceEaseInOut_12(Curve_t1446801592 * value)
	{
		___BounceEaseInOut_12 = value;
		Il2CppCodeGenWriteBarrier((&___BounceEaseInOut_12), value);
	}

	inline static int32_t get_offset_of_BackEaseIn_13() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___BackEaseIn_13)); }
	inline Curve_t1446801592 * get_BackEaseIn_13() const { return ___BackEaseIn_13; }
	inline Curve_t1446801592 ** get_address_of_BackEaseIn_13() { return &___BackEaseIn_13; }
	inline void set_BackEaseIn_13(Curve_t1446801592 * value)
	{
		___BackEaseIn_13 = value;
		Il2CppCodeGenWriteBarrier((&___BackEaseIn_13), value);
	}

	inline static int32_t get_offset_of_BackEaseOut_14() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___BackEaseOut_14)); }
	inline Curve_t1446801592 * get_BackEaseOut_14() const { return ___BackEaseOut_14; }
	inline Curve_t1446801592 ** get_address_of_BackEaseOut_14() { return &___BackEaseOut_14; }
	inline void set_BackEaseOut_14(Curve_t1446801592 * value)
	{
		___BackEaseOut_14 = value;
		Il2CppCodeGenWriteBarrier((&___BackEaseOut_14), value);
	}

	inline static int32_t get_offset_of_BackEaseInOut_15() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___BackEaseInOut_15)); }
	inline Curve_t1446801592 * get_BackEaseInOut_15() const { return ___BackEaseInOut_15; }
	inline Curve_t1446801592 ** get_address_of_BackEaseInOut_15() { return &___BackEaseInOut_15; }
	inline void set_BackEaseInOut_15(Curve_t1446801592 * value)
	{
		___BackEaseInOut_15 = value;
		Il2CppCodeGenWriteBarrier((&___BackEaseInOut_15), value);
	}

	inline static int32_t get_offset_of_SineEaseIn_16() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___SineEaseIn_16)); }
	inline Curve_t1446801592 * get_SineEaseIn_16() const { return ___SineEaseIn_16; }
	inline Curve_t1446801592 ** get_address_of_SineEaseIn_16() { return &___SineEaseIn_16; }
	inline void set_SineEaseIn_16(Curve_t1446801592 * value)
	{
		___SineEaseIn_16 = value;
		Il2CppCodeGenWriteBarrier((&___SineEaseIn_16), value);
	}

	inline static int32_t get_offset_of_SineEaseOut_17() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___SineEaseOut_17)); }
	inline Curve_t1446801592 * get_SineEaseOut_17() const { return ___SineEaseOut_17; }
	inline Curve_t1446801592 ** get_address_of_SineEaseOut_17() { return &___SineEaseOut_17; }
	inline void set_SineEaseOut_17(Curve_t1446801592 * value)
	{
		___SineEaseOut_17 = value;
		Il2CppCodeGenWriteBarrier((&___SineEaseOut_17), value);
	}

	inline static int32_t get_offset_of_SineEaseInOut_18() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___SineEaseInOut_18)); }
	inline Curve_t1446801592 * get_SineEaseInOut_18() const { return ___SineEaseInOut_18; }
	inline Curve_t1446801592 ** get_address_of_SineEaseInOut_18() { return &___SineEaseInOut_18; }
	inline void set_SineEaseInOut_18(Curve_t1446801592 * value)
	{
		___SineEaseInOut_18 = value;
		Il2CppCodeGenWriteBarrier((&___SineEaseInOut_18), value);
	}

	inline static int32_t get_offset_of_ElasticEaseIn_19() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___ElasticEaseIn_19)); }
	inline Curve_t1446801592 * get_ElasticEaseIn_19() const { return ___ElasticEaseIn_19; }
	inline Curve_t1446801592 ** get_address_of_ElasticEaseIn_19() { return &___ElasticEaseIn_19; }
	inline void set_ElasticEaseIn_19(Curve_t1446801592 * value)
	{
		___ElasticEaseIn_19 = value;
		Il2CppCodeGenWriteBarrier((&___ElasticEaseIn_19), value);
	}

	inline static int32_t get_offset_of_ElasticEaseOut_20() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___ElasticEaseOut_20)); }
	inline Curve_t1446801592 * get_ElasticEaseOut_20() const { return ___ElasticEaseOut_20; }
	inline Curve_t1446801592 ** get_address_of_ElasticEaseOut_20() { return &___ElasticEaseOut_20; }
	inline void set_ElasticEaseOut_20(Curve_t1446801592 * value)
	{
		___ElasticEaseOut_20 = value;
		Il2CppCodeGenWriteBarrier((&___ElasticEaseOut_20), value);
	}

	inline static int32_t get_offset_of_ElasticEaseInOut_21() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___ElasticEaseInOut_21)); }
	inline Curve_t1446801592 * get_ElasticEaseInOut_21() const { return ___ElasticEaseInOut_21; }
	inline Curve_t1446801592 ** get_address_of_ElasticEaseInOut_21() { return &___ElasticEaseInOut_21; }
	inline void set_ElasticEaseInOut_21(Curve_t1446801592 * value)
	{
		___ElasticEaseInOut_21 = value;
		Il2CppCodeGenWriteBarrier((&___ElasticEaseInOut_21), value);
	}

	inline static int32_t get_offset_of_CircularEaseIn_22() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___CircularEaseIn_22)); }
	inline Curve_t1446801592 * get_CircularEaseIn_22() const { return ___CircularEaseIn_22; }
	inline Curve_t1446801592 ** get_address_of_CircularEaseIn_22() { return &___CircularEaseIn_22; }
	inline void set_CircularEaseIn_22(Curve_t1446801592 * value)
	{
		___CircularEaseIn_22 = value;
		Il2CppCodeGenWriteBarrier((&___CircularEaseIn_22), value);
	}

	inline static int32_t get_offset_of_CircularEaseOut_23() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___CircularEaseOut_23)); }
	inline Curve_t1446801592 * get_CircularEaseOut_23() const { return ___CircularEaseOut_23; }
	inline Curve_t1446801592 ** get_address_of_CircularEaseOut_23() { return &___CircularEaseOut_23; }
	inline void set_CircularEaseOut_23(Curve_t1446801592 * value)
	{
		___CircularEaseOut_23 = value;
		Il2CppCodeGenWriteBarrier((&___CircularEaseOut_23), value);
	}

	inline static int32_t get_offset_of_CircularEaseInOut_24() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___CircularEaseInOut_24)); }
	inline Curve_t1446801592 * get_CircularEaseInOut_24() const { return ___CircularEaseInOut_24; }
	inline Curve_t1446801592 ** get_address_of_CircularEaseInOut_24() { return &___CircularEaseInOut_24; }
	inline void set_CircularEaseInOut_24(Curve_t1446801592 * value)
	{
		___CircularEaseInOut_24 = value;
		Il2CppCodeGenWriteBarrier((&___CircularEaseInOut_24), value);
	}

	inline static int32_t get_offset_of_ExponentialEaseIn_25() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___ExponentialEaseIn_25)); }
	inline Curve_t1446801592 * get_ExponentialEaseIn_25() const { return ___ExponentialEaseIn_25; }
	inline Curve_t1446801592 ** get_address_of_ExponentialEaseIn_25() { return &___ExponentialEaseIn_25; }
	inline void set_ExponentialEaseIn_25(Curve_t1446801592 * value)
	{
		___ExponentialEaseIn_25 = value;
		Il2CppCodeGenWriteBarrier((&___ExponentialEaseIn_25), value);
	}

	inline static int32_t get_offset_of_ExponentialEaseOut_26() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___ExponentialEaseOut_26)); }
	inline Curve_t1446801592 * get_ExponentialEaseOut_26() const { return ___ExponentialEaseOut_26; }
	inline Curve_t1446801592 ** get_address_of_ExponentialEaseOut_26() { return &___ExponentialEaseOut_26; }
	inline void set_ExponentialEaseOut_26(Curve_t1446801592 * value)
	{
		___ExponentialEaseOut_26 = value;
		Il2CppCodeGenWriteBarrier((&___ExponentialEaseOut_26), value);
	}

	inline static int32_t get_offset_of_ExponentialEaseInOut_27() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___ExponentialEaseInOut_27)); }
	inline Curve_t1446801592 * get_ExponentialEaseInOut_27() const { return ___ExponentialEaseInOut_27; }
	inline Curve_t1446801592 ** get_address_of_ExponentialEaseInOut_27() { return &___ExponentialEaseInOut_27; }
	inline void set_ExponentialEaseInOut_27(Curve_t1446801592 * value)
	{
		___ExponentialEaseInOut_27 = value;
		Il2CppCodeGenWriteBarrier((&___ExponentialEaseInOut_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVE_T1446801592_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef QUARTICEASEINCURVE_T3162537896_H
#define QUARTICEASEINCURVE_T3162537896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/QuarticEaseInCurve
struct  QuarticEaseInCurve_t3162537896  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUARTICEASEINCURVE_T3162537896_H
#ifndef ENUMERATOR_T645128053_H
#define ENUMERATOR_T645128053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<TweenAnimation.Models.TweenAnimation>
struct  Enumerator_t645128053 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t3050851472 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	TweenAnimation_t1578776730 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t645128053, ___l_0)); }
	inline List_1_t3050851472 * get_l_0() const { return ___l_0; }
	inline List_1_t3050851472 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t3050851472 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t645128053, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t645128053, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t645128053, ___current_3)); }
	inline TweenAnimation_t1578776730 * get_current_3() const { return ___current_3; }
	inline TweenAnimation_t1578776730 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(TweenAnimation_t1578776730 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T645128053_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef ENUMERATOR_T2146457487_H
#define ENUMERATOR_T2146457487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t2146457487 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t257213610 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___l_0)); }
	inline List_1_t257213610 * get_l_0() const { return ___l_0; }
	inline List_1_t257213610 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t257213610 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2146457487_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef SINEEASEINCURVE_T3529713798_H
#define SINEEASEINCURVE_T3529713798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/SineEaseInCurve
struct  SineEaseInCurve_t3529713798  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINEEASEINCURVE_T3529713798_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef QUARTICEASEINOUTCURVE_T1421613264_H
#define QUARTICEASEINOUTCURVE_T1421613264_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/QuarticEaseInOutCurve
struct  QuarticEaseInOutCurve_t1421613264  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUARTICEASEINOUTCURVE_T1421613264_H
#ifndef QUARTICEASEOUTCURVE_T3969926341_H
#define QUARTICEASEOUTCURVE_T3969926341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/QuarticEaseOutCurve
struct  QuarticEaseOutCurve_t3969926341  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUARTICEASEOUTCURVE_T3969926341_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef SINEEASEINOUTCURVE_T3024528697_H
#define SINEEASEINOUTCURVE_T3024528697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/SineEaseInOutCurve
struct  SineEaseInOutCurve_t3024528697  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINEEASEINOUTCURVE_T3024528697_H
#ifndef SINEEASEOUTCURVE_T349540267_H
#define SINEEASEOUTCURVE_T349540267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/SineEaseOutCurve
struct  SineEaseOutCurve_t349540267  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINEEASEOUTCURVE_T349540267_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef WEAPON_T4063826929_H
#define WEAPON_T4063826929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Weapon
struct  Weapon_t4063826929 
{
public:
	// System.Int32 Weapon::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Weapon_t4063826929, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEAPON_T4063826929_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef ACTION_T1264377477_H
#define ACTION_T1264377477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action
struct  Action_t1264377477  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T1264377477_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef FUNC_2_T1832049457_H
#define FUNC_2_T1832049457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<TweenAnimation.Models.TweenAnimation,System.Boolean>
struct  Func_2_t1832049457  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T1832049457_H
#ifndef ACTION_1_T1751244325_H
#define ACTION_1_T1751244325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<TweenAnimation.Models.TweenAnimation>
struct  Action_1_t1751244325  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T1751244325_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef TWEENSCRIPT_T2995776179_H
#define TWEENSCRIPT_T2995776179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenScript
struct  TweenScript_t2995776179  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TweenScript::timeScale
	float ___timeScale_2;
	// System.Collections.Generic.List`1<TweenAnimation.Models.TweenAnimation> TweenScript::animations
	List_1_t3050851472 * ___animations_3;
	// System.Collections.Generic.List`1<TweenAnimation.Models.TweenAnimation> TweenScript::animationsToEnqeue
	List_1_t3050851472 * ___animationsToEnqeue_4;

public:
	inline static int32_t get_offset_of_timeScale_2() { return static_cast<int32_t>(offsetof(TweenScript_t2995776179, ___timeScale_2)); }
	inline float get_timeScale_2() const { return ___timeScale_2; }
	inline float* get_address_of_timeScale_2() { return &___timeScale_2; }
	inline void set_timeScale_2(float value)
	{
		___timeScale_2 = value;
	}

	inline static int32_t get_offset_of_animations_3() { return static_cast<int32_t>(offsetof(TweenScript_t2995776179, ___animations_3)); }
	inline List_1_t3050851472 * get_animations_3() const { return ___animations_3; }
	inline List_1_t3050851472 ** get_address_of_animations_3() { return &___animations_3; }
	inline void set_animations_3(List_1_t3050851472 * value)
	{
		___animations_3 = value;
		Il2CppCodeGenWriteBarrier((&___animations_3), value);
	}

	inline static int32_t get_offset_of_animationsToEnqeue_4() { return static_cast<int32_t>(offsetof(TweenScript_t2995776179, ___animationsToEnqeue_4)); }
	inline List_1_t3050851472 * get_animationsToEnqeue_4() const { return ___animationsToEnqeue_4; }
	inline List_1_t3050851472 ** get_address_of_animationsToEnqeue_4() { return &___animationsToEnqeue_4; }
	inline void set_animationsToEnqeue_4(List_1_t3050851472 * value)
	{
		___animationsToEnqeue_4 = value;
		Il2CppCodeGenWriteBarrier((&___animationsToEnqeue_4), value);
	}
};

struct TweenScript_t2995776179_StaticFields
{
public:
	// TweenScript TweenScript::<Instance>k__BackingField
	TweenScript_t2995776179 * ___U3CInstanceU3Ek__BackingField_5;
	// System.Func`2<TweenAnimation.Models.TweenAnimation,System.Boolean> TweenScript::<>f__am$cache0
	Func_2_t1832049457 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TweenScript_t2995776179_StaticFields, ___U3CInstanceU3Ek__BackingField_5)); }
	inline TweenScript_t2995776179 * get_U3CInstanceU3Ek__BackingField_5() const { return ___U3CInstanceU3Ek__BackingField_5; }
	inline TweenScript_t2995776179 ** get_address_of_U3CInstanceU3Ek__BackingField_5() { return &___U3CInstanceU3Ek__BackingField_5; }
	inline void set_U3CInstanceU3Ek__BackingField_5(TweenScript_t2995776179 * value)
	{
		___U3CInstanceU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(TweenScript_t2995776179_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Func_2_t1832049457 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Func_2_t1832049457 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Func_2_t1832049457 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENSCRIPT_T2995776179_H


// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
extern "C"  void List_1_AddRange_m3709462088_gshared (List_1_t257213610 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m3697625829_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t2146457487  List_1_GetEnumerator_m2930774921_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m470245444_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2142368520_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3007748546_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Void System.Func`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m3104565095_gshared (Func_2_t3759279471 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  RuntimeObject* Enumerable_Where_TisRuntimeObject_m3454096398_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, Func_2_t3759279471 * p1, const RuntimeMethod* method);
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  List_1_t257213610 * Enumerable_ToList_TisRuntimeObject_m1610189404_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
extern "C"  bool List_1_Remove_m1416767016_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m118522912_gshared (Action_1_t3252573759 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::ForEach(System.Action`1<!0>)
extern "C"  void List_1_ForEach_m3737504377_gshared (List_1_t257213610 * __this, Action_1_t3252573759 * p0, const RuntimeMethod* method);

// System.Void TweenAnimation.Models.Curve::.ctor()
extern "C"  void Curve__ctor_m733493893 (Curve_t1446801592 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_Subtraction_m3073674971 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_op_Multiply_m3376773913 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_Addition_m779775034 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
extern "C"  Vector3_t3722313464  Quaternion_get_eulerAngles_m3425202016 (Quaternion_t2301928331 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
extern "C"  Quaternion_t2301928331  Quaternion_Euler_m1803555822 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t3722313464  Vector3_get_zero_m1409827619 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single TweenAnimation.Models.Curve/SineEaseInCurve::SineIn(System.Single)
extern "C"  float SineEaseInCurve_SineIn_m1117613811 (SineEaseInCurve_t3529713798 * __this, float ___t0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single TweenAnimation.Models.Curve/SineEaseInOutCurve::SineInOut(System.Single)
extern "C"  float SineEaseInOutCurve_SineInOut_m1921845045 (SineEaseInOutCurve_t3024528697 * __this, float ___t0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single TweenAnimation.Models.Curve/SineEaseOutCurve::SineOut(System.Single)
extern "C"  float SineEaseOutCurve_SineOut_m379528209 (SineEaseOutCurve_t349540267 * __this, float ___t0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_time()
extern "C"  float Time_get_time_m2907476221 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// TweenScript TweenScript::get_Instance()
extern "C"  TweenScript_t2995776179 * TweenScript_get_Instance_m3414923116 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TweenScript::Enqueue(TweenAnimation.Models.TweenAnimation)
extern "C"  void TweenScript_Enqueue_m1389370299 (TweenScript_t2995776179 * __this, TweenAnimation_t1578776730 * ___animation0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action::Invoke()
extern "C"  void Action_Invoke_m937035532 (Action_t1264377477 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<TweenAnimation.Models.TweenAnimation>::.ctor()
#define List_1__ctor_m465774663(__this, method) ((  void (*) (List_1_t3050851472 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
extern "C"  void MonoBehaviour_print_m330341231 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TweenScript::set_Instance(TweenScript)
extern "C"  void TweenScript_set_Instance_m2955198855 (RuntimeObject * __this /* static, unused */, TweenScript_t2995776179 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<TweenAnimation.Models.TweenAnimation>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
#define List_1_AddRange_m4287091714(__this, p0, method) ((  void (*) (List_1_t3050851472 *, RuntimeObject*, const RuntimeMethod*))List_1_AddRange_m3709462088_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<TweenAnimation.Models.TweenAnimation>::Clear()
#define List_1_Clear_m2453644996(__this, method) ((  void (*) (List_1_t3050851472 *, const RuntimeMethod*))List_1_Clear_m3697625829_gshared)(__this, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<TweenAnimation.Models.TweenAnimation>::GetEnumerator()
#define List_1_GetEnumerator_m1204443697(__this, method) ((  Enumerator_t645128053  (*) (List_1_t3050851472 *, const RuntimeMethod*))List_1_GetEnumerator_m2930774921_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<TweenAnimation.Models.TweenAnimation>::get_Current()
#define Enumerator_get_Current_m673827862(__this, method) ((  TweenAnimation_t1578776730 * (*) (Enumerator_t645128053 *, const RuntimeMethod*))Enumerator_get_Current_m470245444_gshared)(__this, method)
// System.Void TweenAnimation.Models.TweenAnimation::Update()
extern "C"  void TweenAnimation_Update_m1480098314 (TweenAnimation_t1578776730 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenAnimation.Models.TweenAnimation::get_IsFinished()
extern "C"  bool TweenAnimation_get_IsFinished_m4219775029 (TweenAnimation_t1578776730 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<TweenAnimation.Models.TweenAnimation>::MoveNext()
#define Enumerator_MoveNext_m353787894(__this, method) ((  bool (*) (Enumerator_t645128053 *, const RuntimeMethod*))Enumerator_MoveNext_m2142368520_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<TweenAnimation.Models.TweenAnimation>::Dispose()
#define Enumerator_Dispose_m2309435788(__this, method) ((  void (*) (Enumerator_t645128053 *, const RuntimeMethod*))Enumerator_Dispose_m3007748546_gshared)(__this, method)
// System.Void System.Func`2<TweenAnimation.Models.TweenAnimation,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m2927733507(__this, p0, p1, method) ((  void (*) (Func_2_t1832049457 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_m3104565095_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<TweenAnimation.Models.TweenAnimation>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisTweenAnimation_t1578776730_m2727003430(__this /* static, unused */, p0, p1, method) ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Func_2_t1832049457 *, const RuntimeMethod*))Enumerable_Where_TisRuntimeObject_m3454096398_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<TweenAnimation.Models.TweenAnimation>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisTweenAnimation_t1578776730_m855476414(__this /* static, unused */, p0, method) ((  List_1_t3050851472 * (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))Enumerable_ToList_TisRuntimeObject_m1610189404_gshared)(__this /* static, unused */, p0, method)
// System.Void System.Collections.Generic.List`1<TweenAnimation.Models.TweenAnimation>::Add(!0)
#define List_1_Add_m702000797(__this, p0, method) ((  void (*) (List_1_t3050851472 *, TweenAnimation_t1578776730 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.List`1<TweenAnimation.Models.TweenAnimation>::Remove(!0)
#define List_1_Remove_m4213504351(__this, p0, method) ((  bool (*) (List_1_t3050851472 *, TweenAnimation_t1578776730 *, const RuntimeMethod*))List_1_Remove_m1416767016_gshared)(__this, p0, method)
// System.Void System.Action`1<TweenAnimation.Models.TweenAnimation>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m1160958587(__this, p0, p1, method) ((  void (*) (Action_1_t1751244325 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_m118522912_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.List`1<TweenAnimation.Models.TweenAnimation>::ForEach(System.Action`1<!0>)
#define List_1_ForEach_m2023082796(__this, p0, method) ((  void (*) (List_1_t3050851472 *, Action_1_t1751244325 *, const RuntimeMethod*))List_1_ForEach_m3737504377_gshared)(__this, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TweenAnimation.Models.Curve/QuarticEaseInCurve::.ctor()
extern "C"  void QuarticEaseInCurve__ctor_m226203159 (QuarticEaseInCurve_t3162537896 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuarticEaseInCurve__ctor_m226203159_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Curve_t1446801592_il2cpp_TypeInfo_var);
		Curve__ctor_m733493893(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 TweenAnimation.Models.Curve/QuarticEaseInCurve::Interpolate(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  QuarticEaseInCurve_Interpolate_m3318808311 (QuarticEaseInCurve_t3162537896 * __this, Vector3_t3722313464  ___v00, Vector3_t3722313464  ___v11, float ___t2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuarticEaseInCurve_Interpolate_m3318808311_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t3722313464  L_0 = ___v00;
		Vector3_t3722313464  L_1 = ___v11;
		Vector3_t3722313464  L_2 = ___v00;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		float L_4 = ___t2;
		Vector3_t3722313464  L_5 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = ___t2;
		Vector3_t3722313464  L_7 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		float L_8 = ___t2;
		Vector3_t3722313464  L_9 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		float L_10 = ___t2;
		Vector3_t3722313464  L_11 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		Vector3_t3722313464  L_12 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_0, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// UnityEngine.Quaternion TweenAnimation.Models.Curve/QuarticEaseInCurve::Interpolate(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t2301928331  QuarticEaseInCurve_Interpolate_m1834291297 (QuarticEaseInCurve_t3162537896 * __this, Quaternion_t2301928331  ___q00, Quaternion_t2301928331  ___q11, float ___t2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuarticEaseInCurve_Interpolate_m1834291297_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = Quaternion_get_eulerAngles_m3425202016((Quaternion_t2301928331 *)(&___q00), /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Quaternion_get_eulerAngles_m3425202016((Quaternion_t2301928331 *)(&___q11), /*hidden argument*/NULL);
		Vector3_t3722313464  L_2 = Quaternion_get_eulerAngles_m3425202016((Quaternion_t2301928331 *)(&___q00), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		float L_4 = ___t2;
		Vector3_t3722313464  L_5 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = ___t2;
		Vector3_t3722313464  L_7 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		float L_8 = ___t2;
		Vector3_t3722313464  L_9 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		float L_10 = ___t2;
		Vector3_t3722313464  L_11 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		Vector3_t3722313464  L_12 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_0, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		Vector3_t3722313464  L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_14 = Quaternion_Euler_m1803555822(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TweenAnimation.Models.Curve/QuarticEaseInOutCurve::.ctor()
extern "C"  void QuarticEaseInOutCurve__ctor_m2652695060 (QuarticEaseInOutCurve_t1421613264 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuarticEaseInOutCurve__ctor_m2652695060_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Curve_t1446801592_il2cpp_TypeInfo_var);
		Curve__ctor_m733493893(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 TweenAnimation.Models.Curve/QuarticEaseInOutCurve::Interpolate(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  QuarticEaseInOutCurve_Interpolate_m272778676 (QuarticEaseInOutCurve_t1421613264 * __this, Vector3_t3722313464  ___v00, Vector3_t3722313464  ___v11, float ___t2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuarticEaseInOutCurve_Interpolate_m272778676_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___t2;
		___t2 = ((float)il2cpp_codegen_multiply((float)L_0, (float)(2.0f)));
		float L_1 = ___t2;
		if ((!(((float)L_1) < ((float)(1.0f)))))
		{
			goto IL_0044;
		}
	}
	{
		Vector3_t3722313464  L_2 = ___v00;
		Vector3_t3722313464  L_3 = ___v11;
		Vector3_t3722313464  L_4 = ___v00;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_5 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Vector3_t3722313464  L_6 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_5, (0.5f), /*hidden argument*/NULL);
		float L_7 = ___t2;
		Vector3_t3722313464  L_8 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		float L_9 = ___t2;
		Vector3_t3722313464  L_10 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		float L_11 = ___t2;
		Vector3_t3722313464  L_12 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		float L_13 = ___t2;
		Vector3_t3722313464  L_14 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		Vector3_t3722313464  L_15 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_2, L_14, /*hidden argument*/NULL);
		return L_15;
	}

IL_0044:
	{
		float L_16 = ___t2;
		___t2 = ((float)il2cpp_codegen_subtract((float)L_16, (float)(2.0f)));
		Vector3_t3722313464  L_17 = ___v00;
		Vector3_t3722313464  L_18 = ___v11;
		Vector3_t3722313464  L_19 = ___v00;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_20 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		Vector3_t3722313464  L_21 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_20, (0.5f), /*hidden argument*/NULL);
		float L_22 = ___t2;
		float L_23 = ___t2;
		float L_24 = ___t2;
		float L_25 = ___t2;
		Vector3_t3722313464  L_26 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_21, ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)((-L_22)), (float)L_23)), (float)L_24)), (float)L_25)), (float)(2.0f))), /*hidden argument*/NULL);
		Vector3_t3722313464  L_27 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_17, L_26, /*hidden argument*/NULL);
		return L_27;
	}
}
// UnityEngine.Quaternion TweenAnimation.Models.Curve/QuarticEaseInOutCurve::Interpolate(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t2301928331  QuarticEaseInOutCurve_Interpolate_m37107443 (QuarticEaseInOutCurve_t1421613264 * __this, Quaternion_t2301928331  ___q00, Quaternion_t2301928331  ___q11, float ___t2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuarticEaseInOutCurve_Interpolate_m37107443_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_0 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___t2;
		___t2 = ((float)il2cpp_codegen_multiply((float)L_1, (float)(2.0f)));
		float L_2 = ___t2;
		if ((!(((float)L_2) < ((float)(1.0f)))))
		{
			goto IL_0061;
		}
	}
	{
		Vector3_t3722313464  L_3 = Quaternion_get_eulerAngles_m3425202016((Quaternion_t2301928331 *)(&___q00), /*hidden argument*/NULL);
		Vector3_t3722313464  L_4 = Quaternion_get_eulerAngles_m3425202016((Quaternion_t2301928331 *)(&___q11), /*hidden argument*/NULL);
		Vector3_t3722313464  L_5 = Quaternion_get_eulerAngles_m3425202016((Quaternion_t2301928331 *)(&___q00), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_6 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_6, (0.5f), /*hidden argument*/NULL);
		float L_8 = ___t2;
		Vector3_t3722313464  L_9 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		float L_10 = ___t2;
		Vector3_t3722313464  L_11 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		float L_12 = ___t2;
		Vector3_t3722313464  L_13 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		float L_14 = ___t2;
		Vector3_t3722313464  L_15 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		Vector3_t3722313464  L_16 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_3, L_15, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_00a7;
	}

IL_0061:
	{
		float L_17 = ___t2;
		___t2 = ((float)il2cpp_codegen_subtract((float)L_17, (float)(2.0f)));
		Vector3_t3722313464  L_18 = Quaternion_get_eulerAngles_m3425202016((Quaternion_t2301928331 *)(&___q00), /*hidden argument*/NULL);
		Vector3_t3722313464  L_19 = Quaternion_get_eulerAngles_m3425202016((Quaternion_t2301928331 *)(&___q11), /*hidden argument*/NULL);
		Vector3_t3722313464  L_20 = Quaternion_get_eulerAngles_m3425202016((Quaternion_t2301928331 *)(&___q00), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_21 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		Vector3_t3722313464  L_22 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_21, (0.5f), /*hidden argument*/NULL);
		float L_23 = ___t2;
		float L_24 = ___t2;
		float L_25 = ___t2;
		float L_26 = ___t2;
		Vector3_t3722313464  L_27 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_22, ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)((-L_23)), (float)L_24)), (float)L_25)), (float)L_26)), (float)(2.0f))), /*hidden argument*/NULL);
		Vector3_t3722313464  L_28 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_18, L_27, /*hidden argument*/NULL);
		V_0 = L_28;
	}

IL_00a7:
	{
		Vector3_t3722313464  L_29 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_30 = Quaternion_Euler_m1803555822(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		return L_30;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TweenAnimation.Models.Curve/QuarticEaseOutCurve::.ctor()
extern "C"  void QuarticEaseOutCurve__ctor_m1956297805 (QuarticEaseOutCurve_t3969926341 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuarticEaseOutCurve__ctor_m1956297805_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Curve_t1446801592_il2cpp_TypeInfo_var);
		Curve__ctor_m733493893(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 TweenAnimation.Models.Curve/QuarticEaseOutCurve::Interpolate(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  QuarticEaseOutCurve_Interpolate_m502579920 (QuarticEaseOutCurve_t3969926341 * __this, Vector3_t3722313464  ___v00, Vector3_t3722313464  ___v11, float ___t2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuarticEaseOutCurve_Interpolate_m502579920_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t3722313464  L_0 = ___v00;
		Vector3_t3722313464  L_1 = ___v11;
		Vector3_t3722313464  L_2 = ___v00;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		float L_4 = ___t2;
		float L_5 = ___t2;
		float L_6 = ___t2;
		float L_7 = ___t2;
		Vector3_t3722313464  L_8 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_3, ((float)il2cpp_codegen_add((float)(1.0f), (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_4, (float)(1.0f))), (float)((float)il2cpp_codegen_subtract((float)L_5, (float)(1.0f))))), (float)((float)il2cpp_codegen_subtract((float)L_6, (float)(1.0f))))), (float)((float)il2cpp_codegen_subtract((float)L_7, (float)(1.0f))))))), /*hidden argument*/NULL);
		Vector3_t3722313464  L_9 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_0, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Quaternion TweenAnimation.Models.Curve/QuarticEaseOutCurve::Interpolate(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t2301928331  QuarticEaseOutCurve_Interpolate_m3600053911 (QuarticEaseOutCurve_t3969926341 * __this, Quaternion_t2301928331  ___q00, Quaternion_t2301928331  ___q11, float ___t2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuarticEaseOutCurve_Interpolate_m3600053911_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = Quaternion_get_eulerAngles_m3425202016((Quaternion_t2301928331 *)(&___q00), /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Quaternion_get_eulerAngles_m3425202016((Quaternion_t2301928331 *)(&___q11), /*hidden argument*/NULL);
		Vector3_t3722313464  L_2 = Quaternion_get_eulerAngles_m3425202016((Quaternion_t2301928331 *)(&___q00), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		float L_4 = ___t2;
		float L_5 = ___t2;
		float L_6 = ___t2;
		float L_7 = ___t2;
		Vector3_t3722313464  L_8 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_3, ((float)il2cpp_codegen_add((float)(1.0f), (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_4, (float)(1.0f))), (float)((float)il2cpp_codegen_subtract((float)L_5, (float)(1.0f))))), (float)((float)il2cpp_codegen_subtract((float)L_6, (float)(1.0f))))), (float)((float)il2cpp_codegen_subtract((float)L_7, (float)(1.0f))))))), /*hidden argument*/NULL);
		Vector3_t3722313464  L_9 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_0, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		Vector3_t3722313464  L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_11 = Quaternion_Euler_m1803555822(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TweenAnimation.Models.Curve/SineEaseInCurve::.ctor()
extern "C"  void SineEaseInCurve__ctor_m2069362206 (SineEaseInCurve_t3529713798 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SineEaseInCurve__ctor_m2069362206_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Curve_t1446801592_il2cpp_TypeInfo_var);
		Curve__ctor_m733493893(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single TweenAnimation.Models.Curve/SineEaseInCurve::SineIn(System.Single)
extern "C"  float SineEaseInCurve_SineIn_m1117613811 (SineEaseInCurve_t3529713798 * __this, float ___t0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SineEaseInCurve_SineIn_m1117613811_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___t0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_1 = sinf(((float)((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_0, (float)(1.0f))), (float)(3.14159274f)))/(float)(2.0f))));
		return ((float)il2cpp_codegen_add((float)L_1, (float)(1.0f)));
	}
}
// UnityEngine.Vector3 TweenAnimation.Models.Curve/SineEaseInCurve::Interpolate(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  SineEaseInCurve_Interpolate_m4184295870 (SineEaseInCurve_t3529713798 * __this, Vector3_t3722313464  ___v00, Vector3_t3722313464  ___v11, float ___t2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SineEaseInCurve_Interpolate_m4184295870_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t3722313464  L_0 = ___v00;
		Vector3_t3722313464  L_1 = ___v11;
		Vector3_t3722313464  L_2 = ___v00;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		float L_4 = ___t2;
		float L_5 = SineEaseInCurve_SineIn_m1117613811(__this, L_4, /*hidden argument*/NULL);
		Vector3_t3722313464  L_6 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_0, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// UnityEngine.Quaternion TweenAnimation.Models.Curve/SineEaseInCurve::Interpolate(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t2301928331  SineEaseInCurve_Interpolate_m4158627042 (SineEaseInCurve_t3529713798 * __this, Quaternion_t2301928331  ___q00, Quaternion_t2301928331  ___q11, float ___t2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SineEaseInCurve_Interpolate_m4158627042_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = Quaternion_get_eulerAngles_m3425202016((Quaternion_t2301928331 *)(&___q00), /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Quaternion_get_eulerAngles_m3425202016((Quaternion_t2301928331 *)(&___q11), /*hidden argument*/NULL);
		Vector3_t3722313464  L_2 = Quaternion_get_eulerAngles_m3425202016((Quaternion_t2301928331 *)(&___q00), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		float L_4 = ___t2;
		float L_5 = SineEaseInCurve_SineIn_m1117613811(__this, L_4, /*hidden argument*/NULL);
		Vector3_t3722313464  L_6 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_0, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Vector3_t3722313464  L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_9 = Quaternion_Euler_m1803555822(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TweenAnimation.Models.Curve/SineEaseInOutCurve::.ctor()
extern "C"  void SineEaseInOutCurve__ctor_m536029866 (SineEaseInOutCurve_t3024528697 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SineEaseInOutCurve__ctor_m536029866_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Curve_t1446801592_il2cpp_TypeInfo_var);
		Curve__ctor_m733493893(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single TweenAnimation.Models.Curve/SineEaseInOutCurve::SineInOut(System.Single)
extern "C"  float SineEaseInOutCurve_SineInOut_m1921845045 (SineEaseInOutCurve_t3024528697 * __this, float ___t0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SineEaseInOutCurve_SineInOut_m1921845045_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___t0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_1 = cosf(((float)il2cpp_codegen_multiply((float)L_0, (float)(3.14159274f))));
		return ((float)il2cpp_codegen_multiply((float)(0.5f), (float)((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_1))));
	}
}
// UnityEngine.Vector3 TweenAnimation.Models.Curve/SineEaseInOutCurve::Interpolate(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  SineEaseInOutCurve_Interpolate_m586090789 (SineEaseInOutCurve_t3024528697 * __this, Vector3_t3722313464  ___v00, Vector3_t3722313464  ___v11, float ___t2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SineEaseInOutCurve_Interpolate_m586090789_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t3722313464  L_0 = ___v00;
		Vector3_t3722313464  L_1 = ___v11;
		Vector3_t3722313464  L_2 = ___v00;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		float L_4 = ___t2;
		float L_5 = SineEaseInOutCurve_SineInOut_m1921845045(__this, L_4, /*hidden argument*/NULL);
		Vector3_t3722313464  L_6 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_0, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// UnityEngine.Quaternion TweenAnimation.Models.Curve/SineEaseInOutCurve::Interpolate(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t2301928331  SineEaseInOutCurve_Interpolate_m1920943708 (SineEaseInOutCurve_t3024528697 * __this, Quaternion_t2301928331  ___q00, Quaternion_t2301928331  ___q11, float ___t2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SineEaseInOutCurve_Interpolate_m1920943708_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = Quaternion_get_eulerAngles_m3425202016((Quaternion_t2301928331 *)(&___q00), /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Quaternion_get_eulerAngles_m3425202016((Quaternion_t2301928331 *)(&___q11), /*hidden argument*/NULL);
		Vector3_t3722313464  L_2 = Quaternion_get_eulerAngles_m3425202016((Quaternion_t2301928331 *)(&___q00), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		float L_4 = ___t2;
		float L_5 = SineEaseInOutCurve_SineInOut_m1921845045(__this, L_4, /*hidden argument*/NULL);
		Vector3_t3722313464  L_6 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_0, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Vector3_t3722313464  L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_9 = Quaternion_Euler_m1803555822(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TweenAnimation.Models.Curve/SineEaseOutCurve::.ctor()
extern "C"  void SineEaseOutCurve__ctor_m970120592 (SineEaseOutCurve_t349540267 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SineEaseOutCurve__ctor_m970120592_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Curve_t1446801592_il2cpp_TypeInfo_var);
		Curve__ctor_m733493893(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single TweenAnimation.Models.Curve/SineEaseOutCurve::SineOut(System.Single)
extern "C"  float SineEaseOutCurve_SineOut_m379528209 (SineEaseOutCurve_t349540267 * __this, float ___t0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SineEaseOutCurve_SineOut_m379528209_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___t0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_1 = sinf(((float)((float)((float)il2cpp_codegen_multiply((float)L_0, (float)(3.14159274f)))/(float)(2.0f))));
		return L_1;
	}
}
// UnityEngine.Vector3 TweenAnimation.Models.Curve/SineEaseOutCurve::Interpolate(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  SineEaseOutCurve_Interpolate_m3953486845 (SineEaseOutCurve_t349540267 * __this, Vector3_t3722313464  ___v00, Vector3_t3722313464  ___v11, float ___t2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SineEaseOutCurve_Interpolate_m3953486845_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t3722313464  L_0 = ___v00;
		Vector3_t3722313464  L_1 = ___v11;
		Vector3_t3722313464  L_2 = ___v00;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		float L_4 = ___t2;
		float L_5 = SineEaseOutCurve_SineOut_m379528209(__this, L_4, /*hidden argument*/NULL);
		Vector3_t3722313464  L_6 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_0, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// UnityEngine.Quaternion TweenAnimation.Models.Curve/SineEaseOutCurve::Interpolate(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t2301928331  SineEaseOutCurve_Interpolate_m4082407585 (SineEaseOutCurve_t349540267 * __this, Quaternion_t2301928331  ___q00, Quaternion_t2301928331  ___q11, float ___t2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SineEaseOutCurve_Interpolate_m4082407585_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = Quaternion_get_eulerAngles_m3425202016((Quaternion_t2301928331 *)(&___q00), /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Quaternion_get_eulerAngles_m3425202016((Quaternion_t2301928331 *)(&___q11), /*hidden argument*/NULL);
		Vector3_t3722313464  L_2 = Quaternion_get_eulerAngles_m3425202016((Quaternion_t2301928331 *)(&___q00), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		float L_4 = ___t2;
		float L_5 = SineEaseOutCurve_SineOut_m379528209(__this, L_4, /*hidden argument*/NULL);
		Vector3_t3722313464  L_6 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_0, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Vector3_t3722313464  L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_9 = Quaternion_Euler_m1803555822(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TweenAnimation.Models.TweenAnimation::.ctor(UnityEngine.Transform)
extern "C"  void TweenAnimation__ctor_m1020063861 (TweenAnimation_t1578776730 * __this, Transform_t3600365921 * ___transform0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenAnimation__ctor_m1020063861_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Curve_t1446801592_il2cpp_TypeInfo_var);
		Curve_t1446801592 * L_0 = ((Curve_t1446801592_StaticFields*)il2cpp_codegen_static_fields_for(Curve_t1446801592_il2cpp_TypeInfo_var))->get_Linear_0();
		__this->set_curve_1(L_0);
		__this->set_duration_2((1.0f));
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_1 = ___transform0;
		__this->set_transform_5(L_1);
		return;
	}
}
// System.Boolean TweenAnimation.Models.TweenAnimation::get_IsFinished()
extern "C"  bool TweenAnimation_get_IsFinished_m4219775029 (TweenAnimation_t1578776730 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_isFinished_6();
		return L_0;
	}
}
// TweenAnimation.Models.TweenAnimation TweenAnimation.Models.TweenAnimation::For(System.Single)
extern "C"  TweenAnimation_t1578776730 * TweenAnimation_For_m3585087881 (TweenAnimation_t1578776730 * __this, float ___duration0, const RuntimeMethod* method)
{
	{
		float L_0 = ___duration0;
		__this->set_duration_2(L_0);
		return __this;
	}
}
// TweenAnimation.Models.TweenAnimation TweenAnimation.Models.TweenAnimation::Delay(System.Single)
extern "C"  TweenAnimation_t1578776730 * TweenAnimation_Delay_m3094601157 (TweenAnimation_t1578776730 * __this, float ___delay0, const RuntimeMethod* method)
{
	{
		float L_0 = ___delay0;
		__this->set_delay_3(L_0);
		return __this;
	}
}
// TweenAnimation.Models.TweenAnimation TweenAnimation.Models.TweenAnimation::AndThen(System.Action)
extern "C"  TweenAnimation_t1578776730 * TweenAnimation_AndThen_m2805858648 (TweenAnimation_t1578776730 * __this, Action_t1264377477 * ___callback0, const RuntimeMethod* method)
{
	{
		Action_t1264377477 * L_0 = ___callback0;
		__this->set_finishCallback_7(L_0);
		return __this;
	}
}
// TweenAnimation.Models.TweenAnimation TweenAnimation.Models.TweenAnimation::ButBefore(System.Action)
extern "C"  TweenAnimation_t1578776730 * TweenAnimation_ButBefore_m2189244655 (TweenAnimation_t1578776730 * __this, Action_t1264377477 * ___callback0, const RuntimeMethod* method)
{
	{
		Action_t1264377477 * L_0 = ___callback0;
		__this->set_startCallback_8(L_0);
		return __this;
	}
}
// TweenAnimation.Models.TweenAnimation TweenAnimation.Models.TweenAnimation::Over(TweenAnimation.Models.Curve)
extern "C"  TweenAnimation_t1578776730 * TweenAnimation_Over_m176786689 (TweenAnimation_t1578776730 * __this, Curve_t1446801592 * ___curve0, const RuntimeMethod* method)
{
	{
		Curve_t1446801592 * L_0 = ___curve0;
		__this->set_curve_1(L_0);
		return __this;
	}
}
// System.Void TweenAnimation.Models.TweenAnimation::Update()
extern "C"  void TweenAnimation_Update_m1480098314 (TweenAnimation_t1578776730 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenAnimation_Update_m1480098314_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		Transform_t3600365921 * L_0 = __this->get_transform_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		Transform_t3600365921 * L_2 = __this->get_transform_5();
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002f;
		}
	}

IL_0027:
	{
		__this->set_isFinished_6((bool)1);
		return;
	}

IL_002f:
	{
		int32_t L_5 = __this->get_frameCount_0();
		__this->set_frameCount_0(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)));
		float L_6 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = __this->get_startTime_4();
		TweenScript_t2995776179 * L_8 = TweenScript_get_Instance_m3414923116(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		float L_9 = L_8->get_timeScale_2();
		V_0 = ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_6, (float)L_7)), (float)L_9));
		float L_10 = V_0;
		float L_11 = __this->get_delay_3();
		if ((!(((float)L_10) < ((float)L_11))))
		{
			goto IL_0062;
		}
	}
	{
		return;
	}

IL_0062:
	{
		float L_12 = V_0;
		float L_13 = __this->get_delay_3();
		float L_14 = __this->get_duration_2();
		if ((!(((float)L_12) >= ((float)((float)il2cpp_codegen_add((float)L_13, (float)L_14))))))
		{
			goto IL_0088;
		}
	}
	{
		VirtActionInvoker1< float >::Invoke(7 /* System.Void TweenAnimation.Models.TweenAnimation::Tick(System.Single) */, __this, (1.0f));
		__this->set_isFinished_6((bool)1);
		return;
	}

IL_0088:
	{
		float L_15 = V_0;
		float L_16 = __this->get_delay_3();
		float L_17 = __this->get_duration_2();
		V_1 = ((float)((float)((float)il2cpp_codegen_subtract((float)L_15, (float)L_16))/(float)L_17));
		float L_18 = V_1;
		VirtActionInvoker1< float >::Invoke(7 /* System.Void TweenAnimation.Models.TweenAnimation::Tick(System.Single) */, __this, L_18);
		return;
	}
}
// TweenAnimation.Models.TweenAnimation TweenAnimation.Models.TweenAnimation::Start()
extern "C"  TweenAnimation_t1578776730 * TweenAnimation_Start_m232092281 (TweenAnimation_t1578776730 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_startTime_4(L_0);
		VirtActionInvoker1< float >::Invoke(7 /* System.Void TweenAnimation.Models.TweenAnimation::Tick(System.Single) */, __this, (0.0f));
		TweenScript_t2995776179 * L_1 = TweenScript_get_Instance_m3414923116(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		TweenScript_Enqueue_m1389370299(L_1, __this, /*hidden argument*/NULL);
		return __this;
	}
}
// System.Void TweenAnimation.Models.TweenAnimation::Started()
extern "C"  void TweenAnimation_Started_m2603090808 (TweenAnimation_t1578776730 * __this, const RuntimeMethod* method)
{
	{
		Action_t1264377477 * L_0 = __this->get_startCallback_8();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Action_t1264377477 * L_1 = __this->get_startCallback_8();
		NullCheck(L_1);
		Action_Invoke_m937035532(L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void TweenAnimation.Models.TweenAnimation::Finished()
extern "C"  void TweenAnimation_Finished_m338973551 (TweenAnimation_t1578776730 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenAnimation_Finished_m338973551_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3600365921 * L_0 = __this->get_transform_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		Transform_t3600365921 * L_2 = __this->get_transform_5();
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0028;
		}
	}

IL_0027:
	{
		return;
	}

IL_0028:
	{
		Action_t1264377477 * L_5 = __this->get_finishCallback_7();
		if (!L_5)
		{
			goto IL_003e;
		}
	}
	{
		Action_t1264377477 * L_6 = __this->get_finishCallback_7();
		NullCheck(L_6);
		Action_Invoke_m937035532(L_6, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TweenScript::.ctor()
extern "C"  void TweenScript__ctor_m3663774949 (TweenScript_t2995776179 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenScript__ctor_m3663774949_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_timeScale_2((1.0f));
		List_1_t3050851472 * L_0 = (List_1_t3050851472 *)il2cpp_codegen_object_new(List_1_t3050851472_il2cpp_TypeInfo_var);
		List_1__ctor_m465774663(L_0, /*hidden argument*/List_1__ctor_m465774663_RuntimeMethod_var);
		__this->set_animations_3(L_0);
		List_1_t3050851472 * L_1 = (List_1_t3050851472 *)il2cpp_codegen_object_new(List_1_t3050851472_il2cpp_TypeInfo_var);
		List_1__ctor_m465774663(L_1, /*hidden argument*/List_1__ctor_m465774663_RuntimeMethod_var);
		__this->set_animationsToEnqeue_4(L_1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// TweenScript TweenScript::get_Instance()
extern "C"  TweenScript_t2995776179 * TweenScript_get_Instance_m3414923116 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenScript_get_Instance_m3414923116_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TweenScript_t2995776179 * L_0 = ((TweenScript_t2995776179_StaticFields*)il2cpp_codegen_static_fields_for(TweenScript_t2995776179_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void TweenScript::set_Instance(TweenScript)
extern "C"  void TweenScript_set_Instance_m2955198855 (RuntimeObject * __this /* static, unused */, TweenScript_t2995776179 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenScript_set_Instance_m2955198855_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TweenScript_t2995776179 * L_0 = ___value0;
		((TweenScript_t2995776179_StaticFields*)il2cpp_codegen_static_fields_for(TweenScript_t2995776179_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void TweenScript::Awake()
extern "C"  void TweenScript_Awake_m157295781 (TweenScript_t2995776179 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenScript_Awake_m157295781_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, _stringLiteral3417423516, /*hidden argument*/NULL);
		TweenScript_set_Instance_m2955198855(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TweenScript::Update()
extern "C"  void TweenScript_Update_m1366624147 (TweenScript_t2995776179 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenScript_Update_m1366624147_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	TweenAnimation_t1578776730 * V_1 = NULL;
	Enumerator_t645128053  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	List_1_t3050851472 * G_B10_0 = NULL;
	TweenScript_t2995776179 * G_B10_1 = NULL;
	List_1_t3050851472 * G_B9_0 = NULL;
	TweenScript_t2995776179 * G_B9_1 = NULL;
	{
		V_0 = 0;
		List_1_t3050851472 * L_0 = __this->get_animations_3();
		List_1_t3050851472 * L_1 = __this->get_animationsToEnqeue_4();
		NullCheck(L_0);
		List_1_AddRange_m4287091714(L_0, L_1, /*hidden argument*/List_1_AddRange_m4287091714_RuntimeMethod_var);
		List_1_t3050851472 * L_2 = __this->get_animationsToEnqeue_4();
		NullCheck(L_2);
		List_1_Clear_m2453644996(L_2, /*hidden argument*/List_1_Clear_m2453644996_RuntimeMethod_var);
		List_1_t3050851472 * L_3 = __this->get_animations_3();
		NullCheck(L_3);
		Enumerator_t645128053  L_4 = List_1_GetEnumerator_m1204443697(L_3, /*hidden argument*/List_1_GetEnumerator_m1204443697_RuntimeMethod_var);
		V_2 = L_4;
	}

IL_002a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0052;
		}

IL_002f:
		{
			TweenAnimation_t1578776730 * L_5 = Enumerator_get_Current_m673827862((Enumerator_t645128053 *)(&V_2), /*hidden argument*/Enumerator_get_Current_m673827862_RuntimeMethod_var);
			V_1 = L_5;
			TweenAnimation_t1578776730 * L_6 = V_1;
			NullCheck(L_6);
			TweenAnimation_Update_m1480098314(L_6, /*hidden argument*/NULL);
			TweenAnimation_t1578776730 * L_7 = V_1;
			NullCheck(L_7);
			bool L_8 = TweenAnimation_get_IsFinished_m4219775029(L_7, /*hidden argument*/NULL);
			if (!L_8)
			{
				goto IL_0052;
			}
		}

IL_0048:
		{
			int32_t L_9 = V_0;
			V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
			TweenAnimation_t1578776730 * L_10 = V_1;
			NullCheck(L_10);
			VirtActionInvoker0::Invoke(6 /* System.Void TweenAnimation.Models.TweenAnimation::Finished() */, L_10);
		}

IL_0052:
		{
			bool L_11 = Enumerator_MoveNext_m353787894((Enumerator_t645128053 *)(&V_2), /*hidden argument*/Enumerator_MoveNext_m353787894_RuntimeMethod_var);
			if (L_11)
			{
				goto IL_002f;
			}
		}

IL_005e:
		{
			IL2CPP_LEAVE(0x71, FINALLY_0063);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0063;
	}

FINALLY_0063:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2309435788((Enumerator_t645128053 *)(&V_2), /*hidden argument*/Enumerator_Dispose_m2309435788_RuntimeMethod_var);
		IL2CPP_END_FINALLY(99)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(99)
	{
		IL2CPP_JUMP_TBL(0x71, IL_0071)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0071:
	{
		int32_t L_12 = V_0;
		if ((((int32_t)L_12) <= ((int32_t)0)))
		{
			goto IL_00ab;
		}
	}
	{
		List_1_t3050851472 * L_13 = __this->get_animations_3();
		Func_2_t1832049457 * L_14 = ((TweenScript_t2995776179_StaticFields*)il2cpp_codegen_static_fields_for(TweenScript_t2995776179_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_6();
		G_B9_0 = L_13;
		G_B9_1 = __this;
		if (L_14)
		{
			G_B10_0 = L_13;
			G_B10_1 = __this;
			goto IL_0097;
		}
	}
	{
		intptr_t L_15 = (intptr_t)TweenScript_U3CUpdateU3Em__0_m25612604_RuntimeMethod_var;
		Func_2_t1832049457 * L_16 = (Func_2_t1832049457 *)il2cpp_codegen_object_new(Func_2_t1832049457_il2cpp_TypeInfo_var);
		Func_2__ctor_m2927733507(L_16, NULL, L_15, /*hidden argument*/Func_2__ctor_m2927733507_RuntimeMethod_var);
		((TweenScript_t2995776179_StaticFields*)il2cpp_codegen_static_fields_for(TweenScript_t2995776179_il2cpp_TypeInfo_var))->set_U3CU3Ef__amU24cache0_6(L_16);
		G_B10_0 = G_B9_0;
		G_B10_1 = G_B9_1;
	}

IL_0097:
	{
		Func_2_t1832049457 * L_17 = ((TweenScript_t2995776179_StaticFields*)il2cpp_codegen_static_fields_for(TweenScript_t2995776179_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_6();
		RuntimeObject* L_18 = Enumerable_Where_TisTweenAnimation_t1578776730_m2727003430(NULL /*static, unused*/, G_B10_0, L_17, /*hidden argument*/Enumerable_Where_TisTweenAnimation_t1578776730_m2727003430_RuntimeMethod_var);
		List_1_t3050851472 * L_19 = Enumerable_ToList_TisTweenAnimation_t1578776730_m855476414(NULL /*static, unused*/, L_18, /*hidden argument*/Enumerable_ToList_TisTweenAnimation_t1578776730_m855476414_RuntimeMethod_var);
		NullCheck(G_B10_1);
		G_B10_1->set_animations_3(L_19);
	}

IL_00ab:
	{
		return;
	}
}
// System.Void TweenScript::Enqueue(TweenAnimation.Models.TweenAnimation)
extern "C"  void TweenScript_Enqueue_m1389370299 (TweenScript_t2995776179 * __this, TweenAnimation_t1578776730 * ___animation0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenScript_Enqueue_m1389370299_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3050851472 * L_0 = __this->get_animationsToEnqeue_4();
		TweenAnimation_t1578776730 * L_1 = ___animation0;
		NullCheck(L_0);
		List_1_Add_m702000797(L_0, L_1, /*hidden argument*/List_1_Add_m702000797_RuntimeMethod_var);
		TweenAnimation_t1578776730 * L_2 = ___animation0;
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(5 /* System.Void TweenAnimation.Models.TweenAnimation::Started() */, L_2);
		return;
	}
}
// System.Void TweenScript::Dequeue(TweenAnimation.Models.TweenAnimation)
extern "C"  void TweenScript_Dequeue_m3155962551 (TweenScript_t2995776179 * __this, TweenAnimation_t1578776730 * ___animation0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenScript_Dequeue_m3155962551_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3050851472 * L_0 = __this->get_animations_3();
		TweenAnimation_t1578776730 * L_1 = ___animation0;
		NullCheck(L_0);
		List_1_Remove_m4213504351(L_0, L_1, /*hidden argument*/List_1_Remove_m4213504351_RuntimeMethod_var);
		List_1_t3050851472 * L_2 = __this->get_animationsToEnqeue_4();
		TweenAnimation_t1578776730 * L_3 = ___animation0;
		NullCheck(L_2);
		List_1_Remove_m4213504351(L_2, L_3, /*hidden argument*/List_1_Remove_m4213504351_RuntimeMethod_var);
		return;
	}
}
// System.Void TweenScript::Dequeue(System.Collections.Generic.List`1<TweenAnimation.Models.TweenAnimation>)
extern "C"  void TweenScript_Dequeue_m3048948346 (TweenScript_t2995776179 * __this, List_1_t3050851472 * ___animationsToRemove0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenScript_Dequeue_m3048948346_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3050851472 * L_0 = ___animationsToRemove0;
		intptr_t L_1 = (intptr_t)TweenScript_U3CDequeueU3Em__1_m1553395588_RuntimeMethod_var;
		Action_1_t1751244325 * L_2 = (Action_1_t1751244325 *)il2cpp_codegen_object_new(Action_1_t1751244325_il2cpp_TypeInfo_var);
		Action_1__ctor_m1160958587(L_2, __this, L_1, /*hidden argument*/Action_1__ctor_m1160958587_RuntimeMethod_var);
		NullCheck(L_0);
		List_1_ForEach_m2023082796(L_0, L_2, /*hidden argument*/List_1_ForEach_m2023082796_RuntimeMethod_var);
		List_1_t3050851472 * L_3 = ___animationsToRemove0;
		intptr_t L_4 = (intptr_t)TweenScript_U3CDequeueU3Em__2_m512505057_RuntimeMethod_var;
		Action_1_t1751244325 * L_5 = (Action_1_t1751244325 *)il2cpp_codegen_object_new(Action_1_t1751244325_il2cpp_TypeInfo_var);
		Action_1__ctor_m1160958587(L_5, __this, L_4, /*hidden argument*/Action_1__ctor_m1160958587_RuntimeMethod_var);
		NullCheck(L_3);
		List_1_ForEach_m2023082796(L_3, L_5, /*hidden argument*/List_1_ForEach_m2023082796_RuntimeMethod_var);
		return;
	}
}
// System.Boolean TweenScript::<Update>m__0(TweenAnimation.Models.TweenAnimation)
extern "C"  bool TweenScript_U3CUpdateU3Em__0_m25612604 (RuntimeObject * __this /* static, unused */, TweenAnimation_t1578776730 * ___a0, const RuntimeMethod* method)
{
	{
		TweenAnimation_t1578776730 * L_0 = ___a0;
		NullCheck(L_0);
		bool L_1 = TweenAnimation_get_IsFinished_m4219775029(L_0, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void TweenScript::<Dequeue>m__1(TweenAnimation.Models.TweenAnimation)
extern "C"  void TweenScript_U3CDequeueU3Em__1_m1553395588 (TweenScript_t2995776179 * __this, TweenAnimation_t1578776730 * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenScript_U3CDequeueU3Em__1_m1553395588_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3050851472 * L_0 = __this->get_animations_3();
		TweenAnimation_t1578776730 * L_1 = ___x0;
		NullCheck(L_0);
		List_1_Remove_m4213504351(L_0, L_1, /*hidden argument*/List_1_Remove_m4213504351_RuntimeMethod_var);
		return;
	}
}
// System.Void TweenScript::<Dequeue>m__2(TweenAnimation.Models.TweenAnimation)
extern "C"  void TweenScript_U3CDequeueU3Em__2_m512505057 (TweenScript_t2995776179 * __this, TweenAnimation_t1578776730 * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenScript_U3CDequeueU3Em__2_m512505057_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3050851472 * L_0 = __this->get_animationsToEnqeue_4();
		TweenAnimation_t1578776730 * L_1 = ___x0;
		NullCheck(L_0);
		List_1_Remove_m4213504351(L_0, L_1, /*hidden argument*/List_1_Remove_m4213504351_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
