﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.Object
struct Object_t631007953;
// System.String
struct String_t;
// System.Collections.Generic.List`1<TMPro.SpriteAssetUtilities.TexturePacker/SpriteData>
struct List_1_t225505033;
// System.Collections.Generic.List`1<TMPro.KerningPair>
struct List_1_t3742930331;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Func`2<TMPro.KerningPair,System.Int32>
struct Func_2_t2554309206;
// System.Type
struct Type_t;
// UnityEngine.Analytics.AnalyticsEventParamListContainer
struct AnalyticsEventParamListContainer_t587083383;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget>
struct List_1_t235857739;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule>
struct List_1_t3418373063;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// UnityEngine.Analytics.AnalyticsEventTracker
struct AnalyticsEventTracker_t2285229262;
// System.Collections.Generic.List`1<UnityEngine.Analytics.AnalyticsEventParam>
struct List_1_t3952196670;
// TweenAnimation.Models.Curve
struct Curve_t1446801592;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Action
struct Action_t1264377477;
// System.Collections.Generic.List`1<TMPro.TMP_Text>
struct List_1_t4071693616;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1839659084;
// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>
struct List_1_t3593973608;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// TMPro.FastAction`2<System.Object,TMPro.Compute_DT_EventArgs>
struct FastAction_2_t2884460249;
// TMPro.FastAction`2<System.Boolean,UnityEngine.Material>
struct FastAction_2_t2206961073;
// TMPro.FastAction`2<System.Boolean,TMPro.TMP_FontAsset>
struct FastAction_2_t2230967576;
// TMPro.FastAction`2<System.Boolean,UnityEngine.Object>
struct FastAction_2_t2497593903;
// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshPro>
struct FastAction_2_t4260179116;
// TMPro.FastAction`3<UnityEngine.GameObject,UnityEngine.Material,UnityEngine.Material>
struct FastAction_3_t58649212;
// TMPro.FastAction`1<System.Boolean>
struct FastAction_1_t3758825850;
// TMPro.FastAction`1<TMPro.TMP_ColorGradient>
struct FastAction_1_t3044626357;
// TMPro.FastAction
struct FastAction_t3491443480;
// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshProUGUI>
struct FastAction_2_t2395899227;
// TMPro.FastAction`1<UnityEngine.Object>
struct FastAction_1_t4292545838;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t364381626;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_t484820633;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.AnimationClip
struct AnimationClip_t2318505987;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.String[]
struct StringU5BU5D_t1281789340;
// TMPro.TMP_Text
struct TMP_Text_t2599618874;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Func`1<UnityEngine.Vector3>
struct Func_1_t3152060111;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.UI.Image
struct Image_t2670269651;
// TMPro.MaterialReference[]
struct MaterialReferenceU5BU5D_t648826345;
// UnityEngine.Color32[]
struct Color32U5BU5D_t3850468773;
// TMPro.TMP_CharacterInfo[]
struct TMP_CharacterInfoU5BU5D_t1930184704;
// TMPro.TMP_WordInfo[]
struct TMP_WordInfoU5BU5D_t3766301798;
// TMPro.TMP_LinkInfo[]
struct TMP_LinkInfoU5BU5D_t3558768157;
// TMPro.TMP_LineInfo[]
struct TMP_LineInfoU5BU5D_t4120149533;
// TMPro.TMP_PageInfo[]
struct TMP_PageInfoU5BU5D_t2463031060;
// TMPro.TMP_MeshInfo[]
struct TMP_MeshInfoU5BU5D_t3365986247;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// TMPro.TMP_TextElement
struct TMP_TextElement_t129727469;
// TMPro.TextAlignmentOptions[]
struct TextAlignmentOptionsU5BU5D_t3552942253;
// UnityEngine.Analytics.TriggerListContainer
struct TriggerListContainer_t2032715483;
// UnityEngine.Analytics.EventTrigger/OnTrigger
struct OnTrigger_t4184125570;
// UnityEngine.Analytics.TriggerMethod
struct TriggerMethod_t582536534;
// UnityEngine.Analytics.TrackableField
struct TrackableField_t1772682203;
// UnityEngine.Analytics.ValueProperty
struct ValueProperty_t1868393739;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_t3598145122;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.Analytics.TrackableProperty
struct TrackableProperty_t3943537984;
// UnityEngine.Analytics.EventTrigger
struct EventTrigger_t2527451695;
// UnityEngine.Analytics.StandardEventPayload
struct StandardEventPayload_t1629891255;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// TMPro.TextMeshPro
struct TextMeshPro_t2393593166;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.Material[]
struct MaterialU5BU5D_t561872642;
// TMPro.TMP_ColorGradient
struct TMP_ColorGradient_t3678055768;
// TMPro.TMP_SpriteAnimator
struct TMP_SpriteAnimator_t2836635477;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t1785403678;
// TMPro.XML_TagAttribute[]
struct XML_TagAttributeU5BU5D_t284240280;
// TMPro.TMP_Glyph
struct TMP_Glyph_t581847833;
// TMPro.InlineGraphicManager
struct InlineGraphicManager_t2871008645;
// TMPro.TMP_SubMeshUI[]
struct TMP_SubMeshUIU5BU5D_t3119380694;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.MeshFilter
struct MeshFilter_t3523625662;
// TMPro.TMP_SubMesh[]
struct TMP_SubMeshU5BU5D_t1860744960;




#ifndef U3CMODULEU3E_T692745549_H
#define U3CMODULEU3E_T692745549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745549 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745549_H
#ifndef U3CMODULEU3E_T692745548_H
#define U3CMODULEU3E_T692745548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745548 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745548_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745550_H
#define U3CMODULEU3E_T692745550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745550 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745550_H
#ifndef TRACKABLEPROPERTYBASE_T2121532948_H
#define TRACKABLEPROPERTYBASE_T2121532948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackablePropertyBase
struct  TrackablePropertyBase_t2121532948  : public RuntimeObject
{
public:
	// UnityEngine.Object UnityEngine.Analytics.TrackablePropertyBase::m_Target
	Object_t631007953 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackablePropertyBase::m_Path
	String_t* ___m_Path_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackablePropertyBase_t2121532948, ___m_Target_0)); }
	inline Object_t631007953 * get_m_Target_0() const { return ___m_Target_0; }
	inline Object_t631007953 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(Object_t631007953 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(TrackablePropertyBase_t2121532948, ___m_Path_1)); }
	inline String_t* get_m_Path_1() const { return ___m_Path_1; }
	inline String_t** get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(String_t* value)
	{
		___m_Path_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Path_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEPROPERTYBASE_T2121532948_H
#ifndef SPRITEDATAOBJECT_T308163541_H
#define SPRITEDATAOBJECT_T308163541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker/SpriteDataObject
struct  SpriteDataObject_t308163541  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.SpriteAssetUtilities.TexturePacker/SpriteData> TMPro.SpriteAssetUtilities.TexturePacker/SpriteDataObject::frames
	List_1_t225505033 * ___frames_0;

public:
	inline static int32_t get_offset_of_frames_0() { return static_cast<int32_t>(offsetof(SpriteDataObject_t308163541, ___frames_0)); }
	inline List_1_t225505033 * get_frames_0() const { return ___frames_0; }
	inline List_1_t225505033 ** get_address_of_frames_0() { return &___frames_0; }
	inline void set_frames_0(List_1_t225505033 * value)
	{
		___frames_0 = value;
		Il2CppCodeGenWriteBarrier((&___frames_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEDATAOBJECT_T308163541_H
#ifndef TEXTUREPACKER_T3148178657_H
#define TEXTUREPACKER_T3148178657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker
struct  TexturePacker_t3148178657  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREPACKER_T3148178657_H
#ifndef SHADERUTILITIES_T714255158_H
#define SHADERUTILITIES_T714255158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.ShaderUtilities
struct  ShaderUtilities_t714255158  : public RuntimeObject
{
public:

public:
};

struct ShaderUtilities_t714255158_StaticFields
{
public:
	// System.Int32 TMPro.ShaderUtilities::ID_MainTex
	int32_t ___ID_MainTex_0;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceTex
	int32_t ___ID_FaceTex_1;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceColor
	int32_t ___ID_FaceColor_2;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceDilate
	int32_t ___ID_FaceDilate_3;
	// System.Int32 TMPro.ShaderUtilities::ID_Shininess
	int32_t ___ID_Shininess_4;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayColor
	int32_t ___ID_UnderlayColor_5;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayOffsetX
	int32_t ___ID_UnderlayOffsetX_6;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayOffsetY
	int32_t ___ID_UnderlayOffsetY_7;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayDilate
	int32_t ___ID_UnderlayDilate_8;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlaySoftness
	int32_t ___ID_UnderlaySoftness_9;
	// System.Int32 TMPro.ShaderUtilities::ID_WeightNormal
	int32_t ___ID_WeightNormal_10;
	// System.Int32 TMPro.ShaderUtilities::ID_WeightBold
	int32_t ___ID_WeightBold_11;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineTex
	int32_t ___ID_OutlineTex_12;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineWidth
	int32_t ___ID_OutlineWidth_13;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineSoftness
	int32_t ___ID_OutlineSoftness_14;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineColor
	int32_t ___ID_OutlineColor_15;
	// System.Int32 TMPro.ShaderUtilities::ID_GradientScale
	int32_t ___ID_GradientScale_16;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleX
	int32_t ___ID_ScaleX_17;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleY
	int32_t ___ID_ScaleY_18;
	// System.Int32 TMPro.ShaderUtilities::ID_PerspectiveFilter
	int32_t ___ID_PerspectiveFilter_19;
	// System.Int32 TMPro.ShaderUtilities::ID_TextureWidth
	int32_t ___ID_TextureWidth_20;
	// System.Int32 TMPro.ShaderUtilities::ID_TextureHeight
	int32_t ___ID_TextureHeight_21;
	// System.Int32 TMPro.ShaderUtilities::ID_BevelAmount
	int32_t ___ID_BevelAmount_22;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowColor
	int32_t ___ID_GlowColor_23;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowOffset
	int32_t ___ID_GlowOffset_24;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowPower
	int32_t ___ID_GlowPower_25;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowOuter
	int32_t ___ID_GlowOuter_26;
	// System.Int32 TMPro.ShaderUtilities::ID_LightAngle
	int32_t ___ID_LightAngle_27;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMap
	int32_t ___ID_EnvMap_28;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMatrix
	int32_t ___ID_EnvMatrix_29;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMatrixRotation
	int32_t ___ID_EnvMatrixRotation_30;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskCoord
	int32_t ___ID_MaskCoord_31;
	// System.Int32 TMPro.ShaderUtilities::ID_ClipRect
	int32_t ___ID_ClipRect_32;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskSoftnessX
	int32_t ___ID_MaskSoftnessX_33;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskSoftnessY
	int32_t ___ID_MaskSoftnessY_34;
	// System.Int32 TMPro.ShaderUtilities::ID_VertexOffsetX
	int32_t ___ID_VertexOffsetX_35;
	// System.Int32 TMPro.ShaderUtilities::ID_VertexOffsetY
	int32_t ___ID_VertexOffsetY_36;
	// System.Int32 TMPro.ShaderUtilities::ID_UseClipRect
	int32_t ___ID_UseClipRect_37;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilID
	int32_t ___ID_StencilID_38;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilOp
	int32_t ___ID_StencilOp_39;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilComp
	int32_t ___ID_StencilComp_40;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilReadMask
	int32_t ___ID_StencilReadMask_41;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilWriteMask
	int32_t ___ID_StencilWriteMask_42;
	// System.Int32 TMPro.ShaderUtilities::ID_ShaderFlags
	int32_t ___ID_ShaderFlags_43;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_A
	int32_t ___ID_ScaleRatio_A_44;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_B
	int32_t ___ID_ScaleRatio_B_45;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_C
	int32_t ___ID_ScaleRatio_C_46;
	// System.String TMPro.ShaderUtilities::Keyword_Bevel
	String_t* ___Keyword_Bevel_47;
	// System.String TMPro.ShaderUtilities::Keyword_Glow
	String_t* ___Keyword_Glow_48;
	// System.String TMPro.ShaderUtilities::Keyword_Underlay
	String_t* ___Keyword_Underlay_49;
	// System.String TMPro.ShaderUtilities::Keyword_Ratios
	String_t* ___Keyword_Ratios_50;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_SOFT
	String_t* ___Keyword_MASK_SOFT_51;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_HARD
	String_t* ___Keyword_MASK_HARD_52;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_TEX
	String_t* ___Keyword_MASK_TEX_53;
	// System.String TMPro.ShaderUtilities::Keyword_Outline
	String_t* ___Keyword_Outline_54;
	// System.String TMPro.ShaderUtilities::ShaderTag_ZTestMode
	String_t* ___ShaderTag_ZTestMode_55;
	// System.String TMPro.ShaderUtilities::ShaderTag_CullMode
	String_t* ___ShaderTag_CullMode_56;
	// System.Single TMPro.ShaderUtilities::m_clamp
	float ___m_clamp_57;
	// System.Boolean TMPro.ShaderUtilities::isInitialized
	bool ___isInitialized_58;

public:
	inline static int32_t get_offset_of_ID_MainTex_0() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_MainTex_0)); }
	inline int32_t get_ID_MainTex_0() const { return ___ID_MainTex_0; }
	inline int32_t* get_address_of_ID_MainTex_0() { return &___ID_MainTex_0; }
	inline void set_ID_MainTex_0(int32_t value)
	{
		___ID_MainTex_0 = value;
	}

	inline static int32_t get_offset_of_ID_FaceTex_1() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_FaceTex_1)); }
	inline int32_t get_ID_FaceTex_1() const { return ___ID_FaceTex_1; }
	inline int32_t* get_address_of_ID_FaceTex_1() { return &___ID_FaceTex_1; }
	inline void set_ID_FaceTex_1(int32_t value)
	{
		___ID_FaceTex_1 = value;
	}

	inline static int32_t get_offset_of_ID_FaceColor_2() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_FaceColor_2)); }
	inline int32_t get_ID_FaceColor_2() const { return ___ID_FaceColor_2; }
	inline int32_t* get_address_of_ID_FaceColor_2() { return &___ID_FaceColor_2; }
	inline void set_ID_FaceColor_2(int32_t value)
	{
		___ID_FaceColor_2 = value;
	}

	inline static int32_t get_offset_of_ID_FaceDilate_3() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_FaceDilate_3)); }
	inline int32_t get_ID_FaceDilate_3() const { return ___ID_FaceDilate_3; }
	inline int32_t* get_address_of_ID_FaceDilate_3() { return &___ID_FaceDilate_3; }
	inline void set_ID_FaceDilate_3(int32_t value)
	{
		___ID_FaceDilate_3 = value;
	}

	inline static int32_t get_offset_of_ID_Shininess_4() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_Shininess_4)); }
	inline int32_t get_ID_Shininess_4() const { return ___ID_Shininess_4; }
	inline int32_t* get_address_of_ID_Shininess_4() { return &___ID_Shininess_4; }
	inline void set_ID_Shininess_4(int32_t value)
	{
		___ID_Shininess_4 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayColor_5() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UnderlayColor_5)); }
	inline int32_t get_ID_UnderlayColor_5() const { return ___ID_UnderlayColor_5; }
	inline int32_t* get_address_of_ID_UnderlayColor_5() { return &___ID_UnderlayColor_5; }
	inline void set_ID_UnderlayColor_5(int32_t value)
	{
		___ID_UnderlayColor_5 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayOffsetX_6() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UnderlayOffsetX_6)); }
	inline int32_t get_ID_UnderlayOffsetX_6() const { return ___ID_UnderlayOffsetX_6; }
	inline int32_t* get_address_of_ID_UnderlayOffsetX_6() { return &___ID_UnderlayOffsetX_6; }
	inline void set_ID_UnderlayOffsetX_6(int32_t value)
	{
		___ID_UnderlayOffsetX_6 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayOffsetY_7() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UnderlayOffsetY_7)); }
	inline int32_t get_ID_UnderlayOffsetY_7() const { return ___ID_UnderlayOffsetY_7; }
	inline int32_t* get_address_of_ID_UnderlayOffsetY_7() { return &___ID_UnderlayOffsetY_7; }
	inline void set_ID_UnderlayOffsetY_7(int32_t value)
	{
		___ID_UnderlayOffsetY_7 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayDilate_8() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UnderlayDilate_8)); }
	inline int32_t get_ID_UnderlayDilate_8() const { return ___ID_UnderlayDilate_8; }
	inline int32_t* get_address_of_ID_UnderlayDilate_8() { return &___ID_UnderlayDilate_8; }
	inline void set_ID_UnderlayDilate_8(int32_t value)
	{
		___ID_UnderlayDilate_8 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlaySoftness_9() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UnderlaySoftness_9)); }
	inline int32_t get_ID_UnderlaySoftness_9() const { return ___ID_UnderlaySoftness_9; }
	inline int32_t* get_address_of_ID_UnderlaySoftness_9() { return &___ID_UnderlaySoftness_9; }
	inline void set_ID_UnderlaySoftness_9(int32_t value)
	{
		___ID_UnderlaySoftness_9 = value;
	}

	inline static int32_t get_offset_of_ID_WeightNormal_10() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_WeightNormal_10)); }
	inline int32_t get_ID_WeightNormal_10() const { return ___ID_WeightNormal_10; }
	inline int32_t* get_address_of_ID_WeightNormal_10() { return &___ID_WeightNormal_10; }
	inline void set_ID_WeightNormal_10(int32_t value)
	{
		___ID_WeightNormal_10 = value;
	}

	inline static int32_t get_offset_of_ID_WeightBold_11() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_WeightBold_11)); }
	inline int32_t get_ID_WeightBold_11() const { return ___ID_WeightBold_11; }
	inline int32_t* get_address_of_ID_WeightBold_11() { return &___ID_WeightBold_11; }
	inline void set_ID_WeightBold_11(int32_t value)
	{
		___ID_WeightBold_11 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineTex_12() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_OutlineTex_12)); }
	inline int32_t get_ID_OutlineTex_12() const { return ___ID_OutlineTex_12; }
	inline int32_t* get_address_of_ID_OutlineTex_12() { return &___ID_OutlineTex_12; }
	inline void set_ID_OutlineTex_12(int32_t value)
	{
		___ID_OutlineTex_12 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineWidth_13() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_OutlineWidth_13)); }
	inline int32_t get_ID_OutlineWidth_13() const { return ___ID_OutlineWidth_13; }
	inline int32_t* get_address_of_ID_OutlineWidth_13() { return &___ID_OutlineWidth_13; }
	inline void set_ID_OutlineWidth_13(int32_t value)
	{
		___ID_OutlineWidth_13 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineSoftness_14() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_OutlineSoftness_14)); }
	inline int32_t get_ID_OutlineSoftness_14() const { return ___ID_OutlineSoftness_14; }
	inline int32_t* get_address_of_ID_OutlineSoftness_14() { return &___ID_OutlineSoftness_14; }
	inline void set_ID_OutlineSoftness_14(int32_t value)
	{
		___ID_OutlineSoftness_14 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineColor_15() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_OutlineColor_15)); }
	inline int32_t get_ID_OutlineColor_15() const { return ___ID_OutlineColor_15; }
	inline int32_t* get_address_of_ID_OutlineColor_15() { return &___ID_OutlineColor_15; }
	inline void set_ID_OutlineColor_15(int32_t value)
	{
		___ID_OutlineColor_15 = value;
	}

	inline static int32_t get_offset_of_ID_GradientScale_16() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_GradientScale_16)); }
	inline int32_t get_ID_GradientScale_16() const { return ___ID_GradientScale_16; }
	inline int32_t* get_address_of_ID_GradientScale_16() { return &___ID_GradientScale_16; }
	inline void set_ID_GradientScale_16(int32_t value)
	{
		___ID_GradientScale_16 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleX_17() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ScaleX_17)); }
	inline int32_t get_ID_ScaleX_17() const { return ___ID_ScaleX_17; }
	inline int32_t* get_address_of_ID_ScaleX_17() { return &___ID_ScaleX_17; }
	inline void set_ID_ScaleX_17(int32_t value)
	{
		___ID_ScaleX_17 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleY_18() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ScaleY_18)); }
	inline int32_t get_ID_ScaleY_18() const { return ___ID_ScaleY_18; }
	inline int32_t* get_address_of_ID_ScaleY_18() { return &___ID_ScaleY_18; }
	inline void set_ID_ScaleY_18(int32_t value)
	{
		___ID_ScaleY_18 = value;
	}

	inline static int32_t get_offset_of_ID_PerspectiveFilter_19() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_PerspectiveFilter_19)); }
	inline int32_t get_ID_PerspectiveFilter_19() const { return ___ID_PerspectiveFilter_19; }
	inline int32_t* get_address_of_ID_PerspectiveFilter_19() { return &___ID_PerspectiveFilter_19; }
	inline void set_ID_PerspectiveFilter_19(int32_t value)
	{
		___ID_PerspectiveFilter_19 = value;
	}

	inline static int32_t get_offset_of_ID_TextureWidth_20() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_TextureWidth_20)); }
	inline int32_t get_ID_TextureWidth_20() const { return ___ID_TextureWidth_20; }
	inline int32_t* get_address_of_ID_TextureWidth_20() { return &___ID_TextureWidth_20; }
	inline void set_ID_TextureWidth_20(int32_t value)
	{
		___ID_TextureWidth_20 = value;
	}

	inline static int32_t get_offset_of_ID_TextureHeight_21() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_TextureHeight_21)); }
	inline int32_t get_ID_TextureHeight_21() const { return ___ID_TextureHeight_21; }
	inline int32_t* get_address_of_ID_TextureHeight_21() { return &___ID_TextureHeight_21; }
	inline void set_ID_TextureHeight_21(int32_t value)
	{
		___ID_TextureHeight_21 = value;
	}

	inline static int32_t get_offset_of_ID_BevelAmount_22() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_BevelAmount_22)); }
	inline int32_t get_ID_BevelAmount_22() const { return ___ID_BevelAmount_22; }
	inline int32_t* get_address_of_ID_BevelAmount_22() { return &___ID_BevelAmount_22; }
	inline void set_ID_BevelAmount_22(int32_t value)
	{
		___ID_BevelAmount_22 = value;
	}

	inline static int32_t get_offset_of_ID_GlowColor_23() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_GlowColor_23)); }
	inline int32_t get_ID_GlowColor_23() const { return ___ID_GlowColor_23; }
	inline int32_t* get_address_of_ID_GlowColor_23() { return &___ID_GlowColor_23; }
	inline void set_ID_GlowColor_23(int32_t value)
	{
		___ID_GlowColor_23 = value;
	}

	inline static int32_t get_offset_of_ID_GlowOffset_24() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_GlowOffset_24)); }
	inline int32_t get_ID_GlowOffset_24() const { return ___ID_GlowOffset_24; }
	inline int32_t* get_address_of_ID_GlowOffset_24() { return &___ID_GlowOffset_24; }
	inline void set_ID_GlowOffset_24(int32_t value)
	{
		___ID_GlowOffset_24 = value;
	}

	inline static int32_t get_offset_of_ID_GlowPower_25() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_GlowPower_25)); }
	inline int32_t get_ID_GlowPower_25() const { return ___ID_GlowPower_25; }
	inline int32_t* get_address_of_ID_GlowPower_25() { return &___ID_GlowPower_25; }
	inline void set_ID_GlowPower_25(int32_t value)
	{
		___ID_GlowPower_25 = value;
	}

	inline static int32_t get_offset_of_ID_GlowOuter_26() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_GlowOuter_26)); }
	inline int32_t get_ID_GlowOuter_26() const { return ___ID_GlowOuter_26; }
	inline int32_t* get_address_of_ID_GlowOuter_26() { return &___ID_GlowOuter_26; }
	inline void set_ID_GlowOuter_26(int32_t value)
	{
		___ID_GlowOuter_26 = value;
	}

	inline static int32_t get_offset_of_ID_LightAngle_27() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_LightAngle_27)); }
	inline int32_t get_ID_LightAngle_27() const { return ___ID_LightAngle_27; }
	inline int32_t* get_address_of_ID_LightAngle_27() { return &___ID_LightAngle_27; }
	inline void set_ID_LightAngle_27(int32_t value)
	{
		___ID_LightAngle_27 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMap_28() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_EnvMap_28)); }
	inline int32_t get_ID_EnvMap_28() const { return ___ID_EnvMap_28; }
	inline int32_t* get_address_of_ID_EnvMap_28() { return &___ID_EnvMap_28; }
	inline void set_ID_EnvMap_28(int32_t value)
	{
		___ID_EnvMap_28 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMatrix_29() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_EnvMatrix_29)); }
	inline int32_t get_ID_EnvMatrix_29() const { return ___ID_EnvMatrix_29; }
	inline int32_t* get_address_of_ID_EnvMatrix_29() { return &___ID_EnvMatrix_29; }
	inline void set_ID_EnvMatrix_29(int32_t value)
	{
		___ID_EnvMatrix_29 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMatrixRotation_30() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_EnvMatrixRotation_30)); }
	inline int32_t get_ID_EnvMatrixRotation_30() const { return ___ID_EnvMatrixRotation_30; }
	inline int32_t* get_address_of_ID_EnvMatrixRotation_30() { return &___ID_EnvMatrixRotation_30; }
	inline void set_ID_EnvMatrixRotation_30(int32_t value)
	{
		___ID_EnvMatrixRotation_30 = value;
	}

	inline static int32_t get_offset_of_ID_MaskCoord_31() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_MaskCoord_31)); }
	inline int32_t get_ID_MaskCoord_31() const { return ___ID_MaskCoord_31; }
	inline int32_t* get_address_of_ID_MaskCoord_31() { return &___ID_MaskCoord_31; }
	inline void set_ID_MaskCoord_31(int32_t value)
	{
		___ID_MaskCoord_31 = value;
	}

	inline static int32_t get_offset_of_ID_ClipRect_32() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ClipRect_32)); }
	inline int32_t get_ID_ClipRect_32() const { return ___ID_ClipRect_32; }
	inline int32_t* get_address_of_ID_ClipRect_32() { return &___ID_ClipRect_32; }
	inline void set_ID_ClipRect_32(int32_t value)
	{
		___ID_ClipRect_32 = value;
	}

	inline static int32_t get_offset_of_ID_MaskSoftnessX_33() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_MaskSoftnessX_33)); }
	inline int32_t get_ID_MaskSoftnessX_33() const { return ___ID_MaskSoftnessX_33; }
	inline int32_t* get_address_of_ID_MaskSoftnessX_33() { return &___ID_MaskSoftnessX_33; }
	inline void set_ID_MaskSoftnessX_33(int32_t value)
	{
		___ID_MaskSoftnessX_33 = value;
	}

	inline static int32_t get_offset_of_ID_MaskSoftnessY_34() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_MaskSoftnessY_34)); }
	inline int32_t get_ID_MaskSoftnessY_34() const { return ___ID_MaskSoftnessY_34; }
	inline int32_t* get_address_of_ID_MaskSoftnessY_34() { return &___ID_MaskSoftnessY_34; }
	inline void set_ID_MaskSoftnessY_34(int32_t value)
	{
		___ID_MaskSoftnessY_34 = value;
	}

	inline static int32_t get_offset_of_ID_VertexOffsetX_35() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_VertexOffsetX_35)); }
	inline int32_t get_ID_VertexOffsetX_35() const { return ___ID_VertexOffsetX_35; }
	inline int32_t* get_address_of_ID_VertexOffsetX_35() { return &___ID_VertexOffsetX_35; }
	inline void set_ID_VertexOffsetX_35(int32_t value)
	{
		___ID_VertexOffsetX_35 = value;
	}

	inline static int32_t get_offset_of_ID_VertexOffsetY_36() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_VertexOffsetY_36)); }
	inline int32_t get_ID_VertexOffsetY_36() const { return ___ID_VertexOffsetY_36; }
	inline int32_t* get_address_of_ID_VertexOffsetY_36() { return &___ID_VertexOffsetY_36; }
	inline void set_ID_VertexOffsetY_36(int32_t value)
	{
		___ID_VertexOffsetY_36 = value;
	}

	inline static int32_t get_offset_of_ID_UseClipRect_37() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UseClipRect_37)); }
	inline int32_t get_ID_UseClipRect_37() const { return ___ID_UseClipRect_37; }
	inline int32_t* get_address_of_ID_UseClipRect_37() { return &___ID_UseClipRect_37; }
	inline void set_ID_UseClipRect_37(int32_t value)
	{
		___ID_UseClipRect_37 = value;
	}

	inline static int32_t get_offset_of_ID_StencilID_38() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_StencilID_38)); }
	inline int32_t get_ID_StencilID_38() const { return ___ID_StencilID_38; }
	inline int32_t* get_address_of_ID_StencilID_38() { return &___ID_StencilID_38; }
	inline void set_ID_StencilID_38(int32_t value)
	{
		___ID_StencilID_38 = value;
	}

	inline static int32_t get_offset_of_ID_StencilOp_39() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_StencilOp_39)); }
	inline int32_t get_ID_StencilOp_39() const { return ___ID_StencilOp_39; }
	inline int32_t* get_address_of_ID_StencilOp_39() { return &___ID_StencilOp_39; }
	inline void set_ID_StencilOp_39(int32_t value)
	{
		___ID_StencilOp_39 = value;
	}

	inline static int32_t get_offset_of_ID_StencilComp_40() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_StencilComp_40)); }
	inline int32_t get_ID_StencilComp_40() const { return ___ID_StencilComp_40; }
	inline int32_t* get_address_of_ID_StencilComp_40() { return &___ID_StencilComp_40; }
	inline void set_ID_StencilComp_40(int32_t value)
	{
		___ID_StencilComp_40 = value;
	}

	inline static int32_t get_offset_of_ID_StencilReadMask_41() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_StencilReadMask_41)); }
	inline int32_t get_ID_StencilReadMask_41() const { return ___ID_StencilReadMask_41; }
	inline int32_t* get_address_of_ID_StencilReadMask_41() { return &___ID_StencilReadMask_41; }
	inline void set_ID_StencilReadMask_41(int32_t value)
	{
		___ID_StencilReadMask_41 = value;
	}

	inline static int32_t get_offset_of_ID_StencilWriteMask_42() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_StencilWriteMask_42)); }
	inline int32_t get_ID_StencilWriteMask_42() const { return ___ID_StencilWriteMask_42; }
	inline int32_t* get_address_of_ID_StencilWriteMask_42() { return &___ID_StencilWriteMask_42; }
	inline void set_ID_StencilWriteMask_42(int32_t value)
	{
		___ID_StencilWriteMask_42 = value;
	}

	inline static int32_t get_offset_of_ID_ShaderFlags_43() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ShaderFlags_43)); }
	inline int32_t get_ID_ShaderFlags_43() const { return ___ID_ShaderFlags_43; }
	inline int32_t* get_address_of_ID_ShaderFlags_43() { return &___ID_ShaderFlags_43; }
	inline void set_ID_ShaderFlags_43(int32_t value)
	{
		___ID_ShaderFlags_43 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_A_44() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ScaleRatio_A_44)); }
	inline int32_t get_ID_ScaleRatio_A_44() const { return ___ID_ScaleRatio_A_44; }
	inline int32_t* get_address_of_ID_ScaleRatio_A_44() { return &___ID_ScaleRatio_A_44; }
	inline void set_ID_ScaleRatio_A_44(int32_t value)
	{
		___ID_ScaleRatio_A_44 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_B_45() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ScaleRatio_B_45)); }
	inline int32_t get_ID_ScaleRatio_B_45() const { return ___ID_ScaleRatio_B_45; }
	inline int32_t* get_address_of_ID_ScaleRatio_B_45() { return &___ID_ScaleRatio_B_45; }
	inline void set_ID_ScaleRatio_B_45(int32_t value)
	{
		___ID_ScaleRatio_B_45 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_C_46() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ScaleRatio_C_46)); }
	inline int32_t get_ID_ScaleRatio_C_46() const { return ___ID_ScaleRatio_C_46; }
	inline int32_t* get_address_of_ID_ScaleRatio_C_46() { return &___ID_ScaleRatio_C_46; }
	inline void set_ID_ScaleRatio_C_46(int32_t value)
	{
		___ID_ScaleRatio_C_46 = value;
	}

	inline static int32_t get_offset_of_Keyword_Bevel_47() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_Bevel_47)); }
	inline String_t* get_Keyword_Bevel_47() const { return ___Keyword_Bevel_47; }
	inline String_t** get_address_of_Keyword_Bevel_47() { return &___Keyword_Bevel_47; }
	inline void set_Keyword_Bevel_47(String_t* value)
	{
		___Keyword_Bevel_47 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Bevel_47), value);
	}

	inline static int32_t get_offset_of_Keyword_Glow_48() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_Glow_48)); }
	inline String_t* get_Keyword_Glow_48() const { return ___Keyword_Glow_48; }
	inline String_t** get_address_of_Keyword_Glow_48() { return &___Keyword_Glow_48; }
	inline void set_Keyword_Glow_48(String_t* value)
	{
		___Keyword_Glow_48 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Glow_48), value);
	}

	inline static int32_t get_offset_of_Keyword_Underlay_49() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_Underlay_49)); }
	inline String_t* get_Keyword_Underlay_49() const { return ___Keyword_Underlay_49; }
	inline String_t** get_address_of_Keyword_Underlay_49() { return &___Keyword_Underlay_49; }
	inline void set_Keyword_Underlay_49(String_t* value)
	{
		___Keyword_Underlay_49 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Underlay_49), value);
	}

	inline static int32_t get_offset_of_Keyword_Ratios_50() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_Ratios_50)); }
	inline String_t* get_Keyword_Ratios_50() const { return ___Keyword_Ratios_50; }
	inline String_t** get_address_of_Keyword_Ratios_50() { return &___Keyword_Ratios_50; }
	inline void set_Keyword_Ratios_50(String_t* value)
	{
		___Keyword_Ratios_50 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Ratios_50), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_SOFT_51() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_MASK_SOFT_51)); }
	inline String_t* get_Keyword_MASK_SOFT_51() const { return ___Keyword_MASK_SOFT_51; }
	inline String_t** get_address_of_Keyword_MASK_SOFT_51() { return &___Keyword_MASK_SOFT_51; }
	inline void set_Keyword_MASK_SOFT_51(String_t* value)
	{
		___Keyword_MASK_SOFT_51 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_SOFT_51), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_HARD_52() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_MASK_HARD_52)); }
	inline String_t* get_Keyword_MASK_HARD_52() const { return ___Keyword_MASK_HARD_52; }
	inline String_t** get_address_of_Keyword_MASK_HARD_52() { return &___Keyword_MASK_HARD_52; }
	inline void set_Keyword_MASK_HARD_52(String_t* value)
	{
		___Keyword_MASK_HARD_52 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_HARD_52), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_TEX_53() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_MASK_TEX_53)); }
	inline String_t* get_Keyword_MASK_TEX_53() const { return ___Keyword_MASK_TEX_53; }
	inline String_t** get_address_of_Keyword_MASK_TEX_53() { return &___Keyword_MASK_TEX_53; }
	inline void set_Keyword_MASK_TEX_53(String_t* value)
	{
		___Keyword_MASK_TEX_53 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_TEX_53), value);
	}

	inline static int32_t get_offset_of_Keyword_Outline_54() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_Outline_54)); }
	inline String_t* get_Keyword_Outline_54() const { return ___Keyword_Outline_54; }
	inline String_t** get_address_of_Keyword_Outline_54() { return &___Keyword_Outline_54; }
	inline void set_Keyword_Outline_54(String_t* value)
	{
		___Keyword_Outline_54 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Outline_54), value);
	}

	inline static int32_t get_offset_of_ShaderTag_ZTestMode_55() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ShaderTag_ZTestMode_55)); }
	inline String_t* get_ShaderTag_ZTestMode_55() const { return ___ShaderTag_ZTestMode_55; }
	inline String_t** get_address_of_ShaderTag_ZTestMode_55() { return &___ShaderTag_ZTestMode_55; }
	inline void set_ShaderTag_ZTestMode_55(String_t* value)
	{
		___ShaderTag_ZTestMode_55 = value;
		Il2CppCodeGenWriteBarrier((&___ShaderTag_ZTestMode_55), value);
	}

	inline static int32_t get_offset_of_ShaderTag_CullMode_56() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ShaderTag_CullMode_56)); }
	inline String_t* get_ShaderTag_CullMode_56() const { return ___ShaderTag_CullMode_56; }
	inline String_t** get_address_of_ShaderTag_CullMode_56() { return &___ShaderTag_CullMode_56; }
	inline void set_ShaderTag_CullMode_56(String_t* value)
	{
		___ShaderTag_CullMode_56 = value;
		Il2CppCodeGenWriteBarrier((&___ShaderTag_CullMode_56), value);
	}

	inline static int32_t get_offset_of_m_clamp_57() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___m_clamp_57)); }
	inline float get_m_clamp_57() const { return ___m_clamp_57; }
	inline float* get_address_of_m_clamp_57() { return &___m_clamp_57; }
	inline void set_m_clamp_57(float value)
	{
		___m_clamp_57 = value;
	}

	inline static int32_t get_offset_of_isInitialized_58() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___isInitialized_58)); }
	inline bool get_isInitialized_58() const { return ___isInitialized_58; }
	inline bool* get_address_of_isInitialized_58() { return &___isInitialized_58; }
	inline void set_isInitialized_58(bool value)
	{
		___isInitialized_58 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERUTILITIES_T714255158_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T2918531336_H
#define U3CU3EC__DISPLAYCLASS3_0_T2918531336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable/<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t2918531336  : public RuntimeObject
{
public:
	// System.Int32 TMPro.KerningTable/<>c__DisplayClass3_0::left
	int32_t ___left_0;
	// System.Int32 TMPro.KerningTable/<>c__DisplayClass3_0::right
	int32_t ___right_1;

public:
	inline static int32_t get_offset_of_left_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t2918531336, ___left_0)); }
	inline int32_t get_left_0() const { return ___left_0; }
	inline int32_t* get_address_of_left_0() { return &___left_0; }
	inline void set_left_0(int32_t value)
	{
		___left_0 = value;
	}

	inline static int32_t get_offset_of_right_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t2918531336, ___right_1)); }
	inline int32_t get_right_1() const { return ___right_1; }
	inline int32_t* get_address_of_right_1() { return &___right_1; }
	inline void set_right_1(int32_t value)
	{
		___right_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T2918531336_H
#ifndef KERNINGTABLE_T2322366871_H
#define KERNINGTABLE_T2322366871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable
struct  KerningTable_t2322366871  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.KerningPair> TMPro.KerningTable::kerningPairs
	List_1_t3742930331 * ___kerningPairs_0;

public:
	inline static int32_t get_offset_of_kerningPairs_0() { return static_cast<int32_t>(offsetof(KerningTable_t2322366871, ___kerningPairs_0)); }
	inline List_1_t3742930331 * get_kerningPairs_0() const { return ___kerningPairs_0; }
	inline List_1_t3742930331 ** get_address_of_kerningPairs_0() { return &___kerningPairs_0; }
	inline void set_kerningPairs_0(List_1_t3742930331 * value)
	{
		___kerningPairs_0 = value;
		Il2CppCodeGenWriteBarrier((&___kerningPairs_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNINGTABLE_T2322366871_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T2918531329_H
#define U3CU3EC__DISPLAYCLASS4_0_T2918531329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable/<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t2918531329  : public RuntimeObject
{
public:
	// System.Int32 TMPro.KerningTable/<>c__DisplayClass4_0::left
	int32_t ___left_0;
	// System.Int32 TMPro.KerningTable/<>c__DisplayClass4_0::right
	int32_t ___right_1;

public:
	inline static int32_t get_offset_of_left_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t2918531329, ___left_0)); }
	inline int32_t get_left_0() const { return ___left_0; }
	inline int32_t* get_address_of_left_0() { return &___left_0; }
	inline void set_left_0(int32_t value)
	{
		___left_0 = value;
	}

	inline static int32_t get_offset_of_right_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t2918531329, ___right_1)); }
	inline int32_t get_right_1() const { return ___right_1; }
	inline int32_t* get_address_of_right_1() { return &___right_1; }
	inline void set_right_1(int32_t value)
	{
		___right_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T2918531329_H
#ifndef TMP_FONTUTILITIES_T2599150238_H
#define TMP_FONTUTILITIES_T2599150238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontUtilities
struct  TMP_FontUtilities_t2599150238  : public RuntimeObject
{
public:

public:
};

struct TMP_FontUtilities_t2599150238_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Int32> TMPro.TMP_FontUtilities::k_searchedFontAssets
	List_1_t128053199 * ___k_searchedFontAssets_0;

public:
	inline static int32_t get_offset_of_k_searchedFontAssets_0() { return static_cast<int32_t>(offsetof(TMP_FontUtilities_t2599150238_StaticFields, ___k_searchedFontAssets_0)); }
	inline List_1_t128053199 * get_k_searchedFontAssets_0() const { return ___k_searchedFontAssets_0; }
	inline List_1_t128053199 ** get_address_of_k_searchedFontAssets_0() { return &___k_searchedFontAssets_0; }
	inline void set_k_searchedFontAssets_0(List_1_t128053199 * value)
	{
		___k_searchedFontAssets_0 = value;
		Il2CppCodeGenWriteBarrier((&___k_searchedFontAssets_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_FONTUTILITIES_T2599150238_H
#ifndef U3CU3EC_T4077839018_H
#define U3CU3EC_T4077839018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable/<>c
struct  U3CU3Ec_t4077839018  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t4077839018_StaticFields
{
public:
	// TMPro.KerningTable/<>c TMPro.KerningTable/<>c::<>9
	U3CU3Ec_t4077839018 * ___U3CU3E9_0;
	// System.Func`2<TMPro.KerningPair,System.Int32> TMPro.KerningTable/<>c::<>9__6_0
	Func_2_t2554309206 * ___U3CU3E9__6_0_1;
	// System.Func`2<TMPro.KerningPair,System.Int32> TMPro.KerningTable/<>c::<>9__6_1
	Func_2_t2554309206 * ___U3CU3E9__6_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4077839018_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t4077839018 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t4077839018 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t4077839018 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4077839018_StaticFields, ___U3CU3E9__6_0_1)); }
	inline Func_2_t2554309206 * get_U3CU3E9__6_0_1() const { return ___U3CU3E9__6_0_1; }
	inline Func_2_t2554309206 ** get_address_of_U3CU3E9__6_0_1() { return &___U3CU3E9__6_0_1; }
	inline void set_U3CU3E9__6_0_1(Func_2_t2554309206 * value)
	{
		___U3CU3E9__6_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4077839018_StaticFields, ___U3CU3E9__6_1_2)); }
	inline Func_2_t2554309206 * get_U3CU3E9__6_1_2() const { return ___U3CU3E9__6_1_2; }
	inline Func_2_t2554309206 ** get_address_of_U3CU3E9__6_1_2() { return &___U3CU3E9__6_1_2; }
	inline void set_U3CU3E9__6_1_2(Func_2_t2554309206 * value)
	{
		___U3CU3E9__6_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T4077839018_H
#ifndef TRIGGERMETHOD_T582536534_H
#define TRIGGERMETHOD_T582536534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerMethod
struct  TriggerMethod_t582536534  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERMETHOD_T582536534_H
#ifndef CURVE_T1446801592_H
#define CURVE_T1446801592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve
struct  Curve_t1446801592  : public RuntimeObject
{
public:

public:
};

struct Curve_t1446801592_StaticFields
{
public:
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::Linear
	Curve_t1446801592 * ___Linear_0;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::CubicEaseIn
	Curve_t1446801592 * ___CubicEaseIn_1;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::CubicEaseOut
	Curve_t1446801592 * ___CubicEaseOut_2;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::CubicEaseInOut
	Curve_t1446801592 * ___CubicEaseInOut_3;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::QuarticEaseIn
	Curve_t1446801592 * ___QuarticEaseIn_4;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::QuarticEaseOut
	Curve_t1446801592 * ___QuarticEaseOut_5;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::QuarticEaseInOut
	Curve_t1446801592 * ___QuarticEaseInOut_6;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::QuadricEaseIn
	Curve_t1446801592 * ___QuadricEaseIn_7;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::QuadricEaseOut
	Curve_t1446801592 * ___QuadricEaseOut_8;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::QuadricEaseInOut
	Curve_t1446801592 * ___QuadricEaseInOut_9;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::BounceEaseOut
	Curve_t1446801592 * ___BounceEaseOut_10;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::BounceEaseIn
	Curve_t1446801592 * ___BounceEaseIn_11;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::BounceEaseInOut
	Curve_t1446801592 * ___BounceEaseInOut_12;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::BackEaseIn
	Curve_t1446801592 * ___BackEaseIn_13;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::BackEaseOut
	Curve_t1446801592 * ___BackEaseOut_14;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::BackEaseInOut
	Curve_t1446801592 * ___BackEaseInOut_15;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::SineEaseIn
	Curve_t1446801592 * ___SineEaseIn_16;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::SineEaseOut
	Curve_t1446801592 * ___SineEaseOut_17;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::SineEaseInOut
	Curve_t1446801592 * ___SineEaseInOut_18;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::ElasticEaseIn
	Curve_t1446801592 * ___ElasticEaseIn_19;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::ElasticEaseOut
	Curve_t1446801592 * ___ElasticEaseOut_20;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::ElasticEaseInOut
	Curve_t1446801592 * ___ElasticEaseInOut_21;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::CircularEaseIn
	Curve_t1446801592 * ___CircularEaseIn_22;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::CircularEaseOut
	Curve_t1446801592 * ___CircularEaseOut_23;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::CircularEaseInOut
	Curve_t1446801592 * ___CircularEaseInOut_24;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::ExponentialEaseIn
	Curve_t1446801592 * ___ExponentialEaseIn_25;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::ExponentialEaseOut
	Curve_t1446801592 * ___ExponentialEaseOut_26;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::ExponentialEaseInOut
	Curve_t1446801592 * ___ExponentialEaseInOut_27;

public:
	inline static int32_t get_offset_of_Linear_0() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___Linear_0)); }
	inline Curve_t1446801592 * get_Linear_0() const { return ___Linear_0; }
	inline Curve_t1446801592 ** get_address_of_Linear_0() { return &___Linear_0; }
	inline void set_Linear_0(Curve_t1446801592 * value)
	{
		___Linear_0 = value;
		Il2CppCodeGenWriteBarrier((&___Linear_0), value);
	}

	inline static int32_t get_offset_of_CubicEaseIn_1() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___CubicEaseIn_1)); }
	inline Curve_t1446801592 * get_CubicEaseIn_1() const { return ___CubicEaseIn_1; }
	inline Curve_t1446801592 ** get_address_of_CubicEaseIn_1() { return &___CubicEaseIn_1; }
	inline void set_CubicEaseIn_1(Curve_t1446801592 * value)
	{
		___CubicEaseIn_1 = value;
		Il2CppCodeGenWriteBarrier((&___CubicEaseIn_1), value);
	}

	inline static int32_t get_offset_of_CubicEaseOut_2() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___CubicEaseOut_2)); }
	inline Curve_t1446801592 * get_CubicEaseOut_2() const { return ___CubicEaseOut_2; }
	inline Curve_t1446801592 ** get_address_of_CubicEaseOut_2() { return &___CubicEaseOut_2; }
	inline void set_CubicEaseOut_2(Curve_t1446801592 * value)
	{
		___CubicEaseOut_2 = value;
		Il2CppCodeGenWriteBarrier((&___CubicEaseOut_2), value);
	}

	inline static int32_t get_offset_of_CubicEaseInOut_3() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___CubicEaseInOut_3)); }
	inline Curve_t1446801592 * get_CubicEaseInOut_3() const { return ___CubicEaseInOut_3; }
	inline Curve_t1446801592 ** get_address_of_CubicEaseInOut_3() { return &___CubicEaseInOut_3; }
	inline void set_CubicEaseInOut_3(Curve_t1446801592 * value)
	{
		___CubicEaseInOut_3 = value;
		Il2CppCodeGenWriteBarrier((&___CubicEaseInOut_3), value);
	}

	inline static int32_t get_offset_of_QuarticEaseIn_4() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___QuarticEaseIn_4)); }
	inline Curve_t1446801592 * get_QuarticEaseIn_4() const { return ___QuarticEaseIn_4; }
	inline Curve_t1446801592 ** get_address_of_QuarticEaseIn_4() { return &___QuarticEaseIn_4; }
	inline void set_QuarticEaseIn_4(Curve_t1446801592 * value)
	{
		___QuarticEaseIn_4 = value;
		Il2CppCodeGenWriteBarrier((&___QuarticEaseIn_4), value);
	}

	inline static int32_t get_offset_of_QuarticEaseOut_5() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___QuarticEaseOut_5)); }
	inline Curve_t1446801592 * get_QuarticEaseOut_5() const { return ___QuarticEaseOut_5; }
	inline Curve_t1446801592 ** get_address_of_QuarticEaseOut_5() { return &___QuarticEaseOut_5; }
	inline void set_QuarticEaseOut_5(Curve_t1446801592 * value)
	{
		___QuarticEaseOut_5 = value;
		Il2CppCodeGenWriteBarrier((&___QuarticEaseOut_5), value);
	}

	inline static int32_t get_offset_of_QuarticEaseInOut_6() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___QuarticEaseInOut_6)); }
	inline Curve_t1446801592 * get_QuarticEaseInOut_6() const { return ___QuarticEaseInOut_6; }
	inline Curve_t1446801592 ** get_address_of_QuarticEaseInOut_6() { return &___QuarticEaseInOut_6; }
	inline void set_QuarticEaseInOut_6(Curve_t1446801592 * value)
	{
		___QuarticEaseInOut_6 = value;
		Il2CppCodeGenWriteBarrier((&___QuarticEaseInOut_6), value);
	}

	inline static int32_t get_offset_of_QuadricEaseIn_7() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___QuadricEaseIn_7)); }
	inline Curve_t1446801592 * get_QuadricEaseIn_7() const { return ___QuadricEaseIn_7; }
	inline Curve_t1446801592 ** get_address_of_QuadricEaseIn_7() { return &___QuadricEaseIn_7; }
	inline void set_QuadricEaseIn_7(Curve_t1446801592 * value)
	{
		___QuadricEaseIn_7 = value;
		Il2CppCodeGenWriteBarrier((&___QuadricEaseIn_7), value);
	}

	inline static int32_t get_offset_of_QuadricEaseOut_8() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___QuadricEaseOut_8)); }
	inline Curve_t1446801592 * get_QuadricEaseOut_8() const { return ___QuadricEaseOut_8; }
	inline Curve_t1446801592 ** get_address_of_QuadricEaseOut_8() { return &___QuadricEaseOut_8; }
	inline void set_QuadricEaseOut_8(Curve_t1446801592 * value)
	{
		___QuadricEaseOut_8 = value;
		Il2CppCodeGenWriteBarrier((&___QuadricEaseOut_8), value);
	}

	inline static int32_t get_offset_of_QuadricEaseInOut_9() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___QuadricEaseInOut_9)); }
	inline Curve_t1446801592 * get_QuadricEaseInOut_9() const { return ___QuadricEaseInOut_9; }
	inline Curve_t1446801592 ** get_address_of_QuadricEaseInOut_9() { return &___QuadricEaseInOut_9; }
	inline void set_QuadricEaseInOut_9(Curve_t1446801592 * value)
	{
		___QuadricEaseInOut_9 = value;
		Il2CppCodeGenWriteBarrier((&___QuadricEaseInOut_9), value);
	}

	inline static int32_t get_offset_of_BounceEaseOut_10() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___BounceEaseOut_10)); }
	inline Curve_t1446801592 * get_BounceEaseOut_10() const { return ___BounceEaseOut_10; }
	inline Curve_t1446801592 ** get_address_of_BounceEaseOut_10() { return &___BounceEaseOut_10; }
	inline void set_BounceEaseOut_10(Curve_t1446801592 * value)
	{
		___BounceEaseOut_10 = value;
		Il2CppCodeGenWriteBarrier((&___BounceEaseOut_10), value);
	}

	inline static int32_t get_offset_of_BounceEaseIn_11() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___BounceEaseIn_11)); }
	inline Curve_t1446801592 * get_BounceEaseIn_11() const { return ___BounceEaseIn_11; }
	inline Curve_t1446801592 ** get_address_of_BounceEaseIn_11() { return &___BounceEaseIn_11; }
	inline void set_BounceEaseIn_11(Curve_t1446801592 * value)
	{
		___BounceEaseIn_11 = value;
		Il2CppCodeGenWriteBarrier((&___BounceEaseIn_11), value);
	}

	inline static int32_t get_offset_of_BounceEaseInOut_12() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___BounceEaseInOut_12)); }
	inline Curve_t1446801592 * get_BounceEaseInOut_12() const { return ___BounceEaseInOut_12; }
	inline Curve_t1446801592 ** get_address_of_BounceEaseInOut_12() { return &___BounceEaseInOut_12; }
	inline void set_BounceEaseInOut_12(Curve_t1446801592 * value)
	{
		___BounceEaseInOut_12 = value;
		Il2CppCodeGenWriteBarrier((&___BounceEaseInOut_12), value);
	}

	inline static int32_t get_offset_of_BackEaseIn_13() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___BackEaseIn_13)); }
	inline Curve_t1446801592 * get_BackEaseIn_13() const { return ___BackEaseIn_13; }
	inline Curve_t1446801592 ** get_address_of_BackEaseIn_13() { return &___BackEaseIn_13; }
	inline void set_BackEaseIn_13(Curve_t1446801592 * value)
	{
		___BackEaseIn_13 = value;
		Il2CppCodeGenWriteBarrier((&___BackEaseIn_13), value);
	}

	inline static int32_t get_offset_of_BackEaseOut_14() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___BackEaseOut_14)); }
	inline Curve_t1446801592 * get_BackEaseOut_14() const { return ___BackEaseOut_14; }
	inline Curve_t1446801592 ** get_address_of_BackEaseOut_14() { return &___BackEaseOut_14; }
	inline void set_BackEaseOut_14(Curve_t1446801592 * value)
	{
		___BackEaseOut_14 = value;
		Il2CppCodeGenWriteBarrier((&___BackEaseOut_14), value);
	}

	inline static int32_t get_offset_of_BackEaseInOut_15() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___BackEaseInOut_15)); }
	inline Curve_t1446801592 * get_BackEaseInOut_15() const { return ___BackEaseInOut_15; }
	inline Curve_t1446801592 ** get_address_of_BackEaseInOut_15() { return &___BackEaseInOut_15; }
	inline void set_BackEaseInOut_15(Curve_t1446801592 * value)
	{
		___BackEaseInOut_15 = value;
		Il2CppCodeGenWriteBarrier((&___BackEaseInOut_15), value);
	}

	inline static int32_t get_offset_of_SineEaseIn_16() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___SineEaseIn_16)); }
	inline Curve_t1446801592 * get_SineEaseIn_16() const { return ___SineEaseIn_16; }
	inline Curve_t1446801592 ** get_address_of_SineEaseIn_16() { return &___SineEaseIn_16; }
	inline void set_SineEaseIn_16(Curve_t1446801592 * value)
	{
		___SineEaseIn_16 = value;
		Il2CppCodeGenWriteBarrier((&___SineEaseIn_16), value);
	}

	inline static int32_t get_offset_of_SineEaseOut_17() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___SineEaseOut_17)); }
	inline Curve_t1446801592 * get_SineEaseOut_17() const { return ___SineEaseOut_17; }
	inline Curve_t1446801592 ** get_address_of_SineEaseOut_17() { return &___SineEaseOut_17; }
	inline void set_SineEaseOut_17(Curve_t1446801592 * value)
	{
		___SineEaseOut_17 = value;
		Il2CppCodeGenWriteBarrier((&___SineEaseOut_17), value);
	}

	inline static int32_t get_offset_of_SineEaseInOut_18() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___SineEaseInOut_18)); }
	inline Curve_t1446801592 * get_SineEaseInOut_18() const { return ___SineEaseInOut_18; }
	inline Curve_t1446801592 ** get_address_of_SineEaseInOut_18() { return &___SineEaseInOut_18; }
	inline void set_SineEaseInOut_18(Curve_t1446801592 * value)
	{
		___SineEaseInOut_18 = value;
		Il2CppCodeGenWriteBarrier((&___SineEaseInOut_18), value);
	}

	inline static int32_t get_offset_of_ElasticEaseIn_19() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___ElasticEaseIn_19)); }
	inline Curve_t1446801592 * get_ElasticEaseIn_19() const { return ___ElasticEaseIn_19; }
	inline Curve_t1446801592 ** get_address_of_ElasticEaseIn_19() { return &___ElasticEaseIn_19; }
	inline void set_ElasticEaseIn_19(Curve_t1446801592 * value)
	{
		___ElasticEaseIn_19 = value;
		Il2CppCodeGenWriteBarrier((&___ElasticEaseIn_19), value);
	}

	inline static int32_t get_offset_of_ElasticEaseOut_20() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___ElasticEaseOut_20)); }
	inline Curve_t1446801592 * get_ElasticEaseOut_20() const { return ___ElasticEaseOut_20; }
	inline Curve_t1446801592 ** get_address_of_ElasticEaseOut_20() { return &___ElasticEaseOut_20; }
	inline void set_ElasticEaseOut_20(Curve_t1446801592 * value)
	{
		___ElasticEaseOut_20 = value;
		Il2CppCodeGenWriteBarrier((&___ElasticEaseOut_20), value);
	}

	inline static int32_t get_offset_of_ElasticEaseInOut_21() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___ElasticEaseInOut_21)); }
	inline Curve_t1446801592 * get_ElasticEaseInOut_21() const { return ___ElasticEaseInOut_21; }
	inline Curve_t1446801592 ** get_address_of_ElasticEaseInOut_21() { return &___ElasticEaseInOut_21; }
	inline void set_ElasticEaseInOut_21(Curve_t1446801592 * value)
	{
		___ElasticEaseInOut_21 = value;
		Il2CppCodeGenWriteBarrier((&___ElasticEaseInOut_21), value);
	}

	inline static int32_t get_offset_of_CircularEaseIn_22() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___CircularEaseIn_22)); }
	inline Curve_t1446801592 * get_CircularEaseIn_22() const { return ___CircularEaseIn_22; }
	inline Curve_t1446801592 ** get_address_of_CircularEaseIn_22() { return &___CircularEaseIn_22; }
	inline void set_CircularEaseIn_22(Curve_t1446801592 * value)
	{
		___CircularEaseIn_22 = value;
		Il2CppCodeGenWriteBarrier((&___CircularEaseIn_22), value);
	}

	inline static int32_t get_offset_of_CircularEaseOut_23() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___CircularEaseOut_23)); }
	inline Curve_t1446801592 * get_CircularEaseOut_23() const { return ___CircularEaseOut_23; }
	inline Curve_t1446801592 ** get_address_of_CircularEaseOut_23() { return &___CircularEaseOut_23; }
	inline void set_CircularEaseOut_23(Curve_t1446801592 * value)
	{
		___CircularEaseOut_23 = value;
		Il2CppCodeGenWriteBarrier((&___CircularEaseOut_23), value);
	}

	inline static int32_t get_offset_of_CircularEaseInOut_24() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___CircularEaseInOut_24)); }
	inline Curve_t1446801592 * get_CircularEaseInOut_24() const { return ___CircularEaseInOut_24; }
	inline Curve_t1446801592 ** get_address_of_CircularEaseInOut_24() { return &___CircularEaseInOut_24; }
	inline void set_CircularEaseInOut_24(Curve_t1446801592 * value)
	{
		___CircularEaseInOut_24 = value;
		Il2CppCodeGenWriteBarrier((&___CircularEaseInOut_24), value);
	}

	inline static int32_t get_offset_of_ExponentialEaseIn_25() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___ExponentialEaseIn_25)); }
	inline Curve_t1446801592 * get_ExponentialEaseIn_25() const { return ___ExponentialEaseIn_25; }
	inline Curve_t1446801592 ** get_address_of_ExponentialEaseIn_25() { return &___ExponentialEaseIn_25; }
	inline void set_ExponentialEaseIn_25(Curve_t1446801592 * value)
	{
		___ExponentialEaseIn_25 = value;
		Il2CppCodeGenWriteBarrier((&___ExponentialEaseIn_25), value);
	}

	inline static int32_t get_offset_of_ExponentialEaseOut_26() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___ExponentialEaseOut_26)); }
	inline Curve_t1446801592 * get_ExponentialEaseOut_26() const { return ___ExponentialEaseOut_26; }
	inline Curve_t1446801592 ** get_address_of_ExponentialEaseOut_26() { return &___ExponentialEaseOut_26; }
	inline void set_ExponentialEaseOut_26(Curve_t1446801592 * value)
	{
		___ExponentialEaseOut_26 = value;
		Il2CppCodeGenWriteBarrier((&___ExponentialEaseOut_26), value);
	}

	inline static int32_t get_offset_of_ExponentialEaseInOut_27() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___ExponentialEaseInOut_27)); }
	inline Curve_t1446801592 * get_ExponentialEaseInOut_27() const { return ___ExponentialEaseInOut_27; }
	inline Curve_t1446801592 ** get_address_of_ExponentialEaseInOut_27() { return &___ExponentialEaseInOut_27; }
	inline void set_ExponentialEaseInOut_27(Curve_t1446801592 * value)
	{
		___ExponentialEaseInOut_27 = value;
		Il2CppCodeGenWriteBarrier((&___ExponentialEaseInOut_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVE_T1446801592_H
#ifndef STANDARDEVENTPAYLOAD_T1629891255_H
#define STANDARDEVENTPAYLOAD_T1629891255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.StandardEventPayload
struct  StandardEventPayload_t1629891255  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.StandardEventPayload::m_IsEventExpanded
	bool ___m_IsEventExpanded_0;
	// System.String UnityEngine.Analytics.StandardEventPayload::m_StandardEventType
	String_t* ___m_StandardEventType_1;
	// System.Type UnityEngine.Analytics.StandardEventPayload::standardEventType
	Type_t * ___standardEventType_2;
	// UnityEngine.Analytics.AnalyticsEventParamListContainer UnityEngine.Analytics.StandardEventPayload::m_Parameters
	AnalyticsEventParamListContainer_t587083383 * ___m_Parameters_3;
	// System.String UnityEngine.Analytics.StandardEventPayload::m_Name
	String_t* ___m_Name_5;

public:
	inline static int32_t get_offset_of_m_IsEventExpanded_0() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_IsEventExpanded_0)); }
	inline bool get_m_IsEventExpanded_0() const { return ___m_IsEventExpanded_0; }
	inline bool* get_address_of_m_IsEventExpanded_0() { return &___m_IsEventExpanded_0; }
	inline void set_m_IsEventExpanded_0(bool value)
	{
		___m_IsEventExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_StandardEventType_1() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_StandardEventType_1)); }
	inline String_t* get_m_StandardEventType_1() const { return ___m_StandardEventType_1; }
	inline String_t** get_address_of_m_StandardEventType_1() { return &___m_StandardEventType_1; }
	inline void set_m_StandardEventType_1(String_t* value)
	{
		___m_StandardEventType_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_StandardEventType_1), value);
	}

	inline static int32_t get_offset_of_standardEventType_2() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___standardEventType_2)); }
	inline Type_t * get_standardEventType_2() const { return ___standardEventType_2; }
	inline Type_t ** get_address_of_standardEventType_2() { return &___standardEventType_2; }
	inline void set_standardEventType_2(Type_t * value)
	{
		___standardEventType_2 = value;
		Il2CppCodeGenWriteBarrier((&___standardEventType_2), value);
	}

	inline static int32_t get_offset_of_m_Parameters_3() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_Parameters_3)); }
	inline AnalyticsEventParamListContainer_t587083383 * get_m_Parameters_3() const { return ___m_Parameters_3; }
	inline AnalyticsEventParamListContainer_t587083383 ** get_address_of_m_Parameters_3() { return &___m_Parameters_3; }
	inline void set_m_Parameters_3(AnalyticsEventParamListContainer_t587083383 * value)
	{
		___m_Parameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parameters_3), value);
	}

	inline static int32_t get_offset_of_m_Name_5() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_Name_5)); }
	inline String_t* get_m_Name_5() const { return ___m_Name_5; }
	inline String_t** get_address_of_m_Name_5() { return &___m_Name_5; }
	inline void set_m_Name_5(String_t* value)
	{
		___m_Name_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_5), value);
	}
};

struct StandardEventPayload_t1629891255_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.StandardEventPayload::m_EventData
	Dictionary_2_t2865362463 * ___m_EventData_4;

public:
	inline static int32_t get_offset_of_m_EventData_4() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255_StaticFields, ___m_EventData_4)); }
	inline Dictionary_2_t2865362463 * get_m_EventData_4() const { return ___m_EventData_4; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_EventData_4() { return &___m_EventData_4; }
	inline void set_m_EventData_4(Dictionary_2_t2865362463 * value)
	{
		___m_EventData_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventData_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDEVENTPAYLOAD_T1629891255_H
#ifndef TRACKABLEPROPERTY_T3943537984_H
#define TRACKABLEPROPERTY_T3943537984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty
struct  TrackableProperty_t3943537984  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget> UnityEngine.Analytics.TrackableProperty::m_Fields
	List_1_t235857739 * ___m_Fields_1;

public:
	inline static int32_t get_offset_of_m_Fields_1() { return static_cast<int32_t>(offsetof(TrackableProperty_t3943537984, ___m_Fields_1)); }
	inline List_1_t235857739 * get_m_Fields_1() const { return ___m_Fields_1; }
	inline List_1_t235857739 ** get_address_of_m_Fields_1() { return &___m_Fields_1; }
	inline void set_m_Fields_1(List_1_t235857739 * value)
	{
		___m_Fields_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Fields_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEPROPERTY_T3943537984_H
#ifndef TRIGGERLISTCONTAINER_T2032715483_H
#define TRIGGERLISTCONTAINER_T2032715483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerListContainer
struct  TriggerListContainer_t2032715483  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule> UnityEngine.Analytics.TriggerListContainer::m_Rules
	List_1_t3418373063 * ___m_Rules_0;

public:
	inline static int32_t get_offset_of_m_Rules_0() { return static_cast<int32_t>(offsetof(TriggerListContainer_t2032715483, ___m_Rules_0)); }
	inline List_1_t3418373063 * get_m_Rules_0() const { return ___m_Rules_0; }
	inline List_1_t3418373063 ** get_address_of_m_Rules_0() { return &___m_Rules_0; }
	inline void set_m_Rules_0(List_1_t3418373063 * value)
	{
		___m_Rules_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLISTCONTAINER_T2032715483_H
#ifndef FIELDWITHTARGET_T3058750293_H
#define FIELDWITHTARGET_T3058750293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty/FieldWithTarget
struct  FieldWithTarget_t3058750293  : public RuntimeObject
{
public:
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_ParamName
	String_t* ___m_ParamName_0;
	// UnityEngine.Object UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_Target
	Object_t631007953 * ___m_Target_1;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_FieldPath
	String_t* ___m_FieldPath_2;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_TypeString
	String_t* ___m_TypeString_3;
	// System.Boolean UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_DoStatic
	bool ___m_DoStatic_4;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_StaticString
	String_t* ___m_StaticString_5;

public:
	inline static int32_t get_offset_of_m_ParamName_0() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_ParamName_0)); }
	inline String_t* get_m_ParamName_0() const { return ___m_ParamName_0; }
	inline String_t** get_address_of_m_ParamName_0() { return &___m_ParamName_0; }
	inline void set_m_ParamName_0(String_t* value)
	{
		___m_ParamName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParamName_0), value);
	}

	inline static int32_t get_offset_of_m_Target_1() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_Target_1)); }
	inline Object_t631007953 * get_m_Target_1() const { return ___m_Target_1; }
	inline Object_t631007953 ** get_address_of_m_Target_1() { return &___m_Target_1; }
	inline void set_m_Target_1(Object_t631007953 * value)
	{
		___m_Target_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_1), value);
	}

	inline static int32_t get_offset_of_m_FieldPath_2() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_FieldPath_2)); }
	inline String_t* get_m_FieldPath_2() const { return ___m_FieldPath_2; }
	inline String_t** get_address_of_m_FieldPath_2() { return &___m_FieldPath_2; }
	inline void set_m_FieldPath_2(String_t* value)
	{
		___m_FieldPath_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_FieldPath_2), value);
	}

	inline static int32_t get_offset_of_m_TypeString_3() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_TypeString_3)); }
	inline String_t* get_m_TypeString_3() const { return ___m_TypeString_3; }
	inline String_t** get_address_of_m_TypeString_3() { return &___m_TypeString_3; }
	inline void set_m_TypeString_3(String_t* value)
	{
		___m_TypeString_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeString_3), value);
	}

	inline static int32_t get_offset_of_m_DoStatic_4() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_DoStatic_4)); }
	inline bool get_m_DoStatic_4() const { return ___m_DoStatic_4; }
	inline bool* get_address_of_m_DoStatic_4() { return &___m_DoStatic_4; }
	inline void set_m_DoStatic_4(bool value)
	{
		___m_DoStatic_4 = value;
	}

	inline static int32_t get_offset_of_m_StaticString_5() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_StaticString_5)); }
	inline String_t* get_m_StaticString_5() const { return ___m_StaticString_5; }
	inline String_t** get_address_of_m_StaticString_5() { return &___m_StaticString_5; }
	inline void set_m_StaticString_5(String_t* value)
	{
		___m_StaticString_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_StaticString_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDWITHTARGET_T3058750293_H
#ifndef TRACKABLETRIGGER_T621205209_H
#define TRACKABLETRIGGER_T621205209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableTrigger
struct  TrackableTrigger_t621205209  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.Analytics.TrackableTrigger::m_Target
	GameObject_t1113636619 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackableTrigger::m_MethodPath
	String_t* ___m_MethodPath_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_Target_0)); }
	inline GameObject_t1113636619 * get_m_Target_0() const { return ___m_Target_0; }
	inline GameObject_t1113636619 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(GameObject_t1113636619 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_MethodPath_1() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_MethodPath_1)); }
	inline String_t* get_m_MethodPath_1() const { return ___m_MethodPath_1; }
	inline String_t** get_address_of_m_MethodPath_1() { return &___m_MethodPath_1; }
	inline void set_m_MethodPath_1(String_t* value)
	{
		___m_MethodPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_MethodPath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLETRIGGER_T621205209_H
#ifndef ANALYTICSEVENT_T4058973021_H
#define ANALYTICSEVENT_T4058973021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEvent
struct  AnalyticsEvent_t4058973021  : public RuntimeObject
{
public:

public:
};

struct AnalyticsEvent_t4058973021_StaticFields
{
public:
	// System.String UnityEngine.Analytics.AnalyticsEvent::k_SdkVersion
	String_t* ___k_SdkVersion_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.AnalyticsEvent::m_EventData
	Dictionary_2_t2865362463 * ___m_EventData_1;
	// System.Boolean UnityEngine.Analytics.AnalyticsEvent::_debugMode
	bool ____debugMode_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.Analytics.AnalyticsEvent::enumRenameTable
	Dictionary_2_t1632706988 * ___enumRenameTable_3;

public:
	inline static int32_t get_offset_of_k_SdkVersion_0() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ___k_SdkVersion_0)); }
	inline String_t* get_k_SdkVersion_0() const { return ___k_SdkVersion_0; }
	inline String_t** get_address_of_k_SdkVersion_0() { return &___k_SdkVersion_0; }
	inline void set_k_SdkVersion_0(String_t* value)
	{
		___k_SdkVersion_0 = value;
		Il2CppCodeGenWriteBarrier((&___k_SdkVersion_0), value);
	}

	inline static int32_t get_offset_of_m_EventData_1() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ___m_EventData_1)); }
	inline Dictionary_2_t2865362463 * get_m_EventData_1() const { return ___m_EventData_1; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_EventData_1() { return &___m_EventData_1; }
	inline void set_m_EventData_1(Dictionary_2_t2865362463 * value)
	{
		___m_EventData_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventData_1), value);
	}

	inline static int32_t get_offset_of__debugMode_2() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ____debugMode_2)); }
	inline bool get__debugMode_2() const { return ____debugMode_2; }
	inline bool* get_address_of__debugMode_2() { return &____debugMode_2; }
	inline void set__debugMode_2(bool value)
	{
		____debugMode_2 = value;
	}

	inline static int32_t get_offset_of_enumRenameTable_3() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ___enumRenameTable_3)); }
	inline Dictionary_2_t1632706988 * get_enumRenameTable_3() const { return ___enumRenameTable_3; }
	inline Dictionary_2_t1632706988 ** get_address_of_enumRenameTable_3() { return &___enumRenameTable_3; }
	inline void set_enumRenameTable_3(Dictionary_2_t1632706988 * value)
	{
		___enumRenameTable_3 = value;
		Il2CppCodeGenWriteBarrier((&___enumRenameTable_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENT_T4058973021_H
#ifndef U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#define U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0
struct  U3CTimedTriggerU3Ec__Iterator0_t3813435494  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.AnalyticsEventTracker UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$this
	AnalyticsEventTracker_t2285229262 * ___U24this_0;
	// System.Object UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24this_0)); }
	inline AnalyticsEventTracker_t2285229262 * get_U24this_0() const { return ___U24this_0; }
	inline AnalyticsEventTracker_t2285229262 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AnalyticsEventTracker_t2285229262 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#ifndef ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#define ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParamListContainer
struct  AnalyticsEventParamListContainer_t587083383  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.AnalyticsEventParam> UnityEngine.Analytics.AnalyticsEventParamListContainer::m_Parameters
	List_1_t3952196670 * ___m_Parameters_0;

public:
	inline static int32_t get_offset_of_m_Parameters_0() { return static_cast<int32_t>(offsetof(AnalyticsEventParamListContainer_t587083383, ___m_Parameters_0)); }
	inline List_1_t3952196670 * get_m_Parameters_0() const { return ___m_Parameters_0; }
	inline List_1_t3952196670 ** get_address_of_m_Parameters_0() { return &___m_Parameters_0; }
	inline void set_m_Parameters_0(List_1_t3952196670 * value)
	{
		___m_Parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parameters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#ifndef ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#define ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTrackerSettings
struct  AnalyticsEventTrackerSettings_t480422680  : public RuntimeObject
{
public:

public:
};

struct AnalyticsEventTrackerSettings_t480422680_StaticFields
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTrackerSettings::paramCountMax
	int32_t ___paramCountMax_0;
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTrackerSettings::triggerRuleCountMax
	int32_t ___triggerRuleCountMax_1;

public:
	inline static int32_t get_offset_of_paramCountMax_0() { return static_cast<int32_t>(offsetof(AnalyticsEventTrackerSettings_t480422680_StaticFields, ___paramCountMax_0)); }
	inline int32_t get_paramCountMax_0() const { return ___paramCountMax_0; }
	inline int32_t* get_address_of_paramCountMax_0() { return &___paramCountMax_0; }
	inline void set_paramCountMax_0(int32_t value)
	{
		___paramCountMax_0 = value;
	}

	inline static int32_t get_offset_of_triggerRuleCountMax_1() { return static_cast<int32_t>(offsetof(AnalyticsEventTrackerSettings_t480422680_StaticFields, ___triggerRuleCountMax_1)); }
	inline int32_t get_triggerRuleCountMax_1() const { return ___triggerRuleCountMax_1; }
	inline int32_t* get_address_of_triggerRuleCountMax_1() { return &___triggerRuleCountMax_1; }
	inline void set_triggerRuleCountMax_1(int32_t value)
	{
		___triggerRuleCountMax_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#ifndef KERNINGPAIR_T2270855589_H
#define KERNINGPAIR_T2270855589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningPair
struct  KerningPair_t2270855589  : public RuntimeObject
{
public:
	// System.Int32 TMPro.KerningPair::AscII_Left
	int32_t ___AscII_Left_0;
	// System.Int32 TMPro.KerningPair::AscII_Right
	int32_t ___AscII_Right_1;
	// System.Single TMPro.KerningPair::XadvanceOffset
	float ___XadvanceOffset_2;

public:
	inline static int32_t get_offset_of_AscII_Left_0() { return static_cast<int32_t>(offsetof(KerningPair_t2270855589, ___AscII_Left_0)); }
	inline int32_t get_AscII_Left_0() const { return ___AscII_Left_0; }
	inline int32_t* get_address_of_AscII_Left_0() { return &___AscII_Left_0; }
	inline void set_AscII_Left_0(int32_t value)
	{
		___AscII_Left_0 = value;
	}

	inline static int32_t get_offset_of_AscII_Right_1() { return static_cast<int32_t>(offsetof(KerningPair_t2270855589, ___AscII_Right_1)); }
	inline int32_t get_AscII_Right_1() const { return ___AscII_Right_1; }
	inline int32_t* get_address_of_AscII_Right_1() { return &___AscII_Right_1; }
	inline void set_AscII_Right_1(int32_t value)
	{
		___AscII_Right_1 = value;
	}

	inline static int32_t get_offset_of_XadvanceOffset_2() { return static_cast<int32_t>(offsetof(KerningPair_t2270855589, ___XadvanceOffset_2)); }
	inline float get_XadvanceOffset_2() const { return ___XadvanceOffset_2; }
	inline float* get_address_of_XadvanceOffset_2() { return &___XadvanceOffset_2; }
	inline void set_XadvanceOffset_2(float value)
	{
		___XadvanceOffset_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNINGPAIR_T2270855589_H
#ifndef TMPRO_EXTENSIONMETHODS_T1453208966_H
#define TMPRO_EXTENSIONMETHODS_T1453208966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMPro_ExtensionMethods
struct  TMPro_ExtensionMethods_t1453208966  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMPRO_EXTENSIONMETHODS_T1453208966_H
#ifndef TMP_TEXTELEMENT_T129727469_H
#define TMP_TEXTELEMENT_T129727469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElement
struct  TMP_TextElement_t129727469  : public RuntimeObject
{
public:
	// System.Int32 TMPro.TMP_TextElement::id
	int32_t ___id_0;
	// System.Single TMPro.TMP_TextElement::x
	float ___x_1;
	// System.Single TMPro.TMP_TextElement::y
	float ___y_2;
	// System.Single TMPro.TMP_TextElement::width
	float ___width_3;
	// System.Single TMPro.TMP_TextElement::height
	float ___height_4;
	// System.Single TMPro.TMP_TextElement::xOffset
	float ___xOffset_5;
	// System.Single TMPro.TMP_TextElement::yOffset
	float ___yOffset_6;
	// System.Single TMPro.TMP_TextElement::xAdvance
	float ___xAdvance_7;
	// System.Single TMPro.TMP_TextElement::scale
	float ___scale_8;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___width_3)); }
	inline float get_width_3() const { return ___width_3; }
	inline float* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(float value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___height_4)); }
	inline float get_height_4() const { return ___height_4; }
	inline float* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(float value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_xOffset_5() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___xOffset_5)); }
	inline float get_xOffset_5() const { return ___xOffset_5; }
	inline float* get_address_of_xOffset_5() { return &___xOffset_5; }
	inline void set_xOffset_5(float value)
	{
		___xOffset_5 = value;
	}

	inline static int32_t get_offset_of_yOffset_6() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___yOffset_6)); }
	inline float get_yOffset_6() const { return ___yOffset_6; }
	inline float* get_address_of_yOffset_6() { return &___yOffset_6; }
	inline void set_yOffset_6(float value)
	{
		___yOffset_6 = value;
	}

	inline static int32_t get_offset_of_xAdvance_7() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___xAdvance_7)); }
	inline float get_xAdvance_7() const { return ___xAdvance_7; }
	inline float* get_address_of_xAdvance_7() { return &___xAdvance_7; }
	inline void set_xAdvance_7(float value)
	{
		___xAdvance_7 = value;
	}

	inline static int32_t get_offset_of_scale_8() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___scale_8)); }
	inline float get_scale_8() const { return ___scale_8; }
	inline float* get_address_of_scale_8() { return &___scale_8; }
	inline void set_scale_8(float value)
	{
		___scale_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENT_T129727469_H
#ifndef TWEENANIMATION_T1578776730_H
#define TWEENANIMATION_T1578776730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.TweenAnimation
struct  TweenAnimation_t1578776730  : public RuntimeObject
{
public:
	// System.Int32 TweenAnimation.Models.TweenAnimation::frameCount
	int32_t ___frameCount_0;
	// TweenAnimation.Models.Curve TweenAnimation.Models.TweenAnimation::curve
	Curve_t1446801592 * ___curve_1;
	// System.Single TweenAnimation.Models.TweenAnimation::duration
	float ___duration_2;
	// System.Single TweenAnimation.Models.TweenAnimation::delay
	float ___delay_3;
	// System.Single TweenAnimation.Models.TweenAnimation::startTime
	float ___startTime_4;
	// UnityEngine.Transform TweenAnimation.Models.TweenAnimation::transform
	Transform_t3600365921 * ___transform_5;
	// System.Boolean TweenAnimation.Models.TweenAnimation::isFinished
	bool ___isFinished_6;
	// System.Action TweenAnimation.Models.TweenAnimation::finishCallback
	Action_t1264377477 * ___finishCallback_7;
	// System.Action TweenAnimation.Models.TweenAnimation::startCallback
	Action_t1264377477 * ___startCallback_8;

public:
	inline static int32_t get_offset_of_frameCount_0() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___frameCount_0)); }
	inline int32_t get_frameCount_0() const { return ___frameCount_0; }
	inline int32_t* get_address_of_frameCount_0() { return &___frameCount_0; }
	inline void set_frameCount_0(int32_t value)
	{
		___frameCount_0 = value;
	}

	inline static int32_t get_offset_of_curve_1() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___curve_1)); }
	inline Curve_t1446801592 * get_curve_1() const { return ___curve_1; }
	inline Curve_t1446801592 ** get_address_of_curve_1() { return &___curve_1; }
	inline void set_curve_1(Curve_t1446801592 * value)
	{
		___curve_1 = value;
		Il2CppCodeGenWriteBarrier((&___curve_1), value);
	}

	inline static int32_t get_offset_of_duration_2() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___duration_2)); }
	inline float get_duration_2() const { return ___duration_2; }
	inline float* get_address_of_duration_2() { return &___duration_2; }
	inline void set_duration_2(float value)
	{
		___duration_2 = value;
	}

	inline static int32_t get_offset_of_delay_3() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___delay_3)); }
	inline float get_delay_3() const { return ___delay_3; }
	inline float* get_address_of_delay_3() { return &___delay_3; }
	inline void set_delay_3(float value)
	{
		___delay_3 = value;
	}

	inline static int32_t get_offset_of_startTime_4() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___startTime_4)); }
	inline float get_startTime_4() const { return ___startTime_4; }
	inline float* get_address_of_startTime_4() { return &___startTime_4; }
	inline void set_startTime_4(float value)
	{
		___startTime_4 = value;
	}

	inline static int32_t get_offset_of_transform_5() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___transform_5)); }
	inline Transform_t3600365921 * get_transform_5() const { return ___transform_5; }
	inline Transform_t3600365921 ** get_address_of_transform_5() { return &___transform_5; }
	inline void set_transform_5(Transform_t3600365921 * value)
	{
		___transform_5 = value;
		Il2CppCodeGenWriteBarrier((&___transform_5), value);
	}

	inline static int32_t get_offset_of_isFinished_6() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___isFinished_6)); }
	inline bool get_isFinished_6() const { return ___isFinished_6; }
	inline bool* get_address_of_isFinished_6() { return &___isFinished_6; }
	inline void set_isFinished_6(bool value)
	{
		___isFinished_6 = value;
	}

	inline static int32_t get_offset_of_finishCallback_7() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___finishCallback_7)); }
	inline Action_t1264377477 * get_finishCallback_7() const { return ___finishCallback_7; }
	inline Action_t1264377477 ** get_address_of_finishCallback_7() { return &___finishCallback_7; }
	inline void set_finishCallback_7(Action_t1264377477 * value)
	{
		___finishCallback_7 = value;
		Il2CppCodeGenWriteBarrier((&___finishCallback_7), value);
	}

	inline static int32_t get_offset_of_startCallback_8() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___startCallback_8)); }
	inline Action_t1264377477 * get_startCallback_8() const { return ___startCallback_8; }
	inline Action_t1264377477 ** get_address_of_startCallback_8() { return &___startCallback_8; }
	inline void set_startCallback_8(Action_t1264377477 * value)
	{
		___startCallback_8 = value;
		Il2CppCodeGenWriteBarrier((&___startCallback_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENANIMATION_T1578776730_H
#ifndef TMP_UPDATEMANAGER_T4114267509_H
#define TMP_UPDATEMANAGER_T4114267509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_UpdateManager
struct  TMP_UpdateManager_t4114267509  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.TMP_Text> TMPro.TMP_UpdateManager::m_LayoutRebuildQueue
	List_1_t4071693616 * ___m_LayoutRebuildQueue_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateManager::m_LayoutQueueLookup
	Dictionary_2_t1839659084 * ___m_LayoutQueueLookup_2;
	// System.Collections.Generic.List`1<TMPro.TMP_Text> TMPro.TMP_UpdateManager::m_GraphicRebuildQueue
	List_1_t4071693616 * ___m_GraphicRebuildQueue_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateManager::m_GraphicQueueLookup
	Dictionary_2_t1839659084 * ___m_GraphicQueueLookup_4;

public:
	inline static int32_t get_offset_of_m_LayoutRebuildQueue_1() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t4114267509, ___m_LayoutRebuildQueue_1)); }
	inline List_1_t4071693616 * get_m_LayoutRebuildQueue_1() const { return ___m_LayoutRebuildQueue_1; }
	inline List_1_t4071693616 ** get_address_of_m_LayoutRebuildQueue_1() { return &___m_LayoutRebuildQueue_1; }
	inline void set_m_LayoutRebuildQueue_1(List_1_t4071693616 * value)
	{
		___m_LayoutRebuildQueue_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutRebuildQueue_1), value);
	}

	inline static int32_t get_offset_of_m_LayoutQueueLookup_2() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t4114267509, ___m_LayoutQueueLookup_2)); }
	inline Dictionary_2_t1839659084 * get_m_LayoutQueueLookup_2() const { return ___m_LayoutQueueLookup_2; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_LayoutQueueLookup_2() { return &___m_LayoutQueueLookup_2; }
	inline void set_m_LayoutQueueLookup_2(Dictionary_2_t1839659084 * value)
	{
		___m_LayoutQueueLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutQueueLookup_2), value);
	}

	inline static int32_t get_offset_of_m_GraphicRebuildQueue_3() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t4114267509, ___m_GraphicRebuildQueue_3)); }
	inline List_1_t4071693616 * get_m_GraphicRebuildQueue_3() const { return ___m_GraphicRebuildQueue_3; }
	inline List_1_t4071693616 ** get_address_of_m_GraphicRebuildQueue_3() { return &___m_GraphicRebuildQueue_3; }
	inline void set_m_GraphicRebuildQueue_3(List_1_t4071693616 * value)
	{
		___m_GraphicRebuildQueue_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicRebuildQueue_3), value);
	}

	inline static int32_t get_offset_of_m_GraphicQueueLookup_4() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t4114267509, ___m_GraphicQueueLookup_4)); }
	inline Dictionary_2_t1839659084 * get_m_GraphicQueueLookup_4() const { return ___m_GraphicQueueLookup_4; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_GraphicQueueLookup_4() { return &___m_GraphicQueueLookup_4; }
	inline void set_m_GraphicQueueLookup_4(Dictionary_2_t1839659084 * value)
	{
		___m_GraphicQueueLookup_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicQueueLookup_4), value);
	}
};

struct TMP_UpdateManager_t4114267509_StaticFields
{
public:
	// TMPro.TMP_UpdateManager TMPro.TMP_UpdateManager::s_Instance
	TMP_UpdateManager_t4114267509 * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t4114267509_StaticFields, ___s_Instance_0)); }
	inline TMP_UpdateManager_t4114267509 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline TMP_UpdateManager_t4114267509 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(TMP_UpdateManager_t4114267509 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_UPDATEMANAGER_T4114267509_H
#ifndef TMP_UPDATEREGISTRY_T461608481_H
#define TMP_UPDATEREGISTRY_T461608481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_UpdateRegistry
struct  TMP_UpdateRegistry_t461608481  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement> TMPro.TMP_UpdateRegistry::m_LayoutRebuildQueue
	List_1_t3593973608 * ___m_LayoutRebuildQueue_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateRegistry::m_LayoutQueueLookup
	Dictionary_2_t1839659084 * ___m_LayoutQueueLookup_2;
	// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement> TMPro.TMP_UpdateRegistry::m_GraphicRebuildQueue
	List_1_t3593973608 * ___m_GraphicRebuildQueue_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateRegistry::m_GraphicQueueLookup
	Dictionary_2_t1839659084 * ___m_GraphicQueueLookup_4;

public:
	inline static int32_t get_offset_of_m_LayoutRebuildQueue_1() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t461608481, ___m_LayoutRebuildQueue_1)); }
	inline List_1_t3593973608 * get_m_LayoutRebuildQueue_1() const { return ___m_LayoutRebuildQueue_1; }
	inline List_1_t3593973608 ** get_address_of_m_LayoutRebuildQueue_1() { return &___m_LayoutRebuildQueue_1; }
	inline void set_m_LayoutRebuildQueue_1(List_1_t3593973608 * value)
	{
		___m_LayoutRebuildQueue_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutRebuildQueue_1), value);
	}

	inline static int32_t get_offset_of_m_LayoutQueueLookup_2() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t461608481, ___m_LayoutQueueLookup_2)); }
	inline Dictionary_2_t1839659084 * get_m_LayoutQueueLookup_2() const { return ___m_LayoutQueueLookup_2; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_LayoutQueueLookup_2() { return &___m_LayoutQueueLookup_2; }
	inline void set_m_LayoutQueueLookup_2(Dictionary_2_t1839659084 * value)
	{
		___m_LayoutQueueLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutQueueLookup_2), value);
	}

	inline static int32_t get_offset_of_m_GraphicRebuildQueue_3() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t461608481, ___m_GraphicRebuildQueue_3)); }
	inline List_1_t3593973608 * get_m_GraphicRebuildQueue_3() const { return ___m_GraphicRebuildQueue_3; }
	inline List_1_t3593973608 ** get_address_of_m_GraphicRebuildQueue_3() { return &___m_GraphicRebuildQueue_3; }
	inline void set_m_GraphicRebuildQueue_3(List_1_t3593973608 * value)
	{
		___m_GraphicRebuildQueue_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicRebuildQueue_3), value);
	}

	inline static int32_t get_offset_of_m_GraphicQueueLookup_4() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t461608481, ___m_GraphicQueueLookup_4)); }
	inline Dictionary_2_t1839659084 * get_m_GraphicQueueLookup_4() const { return ___m_GraphicQueueLookup_4; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_GraphicQueueLookup_4() { return &___m_GraphicQueueLookup_4; }
	inline void set_m_GraphicQueueLookup_4(Dictionary_2_t1839659084 * value)
	{
		___m_GraphicQueueLookup_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicQueueLookup_4), value);
	}
};

struct TMP_UpdateRegistry_t461608481_StaticFields
{
public:
	// TMPro.TMP_UpdateRegistry TMPro.TMP_UpdateRegistry::s_Instance
	TMP_UpdateRegistry_t461608481 * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t461608481_StaticFields, ___s_Instance_0)); }
	inline TMP_UpdateRegistry_t461608481 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline TMP_UpdateRegistry_t461608481 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(TMP_UpdateRegistry_t461608481 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_UPDATEREGISTRY_T461608481_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef TMP_TEXTUTILITIES_T2105690005_H
#define TMP_TEXTUTILITIES_T2105690005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextUtilities
struct  TMP_TextUtilities_t2105690005  : public RuntimeObject
{
public:

public:
};

struct TMP_TextUtilities_t2105690005_StaticFields
{
public:
	// UnityEngine.Vector3[] TMPro.TMP_TextUtilities::m_rectWorldCorners
	Vector3U5BU5D_t1718750761* ___m_rectWorldCorners_0;

public:
	inline static int32_t get_offset_of_m_rectWorldCorners_0() { return static_cast<int32_t>(offsetof(TMP_TextUtilities_t2105690005_StaticFields, ___m_rectWorldCorners_0)); }
	inline Vector3U5BU5D_t1718750761* get_m_rectWorldCorners_0() const { return ___m_rectWorldCorners_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_rectWorldCorners_0() { return &___m_rectWorldCorners_0; }
	inline void set_m_rectWorldCorners_0(Vector3U5BU5D_t1718750761* value)
	{
		___m_rectWorldCorners_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectWorldCorners_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTUTILITIES_T2105690005_H
#ifndef TMPRO_EVENTMANAGER_T712497257_H
#define TMPRO_EVENTMANAGER_T712497257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMPro_EventManager
struct  TMPro_EventManager_t712497257  : public RuntimeObject
{
public:

public:
};

struct TMPro_EventManager_t712497257_StaticFields
{
public:
	// TMPro.FastAction`2<System.Object,TMPro.Compute_DT_EventArgs> TMPro.TMPro_EventManager::COMPUTE_DT_EVENT
	FastAction_2_t2884460249 * ___COMPUTE_DT_EVENT_0;
	// TMPro.FastAction`2<System.Boolean,UnityEngine.Material> TMPro.TMPro_EventManager::MATERIAL_PROPERTY_EVENT
	FastAction_2_t2206961073 * ___MATERIAL_PROPERTY_EVENT_1;
	// TMPro.FastAction`2<System.Boolean,TMPro.TMP_FontAsset> TMPro.TMPro_EventManager::FONT_PROPERTY_EVENT
	FastAction_2_t2230967576 * ___FONT_PROPERTY_EVENT_2;
	// TMPro.FastAction`2<System.Boolean,UnityEngine.Object> TMPro.TMPro_EventManager::SPRITE_ASSET_PROPERTY_EVENT
	FastAction_2_t2497593903 * ___SPRITE_ASSET_PROPERTY_EVENT_3;
	// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshPro> TMPro.TMPro_EventManager::TEXTMESHPRO_PROPERTY_EVENT
	FastAction_2_t4260179116 * ___TEXTMESHPRO_PROPERTY_EVENT_4;
	// TMPro.FastAction`3<UnityEngine.GameObject,UnityEngine.Material,UnityEngine.Material> TMPro.TMPro_EventManager::DRAG_AND_DROP_MATERIAL_EVENT
	FastAction_3_t58649212 * ___DRAG_AND_DROP_MATERIAL_EVENT_5;
	// TMPro.FastAction`1<System.Boolean> TMPro.TMPro_EventManager::TEXT_STYLE_PROPERTY_EVENT
	FastAction_1_t3758825850 * ___TEXT_STYLE_PROPERTY_EVENT_6;
	// TMPro.FastAction`1<TMPro.TMP_ColorGradient> TMPro.TMPro_EventManager::COLOR_GRADIENT_PROPERTY_EVENT
	FastAction_1_t3044626357 * ___COLOR_GRADIENT_PROPERTY_EVENT_7;
	// TMPro.FastAction TMPro.TMPro_EventManager::TMP_SETTINGS_PROPERTY_EVENT
	FastAction_t3491443480 * ___TMP_SETTINGS_PROPERTY_EVENT_8;
	// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshProUGUI> TMPro.TMPro_EventManager::TEXTMESHPRO_UGUI_PROPERTY_EVENT
	FastAction_2_t2395899227 * ___TEXTMESHPRO_UGUI_PROPERTY_EVENT_9;
	// TMPro.FastAction TMPro.TMPro_EventManager::OnPreRenderObject_Event
	FastAction_t3491443480 * ___OnPreRenderObject_Event_10;
	// TMPro.FastAction`1<UnityEngine.Object> TMPro.TMPro_EventManager::TEXT_CHANGED_EVENT
	FastAction_1_t4292545838 * ___TEXT_CHANGED_EVENT_11;

public:
	inline static int32_t get_offset_of_COMPUTE_DT_EVENT_0() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___COMPUTE_DT_EVENT_0)); }
	inline FastAction_2_t2884460249 * get_COMPUTE_DT_EVENT_0() const { return ___COMPUTE_DT_EVENT_0; }
	inline FastAction_2_t2884460249 ** get_address_of_COMPUTE_DT_EVENT_0() { return &___COMPUTE_DT_EVENT_0; }
	inline void set_COMPUTE_DT_EVENT_0(FastAction_2_t2884460249 * value)
	{
		___COMPUTE_DT_EVENT_0 = value;
		Il2CppCodeGenWriteBarrier((&___COMPUTE_DT_EVENT_0), value);
	}

	inline static int32_t get_offset_of_MATERIAL_PROPERTY_EVENT_1() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___MATERIAL_PROPERTY_EVENT_1)); }
	inline FastAction_2_t2206961073 * get_MATERIAL_PROPERTY_EVENT_1() const { return ___MATERIAL_PROPERTY_EVENT_1; }
	inline FastAction_2_t2206961073 ** get_address_of_MATERIAL_PROPERTY_EVENT_1() { return &___MATERIAL_PROPERTY_EVENT_1; }
	inline void set_MATERIAL_PROPERTY_EVENT_1(FastAction_2_t2206961073 * value)
	{
		___MATERIAL_PROPERTY_EVENT_1 = value;
		Il2CppCodeGenWriteBarrier((&___MATERIAL_PROPERTY_EVENT_1), value);
	}

	inline static int32_t get_offset_of_FONT_PROPERTY_EVENT_2() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___FONT_PROPERTY_EVENT_2)); }
	inline FastAction_2_t2230967576 * get_FONT_PROPERTY_EVENT_2() const { return ___FONT_PROPERTY_EVENT_2; }
	inline FastAction_2_t2230967576 ** get_address_of_FONT_PROPERTY_EVENT_2() { return &___FONT_PROPERTY_EVENT_2; }
	inline void set_FONT_PROPERTY_EVENT_2(FastAction_2_t2230967576 * value)
	{
		___FONT_PROPERTY_EVENT_2 = value;
		Il2CppCodeGenWriteBarrier((&___FONT_PROPERTY_EVENT_2), value);
	}

	inline static int32_t get_offset_of_SPRITE_ASSET_PROPERTY_EVENT_3() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___SPRITE_ASSET_PROPERTY_EVENT_3)); }
	inline FastAction_2_t2497593903 * get_SPRITE_ASSET_PROPERTY_EVENT_3() const { return ___SPRITE_ASSET_PROPERTY_EVENT_3; }
	inline FastAction_2_t2497593903 ** get_address_of_SPRITE_ASSET_PROPERTY_EVENT_3() { return &___SPRITE_ASSET_PROPERTY_EVENT_3; }
	inline void set_SPRITE_ASSET_PROPERTY_EVENT_3(FastAction_2_t2497593903 * value)
	{
		___SPRITE_ASSET_PROPERTY_EVENT_3 = value;
		Il2CppCodeGenWriteBarrier((&___SPRITE_ASSET_PROPERTY_EVENT_3), value);
	}

	inline static int32_t get_offset_of_TEXTMESHPRO_PROPERTY_EVENT_4() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___TEXTMESHPRO_PROPERTY_EVENT_4)); }
	inline FastAction_2_t4260179116 * get_TEXTMESHPRO_PROPERTY_EVENT_4() const { return ___TEXTMESHPRO_PROPERTY_EVENT_4; }
	inline FastAction_2_t4260179116 ** get_address_of_TEXTMESHPRO_PROPERTY_EVENT_4() { return &___TEXTMESHPRO_PROPERTY_EVENT_4; }
	inline void set_TEXTMESHPRO_PROPERTY_EVENT_4(FastAction_2_t4260179116 * value)
	{
		___TEXTMESHPRO_PROPERTY_EVENT_4 = value;
		Il2CppCodeGenWriteBarrier((&___TEXTMESHPRO_PROPERTY_EVENT_4), value);
	}

	inline static int32_t get_offset_of_DRAG_AND_DROP_MATERIAL_EVENT_5() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___DRAG_AND_DROP_MATERIAL_EVENT_5)); }
	inline FastAction_3_t58649212 * get_DRAG_AND_DROP_MATERIAL_EVENT_5() const { return ___DRAG_AND_DROP_MATERIAL_EVENT_5; }
	inline FastAction_3_t58649212 ** get_address_of_DRAG_AND_DROP_MATERIAL_EVENT_5() { return &___DRAG_AND_DROP_MATERIAL_EVENT_5; }
	inline void set_DRAG_AND_DROP_MATERIAL_EVENT_5(FastAction_3_t58649212 * value)
	{
		___DRAG_AND_DROP_MATERIAL_EVENT_5 = value;
		Il2CppCodeGenWriteBarrier((&___DRAG_AND_DROP_MATERIAL_EVENT_5), value);
	}

	inline static int32_t get_offset_of_TEXT_STYLE_PROPERTY_EVENT_6() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___TEXT_STYLE_PROPERTY_EVENT_6)); }
	inline FastAction_1_t3758825850 * get_TEXT_STYLE_PROPERTY_EVENT_6() const { return ___TEXT_STYLE_PROPERTY_EVENT_6; }
	inline FastAction_1_t3758825850 ** get_address_of_TEXT_STYLE_PROPERTY_EVENT_6() { return &___TEXT_STYLE_PROPERTY_EVENT_6; }
	inline void set_TEXT_STYLE_PROPERTY_EVENT_6(FastAction_1_t3758825850 * value)
	{
		___TEXT_STYLE_PROPERTY_EVENT_6 = value;
		Il2CppCodeGenWriteBarrier((&___TEXT_STYLE_PROPERTY_EVENT_6), value);
	}

	inline static int32_t get_offset_of_COLOR_GRADIENT_PROPERTY_EVENT_7() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___COLOR_GRADIENT_PROPERTY_EVENT_7)); }
	inline FastAction_1_t3044626357 * get_COLOR_GRADIENT_PROPERTY_EVENT_7() const { return ___COLOR_GRADIENT_PROPERTY_EVENT_7; }
	inline FastAction_1_t3044626357 ** get_address_of_COLOR_GRADIENT_PROPERTY_EVENT_7() { return &___COLOR_GRADIENT_PROPERTY_EVENT_7; }
	inline void set_COLOR_GRADIENT_PROPERTY_EVENT_7(FastAction_1_t3044626357 * value)
	{
		___COLOR_GRADIENT_PROPERTY_EVENT_7 = value;
		Il2CppCodeGenWriteBarrier((&___COLOR_GRADIENT_PROPERTY_EVENT_7), value);
	}

	inline static int32_t get_offset_of_TMP_SETTINGS_PROPERTY_EVENT_8() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___TMP_SETTINGS_PROPERTY_EVENT_8)); }
	inline FastAction_t3491443480 * get_TMP_SETTINGS_PROPERTY_EVENT_8() const { return ___TMP_SETTINGS_PROPERTY_EVENT_8; }
	inline FastAction_t3491443480 ** get_address_of_TMP_SETTINGS_PROPERTY_EVENT_8() { return &___TMP_SETTINGS_PROPERTY_EVENT_8; }
	inline void set_TMP_SETTINGS_PROPERTY_EVENT_8(FastAction_t3491443480 * value)
	{
		___TMP_SETTINGS_PROPERTY_EVENT_8 = value;
		Il2CppCodeGenWriteBarrier((&___TMP_SETTINGS_PROPERTY_EVENT_8), value);
	}

	inline static int32_t get_offset_of_TEXTMESHPRO_UGUI_PROPERTY_EVENT_9() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___TEXTMESHPRO_UGUI_PROPERTY_EVENT_9)); }
	inline FastAction_2_t2395899227 * get_TEXTMESHPRO_UGUI_PROPERTY_EVENT_9() const { return ___TEXTMESHPRO_UGUI_PROPERTY_EVENT_9; }
	inline FastAction_2_t2395899227 ** get_address_of_TEXTMESHPRO_UGUI_PROPERTY_EVENT_9() { return &___TEXTMESHPRO_UGUI_PROPERTY_EVENT_9; }
	inline void set_TEXTMESHPRO_UGUI_PROPERTY_EVENT_9(FastAction_2_t2395899227 * value)
	{
		___TEXTMESHPRO_UGUI_PROPERTY_EVENT_9 = value;
		Il2CppCodeGenWriteBarrier((&___TEXTMESHPRO_UGUI_PROPERTY_EVENT_9), value);
	}

	inline static int32_t get_offset_of_OnPreRenderObject_Event_10() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___OnPreRenderObject_Event_10)); }
	inline FastAction_t3491443480 * get_OnPreRenderObject_Event_10() const { return ___OnPreRenderObject_Event_10; }
	inline FastAction_t3491443480 ** get_address_of_OnPreRenderObject_Event_10() { return &___OnPreRenderObject_Event_10; }
	inline void set_OnPreRenderObject_Event_10(FastAction_t3491443480 * value)
	{
		___OnPreRenderObject_Event_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnPreRenderObject_Event_10), value);
	}

	inline static int32_t get_offset_of_TEXT_CHANGED_EVENT_11() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___TEXT_CHANGED_EVENT_11)); }
	inline FastAction_1_t4292545838 * get_TEXT_CHANGED_EVENT_11() const { return ___TEXT_CHANGED_EVENT_11; }
	inline FastAction_1_t4292545838 ** get_address_of_TEXT_CHANGED_EVENT_11() { return &___TEXT_CHANGED_EVENT_11; }
	inline void set_TEXT_CHANGED_EVENT_11(FastAction_1_t4292545838 * value)
	{
		___TEXT_CHANGED_EVENT_11 = value;
		Il2CppCodeGenWriteBarrier((&___TEXT_CHANGED_EVENT_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMPRO_EVENTMANAGER_T712497257_H
#ifndef FACEINFO_T2243299176_H
#define FACEINFO_T2243299176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FaceInfo
struct  FaceInfo_t2243299176  : public RuntimeObject
{
public:
	// System.String TMPro.FaceInfo::Name
	String_t* ___Name_0;
	// System.Single TMPro.FaceInfo::PointSize
	float ___PointSize_1;
	// System.Single TMPro.FaceInfo::Scale
	float ___Scale_2;
	// System.Int32 TMPro.FaceInfo::CharacterCount
	int32_t ___CharacterCount_3;
	// System.Single TMPro.FaceInfo::LineHeight
	float ___LineHeight_4;
	// System.Single TMPro.FaceInfo::Baseline
	float ___Baseline_5;
	// System.Single TMPro.FaceInfo::Ascender
	float ___Ascender_6;
	// System.Single TMPro.FaceInfo::CapHeight
	float ___CapHeight_7;
	// System.Single TMPro.FaceInfo::Descender
	float ___Descender_8;
	// System.Single TMPro.FaceInfo::CenterLine
	float ___CenterLine_9;
	// System.Single TMPro.FaceInfo::SuperscriptOffset
	float ___SuperscriptOffset_10;
	// System.Single TMPro.FaceInfo::SubscriptOffset
	float ___SubscriptOffset_11;
	// System.Single TMPro.FaceInfo::SubSize
	float ___SubSize_12;
	// System.Single TMPro.FaceInfo::Underline
	float ___Underline_13;
	// System.Single TMPro.FaceInfo::UnderlineThickness
	float ___UnderlineThickness_14;
	// System.Single TMPro.FaceInfo::strikethrough
	float ___strikethrough_15;
	// System.Single TMPro.FaceInfo::strikethroughThickness
	float ___strikethroughThickness_16;
	// System.Single TMPro.FaceInfo::TabWidth
	float ___TabWidth_17;
	// System.Single TMPro.FaceInfo::Padding
	float ___Padding_18;
	// System.Single TMPro.FaceInfo::AtlasWidth
	float ___AtlasWidth_19;
	// System.Single TMPro.FaceInfo::AtlasHeight
	float ___AtlasHeight_20;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_PointSize_1() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___PointSize_1)); }
	inline float get_PointSize_1() const { return ___PointSize_1; }
	inline float* get_address_of_PointSize_1() { return &___PointSize_1; }
	inline void set_PointSize_1(float value)
	{
		___PointSize_1 = value;
	}

	inline static int32_t get_offset_of_Scale_2() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___Scale_2)); }
	inline float get_Scale_2() const { return ___Scale_2; }
	inline float* get_address_of_Scale_2() { return &___Scale_2; }
	inline void set_Scale_2(float value)
	{
		___Scale_2 = value;
	}

	inline static int32_t get_offset_of_CharacterCount_3() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___CharacterCount_3)); }
	inline int32_t get_CharacterCount_3() const { return ___CharacterCount_3; }
	inline int32_t* get_address_of_CharacterCount_3() { return &___CharacterCount_3; }
	inline void set_CharacterCount_3(int32_t value)
	{
		___CharacterCount_3 = value;
	}

	inline static int32_t get_offset_of_LineHeight_4() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___LineHeight_4)); }
	inline float get_LineHeight_4() const { return ___LineHeight_4; }
	inline float* get_address_of_LineHeight_4() { return &___LineHeight_4; }
	inline void set_LineHeight_4(float value)
	{
		___LineHeight_4 = value;
	}

	inline static int32_t get_offset_of_Baseline_5() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___Baseline_5)); }
	inline float get_Baseline_5() const { return ___Baseline_5; }
	inline float* get_address_of_Baseline_5() { return &___Baseline_5; }
	inline void set_Baseline_5(float value)
	{
		___Baseline_5 = value;
	}

	inline static int32_t get_offset_of_Ascender_6() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___Ascender_6)); }
	inline float get_Ascender_6() const { return ___Ascender_6; }
	inline float* get_address_of_Ascender_6() { return &___Ascender_6; }
	inline void set_Ascender_6(float value)
	{
		___Ascender_6 = value;
	}

	inline static int32_t get_offset_of_CapHeight_7() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___CapHeight_7)); }
	inline float get_CapHeight_7() const { return ___CapHeight_7; }
	inline float* get_address_of_CapHeight_7() { return &___CapHeight_7; }
	inline void set_CapHeight_7(float value)
	{
		___CapHeight_7 = value;
	}

	inline static int32_t get_offset_of_Descender_8() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___Descender_8)); }
	inline float get_Descender_8() const { return ___Descender_8; }
	inline float* get_address_of_Descender_8() { return &___Descender_8; }
	inline void set_Descender_8(float value)
	{
		___Descender_8 = value;
	}

	inline static int32_t get_offset_of_CenterLine_9() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___CenterLine_9)); }
	inline float get_CenterLine_9() const { return ___CenterLine_9; }
	inline float* get_address_of_CenterLine_9() { return &___CenterLine_9; }
	inline void set_CenterLine_9(float value)
	{
		___CenterLine_9 = value;
	}

	inline static int32_t get_offset_of_SuperscriptOffset_10() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___SuperscriptOffset_10)); }
	inline float get_SuperscriptOffset_10() const { return ___SuperscriptOffset_10; }
	inline float* get_address_of_SuperscriptOffset_10() { return &___SuperscriptOffset_10; }
	inline void set_SuperscriptOffset_10(float value)
	{
		___SuperscriptOffset_10 = value;
	}

	inline static int32_t get_offset_of_SubscriptOffset_11() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___SubscriptOffset_11)); }
	inline float get_SubscriptOffset_11() const { return ___SubscriptOffset_11; }
	inline float* get_address_of_SubscriptOffset_11() { return &___SubscriptOffset_11; }
	inline void set_SubscriptOffset_11(float value)
	{
		___SubscriptOffset_11 = value;
	}

	inline static int32_t get_offset_of_SubSize_12() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___SubSize_12)); }
	inline float get_SubSize_12() const { return ___SubSize_12; }
	inline float* get_address_of_SubSize_12() { return &___SubSize_12; }
	inline void set_SubSize_12(float value)
	{
		___SubSize_12 = value;
	}

	inline static int32_t get_offset_of_Underline_13() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___Underline_13)); }
	inline float get_Underline_13() const { return ___Underline_13; }
	inline float* get_address_of_Underline_13() { return &___Underline_13; }
	inline void set_Underline_13(float value)
	{
		___Underline_13 = value;
	}

	inline static int32_t get_offset_of_UnderlineThickness_14() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___UnderlineThickness_14)); }
	inline float get_UnderlineThickness_14() const { return ___UnderlineThickness_14; }
	inline float* get_address_of_UnderlineThickness_14() { return &___UnderlineThickness_14; }
	inline void set_UnderlineThickness_14(float value)
	{
		___UnderlineThickness_14 = value;
	}

	inline static int32_t get_offset_of_strikethrough_15() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___strikethrough_15)); }
	inline float get_strikethrough_15() const { return ___strikethrough_15; }
	inline float* get_address_of_strikethrough_15() { return &___strikethrough_15; }
	inline void set_strikethrough_15(float value)
	{
		___strikethrough_15 = value;
	}

	inline static int32_t get_offset_of_strikethroughThickness_16() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___strikethroughThickness_16)); }
	inline float get_strikethroughThickness_16() const { return ___strikethroughThickness_16; }
	inline float* get_address_of_strikethroughThickness_16() { return &___strikethroughThickness_16; }
	inline void set_strikethroughThickness_16(float value)
	{
		___strikethroughThickness_16 = value;
	}

	inline static int32_t get_offset_of_TabWidth_17() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___TabWidth_17)); }
	inline float get_TabWidth_17() const { return ___TabWidth_17; }
	inline float* get_address_of_TabWidth_17() { return &___TabWidth_17; }
	inline void set_TabWidth_17(float value)
	{
		___TabWidth_17 = value;
	}

	inline static int32_t get_offset_of_Padding_18() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___Padding_18)); }
	inline float get_Padding_18() const { return ___Padding_18; }
	inline float* get_address_of_Padding_18() { return &___Padding_18; }
	inline void set_Padding_18(float value)
	{
		___Padding_18 = value;
	}

	inline static int32_t get_offset_of_AtlasWidth_19() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___AtlasWidth_19)); }
	inline float get_AtlasWidth_19() const { return ___AtlasWidth_19; }
	inline float* get_address_of_AtlasWidth_19() { return &___AtlasWidth_19; }
	inline void set_AtlasWidth_19(float value)
	{
		___AtlasWidth_19 = value;
	}

	inline static int32_t get_offset_of_AtlasHeight_20() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___AtlasHeight_20)); }
	inline float get_AtlasHeight_20() const { return ___AtlasHeight_20; }
	inline float* get_address_of_AtlasHeight_20() { return &___AtlasHeight_20; }
	inline void set_AtlasHeight_20(float value)
	{
		___AtlasHeight_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEINFO_T2243299176_H
#ifndef TMP_XMLTAGSTACK_1_T960921318_H
#define TMP_XMLTAGSTACK_1_T960921318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<System.Single>
struct  TMP_XmlTagStack_1_t960921318 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	SingleU5BU5D_t1444911251* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	float ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___itemStack_0)); }
	inline SingleU5BU5D_t1444911251* get_itemStack_0() const { return ___itemStack_0; }
	inline SingleU5BU5D_t1444911251** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(SingleU5BU5D_t1444911251* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___m_defaultItem_3)); }
	inline float get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline float* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(float value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T960921318_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef COLOR32_T2600501292_H
#define COLOR32_T2600501292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t2600501292 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2600501292_H
#ifndef __STATICARRAYINITTYPESIZEU3D12_T2710994317_H
#define __STATICARRAYINITTYPESIZEU3D12_T2710994317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12
struct  __StaticArrayInitTypeSizeU3D12_t2710994317 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t2710994317__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D12_T2710994317_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef TMP_BASICXMLTAGSTACK_T2962628096_H
#define TMP_BASICXMLTAGSTACK_T2962628096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_BasicXmlTagStack
struct  TMP_BasicXmlTagStack_t2962628096 
{
public:
	// System.Byte TMPro.TMP_BasicXmlTagStack::bold
	uint8_t ___bold_0;
	// System.Byte TMPro.TMP_BasicXmlTagStack::italic
	uint8_t ___italic_1;
	// System.Byte TMPro.TMP_BasicXmlTagStack::underline
	uint8_t ___underline_2;
	// System.Byte TMPro.TMP_BasicXmlTagStack::strikethrough
	uint8_t ___strikethrough_3;
	// System.Byte TMPro.TMP_BasicXmlTagStack::highlight
	uint8_t ___highlight_4;
	// System.Byte TMPro.TMP_BasicXmlTagStack::superscript
	uint8_t ___superscript_5;
	// System.Byte TMPro.TMP_BasicXmlTagStack::subscript
	uint8_t ___subscript_6;
	// System.Byte TMPro.TMP_BasicXmlTagStack::uppercase
	uint8_t ___uppercase_7;
	// System.Byte TMPro.TMP_BasicXmlTagStack::lowercase
	uint8_t ___lowercase_8;
	// System.Byte TMPro.TMP_BasicXmlTagStack::smallcaps
	uint8_t ___smallcaps_9;

public:
	inline static int32_t get_offset_of_bold_0() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___bold_0)); }
	inline uint8_t get_bold_0() const { return ___bold_0; }
	inline uint8_t* get_address_of_bold_0() { return &___bold_0; }
	inline void set_bold_0(uint8_t value)
	{
		___bold_0 = value;
	}

	inline static int32_t get_offset_of_italic_1() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___italic_1)); }
	inline uint8_t get_italic_1() const { return ___italic_1; }
	inline uint8_t* get_address_of_italic_1() { return &___italic_1; }
	inline void set_italic_1(uint8_t value)
	{
		___italic_1 = value;
	}

	inline static int32_t get_offset_of_underline_2() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___underline_2)); }
	inline uint8_t get_underline_2() const { return ___underline_2; }
	inline uint8_t* get_address_of_underline_2() { return &___underline_2; }
	inline void set_underline_2(uint8_t value)
	{
		___underline_2 = value;
	}

	inline static int32_t get_offset_of_strikethrough_3() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___strikethrough_3)); }
	inline uint8_t get_strikethrough_3() const { return ___strikethrough_3; }
	inline uint8_t* get_address_of_strikethrough_3() { return &___strikethrough_3; }
	inline void set_strikethrough_3(uint8_t value)
	{
		___strikethrough_3 = value;
	}

	inline static int32_t get_offset_of_highlight_4() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___highlight_4)); }
	inline uint8_t get_highlight_4() const { return ___highlight_4; }
	inline uint8_t* get_address_of_highlight_4() { return &___highlight_4; }
	inline void set_highlight_4(uint8_t value)
	{
		___highlight_4 = value;
	}

	inline static int32_t get_offset_of_superscript_5() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___superscript_5)); }
	inline uint8_t get_superscript_5() const { return ___superscript_5; }
	inline uint8_t* get_address_of_superscript_5() { return &___superscript_5; }
	inline void set_superscript_5(uint8_t value)
	{
		___superscript_5 = value;
	}

	inline static int32_t get_offset_of_subscript_6() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___subscript_6)); }
	inline uint8_t get_subscript_6() const { return ___subscript_6; }
	inline uint8_t* get_address_of_subscript_6() { return &___subscript_6; }
	inline void set_subscript_6(uint8_t value)
	{
		___subscript_6 = value;
	}

	inline static int32_t get_offset_of_uppercase_7() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___uppercase_7)); }
	inline uint8_t get_uppercase_7() const { return ___uppercase_7; }
	inline uint8_t* get_address_of_uppercase_7() { return &___uppercase_7; }
	inline void set_uppercase_7(uint8_t value)
	{
		___uppercase_7 = value;
	}

	inline static int32_t get_offset_of_lowercase_8() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___lowercase_8)); }
	inline uint8_t get_lowercase_8() const { return ___lowercase_8; }
	inline uint8_t* get_address_of_lowercase_8() { return &___lowercase_8; }
	inline void set_lowercase_8(uint8_t value)
	{
		___lowercase_8 = value;
	}

	inline static int32_t get_offset_of_smallcaps_9() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___smallcaps_9)); }
	inline uint8_t get_smallcaps_9() const { return ___smallcaps_9; }
	inline uint8_t* get_address_of_smallcaps_9() { return &___smallcaps_9; }
	inline void set_smallcaps_9(uint8_t value)
	{
		___smallcaps_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_BASICXMLTAGSTACK_T2962628096_H
#ifndef __STATICARRAYINITTYPESIZEU3D40_T1547998295_H
#define __STATICARRAYINITTYPESIZEU3D40_T1547998295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40
struct  __StaticArrayInitTypeSizeU3D40_t1547998295 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D40_t1547998295__padding[40];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D40_T1547998295_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef LINEARCURVE_T731416727_H
#define LINEARCURVE_T731416727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/LinearCurve
struct  LinearCurve_t731416727  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEARCURVE_T731416727_H
#ifndef MATERIALREFERENCE_T1952344632_H
#define MATERIALREFERENCE_T1952344632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaterialReference
struct  MaterialReference_t1952344632 
{
public:
	// System.Int32 TMPro.MaterialReference::index
	int32_t ___index_0;
	// TMPro.TMP_FontAsset TMPro.MaterialReference::fontAsset
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	// TMPro.TMP_SpriteAsset TMPro.MaterialReference::spriteAsset
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	// UnityEngine.Material TMPro.MaterialReference::material
	Material_t340375123 * ___material_3;
	// System.Boolean TMPro.MaterialReference::isDefaultMaterial
	bool ___isDefaultMaterial_4;
	// System.Boolean TMPro.MaterialReference::isFallbackMaterial
	bool ___isFallbackMaterial_5;
	// UnityEngine.Material TMPro.MaterialReference::fallbackMaterial
	Material_t340375123 * ___fallbackMaterial_6;
	// System.Single TMPro.MaterialReference::padding
	float ___padding_7;
	// System.Int32 TMPro.MaterialReference::referenceCount
	int32_t ___referenceCount_8;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_fontAsset_1() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___fontAsset_1)); }
	inline TMP_FontAsset_t364381626 * get_fontAsset_1() const { return ___fontAsset_1; }
	inline TMP_FontAsset_t364381626 ** get_address_of_fontAsset_1() { return &___fontAsset_1; }
	inline void set_fontAsset_1(TMP_FontAsset_t364381626 * value)
	{
		___fontAsset_1 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_1), value);
	}

	inline static int32_t get_offset_of_spriteAsset_2() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___spriteAsset_2)); }
	inline TMP_SpriteAsset_t484820633 * get_spriteAsset_2() const { return ___spriteAsset_2; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_spriteAsset_2() { return &___spriteAsset_2; }
	inline void set_spriteAsset_2(TMP_SpriteAsset_t484820633 * value)
	{
		___spriteAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_2), value);
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___material_3)); }
	inline Material_t340375123 * get_material_3() const { return ___material_3; }
	inline Material_t340375123 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_t340375123 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_isDefaultMaterial_4() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___isDefaultMaterial_4)); }
	inline bool get_isDefaultMaterial_4() const { return ___isDefaultMaterial_4; }
	inline bool* get_address_of_isDefaultMaterial_4() { return &___isDefaultMaterial_4; }
	inline void set_isDefaultMaterial_4(bool value)
	{
		___isDefaultMaterial_4 = value;
	}

	inline static int32_t get_offset_of_isFallbackMaterial_5() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___isFallbackMaterial_5)); }
	inline bool get_isFallbackMaterial_5() const { return ___isFallbackMaterial_5; }
	inline bool* get_address_of_isFallbackMaterial_5() { return &___isFallbackMaterial_5; }
	inline void set_isFallbackMaterial_5(bool value)
	{
		___isFallbackMaterial_5 = value;
	}

	inline static int32_t get_offset_of_fallbackMaterial_6() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___fallbackMaterial_6)); }
	inline Material_t340375123 * get_fallbackMaterial_6() const { return ___fallbackMaterial_6; }
	inline Material_t340375123 ** get_address_of_fallbackMaterial_6() { return &___fallbackMaterial_6; }
	inline void set_fallbackMaterial_6(Material_t340375123 * value)
	{
		___fallbackMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackMaterial_6), value);
	}

	inline static int32_t get_offset_of_padding_7() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___padding_7)); }
	inline float get_padding_7() const { return ___padding_7; }
	inline float* get_address_of_padding_7() { return &___padding_7; }
	inline void set_padding_7(float value)
	{
		___padding_7 = value;
	}

	inline static int32_t get_offset_of_referenceCount_8() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___referenceCount_8)); }
	inline int32_t get_referenceCount_8() const { return ___referenceCount_8; }
	inline int32_t* get_address_of_referenceCount_8() { return &___referenceCount_8; }
	inline void set_referenceCount_8(int32_t value)
	{
		___referenceCount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.MaterialReference
struct MaterialReference_t1952344632_marshaled_pinvoke
{
	int32_t ___index_0;
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	Material_t340375123 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t340375123 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
// Native definition for COM marshalling of TMPro.MaterialReference
struct MaterialReference_t1952344632_marshaled_com
{
	int32_t ___index_0;
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	Material_t340375123 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t340375123 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
#endif // MATERIALREFERENCE_T1952344632_H
#ifndef CLIP_T774875397_H
#define CLIP_T774875397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Animations.Clip
struct  Clip_t774875397  : public TweenAnimation_t1578776730
{
public:
	// UnityEngine.AnimationClip TweenAnimation.Models.Animations.Clip::animationClip
	AnimationClip_t2318505987 * ___animationClip_9;

public:
	inline static int32_t get_offset_of_animationClip_9() { return static_cast<int32_t>(offsetof(Clip_t774875397, ___animationClip_9)); }
	inline AnimationClip_t2318505987 * get_animationClip_9() const { return ___animationClip_9; }
	inline AnimationClip_t2318505987 ** get_address_of_animationClip_9() { return &___animationClip_9; }
	inline void set_animationClip_9(AnimationClip_t2318505987 * value)
	{
		___animationClip_9 = value;
		Il2CppCodeGenWriteBarrier((&___animationClip_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIP_T774875397_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef CUBICEASEINCURVE_T4146432715_H
#define CUBICEASEINCURVE_T4146432715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/CubicEaseInCurve
struct  CubicEaseInCurve_t4146432715  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBICEASEINCURVE_T4146432715_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef TRACKABLEFIELD_T1772682203_H
#define TRACKABLEFIELD_T1772682203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableField
struct  TrackableField_t1772682203  : public TrackablePropertyBase_t2121532948
{
public:
	// System.String[] UnityEngine.Analytics.TrackableField::m_ValidTypeNames
	StringU5BU5D_t1281789340* ___m_ValidTypeNames_2;
	// System.String UnityEngine.Analytics.TrackableField::m_Type
	String_t* ___m_Type_3;
	// System.String UnityEngine.Analytics.TrackableField::m_EnumType
	String_t* ___m_EnumType_4;

public:
	inline static int32_t get_offset_of_m_ValidTypeNames_2() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_ValidTypeNames_2)); }
	inline StringU5BU5D_t1281789340* get_m_ValidTypeNames_2() const { return ___m_ValidTypeNames_2; }
	inline StringU5BU5D_t1281789340** get_address_of_m_ValidTypeNames_2() { return &___m_ValidTypeNames_2; }
	inline void set_m_ValidTypeNames_2(StringU5BU5D_t1281789340* value)
	{
		___m_ValidTypeNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValidTypeNames_2), value);
	}

	inline static int32_t get_offset_of_m_Type_3() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_Type_3)); }
	inline String_t* get_m_Type_3() const { return ___m_Type_3; }
	inline String_t** get_address_of_m_Type_3() { return &___m_Type_3; }
	inline void set_m_Type_3(String_t* value)
	{
		___m_Type_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type_3), value);
	}

	inline static int32_t get_offset_of_m_EnumType_4() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_EnumType_4)); }
	inline String_t* get_m_EnumType_4() const { return ___m_EnumType_4; }
	inline String_t** get_address_of_m_EnumType_4() { return &___m_EnumType_4; }
	inline void set_m_EnumType_4(String_t* value)
	{
		___m_EnumType_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EnumType_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEFIELD_T1772682203_H
#ifndef CUBICEASEINOUTCURVE_T574754590_H
#define CUBICEASEINOUTCURVE_T574754590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/CubicEaseInOutCurve
struct  CubicEaseInOutCurve_t574754590  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBICEASEINOUTCURVE_T574754590_H
#ifndef CUBICEASEOUTCURVE_T2195481624_H
#define CUBICEASEOUTCURVE_T2195481624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/CubicEaseOutCurve
struct  CubicEaseOutCurve_t2195481624  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBICEASEOUTCURVE_T2195481624_H
#ifndef TMP_WORDINFO_T3331066303_H
#define TMP_WORDINFO_T3331066303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_WordInfo
struct  TMP_WordInfo_t3331066303 
{
public:
	// TMPro.TMP_Text TMPro.TMP_WordInfo::textComponent
	TMP_Text_t2599618874 * ___textComponent_0;
	// System.Int32 TMPro.TMP_WordInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_1;
	// System.Int32 TMPro.TMP_WordInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_2;
	// System.Int32 TMPro.TMP_WordInfo::characterCount
	int32_t ___characterCount_3;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t3331066303, ___textComponent_0)); }
	inline TMP_Text_t2599618874 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t2599618874 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t2599618874 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_firstCharacterIndex_1() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t3331066303, ___firstCharacterIndex_1)); }
	inline int32_t get_firstCharacterIndex_1() const { return ___firstCharacterIndex_1; }
	inline int32_t* get_address_of_firstCharacterIndex_1() { return &___firstCharacterIndex_1; }
	inline void set_firstCharacterIndex_1(int32_t value)
	{
		___firstCharacterIndex_1 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_2() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t3331066303, ___lastCharacterIndex_2)); }
	inline int32_t get_lastCharacterIndex_2() const { return ___lastCharacterIndex_2; }
	inline int32_t* get_address_of_lastCharacterIndex_2() { return &___lastCharacterIndex_2; }
	inline void set_lastCharacterIndex_2(int32_t value)
	{
		___lastCharacterIndex_2 = value;
	}

	inline static int32_t get_offset_of_characterCount_3() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t3331066303, ___characterCount_3)); }
	inline int32_t get_characterCount_3() const { return ___characterCount_3; }
	inline int32_t* get_address_of_characterCount_3() { return &___characterCount_3; }
	inline void set_characterCount_3(int32_t value)
	{
		___characterCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_WordInfo
struct TMP_WordInfo_t3331066303_marshaled_pinvoke
{
	TMP_Text_t2599618874 * ___textComponent_0;
	int32_t ___firstCharacterIndex_1;
	int32_t ___lastCharacterIndex_2;
	int32_t ___characterCount_3;
};
// Native definition for COM marshalling of TMPro.TMP_WordInfo
struct TMP_WordInfo_t3331066303_marshaled_com
{
	TMP_Text_t2599618874 * ___textComponent_0;
	int32_t ___firstCharacterIndex_1;
	int32_t ___lastCharacterIndex_2;
	int32_t ___characterCount_3;
};
#endif // TMP_WORDINFO_T3331066303_H
#ifndef TMP_LINKINFO_T1092083476_H
#define TMP_LINKINFO_T1092083476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LinkInfo
struct  TMP_LinkInfo_t1092083476 
{
public:
	// TMPro.TMP_Text TMPro.TMP_LinkInfo::textComponent
	TMP_Text_t2599618874 * ___textComponent_0;
	// System.Int32 TMPro.TMP_LinkInfo::hashCode
	int32_t ___hashCode_1;
	// System.Int32 TMPro.TMP_LinkInfo::linkIdFirstCharacterIndex
	int32_t ___linkIdFirstCharacterIndex_2;
	// System.Int32 TMPro.TMP_LinkInfo::linkIdLength
	int32_t ___linkIdLength_3;
	// System.Int32 TMPro.TMP_LinkInfo::linkTextfirstCharacterIndex
	int32_t ___linkTextfirstCharacterIndex_4;
	// System.Int32 TMPro.TMP_LinkInfo::linkTextLength
	int32_t ___linkTextLength_5;
	// System.Char[] TMPro.TMP_LinkInfo::linkID
	CharU5BU5D_t3528271667* ___linkID_6;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___textComponent_0)); }
	inline TMP_Text_t2599618874 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t2599618874 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t2599618874 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_hashCode_1() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___hashCode_1)); }
	inline int32_t get_hashCode_1() const { return ___hashCode_1; }
	inline int32_t* get_address_of_hashCode_1() { return &___hashCode_1; }
	inline void set_hashCode_1(int32_t value)
	{
		___hashCode_1 = value;
	}

	inline static int32_t get_offset_of_linkIdFirstCharacterIndex_2() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___linkIdFirstCharacterIndex_2)); }
	inline int32_t get_linkIdFirstCharacterIndex_2() const { return ___linkIdFirstCharacterIndex_2; }
	inline int32_t* get_address_of_linkIdFirstCharacterIndex_2() { return &___linkIdFirstCharacterIndex_2; }
	inline void set_linkIdFirstCharacterIndex_2(int32_t value)
	{
		___linkIdFirstCharacterIndex_2 = value;
	}

	inline static int32_t get_offset_of_linkIdLength_3() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___linkIdLength_3)); }
	inline int32_t get_linkIdLength_3() const { return ___linkIdLength_3; }
	inline int32_t* get_address_of_linkIdLength_3() { return &___linkIdLength_3; }
	inline void set_linkIdLength_3(int32_t value)
	{
		___linkIdLength_3 = value;
	}

	inline static int32_t get_offset_of_linkTextfirstCharacterIndex_4() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___linkTextfirstCharacterIndex_4)); }
	inline int32_t get_linkTextfirstCharacterIndex_4() const { return ___linkTextfirstCharacterIndex_4; }
	inline int32_t* get_address_of_linkTextfirstCharacterIndex_4() { return &___linkTextfirstCharacterIndex_4; }
	inline void set_linkTextfirstCharacterIndex_4(int32_t value)
	{
		___linkTextfirstCharacterIndex_4 = value;
	}

	inline static int32_t get_offset_of_linkTextLength_5() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___linkTextLength_5)); }
	inline int32_t get_linkTextLength_5() const { return ___linkTextLength_5; }
	inline int32_t* get_address_of_linkTextLength_5() { return &___linkTextLength_5; }
	inline void set_linkTextLength_5(int32_t value)
	{
		___linkTextLength_5 = value;
	}

	inline static int32_t get_offset_of_linkID_6() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___linkID_6)); }
	inline CharU5BU5D_t3528271667* get_linkID_6() const { return ___linkID_6; }
	inline CharU5BU5D_t3528271667** get_address_of_linkID_6() { return &___linkID_6; }
	inline void set_linkID_6(CharU5BU5D_t3528271667* value)
	{
		___linkID_6 = value;
		Il2CppCodeGenWriteBarrier((&___linkID_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_LinkInfo
struct TMP_LinkInfo_t1092083476_marshaled_pinvoke
{
	TMP_Text_t2599618874 * ___textComponent_0;
	int32_t ___hashCode_1;
	int32_t ___linkIdFirstCharacterIndex_2;
	int32_t ___linkIdLength_3;
	int32_t ___linkTextfirstCharacterIndex_4;
	int32_t ___linkTextLength_5;
	uint8_t* ___linkID_6;
};
// Native definition for COM marshalling of TMPro.TMP_LinkInfo
struct TMP_LinkInfo_t1092083476_marshaled_com
{
	TMP_Text_t2599618874 * ___textComponent_0;
	int32_t ___hashCode_1;
	int32_t ___linkIdFirstCharacterIndex_2;
	int32_t ___linkIdLength_3;
	int32_t ___linkTextfirstCharacterIndex_4;
	int32_t ___linkTextLength_5;
	uint8_t* ___linkID_6;
};
#endif // TMP_LINKINFO_T1092083476_H
#ifndef TMP_SPRITEINFO_T2726321384_H
#define TMP_SPRITEINFO_T2726321384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteInfo
struct  TMP_SpriteInfo_t2726321384 
{
public:
	// System.Int32 TMPro.TMP_SpriteInfo::spriteIndex
	int32_t ___spriteIndex_0;
	// System.Int32 TMPro.TMP_SpriteInfo::characterIndex
	int32_t ___characterIndex_1;
	// System.Int32 TMPro.TMP_SpriteInfo::vertexIndex
	int32_t ___vertexIndex_2;

public:
	inline static int32_t get_offset_of_spriteIndex_0() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t2726321384, ___spriteIndex_0)); }
	inline int32_t get_spriteIndex_0() const { return ___spriteIndex_0; }
	inline int32_t* get_address_of_spriteIndex_0() { return &___spriteIndex_0; }
	inline void set_spriteIndex_0(int32_t value)
	{
		___spriteIndex_0 = value;
	}

	inline static int32_t get_offset_of_characterIndex_1() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t2726321384, ___characterIndex_1)); }
	inline int32_t get_characterIndex_1() const { return ___characterIndex_1; }
	inline int32_t* get_address_of_characterIndex_1() { return &___characterIndex_1; }
	inline void set_characterIndex_1(int32_t value)
	{
		___characterIndex_1 = value;
	}

	inline static int32_t get_offset_of_vertexIndex_2() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t2726321384, ___vertexIndex_2)); }
	inline int32_t get_vertexIndex_2() const { return ___vertexIndex_2; }
	inline int32_t* get_address_of_vertexIndex_2() { return &___vertexIndex_2; }
	inline void set_vertexIndex_2(int32_t value)
	{
		___vertexIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SPRITEINFO_T2726321384_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef TMP_PAGEINFO_T2608430633_H
#define TMP_PAGEINFO_T2608430633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_PageInfo
struct  TMP_PageInfo_t2608430633 
{
public:
	// System.Int32 TMPro.TMP_PageInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_0;
	// System.Int32 TMPro.TMP_PageInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_1;
	// System.Single TMPro.TMP_PageInfo::ascender
	float ___ascender_2;
	// System.Single TMPro.TMP_PageInfo::baseLine
	float ___baseLine_3;
	// System.Single TMPro.TMP_PageInfo::descender
	float ___descender_4;

public:
	inline static int32_t get_offset_of_firstCharacterIndex_0() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t2608430633, ___firstCharacterIndex_0)); }
	inline int32_t get_firstCharacterIndex_0() const { return ___firstCharacterIndex_0; }
	inline int32_t* get_address_of_firstCharacterIndex_0() { return &___firstCharacterIndex_0; }
	inline void set_firstCharacterIndex_0(int32_t value)
	{
		___firstCharacterIndex_0 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_1() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t2608430633, ___lastCharacterIndex_1)); }
	inline int32_t get_lastCharacterIndex_1() const { return ___lastCharacterIndex_1; }
	inline int32_t* get_address_of_lastCharacterIndex_1() { return &___lastCharacterIndex_1; }
	inline void set_lastCharacterIndex_1(int32_t value)
	{
		___lastCharacterIndex_1 = value;
	}

	inline static int32_t get_offset_of_ascender_2() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t2608430633, ___ascender_2)); }
	inline float get_ascender_2() const { return ___ascender_2; }
	inline float* get_address_of_ascender_2() { return &___ascender_2; }
	inline void set_ascender_2(float value)
	{
		___ascender_2 = value;
	}

	inline static int32_t get_offset_of_baseLine_3() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t2608430633, ___baseLine_3)); }
	inline float get_baseLine_3() const { return ___baseLine_3; }
	inline float* get_address_of_baseLine_3() { return &___baseLine_3; }
	inline void set_baseLine_3(float value)
	{
		___baseLine_3 = value;
	}

	inline static int32_t get_offset_of_descender_4() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t2608430633, ___descender_4)); }
	inline float get_descender_4() const { return ___descender_4; }
	inline float* get_address_of_descender_4() { return &___descender_4; }
	inline void set_descender_4(float value)
	{
		___descender_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_PAGEINFO_T2608430633_H
#ifndef FONTCREATIONSETTING_T628772060_H
#define FONTCREATIONSETTING_T628772060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontCreationSetting
struct  FontCreationSetting_t628772060 
{
public:
	// System.String TMPro.FontCreationSetting::fontSourcePath
	String_t* ___fontSourcePath_0;
	// System.Int32 TMPro.FontCreationSetting::fontSizingMode
	int32_t ___fontSizingMode_1;
	// System.Int32 TMPro.FontCreationSetting::fontSize
	int32_t ___fontSize_2;
	// System.Int32 TMPro.FontCreationSetting::fontPadding
	int32_t ___fontPadding_3;
	// System.Int32 TMPro.FontCreationSetting::fontPackingMode
	int32_t ___fontPackingMode_4;
	// System.Int32 TMPro.FontCreationSetting::fontAtlasWidth
	int32_t ___fontAtlasWidth_5;
	// System.Int32 TMPro.FontCreationSetting::fontAtlasHeight
	int32_t ___fontAtlasHeight_6;
	// System.Int32 TMPro.FontCreationSetting::fontCharacterSet
	int32_t ___fontCharacterSet_7;
	// System.Int32 TMPro.FontCreationSetting::fontStyle
	int32_t ___fontStyle_8;
	// System.Single TMPro.FontCreationSetting::fontStlyeModifier
	float ___fontStlyeModifier_9;
	// System.Int32 TMPro.FontCreationSetting::fontRenderMode
	int32_t ___fontRenderMode_10;
	// System.Boolean TMPro.FontCreationSetting::fontKerning
	bool ___fontKerning_11;

public:
	inline static int32_t get_offset_of_fontSourcePath_0() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontSourcePath_0)); }
	inline String_t* get_fontSourcePath_0() const { return ___fontSourcePath_0; }
	inline String_t** get_address_of_fontSourcePath_0() { return &___fontSourcePath_0; }
	inline void set_fontSourcePath_0(String_t* value)
	{
		___fontSourcePath_0 = value;
		Il2CppCodeGenWriteBarrier((&___fontSourcePath_0), value);
	}

	inline static int32_t get_offset_of_fontSizingMode_1() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontSizingMode_1)); }
	inline int32_t get_fontSizingMode_1() const { return ___fontSizingMode_1; }
	inline int32_t* get_address_of_fontSizingMode_1() { return &___fontSizingMode_1; }
	inline void set_fontSizingMode_1(int32_t value)
	{
		___fontSizingMode_1 = value;
	}

	inline static int32_t get_offset_of_fontSize_2() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontSize_2)); }
	inline int32_t get_fontSize_2() const { return ___fontSize_2; }
	inline int32_t* get_address_of_fontSize_2() { return &___fontSize_2; }
	inline void set_fontSize_2(int32_t value)
	{
		___fontSize_2 = value;
	}

	inline static int32_t get_offset_of_fontPadding_3() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontPadding_3)); }
	inline int32_t get_fontPadding_3() const { return ___fontPadding_3; }
	inline int32_t* get_address_of_fontPadding_3() { return &___fontPadding_3; }
	inline void set_fontPadding_3(int32_t value)
	{
		___fontPadding_3 = value;
	}

	inline static int32_t get_offset_of_fontPackingMode_4() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontPackingMode_4)); }
	inline int32_t get_fontPackingMode_4() const { return ___fontPackingMode_4; }
	inline int32_t* get_address_of_fontPackingMode_4() { return &___fontPackingMode_4; }
	inline void set_fontPackingMode_4(int32_t value)
	{
		___fontPackingMode_4 = value;
	}

	inline static int32_t get_offset_of_fontAtlasWidth_5() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontAtlasWidth_5)); }
	inline int32_t get_fontAtlasWidth_5() const { return ___fontAtlasWidth_5; }
	inline int32_t* get_address_of_fontAtlasWidth_5() { return &___fontAtlasWidth_5; }
	inline void set_fontAtlasWidth_5(int32_t value)
	{
		___fontAtlasWidth_5 = value;
	}

	inline static int32_t get_offset_of_fontAtlasHeight_6() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontAtlasHeight_6)); }
	inline int32_t get_fontAtlasHeight_6() const { return ___fontAtlasHeight_6; }
	inline int32_t* get_address_of_fontAtlasHeight_6() { return &___fontAtlasHeight_6; }
	inline void set_fontAtlasHeight_6(int32_t value)
	{
		___fontAtlasHeight_6 = value;
	}

	inline static int32_t get_offset_of_fontCharacterSet_7() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontCharacterSet_7)); }
	inline int32_t get_fontCharacterSet_7() const { return ___fontCharacterSet_7; }
	inline int32_t* get_address_of_fontCharacterSet_7() { return &___fontCharacterSet_7; }
	inline void set_fontCharacterSet_7(int32_t value)
	{
		___fontCharacterSet_7 = value;
	}

	inline static int32_t get_offset_of_fontStyle_8() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontStyle_8)); }
	inline int32_t get_fontStyle_8() const { return ___fontStyle_8; }
	inline int32_t* get_address_of_fontStyle_8() { return &___fontStyle_8; }
	inline void set_fontStyle_8(int32_t value)
	{
		___fontStyle_8 = value;
	}

	inline static int32_t get_offset_of_fontStlyeModifier_9() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontStlyeModifier_9)); }
	inline float get_fontStlyeModifier_9() const { return ___fontStlyeModifier_9; }
	inline float* get_address_of_fontStlyeModifier_9() { return &___fontStlyeModifier_9; }
	inline void set_fontStlyeModifier_9(float value)
	{
		___fontStlyeModifier_9 = value;
	}

	inline static int32_t get_offset_of_fontRenderMode_10() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontRenderMode_10)); }
	inline int32_t get_fontRenderMode_10() const { return ___fontRenderMode_10; }
	inline int32_t* get_address_of_fontRenderMode_10() { return &___fontRenderMode_10; }
	inline void set_fontRenderMode_10(int32_t value)
	{
		___fontRenderMode_10 = value;
	}

	inline static int32_t get_offset_of_fontKerning_11() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontKerning_11)); }
	inline bool get_fontKerning_11() const { return ___fontKerning_11; }
	inline bool* get_address_of_fontKerning_11() { return &___fontKerning_11; }
	inline void set_fontKerning_11(bool value)
	{
		___fontKerning_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.FontCreationSetting
struct FontCreationSetting_t628772060_marshaled_pinvoke
{
	char* ___fontSourcePath_0;
	int32_t ___fontSizingMode_1;
	int32_t ___fontSize_2;
	int32_t ___fontPadding_3;
	int32_t ___fontPackingMode_4;
	int32_t ___fontAtlasWidth_5;
	int32_t ___fontAtlasHeight_6;
	int32_t ___fontCharacterSet_7;
	int32_t ___fontStyle_8;
	float ___fontStlyeModifier_9;
	int32_t ___fontRenderMode_10;
	int32_t ___fontKerning_11;
};
// Native definition for COM marshalling of TMPro.FontCreationSetting
struct FontCreationSetting_t628772060_marshaled_com
{
	Il2CppChar* ___fontSourcePath_0;
	int32_t ___fontSizingMode_1;
	int32_t ___fontSize_2;
	int32_t ___fontPadding_3;
	int32_t ___fontPackingMode_4;
	int32_t ___fontAtlasWidth_5;
	int32_t ___fontAtlasHeight_6;
	int32_t ___fontCharacterSet_7;
	int32_t ___fontStyle_8;
	float ___fontStlyeModifier_9;
	int32_t ___fontRenderMode_10;
	int32_t ___fontKerning_11;
};
#endif // FONTCREATIONSETTING_T628772060_H
#ifndef KERNINGPAIRKEY_T536493877_H
#define KERNINGPAIRKEY_T536493877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningPairKey
struct  KerningPairKey_t536493877 
{
public:
	// System.Int32 TMPro.KerningPairKey::ascii_Left
	int32_t ___ascii_Left_0;
	// System.Int32 TMPro.KerningPairKey::ascii_Right
	int32_t ___ascii_Right_1;
	// System.Int32 TMPro.KerningPairKey::key
	int32_t ___key_2;

public:
	inline static int32_t get_offset_of_ascii_Left_0() { return static_cast<int32_t>(offsetof(KerningPairKey_t536493877, ___ascii_Left_0)); }
	inline int32_t get_ascii_Left_0() const { return ___ascii_Left_0; }
	inline int32_t* get_address_of_ascii_Left_0() { return &___ascii_Left_0; }
	inline void set_ascii_Left_0(int32_t value)
	{
		___ascii_Left_0 = value;
	}

	inline static int32_t get_offset_of_ascii_Right_1() { return static_cast<int32_t>(offsetof(KerningPairKey_t536493877, ___ascii_Right_1)); }
	inline int32_t get_ascii_Right_1() const { return ___ascii_Right_1; }
	inline int32_t* get_address_of_ascii_Right_1() { return &___ascii_Right_1; }
	inline void set_ascii_Right_1(int32_t value)
	{
		___ascii_Right_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(KerningPairKey_t536493877, ___key_2)); }
	inline int32_t get_key_2() const { return ___key_2; }
	inline int32_t* get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(int32_t value)
	{
		___key_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNINGPAIRKEY_T536493877_H
#ifndef TMP_GLYPH_T581847833_H
#define TMP_GLYPH_T581847833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Glyph
struct  TMP_Glyph_t581847833  : public TMP_TextElement_t129727469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_GLYPH_T581847833_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef SPRITESIZE_T3355290999_H
#define SPRITESIZE_T3355290999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker/SpriteSize
struct  SpriteSize_t3355290999 
{
public:
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteSize::w
	float ___w_0;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteSize::h
	float ___h_1;

public:
	inline static int32_t get_offset_of_w_0() { return static_cast<int32_t>(offsetof(SpriteSize_t3355290999, ___w_0)); }
	inline float get_w_0() const { return ___w_0; }
	inline float* get_address_of_w_0() { return &___w_0; }
	inline void set_w_0(float value)
	{
		___w_0 = value;
	}

	inline static int32_t get_offset_of_h_1() { return static_cast<int32_t>(offsetof(SpriteSize_t3355290999, ___h_1)); }
	inline float get_h_1() const { return ___h_1; }
	inline float* get_address_of_h_1() { return &___h_1; }
	inline void set_h_1(float value)
	{
		___h_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITESIZE_T3355290999_H
#ifndef TMP_XMLTAGSTACK_1_T2514600297_H
#define TMP_XMLTAGSTACK_1_T2514600297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<System.Int32>
struct  TMP_XmlTagStack_1_t2514600297 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	Int32U5BU5D_t385246372* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	int32_t ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___itemStack_0)); }
	inline Int32U5BU5D_t385246372* get_itemStack_0() const { return ___itemStack_0; }
	inline Int32U5BU5D_t385246372** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Int32U5BU5D_t385246372* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___m_defaultItem_3)); }
	inline int32_t get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline int32_t* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(int32_t value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T2514600297_H
#ifndef TAGATTRIBUTE_T688278634_H
#define TAGATTRIBUTE_T688278634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagAttribute
struct  TagAttribute_t688278634 
{
public:
	// System.Int32 TMPro.TagAttribute::startIndex
	int32_t ___startIndex_0;
	// System.Int32 TMPro.TagAttribute::length
	int32_t ___length_1;
	// System.Int32 TMPro.TagAttribute::hashCode
	int32_t ___hashCode_2;

public:
	inline static int32_t get_offset_of_startIndex_0() { return static_cast<int32_t>(offsetof(TagAttribute_t688278634, ___startIndex_0)); }
	inline int32_t get_startIndex_0() const { return ___startIndex_0; }
	inline int32_t* get_address_of_startIndex_0() { return &___startIndex_0; }
	inline void set_startIndex_0(int32_t value)
	{
		___startIndex_0 = value;
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(TagAttribute_t688278634, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_hashCode_2() { return static_cast<int32_t>(offsetof(TagAttribute_t688278634, ___hashCode_2)); }
	inline int32_t get_hashCode_2() const { return ___hashCode_2; }
	inline int32_t* get_address_of_hashCode_2() { return &___hashCode_2; }
	inline void set_hashCode_2(int32_t value)
	{
		___hashCode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGATTRIBUTE_T688278634_H
#ifndef SPRITEFRAME_T3912389194_H
#define SPRITEFRAME_T3912389194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame
struct  SpriteFrame_t3912389194 
{
public:
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame::x
	float ___x_0;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame::y
	float ___y_1;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame::w
	float ___w_2;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame::h
	float ___h_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SpriteFrame_t3912389194, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(SpriteFrame_t3912389194, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_w_2() { return static_cast<int32_t>(offsetof(SpriteFrame_t3912389194, ___w_2)); }
	inline float get_w_2() const { return ___w_2; }
	inline float* get_address_of_w_2() { return &___w_2; }
	inline void set_w_2(float value)
	{
		___w_2 = value;
	}

	inline static int32_t get_offset_of_h_3() { return static_cast<int32_t>(offsetof(SpriteFrame_t3912389194, ___h_3)); }
	inline float get_h_3() const { return ___h_3; }
	inline float* get_address_of_h_3() { return &___h_3; }
	inline void set_h_3(float value)
	{
		___h_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEFRAME_T3912389194_H
#ifndef POSITION3D_T1995122365_H
#define POSITION3D_T1995122365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Animations.Position3D
struct  Position3D_t1995122365  : public TweenAnimation_t1578776730
{
public:
	// UnityEngine.Vector3 TweenAnimation.Models.Animations.Position3D::v0
	Vector3_t3722313464  ___v0_9;
	// UnityEngine.Vector3 TweenAnimation.Models.Animations.Position3D::v1
	Vector3_t3722313464  ___v1_10;
	// System.Func`1<UnityEngine.Vector3> TweenAnimation.Models.Animations.Position3D::v1Func
	Func_1_t3152060111 * ___v1Func_11;
	// System.Boolean TweenAnimation.Models.Animations.Position3D::useGlobalValues
	bool ___useGlobalValues_12;

public:
	inline static int32_t get_offset_of_v0_9() { return static_cast<int32_t>(offsetof(Position3D_t1995122365, ___v0_9)); }
	inline Vector3_t3722313464  get_v0_9() const { return ___v0_9; }
	inline Vector3_t3722313464 * get_address_of_v0_9() { return &___v0_9; }
	inline void set_v0_9(Vector3_t3722313464  value)
	{
		___v0_9 = value;
	}

	inline static int32_t get_offset_of_v1_10() { return static_cast<int32_t>(offsetof(Position3D_t1995122365, ___v1_10)); }
	inline Vector3_t3722313464  get_v1_10() const { return ___v1_10; }
	inline Vector3_t3722313464 * get_address_of_v1_10() { return &___v1_10; }
	inline void set_v1_10(Vector3_t3722313464  value)
	{
		___v1_10 = value;
	}

	inline static int32_t get_offset_of_v1Func_11() { return static_cast<int32_t>(offsetof(Position3D_t1995122365, ___v1Func_11)); }
	inline Func_1_t3152060111 * get_v1Func_11() const { return ___v1Func_11; }
	inline Func_1_t3152060111 ** get_address_of_v1Func_11() { return &___v1Func_11; }
	inline void set_v1Func_11(Func_1_t3152060111 * value)
	{
		___v1Func_11 = value;
		Il2CppCodeGenWriteBarrier((&___v1Func_11), value);
	}

	inline static int32_t get_offset_of_useGlobalValues_12() { return static_cast<int32_t>(offsetof(Position3D_t1995122365, ___useGlobalValues_12)); }
	inline bool get_useGlobalValues_12() const { return ___useGlobalValues_12; }
	inline bool* get_address_of_useGlobalValues_12() { return &___useGlobalValues_12; }
	inline void set_useGlobalValues_12(bool value)
	{
		___useGlobalValues_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITION3D_T1995122365_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef ROTATION_T2635690123_H
#define ROTATION_T2635690123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Animations.Rotation
struct  Rotation_t2635690123  : public TweenAnimation_t1578776730
{
public:
	// UnityEngine.Quaternion TweenAnimation.Models.Animations.Rotation::q0
	Quaternion_t2301928331  ___q0_9;
	// UnityEngine.Quaternion TweenAnimation.Models.Animations.Rotation::q1
	Quaternion_t2301928331  ___q1_10;

public:
	inline static int32_t get_offset_of_q0_9() { return static_cast<int32_t>(offsetof(Rotation_t2635690123, ___q0_9)); }
	inline Quaternion_t2301928331  get_q0_9() const { return ___q0_9; }
	inline Quaternion_t2301928331 * get_address_of_q0_9() { return &___q0_9; }
	inline void set_q0_9(Quaternion_t2301928331  value)
	{
		___q0_9 = value;
	}

	inline static int32_t get_offset_of_q1_10() { return static_cast<int32_t>(offsetof(Rotation_t2635690123, ___q1_10)); }
	inline Quaternion_t2301928331  get_q1_10() const { return ___q1_10; }
	inline Quaternion_t2301928331 * get_address_of_q1_10() { return &___q1_10; }
	inline void set_q1_10(Quaternion_t2301928331  value)
	{
		___q1_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATION_T2635690123_H
#ifndef SCALE2D_T1489313937_H
#define SCALE2D_T1489313937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Animations.Scale2D
struct  Scale2D_t1489313937  : public TweenAnimation_t1578776730
{
public:
	// UnityEngine.Vector3 TweenAnimation.Models.Animations.Scale2D::v0
	Vector3_t3722313464  ___v0_9;
	// UnityEngine.Vector3 TweenAnimation.Models.Animations.Scale2D::v1
	Vector3_t3722313464  ___v1_10;

public:
	inline static int32_t get_offset_of_v0_9() { return static_cast<int32_t>(offsetof(Scale2D_t1489313937, ___v0_9)); }
	inline Vector3_t3722313464  get_v0_9() const { return ___v0_9; }
	inline Vector3_t3722313464 * get_address_of_v0_9() { return &___v0_9; }
	inline void set_v0_9(Vector3_t3722313464  value)
	{
		___v0_9 = value;
	}

	inline static int32_t get_offset_of_v1_10() { return static_cast<int32_t>(offsetof(Scale2D_t1489313937, ___v1_10)); }
	inline Vector3_t3722313464  get_v1_10() const { return ___v1_10; }
	inline Vector3_t3722313464 * get_address_of_v1_10() { return &___v1_10; }
	inline void set_v1_10(Vector3_t3722313464  value)
	{
		___v1_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALE2D_T1489313937_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef TEXTRENDERFLAGS_T2418684345_H
#define TEXTRENDERFLAGS_T2418684345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextRenderFlags
struct  TextRenderFlags_t2418684345 
{
public:
	// System.Int32 TMPro.TextRenderFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextRenderFlags_t2418684345, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTRENDERFLAGS_T2418684345_H
#ifndef MASKINGTYPES_T3687969768_H
#define MASKINGTYPES_T3687969768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaskingTypes
struct  MaskingTypes_t3687969768 
{
public:
	// System.Int32 TMPro.MaskingTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MaskingTypes_t3687969768, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKINGTYPES_T3687969768_H
#ifndef POSITION2D_T1995187901_H
#define POSITION2D_T1995187901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Animations.Position2D
struct  Position2D_t1995187901  : public TweenAnimation_t1578776730
{
public:
	// UnityEngine.Vector3 TweenAnimation.Models.Animations.Position2D::v0
	Vector3_t3722313464  ___v0_9;
	// UnityEngine.Vector3 TweenAnimation.Models.Animations.Position2D::v1
	Vector3_t3722313464  ___v1_10;
	// System.Boolean TweenAnimation.Models.Animations.Position2D::useGlobalValues
	bool ___useGlobalValues_11;

public:
	inline static int32_t get_offset_of_v0_9() { return static_cast<int32_t>(offsetof(Position2D_t1995187901, ___v0_9)); }
	inline Vector3_t3722313464  get_v0_9() const { return ___v0_9; }
	inline Vector3_t3722313464 * get_address_of_v0_9() { return &___v0_9; }
	inline void set_v0_9(Vector3_t3722313464  value)
	{
		___v0_9 = value;
	}

	inline static int32_t get_offset_of_v1_10() { return static_cast<int32_t>(offsetof(Position2D_t1995187901, ___v1_10)); }
	inline Vector3_t3722313464  get_v1_10() const { return ___v1_10; }
	inline Vector3_t3722313464 * get_address_of_v1_10() { return &___v1_10; }
	inline void set_v1_10(Vector3_t3722313464  value)
	{
		___v1_10 = value;
	}

	inline static int32_t get_offset_of_useGlobalValues_11() { return static_cast<int32_t>(offsetof(Position2D_t1995187901, ___useGlobalValues_11)); }
	inline bool get_useGlobalValues_11() const { return ___useGlobalValues_11; }
	inline bool* get_address_of_useGlobalValues_11() { return &___useGlobalValues_11; }
	inline void set_useGlobalValues_11(bool value)
	{
		___useGlobalValues_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITION2D_T1995187901_H
#ifndef IMAGECOLOR_T691475714_H
#define IMAGECOLOR_T691475714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Animations.ImageColor
struct  ImageColor_t691475714  : public TweenAnimation_t1578776730
{
public:
	// UnityEngine.Color TweenAnimation.Models.Animations.ImageColor::from
	Color_t2555686324  ___from_9;
	// UnityEngine.Color TweenAnimation.Models.Animations.ImageColor::to
	Color_t2555686324  ___to_10;
	// UnityEngine.UI.Image TweenAnimation.Models.Animations.ImageColor::image
	Image_t2670269651 * ___image_11;

public:
	inline static int32_t get_offset_of_from_9() { return static_cast<int32_t>(offsetof(ImageColor_t691475714, ___from_9)); }
	inline Color_t2555686324  get_from_9() const { return ___from_9; }
	inline Color_t2555686324 * get_address_of_from_9() { return &___from_9; }
	inline void set_from_9(Color_t2555686324  value)
	{
		___from_9 = value;
	}

	inline static int32_t get_offset_of_to_10() { return static_cast<int32_t>(offsetof(ImageColor_t691475714, ___to_10)); }
	inline Color_t2555686324  get_to_10() const { return ___to_10; }
	inline Color_t2555686324 * get_address_of_to_10() { return &___to_10; }
	inline void set_to_10(Color_t2555686324  value)
	{
		___to_10 = value;
	}

	inline static int32_t get_offset_of_image_11() { return static_cast<int32_t>(offsetof(ImageColor_t691475714, ___image_11)); }
	inline Image_t2670269651 * get_image_11() const { return ___image_11; }
	inline Image_t2670269651 ** get_address_of_image_11() { return &___image_11; }
	inline void set_image_11(Image_t2670269651 * value)
	{
		___image_11 = value;
		Il2CppCodeGenWriteBarrier((&___image_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGECOLOR_T691475714_H
#ifndef TEXTOVERFLOWMODES_T1430035314_H
#define TEXTOVERFLOWMODES_T1430035314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextOverflowModes
struct  TextOverflowModes_t1430035314 
{
public:
	// System.Int32 TMPro.TextOverflowModes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextOverflowModes_t1430035314, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTOVERFLOWMODES_T1430035314_H
#ifndef TEXTALIGNMENTOPTIONS_T4036791236_H
#define TEXTALIGNMENTOPTIONS_T4036791236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextAlignmentOptions
struct  TextAlignmentOptions_t4036791236 
{
public:
	// System.Int32 TMPro.TextAlignmentOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAlignmentOptions_t4036791236, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTALIGNMENTOPTIONS_T4036791236_H
#ifndef BOUNDS_T2266837910_H
#define BOUNDS_T2266837910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t2266837910 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t3722313464  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t3722313464  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Center_0)); }
	inline Vector3_t3722313464  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t3722313464 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t3722313464  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Extents_1)); }
	inline Vector3_t3722313464  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t3722313464 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t3722313464  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T2266837910_H
#ifndef VERTEXSORTINGORDER_T2659893934_H
#define VERTEXSORTINGORDER_T2659893934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexSortingOrder
struct  VertexSortingOrder_t2659893934 
{
public:
	// System.Int32 TMPro.VertexSortingOrder::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VertexSortingOrder_t2659893934, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXSORTINGORDER_T2659893934_H
#ifndef TEXTUREMAPPINGOPTIONS_T270963663_H
#define TEXTUREMAPPINGOPTIONS_T270963663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextureMappingOptions
struct  TextureMappingOptions_t270963663 
{
public:
	// System.Int32 TMPro.TextureMappingOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureMappingOptions_t270963663, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREMAPPINGOPTIONS_T270963663_H
#ifndef TMP_XMLTAGSTACK_1_T1515999176_H
#define TMP_XMLTAGSTACK_1_T1515999176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference>
struct  TMP_XmlTagStack_1_t1515999176 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	MaterialReferenceU5BU5D_t648826345* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	MaterialReference_t1952344632  ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___itemStack_0)); }
	inline MaterialReferenceU5BU5D_t648826345* get_itemStack_0() const { return ___itemStack_0; }
	inline MaterialReferenceU5BU5D_t648826345** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(MaterialReferenceU5BU5D_t648826345* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___m_defaultItem_3)); }
	inline MaterialReference_t1952344632  get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline MaterialReference_t1952344632 * get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(MaterialReference_t1952344632  value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T1515999176_H
#ifndef SHAKE2D_T1926097863_H
#define SHAKE2D_T1926097863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Animations.Shake2D
struct  Shake2D_t1926097863  : public TweenAnimation_t1578776730
{
public:
	// UnityEngine.Vector3 TweenAnimation.Models.Animations.Shake2D::magnitude
	Vector3_t3722313464  ___magnitude_9;
	// System.Int32 TweenAnimation.Models.Animations.Shake2D::times
	int32_t ___times_10;

public:
	inline static int32_t get_offset_of_magnitude_9() { return static_cast<int32_t>(offsetof(Shake2D_t1926097863, ___magnitude_9)); }
	inline Vector3_t3722313464  get_magnitude_9() const { return ___magnitude_9; }
	inline Vector3_t3722313464 * get_address_of_magnitude_9() { return &___magnitude_9; }
	inline void set_magnitude_9(Vector3_t3722313464  value)
	{
		___magnitude_9 = value;
	}

	inline static int32_t get_offset_of_times_10() { return static_cast<int32_t>(offsetof(Shake2D_t1926097863, ___times_10)); }
	inline int32_t get_times_10() const { return ___times_10; }
	inline int32_t* get_address_of_times_10() { return &___times_10; }
	inline void set_times_10(int32_t value)
	{
		___times_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAKE2D_T1926097863_H
#ifndef FONTSTYLES_T3828945032_H
#define FONTSTYLES_T3828945032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontStyles
struct  FontStyles_t3828945032 
{
public:
	// System.Int32 TMPro.FontStyles::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FontStyles_t3828945032, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLES_T3828945032_H
#ifndef TMP_XMLTAGSTACK_1_T2164155836_H
#define TMP_XMLTAGSTACK_1_T2164155836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32>
struct  TMP_XmlTagStack_1_t2164155836 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	Color32U5BU5D_t3850468773* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	Color32_t2600501292  ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___itemStack_0)); }
	inline Color32U5BU5D_t3850468773* get_itemStack_0() const { return ___itemStack_0; }
	inline Color32U5BU5D_t3850468773** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Color32U5BU5D_t3850468773* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___m_defaultItem_3)); }
	inline Color32_t2600501292  get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline Color32_t2600501292 * get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(Color32_t2600501292  value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T2164155836_H
#ifndef TMP_TEXTELEMENTTYPE_T1276645592_H
#define TMP_TEXTELEMENTTYPE_T1276645592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElementType
struct  TMP_TextElementType_t1276645592 
{
public:
	// System.Int32 TMPro.TMP_TextElementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TMP_TextElementType_t1276645592, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENTTYPE_T1276645592_H
#ifndef SPRITEDATA_T3048397587_H
#define SPRITEDATA_T3048397587_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker/SpriteData
struct  SpriteData_t3048397587 
{
public:
	// System.String TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::filename
	String_t* ___filename_0;
	// TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::frame
	SpriteFrame_t3912389194  ___frame_1;
	// System.Boolean TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::rotated
	bool ___rotated_2;
	// System.Boolean TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::trimmed
	bool ___trimmed_3;
	// TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::spriteSourceSize
	SpriteFrame_t3912389194  ___spriteSourceSize_4;
	// TMPro.SpriteAssetUtilities.TexturePacker/SpriteSize TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::sourceSize
	SpriteSize_t3355290999  ___sourceSize_5;
	// UnityEngine.Vector2 TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::pivot
	Vector2_t2156229523  ___pivot_6;

public:
	inline static int32_t get_offset_of_filename_0() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___filename_0)); }
	inline String_t* get_filename_0() const { return ___filename_0; }
	inline String_t** get_address_of_filename_0() { return &___filename_0; }
	inline void set_filename_0(String_t* value)
	{
		___filename_0 = value;
		Il2CppCodeGenWriteBarrier((&___filename_0), value);
	}

	inline static int32_t get_offset_of_frame_1() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___frame_1)); }
	inline SpriteFrame_t3912389194  get_frame_1() const { return ___frame_1; }
	inline SpriteFrame_t3912389194 * get_address_of_frame_1() { return &___frame_1; }
	inline void set_frame_1(SpriteFrame_t3912389194  value)
	{
		___frame_1 = value;
	}

	inline static int32_t get_offset_of_rotated_2() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___rotated_2)); }
	inline bool get_rotated_2() const { return ___rotated_2; }
	inline bool* get_address_of_rotated_2() { return &___rotated_2; }
	inline void set_rotated_2(bool value)
	{
		___rotated_2 = value;
	}

	inline static int32_t get_offset_of_trimmed_3() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___trimmed_3)); }
	inline bool get_trimmed_3() const { return ___trimmed_3; }
	inline bool* get_address_of_trimmed_3() { return &___trimmed_3; }
	inline void set_trimmed_3(bool value)
	{
		___trimmed_3 = value;
	}

	inline static int32_t get_offset_of_spriteSourceSize_4() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___spriteSourceSize_4)); }
	inline SpriteFrame_t3912389194  get_spriteSourceSize_4() const { return ___spriteSourceSize_4; }
	inline SpriteFrame_t3912389194 * get_address_of_spriteSourceSize_4() { return &___spriteSourceSize_4; }
	inline void set_spriteSourceSize_4(SpriteFrame_t3912389194  value)
	{
		___spriteSourceSize_4 = value;
	}

	inline static int32_t get_offset_of_sourceSize_5() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___sourceSize_5)); }
	inline SpriteSize_t3355290999  get_sourceSize_5() const { return ___sourceSize_5; }
	inline SpriteSize_t3355290999 * get_address_of_sourceSize_5() { return &___sourceSize_5; }
	inline void set_sourceSize_5(SpriteSize_t3355290999  value)
	{
		___sourceSize_5 = value;
	}

	inline static int32_t get_offset_of_pivot_6() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___pivot_6)); }
	inline Vector2_t2156229523  get_pivot_6() const { return ___pivot_6; }
	inline Vector2_t2156229523 * get_address_of_pivot_6() { return &___pivot_6; }
	inline void set_pivot_6(Vector2_t2156229523  value)
	{
		___pivot_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.SpriteAssetUtilities.TexturePacker/SpriteData
struct SpriteData_t3048397587_marshaled_pinvoke
{
	char* ___filename_0;
	SpriteFrame_t3912389194  ___frame_1;
	int32_t ___rotated_2;
	int32_t ___trimmed_3;
	SpriteFrame_t3912389194  ___spriteSourceSize_4;
	SpriteSize_t3355290999  ___sourceSize_5;
	Vector2_t2156229523  ___pivot_6;
};
// Native definition for COM marshalling of TMPro.SpriteAssetUtilities.TexturePacker/SpriteData
struct SpriteData_t3048397587_marshaled_com
{
	Il2CppChar* ___filename_0;
	SpriteFrame_t3912389194  ___frame_1;
	int32_t ___rotated_2;
	int32_t ___trimmed_3;
	SpriteFrame_t3912389194  ___spriteSourceSize_4;
	SpriteSize_t3355290999  ___sourceSize_5;
	Vector2_t2156229523  ___pivot_6;
};
#endif // SPRITEDATA_T3048397587_H
#ifndef SPRITEASSETIMPORTFORMATS_T116390639_H
#define SPRITEASSETIMPORTFORMATS_T116390639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.SpriteAssetImportFormats
struct  SpriteAssetImportFormats_t116390639 
{
public:
	// System.Int32 TMPro.SpriteAssetUtilities.SpriteAssetImportFormats::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SpriteAssetImportFormats_t116390639, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEASSETIMPORTFORMATS_T116390639_H
#ifndef COMPUTE_DISTANCETRANSFORM_EVENTTYPES_T2554612104_H
#define COMPUTE_DISTANCETRANSFORM_EVENTTYPES_T2554612104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Compute_DistanceTransform_EventTypes
struct  Compute_DistanceTransform_EventTypes_t2554612104 
{
public:
	// System.Int32 TMPro.Compute_DistanceTransform_EventTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Compute_DistanceTransform_EventTypes_t2554612104, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPUTE_DISTANCETRANSFORM_EVENTTYPES_T2554612104_H
#ifndef TMP_VERTEX_T2404176824_H
#define TMP_VERTEX_T2404176824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Vertex
struct  TMP_Vertex_t2404176824 
{
public:
	// UnityEngine.Vector3 TMPro.TMP_Vertex::position
	Vector3_t3722313464  ___position_0;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv
	Vector2_t2156229523  ___uv_1;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv2
	Vector2_t2156229523  ___uv2_2;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv4
	Vector2_t2156229523  ___uv4_3;
	// UnityEngine.Color32 TMPro.TMP_Vertex::color
	Color32_t2600501292  ___color_4;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___position_0)); }
	inline Vector3_t3722313464  get_position_0() const { return ___position_0; }
	inline Vector3_t3722313464 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t3722313464  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_uv_1() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___uv_1)); }
	inline Vector2_t2156229523  get_uv_1() const { return ___uv_1; }
	inline Vector2_t2156229523 * get_address_of_uv_1() { return &___uv_1; }
	inline void set_uv_1(Vector2_t2156229523  value)
	{
		___uv_1 = value;
	}

	inline static int32_t get_offset_of_uv2_2() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___uv2_2)); }
	inline Vector2_t2156229523  get_uv2_2() const { return ___uv2_2; }
	inline Vector2_t2156229523 * get_address_of_uv2_2() { return &___uv2_2; }
	inline void set_uv2_2(Vector2_t2156229523  value)
	{
		___uv2_2 = value;
	}

	inline static int32_t get_offset_of_uv4_3() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___uv4_3)); }
	inline Vector2_t2156229523  get_uv4_3() const { return ___uv4_3; }
	inline Vector2_t2156229523 * get_address_of_uv4_3() { return &___uv4_3; }
	inline void set_uv4_3(Vector2_t2156229523  value)
	{
		___uv4_3 = value;
	}

	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___color_4)); }
	inline Color32_t2600501292  get_color_4() const { return ___color_4; }
	inline Color32_t2600501292 * get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(Color32_t2600501292  value)
	{
		___color_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_VERTEX_T2404176824_H
#ifndef LINESEGMENT_T1526544958_H
#define LINESEGMENT_T1526544958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextUtilities/LineSegment
struct  LineSegment_t1526544958 
{
public:
	// UnityEngine.Vector3 TMPro.TMP_TextUtilities/LineSegment::Point1
	Vector3_t3722313464  ___Point1_0;
	// UnityEngine.Vector3 TMPro.TMP_TextUtilities/LineSegment::Point2
	Vector3_t3722313464  ___Point2_1;

public:
	inline static int32_t get_offset_of_Point1_0() { return static_cast<int32_t>(offsetof(LineSegment_t1526544958, ___Point1_0)); }
	inline Vector3_t3722313464  get_Point1_0() const { return ___Point1_0; }
	inline Vector3_t3722313464 * get_address_of_Point1_0() { return &___Point1_0; }
	inline void set_Point1_0(Vector3_t3722313464  value)
	{
		___Point1_0 = value;
	}

	inline static int32_t get_offset_of_Point2_1() { return static_cast<int32_t>(offsetof(LineSegment_t1526544958, ___Point2_1)); }
	inline Vector3_t3722313464  get_Point2_1() const { return ___Point2_1; }
	inline Vector3_t3722313464 * get_address_of_Point2_1() { return &___Point2_1; }
	inline void set_Point2_1(Vector3_t3722313464  value)
	{
		___Point2_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINESEGMENT_T1526544958_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255366  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	__StaticArrayInitTypeSizeU3D12_t2710994317  ___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40 <PrivateImplementationDetails>::9E6378168821DBABB7EE3D0154346480FAC8AEF1
	__StaticArrayInitTypeSizeU3D40_t1547998295  ___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1;

public:
	inline static int32_t get_offset_of_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline __StaticArrayInitTypeSizeU3D12_t2710994317  get_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline __StaticArrayInitTypeSizeU3D12_t2710994317 * get_address_of_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(__StaticArrayInitTypeSizeU3D12_t2710994317  value)
	{
		___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}

	inline static int32_t get_offset_of_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1)); }
	inline __StaticArrayInitTypeSizeU3D40_t1547998295  get_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1() const { return ___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline __StaticArrayInitTypeSizeU3D40_t1547998295 * get_address_of_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return &___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline void set_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1(__StaticArrayInitTypeSizeU3D40_t1547998295  value)
	{
		___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#ifndef TEXTCONTAINERANCHORS_T945851193_H
#define TEXTCONTAINERANCHORS_T945851193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextContainerAnchors
struct  TextContainerAnchors_t945851193 
{
public:
	// System.Int32 TMPro.TextContainerAnchors::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextContainerAnchors_t945851193, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTCONTAINERANCHORS_T945851193_H
#ifndef MESH_EXTENTS_T3388355125_H
#define MESH_EXTENTS_T3388355125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Mesh_Extents
struct  Mesh_Extents_t3388355125 
{
public:
	// UnityEngine.Vector2 TMPro.Mesh_Extents::min
	Vector2_t2156229523  ___min_0;
	// UnityEngine.Vector2 TMPro.Mesh_Extents::max
	Vector2_t2156229523  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Mesh_Extents_t3388355125, ___min_0)); }
	inline Vector2_t2156229523  get_min_0() const { return ___min_0; }
	inline Vector2_t2156229523 * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_t2156229523  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Mesh_Extents_t3388355125, ___max_1)); }
	inline Vector2_t2156229523  get_max_1() const { return ___max_1; }
	inline Vector2_t2156229523 * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_t2156229523  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_EXTENTS_T3388355125_H
#ifndef EXTENTS_T3837212874_H
#define EXTENTS_T3837212874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Extents
struct  Extents_t3837212874 
{
public:
	// UnityEngine.Vector2 TMPro.Extents::min
	Vector2_t2156229523  ___min_0;
	// UnityEngine.Vector2 TMPro.Extents::max
	Vector2_t2156229523  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Extents_t3837212874, ___min_0)); }
	inline Vector2_t2156229523  get_min_0() const { return ___min_0; }
	inline Vector2_t2156229523 * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_t2156229523  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Extents_t3837212874, ___max_1)); }
	inline Vector2_t2156229523  get_max_1() const { return ___max_1; }
	inline Vector2_t2156229523 * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_t2156229523  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENTS_T3837212874_H
#ifndef VERTEXGRADIENT_T345148380_H
#define VERTEXGRADIENT_T345148380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexGradient
struct  VertexGradient_t345148380 
{
public:
	// UnityEngine.Color TMPro.VertexGradient::topLeft
	Color_t2555686324  ___topLeft_0;
	// UnityEngine.Color TMPro.VertexGradient::topRight
	Color_t2555686324  ___topRight_1;
	// UnityEngine.Color TMPro.VertexGradient::bottomLeft
	Color_t2555686324  ___bottomLeft_2;
	// UnityEngine.Color TMPro.VertexGradient::bottomRight
	Color_t2555686324  ___bottomRight_3;

public:
	inline static int32_t get_offset_of_topLeft_0() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___topLeft_0)); }
	inline Color_t2555686324  get_topLeft_0() const { return ___topLeft_0; }
	inline Color_t2555686324 * get_address_of_topLeft_0() { return &___topLeft_0; }
	inline void set_topLeft_0(Color_t2555686324  value)
	{
		___topLeft_0 = value;
	}

	inline static int32_t get_offset_of_topRight_1() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___topRight_1)); }
	inline Color_t2555686324  get_topRight_1() const { return ___topRight_1; }
	inline Color_t2555686324 * get_address_of_topRight_1() { return &___topRight_1; }
	inline void set_topRight_1(Color_t2555686324  value)
	{
		___topRight_1 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_2() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___bottomLeft_2)); }
	inline Color_t2555686324  get_bottomLeft_2() const { return ___bottomLeft_2; }
	inline Color_t2555686324 * get_address_of_bottomLeft_2() { return &___bottomLeft_2; }
	inline void set_bottomLeft_2(Color_t2555686324  value)
	{
		___bottomLeft_2 = value;
	}

	inline static int32_t get_offset_of_bottomRight_3() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___bottomRight_3)); }
	inline Color_t2555686324  get_bottomRight_3() const { return ___bottomRight_3; }
	inline Color_t2555686324 * get_address_of_bottomRight_3() { return &___bottomRight_3; }
	inline void set_bottomRight_3(Color_t2555686324  value)
	{
		___bottomRight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXGRADIENT_T345148380_H
#ifndef TMP_MATH_T624304809_H
#define TMP_MATH_T624304809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Math
struct  TMP_Math_t624304809  : public RuntimeObject
{
public:

public:
};

struct TMP_Math_t624304809_StaticFields
{
public:
	// UnityEngine.Vector2 TMPro.TMP_Math::MAX_16BIT
	Vector2_t2156229523  ___MAX_16BIT_6;
	// UnityEngine.Vector2 TMPro.TMP_Math::MIN_16BIT
	Vector2_t2156229523  ___MIN_16BIT_7;

public:
	inline static int32_t get_offset_of_MAX_16BIT_6() { return static_cast<int32_t>(offsetof(TMP_Math_t624304809_StaticFields, ___MAX_16BIT_6)); }
	inline Vector2_t2156229523  get_MAX_16BIT_6() const { return ___MAX_16BIT_6; }
	inline Vector2_t2156229523 * get_address_of_MAX_16BIT_6() { return &___MAX_16BIT_6; }
	inline void set_MAX_16BIT_6(Vector2_t2156229523  value)
	{
		___MAX_16BIT_6 = value;
	}

	inline static int32_t get_offset_of_MIN_16BIT_7() { return static_cast<int32_t>(offsetof(TMP_Math_t624304809_StaticFields, ___MIN_16BIT_7)); }
	inline Vector2_t2156229523  get_MIN_16BIT_7() const { return ___MIN_16BIT_7; }
	inline Vector2_t2156229523 * get_address_of_MIN_16BIT_7() { return &___MIN_16BIT_7; }
	inline void set_MIN_16BIT_7(Vector2_t2156229523  value)
	{
		___MIN_16BIT_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_MATH_T624304809_H
#ifndef GLYPH2D_T1260586688_H
#define GLYPH2D_T1260586688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Glyph2D
struct  Glyph2D_t1260586688  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 TMPro.Glyph2D::bottomLeft
	Vector3_t3722313464  ___bottomLeft_0;
	// UnityEngine.Vector3 TMPro.Glyph2D::topLeft
	Vector3_t3722313464  ___topLeft_1;
	// UnityEngine.Vector3 TMPro.Glyph2D::bottomRight
	Vector3_t3722313464  ___bottomRight_2;
	// UnityEngine.Vector3 TMPro.Glyph2D::topRight
	Vector3_t3722313464  ___topRight_3;
	// UnityEngine.Vector2 TMPro.Glyph2D::uv0
	Vector2_t2156229523  ___uv0_4;
	// UnityEngine.Vector2 TMPro.Glyph2D::uv1
	Vector2_t2156229523  ___uv1_5;
	// UnityEngine.Vector2 TMPro.Glyph2D::uv2
	Vector2_t2156229523  ___uv2_6;
	// UnityEngine.Vector2 TMPro.Glyph2D::uv3
	Vector2_t2156229523  ___uv3_7;

public:
	inline static int32_t get_offset_of_bottomLeft_0() { return static_cast<int32_t>(offsetof(Glyph2D_t1260586688, ___bottomLeft_0)); }
	inline Vector3_t3722313464  get_bottomLeft_0() const { return ___bottomLeft_0; }
	inline Vector3_t3722313464 * get_address_of_bottomLeft_0() { return &___bottomLeft_0; }
	inline void set_bottomLeft_0(Vector3_t3722313464  value)
	{
		___bottomLeft_0 = value;
	}

	inline static int32_t get_offset_of_topLeft_1() { return static_cast<int32_t>(offsetof(Glyph2D_t1260586688, ___topLeft_1)); }
	inline Vector3_t3722313464  get_topLeft_1() const { return ___topLeft_1; }
	inline Vector3_t3722313464 * get_address_of_topLeft_1() { return &___topLeft_1; }
	inline void set_topLeft_1(Vector3_t3722313464  value)
	{
		___topLeft_1 = value;
	}

	inline static int32_t get_offset_of_bottomRight_2() { return static_cast<int32_t>(offsetof(Glyph2D_t1260586688, ___bottomRight_2)); }
	inline Vector3_t3722313464  get_bottomRight_2() const { return ___bottomRight_2; }
	inline Vector3_t3722313464 * get_address_of_bottomRight_2() { return &___bottomRight_2; }
	inline void set_bottomRight_2(Vector3_t3722313464  value)
	{
		___bottomRight_2 = value;
	}

	inline static int32_t get_offset_of_topRight_3() { return static_cast<int32_t>(offsetof(Glyph2D_t1260586688, ___topRight_3)); }
	inline Vector3_t3722313464  get_topRight_3() const { return ___topRight_3; }
	inline Vector3_t3722313464 * get_address_of_topRight_3() { return &___topRight_3; }
	inline void set_topRight_3(Vector3_t3722313464  value)
	{
		___topRight_3 = value;
	}

	inline static int32_t get_offset_of_uv0_4() { return static_cast<int32_t>(offsetof(Glyph2D_t1260586688, ___uv0_4)); }
	inline Vector2_t2156229523  get_uv0_4() const { return ___uv0_4; }
	inline Vector2_t2156229523 * get_address_of_uv0_4() { return &___uv0_4; }
	inline void set_uv0_4(Vector2_t2156229523  value)
	{
		___uv0_4 = value;
	}

	inline static int32_t get_offset_of_uv1_5() { return static_cast<int32_t>(offsetof(Glyph2D_t1260586688, ___uv1_5)); }
	inline Vector2_t2156229523  get_uv1_5() const { return ___uv1_5; }
	inline Vector2_t2156229523 * get_address_of_uv1_5() { return &___uv1_5; }
	inline void set_uv1_5(Vector2_t2156229523  value)
	{
		___uv1_5 = value;
	}

	inline static int32_t get_offset_of_uv2_6() { return static_cast<int32_t>(offsetof(Glyph2D_t1260586688, ___uv2_6)); }
	inline Vector2_t2156229523  get_uv2_6() const { return ___uv2_6; }
	inline Vector2_t2156229523 * get_address_of_uv2_6() { return &___uv2_6; }
	inline void set_uv2_6(Vector2_t2156229523  value)
	{
		___uv2_6 = value;
	}

	inline static int32_t get_offset_of_uv3_7() { return static_cast<int32_t>(offsetof(Glyph2D_t1260586688, ___uv3_7)); }
	inline Vector2_t2156229523  get_uv3_7() const { return ___uv3_7; }
	inline Vector2_t2156229523 * get_address_of_uv3_7() { return &___uv3_7; }
	inline void set_uv3_7(Vector2_t2156229523  value)
	{
		___uv3_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLYPH2D_T1260586688_H
#ifndef TMP_VERTEXDATAUPDATEFLAGS_T388000256_H
#define TMP_VERTEXDATAUPDATEFLAGS_T388000256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_VertexDataUpdateFlags
struct  TMP_VertexDataUpdateFlags_t388000256 
{
public:
	// System.Int32 TMPro.TMP_VertexDataUpdateFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TMP_VertexDataUpdateFlags_t388000256, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_VERTEXDATAUPDATEFLAGS_T388000256_H
#ifndef REQUIREMENTTYPE_T3584265503_H
#define REQUIREMENTTYPE_T3584265503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParam/RequirementType
struct  RequirementType_t3584265503 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsEventParam/RequirementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RequirementType_t3584265503, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIREMENTTYPE_T3584265503_H
#ifndef TAGUNITS_T1169424683_H
#define TAGUNITS_T1169424683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagUnits
struct  TagUnits_t1169424683 
{
public:
	// System.Int32 TMPro.TagUnits::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TagUnits_t1169424683, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGUNITS_T1169424683_H
#ifndef TRIGGERLIFECYCLEEVENT_T3193146760_H
#define TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerLifecycleEvent
struct  TriggerLifecycleEvent_t3193146760 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerLifecycleEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerLifecycleEvent_t3193146760, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifndef TEXTINPUTSOURCES_T1522115805_H
#define TEXTINPUTSOURCES_T1522115805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text/TextInputSources
struct  TextInputSources_t1522115805 
{
public:
	// System.Int32 TMPro.TMP_Text/TextInputSources::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextInputSources_t1522115805, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTINPUTSOURCES_T1522115805_H
#ifndef TRIGGERTYPE_T105272677_H
#define TRIGGERTYPE_T105272677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerType
struct  TriggerType_t105272677 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerType_t105272677, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERTYPE_T105272677_H
#ifndef TRIGGEROPERATOR_T3611898925_H
#define TRIGGEROPERATOR_T3611898925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerOperator
struct  TriggerOperator_t3611898925 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerOperator::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerOperator_t3611898925, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEROPERATOR_T3611898925_H
#ifndef TAGTYPE_T123236451_H
#define TAGTYPE_T123236451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagType
struct  TagType_t123236451 
{
public:
	// System.Int32 TMPro.TagType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TagType_t123236451, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGTYPE_T123236451_H
#ifndef CARETPOSITION_T3997512201_H
#define CARETPOSITION_T3997512201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.CaretPosition
struct  CaretPosition_t3997512201 
{
public:
	// System.Int32 TMPro.CaretPosition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CaretPosition_t3997512201, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARETPOSITION_T3997512201_H
#ifndef FONTWEIGHTS_T3122883458_H
#define FONTWEIGHTS_T3122883458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontWeights
struct  FontWeights_t3122883458 
{
public:
	// System.Int32 TMPro.FontWeights::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FontWeights_t3122883458, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTWEIGHTS_T3122883458_H
#ifndef TRIGGERBOOL_T501031542_H
#define TRIGGERBOOL_T501031542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerBool
struct  TriggerBool_t501031542 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerBool::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerBool_t501031542, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERBOOL_T501031542_H
#ifndef TRIGGER_T4199345191_H
#define TRIGGER_T4199345191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker/Trigger
struct  Trigger_t4199345191 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker/Trigger::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Trigger_t4199345191, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER_T4199345191_H
#ifndef TMP_TEXTINFO_T3598145122_H
#define TMP_TEXTINFO_T3598145122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextInfo
struct  TMP_TextInfo_t3598145122  : public RuntimeObject
{
public:
	// TMPro.TMP_Text TMPro.TMP_TextInfo::textComponent
	TMP_Text_t2599618874 * ___textComponent_2;
	// System.Int32 TMPro.TMP_TextInfo::characterCount
	int32_t ___characterCount_3;
	// System.Int32 TMPro.TMP_TextInfo::spriteCount
	int32_t ___spriteCount_4;
	// System.Int32 TMPro.TMP_TextInfo::spaceCount
	int32_t ___spaceCount_5;
	// System.Int32 TMPro.TMP_TextInfo::wordCount
	int32_t ___wordCount_6;
	// System.Int32 TMPro.TMP_TextInfo::linkCount
	int32_t ___linkCount_7;
	// System.Int32 TMPro.TMP_TextInfo::lineCount
	int32_t ___lineCount_8;
	// System.Int32 TMPro.TMP_TextInfo::pageCount
	int32_t ___pageCount_9;
	// System.Int32 TMPro.TMP_TextInfo::materialCount
	int32_t ___materialCount_10;
	// TMPro.TMP_CharacterInfo[] TMPro.TMP_TextInfo::characterInfo
	TMP_CharacterInfoU5BU5D_t1930184704* ___characterInfo_11;
	// TMPro.TMP_WordInfo[] TMPro.TMP_TextInfo::wordInfo
	TMP_WordInfoU5BU5D_t3766301798* ___wordInfo_12;
	// TMPro.TMP_LinkInfo[] TMPro.TMP_TextInfo::linkInfo
	TMP_LinkInfoU5BU5D_t3558768157* ___linkInfo_13;
	// TMPro.TMP_LineInfo[] TMPro.TMP_TextInfo::lineInfo
	TMP_LineInfoU5BU5D_t4120149533* ___lineInfo_14;
	// TMPro.TMP_PageInfo[] TMPro.TMP_TextInfo::pageInfo
	TMP_PageInfoU5BU5D_t2463031060* ___pageInfo_15;
	// TMPro.TMP_MeshInfo[] TMPro.TMP_TextInfo::meshInfo
	TMP_MeshInfoU5BU5D_t3365986247* ___meshInfo_16;
	// TMPro.TMP_MeshInfo[] TMPro.TMP_TextInfo::m_CachedMeshInfo
	TMP_MeshInfoU5BU5D_t3365986247* ___m_CachedMeshInfo_17;

public:
	inline static int32_t get_offset_of_textComponent_2() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___textComponent_2)); }
	inline TMP_Text_t2599618874 * get_textComponent_2() const { return ___textComponent_2; }
	inline TMP_Text_t2599618874 ** get_address_of_textComponent_2() { return &___textComponent_2; }
	inline void set_textComponent_2(TMP_Text_t2599618874 * value)
	{
		___textComponent_2 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_2), value);
	}

	inline static int32_t get_offset_of_characterCount_3() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___characterCount_3)); }
	inline int32_t get_characterCount_3() const { return ___characterCount_3; }
	inline int32_t* get_address_of_characterCount_3() { return &___characterCount_3; }
	inline void set_characterCount_3(int32_t value)
	{
		___characterCount_3 = value;
	}

	inline static int32_t get_offset_of_spriteCount_4() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___spriteCount_4)); }
	inline int32_t get_spriteCount_4() const { return ___spriteCount_4; }
	inline int32_t* get_address_of_spriteCount_4() { return &___spriteCount_4; }
	inline void set_spriteCount_4(int32_t value)
	{
		___spriteCount_4 = value;
	}

	inline static int32_t get_offset_of_spaceCount_5() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___spaceCount_5)); }
	inline int32_t get_spaceCount_5() const { return ___spaceCount_5; }
	inline int32_t* get_address_of_spaceCount_5() { return &___spaceCount_5; }
	inline void set_spaceCount_5(int32_t value)
	{
		___spaceCount_5 = value;
	}

	inline static int32_t get_offset_of_wordCount_6() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___wordCount_6)); }
	inline int32_t get_wordCount_6() const { return ___wordCount_6; }
	inline int32_t* get_address_of_wordCount_6() { return &___wordCount_6; }
	inline void set_wordCount_6(int32_t value)
	{
		___wordCount_6 = value;
	}

	inline static int32_t get_offset_of_linkCount_7() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___linkCount_7)); }
	inline int32_t get_linkCount_7() const { return ___linkCount_7; }
	inline int32_t* get_address_of_linkCount_7() { return &___linkCount_7; }
	inline void set_linkCount_7(int32_t value)
	{
		___linkCount_7 = value;
	}

	inline static int32_t get_offset_of_lineCount_8() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___lineCount_8)); }
	inline int32_t get_lineCount_8() const { return ___lineCount_8; }
	inline int32_t* get_address_of_lineCount_8() { return &___lineCount_8; }
	inline void set_lineCount_8(int32_t value)
	{
		___lineCount_8 = value;
	}

	inline static int32_t get_offset_of_pageCount_9() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___pageCount_9)); }
	inline int32_t get_pageCount_9() const { return ___pageCount_9; }
	inline int32_t* get_address_of_pageCount_9() { return &___pageCount_9; }
	inline void set_pageCount_9(int32_t value)
	{
		___pageCount_9 = value;
	}

	inline static int32_t get_offset_of_materialCount_10() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___materialCount_10)); }
	inline int32_t get_materialCount_10() const { return ___materialCount_10; }
	inline int32_t* get_address_of_materialCount_10() { return &___materialCount_10; }
	inline void set_materialCount_10(int32_t value)
	{
		___materialCount_10 = value;
	}

	inline static int32_t get_offset_of_characterInfo_11() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___characterInfo_11)); }
	inline TMP_CharacterInfoU5BU5D_t1930184704* get_characterInfo_11() const { return ___characterInfo_11; }
	inline TMP_CharacterInfoU5BU5D_t1930184704** get_address_of_characterInfo_11() { return &___characterInfo_11; }
	inline void set_characterInfo_11(TMP_CharacterInfoU5BU5D_t1930184704* value)
	{
		___characterInfo_11 = value;
		Il2CppCodeGenWriteBarrier((&___characterInfo_11), value);
	}

	inline static int32_t get_offset_of_wordInfo_12() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___wordInfo_12)); }
	inline TMP_WordInfoU5BU5D_t3766301798* get_wordInfo_12() const { return ___wordInfo_12; }
	inline TMP_WordInfoU5BU5D_t3766301798** get_address_of_wordInfo_12() { return &___wordInfo_12; }
	inline void set_wordInfo_12(TMP_WordInfoU5BU5D_t3766301798* value)
	{
		___wordInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___wordInfo_12), value);
	}

	inline static int32_t get_offset_of_linkInfo_13() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___linkInfo_13)); }
	inline TMP_LinkInfoU5BU5D_t3558768157* get_linkInfo_13() const { return ___linkInfo_13; }
	inline TMP_LinkInfoU5BU5D_t3558768157** get_address_of_linkInfo_13() { return &___linkInfo_13; }
	inline void set_linkInfo_13(TMP_LinkInfoU5BU5D_t3558768157* value)
	{
		___linkInfo_13 = value;
		Il2CppCodeGenWriteBarrier((&___linkInfo_13), value);
	}

	inline static int32_t get_offset_of_lineInfo_14() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___lineInfo_14)); }
	inline TMP_LineInfoU5BU5D_t4120149533* get_lineInfo_14() const { return ___lineInfo_14; }
	inline TMP_LineInfoU5BU5D_t4120149533** get_address_of_lineInfo_14() { return &___lineInfo_14; }
	inline void set_lineInfo_14(TMP_LineInfoU5BU5D_t4120149533* value)
	{
		___lineInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&___lineInfo_14), value);
	}

	inline static int32_t get_offset_of_pageInfo_15() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___pageInfo_15)); }
	inline TMP_PageInfoU5BU5D_t2463031060* get_pageInfo_15() const { return ___pageInfo_15; }
	inline TMP_PageInfoU5BU5D_t2463031060** get_address_of_pageInfo_15() { return &___pageInfo_15; }
	inline void set_pageInfo_15(TMP_PageInfoU5BU5D_t2463031060* value)
	{
		___pageInfo_15 = value;
		Il2CppCodeGenWriteBarrier((&___pageInfo_15), value);
	}

	inline static int32_t get_offset_of_meshInfo_16() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___meshInfo_16)); }
	inline TMP_MeshInfoU5BU5D_t3365986247* get_meshInfo_16() const { return ___meshInfo_16; }
	inline TMP_MeshInfoU5BU5D_t3365986247** get_address_of_meshInfo_16() { return &___meshInfo_16; }
	inline void set_meshInfo_16(TMP_MeshInfoU5BU5D_t3365986247* value)
	{
		___meshInfo_16 = value;
		Il2CppCodeGenWriteBarrier((&___meshInfo_16), value);
	}

	inline static int32_t get_offset_of_m_CachedMeshInfo_17() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___m_CachedMeshInfo_17)); }
	inline TMP_MeshInfoU5BU5D_t3365986247* get_m_CachedMeshInfo_17() const { return ___m_CachedMeshInfo_17; }
	inline TMP_MeshInfoU5BU5D_t3365986247** get_address_of_m_CachedMeshInfo_17() { return &___m_CachedMeshInfo_17; }
	inline void set_m_CachedMeshInfo_17(TMP_MeshInfoU5BU5D_t3365986247* value)
	{
		___m_CachedMeshInfo_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachedMeshInfo_17), value);
	}
};

struct TMP_TextInfo_t3598145122_StaticFields
{
public:
	// UnityEngine.Vector2 TMPro.TMP_TextInfo::k_InfinityVectorPositive
	Vector2_t2156229523  ___k_InfinityVectorPositive_0;
	// UnityEngine.Vector2 TMPro.TMP_TextInfo::k_InfinityVectorNegative
	Vector2_t2156229523  ___k_InfinityVectorNegative_1;

public:
	inline static int32_t get_offset_of_k_InfinityVectorPositive_0() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122_StaticFields, ___k_InfinityVectorPositive_0)); }
	inline Vector2_t2156229523  get_k_InfinityVectorPositive_0() const { return ___k_InfinityVectorPositive_0; }
	inline Vector2_t2156229523 * get_address_of_k_InfinityVectorPositive_0() { return &___k_InfinityVectorPositive_0; }
	inline void set_k_InfinityVectorPositive_0(Vector2_t2156229523  value)
	{
		___k_InfinityVectorPositive_0 = value;
	}

	inline static int32_t get_offset_of_k_InfinityVectorNegative_1() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122_StaticFields, ___k_InfinityVectorNegative_1)); }
	inline Vector2_t2156229523  get_k_InfinityVectorNegative_1() const { return ___k_InfinityVectorNegative_1; }
	inline Vector2_t2156229523 * get_address_of_k_InfinityVectorNegative_1() { return &___k_InfinityVectorNegative_1; }
	inline void set_k_InfinityVectorNegative_1(Vector2_t2156229523  value)
	{
		___k_InfinityVectorNegative_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTINFO_T3598145122_H
#ifndef PROPERTYTYPE_T4040930247_H
#define PROPERTYTYPE_T4040930247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ValueProperty/PropertyType
struct  PropertyType_t4040930247 
{
public:
	// System.Int32 UnityEngine.Analytics.ValueProperty/PropertyType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PropertyType_t4040930247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYTYPE_T4040930247_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef CARETINFO_T841780893_H
#define CARETINFO_T841780893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.CaretInfo
struct  CaretInfo_t841780893 
{
public:
	// System.Int32 TMPro.CaretInfo::index
	int32_t ___index_0;
	// TMPro.CaretPosition TMPro.CaretInfo::position
	int32_t ___position_1;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(CaretInfo_t841780893, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(CaretInfo_t841780893, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARETINFO_T841780893_H
#ifndef COMPUTE_DT_EVENTARGS_T1071353166_H
#define COMPUTE_DT_EVENTARGS_T1071353166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Compute_DT_EventArgs
struct  Compute_DT_EventArgs_t1071353166  : public RuntimeObject
{
public:
	// TMPro.Compute_DistanceTransform_EventTypes TMPro.Compute_DT_EventArgs::EventType
	int32_t ___EventType_0;
	// System.Single TMPro.Compute_DT_EventArgs::ProgressPercentage
	float ___ProgressPercentage_1;
	// UnityEngine.Color[] TMPro.Compute_DT_EventArgs::Colors
	ColorU5BU5D_t941916413* ___Colors_2;

public:
	inline static int32_t get_offset_of_EventType_0() { return static_cast<int32_t>(offsetof(Compute_DT_EventArgs_t1071353166, ___EventType_0)); }
	inline int32_t get_EventType_0() const { return ___EventType_0; }
	inline int32_t* get_address_of_EventType_0() { return &___EventType_0; }
	inline void set_EventType_0(int32_t value)
	{
		___EventType_0 = value;
	}

	inline static int32_t get_offset_of_ProgressPercentage_1() { return static_cast<int32_t>(offsetof(Compute_DT_EventArgs_t1071353166, ___ProgressPercentage_1)); }
	inline float get_ProgressPercentage_1() const { return ___ProgressPercentage_1; }
	inline float* get_address_of_ProgressPercentage_1() { return &___ProgressPercentage_1; }
	inline void set_ProgressPercentage_1(float value)
	{
		___ProgressPercentage_1 = value;
	}

	inline static int32_t get_offset_of_Colors_2() { return static_cast<int32_t>(offsetof(Compute_DT_EventArgs_t1071353166, ___Colors_2)); }
	inline ColorU5BU5D_t941916413* get_Colors_2() const { return ___Colors_2; }
	inline ColorU5BU5D_t941916413** get_address_of_Colors_2() { return &___Colors_2; }
	inline void set_Colors_2(ColorU5BU5D_t941916413* value)
	{
		___Colors_2 = value;
		Il2CppCodeGenWriteBarrier((&___Colors_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPUTE_DT_EVENTARGS_T1071353166_H
#ifndef TMP_LINEINFO_T1079631636_H
#define TMP_LINEINFO_T1079631636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LineInfo
struct  TMP_LineInfo_t1079631636 
{
public:
	// System.Int32 TMPro.TMP_LineInfo::characterCount
	int32_t ___characterCount_0;
	// System.Int32 TMPro.TMP_LineInfo::visibleCharacterCount
	int32_t ___visibleCharacterCount_1;
	// System.Int32 TMPro.TMP_LineInfo::spaceCount
	int32_t ___spaceCount_2;
	// System.Int32 TMPro.TMP_LineInfo::wordCount
	int32_t ___wordCount_3;
	// System.Int32 TMPro.TMP_LineInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_4;
	// System.Int32 TMPro.TMP_LineInfo::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_5;
	// System.Int32 TMPro.TMP_LineInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_6;
	// System.Int32 TMPro.TMP_LineInfo::lastVisibleCharacterIndex
	int32_t ___lastVisibleCharacterIndex_7;
	// System.Single TMPro.TMP_LineInfo::length
	float ___length_8;
	// System.Single TMPro.TMP_LineInfo::lineHeight
	float ___lineHeight_9;
	// System.Single TMPro.TMP_LineInfo::ascender
	float ___ascender_10;
	// System.Single TMPro.TMP_LineInfo::baseline
	float ___baseline_11;
	// System.Single TMPro.TMP_LineInfo::descender
	float ___descender_12;
	// System.Single TMPro.TMP_LineInfo::maxAdvance
	float ___maxAdvance_13;
	// System.Single TMPro.TMP_LineInfo::width
	float ___width_14;
	// System.Single TMPro.TMP_LineInfo::marginLeft
	float ___marginLeft_15;
	// System.Single TMPro.TMP_LineInfo::marginRight
	float ___marginRight_16;
	// TMPro.TextAlignmentOptions TMPro.TMP_LineInfo::alignment
	int32_t ___alignment_17;
	// TMPro.Extents TMPro.TMP_LineInfo::lineExtents
	Extents_t3837212874  ___lineExtents_18;

public:
	inline static int32_t get_offset_of_characterCount_0() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___characterCount_0)); }
	inline int32_t get_characterCount_0() const { return ___characterCount_0; }
	inline int32_t* get_address_of_characterCount_0() { return &___characterCount_0; }
	inline void set_characterCount_0(int32_t value)
	{
		___characterCount_0 = value;
	}

	inline static int32_t get_offset_of_visibleCharacterCount_1() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___visibleCharacterCount_1)); }
	inline int32_t get_visibleCharacterCount_1() const { return ___visibleCharacterCount_1; }
	inline int32_t* get_address_of_visibleCharacterCount_1() { return &___visibleCharacterCount_1; }
	inline void set_visibleCharacterCount_1(int32_t value)
	{
		___visibleCharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_spaceCount_2() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___spaceCount_2)); }
	inline int32_t get_spaceCount_2() const { return ___spaceCount_2; }
	inline int32_t* get_address_of_spaceCount_2() { return &___spaceCount_2; }
	inline void set_spaceCount_2(int32_t value)
	{
		___spaceCount_2 = value;
	}

	inline static int32_t get_offset_of_wordCount_3() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___wordCount_3)); }
	inline int32_t get_wordCount_3() const { return ___wordCount_3; }
	inline int32_t* get_address_of_wordCount_3() { return &___wordCount_3; }
	inline void set_wordCount_3(int32_t value)
	{
		___wordCount_3 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_4() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___firstCharacterIndex_4)); }
	inline int32_t get_firstCharacterIndex_4() const { return ___firstCharacterIndex_4; }
	inline int32_t* get_address_of_firstCharacterIndex_4() { return &___firstCharacterIndex_4; }
	inline void set_firstCharacterIndex_4(int32_t value)
	{
		___firstCharacterIndex_4 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_5() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___firstVisibleCharacterIndex_5)); }
	inline int32_t get_firstVisibleCharacterIndex_5() const { return ___firstVisibleCharacterIndex_5; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_5() { return &___firstVisibleCharacterIndex_5; }
	inline void set_firstVisibleCharacterIndex_5(int32_t value)
	{
		___firstVisibleCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_6() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lastCharacterIndex_6)); }
	inline int32_t get_lastCharacterIndex_6() const { return ___lastCharacterIndex_6; }
	inline int32_t* get_address_of_lastCharacterIndex_6() { return &___lastCharacterIndex_6; }
	inline void set_lastCharacterIndex_6(int32_t value)
	{
		___lastCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharacterIndex_7() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lastVisibleCharacterIndex_7)); }
	inline int32_t get_lastVisibleCharacterIndex_7() const { return ___lastVisibleCharacterIndex_7; }
	inline int32_t* get_address_of_lastVisibleCharacterIndex_7() { return &___lastVisibleCharacterIndex_7; }
	inline void set_lastVisibleCharacterIndex_7(int32_t value)
	{
		___lastVisibleCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_length_8() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___length_8)); }
	inline float get_length_8() const { return ___length_8; }
	inline float* get_address_of_length_8() { return &___length_8; }
	inline void set_length_8(float value)
	{
		___length_8 = value;
	}

	inline static int32_t get_offset_of_lineHeight_9() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lineHeight_9)); }
	inline float get_lineHeight_9() const { return ___lineHeight_9; }
	inline float* get_address_of_lineHeight_9() { return &___lineHeight_9; }
	inline void set_lineHeight_9(float value)
	{
		___lineHeight_9 = value;
	}

	inline static int32_t get_offset_of_ascender_10() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___ascender_10)); }
	inline float get_ascender_10() const { return ___ascender_10; }
	inline float* get_address_of_ascender_10() { return &___ascender_10; }
	inline void set_ascender_10(float value)
	{
		___ascender_10 = value;
	}

	inline static int32_t get_offset_of_baseline_11() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___baseline_11)); }
	inline float get_baseline_11() const { return ___baseline_11; }
	inline float* get_address_of_baseline_11() { return &___baseline_11; }
	inline void set_baseline_11(float value)
	{
		___baseline_11 = value;
	}

	inline static int32_t get_offset_of_descender_12() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___descender_12)); }
	inline float get_descender_12() const { return ___descender_12; }
	inline float* get_address_of_descender_12() { return &___descender_12; }
	inline void set_descender_12(float value)
	{
		___descender_12 = value;
	}

	inline static int32_t get_offset_of_maxAdvance_13() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___maxAdvance_13)); }
	inline float get_maxAdvance_13() const { return ___maxAdvance_13; }
	inline float* get_address_of_maxAdvance_13() { return &___maxAdvance_13; }
	inline void set_maxAdvance_13(float value)
	{
		___maxAdvance_13 = value;
	}

	inline static int32_t get_offset_of_width_14() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___width_14)); }
	inline float get_width_14() const { return ___width_14; }
	inline float* get_address_of_width_14() { return &___width_14; }
	inline void set_width_14(float value)
	{
		___width_14 = value;
	}

	inline static int32_t get_offset_of_marginLeft_15() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___marginLeft_15)); }
	inline float get_marginLeft_15() const { return ___marginLeft_15; }
	inline float* get_address_of_marginLeft_15() { return &___marginLeft_15; }
	inline void set_marginLeft_15(float value)
	{
		___marginLeft_15 = value;
	}

	inline static int32_t get_offset_of_marginRight_16() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___marginRight_16)); }
	inline float get_marginRight_16() const { return ___marginRight_16; }
	inline float* get_address_of_marginRight_16() { return &___marginRight_16; }
	inline void set_marginRight_16(float value)
	{
		___marginRight_16 = value;
	}

	inline static int32_t get_offset_of_alignment_17() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___alignment_17)); }
	inline int32_t get_alignment_17() const { return ___alignment_17; }
	inline int32_t* get_address_of_alignment_17() { return &___alignment_17; }
	inline void set_alignment_17(int32_t value)
	{
		___alignment_17 = value;
	}

	inline static int32_t get_offset_of_lineExtents_18() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lineExtents_18)); }
	inline Extents_t3837212874  get_lineExtents_18() const { return ___lineExtents_18; }
	inline Extents_t3837212874 * get_address_of_lineExtents_18() { return &___lineExtents_18; }
	inline void set_lineExtents_18(Extents_t3837212874  value)
	{
		___lineExtents_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_LINEINFO_T1079631636_H
#ifndef TMP_CHARACTERINFO_T3185626797_H
#define TMP_CHARACTERINFO_T3185626797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_CharacterInfo
struct  TMP_CharacterInfo_t3185626797 
{
public:
	// System.Char TMPro.TMP_CharacterInfo::character
	Il2CppChar ___character_0;
	// System.Int16 TMPro.TMP_CharacterInfo::index
	int16_t ___index_1;
	// TMPro.TMP_TextElementType TMPro.TMP_CharacterInfo::elementType
	int32_t ___elementType_2;
	// TMPro.TMP_TextElement TMPro.TMP_CharacterInfo::textElement
	TMP_TextElement_t129727469 * ___textElement_3;
	// TMPro.TMP_FontAsset TMPro.TMP_CharacterInfo::fontAsset
	TMP_FontAsset_t364381626 * ___fontAsset_4;
	// TMPro.TMP_SpriteAsset TMPro.TMP_CharacterInfo::spriteAsset
	TMP_SpriteAsset_t484820633 * ___spriteAsset_5;
	// System.Int32 TMPro.TMP_CharacterInfo::spriteIndex
	int32_t ___spriteIndex_6;
	// UnityEngine.Material TMPro.TMP_CharacterInfo::material
	Material_t340375123 * ___material_7;
	// System.Int32 TMPro.TMP_CharacterInfo::materialReferenceIndex
	int32_t ___materialReferenceIndex_8;
	// System.Boolean TMPro.TMP_CharacterInfo::isUsingAlternateTypeface
	bool ___isUsingAlternateTypeface_9;
	// System.Single TMPro.TMP_CharacterInfo::pointSize
	float ___pointSize_10;
	// System.Int16 TMPro.TMP_CharacterInfo::lineNumber
	int16_t ___lineNumber_11;
	// System.Int16 TMPro.TMP_CharacterInfo::pageNumber
	int16_t ___pageNumber_12;
	// System.Int32 TMPro.TMP_CharacterInfo::vertexIndex
	int32_t ___vertexIndex_13;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_TL
	TMP_Vertex_t2404176824  ___vertex_TL_14;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_BL
	TMP_Vertex_t2404176824  ___vertex_BL_15;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_TR
	TMP_Vertex_t2404176824  ___vertex_TR_16;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_BR
	TMP_Vertex_t2404176824  ___vertex_BR_17;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::topLeft
	Vector3_t3722313464  ___topLeft_18;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::bottomLeft
	Vector3_t3722313464  ___bottomLeft_19;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::topRight
	Vector3_t3722313464  ___topRight_20;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::bottomRight
	Vector3_t3722313464  ___bottomRight_21;
	// System.Single TMPro.TMP_CharacterInfo::origin
	float ___origin_22;
	// System.Single TMPro.TMP_CharacterInfo::ascender
	float ___ascender_23;
	// System.Single TMPro.TMP_CharacterInfo::baseLine
	float ___baseLine_24;
	// System.Single TMPro.TMP_CharacterInfo::descender
	float ___descender_25;
	// System.Single TMPro.TMP_CharacterInfo::xAdvance
	float ___xAdvance_26;
	// System.Single TMPro.TMP_CharacterInfo::aspectRatio
	float ___aspectRatio_27;
	// System.Single TMPro.TMP_CharacterInfo::scale
	float ___scale_28;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::color
	Color32_t2600501292  ___color_29;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::underlineColor
	Color32_t2600501292  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::strikethroughColor
	Color32_t2600501292  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::highlightColor
	Color32_t2600501292  ___highlightColor_32;
	// TMPro.FontStyles TMPro.TMP_CharacterInfo::style
	int32_t ___style_33;
	// System.Boolean TMPro.TMP_CharacterInfo::isVisible
	bool ___isVisible_34;

public:
	inline static int32_t get_offset_of_character_0() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___character_0)); }
	inline Il2CppChar get_character_0() const { return ___character_0; }
	inline Il2CppChar* get_address_of_character_0() { return &___character_0; }
	inline void set_character_0(Il2CppChar value)
	{
		___character_0 = value;
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___index_1)); }
	inline int16_t get_index_1() const { return ___index_1; }
	inline int16_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int16_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_elementType_2() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___elementType_2)); }
	inline int32_t get_elementType_2() const { return ___elementType_2; }
	inline int32_t* get_address_of_elementType_2() { return &___elementType_2; }
	inline void set_elementType_2(int32_t value)
	{
		___elementType_2 = value;
	}

	inline static int32_t get_offset_of_textElement_3() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___textElement_3)); }
	inline TMP_TextElement_t129727469 * get_textElement_3() const { return ___textElement_3; }
	inline TMP_TextElement_t129727469 ** get_address_of_textElement_3() { return &___textElement_3; }
	inline void set_textElement_3(TMP_TextElement_t129727469 * value)
	{
		___textElement_3 = value;
		Il2CppCodeGenWriteBarrier((&___textElement_3), value);
	}

	inline static int32_t get_offset_of_fontAsset_4() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___fontAsset_4)); }
	inline TMP_FontAsset_t364381626 * get_fontAsset_4() const { return ___fontAsset_4; }
	inline TMP_FontAsset_t364381626 ** get_address_of_fontAsset_4() { return &___fontAsset_4; }
	inline void set_fontAsset_4(TMP_FontAsset_t364381626 * value)
	{
		___fontAsset_4 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_4), value);
	}

	inline static int32_t get_offset_of_spriteAsset_5() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___spriteAsset_5)); }
	inline TMP_SpriteAsset_t484820633 * get_spriteAsset_5() const { return ___spriteAsset_5; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_spriteAsset_5() { return &___spriteAsset_5; }
	inline void set_spriteAsset_5(TMP_SpriteAsset_t484820633 * value)
	{
		___spriteAsset_5 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_5), value);
	}

	inline static int32_t get_offset_of_spriteIndex_6() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___spriteIndex_6)); }
	inline int32_t get_spriteIndex_6() const { return ___spriteIndex_6; }
	inline int32_t* get_address_of_spriteIndex_6() { return &___spriteIndex_6; }
	inline void set_spriteIndex_6(int32_t value)
	{
		___spriteIndex_6 = value;
	}

	inline static int32_t get_offset_of_material_7() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___material_7)); }
	inline Material_t340375123 * get_material_7() const { return ___material_7; }
	inline Material_t340375123 ** get_address_of_material_7() { return &___material_7; }
	inline void set_material_7(Material_t340375123 * value)
	{
		___material_7 = value;
		Il2CppCodeGenWriteBarrier((&___material_7), value);
	}

	inline static int32_t get_offset_of_materialReferenceIndex_8() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___materialReferenceIndex_8)); }
	inline int32_t get_materialReferenceIndex_8() const { return ___materialReferenceIndex_8; }
	inline int32_t* get_address_of_materialReferenceIndex_8() { return &___materialReferenceIndex_8; }
	inline void set_materialReferenceIndex_8(int32_t value)
	{
		___materialReferenceIndex_8 = value;
	}

	inline static int32_t get_offset_of_isUsingAlternateTypeface_9() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___isUsingAlternateTypeface_9)); }
	inline bool get_isUsingAlternateTypeface_9() const { return ___isUsingAlternateTypeface_9; }
	inline bool* get_address_of_isUsingAlternateTypeface_9() { return &___isUsingAlternateTypeface_9; }
	inline void set_isUsingAlternateTypeface_9(bool value)
	{
		___isUsingAlternateTypeface_9 = value;
	}

	inline static int32_t get_offset_of_pointSize_10() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___pointSize_10)); }
	inline float get_pointSize_10() const { return ___pointSize_10; }
	inline float* get_address_of_pointSize_10() { return &___pointSize_10; }
	inline void set_pointSize_10(float value)
	{
		___pointSize_10 = value;
	}

	inline static int32_t get_offset_of_lineNumber_11() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___lineNumber_11)); }
	inline int16_t get_lineNumber_11() const { return ___lineNumber_11; }
	inline int16_t* get_address_of_lineNumber_11() { return &___lineNumber_11; }
	inline void set_lineNumber_11(int16_t value)
	{
		___lineNumber_11 = value;
	}

	inline static int32_t get_offset_of_pageNumber_12() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___pageNumber_12)); }
	inline int16_t get_pageNumber_12() const { return ___pageNumber_12; }
	inline int16_t* get_address_of_pageNumber_12() { return &___pageNumber_12; }
	inline void set_pageNumber_12(int16_t value)
	{
		___pageNumber_12 = value;
	}

	inline static int32_t get_offset_of_vertexIndex_13() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertexIndex_13)); }
	inline int32_t get_vertexIndex_13() const { return ___vertexIndex_13; }
	inline int32_t* get_address_of_vertexIndex_13() { return &___vertexIndex_13; }
	inline void set_vertexIndex_13(int32_t value)
	{
		___vertexIndex_13 = value;
	}

	inline static int32_t get_offset_of_vertex_TL_14() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_TL_14)); }
	inline TMP_Vertex_t2404176824  get_vertex_TL_14() const { return ___vertex_TL_14; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_TL_14() { return &___vertex_TL_14; }
	inline void set_vertex_TL_14(TMP_Vertex_t2404176824  value)
	{
		___vertex_TL_14 = value;
	}

	inline static int32_t get_offset_of_vertex_BL_15() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_BL_15)); }
	inline TMP_Vertex_t2404176824  get_vertex_BL_15() const { return ___vertex_BL_15; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_BL_15() { return &___vertex_BL_15; }
	inline void set_vertex_BL_15(TMP_Vertex_t2404176824  value)
	{
		___vertex_BL_15 = value;
	}

	inline static int32_t get_offset_of_vertex_TR_16() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_TR_16)); }
	inline TMP_Vertex_t2404176824  get_vertex_TR_16() const { return ___vertex_TR_16; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_TR_16() { return &___vertex_TR_16; }
	inline void set_vertex_TR_16(TMP_Vertex_t2404176824  value)
	{
		___vertex_TR_16 = value;
	}

	inline static int32_t get_offset_of_vertex_BR_17() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_BR_17)); }
	inline TMP_Vertex_t2404176824  get_vertex_BR_17() const { return ___vertex_BR_17; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_BR_17() { return &___vertex_BR_17; }
	inline void set_vertex_BR_17(TMP_Vertex_t2404176824  value)
	{
		___vertex_BR_17 = value;
	}

	inline static int32_t get_offset_of_topLeft_18() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___topLeft_18)); }
	inline Vector3_t3722313464  get_topLeft_18() const { return ___topLeft_18; }
	inline Vector3_t3722313464 * get_address_of_topLeft_18() { return &___topLeft_18; }
	inline void set_topLeft_18(Vector3_t3722313464  value)
	{
		___topLeft_18 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_19() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___bottomLeft_19)); }
	inline Vector3_t3722313464  get_bottomLeft_19() const { return ___bottomLeft_19; }
	inline Vector3_t3722313464 * get_address_of_bottomLeft_19() { return &___bottomLeft_19; }
	inline void set_bottomLeft_19(Vector3_t3722313464  value)
	{
		___bottomLeft_19 = value;
	}

	inline static int32_t get_offset_of_topRight_20() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___topRight_20)); }
	inline Vector3_t3722313464  get_topRight_20() const { return ___topRight_20; }
	inline Vector3_t3722313464 * get_address_of_topRight_20() { return &___topRight_20; }
	inline void set_topRight_20(Vector3_t3722313464  value)
	{
		___topRight_20 = value;
	}

	inline static int32_t get_offset_of_bottomRight_21() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___bottomRight_21)); }
	inline Vector3_t3722313464  get_bottomRight_21() const { return ___bottomRight_21; }
	inline Vector3_t3722313464 * get_address_of_bottomRight_21() { return &___bottomRight_21; }
	inline void set_bottomRight_21(Vector3_t3722313464  value)
	{
		___bottomRight_21 = value;
	}

	inline static int32_t get_offset_of_origin_22() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___origin_22)); }
	inline float get_origin_22() const { return ___origin_22; }
	inline float* get_address_of_origin_22() { return &___origin_22; }
	inline void set_origin_22(float value)
	{
		___origin_22 = value;
	}

	inline static int32_t get_offset_of_ascender_23() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___ascender_23)); }
	inline float get_ascender_23() const { return ___ascender_23; }
	inline float* get_address_of_ascender_23() { return &___ascender_23; }
	inline void set_ascender_23(float value)
	{
		___ascender_23 = value;
	}

	inline static int32_t get_offset_of_baseLine_24() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___baseLine_24)); }
	inline float get_baseLine_24() const { return ___baseLine_24; }
	inline float* get_address_of_baseLine_24() { return &___baseLine_24; }
	inline void set_baseLine_24(float value)
	{
		___baseLine_24 = value;
	}

	inline static int32_t get_offset_of_descender_25() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___descender_25)); }
	inline float get_descender_25() const { return ___descender_25; }
	inline float* get_address_of_descender_25() { return &___descender_25; }
	inline void set_descender_25(float value)
	{
		___descender_25 = value;
	}

	inline static int32_t get_offset_of_xAdvance_26() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___xAdvance_26)); }
	inline float get_xAdvance_26() const { return ___xAdvance_26; }
	inline float* get_address_of_xAdvance_26() { return &___xAdvance_26; }
	inline void set_xAdvance_26(float value)
	{
		___xAdvance_26 = value;
	}

	inline static int32_t get_offset_of_aspectRatio_27() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___aspectRatio_27)); }
	inline float get_aspectRatio_27() const { return ___aspectRatio_27; }
	inline float* get_address_of_aspectRatio_27() { return &___aspectRatio_27; }
	inline void set_aspectRatio_27(float value)
	{
		___aspectRatio_27 = value;
	}

	inline static int32_t get_offset_of_scale_28() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___scale_28)); }
	inline float get_scale_28() const { return ___scale_28; }
	inline float* get_address_of_scale_28() { return &___scale_28; }
	inline void set_scale_28(float value)
	{
		___scale_28 = value;
	}

	inline static int32_t get_offset_of_color_29() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___color_29)); }
	inline Color32_t2600501292  get_color_29() const { return ___color_29; }
	inline Color32_t2600501292 * get_address_of_color_29() { return &___color_29; }
	inline void set_color_29(Color32_t2600501292  value)
	{
		___color_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___underlineColor_30)); }
	inline Color32_t2600501292  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t2600501292 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t2600501292  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___strikethroughColor_31)); }
	inline Color32_t2600501292  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t2600501292 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t2600501292  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___highlightColor_32)); }
	inline Color32_t2600501292  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t2600501292 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t2600501292  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_style_33() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___style_33)); }
	inline int32_t get_style_33() const { return ___style_33; }
	inline int32_t* get_address_of_style_33() { return &___style_33; }
	inline void set_style_33(int32_t value)
	{
		___style_33 = value;
	}

	inline static int32_t get_offset_of_isVisible_34() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___isVisible_34)); }
	inline bool get_isVisible_34() const { return ___isVisible_34; }
	inline bool* get_address_of_isVisible_34() { return &___isVisible_34; }
	inline void set_isVisible_34(bool value)
	{
		___isVisible_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_CharacterInfo
struct TMP_CharacterInfo_t3185626797_marshaled_pinvoke
{
	uint8_t ___character_0;
	int16_t ___index_1;
	int32_t ___elementType_2;
	TMP_TextElement_t129727469 * ___textElement_3;
	TMP_FontAsset_t364381626 * ___fontAsset_4;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_5;
	int32_t ___spriteIndex_6;
	Material_t340375123 * ___material_7;
	int32_t ___materialReferenceIndex_8;
	int32_t ___isUsingAlternateTypeface_9;
	float ___pointSize_10;
	int16_t ___lineNumber_11;
	int16_t ___pageNumber_12;
	int32_t ___vertexIndex_13;
	TMP_Vertex_t2404176824  ___vertex_TL_14;
	TMP_Vertex_t2404176824  ___vertex_BL_15;
	TMP_Vertex_t2404176824  ___vertex_TR_16;
	TMP_Vertex_t2404176824  ___vertex_BR_17;
	Vector3_t3722313464  ___topLeft_18;
	Vector3_t3722313464  ___bottomLeft_19;
	Vector3_t3722313464  ___topRight_20;
	Vector3_t3722313464  ___bottomRight_21;
	float ___origin_22;
	float ___ascender_23;
	float ___baseLine_24;
	float ___descender_25;
	float ___xAdvance_26;
	float ___aspectRatio_27;
	float ___scale_28;
	Color32_t2600501292  ___color_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	int32_t ___style_33;
	int32_t ___isVisible_34;
};
// Native definition for COM marshalling of TMPro.TMP_CharacterInfo
struct TMP_CharacterInfo_t3185626797_marshaled_com
{
	uint8_t ___character_0;
	int16_t ___index_1;
	int32_t ___elementType_2;
	TMP_TextElement_t129727469 * ___textElement_3;
	TMP_FontAsset_t364381626 * ___fontAsset_4;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_5;
	int32_t ___spriteIndex_6;
	Material_t340375123 * ___material_7;
	int32_t ___materialReferenceIndex_8;
	int32_t ___isUsingAlternateTypeface_9;
	float ___pointSize_10;
	int16_t ___lineNumber_11;
	int16_t ___pageNumber_12;
	int32_t ___vertexIndex_13;
	TMP_Vertex_t2404176824  ___vertex_TL_14;
	TMP_Vertex_t2404176824  ___vertex_BL_15;
	TMP_Vertex_t2404176824  ___vertex_TR_16;
	TMP_Vertex_t2404176824  ___vertex_BR_17;
	Vector3_t3722313464  ___topLeft_18;
	Vector3_t3722313464  ___bottomLeft_19;
	Vector3_t3722313464  ___topRight_20;
	Vector3_t3722313464  ___bottomRight_21;
	float ___origin_22;
	float ___ascender_23;
	float ___baseLine_24;
	float ___descender_25;
	float ___xAdvance_26;
	float ___aspectRatio_27;
	float ___scale_28;
	Color32_t2600501292  ___color_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	int32_t ___style_33;
	int32_t ___isVisible_34;
};
#endif // TMP_CHARACTERINFO_T3185626797_H
#ifndef TMP_XMLTAGSTACK_1_T3600445780_H
#define TMP_XMLTAGSTACK_1_T3600445780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions>
struct  TMP_XmlTagStack_1_t3600445780 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	TextAlignmentOptionsU5BU5D_t3552942253* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	int32_t ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___itemStack_0)); }
	inline TextAlignmentOptionsU5BU5D_t3552942253* get_itemStack_0() const { return ___itemStack_0; }
	inline TextAlignmentOptionsU5BU5D_t3552942253** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(TextAlignmentOptionsU5BU5D_t3552942253* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___m_defaultItem_3)); }
	inline int32_t get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline int32_t* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(int32_t value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T3600445780_H
#ifndef EVENTTRIGGER_T2527451695_H
#define EVENTTRIGGER_T2527451695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger
struct  EventTrigger_t2527451695  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_IsTriggerExpanded
	bool ___m_IsTriggerExpanded_0;
	// UnityEngine.Analytics.TriggerType UnityEngine.Analytics.EventTrigger::m_Type
	int32_t ___m_Type_1;
	// UnityEngine.Analytics.TriggerLifecycleEvent UnityEngine.Analytics.EventTrigger::m_LifecycleEvent
	int32_t ___m_LifecycleEvent_2;
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_ApplyRules
	bool ___m_ApplyRules_3;
	// UnityEngine.Analytics.TriggerListContainer UnityEngine.Analytics.EventTrigger::m_Rules
	TriggerListContainer_t2032715483 * ___m_Rules_4;
	// UnityEngine.Analytics.TriggerBool UnityEngine.Analytics.EventTrigger::m_TriggerBool
	int32_t ___m_TriggerBool_5;
	// System.Single UnityEngine.Analytics.EventTrigger::m_InitTime
	float ___m_InitTime_6;
	// System.Single UnityEngine.Analytics.EventTrigger::m_RepeatTime
	float ___m_RepeatTime_7;
	// System.Int32 UnityEngine.Analytics.EventTrigger::m_Repetitions
	int32_t ___m_Repetitions_8;
	// System.Int32 UnityEngine.Analytics.EventTrigger::repetitionCount
	int32_t ___repetitionCount_9;
	// UnityEngine.Analytics.EventTrigger/OnTrigger UnityEngine.Analytics.EventTrigger::m_TriggerFunction
	OnTrigger_t4184125570 * ___m_TriggerFunction_10;
	// UnityEngine.Analytics.TriggerMethod UnityEngine.Analytics.EventTrigger::m_Method
	TriggerMethod_t582536534 * ___m_Method_11;

public:
	inline static int32_t get_offset_of_m_IsTriggerExpanded_0() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_IsTriggerExpanded_0)); }
	inline bool get_m_IsTriggerExpanded_0() const { return ___m_IsTriggerExpanded_0; }
	inline bool* get_address_of_m_IsTriggerExpanded_0() { return &___m_IsTriggerExpanded_0; }
	inline void set_m_IsTriggerExpanded_0(bool value)
	{
		___m_IsTriggerExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_Type_1() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Type_1)); }
	inline int32_t get_m_Type_1() const { return ___m_Type_1; }
	inline int32_t* get_address_of_m_Type_1() { return &___m_Type_1; }
	inline void set_m_Type_1(int32_t value)
	{
		___m_Type_1 = value;
	}

	inline static int32_t get_offset_of_m_LifecycleEvent_2() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_LifecycleEvent_2)); }
	inline int32_t get_m_LifecycleEvent_2() const { return ___m_LifecycleEvent_2; }
	inline int32_t* get_address_of_m_LifecycleEvent_2() { return &___m_LifecycleEvent_2; }
	inline void set_m_LifecycleEvent_2(int32_t value)
	{
		___m_LifecycleEvent_2 = value;
	}

	inline static int32_t get_offset_of_m_ApplyRules_3() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_ApplyRules_3)); }
	inline bool get_m_ApplyRules_3() const { return ___m_ApplyRules_3; }
	inline bool* get_address_of_m_ApplyRules_3() { return &___m_ApplyRules_3; }
	inline void set_m_ApplyRules_3(bool value)
	{
		___m_ApplyRules_3 = value;
	}

	inline static int32_t get_offset_of_m_Rules_4() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Rules_4)); }
	inline TriggerListContainer_t2032715483 * get_m_Rules_4() const { return ___m_Rules_4; }
	inline TriggerListContainer_t2032715483 ** get_address_of_m_Rules_4() { return &___m_Rules_4; }
	inline void set_m_Rules_4(TriggerListContainer_t2032715483 * value)
	{
		___m_Rules_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_4), value);
	}

	inline static int32_t get_offset_of_m_TriggerBool_5() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerBool_5)); }
	inline int32_t get_m_TriggerBool_5() const { return ___m_TriggerBool_5; }
	inline int32_t* get_address_of_m_TriggerBool_5() { return &___m_TriggerBool_5; }
	inline void set_m_TriggerBool_5(int32_t value)
	{
		___m_TriggerBool_5 = value;
	}

	inline static int32_t get_offset_of_m_InitTime_6() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_InitTime_6)); }
	inline float get_m_InitTime_6() const { return ___m_InitTime_6; }
	inline float* get_address_of_m_InitTime_6() { return &___m_InitTime_6; }
	inline void set_m_InitTime_6(float value)
	{
		___m_InitTime_6 = value;
	}

	inline static int32_t get_offset_of_m_RepeatTime_7() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_RepeatTime_7)); }
	inline float get_m_RepeatTime_7() const { return ___m_RepeatTime_7; }
	inline float* get_address_of_m_RepeatTime_7() { return &___m_RepeatTime_7; }
	inline void set_m_RepeatTime_7(float value)
	{
		___m_RepeatTime_7 = value;
	}

	inline static int32_t get_offset_of_m_Repetitions_8() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Repetitions_8)); }
	inline int32_t get_m_Repetitions_8() const { return ___m_Repetitions_8; }
	inline int32_t* get_address_of_m_Repetitions_8() { return &___m_Repetitions_8; }
	inline void set_m_Repetitions_8(int32_t value)
	{
		___m_Repetitions_8 = value;
	}

	inline static int32_t get_offset_of_repetitionCount_9() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___repetitionCount_9)); }
	inline int32_t get_repetitionCount_9() const { return ___repetitionCount_9; }
	inline int32_t* get_address_of_repetitionCount_9() { return &___repetitionCount_9; }
	inline void set_repetitionCount_9(int32_t value)
	{
		___repetitionCount_9 = value;
	}

	inline static int32_t get_offset_of_m_TriggerFunction_10() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerFunction_10)); }
	inline OnTrigger_t4184125570 * get_m_TriggerFunction_10() const { return ___m_TriggerFunction_10; }
	inline OnTrigger_t4184125570 ** get_address_of_m_TriggerFunction_10() { return &___m_TriggerFunction_10; }
	inline void set_m_TriggerFunction_10(OnTrigger_t4184125570 * value)
	{
		___m_TriggerFunction_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TriggerFunction_10), value);
	}

	inline static int32_t get_offset_of_m_Method_11() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Method_11)); }
	inline TriggerMethod_t582536534 * get_m_Method_11() const { return ___m_Method_11; }
	inline TriggerMethod_t582536534 ** get_address_of_m_Method_11() { return &___m_Method_11; }
	inline void set_m_Method_11(TriggerMethod_t582536534 * value)
	{
		___m_Method_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Method_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGER_T2527451695_H
#ifndef VALUEPROPERTY_T1868393739_H
#define VALUEPROPERTY_T1868393739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ValueProperty
struct  ValueProperty_t1868393739  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_EditingCustomValue
	bool ___m_EditingCustomValue_0;
	// System.Int32 UnityEngine.Analytics.ValueProperty::m_PopupIndex
	int32_t ___m_PopupIndex_1;
	// System.String UnityEngine.Analytics.ValueProperty::m_CustomValue
	String_t* ___m_CustomValue_2;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_FixedType
	bool ___m_FixedType_3;
	// System.String UnityEngine.Analytics.ValueProperty::m_EnumType
	String_t* ___m_EnumType_4;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_EnumTypeIsCustomizable
	bool ___m_EnumTypeIsCustomizable_5;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_CanDisable
	bool ___m_CanDisable_6;
	// UnityEngine.Analytics.ValueProperty/PropertyType UnityEngine.Analytics.ValueProperty::m_PropertyType
	int32_t ___m_PropertyType_7;
	// System.String UnityEngine.Analytics.ValueProperty::m_ValueType
	String_t* ___m_ValueType_8;
	// System.String UnityEngine.Analytics.ValueProperty::m_Value
	String_t* ___m_Value_9;
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.ValueProperty::m_Target
	TrackableField_t1772682203 * ___m_Target_10;

public:
	inline static int32_t get_offset_of_m_EditingCustomValue_0() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EditingCustomValue_0)); }
	inline bool get_m_EditingCustomValue_0() const { return ___m_EditingCustomValue_0; }
	inline bool* get_address_of_m_EditingCustomValue_0() { return &___m_EditingCustomValue_0; }
	inline void set_m_EditingCustomValue_0(bool value)
	{
		___m_EditingCustomValue_0 = value;
	}

	inline static int32_t get_offset_of_m_PopupIndex_1() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_PopupIndex_1)); }
	inline int32_t get_m_PopupIndex_1() const { return ___m_PopupIndex_1; }
	inline int32_t* get_address_of_m_PopupIndex_1() { return &___m_PopupIndex_1; }
	inline void set_m_PopupIndex_1(int32_t value)
	{
		___m_PopupIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_CustomValue_2() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_CustomValue_2)); }
	inline String_t* get_m_CustomValue_2() const { return ___m_CustomValue_2; }
	inline String_t** get_address_of_m_CustomValue_2() { return &___m_CustomValue_2; }
	inline void set_m_CustomValue_2(String_t* value)
	{
		___m_CustomValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomValue_2), value);
	}

	inline static int32_t get_offset_of_m_FixedType_3() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_FixedType_3)); }
	inline bool get_m_FixedType_3() const { return ___m_FixedType_3; }
	inline bool* get_address_of_m_FixedType_3() { return &___m_FixedType_3; }
	inline void set_m_FixedType_3(bool value)
	{
		___m_FixedType_3 = value;
	}

	inline static int32_t get_offset_of_m_EnumType_4() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EnumType_4)); }
	inline String_t* get_m_EnumType_4() const { return ___m_EnumType_4; }
	inline String_t** get_address_of_m_EnumType_4() { return &___m_EnumType_4; }
	inline void set_m_EnumType_4(String_t* value)
	{
		___m_EnumType_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EnumType_4), value);
	}

	inline static int32_t get_offset_of_m_EnumTypeIsCustomizable_5() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EnumTypeIsCustomizable_5)); }
	inline bool get_m_EnumTypeIsCustomizable_5() const { return ___m_EnumTypeIsCustomizable_5; }
	inline bool* get_address_of_m_EnumTypeIsCustomizable_5() { return &___m_EnumTypeIsCustomizable_5; }
	inline void set_m_EnumTypeIsCustomizable_5(bool value)
	{
		___m_EnumTypeIsCustomizable_5 = value;
	}

	inline static int32_t get_offset_of_m_CanDisable_6() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_CanDisable_6)); }
	inline bool get_m_CanDisable_6() const { return ___m_CanDisable_6; }
	inline bool* get_address_of_m_CanDisable_6() { return &___m_CanDisable_6; }
	inline void set_m_CanDisable_6(bool value)
	{
		___m_CanDisable_6 = value;
	}

	inline static int32_t get_offset_of_m_PropertyType_7() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_PropertyType_7)); }
	inline int32_t get_m_PropertyType_7() const { return ___m_PropertyType_7; }
	inline int32_t* get_address_of_m_PropertyType_7() { return &___m_PropertyType_7; }
	inline void set_m_PropertyType_7(int32_t value)
	{
		___m_PropertyType_7 = value;
	}

	inline static int32_t get_offset_of_m_ValueType_8() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_ValueType_8)); }
	inline String_t* get_m_ValueType_8() const { return ___m_ValueType_8; }
	inline String_t** get_address_of_m_ValueType_8() { return &___m_ValueType_8; }
	inline void set_m_ValueType_8(String_t* value)
	{
		___m_ValueType_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValueType_8), value);
	}

	inline static int32_t get_offset_of_m_Value_9() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_Value_9)); }
	inline String_t* get_m_Value_9() const { return ___m_Value_9; }
	inline String_t** get_address_of_m_Value_9() { return &___m_Value_9; }
	inline void set_m_Value_9(String_t* value)
	{
		___m_Value_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_9), value);
	}

	inline static int32_t get_offset_of_m_Target_10() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_Target_10)); }
	inline TrackableField_t1772682203 * get_m_Target_10() const { return ___m_Target_10; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_10() { return &___m_Target_10; }
	inline void set_m_Target_10(TrackableField_t1772682203 * value)
	{
		___m_Target_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUEPROPERTY_T1868393739_H
#ifndef ANALYTICSEVENTPARAM_T2480121928_H
#define ANALYTICSEVENTPARAM_T2480121928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParam
struct  AnalyticsEventParam_t2480121928  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.AnalyticsEventParam/RequirementType UnityEngine.Analytics.AnalyticsEventParam::m_RequirementType
	int32_t ___m_RequirementType_0;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_GroupID
	String_t* ___m_GroupID_1;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_Tooltip
	String_t* ___m_Tooltip_2;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_Name
	String_t* ___m_Name_3;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.AnalyticsEventParam::m_Value
	ValueProperty_t1868393739 * ___m_Value_4;

public:
	inline static int32_t get_offset_of_m_RequirementType_0() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_RequirementType_0)); }
	inline int32_t get_m_RequirementType_0() const { return ___m_RequirementType_0; }
	inline int32_t* get_address_of_m_RequirementType_0() { return &___m_RequirementType_0; }
	inline void set_m_RequirementType_0(int32_t value)
	{
		___m_RequirementType_0 = value;
	}

	inline static int32_t get_offset_of_m_GroupID_1() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_GroupID_1)); }
	inline String_t* get_m_GroupID_1() const { return ___m_GroupID_1; }
	inline String_t** get_address_of_m_GroupID_1() { return &___m_GroupID_1; }
	inline void set_m_GroupID_1(String_t* value)
	{
		___m_GroupID_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_GroupID_1), value);
	}

	inline static int32_t get_offset_of_m_Tooltip_2() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Tooltip_2)); }
	inline String_t* get_m_Tooltip_2() const { return ___m_Tooltip_2; }
	inline String_t** get_address_of_m_Tooltip_2() { return &___m_Tooltip_2; }
	inline void set_m_Tooltip_2(String_t* value)
	{
		___m_Tooltip_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tooltip_2), value);
	}

	inline static int32_t get_offset_of_m_Name_3() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Name_3)); }
	inline String_t* get_m_Name_3() const { return ___m_Name_3; }
	inline String_t** get_address_of_m_Name_3() { return &___m_Name_3; }
	inline void set_m_Name_3(String_t* value)
	{
		___m_Name_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_3), value);
	}

	inline static int32_t get_offset_of_m_Value_4() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Value_4)); }
	inline ValueProperty_t1868393739 * get_m_Value_4() const { return ___m_Value_4; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_4() { return &___m_Value_4; }
	inline void set_m_Value_4(ValueProperty_t1868393739 * value)
	{
		___m_Value_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTPARAM_T2480121928_H
#ifndef XML_TAGATTRIBUTE_T1174424309_H
#define XML_TAGATTRIBUTE_T1174424309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.XML_TagAttribute
struct  XML_TagAttribute_t1174424309 
{
public:
	// System.Int32 TMPro.XML_TagAttribute::nameHashCode
	int32_t ___nameHashCode_0;
	// TMPro.TagType TMPro.XML_TagAttribute::valueType
	int32_t ___valueType_1;
	// System.Int32 TMPro.XML_TagAttribute::valueStartIndex
	int32_t ___valueStartIndex_2;
	// System.Int32 TMPro.XML_TagAttribute::valueLength
	int32_t ___valueLength_3;
	// System.Int32 TMPro.XML_TagAttribute::valueHashCode
	int32_t ___valueHashCode_4;

public:
	inline static int32_t get_offset_of_nameHashCode_0() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1174424309, ___nameHashCode_0)); }
	inline int32_t get_nameHashCode_0() const { return ___nameHashCode_0; }
	inline int32_t* get_address_of_nameHashCode_0() { return &___nameHashCode_0; }
	inline void set_nameHashCode_0(int32_t value)
	{
		___nameHashCode_0 = value;
	}

	inline static int32_t get_offset_of_valueType_1() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1174424309, ___valueType_1)); }
	inline int32_t get_valueType_1() const { return ___valueType_1; }
	inline int32_t* get_address_of_valueType_1() { return &___valueType_1; }
	inline void set_valueType_1(int32_t value)
	{
		___valueType_1 = value;
	}

	inline static int32_t get_offset_of_valueStartIndex_2() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1174424309, ___valueStartIndex_2)); }
	inline int32_t get_valueStartIndex_2() const { return ___valueStartIndex_2; }
	inline int32_t* get_address_of_valueStartIndex_2() { return &___valueStartIndex_2; }
	inline void set_valueStartIndex_2(int32_t value)
	{
		___valueStartIndex_2 = value;
	}

	inline static int32_t get_offset_of_valueLength_3() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1174424309, ___valueLength_3)); }
	inline int32_t get_valueLength_3() const { return ___valueLength_3; }
	inline int32_t* get_address_of_valueLength_3() { return &___valueLength_3; }
	inline void set_valueLength_3(int32_t value)
	{
		___valueLength_3 = value;
	}

	inline static int32_t get_offset_of_valueHashCode_4() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1174424309, ___valueHashCode_4)); }
	inline int32_t get_valueHashCode_4() const { return ___valueHashCode_4; }
	inline int32_t* get_address_of_valueHashCode_4() { return &___valueHashCode_4; }
	inline void set_valueHashCode_4(int32_t value)
	{
		___valueHashCode_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XML_TAGATTRIBUTE_T1174424309_H
#ifndef TRIGGERRULE_T1946298321_H
#define TRIGGERRULE_T1946298321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerRule
struct  TriggerRule_t1946298321  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.TriggerRule::m_Target
	TrackableField_t1772682203 * ___m_Target_0;
	// UnityEngine.Analytics.TriggerOperator UnityEngine.Analytics.TriggerRule::m_Operator
	int32_t ___m_Operator_1;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value
	ValueProperty_t1868393739 * ___m_Value_2;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value2
	ValueProperty_t1868393739 * ___m_Value2_3;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Target_0)); }
	inline TrackableField_t1772682203 * get_m_Target_0() const { return ___m_Target_0; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(TrackableField_t1772682203 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Operator_1() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Operator_1)); }
	inline int32_t get_m_Operator_1() const { return ___m_Operator_1; }
	inline int32_t* get_address_of_m_Operator_1() { return &___m_Operator_1; }
	inline void set_m_Operator_1(int32_t value)
	{
		___m_Operator_1 = value;
	}

	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value_2)); }
	inline ValueProperty_t1868393739 * get_m_Value_2() const { return ___m_Value_2; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(ValueProperty_t1868393739 * value)
	{
		___m_Value_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_2), value);
	}

	inline static int32_t get_offset_of_m_Value2_3() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value2_3)); }
	inline ValueProperty_t1868393739 * get_m_Value2_3() const { return ___m_Value2_3; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value2_3() { return &___m_Value2_3; }
	inline void set_m_Value2_3(ValueProperty_t1868393739 * value)
	{
		___m_Value2_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERRULE_T1946298321_H
#ifndef WORDWRAPSTATE_T341939652_H
#define WORDWRAPSTATE_T341939652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.WordWrapState
struct  WordWrapState_t341939652 
{
public:
	// System.Int32 TMPro.WordWrapState::previous_WordBreak
	int32_t ___previous_WordBreak_0;
	// System.Int32 TMPro.WordWrapState::total_CharacterCount
	int32_t ___total_CharacterCount_1;
	// System.Int32 TMPro.WordWrapState::visible_CharacterCount
	int32_t ___visible_CharacterCount_2;
	// System.Int32 TMPro.WordWrapState::visible_SpriteCount
	int32_t ___visible_SpriteCount_3;
	// System.Int32 TMPro.WordWrapState::visible_LinkCount
	int32_t ___visible_LinkCount_4;
	// System.Int32 TMPro.WordWrapState::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.WordWrapState::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.WordWrapState::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.WordWrapState::lastVisibleCharIndex
	int32_t ___lastVisibleCharIndex_8;
	// System.Int32 TMPro.WordWrapState::lineNumber
	int32_t ___lineNumber_9;
	// System.Single TMPro.WordWrapState::maxCapHeight
	float ___maxCapHeight_10;
	// System.Single TMPro.WordWrapState::maxAscender
	float ___maxAscender_11;
	// System.Single TMPro.WordWrapState::maxDescender
	float ___maxDescender_12;
	// System.Single TMPro.WordWrapState::maxLineAscender
	float ___maxLineAscender_13;
	// System.Single TMPro.WordWrapState::maxLineDescender
	float ___maxLineDescender_14;
	// System.Single TMPro.WordWrapState::previousLineAscender
	float ___previousLineAscender_15;
	// System.Single TMPro.WordWrapState::xAdvance
	float ___xAdvance_16;
	// System.Single TMPro.WordWrapState::preferredWidth
	float ___preferredWidth_17;
	// System.Single TMPro.WordWrapState::preferredHeight
	float ___preferredHeight_18;
	// System.Single TMPro.WordWrapState::previousLineScale
	float ___previousLineScale_19;
	// System.Int32 TMPro.WordWrapState::wordCount
	int32_t ___wordCount_20;
	// TMPro.FontStyles TMPro.WordWrapState::fontStyle
	int32_t ___fontStyle_21;
	// System.Single TMPro.WordWrapState::fontScale
	float ___fontScale_22;
	// System.Single TMPro.WordWrapState::fontScaleMultiplier
	float ___fontScaleMultiplier_23;
	// System.Single TMPro.WordWrapState::currentFontSize
	float ___currentFontSize_24;
	// System.Single TMPro.WordWrapState::baselineOffset
	float ___baselineOffset_25;
	// System.Single TMPro.WordWrapState::lineOffset
	float ___lineOffset_26;
	// TMPro.TMP_TextInfo TMPro.WordWrapState::textInfo
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	// TMPro.TMP_LineInfo TMPro.WordWrapState::lineInfo
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	// UnityEngine.Color32 TMPro.WordWrapState::vertexColor
	Color32_t2600501292  ___vertexColor_29;
	// UnityEngine.Color32 TMPro.WordWrapState::underlineColor
	Color32_t2600501292  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.WordWrapState::strikethroughColor
	Color32_t2600501292  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.WordWrapState::highlightColor
	Color32_t2600501292  ___highlightColor_32;
	// TMPro.TMP_BasicXmlTagStack TMPro.WordWrapState::basicStyleStack
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::colorStack
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::underlineColorStack
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::strikethroughColorStack
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::highlightColorStack
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::sizeStack
	TMP_XmlTagStack_1_t960921318  ___sizeStack_38;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::indentStack
	TMP_XmlTagStack_1_t960921318  ___indentStack_39;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::fontWeightStack
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_40;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::styleStack
	TMP_XmlTagStack_1_t2514600297  ___styleStack_41;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::baselineStack
	TMP_XmlTagStack_1_t960921318  ___baselineStack_42;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::actionStack
	TMP_XmlTagStack_1_t2514600297  ___actionStack_43;
	// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference> TMPro.WordWrapState::materialReferenceStack
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_44;
	// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions> TMPro.WordWrapState::lineJustificationStack
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_45;
	// System.Int32 TMPro.WordWrapState::spriteAnimationID
	int32_t ___spriteAnimationID_46;
	// TMPro.TMP_FontAsset TMPro.WordWrapState::currentFontAsset
	TMP_FontAsset_t364381626 * ___currentFontAsset_47;
	// TMPro.TMP_SpriteAsset TMPro.WordWrapState::currentSpriteAsset
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_48;
	// UnityEngine.Material TMPro.WordWrapState::currentMaterial
	Material_t340375123 * ___currentMaterial_49;
	// System.Int32 TMPro.WordWrapState::currentMaterialIndex
	int32_t ___currentMaterialIndex_50;
	// TMPro.Extents TMPro.WordWrapState::meshExtents
	Extents_t3837212874  ___meshExtents_51;
	// System.Boolean TMPro.WordWrapState::tagNoParsing
	bool ___tagNoParsing_52;

public:
	inline static int32_t get_offset_of_previous_WordBreak_0() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previous_WordBreak_0)); }
	inline int32_t get_previous_WordBreak_0() const { return ___previous_WordBreak_0; }
	inline int32_t* get_address_of_previous_WordBreak_0() { return &___previous_WordBreak_0; }
	inline void set_previous_WordBreak_0(int32_t value)
	{
		___previous_WordBreak_0 = value;
	}

	inline static int32_t get_offset_of_total_CharacterCount_1() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___total_CharacterCount_1)); }
	inline int32_t get_total_CharacterCount_1() const { return ___total_CharacterCount_1; }
	inline int32_t* get_address_of_total_CharacterCount_1() { return &___total_CharacterCount_1; }
	inline void set_total_CharacterCount_1(int32_t value)
	{
		___total_CharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_visible_CharacterCount_2() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_CharacterCount_2)); }
	inline int32_t get_visible_CharacterCount_2() const { return ___visible_CharacterCount_2; }
	inline int32_t* get_address_of_visible_CharacterCount_2() { return &___visible_CharacterCount_2; }
	inline void set_visible_CharacterCount_2(int32_t value)
	{
		___visible_CharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_visible_SpriteCount_3() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_SpriteCount_3)); }
	inline int32_t get_visible_SpriteCount_3() const { return ___visible_SpriteCount_3; }
	inline int32_t* get_address_of_visible_SpriteCount_3() { return &___visible_SpriteCount_3; }
	inline void set_visible_SpriteCount_3(int32_t value)
	{
		___visible_SpriteCount_3 = value;
	}

	inline static int32_t get_offset_of_visible_LinkCount_4() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_LinkCount_4)); }
	inline int32_t get_visible_LinkCount_4() const { return ___visible_LinkCount_4; }
	inline int32_t* get_address_of_visible_LinkCount_4() { return &___visible_LinkCount_4; }
	inline void set_visible_LinkCount_4(int32_t value)
	{
		___visible_LinkCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharIndex_8() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lastVisibleCharIndex_8)); }
	inline int32_t get_lastVisibleCharIndex_8() const { return ___lastVisibleCharIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharIndex_8() { return &___lastVisibleCharIndex_8; }
	inline void set_lastVisibleCharIndex_8(int32_t value)
	{
		___lastVisibleCharIndex_8 = value;
	}

	inline static int32_t get_offset_of_lineNumber_9() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineNumber_9)); }
	inline int32_t get_lineNumber_9() const { return ___lineNumber_9; }
	inline int32_t* get_address_of_lineNumber_9() { return &___lineNumber_9; }
	inline void set_lineNumber_9(int32_t value)
	{
		___lineNumber_9 = value;
	}

	inline static int32_t get_offset_of_maxCapHeight_10() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxCapHeight_10)); }
	inline float get_maxCapHeight_10() const { return ___maxCapHeight_10; }
	inline float* get_address_of_maxCapHeight_10() { return &___maxCapHeight_10; }
	inline void set_maxCapHeight_10(float value)
	{
		___maxCapHeight_10 = value;
	}

	inline static int32_t get_offset_of_maxAscender_11() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxAscender_11)); }
	inline float get_maxAscender_11() const { return ___maxAscender_11; }
	inline float* get_address_of_maxAscender_11() { return &___maxAscender_11; }
	inline void set_maxAscender_11(float value)
	{
		___maxAscender_11 = value;
	}

	inline static int32_t get_offset_of_maxDescender_12() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxDescender_12)); }
	inline float get_maxDescender_12() const { return ___maxDescender_12; }
	inline float* get_address_of_maxDescender_12() { return &___maxDescender_12; }
	inline void set_maxDescender_12(float value)
	{
		___maxDescender_12 = value;
	}

	inline static int32_t get_offset_of_maxLineAscender_13() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxLineAscender_13)); }
	inline float get_maxLineAscender_13() const { return ___maxLineAscender_13; }
	inline float* get_address_of_maxLineAscender_13() { return &___maxLineAscender_13; }
	inline void set_maxLineAscender_13(float value)
	{
		___maxLineAscender_13 = value;
	}

	inline static int32_t get_offset_of_maxLineDescender_14() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxLineDescender_14)); }
	inline float get_maxLineDescender_14() const { return ___maxLineDescender_14; }
	inline float* get_address_of_maxLineDescender_14() { return &___maxLineDescender_14; }
	inline void set_maxLineDescender_14(float value)
	{
		___maxLineDescender_14 = value;
	}

	inline static int32_t get_offset_of_previousLineAscender_15() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previousLineAscender_15)); }
	inline float get_previousLineAscender_15() const { return ___previousLineAscender_15; }
	inline float* get_address_of_previousLineAscender_15() { return &___previousLineAscender_15; }
	inline void set_previousLineAscender_15(float value)
	{
		___previousLineAscender_15 = value;
	}

	inline static int32_t get_offset_of_xAdvance_16() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___xAdvance_16)); }
	inline float get_xAdvance_16() const { return ___xAdvance_16; }
	inline float* get_address_of_xAdvance_16() { return &___xAdvance_16; }
	inline void set_xAdvance_16(float value)
	{
		___xAdvance_16 = value;
	}

	inline static int32_t get_offset_of_preferredWidth_17() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___preferredWidth_17)); }
	inline float get_preferredWidth_17() const { return ___preferredWidth_17; }
	inline float* get_address_of_preferredWidth_17() { return &___preferredWidth_17; }
	inline void set_preferredWidth_17(float value)
	{
		___preferredWidth_17 = value;
	}

	inline static int32_t get_offset_of_preferredHeight_18() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___preferredHeight_18)); }
	inline float get_preferredHeight_18() const { return ___preferredHeight_18; }
	inline float* get_address_of_preferredHeight_18() { return &___preferredHeight_18; }
	inline void set_preferredHeight_18(float value)
	{
		___preferredHeight_18 = value;
	}

	inline static int32_t get_offset_of_previousLineScale_19() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previousLineScale_19)); }
	inline float get_previousLineScale_19() const { return ___previousLineScale_19; }
	inline float* get_address_of_previousLineScale_19() { return &___previousLineScale_19; }
	inline void set_previousLineScale_19(float value)
	{
		___previousLineScale_19 = value;
	}

	inline static int32_t get_offset_of_wordCount_20() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___wordCount_20)); }
	inline int32_t get_wordCount_20() const { return ___wordCount_20; }
	inline int32_t* get_address_of_wordCount_20() { return &___wordCount_20; }
	inline void set_wordCount_20(int32_t value)
	{
		___wordCount_20 = value;
	}

	inline static int32_t get_offset_of_fontStyle_21() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontStyle_21)); }
	inline int32_t get_fontStyle_21() const { return ___fontStyle_21; }
	inline int32_t* get_address_of_fontStyle_21() { return &___fontStyle_21; }
	inline void set_fontStyle_21(int32_t value)
	{
		___fontStyle_21 = value;
	}

	inline static int32_t get_offset_of_fontScale_22() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontScale_22)); }
	inline float get_fontScale_22() const { return ___fontScale_22; }
	inline float* get_address_of_fontScale_22() { return &___fontScale_22; }
	inline void set_fontScale_22(float value)
	{
		___fontScale_22 = value;
	}

	inline static int32_t get_offset_of_fontScaleMultiplier_23() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontScaleMultiplier_23)); }
	inline float get_fontScaleMultiplier_23() const { return ___fontScaleMultiplier_23; }
	inline float* get_address_of_fontScaleMultiplier_23() { return &___fontScaleMultiplier_23; }
	inline void set_fontScaleMultiplier_23(float value)
	{
		___fontScaleMultiplier_23 = value;
	}

	inline static int32_t get_offset_of_currentFontSize_24() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentFontSize_24)); }
	inline float get_currentFontSize_24() const { return ___currentFontSize_24; }
	inline float* get_address_of_currentFontSize_24() { return &___currentFontSize_24; }
	inline void set_currentFontSize_24(float value)
	{
		___currentFontSize_24 = value;
	}

	inline static int32_t get_offset_of_baselineOffset_25() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___baselineOffset_25)); }
	inline float get_baselineOffset_25() const { return ___baselineOffset_25; }
	inline float* get_address_of_baselineOffset_25() { return &___baselineOffset_25; }
	inline void set_baselineOffset_25(float value)
	{
		___baselineOffset_25 = value;
	}

	inline static int32_t get_offset_of_lineOffset_26() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineOffset_26)); }
	inline float get_lineOffset_26() const { return ___lineOffset_26; }
	inline float* get_address_of_lineOffset_26() { return &___lineOffset_26; }
	inline void set_lineOffset_26(float value)
	{
		___lineOffset_26 = value;
	}

	inline static int32_t get_offset_of_textInfo_27() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___textInfo_27)); }
	inline TMP_TextInfo_t3598145122 * get_textInfo_27() const { return ___textInfo_27; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_textInfo_27() { return &___textInfo_27; }
	inline void set_textInfo_27(TMP_TextInfo_t3598145122 * value)
	{
		___textInfo_27 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_27), value);
	}

	inline static int32_t get_offset_of_lineInfo_28() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineInfo_28)); }
	inline TMP_LineInfo_t1079631636  get_lineInfo_28() const { return ___lineInfo_28; }
	inline TMP_LineInfo_t1079631636 * get_address_of_lineInfo_28() { return &___lineInfo_28; }
	inline void set_lineInfo_28(TMP_LineInfo_t1079631636  value)
	{
		___lineInfo_28 = value;
	}

	inline static int32_t get_offset_of_vertexColor_29() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___vertexColor_29)); }
	inline Color32_t2600501292  get_vertexColor_29() const { return ___vertexColor_29; }
	inline Color32_t2600501292 * get_address_of_vertexColor_29() { return &___vertexColor_29; }
	inline void set_vertexColor_29(Color32_t2600501292  value)
	{
		___vertexColor_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___underlineColor_30)); }
	inline Color32_t2600501292  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t2600501292 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t2600501292  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___strikethroughColor_31)); }
	inline Color32_t2600501292  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t2600501292 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t2600501292  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___highlightColor_32)); }
	inline Color32_t2600501292  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t2600501292 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t2600501292  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_basicStyleStack_33() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___basicStyleStack_33)); }
	inline TMP_BasicXmlTagStack_t2962628096  get_basicStyleStack_33() const { return ___basicStyleStack_33; }
	inline TMP_BasicXmlTagStack_t2962628096 * get_address_of_basicStyleStack_33() { return &___basicStyleStack_33; }
	inline void set_basicStyleStack_33(TMP_BasicXmlTagStack_t2962628096  value)
	{
		___basicStyleStack_33 = value;
	}

	inline static int32_t get_offset_of_colorStack_34() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___colorStack_34)); }
	inline TMP_XmlTagStack_1_t2164155836  get_colorStack_34() const { return ___colorStack_34; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_colorStack_34() { return &___colorStack_34; }
	inline void set_colorStack_34(TMP_XmlTagStack_1_t2164155836  value)
	{
		___colorStack_34 = value;
	}

	inline static int32_t get_offset_of_underlineColorStack_35() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___underlineColorStack_35)); }
	inline TMP_XmlTagStack_1_t2164155836  get_underlineColorStack_35() const { return ___underlineColorStack_35; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_underlineColorStack_35() { return &___underlineColorStack_35; }
	inline void set_underlineColorStack_35(TMP_XmlTagStack_1_t2164155836  value)
	{
		___underlineColorStack_35 = value;
	}

	inline static int32_t get_offset_of_strikethroughColorStack_36() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___strikethroughColorStack_36)); }
	inline TMP_XmlTagStack_1_t2164155836  get_strikethroughColorStack_36() const { return ___strikethroughColorStack_36; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_strikethroughColorStack_36() { return &___strikethroughColorStack_36; }
	inline void set_strikethroughColorStack_36(TMP_XmlTagStack_1_t2164155836  value)
	{
		___strikethroughColorStack_36 = value;
	}

	inline static int32_t get_offset_of_highlightColorStack_37() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___highlightColorStack_37)); }
	inline TMP_XmlTagStack_1_t2164155836  get_highlightColorStack_37() const { return ___highlightColorStack_37; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_highlightColorStack_37() { return &___highlightColorStack_37; }
	inline void set_highlightColorStack_37(TMP_XmlTagStack_1_t2164155836  value)
	{
		___highlightColorStack_37 = value;
	}

	inline static int32_t get_offset_of_sizeStack_38() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___sizeStack_38)); }
	inline TMP_XmlTagStack_1_t960921318  get_sizeStack_38() const { return ___sizeStack_38; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_sizeStack_38() { return &___sizeStack_38; }
	inline void set_sizeStack_38(TMP_XmlTagStack_1_t960921318  value)
	{
		___sizeStack_38 = value;
	}

	inline static int32_t get_offset_of_indentStack_39() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___indentStack_39)); }
	inline TMP_XmlTagStack_1_t960921318  get_indentStack_39() const { return ___indentStack_39; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_indentStack_39() { return &___indentStack_39; }
	inline void set_indentStack_39(TMP_XmlTagStack_1_t960921318  value)
	{
		___indentStack_39 = value;
	}

	inline static int32_t get_offset_of_fontWeightStack_40() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontWeightStack_40)); }
	inline TMP_XmlTagStack_1_t2514600297  get_fontWeightStack_40() const { return ___fontWeightStack_40; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_fontWeightStack_40() { return &___fontWeightStack_40; }
	inline void set_fontWeightStack_40(TMP_XmlTagStack_1_t2514600297  value)
	{
		___fontWeightStack_40 = value;
	}

	inline static int32_t get_offset_of_styleStack_41() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___styleStack_41)); }
	inline TMP_XmlTagStack_1_t2514600297  get_styleStack_41() const { return ___styleStack_41; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_styleStack_41() { return &___styleStack_41; }
	inline void set_styleStack_41(TMP_XmlTagStack_1_t2514600297  value)
	{
		___styleStack_41 = value;
	}

	inline static int32_t get_offset_of_baselineStack_42() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___baselineStack_42)); }
	inline TMP_XmlTagStack_1_t960921318  get_baselineStack_42() const { return ___baselineStack_42; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_baselineStack_42() { return &___baselineStack_42; }
	inline void set_baselineStack_42(TMP_XmlTagStack_1_t960921318  value)
	{
		___baselineStack_42 = value;
	}

	inline static int32_t get_offset_of_actionStack_43() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___actionStack_43)); }
	inline TMP_XmlTagStack_1_t2514600297  get_actionStack_43() const { return ___actionStack_43; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_actionStack_43() { return &___actionStack_43; }
	inline void set_actionStack_43(TMP_XmlTagStack_1_t2514600297  value)
	{
		___actionStack_43 = value;
	}

	inline static int32_t get_offset_of_materialReferenceStack_44() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___materialReferenceStack_44)); }
	inline TMP_XmlTagStack_1_t1515999176  get_materialReferenceStack_44() const { return ___materialReferenceStack_44; }
	inline TMP_XmlTagStack_1_t1515999176 * get_address_of_materialReferenceStack_44() { return &___materialReferenceStack_44; }
	inline void set_materialReferenceStack_44(TMP_XmlTagStack_1_t1515999176  value)
	{
		___materialReferenceStack_44 = value;
	}

	inline static int32_t get_offset_of_lineJustificationStack_45() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineJustificationStack_45)); }
	inline TMP_XmlTagStack_1_t3600445780  get_lineJustificationStack_45() const { return ___lineJustificationStack_45; }
	inline TMP_XmlTagStack_1_t3600445780 * get_address_of_lineJustificationStack_45() { return &___lineJustificationStack_45; }
	inline void set_lineJustificationStack_45(TMP_XmlTagStack_1_t3600445780  value)
	{
		___lineJustificationStack_45 = value;
	}

	inline static int32_t get_offset_of_spriteAnimationID_46() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___spriteAnimationID_46)); }
	inline int32_t get_spriteAnimationID_46() const { return ___spriteAnimationID_46; }
	inline int32_t* get_address_of_spriteAnimationID_46() { return &___spriteAnimationID_46; }
	inline void set_spriteAnimationID_46(int32_t value)
	{
		___spriteAnimationID_46 = value;
	}

	inline static int32_t get_offset_of_currentFontAsset_47() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentFontAsset_47)); }
	inline TMP_FontAsset_t364381626 * get_currentFontAsset_47() const { return ___currentFontAsset_47; }
	inline TMP_FontAsset_t364381626 ** get_address_of_currentFontAsset_47() { return &___currentFontAsset_47; }
	inline void set_currentFontAsset_47(TMP_FontAsset_t364381626 * value)
	{
		___currentFontAsset_47 = value;
		Il2CppCodeGenWriteBarrier((&___currentFontAsset_47), value);
	}

	inline static int32_t get_offset_of_currentSpriteAsset_48() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentSpriteAsset_48)); }
	inline TMP_SpriteAsset_t484820633 * get_currentSpriteAsset_48() const { return ___currentSpriteAsset_48; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_currentSpriteAsset_48() { return &___currentSpriteAsset_48; }
	inline void set_currentSpriteAsset_48(TMP_SpriteAsset_t484820633 * value)
	{
		___currentSpriteAsset_48 = value;
		Il2CppCodeGenWriteBarrier((&___currentSpriteAsset_48), value);
	}

	inline static int32_t get_offset_of_currentMaterial_49() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentMaterial_49)); }
	inline Material_t340375123 * get_currentMaterial_49() const { return ___currentMaterial_49; }
	inline Material_t340375123 ** get_address_of_currentMaterial_49() { return &___currentMaterial_49; }
	inline void set_currentMaterial_49(Material_t340375123 * value)
	{
		___currentMaterial_49 = value;
		Il2CppCodeGenWriteBarrier((&___currentMaterial_49), value);
	}

	inline static int32_t get_offset_of_currentMaterialIndex_50() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentMaterialIndex_50)); }
	inline int32_t get_currentMaterialIndex_50() const { return ___currentMaterialIndex_50; }
	inline int32_t* get_address_of_currentMaterialIndex_50() { return &___currentMaterialIndex_50; }
	inline void set_currentMaterialIndex_50(int32_t value)
	{
		___currentMaterialIndex_50 = value;
	}

	inline static int32_t get_offset_of_meshExtents_51() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___meshExtents_51)); }
	inline Extents_t3837212874  get_meshExtents_51() const { return ___meshExtents_51; }
	inline Extents_t3837212874 * get_address_of_meshExtents_51() { return &___meshExtents_51; }
	inline void set_meshExtents_51(Extents_t3837212874  value)
	{
		___meshExtents_51 = value;
	}

	inline static int32_t get_offset_of_tagNoParsing_52() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___tagNoParsing_52)); }
	inline bool get_tagNoParsing_52() const { return ___tagNoParsing_52; }
	inline bool* get_address_of_tagNoParsing_52() { return &___tagNoParsing_52; }
	inline void set_tagNoParsing_52(bool value)
	{
		___tagNoParsing_52 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.WordWrapState
struct WordWrapState_t341939652_marshaled_pinvoke
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	Color32_t2600501292  ___vertexColor_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t960921318  ___sizeStack_38;
	TMP_XmlTagStack_1_t960921318  ___indentStack_39;
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_40;
	TMP_XmlTagStack_1_t2514600297  ___styleStack_41;
	TMP_XmlTagStack_1_t960921318  ___baselineStack_42;
	TMP_XmlTagStack_1_t2514600297  ___actionStack_43;
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_44;
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_45;
	int32_t ___spriteAnimationID_46;
	TMP_FontAsset_t364381626 * ___currentFontAsset_47;
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_48;
	Material_t340375123 * ___currentMaterial_49;
	int32_t ___currentMaterialIndex_50;
	Extents_t3837212874  ___meshExtents_51;
	int32_t ___tagNoParsing_52;
};
// Native definition for COM marshalling of TMPro.WordWrapState
struct WordWrapState_t341939652_marshaled_com
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	Color32_t2600501292  ___vertexColor_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t960921318  ___sizeStack_38;
	TMP_XmlTagStack_1_t960921318  ___indentStack_39;
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_40;
	TMP_XmlTagStack_1_t2514600297  ___styleStack_41;
	TMP_XmlTagStack_1_t960921318  ___baselineStack_42;
	TMP_XmlTagStack_1_t2514600297  ___actionStack_43;
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_44;
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_45;
	int32_t ___spriteAnimationID_46;
	TMP_FontAsset_t364381626 * ___currentFontAsset_47;
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_48;
	Material_t340375123 * ___currentMaterial_49;
	int32_t ___currentMaterialIndex_50;
	Extents_t3837212874  ___meshExtents_51;
	int32_t ___tagNoParsing_52;
};
#endif // WORDWRAPSTATE_T341939652_H
#ifndef ONTRIGGER_T4184125570_H
#define ONTRIGGER_T4184125570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger/OnTrigger
struct  OnTrigger_t4184125570  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTRIGGER_T4184125570_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef PRESENTATION_T669212981_H
#define PRESENTATION_T669212981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Presentation
struct  Presentation_t669212981  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Presentation::left
	bool ___left_2;
	// System.Boolean Presentation::rotated
	bool ___rotated_3;
	// System.Boolean Presentation::move
	bool ___move_4;
	// System.Single Presentation::speed
	float ___speed_5;
	// System.Int32 Presentation::maxX
	int32_t ___maxX_6;

public:
	inline static int32_t get_offset_of_left_2() { return static_cast<int32_t>(offsetof(Presentation_t669212981, ___left_2)); }
	inline bool get_left_2() const { return ___left_2; }
	inline bool* get_address_of_left_2() { return &___left_2; }
	inline void set_left_2(bool value)
	{
		___left_2 = value;
	}

	inline static int32_t get_offset_of_rotated_3() { return static_cast<int32_t>(offsetof(Presentation_t669212981, ___rotated_3)); }
	inline bool get_rotated_3() const { return ___rotated_3; }
	inline bool* get_address_of_rotated_3() { return &___rotated_3; }
	inline void set_rotated_3(bool value)
	{
		___rotated_3 = value;
	}

	inline static int32_t get_offset_of_move_4() { return static_cast<int32_t>(offsetof(Presentation_t669212981, ___move_4)); }
	inline bool get_move_4() const { return ___move_4; }
	inline bool* get_address_of_move_4() { return &___move_4; }
	inline void set_move_4(bool value)
	{
		___move_4 = value;
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(Presentation_t669212981, ___speed_5)); }
	inline float get_speed_5() const { return ___speed_5; }
	inline float* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(float value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_maxX_6() { return static_cast<int32_t>(offsetof(Presentation_t669212981, ___maxX_6)); }
	inline int32_t get_maxX_6() const { return ___maxX_6; }
	inline int32_t* get_address_of_maxX_6() { return &___maxX_6; }
	inline void set_maxX_6(int32_t value)
	{
		___maxX_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESENTATION_T669212981_H
#ifndef ANALYTICSTRACKER_T731021378_H
#define ANALYTICSTRACKER_T731021378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker
struct  AnalyticsTracker_t731021378  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityEngine.Analytics.AnalyticsTracker::m_EventName
	String_t* ___m_EventName_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.AnalyticsTracker::m_Dict
	Dictionary_2_t2865362463 * ___m_Dict_3;
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker::m_PrevDictHash
	int32_t ___m_PrevDictHash_4;
	// UnityEngine.Analytics.TrackableProperty UnityEngine.Analytics.AnalyticsTracker::m_TrackableProperty
	TrackableProperty_t3943537984 * ___m_TrackableProperty_5;
	// UnityEngine.Analytics.AnalyticsTracker/Trigger UnityEngine.Analytics.AnalyticsTracker::m_Trigger
	int32_t ___m_Trigger_6;

public:
	inline static int32_t get_offset_of_m_EventName_2() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_EventName_2)); }
	inline String_t* get_m_EventName_2() const { return ___m_EventName_2; }
	inline String_t** get_address_of_m_EventName_2() { return &___m_EventName_2; }
	inline void set_m_EventName_2(String_t* value)
	{
		___m_EventName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventName_2), value);
	}

	inline static int32_t get_offset_of_m_Dict_3() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_Dict_3)); }
	inline Dictionary_2_t2865362463 * get_m_Dict_3() const { return ___m_Dict_3; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_Dict_3() { return &___m_Dict_3; }
	inline void set_m_Dict_3(Dictionary_2_t2865362463 * value)
	{
		___m_Dict_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dict_3), value);
	}

	inline static int32_t get_offset_of_m_PrevDictHash_4() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_PrevDictHash_4)); }
	inline int32_t get_m_PrevDictHash_4() const { return ___m_PrevDictHash_4; }
	inline int32_t* get_address_of_m_PrevDictHash_4() { return &___m_PrevDictHash_4; }
	inline void set_m_PrevDictHash_4(int32_t value)
	{
		___m_PrevDictHash_4 = value;
	}

	inline static int32_t get_offset_of_m_TrackableProperty_5() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_TrackableProperty_5)); }
	inline TrackableProperty_t3943537984 * get_m_TrackableProperty_5() const { return ___m_TrackableProperty_5; }
	inline TrackableProperty_t3943537984 ** get_address_of_m_TrackableProperty_5() { return &___m_TrackableProperty_5; }
	inline void set_m_TrackableProperty_5(TrackableProperty_t3943537984 * value)
	{
		___m_TrackableProperty_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackableProperty_5), value);
	}

	inline static int32_t get_offset_of_m_Trigger_6() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_Trigger_6)); }
	inline int32_t get_m_Trigger_6() const { return ___m_Trigger_6; }
	inline int32_t* get_address_of_m_Trigger_6() { return &___m_Trigger_6; }
	inline void set_m_Trigger_6(int32_t value)
	{
		___m_Trigger_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSTRACKER_T731021378_H
#ifndef ANALYTICSEVENTTRACKER_T2285229262_H
#define ANALYTICSEVENTTRACKER_T2285229262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTracker
struct  AnalyticsEventTracker_t2285229262  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Analytics.EventTrigger UnityEngine.Analytics.AnalyticsEventTracker::m_Trigger
	EventTrigger_t2527451695 * ___m_Trigger_2;
	// UnityEngine.Analytics.StandardEventPayload UnityEngine.Analytics.AnalyticsEventTracker::m_EventPayload
	StandardEventPayload_t1629891255 * ___m_EventPayload_3;

public:
	inline static int32_t get_offset_of_m_Trigger_2() { return static_cast<int32_t>(offsetof(AnalyticsEventTracker_t2285229262, ___m_Trigger_2)); }
	inline EventTrigger_t2527451695 * get_m_Trigger_2() const { return ___m_Trigger_2; }
	inline EventTrigger_t2527451695 ** get_address_of_m_Trigger_2() { return &___m_Trigger_2; }
	inline void set_m_Trigger_2(EventTrigger_t2527451695 * value)
	{
		___m_Trigger_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Trigger_2), value);
	}

	inline static int32_t get_offset_of_m_EventPayload_3() { return static_cast<int32_t>(offsetof(AnalyticsEventTracker_t2285229262, ___m_EventPayload_3)); }
	inline StandardEventPayload_t1629891255 * get_m_EventPayload_3() const { return ___m_EventPayload_3; }
	inline StandardEventPayload_t1629891255 ** get_address_of_m_EventPayload_3() { return &___m_EventPayload_3; }
	inline void set_m_EventPayload_3(StandardEventPayload_t1629891255 * value)
	{
		___m_EventPayload_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventPayload_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTTRACKER_T2285229262_H
#ifndef TEXTCONTAINER_T97923372_H
#define TEXTCONTAINER_T97923372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextContainer
struct  TextContainer_t97923372  : public UIBehaviour_t3495933518
{
public:
	// System.Boolean TMPro.TextContainer::m_hasChanged
	bool ___m_hasChanged_2;
	// UnityEngine.Vector2 TMPro.TextContainer::m_pivot
	Vector2_t2156229523  ___m_pivot_3;
	// TMPro.TextContainerAnchors TMPro.TextContainer::m_anchorPosition
	int32_t ___m_anchorPosition_4;
	// UnityEngine.Rect TMPro.TextContainer::m_rect
	Rect_t2360479859  ___m_rect_5;
	// System.Boolean TMPro.TextContainer::m_isDefaultWidth
	bool ___m_isDefaultWidth_6;
	// System.Boolean TMPro.TextContainer::m_isDefaultHeight
	bool ___m_isDefaultHeight_7;
	// System.Boolean TMPro.TextContainer::m_isAutoFitting
	bool ___m_isAutoFitting_8;
	// UnityEngine.Vector3[] TMPro.TextContainer::m_corners
	Vector3U5BU5D_t1718750761* ___m_corners_9;
	// UnityEngine.Vector3[] TMPro.TextContainer::m_worldCorners
	Vector3U5BU5D_t1718750761* ___m_worldCorners_10;
	// UnityEngine.Vector4 TMPro.TextContainer::m_margins
	Vector4_t3319028937  ___m_margins_11;
	// UnityEngine.RectTransform TMPro.TextContainer::m_rectTransform
	RectTransform_t3704657025 * ___m_rectTransform_12;
	// TMPro.TextMeshPro TMPro.TextContainer::m_textMeshPro
	TextMeshPro_t2393593166 * ___m_textMeshPro_14;

public:
	inline static int32_t get_offset_of_m_hasChanged_2() { return static_cast<int32_t>(offsetof(TextContainer_t97923372, ___m_hasChanged_2)); }
	inline bool get_m_hasChanged_2() const { return ___m_hasChanged_2; }
	inline bool* get_address_of_m_hasChanged_2() { return &___m_hasChanged_2; }
	inline void set_m_hasChanged_2(bool value)
	{
		___m_hasChanged_2 = value;
	}

	inline static int32_t get_offset_of_m_pivot_3() { return static_cast<int32_t>(offsetof(TextContainer_t97923372, ___m_pivot_3)); }
	inline Vector2_t2156229523  get_m_pivot_3() const { return ___m_pivot_3; }
	inline Vector2_t2156229523 * get_address_of_m_pivot_3() { return &___m_pivot_3; }
	inline void set_m_pivot_3(Vector2_t2156229523  value)
	{
		___m_pivot_3 = value;
	}

	inline static int32_t get_offset_of_m_anchorPosition_4() { return static_cast<int32_t>(offsetof(TextContainer_t97923372, ___m_anchorPosition_4)); }
	inline int32_t get_m_anchorPosition_4() const { return ___m_anchorPosition_4; }
	inline int32_t* get_address_of_m_anchorPosition_4() { return &___m_anchorPosition_4; }
	inline void set_m_anchorPosition_4(int32_t value)
	{
		___m_anchorPosition_4 = value;
	}

	inline static int32_t get_offset_of_m_rect_5() { return static_cast<int32_t>(offsetof(TextContainer_t97923372, ___m_rect_5)); }
	inline Rect_t2360479859  get_m_rect_5() const { return ___m_rect_5; }
	inline Rect_t2360479859 * get_address_of_m_rect_5() { return &___m_rect_5; }
	inline void set_m_rect_5(Rect_t2360479859  value)
	{
		___m_rect_5 = value;
	}

	inline static int32_t get_offset_of_m_isDefaultWidth_6() { return static_cast<int32_t>(offsetof(TextContainer_t97923372, ___m_isDefaultWidth_6)); }
	inline bool get_m_isDefaultWidth_6() const { return ___m_isDefaultWidth_6; }
	inline bool* get_address_of_m_isDefaultWidth_6() { return &___m_isDefaultWidth_6; }
	inline void set_m_isDefaultWidth_6(bool value)
	{
		___m_isDefaultWidth_6 = value;
	}

	inline static int32_t get_offset_of_m_isDefaultHeight_7() { return static_cast<int32_t>(offsetof(TextContainer_t97923372, ___m_isDefaultHeight_7)); }
	inline bool get_m_isDefaultHeight_7() const { return ___m_isDefaultHeight_7; }
	inline bool* get_address_of_m_isDefaultHeight_7() { return &___m_isDefaultHeight_7; }
	inline void set_m_isDefaultHeight_7(bool value)
	{
		___m_isDefaultHeight_7 = value;
	}

	inline static int32_t get_offset_of_m_isAutoFitting_8() { return static_cast<int32_t>(offsetof(TextContainer_t97923372, ___m_isAutoFitting_8)); }
	inline bool get_m_isAutoFitting_8() const { return ___m_isAutoFitting_8; }
	inline bool* get_address_of_m_isAutoFitting_8() { return &___m_isAutoFitting_8; }
	inline void set_m_isAutoFitting_8(bool value)
	{
		___m_isAutoFitting_8 = value;
	}

	inline static int32_t get_offset_of_m_corners_9() { return static_cast<int32_t>(offsetof(TextContainer_t97923372, ___m_corners_9)); }
	inline Vector3U5BU5D_t1718750761* get_m_corners_9() const { return ___m_corners_9; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_corners_9() { return &___m_corners_9; }
	inline void set_m_corners_9(Vector3U5BU5D_t1718750761* value)
	{
		___m_corners_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_corners_9), value);
	}

	inline static int32_t get_offset_of_m_worldCorners_10() { return static_cast<int32_t>(offsetof(TextContainer_t97923372, ___m_worldCorners_10)); }
	inline Vector3U5BU5D_t1718750761* get_m_worldCorners_10() const { return ___m_worldCorners_10; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_worldCorners_10() { return &___m_worldCorners_10; }
	inline void set_m_worldCorners_10(Vector3U5BU5D_t1718750761* value)
	{
		___m_worldCorners_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_worldCorners_10), value);
	}

	inline static int32_t get_offset_of_m_margins_11() { return static_cast<int32_t>(offsetof(TextContainer_t97923372, ___m_margins_11)); }
	inline Vector4_t3319028937  get_m_margins_11() const { return ___m_margins_11; }
	inline Vector4_t3319028937 * get_address_of_m_margins_11() { return &___m_margins_11; }
	inline void set_m_margins_11(Vector4_t3319028937  value)
	{
		___m_margins_11 = value;
	}

	inline static int32_t get_offset_of_m_rectTransform_12() { return static_cast<int32_t>(offsetof(TextContainer_t97923372, ___m_rectTransform_12)); }
	inline RectTransform_t3704657025 * get_m_rectTransform_12() const { return ___m_rectTransform_12; }
	inline RectTransform_t3704657025 ** get_address_of_m_rectTransform_12() { return &___m_rectTransform_12; }
	inline void set_m_rectTransform_12(RectTransform_t3704657025 * value)
	{
		___m_rectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_12), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_14() { return static_cast<int32_t>(offsetof(TextContainer_t97923372, ___m_textMeshPro_14)); }
	inline TextMeshPro_t2393593166 * get_m_textMeshPro_14() const { return ___m_textMeshPro_14; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_textMeshPro_14() { return &___m_textMeshPro_14; }
	inline void set_m_textMeshPro_14(TextMeshPro_t2393593166 * value)
	{
		___m_textMeshPro_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_14), value);
	}
};

struct TextContainer_t97923372_StaticFields
{
public:
	// UnityEngine.Vector2 TMPro.TextContainer::k_defaultSize
	Vector2_t2156229523  ___k_defaultSize_13;

public:
	inline static int32_t get_offset_of_k_defaultSize_13() { return static_cast<int32_t>(offsetof(TextContainer_t97923372_StaticFields, ___k_defaultSize_13)); }
	inline Vector2_t2156229523  get_k_defaultSize_13() const { return ___k_defaultSize_13; }
	inline Vector2_t2156229523 * get_address_of_k_defaultSize_13() { return &___k_defaultSize_13; }
	inline void set_k_defaultSize_13(Vector2_t2156229523  value)
	{
		___k_defaultSize_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTCONTAINER_T97923372_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_t2598313366 * ___m_CanvasRenderer_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_4)); }
	inline Material_t340375123 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t340375123 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t340375123 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_5)); }
	inline Color_t2555686324  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2555686324 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2555686324  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_7)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRenderer_8)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRenderer_8() const { return ___m_CanvasRenderer_8; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRenderer_8() { return &___m_CanvasRenderer_8; }
	inline void set_m_CanvasRenderer_8(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRenderer_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_9)); }
	inline Canvas_t3310196443 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t3310196443 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t340375123 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t340375123 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t3648964284 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t3648964284 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_20)); }
	inline Material_t340375123 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t340375123 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_21)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef TMP_TEXT_T2599618874_H
#define TMP_TEXT_T2599618874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text
struct  TMP_Text_t2599618874  : public MaskableGraphic_t3839221559
{
public:
	// System.String TMPro.TMP_Text::m_text
	String_t* ___m_text_28;
	// System.Boolean TMPro.TMP_Text::m_isRightToLeft
	bool ___m_isRightToLeft_29;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_fontAsset
	TMP_FontAsset_t364381626 * ___m_fontAsset_30;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_currentFontAsset
	TMP_FontAsset_t364381626 * ___m_currentFontAsset_31;
	// System.Boolean TMPro.TMP_Text::m_isSDFShader
	bool ___m_isSDFShader_32;
	// UnityEngine.Material TMPro.TMP_Text::m_sharedMaterial
	Material_t340375123 * ___m_sharedMaterial_33;
	// UnityEngine.Material TMPro.TMP_Text::m_currentMaterial
	Material_t340375123 * ___m_currentMaterial_34;
	// TMPro.MaterialReference[] TMPro.TMP_Text::m_materialReferences
	MaterialReferenceU5BU5D_t648826345* ___m_materialReferences_35;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_Text::m_materialReferenceIndexLookup
	Dictionary_2_t1839659084 * ___m_materialReferenceIndexLookup_36;
	// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference> TMPro.TMP_Text::m_materialReferenceStack
	TMP_XmlTagStack_1_t1515999176  ___m_materialReferenceStack_37;
	// System.Int32 TMPro.TMP_Text::m_currentMaterialIndex
	int32_t ___m_currentMaterialIndex_38;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontSharedMaterials
	MaterialU5BU5D_t561872642* ___m_fontSharedMaterials_39;
	// UnityEngine.Material TMPro.TMP_Text::m_fontMaterial
	Material_t340375123 * ___m_fontMaterial_40;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontMaterials
	MaterialU5BU5D_t561872642* ___m_fontMaterials_41;
	// System.Boolean TMPro.TMP_Text::m_isMaterialDirty
	bool ___m_isMaterialDirty_42;
	// UnityEngine.Color32 TMPro.TMP_Text::m_fontColor32
	Color32_t2600501292  ___m_fontColor32_43;
	// UnityEngine.Color TMPro.TMP_Text::m_fontColor
	Color_t2555686324  ___m_fontColor_44;
	// UnityEngine.Color32 TMPro.TMP_Text::m_underlineColor
	Color32_t2600501292  ___m_underlineColor_46;
	// UnityEngine.Color32 TMPro.TMP_Text::m_strikethroughColor
	Color32_t2600501292  ___m_strikethroughColor_47;
	// UnityEngine.Color32 TMPro.TMP_Text::m_highlightColor
	Color32_t2600501292  ___m_highlightColor_48;
	// System.Boolean TMPro.TMP_Text::m_enableVertexGradient
	bool ___m_enableVertexGradient_49;
	// TMPro.VertexGradient TMPro.TMP_Text::m_fontColorGradient
	VertexGradient_t345148380  ___m_fontColorGradient_50;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_fontColorGradientPreset
	TMP_ColorGradient_t3678055768 * ___m_fontColorGradientPreset_51;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_spriteAsset
	TMP_SpriteAsset_t484820633 * ___m_spriteAsset_52;
	// System.Boolean TMPro.TMP_Text::m_tintAllSprites
	bool ___m_tintAllSprites_53;
	// System.Boolean TMPro.TMP_Text::m_tintSprite
	bool ___m_tintSprite_54;
	// UnityEngine.Color32 TMPro.TMP_Text::m_spriteColor
	Color32_t2600501292  ___m_spriteColor_55;
	// System.Boolean TMPro.TMP_Text::m_overrideHtmlColors
	bool ___m_overrideHtmlColors_56;
	// UnityEngine.Color32 TMPro.TMP_Text::m_faceColor
	Color32_t2600501292  ___m_faceColor_57;
	// UnityEngine.Color32 TMPro.TMP_Text::m_outlineColor
	Color32_t2600501292  ___m_outlineColor_58;
	// System.Single TMPro.TMP_Text::m_outlineWidth
	float ___m_outlineWidth_59;
	// System.Single TMPro.TMP_Text::m_fontSize
	float ___m_fontSize_60;
	// System.Single TMPro.TMP_Text::m_currentFontSize
	float ___m_currentFontSize_61;
	// System.Single TMPro.TMP_Text::m_fontSizeBase
	float ___m_fontSizeBase_62;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_sizeStack
	TMP_XmlTagStack_1_t960921318  ___m_sizeStack_63;
	// System.Int32 TMPro.TMP_Text::m_fontWeight
	int32_t ___m_fontWeight_64;
	// System.Int32 TMPro.TMP_Text::m_fontWeightInternal
	int32_t ___m_fontWeightInternal_65;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_fontWeightStack
	TMP_XmlTagStack_1_t2514600297  ___m_fontWeightStack_66;
	// System.Boolean TMPro.TMP_Text::m_enableAutoSizing
	bool ___m_enableAutoSizing_67;
	// System.Single TMPro.TMP_Text::m_maxFontSize
	float ___m_maxFontSize_68;
	// System.Single TMPro.TMP_Text::m_minFontSize
	float ___m_minFontSize_69;
	// System.Single TMPro.TMP_Text::m_fontSizeMin
	float ___m_fontSizeMin_70;
	// System.Single TMPro.TMP_Text::m_fontSizeMax
	float ___m_fontSizeMax_71;
	// TMPro.FontStyles TMPro.TMP_Text::m_fontStyle
	int32_t ___m_fontStyle_72;
	// TMPro.FontStyles TMPro.TMP_Text::m_style
	int32_t ___m_style_73;
	// TMPro.TMP_BasicXmlTagStack TMPro.TMP_Text::m_fontStyleStack
	TMP_BasicXmlTagStack_t2962628096  ___m_fontStyleStack_74;
	// System.Boolean TMPro.TMP_Text::m_isUsingBold
	bool ___m_isUsingBold_75;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_textAlignment
	int32_t ___m_textAlignment_76;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_lineJustification
	int32_t ___m_lineJustification_77;
	// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions> TMPro.TMP_Text::m_lineJustificationStack
	TMP_XmlTagStack_1_t3600445780  ___m_lineJustificationStack_78;
	// UnityEngine.Vector3[] TMPro.TMP_Text::m_textContainerLocalCorners
	Vector3U5BU5D_t1718750761* ___m_textContainerLocalCorners_79;
	// System.Boolean TMPro.TMP_Text::m_isAlignmentEnumConverted
	bool ___m_isAlignmentEnumConverted_80;
	// System.Single TMPro.TMP_Text::m_characterSpacing
	float ___m_characterSpacing_81;
	// System.Single TMPro.TMP_Text::m_cSpacing
	float ___m_cSpacing_82;
	// System.Single TMPro.TMP_Text::m_monoSpacing
	float ___m_monoSpacing_83;
	// System.Single TMPro.TMP_Text::m_wordSpacing
	float ___m_wordSpacing_84;
	// System.Single TMPro.TMP_Text::m_lineSpacing
	float ___m_lineSpacing_85;
	// System.Single TMPro.TMP_Text::m_lineSpacingDelta
	float ___m_lineSpacingDelta_86;
	// System.Single TMPro.TMP_Text::m_lineHeight
	float ___m_lineHeight_87;
	// System.Single TMPro.TMP_Text::m_lineSpacingMax
	float ___m_lineSpacingMax_88;
	// System.Single TMPro.TMP_Text::m_paragraphSpacing
	float ___m_paragraphSpacing_89;
	// System.Single TMPro.TMP_Text::m_charWidthMaxAdj
	float ___m_charWidthMaxAdj_90;
	// System.Single TMPro.TMP_Text::m_charWidthAdjDelta
	float ___m_charWidthAdjDelta_91;
	// System.Boolean TMPro.TMP_Text::m_enableWordWrapping
	bool ___m_enableWordWrapping_92;
	// System.Boolean TMPro.TMP_Text::m_isCharacterWrappingEnabled
	bool ___m_isCharacterWrappingEnabled_93;
	// System.Boolean TMPro.TMP_Text::m_isNonBreakingSpace
	bool ___m_isNonBreakingSpace_94;
	// System.Boolean TMPro.TMP_Text::m_isIgnoringAlignment
	bool ___m_isIgnoringAlignment_95;
	// System.Single TMPro.TMP_Text::m_wordWrappingRatios
	float ___m_wordWrappingRatios_96;
	// TMPro.TextOverflowModes TMPro.TMP_Text::m_overflowMode
	int32_t ___m_overflowMode_97;
	// System.Int32 TMPro.TMP_Text::m_firstOverflowCharacterIndex
	int32_t ___m_firstOverflowCharacterIndex_98;
	// TMPro.TMP_Text TMPro.TMP_Text::m_linkedTextComponent
	TMP_Text_t2599618874 * ___m_linkedTextComponent_99;
	// System.Boolean TMPro.TMP_Text::m_isLinkedTextComponent
	bool ___m_isLinkedTextComponent_100;
	// System.Boolean TMPro.TMP_Text::m_isTextTruncated
	bool ___m_isTextTruncated_101;
	// System.Boolean TMPro.TMP_Text::m_enableKerning
	bool ___m_enableKerning_102;
	// System.Boolean TMPro.TMP_Text::m_enableExtraPadding
	bool ___m_enableExtraPadding_103;
	// System.Boolean TMPro.TMP_Text::checkPaddingRequired
	bool ___checkPaddingRequired_104;
	// System.Boolean TMPro.TMP_Text::m_isRichText
	bool ___m_isRichText_105;
	// System.Boolean TMPro.TMP_Text::m_parseCtrlCharacters
	bool ___m_parseCtrlCharacters_106;
	// System.Boolean TMPro.TMP_Text::m_isOverlay
	bool ___m_isOverlay_107;
	// System.Boolean TMPro.TMP_Text::m_isOrthographic
	bool ___m_isOrthographic_108;
	// System.Boolean TMPro.TMP_Text::m_isCullingEnabled
	bool ___m_isCullingEnabled_109;
	// System.Boolean TMPro.TMP_Text::m_ignoreRectMaskCulling
	bool ___m_ignoreRectMaskCulling_110;
	// System.Boolean TMPro.TMP_Text::m_ignoreCulling
	bool ___m_ignoreCulling_111;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_horizontalMapping
	int32_t ___m_horizontalMapping_112;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_verticalMapping
	int32_t ___m_verticalMapping_113;
	// System.Single TMPro.TMP_Text::m_uvLineOffset
	float ___m_uvLineOffset_114;
	// TMPro.TextRenderFlags TMPro.TMP_Text::m_renderMode
	int32_t ___m_renderMode_115;
	// TMPro.VertexSortingOrder TMPro.TMP_Text::m_geometrySortingOrder
	int32_t ___m_geometrySortingOrder_116;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacter
	int32_t ___m_firstVisibleCharacter_117;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleCharacters
	int32_t ___m_maxVisibleCharacters_118;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleWords
	int32_t ___m_maxVisibleWords_119;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleLines
	int32_t ___m_maxVisibleLines_120;
	// System.Boolean TMPro.TMP_Text::m_useMaxVisibleDescender
	bool ___m_useMaxVisibleDescender_121;
	// System.Int32 TMPro.TMP_Text::m_pageToDisplay
	int32_t ___m_pageToDisplay_122;
	// System.Boolean TMPro.TMP_Text::m_isNewPage
	bool ___m_isNewPage_123;
	// UnityEngine.Vector4 TMPro.TMP_Text::m_margin
	Vector4_t3319028937  ___m_margin_124;
	// System.Single TMPro.TMP_Text::m_marginLeft
	float ___m_marginLeft_125;
	// System.Single TMPro.TMP_Text::m_marginRight
	float ___m_marginRight_126;
	// System.Single TMPro.TMP_Text::m_marginWidth
	float ___m_marginWidth_127;
	// System.Single TMPro.TMP_Text::m_marginHeight
	float ___m_marginHeight_128;
	// System.Single TMPro.TMP_Text::m_width
	float ___m_width_129;
	// TMPro.TMP_TextInfo TMPro.TMP_Text::m_textInfo
	TMP_TextInfo_t3598145122 * ___m_textInfo_130;
	// System.Boolean TMPro.TMP_Text::m_havePropertiesChanged
	bool ___m_havePropertiesChanged_131;
	// System.Boolean TMPro.TMP_Text::m_isUsingLegacyAnimationComponent
	bool ___m_isUsingLegacyAnimationComponent_132;
	// UnityEngine.Transform TMPro.TMP_Text::m_transform
	Transform_t3600365921 * ___m_transform_133;
	// UnityEngine.RectTransform TMPro.TMP_Text::m_rectTransform
	RectTransform_t3704657025 * ___m_rectTransform_134;
	// System.Boolean TMPro.TMP_Text::<autoSizeTextContainer>k__BackingField
	bool ___U3CautoSizeTextContainerU3Ek__BackingField_135;
	// System.Boolean TMPro.TMP_Text::m_autoSizeTextContainer
	bool ___m_autoSizeTextContainer_136;
	// UnityEngine.Mesh TMPro.TMP_Text::m_mesh
	Mesh_t3648964284 * ___m_mesh_137;
	// System.Boolean TMPro.TMP_Text::m_isVolumetricText
	bool ___m_isVolumetricText_138;
	// TMPro.TMP_SpriteAnimator TMPro.TMP_Text::m_spriteAnimator
	TMP_SpriteAnimator_t2836635477 * ___m_spriteAnimator_139;
	// System.Single TMPro.TMP_Text::m_flexibleHeight
	float ___m_flexibleHeight_140;
	// System.Single TMPro.TMP_Text::m_flexibleWidth
	float ___m_flexibleWidth_141;
	// System.Single TMPro.TMP_Text::m_minWidth
	float ___m_minWidth_142;
	// System.Single TMPro.TMP_Text::m_minHeight
	float ___m_minHeight_143;
	// System.Single TMPro.TMP_Text::m_maxWidth
	float ___m_maxWidth_144;
	// System.Single TMPro.TMP_Text::m_maxHeight
	float ___m_maxHeight_145;
	// UnityEngine.UI.LayoutElement TMPro.TMP_Text::m_LayoutElement
	LayoutElement_t1785403678 * ___m_LayoutElement_146;
	// System.Single TMPro.TMP_Text::m_preferredWidth
	float ___m_preferredWidth_147;
	// System.Single TMPro.TMP_Text::m_renderedWidth
	float ___m_renderedWidth_148;
	// System.Boolean TMPro.TMP_Text::m_isPreferredWidthDirty
	bool ___m_isPreferredWidthDirty_149;
	// System.Single TMPro.TMP_Text::m_preferredHeight
	float ___m_preferredHeight_150;
	// System.Single TMPro.TMP_Text::m_renderedHeight
	float ___m_renderedHeight_151;
	// System.Boolean TMPro.TMP_Text::m_isPreferredHeightDirty
	bool ___m_isPreferredHeightDirty_152;
	// System.Boolean TMPro.TMP_Text::m_isCalculatingPreferredValues
	bool ___m_isCalculatingPreferredValues_153;
	// System.Int32 TMPro.TMP_Text::m_recursiveCount
	int32_t ___m_recursiveCount_154;
	// System.Int32 TMPro.TMP_Text::m_layoutPriority
	int32_t ___m_layoutPriority_155;
	// System.Boolean TMPro.TMP_Text::m_isCalculateSizeRequired
	bool ___m_isCalculateSizeRequired_156;
	// System.Boolean TMPro.TMP_Text::m_isLayoutDirty
	bool ___m_isLayoutDirty_157;
	// System.Boolean TMPro.TMP_Text::m_verticesAlreadyDirty
	bool ___m_verticesAlreadyDirty_158;
	// System.Boolean TMPro.TMP_Text::m_layoutAlreadyDirty
	bool ___m_layoutAlreadyDirty_159;
	// System.Boolean TMPro.TMP_Text::m_isAwake
	bool ___m_isAwake_160;
	// System.Boolean TMPro.TMP_Text::m_isInputParsingRequired
	bool ___m_isInputParsingRequired_161;
	// TMPro.TMP_Text/TextInputSources TMPro.TMP_Text::m_inputSource
	int32_t ___m_inputSource_162;
	// System.String TMPro.TMP_Text::old_text
	String_t* ___old_text_163;
	// System.Single TMPro.TMP_Text::m_fontScale
	float ___m_fontScale_164;
	// System.Single TMPro.TMP_Text::m_fontScaleMultiplier
	float ___m_fontScaleMultiplier_165;
	// System.Char[] TMPro.TMP_Text::m_htmlTag
	CharU5BU5D_t3528271667* ___m_htmlTag_166;
	// TMPro.XML_TagAttribute[] TMPro.TMP_Text::m_xmlAttribute
	XML_TagAttributeU5BU5D_t284240280* ___m_xmlAttribute_167;
	// System.Single[] TMPro.TMP_Text::m_attributeParameterValues
	SingleU5BU5D_t1444911251* ___m_attributeParameterValues_168;
	// System.Single TMPro.TMP_Text::tag_LineIndent
	float ___tag_LineIndent_169;
	// System.Single TMPro.TMP_Text::tag_Indent
	float ___tag_Indent_170;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_indentStack
	TMP_XmlTagStack_1_t960921318  ___m_indentStack_171;
	// System.Boolean TMPro.TMP_Text::tag_NoParsing
	bool ___tag_NoParsing_172;
	// System.Boolean TMPro.TMP_Text::m_isParsingText
	bool ___m_isParsingText_173;
	// UnityEngine.Matrix4x4 TMPro.TMP_Text::m_FXMatrix
	Matrix4x4_t1817901843  ___m_FXMatrix_174;
	// System.Boolean TMPro.TMP_Text::m_isFXMatrixSet
	bool ___m_isFXMatrixSet_175;
	// System.Int32[] TMPro.TMP_Text::m_char_buffer
	Int32U5BU5D_t385246372* ___m_char_buffer_176;
	// TMPro.TMP_CharacterInfo[] TMPro.TMP_Text::m_internalCharacterInfo
	TMP_CharacterInfoU5BU5D_t1930184704* ___m_internalCharacterInfo_177;
	// System.Char[] TMPro.TMP_Text::m_input_CharArray
	CharU5BU5D_t3528271667* ___m_input_CharArray_178;
	// System.Int32 TMPro.TMP_Text::m_charArray_Length
	int32_t ___m_charArray_Length_179;
	// System.Int32 TMPro.TMP_Text::m_totalCharacterCount
	int32_t ___m_totalCharacterCount_180;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedWordWrapState
	WordWrapState_t341939652  ___m_SavedWordWrapState_181;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedLineState
	WordWrapState_t341939652  ___m_SavedLineState_182;
	// System.Int32 TMPro.TMP_Text::m_characterCount
	int32_t ___m_characterCount_183;
	// System.Int32 TMPro.TMP_Text::m_firstCharacterOfLine
	int32_t ___m_firstCharacterOfLine_184;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacterOfLine
	int32_t ___m_firstVisibleCharacterOfLine_185;
	// System.Int32 TMPro.TMP_Text::m_lastCharacterOfLine
	int32_t ___m_lastCharacterOfLine_186;
	// System.Int32 TMPro.TMP_Text::m_lastVisibleCharacterOfLine
	int32_t ___m_lastVisibleCharacterOfLine_187;
	// System.Int32 TMPro.TMP_Text::m_lineNumber
	int32_t ___m_lineNumber_188;
	// System.Int32 TMPro.TMP_Text::m_lineVisibleCharacterCount
	int32_t ___m_lineVisibleCharacterCount_189;
	// System.Int32 TMPro.TMP_Text::m_pageNumber
	int32_t ___m_pageNumber_190;
	// System.Single TMPro.TMP_Text::m_maxAscender
	float ___m_maxAscender_191;
	// System.Single TMPro.TMP_Text::m_maxCapHeight
	float ___m_maxCapHeight_192;
	// System.Single TMPro.TMP_Text::m_maxDescender
	float ___m_maxDescender_193;
	// System.Single TMPro.TMP_Text::m_maxLineAscender
	float ___m_maxLineAscender_194;
	// System.Single TMPro.TMP_Text::m_maxLineDescender
	float ___m_maxLineDescender_195;
	// System.Single TMPro.TMP_Text::m_startOfLineAscender
	float ___m_startOfLineAscender_196;
	// System.Single TMPro.TMP_Text::m_lineOffset
	float ___m_lineOffset_197;
	// TMPro.Extents TMPro.TMP_Text::m_meshExtents
	Extents_t3837212874  ___m_meshExtents_198;
	// UnityEngine.Color32 TMPro.TMP_Text::m_htmlColor
	Color32_t2600501292  ___m_htmlColor_199;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_colorStack
	TMP_XmlTagStack_1_t2164155836  ___m_colorStack_200;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_underlineColorStack
	TMP_XmlTagStack_1_t2164155836  ___m_underlineColorStack_201;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_strikethroughColorStack
	TMP_XmlTagStack_1_t2164155836  ___m_strikethroughColorStack_202;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_highlightColorStack
	TMP_XmlTagStack_1_t2164155836  ___m_highlightColorStack_203;
	// System.Single TMPro.TMP_Text::m_tabSpacing
	float ___m_tabSpacing_204;
	// System.Single TMPro.TMP_Text::m_spacing
	float ___m_spacing_205;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_styleStack
	TMP_XmlTagStack_1_t2514600297  ___m_styleStack_206;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_actionStack
	TMP_XmlTagStack_1_t2514600297  ___m_actionStack_207;
	// System.Single TMPro.TMP_Text::m_padding
	float ___m_padding_208;
	// System.Single TMPro.TMP_Text::m_baselineOffset
	float ___m_baselineOffset_209;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_baselineOffsetStack
	TMP_XmlTagStack_1_t960921318  ___m_baselineOffsetStack_210;
	// System.Single TMPro.TMP_Text::m_xAdvance
	float ___m_xAdvance_211;
	// TMPro.TMP_TextElementType TMPro.TMP_Text::m_textElementType
	int32_t ___m_textElementType_212;
	// TMPro.TMP_TextElement TMPro.TMP_Text::m_cached_TextElement
	TMP_TextElement_t129727469 * ___m_cached_TextElement_213;
	// TMPro.TMP_Glyph TMPro.TMP_Text::m_cached_Underline_GlyphInfo
	TMP_Glyph_t581847833 * ___m_cached_Underline_GlyphInfo_214;
	// TMPro.TMP_Glyph TMPro.TMP_Text::m_cached_Ellipsis_GlyphInfo
	TMP_Glyph_t581847833 * ___m_cached_Ellipsis_GlyphInfo_215;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_defaultSpriteAsset
	TMP_SpriteAsset_t484820633 * ___m_defaultSpriteAsset_216;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_currentSpriteAsset
	TMP_SpriteAsset_t484820633 * ___m_currentSpriteAsset_217;
	// System.Int32 TMPro.TMP_Text::m_spriteCount
	int32_t ___m_spriteCount_218;
	// System.Int32 TMPro.TMP_Text::m_spriteIndex
	int32_t ___m_spriteIndex_219;
	// TMPro.InlineGraphicManager TMPro.TMP_Text::m_inlineGraphics
	InlineGraphicManager_t2871008645 * ___m_inlineGraphics_220;
	// System.Int32 TMPro.TMP_Text::m_spriteAnimationID
	int32_t ___m_spriteAnimationID_221;
	// System.Boolean TMPro.TMP_Text::m_ignoreActiveState
	bool ___m_ignoreActiveState_222;
	// System.Single[] TMPro.TMP_Text::k_Power
	SingleU5BU5D_t1444911251* ___k_Power_223;

public:
	inline static int32_t get_offset_of_m_text_28() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_text_28)); }
	inline String_t* get_m_text_28() const { return ___m_text_28; }
	inline String_t** get_address_of_m_text_28() { return &___m_text_28; }
	inline void set_m_text_28(String_t* value)
	{
		___m_text_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_text_28), value);
	}

	inline static int32_t get_offset_of_m_isRightToLeft_29() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isRightToLeft_29)); }
	inline bool get_m_isRightToLeft_29() const { return ___m_isRightToLeft_29; }
	inline bool* get_address_of_m_isRightToLeft_29() { return &___m_isRightToLeft_29; }
	inline void set_m_isRightToLeft_29(bool value)
	{
		___m_isRightToLeft_29 = value;
	}

	inline static int32_t get_offset_of_m_fontAsset_30() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontAsset_30)); }
	inline TMP_FontAsset_t364381626 * get_m_fontAsset_30() const { return ___m_fontAsset_30; }
	inline TMP_FontAsset_t364381626 ** get_address_of_m_fontAsset_30() { return &___m_fontAsset_30; }
	inline void set_m_fontAsset_30(TMP_FontAsset_t364381626 * value)
	{
		___m_fontAsset_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontAsset_30), value);
	}

	inline static int32_t get_offset_of_m_currentFontAsset_31() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentFontAsset_31)); }
	inline TMP_FontAsset_t364381626 * get_m_currentFontAsset_31() const { return ___m_currentFontAsset_31; }
	inline TMP_FontAsset_t364381626 ** get_address_of_m_currentFontAsset_31() { return &___m_currentFontAsset_31; }
	inline void set_m_currentFontAsset_31(TMP_FontAsset_t364381626 * value)
	{
		___m_currentFontAsset_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentFontAsset_31), value);
	}

	inline static int32_t get_offset_of_m_isSDFShader_32() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isSDFShader_32)); }
	inline bool get_m_isSDFShader_32() const { return ___m_isSDFShader_32; }
	inline bool* get_address_of_m_isSDFShader_32() { return &___m_isSDFShader_32; }
	inline void set_m_isSDFShader_32(bool value)
	{
		___m_isSDFShader_32 = value;
	}

	inline static int32_t get_offset_of_m_sharedMaterial_33() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_sharedMaterial_33)); }
	inline Material_t340375123 * get_m_sharedMaterial_33() const { return ___m_sharedMaterial_33; }
	inline Material_t340375123 ** get_address_of_m_sharedMaterial_33() { return &___m_sharedMaterial_33; }
	inline void set_m_sharedMaterial_33(Material_t340375123 * value)
	{
		___m_sharedMaterial_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_sharedMaterial_33), value);
	}

	inline static int32_t get_offset_of_m_currentMaterial_34() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentMaterial_34)); }
	inline Material_t340375123 * get_m_currentMaterial_34() const { return ___m_currentMaterial_34; }
	inline Material_t340375123 ** get_address_of_m_currentMaterial_34() { return &___m_currentMaterial_34; }
	inline void set_m_currentMaterial_34(Material_t340375123 * value)
	{
		___m_currentMaterial_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentMaterial_34), value);
	}

	inline static int32_t get_offset_of_m_materialReferences_35() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_materialReferences_35)); }
	inline MaterialReferenceU5BU5D_t648826345* get_m_materialReferences_35() const { return ___m_materialReferences_35; }
	inline MaterialReferenceU5BU5D_t648826345** get_address_of_m_materialReferences_35() { return &___m_materialReferences_35; }
	inline void set_m_materialReferences_35(MaterialReferenceU5BU5D_t648826345* value)
	{
		___m_materialReferences_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReferences_35), value);
	}

	inline static int32_t get_offset_of_m_materialReferenceIndexLookup_36() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_materialReferenceIndexLookup_36)); }
	inline Dictionary_2_t1839659084 * get_m_materialReferenceIndexLookup_36() const { return ___m_materialReferenceIndexLookup_36; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_materialReferenceIndexLookup_36() { return &___m_materialReferenceIndexLookup_36; }
	inline void set_m_materialReferenceIndexLookup_36(Dictionary_2_t1839659084 * value)
	{
		___m_materialReferenceIndexLookup_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReferenceIndexLookup_36), value);
	}

	inline static int32_t get_offset_of_m_materialReferenceStack_37() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_materialReferenceStack_37)); }
	inline TMP_XmlTagStack_1_t1515999176  get_m_materialReferenceStack_37() const { return ___m_materialReferenceStack_37; }
	inline TMP_XmlTagStack_1_t1515999176 * get_address_of_m_materialReferenceStack_37() { return &___m_materialReferenceStack_37; }
	inline void set_m_materialReferenceStack_37(TMP_XmlTagStack_1_t1515999176  value)
	{
		___m_materialReferenceStack_37 = value;
	}

	inline static int32_t get_offset_of_m_currentMaterialIndex_38() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentMaterialIndex_38)); }
	inline int32_t get_m_currentMaterialIndex_38() const { return ___m_currentMaterialIndex_38; }
	inline int32_t* get_address_of_m_currentMaterialIndex_38() { return &___m_currentMaterialIndex_38; }
	inline void set_m_currentMaterialIndex_38(int32_t value)
	{
		___m_currentMaterialIndex_38 = value;
	}

	inline static int32_t get_offset_of_m_fontSharedMaterials_39() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSharedMaterials_39)); }
	inline MaterialU5BU5D_t561872642* get_m_fontSharedMaterials_39() const { return ___m_fontSharedMaterials_39; }
	inline MaterialU5BU5D_t561872642** get_address_of_m_fontSharedMaterials_39() { return &___m_fontSharedMaterials_39; }
	inline void set_m_fontSharedMaterials_39(MaterialU5BU5D_t561872642* value)
	{
		___m_fontSharedMaterials_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontSharedMaterials_39), value);
	}

	inline static int32_t get_offset_of_m_fontMaterial_40() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontMaterial_40)); }
	inline Material_t340375123 * get_m_fontMaterial_40() const { return ___m_fontMaterial_40; }
	inline Material_t340375123 ** get_address_of_m_fontMaterial_40() { return &___m_fontMaterial_40; }
	inline void set_m_fontMaterial_40(Material_t340375123 * value)
	{
		___m_fontMaterial_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontMaterial_40), value);
	}

	inline static int32_t get_offset_of_m_fontMaterials_41() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontMaterials_41)); }
	inline MaterialU5BU5D_t561872642* get_m_fontMaterials_41() const { return ___m_fontMaterials_41; }
	inline MaterialU5BU5D_t561872642** get_address_of_m_fontMaterials_41() { return &___m_fontMaterials_41; }
	inline void set_m_fontMaterials_41(MaterialU5BU5D_t561872642* value)
	{
		___m_fontMaterials_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontMaterials_41), value);
	}

	inline static int32_t get_offset_of_m_isMaterialDirty_42() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isMaterialDirty_42)); }
	inline bool get_m_isMaterialDirty_42() const { return ___m_isMaterialDirty_42; }
	inline bool* get_address_of_m_isMaterialDirty_42() { return &___m_isMaterialDirty_42; }
	inline void set_m_isMaterialDirty_42(bool value)
	{
		___m_isMaterialDirty_42 = value;
	}

	inline static int32_t get_offset_of_m_fontColor32_43() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontColor32_43)); }
	inline Color32_t2600501292  get_m_fontColor32_43() const { return ___m_fontColor32_43; }
	inline Color32_t2600501292 * get_address_of_m_fontColor32_43() { return &___m_fontColor32_43; }
	inline void set_m_fontColor32_43(Color32_t2600501292  value)
	{
		___m_fontColor32_43 = value;
	}

	inline static int32_t get_offset_of_m_fontColor_44() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontColor_44)); }
	inline Color_t2555686324  get_m_fontColor_44() const { return ___m_fontColor_44; }
	inline Color_t2555686324 * get_address_of_m_fontColor_44() { return &___m_fontColor_44; }
	inline void set_m_fontColor_44(Color_t2555686324  value)
	{
		___m_fontColor_44 = value;
	}

	inline static int32_t get_offset_of_m_underlineColor_46() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_underlineColor_46)); }
	inline Color32_t2600501292  get_m_underlineColor_46() const { return ___m_underlineColor_46; }
	inline Color32_t2600501292 * get_address_of_m_underlineColor_46() { return &___m_underlineColor_46; }
	inline void set_m_underlineColor_46(Color32_t2600501292  value)
	{
		___m_underlineColor_46 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColor_47() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_strikethroughColor_47)); }
	inline Color32_t2600501292  get_m_strikethroughColor_47() const { return ___m_strikethroughColor_47; }
	inline Color32_t2600501292 * get_address_of_m_strikethroughColor_47() { return &___m_strikethroughColor_47; }
	inline void set_m_strikethroughColor_47(Color32_t2600501292  value)
	{
		___m_strikethroughColor_47 = value;
	}

	inline static int32_t get_offset_of_m_highlightColor_48() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_highlightColor_48)); }
	inline Color32_t2600501292  get_m_highlightColor_48() const { return ___m_highlightColor_48; }
	inline Color32_t2600501292 * get_address_of_m_highlightColor_48() { return &___m_highlightColor_48; }
	inline void set_m_highlightColor_48(Color32_t2600501292  value)
	{
		___m_highlightColor_48 = value;
	}

	inline static int32_t get_offset_of_m_enableVertexGradient_49() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableVertexGradient_49)); }
	inline bool get_m_enableVertexGradient_49() const { return ___m_enableVertexGradient_49; }
	inline bool* get_address_of_m_enableVertexGradient_49() { return &___m_enableVertexGradient_49; }
	inline void set_m_enableVertexGradient_49(bool value)
	{
		___m_enableVertexGradient_49 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradient_50() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontColorGradient_50)); }
	inline VertexGradient_t345148380  get_m_fontColorGradient_50() const { return ___m_fontColorGradient_50; }
	inline VertexGradient_t345148380 * get_address_of_m_fontColorGradient_50() { return &___m_fontColorGradient_50; }
	inline void set_m_fontColorGradient_50(VertexGradient_t345148380  value)
	{
		___m_fontColorGradient_50 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradientPreset_51() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontColorGradientPreset_51)); }
	inline TMP_ColorGradient_t3678055768 * get_m_fontColorGradientPreset_51() const { return ___m_fontColorGradientPreset_51; }
	inline TMP_ColorGradient_t3678055768 ** get_address_of_m_fontColorGradientPreset_51() { return &___m_fontColorGradientPreset_51; }
	inline void set_m_fontColorGradientPreset_51(TMP_ColorGradient_t3678055768 * value)
	{
		___m_fontColorGradientPreset_51 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontColorGradientPreset_51), value);
	}

	inline static int32_t get_offset_of_m_spriteAsset_52() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteAsset_52)); }
	inline TMP_SpriteAsset_t484820633 * get_m_spriteAsset_52() const { return ___m_spriteAsset_52; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_spriteAsset_52() { return &___m_spriteAsset_52; }
	inline void set_m_spriteAsset_52(TMP_SpriteAsset_t484820633 * value)
	{
		___m_spriteAsset_52 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAsset_52), value);
	}

	inline static int32_t get_offset_of_m_tintAllSprites_53() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_tintAllSprites_53)); }
	inline bool get_m_tintAllSprites_53() const { return ___m_tintAllSprites_53; }
	inline bool* get_address_of_m_tintAllSprites_53() { return &___m_tintAllSprites_53; }
	inline void set_m_tintAllSprites_53(bool value)
	{
		___m_tintAllSprites_53 = value;
	}

	inline static int32_t get_offset_of_m_tintSprite_54() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_tintSprite_54)); }
	inline bool get_m_tintSprite_54() const { return ___m_tintSprite_54; }
	inline bool* get_address_of_m_tintSprite_54() { return &___m_tintSprite_54; }
	inline void set_m_tintSprite_54(bool value)
	{
		___m_tintSprite_54 = value;
	}

	inline static int32_t get_offset_of_m_spriteColor_55() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteColor_55)); }
	inline Color32_t2600501292  get_m_spriteColor_55() const { return ___m_spriteColor_55; }
	inline Color32_t2600501292 * get_address_of_m_spriteColor_55() { return &___m_spriteColor_55; }
	inline void set_m_spriteColor_55(Color32_t2600501292  value)
	{
		___m_spriteColor_55 = value;
	}

	inline static int32_t get_offset_of_m_overrideHtmlColors_56() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_overrideHtmlColors_56)); }
	inline bool get_m_overrideHtmlColors_56() const { return ___m_overrideHtmlColors_56; }
	inline bool* get_address_of_m_overrideHtmlColors_56() { return &___m_overrideHtmlColors_56; }
	inline void set_m_overrideHtmlColors_56(bool value)
	{
		___m_overrideHtmlColors_56 = value;
	}

	inline static int32_t get_offset_of_m_faceColor_57() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_faceColor_57)); }
	inline Color32_t2600501292  get_m_faceColor_57() const { return ___m_faceColor_57; }
	inline Color32_t2600501292 * get_address_of_m_faceColor_57() { return &___m_faceColor_57; }
	inline void set_m_faceColor_57(Color32_t2600501292  value)
	{
		___m_faceColor_57 = value;
	}

	inline static int32_t get_offset_of_m_outlineColor_58() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_outlineColor_58)); }
	inline Color32_t2600501292  get_m_outlineColor_58() const { return ___m_outlineColor_58; }
	inline Color32_t2600501292 * get_address_of_m_outlineColor_58() { return &___m_outlineColor_58; }
	inline void set_m_outlineColor_58(Color32_t2600501292  value)
	{
		___m_outlineColor_58 = value;
	}

	inline static int32_t get_offset_of_m_outlineWidth_59() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_outlineWidth_59)); }
	inline float get_m_outlineWidth_59() const { return ___m_outlineWidth_59; }
	inline float* get_address_of_m_outlineWidth_59() { return &___m_outlineWidth_59; }
	inline void set_m_outlineWidth_59(float value)
	{
		___m_outlineWidth_59 = value;
	}

	inline static int32_t get_offset_of_m_fontSize_60() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSize_60)); }
	inline float get_m_fontSize_60() const { return ___m_fontSize_60; }
	inline float* get_address_of_m_fontSize_60() { return &___m_fontSize_60; }
	inline void set_m_fontSize_60(float value)
	{
		___m_fontSize_60 = value;
	}

	inline static int32_t get_offset_of_m_currentFontSize_61() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentFontSize_61)); }
	inline float get_m_currentFontSize_61() const { return ___m_currentFontSize_61; }
	inline float* get_address_of_m_currentFontSize_61() { return &___m_currentFontSize_61; }
	inline void set_m_currentFontSize_61(float value)
	{
		___m_currentFontSize_61 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeBase_62() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSizeBase_62)); }
	inline float get_m_fontSizeBase_62() const { return ___m_fontSizeBase_62; }
	inline float* get_address_of_m_fontSizeBase_62() { return &___m_fontSizeBase_62; }
	inline void set_m_fontSizeBase_62(float value)
	{
		___m_fontSizeBase_62 = value;
	}

	inline static int32_t get_offset_of_m_sizeStack_63() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_sizeStack_63)); }
	inline TMP_XmlTagStack_1_t960921318  get_m_sizeStack_63() const { return ___m_sizeStack_63; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_m_sizeStack_63() { return &___m_sizeStack_63; }
	inline void set_m_sizeStack_63(TMP_XmlTagStack_1_t960921318  value)
	{
		___m_sizeStack_63 = value;
	}

	inline static int32_t get_offset_of_m_fontWeight_64() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontWeight_64)); }
	inline int32_t get_m_fontWeight_64() const { return ___m_fontWeight_64; }
	inline int32_t* get_address_of_m_fontWeight_64() { return &___m_fontWeight_64; }
	inline void set_m_fontWeight_64(int32_t value)
	{
		___m_fontWeight_64 = value;
	}

	inline static int32_t get_offset_of_m_fontWeightInternal_65() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontWeightInternal_65)); }
	inline int32_t get_m_fontWeightInternal_65() const { return ___m_fontWeightInternal_65; }
	inline int32_t* get_address_of_m_fontWeightInternal_65() { return &___m_fontWeightInternal_65; }
	inline void set_m_fontWeightInternal_65(int32_t value)
	{
		___m_fontWeightInternal_65 = value;
	}

	inline static int32_t get_offset_of_m_fontWeightStack_66() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontWeightStack_66)); }
	inline TMP_XmlTagStack_1_t2514600297  get_m_fontWeightStack_66() const { return ___m_fontWeightStack_66; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_m_fontWeightStack_66() { return &___m_fontWeightStack_66; }
	inline void set_m_fontWeightStack_66(TMP_XmlTagStack_1_t2514600297  value)
	{
		___m_fontWeightStack_66 = value;
	}

	inline static int32_t get_offset_of_m_enableAutoSizing_67() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableAutoSizing_67)); }
	inline bool get_m_enableAutoSizing_67() const { return ___m_enableAutoSizing_67; }
	inline bool* get_address_of_m_enableAutoSizing_67() { return &___m_enableAutoSizing_67; }
	inline void set_m_enableAutoSizing_67(bool value)
	{
		___m_enableAutoSizing_67 = value;
	}

	inline static int32_t get_offset_of_m_maxFontSize_68() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxFontSize_68)); }
	inline float get_m_maxFontSize_68() const { return ___m_maxFontSize_68; }
	inline float* get_address_of_m_maxFontSize_68() { return &___m_maxFontSize_68; }
	inline void set_m_maxFontSize_68(float value)
	{
		___m_maxFontSize_68 = value;
	}

	inline static int32_t get_offset_of_m_minFontSize_69() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_minFontSize_69)); }
	inline float get_m_minFontSize_69() const { return ___m_minFontSize_69; }
	inline float* get_address_of_m_minFontSize_69() { return &___m_minFontSize_69; }
	inline void set_m_minFontSize_69(float value)
	{
		___m_minFontSize_69 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMin_70() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSizeMin_70)); }
	inline float get_m_fontSizeMin_70() const { return ___m_fontSizeMin_70; }
	inline float* get_address_of_m_fontSizeMin_70() { return &___m_fontSizeMin_70; }
	inline void set_m_fontSizeMin_70(float value)
	{
		___m_fontSizeMin_70 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMax_71() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSizeMax_71)); }
	inline float get_m_fontSizeMax_71() const { return ___m_fontSizeMax_71; }
	inline float* get_address_of_m_fontSizeMax_71() { return &___m_fontSizeMax_71; }
	inline void set_m_fontSizeMax_71(float value)
	{
		___m_fontSizeMax_71 = value;
	}

	inline static int32_t get_offset_of_m_fontStyle_72() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontStyle_72)); }
	inline int32_t get_m_fontStyle_72() const { return ___m_fontStyle_72; }
	inline int32_t* get_address_of_m_fontStyle_72() { return &___m_fontStyle_72; }
	inline void set_m_fontStyle_72(int32_t value)
	{
		___m_fontStyle_72 = value;
	}

	inline static int32_t get_offset_of_m_style_73() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_style_73)); }
	inline int32_t get_m_style_73() const { return ___m_style_73; }
	inline int32_t* get_address_of_m_style_73() { return &___m_style_73; }
	inline void set_m_style_73(int32_t value)
	{
		___m_style_73 = value;
	}

	inline static int32_t get_offset_of_m_fontStyleStack_74() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontStyleStack_74)); }
	inline TMP_BasicXmlTagStack_t2962628096  get_m_fontStyleStack_74() const { return ___m_fontStyleStack_74; }
	inline TMP_BasicXmlTagStack_t2962628096 * get_address_of_m_fontStyleStack_74() { return &___m_fontStyleStack_74; }
	inline void set_m_fontStyleStack_74(TMP_BasicXmlTagStack_t2962628096  value)
	{
		___m_fontStyleStack_74 = value;
	}

	inline static int32_t get_offset_of_m_isUsingBold_75() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isUsingBold_75)); }
	inline bool get_m_isUsingBold_75() const { return ___m_isUsingBold_75; }
	inline bool* get_address_of_m_isUsingBold_75() { return &___m_isUsingBold_75; }
	inline void set_m_isUsingBold_75(bool value)
	{
		___m_isUsingBold_75 = value;
	}

	inline static int32_t get_offset_of_m_textAlignment_76() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_textAlignment_76)); }
	inline int32_t get_m_textAlignment_76() const { return ___m_textAlignment_76; }
	inline int32_t* get_address_of_m_textAlignment_76() { return &___m_textAlignment_76; }
	inline void set_m_textAlignment_76(int32_t value)
	{
		___m_textAlignment_76 = value;
	}

	inline static int32_t get_offset_of_m_lineJustification_77() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineJustification_77)); }
	inline int32_t get_m_lineJustification_77() const { return ___m_lineJustification_77; }
	inline int32_t* get_address_of_m_lineJustification_77() { return &___m_lineJustification_77; }
	inline void set_m_lineJustification_77(int32_t value)
	{
		___m_lineJustification_77 = value;
	}

	inline static int32_t get_offset_of_m_lineJustificationStack_78() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineJustificationStack_78)); }
	inline TMP_XmlTagStack_1_t3600445780  get_m_lineJustificationStack_78() const { return ___m_lineJustificationStack_78; }
	inline TMP_XmlTagStack_1_t3600445780 * get_address_of_m_lineJustificationStack_78() { return &___m_lineJustificationStack_78; }
	inline void set_m_lineJustificationStack_78(TMP_XmlTagStack_1_t3600445780  value)
	{
		___m_lineJustificationStack_78 = value;
	}

	inline static int32_t get_offset_of_m_textContainerLocalCorners_79() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_textContainerLocalCorners_79)); }
	inline Vector3U5BU5D_t1718750761* get_m_textContainerLocalCorners_79() const { return ___m_textContainerLocalCorners_79; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_textContainerLocalCorners_79() { return &___m_textContainerLocalCorners_79; }
	inline void set_m_textContainerLocalCorners_79(Vector3U5BU5D_t1718750761* value)
	{
		___m_textContainerLocalCorners_79 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainerLocalCorners_79), value);
	}

	inline static int32_t get_offset_of_m_isAlignmentEnumConverted_80() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isAlignmentEnumConverted_80)); }
	inline bool get_m_isAlignmentEnumConverted_80() const { return ___m_isAlignmentEnumConverted_80; }
	inline bool* get_address_of_m_isAlignmentEnumConverted_80() { return &___m_isAlignmentEnumConverted_80; }
	inline void set_m_isAlignmentEnumConverted_80(bool value)
	{
		___m_isAlignmentEnumConverted_80 = value;
	}

	inline static int32_t get_offset_of_m_characterSpacing_81() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_characterSpacing_81)); }
	inline float get_m_characterSpacing_81() const { return ___m_characterSpacing_81; }
	inline float* get_address_of_m_characterSpacing_81() { return &___m_characterSpacing_81; }
	inline void set_m_characterSpacing_81(float value)
	{
		___m_characterSpacing_81 = value;
	}

	inline static int32_t get_offset_of_m_cSpacing_82() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_cSpacing_82)); }
	inline float get_m_cSpacing_82() const { return ___m_cSpacing_82; }
	inline float* get_address_of_m_cSpacing_82() { return &___m_cSpacing_82; }
	inline void set_m_cSpacing_82(float value)
	{
		___m_cSpacing_82 = value;
	}

	inline static int32_t get_offset_of_m_monoSpacing_83() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_monoSpacing_83)); }
	inline float get_m_monoSpacing_83() const { return ___m_monoSpacing_83; }
	inline float* get_address_of_m_monoSpacing_83() { return &___m_monoSpacing_83; }
	inline void set_m_monoSpacing_83(float value)
	{
		___m_monoSpacing_83 = value;
	}

	inline static int32_t get_offset_of_m_wordSpacing_84() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_wordSpacing_84)); }
	inline float get_m_wordSpacing_84() const { return ___m_wordSpacing_84; }
	inline float* get_address_of_m_wordSpacing_84() { return &___m_wordSpacing_84; }
	inline void set_m_wordSpacing_84(float value)
	{
		___m_wordSpacing_84 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacing_85() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineSpacing_85)); }
	inline float get_m_lineSpacing_85() const { return ___m_lineSpacing_85; }
	inline float* get_address_of_m_lineSpacing_85() { return &___m_lineSpacing_85; }
	inline void set_m_lineSpacing_85(float value)
	{
		___m_lineSpacing_85 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingDelta_86() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineSpacingDelta_86)); }
	inline float get_m_lineSpacingDelta_86() const { return ___m_lineSpacingDelta_86; }
	inline float* get_address_of_m_lineSpacingDelta_86() { return &___m_lineSpacingDelta_86; }
	inline void set_m_lineSpacingDelta_86(float value)
	{
		___m_lineSpacingDelta_86 = value;
	}

	inline static int32_t get_offset_of_m_lineHeight_87() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineHeight_87)); }
	inline float get_m_lineHeight_87() const { return ___m_lineHeight_87; }
	inline float* get_address_of_m_lineHeight_87() { return &___m_lineHeight_87; }
	inline void set_m_lineHeight_87(float value)
	{
		___m_lineHeight_87 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingMax_88() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineSpacingMax_88)); }
	inline float get_m_lineSpacingMax_88() const { return ___m_lineSpacingMax_88; }
	inline float* get_address_of_m_lineSpacingMax_88() { return &___m_lineSpacingMax_88; }
	inline void set_m_lineSpacingMax_88(float value)
	{
		___m_lineSpacingMax_88 = value;
	}

	inline static int32_t get_offset_of_m_paragraphSpacing_89() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_paragraphSpacing_89)); }
	inline float get_m_paragraphSpacing_89() const { return ___m_paragraphSpacing_89; }
	inline float* get_address_of_m_paragraphSpacing_89() { return &___m_paragraphSpacing_89; }
	inline void set_m_paragraphSpacing_89(float value)
	{
		___m_paragraphSpacing_89 = value;
	}

	inline static int32_t get_offset_of_m_charWidthMaxAdj_90() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_charWidthMaxAdj_90)); }
	inline float get_m_charWidthMaxAdj_90() const { return ___m_charWidthMaxAdj_90; }
	inline float* get_address_of_m_charWidthMaxAdj_90() { return &___m_charWidthMaxAdj_90; }
	inline void set_m_charWidthMaxAdj_90(float value)
	{
		___m_charWidthMaxAdj_90 = value;
	}

	inline static int32_t get_offset_of_m_charWidthAdjDelta_91() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_charWidthAdjDelta_91)); }
	inline float get_m_charWidthAdjDelta_91() const { return ___m_charWidthAdjDelta_91; }
	inline float* get_address_of_m_charWidthAdjDelta_91() { return &___m_charWidthAdjDelta_91; }
	inline void set_m_charWidthAdjDelta_91(float value)
	{
		___m_charWidthAdjDelta_91 = value;
	}

	inline static int32_t get_offset_of_m_enableWordWrapping_92() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableWordWrapping_92)); }
	inline bool get_m_enableWordWrapping_92() const { return ___m_enableWordWrapping_92; }
	inline bool* get_address_of_m_enableWordWrapping_92() { return &___m_enableWordWrapping_92; }
	inline void set_m_enableWordWrapping_92(bool value)
	{
		___m_enableWordWrapping_92 = value;
	}

	inline static int32_t get_offset_of_m_isCharacterWrappingEnabled_93() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isCharacterWrappingEnabled_93)); }
	inline bool get_m_isCharacterWrappingEnabled_93() const { return ___m_isCharacterWrappingEnabled_93; }
	inline bool* get_address_of_m_isCharacterWrappingEnabled_93() { return &___m_isCharacterWrappingEnabled_93; }
	inline void set_m_isCharacterWrappingEnabled_93(bool value)
	{
		___m_isCharacterWrappingEnabled_93 = value;
	}

	inline static int32_t get_offset_of_m_isNonBreakingSpace_94() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isNonBreakingSpace_94)); }
	inline bool get_m_isNonBreakingSpace_94() const { return ___m_isNonBreakingSpace_94; }
	inline bool* get_address_of_m_isNonBreakingSpace_94() { return &___m_isNonBreakingSpace_94; }
	inline void set_m_isNonBreakingSpace_94(bool value)
	{
		___m_isNonBreakingSpace_94 = value;
	}

	inline static int32_t get_offset_of_m_isIgnoringAlignment_95() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isIgnoringAlignment_95)); }
	inline bool get_m_isIgnoringAlignment_95() const { return ___m_isIgnoringAlignment_95; }
	inline bool* get_address_of_m_isIgnoringAlignment_95() { return &___m_isIgnoringAlignment_95; }
	inline void set_m_isIgnoringAlignment_95(bool value)
	{
		___m_isIgnoringAlignment_95 = value;
	}

	inline static int32_t get_offset_of_m_wordWrappingRatios_96() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_wordWrappingRatios_96)); }
	inline float get_m_wordWrappingRatios_96() const { return ___m_wordWrappingRatios_96; }
	inline float* get_address_of_m_wordWrappingRatios_96() { return &___m_wordWrappingRatios_96; }
	inline void set_m_wordWrappingRatios_96(float value)
	{
		___m_wordWrappingRatios_96 = value;
	}

	inline static int32_t get_offset_of_m_overflowMode_97() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_overflowMode_97)); }
	inline int32_t get_m_overflowMode_97() const { return ___m_overflowMode_97; }
	inline int32_t* get_address_of_m_overflowMode_97() { return &___m_overflowMode_97; }
	inline void set_m_overflowMode_97(int32_t value)
	{
		___m_overflowMode_97 = value;
	}

	inline static int32_t get_offset_of_m_firstOverflowCharacterIndex_98() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_firstOverflowCharacterIndex_98)); }
	inline int32_t get_m_firstOverflowCharacterIndex_98() const { return ___m_firstOverflowCharacterIndex_98; }
	inline int32_t* get_address_of_m_firstOverflowCharacterIndex_98() { return &___m_firstOverflowCharacterIndex_98; }
	inline void set_m_firstOverflowCharacterIndex_98(int32_t value)
	{
		___m_firstOverflowCharacterIndex_98 = value;
	}

	inline static int32_t get_offset_of_m_linkedTextComponent_99() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_linkedTextComponent_99)); }
	inline TMP_Text_t2599618874 * get_m_linkedTextComponent_99() const { return ___m_linkedTextComponent_99; }
	inline TMP_Text_t2599618874 ** get_address_of_m_linkedTextComponent_99() { return &___m_linkedTextComponent_99; }
	inline void set_m_linkedTextComponent_99(TMP_Text_t2599618874 * value)
	{
		___m_linkedTextComponent_99 = value;
		Il2CppCodeGenWriteBarrier((&___m_linkedTextComponent_99), value);
	}

	inline static int32_t get_offset_of_m_isLinkedTextComponent_100() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isLinkedTextComponent_100)); }
	inline bool get_m_isLinkedTextComponent_100() const { return ___m_isLinkedTextComponent_100; }
	inline bool* get_address_of_m_isLinkedTextComponent_100() { return &___m_isLinkedTextComponent_100; }
	inline void set_m_isLinkedTextComponent_100(bool value)
	{
		___m_isLinkedTextComponent_100 = value;
	}

	inline static int32_t get_offset_of_m_isTextTruncated_101() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isTextTruncated_101)); }
	inline bool get_m_isTextTruncated_101() const { return ___m_isTextTruncated_101; }
	inline bool* get_address_of_m_isTextTruncated_101() { return &___m_isTextTruncated_101; }
	inline void set_m_isTextTruncated_101(bool value)
	{
		___m_isTextTruncated_101 = value;
	}

	inline static int32_t get_offset_of_m_enableKerning_102() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableKerning_102)); }
	inline bool get_m_enableKerning_102() const { return ___m_enableKerning_102; }
	inline bool* get_address_of_m_enableKerning_102() { return &___m_enableKerning_102; }
	inline void set_m_enableKerning_102(bool value)
	{
		___m_enableKerning_102 = value;
	}

	inline static int32_t get_offset_of_m_enableExtraPadding_103() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableExtraPadding_103)); }
	inline bool get_m_enableExtraPadding_103() const { return ___m_enableExtraPadding_103; }
	inline bool* get_address_of_m_enableExtraPadding_103() { return &___m_enableExtraPadding_103; }
	inline void set_m_enableExtraPadding_103(bool value)
	{
		___m_enableExtraPadding_103 = value;
	}

	inline static int32_t get_offset_of_checkPaddingRequired_104() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___checkPaddingRequired_104)); }
	inline bool get_checkPaddingRequired_104() const { return ___checkPaddingRequired_104; }
	inline bool* get_address_of_checkPaddingRequired_104() { return &___checkPaddingRequired_104; }
	inline void set_checkPaddingRequired_104(bool value)
	{
		___checkPaddingRequired_104 = value;
	}

	inline static int32_t get_offset_of_m_isRichText_105() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isRichText_105)); }
	inline bool get_m_isRichText_105() const { return ___m_isRichText_105; }
	inline bool* get_address_of_m_isRichText_105() { return &___m_isRichText_105; }
	inline void set_m_isRichText_105(bool value)
	{
		___m_isRichText_105 = value;
	}

	inline static int32_t get_offset_of_m_parseCtrlCharacters_106() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_parseCtrlCharacters_106)); }
	inline bool get_m_parseCtrlCharacters_106() const { return ___m_parseCtrlCharacters_106; }
	inline bool* get_address_of_m_parseCtrlCharacters_106() { return &___m_parseCtrlCharacters_106; }
	inline void set_m_parseCtrlCharacters_106(bool value)
	{
		___m_parseCtrlCharacters_106 = value;
	}

	inline static int32_t get_offset_of_m_isOverlay_107() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isOverlay_107)); }
	inline bool get_m_isOverlay_107() const { return ___m_isOverlay_107; }
	inline bool* get_address_of_m_isOverlay_107() { return &___m_isOverlay_107; }
	inline void set_m_isOverlay_107(bool value)
	{
		___m_isOverlay_107 = value;
	}

	inline static int32_t get_offset_of_m_isOrthographic_108() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isOrthographic_108)); }
	inline bool get_m_isOrthographic_108() const { return ___m_isOrthographic_108; }
	inline bool* get_address_of_m_isOrthographic_108() { return &___m_isOrthographic_108; }
	inline void set_m_isOrthographic_108(bool value)
	{
		___m_isOrthographic_108 = value;
	}

	inline static int32_t get_offset_of_m_isCullingEnabled_109() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isCullingEnabled_109)); }
	inline bool get_m_isCullingEnabled_109() const { return ___m_isCullingEnabled_109; }
	inline bool* get_address_of_m_isCullingEnabled_109() { return &___m_isCullingEnabled_109; }
	inline void set_m_isCullingEnabled_109(bool value)
	{
		___m_isCullingEnabled_109 = value;
	}

	inline static int32_t get_offset_of_m_ignoreRectMaskCulling_110() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_ignoreRectMaskCulling_110)); }
	inline bool get_m_ignoreRectMaskCulling_110() const { return ___m_ignoreRectMaskCulling_110; }
	inline bool* get_address_of_m_ignoreRectMaskCulling_110() { return &___m_ignoreRectMaskCulling_110; }
	inline void set_m_ignoreRectMaskCulling_110(bool value)
	{
		___m_ignoreRectMaskCulling_110 = value;
	}

	inline static int32_t get_offset_of_m_ignoreCulling_111() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_ignoreCulling_111)); }
	inline bool get_m_ignoreCulling_111() const { return ___m_ignoreCulling_111; }
	inline bool* get_address_of_m_ignoreCulling_111() { return &___m_ignoreCulling_111; }
	inline void set_m_ignoreCulling_111(bool value)
	{
		___m_ignoreCulling_111 = value;
	}

	inline static int32_t get_offset_of_m_horizontalMapping_112() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_horizontalMapping_112)); }
	inline int32_t get_m_horizontalMapping_112() const { return ___m_horizontalMapping_112; }
	inline int32_t* get_address_of_m_horizontalMapping_112() { return &___m_horizontalMapping_112; }
	inline void set_m_horizontalMapping_112(int32_t value)
	{
		___m_horizontalMapping_112 = value;
	}

	inline static int32_t get_offset_of_m_verticalMapping_113() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_verticalMapping_113)); }
	inline int32_t get_m_verticalMapping_113() const { return ___m_verticalMapping_113; }
	inline int32_t* get_address_of_m_verticalMapping_113() { return &___m_verticalMapping_113; }
	inline void set_m_verticalMapping_113(int32_t value)
	{
		___m_verticalMapping_113 = value;
	}

	inline static int32_t get_offset_of_m_uvLineOffset_114() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_uvLineOffset_114)); }
	inline float get_m_uvLineOffset_114() const { return ___m_uvLineOffset_114; }
	inline float* get_address_of_m_uvLineOffset_114() { return &___m_uvLineOffset_114; }
	inline void set_m_uvLineOffset_114(float value)
	{
		___m_uvLineOffset_114 = value;
	}

	inline static int32_t get_offset_of_m_renderMode_115() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_renderMode_115)); }
	inline int32_t get_m_renderMode_115() const { return ___m_renderMode_115; }
	inline int32_t* get_address_of_m_renderMode_115() { return &___m_renderMode_115; }
	inline void set_m_renderMode_115(int32_t value)
	{
		___m_renderMode_115 = value;
	}

	inline static int32_t get_offset_of_m_geometrySortingOrder_116() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_geometrySortingOrder_116)); }
	inline int32_t get_m_geometrySortingOrder_116() const { return ___m_geometrySortingOrder_116; }
	inline int32_t* get_address_of_m_geometrySortingOrder_116() { return &___m_geometrySortingOrder_116; }
	inline void set_m_geometrySortingOrder_116(int32_t value)
	{
		___m_geometrySortingOrder_116 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacter_117() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_firstVisibleCharacter_117)); }
	inline int32_t get_m_firstVisibleCharacter_117() const { return ___m_firstVisibleCharacter_117; }
	inline int32_t* get_address_of_m_firstVisibleCharacter_117() { return &___m_firstVisibleCharacter_117; }
	inline void set_m_firstVisibleCharacter_117(int32_t value)
	{
		___m_firstVisibleCharacter_117 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleCharacters_118() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxVisibleCharacters_118)); }
	inline int32_t get_m_maxVisibleCharacters_118() const { return ___m_maxVisibleCharacters_118; }
	inline int32_t* get_address_of_m_maxVisibleCharacters_118() { return &___m_maxVisibleCharacters_118; }
	inline void set_m_maxVisibleCharacters_118(int32_t value)
	{
		___m_maxVisibleCharacters_118 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleWords_119() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxVisibleWords_119)); }
	inline int32_t get_m_maxVisibleWords_119() const { return ___m_maxVisibleWords_119; }
	inline int32_t* get_address_of_m_maxVisibleWords_119() { return &___m_maxVisibleWords_119; }
	inline void set_m_maxVisibleWords_119(int32_t value)
	{
		___m_maxVisibleWords_119 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleLines_120() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxVisibleLines_120)); }
	inline int32_t get_m_maxVisibleLines_120() const { return ___m_maxVisibleLines_120; }
	inline int32_t* get_address_of_m_maxVisibleLines_120() { return &___m_maxVisibleLines_120; }
	inline void set_m_maxVisibleLines_120(int32_t value)
	{
		___m_maxVisibleLines_120 = value;
	}

	inline static int32_t get_offset_of_m_useMaxVisibleDescender_121() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_useMaxVisibleDescender_121)); }
	inline bool get_m_useMaxVisibleDescender_121() const { return ___m_useMaxVisibleDescender_121; }
	inline bool* get_address_of_m_useMaxVisibleDescender_121() { return &___m_useMaxVisibleDescender_121; }
	inline void set_m_useMaxVisibleDescender_121(bool value)
	{
		___m_useMaxVisibleDescender_121 = value;
	}

	inline static int32_t get_offset_of_m_pageToDisplay_122() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_pageToDisplay_122)); }
	inline int32_t get_m_pageToDisplay_122() const { return ___m_pageToDisplay_122; }
	inline int32_t* get_address_of_m_pageToDisplay_122() { return &___m_pageToDisplay_122; }
	inline void set_m_pageToDisplay_122(int32_t value)
	{
		___m_pageToDisplay_122 = value;
	}

	inline static int32_t get_offset_of_m_isNewPage_123() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isNewPage_123)); }
	inline bool get_m_isNewPage_123() const { return ___m_isNewPage_123; }
	inline bool* get_address_of_m_isNewPage_123() { return &___m_isNewPage_123; }
	inline void set_m_isNewPage_123(bool value)
	{
		___m_isNewPage_123 = value;
	}

	inline static int32_t get_offset_of_m_margin_124() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_margin_124)); }
	inline Vector4_t3319028937  get_m_margin_124() const { return ___m_margin_124; }
	inline Vector4_t3319028937 * get_address_of_m_margin_124() { return &___m_margin_124; }
	inline void set_m_margin_124(Vector4_t3319028937  value)
	{
		___m_margin_124 = value;
	}

	inline static int32_t get_offset_of_m_marginLeft_125() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_marginLeft_125)); }
	inline float get_m_marginLeft_125() const { return ___m_marginLeft_125; }
	inline float* get_address_of_m_marginLeft_125() { return &___m_marginLeft_125; }
	inline void set_m_marginLeft_125(float value)
	{
		___m_marginLeft_125 = value;
	}

	inline static int32_t get_offset_of_m_marginRight_126() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_marginRight_126)); }
	inline float get_m_marginRight_126() const { return ___m_marginRight_126; }
	inline float* get_address_of_m_marginRight_126() { return &___m_marginRight_126; }
	inline void set_m_marginRight_126(float value)
	{
		___m_marginRight_126 = value;
	}

	inline static int32_t get_offset_of_m_marginWidth_127() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_marginWidth_127)); }
	inline float get_m_marginWidth_127() const { return ___m_marginWidth_127; }
	inline float* get_address_of_m_marginWidth_127() { return &___m_marginWidth_127; }
	inline void set_m_marginWidth_127(float value)
	{
		___m_marginWidth_127 = value;
	}

	inline static int32_t get_offset_of_m_marginHeight_128() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_marginHeight_128)); }
	inline float get_m_marginHeight_128() const { return ___m_marginHeight_128; }
	inline float* get_address_of_m_marginHeight_128() { return &___m_marginHeight_128; }
	inline void set_m_marginHeight_128(float value)
	{
		___m_marginHeight_128 = value;
	}

	inline static int32_t get_offset_of_m_width_129() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_width_129)); }
	inline float get_m_width_129() const { return ___m_width_129; }
	inline float* get_address_of_m_width_129() { return &___m_width_129; }
	inline void set_m_width_129(float value)
	{
		___m_width_129 = value;
	}

	inline static int32_t get_offset_of_m_textInfo_130() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_textInfo_130)); }
	inline TMP_TextInfo_t3598145122 * get_m_textInfo_130() const { return ___m_textInfo_130; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_m_textInfo_130() { return &___m_textInfo_130; }
	inline void set_m_textInfo_130(TMP_TextInfo_t3598145122 * value)
	{
		___m_textInfo_130 = value;
		Il2CppCodeGenWriteBarrier((&___m_textInfo_130), value);
	}

	inline static int32_t get_offset_of_m_havePropertiesChanged_131() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_havePropertiesChanged_131)); }
	inline bool get_m_havePropertiesChanged_131() const { return ___m_havePropertiesChanged_131; }
	inline bool* get_address_of_m_havePropertiesChanged_131() { return &___m_havePropertiesChanged_131; }
	inline void set_m_havePropertiesChanged_131(bool value)
	{
		___m_havePropertiesChanged_131 = value;
	}

	inline static int32_t get_offset_of_m_isUsingLegacyAnimationComponent_132() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isUsingLegacyAnimationComponent_132)); }
	inline bool get_m_isUsingLegacyAnimationComponent_132() const { return ___m_isUsingLegacyAnimationComponent_132; }
	inline bool* get_address_of_m_isUsingLegacyAnimationComponent_132() { return &___m_isUsingLegacyAnimationComponent_132; }
	inline void set_m_isUsingLegacyAnimationComponent_132(bool value)
	{
		___m_isUsingLegacyAnimationComponent_132 = value;
	}

	inline static int32_t get_offset_of_m_transform_133() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_transform_133)); }
	inline Transform_t3600365921 * get_m_transform_133() const { return ___m_transform_133; }
	inline Transform_t3600365921 ** get_address_of_m_transform_133() { return &___m_transform_133; }
	inline void set_m_transform_133(Transform_t3600365921 * value)
	{
		___m_transform_133 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_133), value);
	}

	inline static int32_t get_offset_of_m_rectTransform_134() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_rectTransform_134)); }
	inline RectTransform_t3704657025 * get_m_rectTransform_134() const { return ___m_rectTransform_134; }
	inline RectTransform_t3704657025 ** get_address_of_m_rectTransform_134() { return &___m_rectTransform_134; }
	inline void set_m_rectTransform_134(RectTransform_t3704657025 * value)
	{
		___m_rectTransform_134 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_134), value);
	}

	inline static int32_t get_offset_of_U3CautoSizeTextContainerU3Ek__BackingField_135() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___U3CautoSizeTextContainerU3Ek__BackingField_135)); }
	inline bool get_U3CautoSizeTextContainerU3Ek__BackingField_135() const { return ___U3CautoSizeTextContainerU3Ek__BackingField_135; }
	inline bool* get_address_of_U3CautoSizeTextContainerU3Ek__BackingField_135() { return &___U3CautoSizeTextContainerU3Ek__BackingField_135; }
	inline void set_U3CautoSizeTextContainerU3Ek__BackingField_135(bool value)
	{
		___U3CautoSizeTextContainerU3Ek__BackingField_135 = value;
	}

	inline static int32_t get_offset_of_m_autoSizeTextContainer_136() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_autoSizeTextContainer_136)); }
	inline bool get_m_autoSizeTextContainer_136() const { return ___m_autoSizeTextContainer_136; }
	inline bool* get_address_of_m_autoSizeTextContainer_136() { return &___m_autoSizeTextContainer_136; }
	inline void set_m_autoSizeTextContainer_136(bool value)
	{
		___m_autoSizeTextContainer_136 = value;
	}

	inline static int32_t get_offset_of_m_mesh_137() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_mesh_137)); }
	inline Mesh_t3648964284 * get_m_mesh_137() const { return ___m_mesh_137; }
	inline Mesh_t3648964284 ** get_address_of_m_mesh_137() { return &___m_mesh_137; }
	inline void set_m_mesh_137(Mesh_t3648964284 * value)
	{
		___m_mesh_137 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_137), value);
	}

	inline static int32_t get_offset_of_m_isVolumetricText_138() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isVolumetricText_138)); }
	inline bool get_m_isVolumetricText_138() const { return ___m_isVolumetricText_138; }
	inline bool* get_address_of_m_isVolumetricText_138() { return &___m_isVolumetricText_138; }
	inline void set_m_isVolumetricText_138(bool value)
	{
		___m_isVolumetricText_138 = value;
	}

	inline static int32_t get_offset_of_m_spriteAnimator_139() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteAnimator_139)); }
	inline TMP_SpriteAnimator_t2836635477 * get_m_spriteAnimator_139() const { return ___m_spriteAnimator_139; }
	inline TMP_SpriteAnimator_t2836635477 ** get_address_of_m_spriteAnimator_139() { return &___m_spriteAnimator_139; }
	inline void set_m_spriteAnimator_139(TMP_SpriteAnimator_t2836635477 * value)
	{
		___m_spriteAnimator_139 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAnimator_139), value);
	}

	inline static int32_t get_offset_of_m_flexibleHeight_140() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_flexibleHeight_140)); }
	inline float get_m_flexibleHeight_140() const { return ___m_flexibleHeight_140; }
	inline float* get_address_of_m_flexibleHeight_140() { return &___m_flexibleHeight_140; }
	inline void set_m_flexibleHeight_140(float value)
	{
		___m_flexibleHeight_140 = value;
	}

	inline static int32_t get_offset_of_m_flexibleWidth_141() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_flexibleWidth_141)); }
	inline float get_m_flexibleWidth_141() const { return ___m_flexibleWidth_141; }
	inline float* get_address_of_m_flexibleWidth_141() { return &___m_flexibleWidth_141; }
	inline void set_m_flexibleWidth_141(float value)
	{
		___m_flexibleWidth_141 = value;
	}

	inline static int32_t get_offset_of_m_minWidth_142() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_minWidth_142)); }
	inline float get_m_minWidth_142() const { return ___m_minWidth_142; }
	inline float* get_address_of_m_minWidth_142() { return &___m_minWidth_142; }
	inline void set_m_minWidth_142(float value)
	{
		___m_minWidth_142 = value;
	}

	inline static int32_t get_offset_of_m_minHeight_143() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_minHeight_143)); }
	inline float get_m_minHeight_143() const { return ___m_minHeight_143; }
	inline float* get_address_of_m_minHeight_143() { return &___m_minHeight_143; }
	inline void set_m_minHeight_143(float value)
	{
		___m_minHeight_143 = value;
	}

	inline static int32_t get_offset_of_m_maxWidth_144() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxWidth_144)); }
	inline float get_m_maxWidth_144() const { return ___m_maxWidth_144; }
	inline float* get_address_of_m_maxWidth_144() { return &___m_maxWidth_144; }
	inline void set_m_maxWidth_144(float value)
	{
		___m_maxWidth_144 = value;
	}

	inline static int32_t get_offset_of_m_maxHeight_145() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxHeight_145)); }
	inline float get_m_maxHeight_145() const { return ___m_maxHeight_145; }
	inline float* get_address_of_m_maxHeight_145() { return &___m_maxHeight_145; }
	inline void set_m_maxHeight_145(float value)
	{
		___m_maxHeight_145 = value;
	}

	inline static int32_t get_offset_of_m_LayoutElement_146() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_LayoutElement_146)); }
	inline LayoutElement_t1785403678 * get_m_LayoutElement_146() const { return ___m_LayoutElement_146; }
	inline LayoutElement_t1785403678 ** get_address_of_m_LayoutElement_146() { return &___m_LayoutElement_146; }
	inline void set_m_LayoutElement_146(LayoutElement_t1785403678 * value)
	{
		___m_LayoutElement_146 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutElement_146), value);
	}

	inline static int32_t get_offset_of_m_preferredWidth_147() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_preferredWidth_147)); }
	inline float get_m_preferredWidth_147() const { return ___m_preferredWidth_147; }
	inline float* get_address_of_m_preferredWidth_147() { return &___m_preferredWidth_147; }
	inline void set_m_preferredWidth_147(float value)
	{
		___m_preferredWidth_147 = value;
	}

	inline static int32_t get_offset_of_m_renderedWidth_148() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_renderedWidth_148)); }
	inline float get_m_renderedWidth_148() const { return ___m_renderedWidth_148; }
	inline float* get_address_of_m_renderedWidth_148() { return &___m_renderedWidth_148; }
	inline void set_m_renderedWidth_148(float value)
	{
		___m_renderedWidth_148 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredWidthDirty_149() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isPreferredWidthDirty_149)); }
	inline bool get_m_isPreferredWidthDirty_149() const { return ___m_isPreferredWidthDirty_149; }
	inline bool* get_address_of_m_isPreferredWidthDirty_149() { return &___m_isPreferredWidthDirty_149; }
	inline void set_m_isPreferredWidthDirty_149(bool value)
	{
		___m_isPreferredWidthDirty_149 = value;
	}

	inline static int32_t get_offset_of_m_preferredHeight_150() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_preferredHeight_150)); }
	inline float get_m_preferredHeight_150() const { return ___m_preferredHeight_150; }
	inline float* get_address_of_m_preferredHeight_150() { return &___m_preferredHeight_150; }
	inline void set_m_preferredHeight_150(float value)
	{
		___m_preferredHeight_150 = value;
	}

	inline static int32_t get_offset_of_m_renderedHeight_151() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_renderedHeight_151)); }
	inline float get_m_renderedHeight_151() const { return ___m_renderedHeight_151; }
	inline float* get_address_of_m_renderedHeight_151() { return &___m_renderedHeight_151; }
	inline void set_m_renderedHeight_151(float value)
	{
		___m_renderedHeight_151 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredHeightDirty_152() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isPreferredHeightDirty_152)); }
	inline bool get_m_isPreferredHeightDirty_152() const { return ___m_isPreferredHeightDirty_152; }
	inline bool* get_address_of_m_isPreferredHeightDirty_152() { return &___m_isPreferredHeightDirty_152; }
	inline void set_m_isPreferredHeightDirty_152(bool value)
	{
		___m_isPreferredHeightDirty_152 = value;
	}

	inline static int32_t get_offset_of_m_isCalculatingPreferredValues_153() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isCalculatingPreferredValues_153)); }
	inline bool get_m_isCalculatingPreferredValues_153() const { return ___m_isCalculatingPreferredValues_153; }
	inline bool* get_address_of_m_isCalculatingPreferredValues_153() { return &___m_isCalculatingPreferredValues_153; }
	inline void set_m_isCalculatingPreferredValues_153(bool value)
	{
		___m_isCalculatingPreferredValues_153 = value;
	}

	inline static int32_t get_offset_of_m_recursiveCount_154() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_recursiveCount_154)); }
	inline int32_t get_m_recursiveCount_154() const { return ___m_recursiveCount_154; }
	inline int32_t* get_address_of_m_recursiveCount_154() { return &___m_recursiveCount_154; }
	inline void set_m_recursiveCount_154(int32_t value)
	{
		___m_recursiveCount_154 = value;
	}

	inline static int32_t get_offset_of_m_layoutPriority_155() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_layoutPriority_155)); }
	inline int32_t get_m_layoutPriority_155() const { return ___m_layoutPriority_155; }
	inline int32_t* get_address_of_m_layoutPriority_155() { return &___m_layoutPriority_155; }
	inline void set_m_layoutPriority_155(int32_t value)
	{
		___m_layoutPriority_155 = value;
	}

	inline static int32_t get_offset_of_m_isCalculateSizeRequired_156() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isCalculateSizeRequired_156)); }
	inline bool get_m_isCalculateSizeRequired_156() const { return ___m_isCalculateSizeRequired_156; }
	inline bool* get_address_of_m_isCalculateSizeRequired_156() { return &___m_isCalculateSizeRequired_156; }
	inline void set_m_isCalculateSizeRequired_156(bool value)
	{
		___m_isCalculateSizeRequired_156 = value;
	}

	inline static int32_t get_offset_of_m_isLayoutDirty_157() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isLayoutDirty_157)); }
	inline bool get_m_isLayoutDirty_157() const { return ___m_isLayoutDirty_157; }
	inline bool* get_address_of_m_isLayoutDirty_157() { return &___m_isLayoutDirty_157; }
	inline void set_m_isLayoutDirty_157(bool value)
	{
		___m_isLayoutDirty_157 = value;
	}

	inline static int32_t get_offset_of_m_verticesAlreadyDirty_158() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_verticesAlreadyDirty_158)); }
	inline bool get_m_verticesAlreadyDirty_158() const { return ___m_verticesAlreadyDirty_158; }
	inline bool* get_address_of_m_verticesAlreadyDirty_158() { return &___m_verticesAlreadyDirty_158; }
	inline void set_m_verticesAlreadyDirty_158(bool value)
	{
		___m_verticesAlreadyDirty_158 = value;
	}

	inline static int32_t get_offset_of_m_layoutAlreadyDirty_159() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_layoutAlreadyDirty_159)); }
	inline bool get_m_layoutAlreadyDirty_159() const { return ___m_layoutAlreadyDirty_159; }
	inline bool* get_address_of_m_layoutAlreadyDirty_159() { return &___m_layoutAlreadyDirty_159; }
	inline void set_m_layoutAlreadyDirty_159(bool value)
	{
		___m_layoutAlreadyDirty_159 = value;
	}

	inline static int32_t get_offset_of_m_isAwake_160() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isAwake_160)); }
	inline bool get_m_isAwake_160() const { return ___m_isAwake_160; }
	inline bool* get_address_of_m_isAwake_160() { return &___m_isAwake_160; }
	inline void set_m_isAwake_160(bool value)
	{
		___m_isAwake_160 = value;
	}

	inline static int32_t get_offset_of_m_isInputParsingRequired_161() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isInputParsingRequired_161)); }
	inline bool get_m_isInputParsingRequired_161() const { return ___m_isInputParsingRequired_161; }
	inline bool* get_address_of_m_isInputParsingRequired_161() { return &___m_isInputParsingRequired_161; }
	inline void set_m_isInputParsingRequired_161(bool value)
	{
		___m_isInputParsingRequired_161 = value;
	}

	inline static int32_t get_offset_of_m_inputSource_162() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_inputSource_162)); }
	inline int32_t get_m_inputSource_162() const { return ___m_inputSource_162; }
	inline int32_t* get_address_of_m_inputSource_162() { return &___m_inputSource_162; }
	inline void set_m_inputSource_162(int32_t value)
	{
		___m_inputSource_162 = value;
	}

	inline static int32_t get_offset_of_old_text_163() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___old_text_163)); }
	inline String_t* get_old_text_163() const { return ___old_text_163; }
	inline String_t** get_address_of_old_text_163() { return &___old_text_163; }
	inline void set_old_text_163(String_t* value)
	{
		___old_text_163 = value;
		Il2CppCodeGenWriteBarrier((&___old_text_163), value);
	}

	inline static int32_t get_offset_of_m_fontScale_164() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontScale_164)); }
	inline float get_m_fontScale_164() const { return ___m_fontScale_164; }
	inline float* get_address_of_m_fontScale_164() { return &___m_fontScale_164; }
	inline void set_m_fontScale_164(float value)
	{
		___m_fontScale_164 = value;
	}

	inline static int32_t get_offset_of_m_fontScaleMultiplier_165() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontScaleMultiplier_165)); }
	inline float get_m_fontScaleMultiplier_165() const { return ___m_fontScaleMultiplier_165; }
	inline float* get_address_of_m_fontScaleMultiplier_165() { return &___m_fontScaleMultiplier_165; }
	inline void set_m_fontScaleMultiplier_165(float value)
	{
		___m_fontScaleMultiplier_165 = value;
	}

	inline static int32_t get_offset_of_m_htmlTag_166() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_htmlTag_166)); }
	inline CharU5BU5D_t3528271667* get_m_htmlTag_166() const { return ___m_htmlTag_166; }
	inline CharU5BU5D_t3528271667** get_address_of_m_htmlTag_166() { return &___m_htmlTag_166; }
	inline void set_m_htmlTag_166(CharU5BU5D_t3528271667* value)
	{
		___m_htmlTag_166 = value;
		Il2CppCodeGenWriteBarrier((&___m_htmlTag_166), value);
	}

	inline static int32_t get_offset_of_m_xmlAttribute_167() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_xmlAttribute_167)); }
	inline XML_TagAttributeU5BU5D_t284240280* get_m_xmlAttribute_167() const { return ___m_xmlAttribute_167; }
	inline XML_TagAttributeU5BU5D_t284240280** get_address_of_m_xmlAttribute_167() { return &___m_xmlAttribute_167; }
	inline void set_m_xmlAttribute_167(XML_TagAttributeU5BU5D_t284240280* value)
	{
		___m_xmlAttribute_167 = value;
		Il2CppCodeGenWriteBarrier((&___m_xmlAttribute_167), value);
	}

	inline static int32_t get_offset_of_m_attributeParameterValues_168() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_attributeParameterValues_168)); }
	inline SingleU5BU5D_t1444911251* get_m_attributeParameterValues_168() const { return ___m_attributeParameterValues_168; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_attributeParameterValues_168() { return &___m_attributeParameterValues_168; }
	inline void set_m_attributeParameterValues_168(SingleU5BU5D_t1444911251* value)
	{
		___m_attributeParameterValues_168 = value;
		Il2CppCodeGenWriteBarrier((&___m_attributeParameterValues_168), value);
	}

	inline static int32_t get_offset_of_tag_LineIndent_169() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___tag_LineIndent_169)); }
	inline float get_tag_LineIndent_169() const { return ___tag_LineIndent_169; }
	inline float* get_address_of_tag_LineIndent_169() { return &___tag_LineIndent_169; }
	inline void set_tag_LineIndent_169(float value)
	{
		___tag_LineIndent_169 = value;
	}

	inline static int32_t get_offset_of_tag_Indent_170() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___tag_Indent_170)); }
	inline float get_tag_Indent_170() const { return ___tag_Indent_170; }
	inline float* get_address_of_tag_Indent_170() { return &___tag_Indent_170; }
	inline void set_tag_Indent_170(float value)
	{
		___tag_Indent_170 = value;
	}

	inline static int32_t get_offset_of_m_indentStack_171() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_indentStack_171)); }
	inline TMP_XmlTagStack_1_t960921318  get_m_indentStack_171() const { return ___m_indentStack_171; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_m_indentStack_171() { return &___m_indentStack_171; }
	inline void set_m_indentStack_171(TMP_XmlTagStack_1_t960921318  value)
	{
		___m_indentStack_171 = value;
	}

	inline static int32_t get_offset_of_tag_NoParsing_172() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___tag_NoParsing_172)); }
	inline bool get_tag_NoParsing_172() const { return ___tag_NoParsing_172; }
	inline bool* get_address_of_tag_NoParsing_172() { return &___tag_NoParsing_172; }
	inline void set_tag_NoParsing_172(bool value)
	{
		___tag_NoParsing_172 = value;
	}

	inline static int32_t get_offset_of_m_isParsingText_173() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isParsingText_173)); }
	inline bool get_m_isParsingText_173() const { return ___m_isParsingText_173; }
	inline bool* get_address_of_m_isParsingText_173() { return &___m_isParsingText_173; }
	inline void set_m_isParsingText_173(bool value)
	{
		___m_isParsingText_173 = value;
	}

	inline static int32_t get_offset_of_m_FXMatrix_174() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_FXMatrix_174)); }
	inline Matrix4x4_t1817901843  get_m_FXMatrix_174() const { return ___m_FXMatrix_174; }
	inline Matrix4x4_t1817901843 * get_address_of_m_FXMatrix_174() { return &___m_FXMatrix_174; }
	inline void set_m_FXMatrix_174(Matrix4x4_t1817901843  value)
	{
		___m_FXMatrix_174 = value;
	}

	inline static int32_t get_offset_of_m_isFXMatrixSet_175() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isFXMatrixSet_175)); }
	inline bool get_m_isFXMatrixSet_175() const { return ___m_isFXMatrixSet_175; }
	inline bool* get_address_of_m_isFXMatrixSet_175() { return &___m_isFXMatrixSet_175; }
	inline void set_m_isFXMatrixSet_175(bool value)
	{
		___m_isFXMatrixSet_175 = value;
	}

	inline static int32_t get_offset_of_m_char_buffer_176() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_char_buffer_176)); }
	inline Int32U5BU5D_t385246372* get_m_char_buffer_176() const { return ___m_char_buffer_176; }
	inline Int32U5BU5D_t385246372** get_address_of_m_char_buffer_176() { return &___m_char_buffer_176; }
	inline void set_m_char_buffer_176(Int32U5BU5D_t385246372* value)
	{
		___m_char_buffer_176 = value;
		Il2CppCodeGenWriteBarrier((&___m_char_buffer_176), value);
	}

	inline static int32_t get_offset_of_m_internalCharacterInfo_177() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_internalCharacterInfo_177)); }
	inline TMP_CharacterInfoU5BU5D_t1930184704* get_m_internalCharacterInfo_177() const { return ___m_internalCharacterInfo_177; }
	inline TMP_CharacterInfoU5BU5D_t1930184704** get_address_of_m_internalCharacterInfo_177() { return &___m_internalCharacterInfo_177; }
	inline void set_m_internalCharacterInfo_177(TMP_CharacterInfoU5BU5D_t1930184704* value)
	{
		___m_internalCharacterInfo_177 = value;
		Il2CppCodeGenWriteBarrier((&___m_internalCharacterInfo_177), value);
	}

	inline static int32_t get_offset_of_m_input_CharArray_178() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_input_CharArray_178)); }
	inline CharU5BU5D_t3528271667* get_m_input_CharArray_178() const { return ___m_input_CharArray_178; }
	inline CharU5BU5D_t3528271667** get_address_of_m_input_CharArray_178() { return &___m_input_CharArray_178; }
	inline void set_m_input_CharArray_178(CharU5BU5D_t3528271667* value)
	{
		___m_input_CharArray_178 = value;
		Il2CppCodeGenWriteBarrier((&___m_input_CharArray_178), value);
	}

	inline static int32_t get_offset_of_m_charArray_Length_179() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_charArray_Length_179)); }
	inline int32_t get_m_charArray_Length_179() const { return ___m_charArray_Length_179; }
	inline int32_t* get_address_of_m_charArray_Length_179() { return &___m_charArray_Length_179; }
	inline void set_m_charArray_Length_179(int32_t value)
	{
		___m_charArray_Length_179 = value;
	}

	inline static int32_t get_offset_of_m_totalCharacterCount_180() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_totalCharacterCount_180)); }
	inline int32_t get_m_totalCharacterCount_180() const { return ___m_totalCharacterCount_180; }
	inline int32_t* get_address_of_m_totalCharacterCount_180() { return &___m_totalCharacterCount_180; }
	inline void set_m_totalCharacterCount_180(int32_t value)
	{
		___m_totalCharacterCount_180 = value;
	}

	inline static int32_t get_offset_of_m_SavedWordWrapState_181() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_SavedWordWrapState_181)); }
	inline WordWrapState_t341939652  get_m_SavedWordWrapState_181() const { return ___m_SavedWordWrapState_181; }
	inline WordWrapState_t341939652 * get_address_of_m_SavedWordWrapState_181() { return &___m_SavedWordWrapState_181; }
	inline void set_m_SavedWordWrapState_181(WordWrapState_t341939652  value)
	{
		___m_SavedWordWrapState_181 = value;
	}

	inline static int32_t get_offset_of_m_SavedLineState_182() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_SavedLineState_182)); }
	inline WordWrapState_t341939652  get_m_SavedLineState_182() const { return ___m_SavedLineState_182; }
	inline WordWrapState_t341939652 * get_address_of_m_SavedLineState_182() { return &___m_SavedLineState_182; }
	inline void set_m_SavedLineState_182(WordWrapState_t341939652  value)
	{
		___m_SavedLineState_182 = value;
	}

	inline static int32_t get_offset_of_m_characterCount_183() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_characterCount_183)); }
	inline int32_t get_m_characterCount_183() const { return ___m_characterCount_183; }
	inline int32_t* get_address_of_m_characterCount_183() { return &___m_characterCount_183; }
	inline void set_m_characterCount_183(int32_t value)
	{
		___m_characterCount_183 = value;
	}

	inline static int32_t get_offset_of_m_firstCharacterOfLine_184() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_firstCharacterOfLine_184)); }
	inline int32_t get_m_firstCharacterOfLine_184() const { return ___m_firstCharacterOfLine_184; }
	inline int32_t* get_address_of_m_firstCharacterOfLine_184() { return &___m_firstCharacterOfLine_184; }
	inline void set_m_firstCharacterOfLine_184(int32_t value)
	{
		___m_firstCharacterOfLine_184 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacterOfLine_185() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_firstVisibleCharacterOfLine_185)); }
	inline int32_t get_m_firstVisibleCharacterOfLine_185() const { return ___m_firstVisibleCharacterOfLine_185; }
	inline int32_t* get_address_of_m_firstVisibleCharacterOfLine_185() { return &___m_firstVisibleCharacterOfLine_185; }
	inline void set_m_firstVisibleCharacterOfLine_185(int32_t value)
	{
		___m_firstVisibleCharacterOfLine_185 = value;
	}

	inline static int32_t get_offset_of_m_lastCharacterOfLine_186() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lastCharacterOfLine_186)); }
	inline int32_t get_m_lastCharacterOfLine_186() const { return ___m_lastCharacterOfLine_186; }
	inline int32_t* get_address_of_m_lastCharacterOfLine_186() { return &___m_lastCharacterOfLine_186; }
	inline void set_m_lastCharacterOfLine_186(int32_t value)
	{
		___m_lastCharacterOfLine_186 = value;
	}

	inline static int32_t get_offset_of_m_lastVisibleCharacterOfLine_187() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lastVisibleCharacterOfLine_187)); }
	inline int32_t get_m_lastVisibleCharacterOfLine_187() const { return ___m_lastVisibleCharacterOfLine_187; }
	inline int32_t* get_address_of_m_lastVisibleCharacterOfLine_187() { return &___m_lastVisibleCharacterOfLine_187; }
	inline void set_m_lastVisibleCharacterOfLine_187(int32_t value)
	{
		___m_lastVisibleCharacterOfLine_187 = value;
	}

	inline static int32_t get_offset_of_m_lineNumber_188() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineNumber_188)); }
	inline int32_t get_m_lineNumber_188() const { return ___m_lineNumber_188; }
	inline int32_t* get_address_of_m_lineNumber_188() { return &___m_lineNumber_188; }
	inline void set_m_lineNumber_188(int32_t value)
	{
		___m_lineNumber_188 = value;
	}

	inline static int32_t get_offset_of_m_lineVisibleCharacterCount_189() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineVisibleCharacterCount_189)); }
	inline int32_t get_m_lineVisibleCharacterCount_189() const { return ___m_lineVisibleCharacterCount_189; }
	inline int32_t* get_address_of_m_lineVisibleCharacterCount_189() { return &___m_lineVisibleCharacterCount_189; }
	inline void set_m_lineVisibleCharacterCount_189(int32_t value)
	{
		___m_lineVisibleCharacterCount_189 = value;
	}

	inline static int32_t get_offset_of_m_pageNumber_190() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_pageNumber_190)); }
	inline int32_t get_m_pageNumber_190() const { return ___m_pageNumber_190; }
	inline int32_t* get_address_of_m_pageNumber_190() { return &___m_pageNumber_190; }
	inline void set_m_pageNumber_190(int32_t value)
	{
		___m_pageNumber_190 = value;
	}

	inline static int32_t get_offset_of_m_maxAscender_191() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxAscender_191)); }
	inline float get_m_maxAscender_191() const { return ___m_maxAscender_191; }
	inline float* get_address_of_m_maxAscender_191() { return &___m_maxAscender_191; }
	inline void set_m_maxAscender_191(float value)
	{
		___m_maxAscender_191 = value;
	}

	inline static int32_t get_offset_of_m_maxCapHeight_192() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxCapHeight_192)); }
	inline float get_m_maxCapHeight_192() const { return ___m_maxCapHeight_192; }
	inline float* get_address_of_m_maxCapHeight_192() { return &___m_maxCapHeight_192; }
	inline void set_m_maxCapHeight_192(float value)
	{
		___m_maxCapHeight_192 = value;
	}

	inline static int32_t get_offset_of_m_maxDescender_193() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxDescender_193)); }
	inline float get_m_maxDescender_193() const { return ___m_maxDescender_193; }
	inline float* get_address_of_m_maxDescender_193() { return &___m_maxDescender_193; }
	inline void set_m_maxDescender_193(float value)
	{
		___m_maxDescender_193 = value;
	}

	inline static int32_t get_offset_of_m_maxLineAscender_194() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxLineAscender_194)); }
	inline float get_m_maxLineAscender_194() const { return ___m_maxLineAscender_194; }
	inline float* get_address_of_m_maxLineAscender_194() { return &___m_maxLineAscender_194; }
	inline void set_m_maxLineAscender_194(float value)
	{
		___m_maxLineAscender_194 = value;
	}

	inline static int32_t get_offset_of_m_maxLineDescender_195() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxLineDescender_195)); }
	inline float get_m_maxLineDescender_195() const { return ___m_maxLineDescender_195; }
	inline float* get_address_of_m_maxLineDescender_195() { return &___m_maxLineDescender_195; }
	inline void set_m_maxLineDescender_195(float value)
	{
		___m_maxLineDescender_195 = value;
	}

	inline static int32_t get_offset_of_m_startOfLineAscender_196() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_startOfLineAscender_196)); }
	inline float get_m_startOfLineAscender_196() const { return ___m_startOfLineAscender_196; }
	inline float* get_address_of_m_startOfLineAscender_196() { return &___m_startOfLineAscender_196; }
	inline void set_m_startOfLineAscender_196(float value)
	{
		___m_startOfLineAscender_196 = value;
	}

	inline static int32_t get_offset_of_m_lineOffset_197() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineOffset_197)); }
	inline float get_m_lineOffset_197() const { return ___m_lineOffset_197; }
	inline float* get_address_of_m_lineOffset_197() { return &___m_lineOffset_197; }
	inline void set_m_lineOffset_197(float value)
	{
		___m_lineOffset_197 = value;
	}

	inline static int32_t get_offset_of_m_meshExtents_198() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_meshExtents_198)); }
	inline Extents_t3837212874  get_m_meshExtents_198() const { return ___m_meshExtents_198; }
	inline Extents_t3837212874 * get_address_of_m_meshExtents_198() { return &___m_meshExtents_198; }
	inline void set_m_meshExtents_198(Extents_t3837212874  value)
	{
		___m_meshExtents_198 = value;
	}

	inline static int32_t get_offset_of_m_htmlColor_199() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_htmlColor_199)); }
	inline Color32_t2600501292  get_m_htmlColor_199() const { return ___m_htmlColor_199; }
	inline Color32_t2600501292 * get_address_of_m_htmlColor_199() { return &___m_htmlColor_199; }
	inline void set_m_htmlColor_199(Color32_t2600501292  value)
	{
		___m_htmlColor_199 = value;
	}

	inline static int32_t get_offset_of_m_colorStack_200() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_colorStack_200)); }
	inline TMP_XmlTagStack_1_t2164155836  get_m_colorStack_200() const { return ___m_colorStack_200; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_m_colorStack_200() { return &___m_colorStack_200; }
	inline void set_m_colorStack_200(TMP_XmlTagStack_1_t2164155836  value)
	{
		___m_colorStack_200 = value;
	}

	inline static int32_t get_offset_of_m_underlineColorStack_201() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_underlineColorStack_201)); }
	inline TMP_XmlTagStack_1_t2164155836  get_m_underlineColorStack_201() const { return ___m_underlineColorStack_201; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_m_underlineColorStack_201() { return &___m_underlineColorStack_201; }
	inline void set_m_underlineColorStack_201(TMP_XmlTagStack_1_t2164155836  value)
	{
		___m_underlineColorStack_201 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColorStack_202() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_strikethroughColorStack_202)); }
	inline TMP_XmlTagStack_1_t2164155836  get_m_strikethroughColorStack_202() const { return ___m_strikethroughColorStack_202; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_m_strikethroughColorStack_202() { return &___m_strikethroughColorStack_202; }
	inline void set_m_strikethroughColorStack_202(TMP_XmlTagStack_1_t2164155836  value)
	{
		___m_strikethroughColorStack_202 = value;
	}

	inline static int32_t get_offset_of_m_highlightColorStack_203() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_highlightColorStack_203)); }
	inline TMP_XmlTagStack_1_t2164155836  get_m_highlightColorStack_203() const { return ___m_highlightColorStack_203; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_m_highlightColorStack_203() { return &___m_highlightColorStack_203; }
	inline void set_m_highlightColorStack_203(TMP_XmlTagStack_1_t2164155836  value)
	{
		___m_highlightColorStack_203 = value;
	}

	inline static int32_t get_offset_of_m_tabSpacing_204() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_tabSpacing_204)); }
	inline float get_m_tabSpacing_204() const { return ___m_tabSpacing_204; }
	inline float* get_address_of_m_tabSpacing_204() { return &___m_tabSpacing_204; }
	inline void set_m_tabSpacing_204(float value)
	{
		___m_tabSpacing_204 = value;
	}

	inline static int32_t get_offset_of_m_spacing_205() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spacing_205)); }
	inline float get_m_spacing_205() const { return ___m_spacing_205; }
	inline float* get_address_of_m_spacing_205() { return &___m_spacing_205; }
	inline void set_m_spacing_205(float value)
	{
		___m_spacing_205 = value;
	}

	inline static int32_t get_offset_of_m_styleStack_206() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_styleStack_206)); }
	inline TMP_XmlTagStack_1_t2514600297  get_m_styleStack_206() const { return ___m_styleStack_206; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_m_styleStack_206() { return &___m_styleStack_206; }
	inline void set_m_styleStack_206(TMP_XmlTagStack_1_t2514600297  value)
	{
		___m_styleStack_206 = value;
	}

	inline static int32_t get_offset_of_m_actionStack_207() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_actionStack_207)); }
	inline TMP_XmlTagStack_1_t2514600297  get_m_actionStack_207() const { return ___m_actionStack_207; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_m_actionStack_207() { return &___m_actionStack_207; }
	inline void set_m_actionStack_207(TMP_XmlTagStack_1_t2514600297  value)
	{
		___m_actionStack_207 = value;
	}

	inline static int32_t get_offset_of_m_padding_208() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_padding_208)); }
	inline float get_m_padding_208() const { return ___m_padding_208; }
	inline float* get_address_of_m_padding_208() { return &___m_padding_208; }
	inline void set_m_padding_208(float value)
	{
		___m_padding_208 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffset_209() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_baselineOffset_209)); }
	inline float get_m_baselineOffset_209() const { return ___m_baselineOffset_209; }
	inline float* get_address_of_m_baselineOffset_209() { return &___m_baselineOffset_209; }
	inline void set_m_baselineOffset_209(float value)
	{
		___m_baselineOffset_209 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffsetStack_210() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_baselineOffsetStack_210)); }
	inline TMP_XmlTagStack_1_t960921318  get_m_baselineOffsetStack_210() const { return ___m_baselineOffsetStack_210; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_m_baselineOffsetStack_210() { return &___m_baselineOffsetStack_210; }
	inline void set_m_baselineOffsetStack_210(TMP_XmlTagStack_1_t960921318  value)
	{
		___m_baselineOffsetStack_210 = value;
	}

	inline static int32_t get_offset_of_m_xAdvance_211() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_xAdvance_211)); }
	inline float get_m_xAdvance_211() const { return ___m_xAdvance_211; }
	inline float* get_address_of_m_xAdvance_211() { return &___m_xAdvance_211; }
	inline void set_m_xAdvance_211(float value)
	{
		___m_xAdvance_211 = value;
	}

	inline static int32_t get_offset_of_m_textElementType_212() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_textElementType_212)); }
	inline int32_t get_m_textElementType_212() const { return ___m_textElementType_212; }
	inline int32_t* get_address_of_m_textElementType_212() { return &___m_textElementType_212; }
	inline void set_m_textElementType_212(int32_t value)
	{
		___m_textElementType_212 = value;
	}

	inline static int32_t get_offset_of_m_cached_TextElement_213() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_cached_TextElement_213)); }
	inline TMP_TextElement_t129727469 * get_m_cached_TextElement_213() const { return ___m_cached_TextElement_213; }
	inline TMP_TextElement_t129727469 ** get_address_of_m_cached_TextElement_213() { return &___m_cached_TextElement_213; }
	inline void set_m_cached_TextElement_213(TMP_TextElement_t129727469 * value)
	{
		___m_cached_TextElement_213 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_TextElement_213), value);
	}

	inline static int32_t get_offset_of_m_cached_Underline_GlyphInfo_214() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_cached_Underline_GlyphInfo_214)); }
	inline TMP_Glyph_t581847833 * get_m_cached_Underline_GlyphInfo_214() const { return ___m_cached_Underline_GlyphInfo_214; }
	inline TMP_Glyph_t581847833 ** get_address_of_m_cached_Underline_GlyphInfo_214() { return &___m_cached_Underline_GlyphInfo_214; }
	inline void set_m_cached_Underline_GlyphInfo_214(TMP_Glyph_t581847833 * value)
	{
		___m_cached_Underline_GlyphInfo_214 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_Underline_GlyphInfo_214), value);
	}

	inline static int32_t get_offset_of_m_cached_Ellipsis_GlyphInfo_215() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_cached_Ellipsis_GlyphInfo_215)); }
	inline TMP_Glyph_t581847833 * get_m_cached_Ellipsis_GlyphInfo_215() const { return ___m_cached_Ellipsis_GlyphInfo_215; }
	inline TMP_Glyph_t581847833 ** get_address_of_m_cached_Ellipsis_GlyphInfo_215() { return &___m_cached_Ellipsis_GlyphInfo_215; }
	inline void set_m_cached_Ellipsis_GlyphInfo_215(TMP_Glyph_t581847833 * value)
	{
		___m_cached_Ellipsis_GlyphInfo_215 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_Ellipsis_GlyphInfo_215), value);
	}

	inline static int32_t get_offset_of_m_defaultSpriteAsset_216() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_defaultSpriteAsset_216)); }
	inline TMP_SpriteAsset_t484820633 * get_m_defaultSpriteAsset_216() const { return ___m_defaultSpriteAsset_216; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_defaultSpriteAsset_216() { return &___m_defaultSpriteAsset_216; }
	inline void set_m_defaultSpriteAsset_216(TMP_SpriteAsset_t484820633 * value)
	{
		___m_defaultSpriteAsset_216 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultSpriteAsset_216), value);
	}

	inline static int32_t get_offset_of_m_currentSpriteAsset_217() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentSpriteAsset_217)); }
	inline TMP_SpriteAsset_t484820633 * get_m_currentSpriteAsset_217() const { return ___m_currentSpriteAsset_217; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_currentSpriteAsset_217() { return &___m_currentSpriteAsset_217; }
	inline void set_m_currentSpriteAsset_217(TMP_SpriteAsset_t484820633 * value)
	{
		___m_currentSpriteAsset_217 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentSpriteAsset_217), value);
	}

	inline static int32_t get_offset_of_m_spriteCount_218() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteCount_218)); }
	inline int32_t get_m_spriteCount_218() const { return ___m_spriteCount_218; }
	inline int32_t* get_address_of_m_spriteCount_218() { return &___m_spriteCount_218; }
	inline void set_m_spriteCount_218(int32_t value)
	{
		___m_spriteCount_218 = value;
	}

	inline static int32_t get_offset_of_m_spriteIndex_219() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteIndex_219)); }
	inline int32_t get_m_spriteIndex_219() const { return ___m_spriteIndex_219; }
	inline int32_t* get_address_of_m_spriteIndex_219() { return &___m_spriteIndex_219; }
	inline void set_m_spriteIndex_219(int32_t value)
	{
		___m_spriteIndex_219 = value;
	}

	inline static int32_t get_offset_of_m_inlineGraphics_220() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_inlineGraphics_220)); }
	inline InlineGraphicManager_t2871008645 * get_m_inlineGraphics_220() const { return ___m_inlineGraphics_220; }
	inline InlineGraphicManager_t2871008645 ** get_address_of_m_inlineGraphics_220() { return &___m_inlineGraphics_220; }
	inline void set_m_inlineGraphics_220(InlineGraphicManager_t2871008645 * value)
	{
		___m_inlineGraphics_220 = value;
		Il2CppCodeGenWriteBarrier((&___m_inlineGraphics_220), value);
	}

	inline static int32_t get_offset_of_m_spriteAnimationID_221() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteAnimationID_221)); }
	inline int32_t get_m_spriteAnimationID_221() const { return ___m_spriteAnimationID_221; }
	inline int32_t* get_address_of_m_spriteAnimationID_221() { return &___m_spriteAnimationID_221; }
	inline void set_m_spriteAnimationID_221(int32_t value)
	{
		___m_spriteAnimationID_221 = value;
	}

	inline static int32_t get_offset_of_m_ignoreActiveState_222() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_ignoreActiveState_222)); }
	inline bool get_m_ignoreActiveState_222() const { return ___m_ignoreActiveState_222; }
	inline bool* get_address_of_m_ignoreActiveState_222() { return &___m_ignoreActiveState_222; }
	inline void set_m_ignoreActiveState_222(bool value)
	{
		___m_ignoreActiveState_222 = value;
	}

	inline static int32_t get_offset_of_k_Power_223() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___k_Power_223)); }
	inline SingleU5BU5D_t1444911251* get_k_Power_223() const { return ___k_Power_223; }
	inline SingleU5BU5D_t1444911251** get_address_of_k_Power_223() { return &___k_Power_223; }
	inline void set_k_Power_223(SingleU5BU5D_t1444911251* value)
	{
		___k_Power_223 = value;
		Il2CppCodeGenWriteBarrier((&___k_Power_223), value);
	}
};

struct TMP_Text_t2599618874_StaticFields
{
public:
	// UnityEngine.Color32 TMPro.TMP_Text::s_colorWhite
	Color32_t2600501292  ___s_colorWhite_45;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargePositiveVector2
	Vector2_t2156229523  ___k_LargePositiveVector2_224;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargeNegativeVector2
	Vector2_t2156229523  ___k_LargeNegativeVector2_225;
	// System.Single TMPro.TMP_Text::k_LargePositiveFloat
	float ___k_LargePositiveFloat_226;
	// System.Single TMPro.TMP_Text::k_LargeNegativeFloat
	float ___k_LargeNegativeFloat_227;
	// System.Int32 TMPro.TMP_Text::k_LargePositiveInt
	int32_t ___k_LargePositiveInt_228;
	// System.Int32 TMPro.TMP_Text::k_LargeNegativeInt
	int32_t ___k_LargeNegativeInt_229;

public:
	inline static int32_t get_offset_of_s_colorWhite_45() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___s_colorWhite_45)); }
	inline Color32_t2600501292  get_s_colorWhite_45() const { return ___s_colorWhite_45; }
	inline Color32_t2600501292 * get_address_of_s_colorWhite_45() { return &___s_colorWhite_45; }
	inline void set_s_colorWhite_45(Color32_t2600501292  value)
	{
		___s_colorWhite_45 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveVector2_224() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargePositiveVector2_224)); }
	inline Vector2_t2156229523  get_k_LargePositiveVector2_224() const { return ___k_LargePositiveVector2_224; }
	inline Vector2_t2156229523 * get_address_of_k_LargePositiveVector2_224() { return &___k_LargePositiveVector2_224; }
	inline void set_k_LargePositiveVector2_224(Vector2_t2156229523  value)
	{
		___k_LargePositiveVector2_224 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeVector2_225() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargeNegativeVector2_225)); }
	inline Vector2_t2156229523  get_k_LargeNegativeVector2_225() const { return ___k_LargeNegativeVector2_225; }
	inline Vector2_t2156229523 * get_address_of_k_LargeNegativeVector2_225() { return &___k_LargeNegativeVector2_225; }
	inline void set_k_LargeNegativeVector2_225(Vector2_t2156229523  value)
	{
		___k_LargeNegativeVector2_225 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveFloat_226() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargePositiveFloat_226)); }
	inline float get_k_LargePositiveFloat_226() const { return ___k_LargePositiveFloat_226; }
	inline float* get_address_of_k_LargePositiveFloat_226() { return &___k_LargePositiveFloat_226; }
	inline void set_k_LargePositiveFloat_226(float value)
	{
		___k_LargePositiveFloat_226 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeFloat_227() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargeNegativeFloat_227)); }
	inline float get_k_LargeNegativeFloat_227() const { return ___k_LargeNegativeFloat_227; }
	inline float* get_address_of_k_LargeNegativeFloat_227() { return &___k_LargeNegativeFloat_227; }
	inline void set_k_LargeNegativeFloat_227(float value)
	{
		___k_LargeNegativeFloat_227 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveInt_228() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargePositiveInt_228)); }
	inline int32_t get_k_LargePositiveInt_228() const { return ___k_LargePositiveInt_228; }
	inline int32_t* get_address_of_k_LargePositiveInt_228() { return &___k_LargePositiveInt_228; }
	inline void set_k_LargePositiveInt_228(int32_t value)
	{
		___k_LargePositiveInt_228 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeInt_229() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargeNegativeInt_229)); }
	inline int32_t get_k_LargeNegativeInt_229() const { return ___k_LargeNegativeInt_229; }
	inline int32_t* get_address_of_k_LargeNegativeInt_229() { return &___k_LargeNegativeInt_229; }
	inline void set_k_LargeNegativeInt_229(int32_t value)
	{
		___k_LargeNegativeInt_229 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXT_T2599618874_H
#ifndef TEXTMESHPROUGUI_T529313277_H
#define TEXTMESHPROUGUI_T529313277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextMeshProUGUI
struct  TextMeshProUGUI_t529313277  : public TMP_Text_t2599618874
{
public:
	// System.Boolean TMPro.TextMeshProUGUI::m_hasFontAssetChanged
	bool ___m_hasFontAssetChanged_230;
	// TMPro.TMP_SubMeshUI[] TMPro.TextMeshProUGUI::m_subTextObjects
	TMP_SubMeshUIU5BU5D_t3119380694* ___m_subTextObjects_231;
	// System.Single TMPro.TextMeshProUGUI::m_previousLossyScaleY
	float ___m_previousLossyScaleY_232;
	// UnityEngine.Vector3[] TMPro.TextMeshProUGUI::m_RectTransformCorners
	Vector3U5BU5D_t1718750761* ___m_RectTransformCorners_233;
	// UnityEngine.CanvasRenderer TMPro.TextMeshProUGUI::m_canvasRenderer
	CanvasRenderer_t2598313366 * ___m_canvasRenderer_234;
	// UnityEngine.Canvas TMPro.TextMeshProUGUI::m_canvas
	Canvas_t3310196443 * ___m_canvas_235;
	// System.Boolean TMPro.TextMeshProUGUI::m_isFirstAllocation
	bool ___m_isFirstAllocation_236;
	// System.Int32 TMPro.TextMeshProUGUI::m_max_characters
	int32_t ___m_max_characters_237;
	// System.Boolean TMPro.TextMeshProUGUI::m_isMaskingEnabled
	bool ___m_isMaskingEnabled_238;
	// UnityEngine.Material TMPro.TextMeshProUGUI::m_baseMaterial
	Material_t340375123 * ___m_baseMaterial_239;
	// System.Boolean TMPro.TextMeshProUGUI::m_isScrollRegionSet
	bool ___m_isScrollRegionSet_240;
	// System.Int32 TMPro.TextMeshProUGUI::m_stencilID
	int32_t ___m_stencilID_241;
	// UnityEngine.Vector4 TMPro.TextMeshProUGUI::m_maskOffset
	Vector4_t3319028937  ___m_maskOffset_242;
	// UnityEngine.Matrix4x4 TMPro.TextMeshProUGUI::m_EnvMapMatrix
	Matrix4x4_t1817901843  ___m_EnvMapMatrix_243;
	// System.Boolean TMPro.TextMeshProUGUI::m_isRegisteredForEvents
	bool ___m_isRegisteredForEvents_244;
	// System.Int32 TMPro.TextMeshProUGUI::m_recursiveCountA
	int32_t ___m_recursiveCountA_245;
	// System.Int32 TMPro.TextMeshProUGUI::loopCountA
	int32_t ___loopCountA_246;
	// System.Boolean TMPro.TextMeshProUGUI::m_isRebuildingLayout
	bool ___m_isRebuildingLayout_247;

public:
	inline static int32_t get_offset_of_m_hasFontAssetChanged_230() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_hasFontAssetChanged_230)); }
	inline bool get_m_hasFontAssetChanged_230() const { return ___m_hasFontAssetChanged_230; }
	inline bool* get_address_of_m_hasFontAssetChanged_230() { return &___m_hasFontAssetChanged_230; }
	inline void set_m_hasFontAssetChanged_230(bool value)
	{
		___m_hasFontAssetChanged_230 = value;
	}

	inline static int32_t get_offset_of_m_subTextObjects_231() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_subTextObjects_231)); }
	inline TMP_SubMeshUIU5BU5D_t3119380694* get_m_subTextObjects_231() const { return ___m_subTextObjects_231; }
	inline TMP_SubMeshUIU5BU5D_t3119380694** get_address_of_m_subTextObjects_231() { return &___m_subTextObjects_231; }
	inline void set_m_subTextObjects_231(TMP_SubMeshUIU5BU5D_t3119380694* value)
	{
		___m_subTextObjects_231 = value;
		Il2CppCodeGenWriteBarrier((&___m_subTextObjects_231), value);
	}

	inline static int32_t get_offset_of_m_previousLossyScaleY_232() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_previousLossyScaleY_232)); }
	inline float get_m_previousLossyScaleY_232() const { return ___m_previousLossyScaleY_232; }
	inline float* get_address_of_m_previousLossyScaleY_232() { return &___m_previousLossyScaleY_232; }
	inline void set_m_previousLossyScaleY_232(float value)
	{
		___m_previousLossyScaleY_232 = value;
	}

	inline static int32_t get_offset_of_m_RectTransformCorners_233() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_RectTransformCorners_233)); }
	inline Vector3U5BU5D_t1718750761* get_m_RectTransformCorners_233() const { return ___m_RectTransformCorners_233; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_RectTransformCorners_233() { return &___m_RectTransformCorners_233; }
	inline void set_m_RectTransformCorners_233(Vector3U5BU5D_t1718750761* value)
	{
		___m_RectTransformCorners_233 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransformCorners_233), value);
	}

	inline static int32_t get_offset_of_m_canvasRenderer_234() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_canvasRenderer_234)); }
	inline CanvasRenderer_t2598313366 * get_m_canvasRenderer_234() const { return ___m_canvasRenderer_234; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_canvasRenderer_234() { return &___m_canvasRenderer_234; }
	inline void set_m_canvasRenderer_234(CanvasRenderer_t2598313366 * value)
	{
		___m_canvasRenderer_234 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvasRenderer_234), value);
	}

	inline static int32_t get_offset_of_m_canvas_235() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_canvas_235)); }
	inline Canvas_t3310196443 * get_m_canvas_235() const { return ___m_canvas_235; }
	inline Canvas_t3310196443 ** get_address_of_m_canvas_235() { return &___m_canvas_235; }
	inline void set_m_canvas_235(Canvas_t3310196443 * value)
	{
		___m_canvas_235 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvas_235), value);
	}

	inline static int32_t get_offset_of_m_isFirstAllocation_236() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_isFirstAllocation_236)); }
	inline bool get_m_isFirstAllocation_236() const { return ___m_isFirstAllocation_236; }
	inline bool* get_address_of_m_isFirstAllocation_236() { return &___m_isFirstAllocation_236; }
	inline void set_m_isFirstAllocation_236(bool value)
	{
		___m_isFirstAllocation_236 = value;
	}

	inline static int32_t get_offset_of_m_max_characters_237() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_max_characters_237)); }
	inline int32_t get_m_max_characters_237() const { return ___m_max_characters_237; }
	inline int32_t* get_address_of_m_max_characters_237() { return &___m_max_characters_237; }
	inline void set_m_max_characters_237(int32_t value)
	{
		___m_max_characters_237 = value;
	}

	inline static int32_t get_offset_of_m_isMaskingEnabled_238() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_isMaskingEnabled_238)); }
	inline bool get_m_isMaskingEnabled_238() const { return ___m_isMaskingEnabled_238; }
	inline bool* get_address_of_m_isMaskingEnabled_238() { return &___m_isMaskingEnabled_238; }
	inline void set_m_isMaskingEnabled_238(bool value)
	{
		___m_isMaskingEnabled_238 = value;
	}

	inline static int32_t get_offset_of_m_baseMaterial_239() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_baseMaterial_239)); }
	inline Material_t340375123 * get_m_baseMaterial_239() const { return ___m_baseMaterial_239; }
	inline Material_t340375123 ** get_address_of_m_baseMaterial_239() { return &___m_baseMaterial_239; }
	inline void set_m_baseMaterial_239(Material_t340375123 * value)
	{
		___m_baseMaterial_239 = value;
		Il2CppCodeGenWriteBarrier((&___m_baseMaterial_239), value);
	}

	inline static int32_t get_offset_of_m_isScrollRegionSet_240() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_isScrollRegionSet_240)); }
	inline bool get_m_isScrollRegionSet_240() const { return ___m_isScrollRegionSet_240; }
	inline bool* get_address_of_m_isScrollRegionSet_240() { return &___m_isScrollRegionSet_240; }
	inline void set_m_isScrollRegionSet_240(bool value)
	{
		___m_isScrollRegionSet_240 = value;
	}

	inline static int32_t get_offset_of_m_stencilID_241() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_stencilID_241)); }
	inline int32_t get_m_stencilID_241() const { return ___m_stencilID_241; }
	inline int32_t* get_address_of_m_stencilID_241() { return &___m_stencilID_241; }
	inline void set_m_stencilID_241(int32_t value)
	{
		___m_stencilID_241 = value;
	}

	inline static int32_t get_offset_of_m_maskOffset_242() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_maskOffset_242)); }
	inline Vector4_t3319028937  get_m_maskOffset_242() const { return ___m_maskOffset_242; }
	inline Vector4_t3319028937 * get_address_of_m_maskOffset_242() { return &___m_maskOffset_242; }
	inline void set_m_maskOffset_242(Vector4_t3319028937  value)
	{
		___m_maskOffset_242 = value;
	}

	inline static int32_t get_offset_of_m_EnvMapMatrix_243() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_EnvMapMatrix_243)); }
	inline Matrix4x4_t1817901843  get_m_EnvMapMatrix_243() const { return ___m_EnvMapMatrix_243; }
	inline Matrix4x4_t1817901843 * get_address_of_m_EnvMapMatrix_243() { return &___m_EnvMapMatrix_243; }
	inline void set_m_EnvMapMatrix_243(Matrix4x4_t1817901843  value)
	{
		___m_EnvMapMatrix_243 = value;
	}

	inline static int32_t get_offset_of_m_isRegisteredForEvents_244() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_isRegisteredForEvents_244)); }
	inline bool get_m_isRegisteredForEvents_244() const { return ___m_isRegisteredForEvents_244; }
	inline bool* get_address_of_m_isRegisteredForEvents_244() { return &___m_isRegisteredForEvents_244; }
	inline void set_m_isRegisteredForEvents_244(bool value)
	{
		___m_isRegisteredForEvents_244 = value;
	}

	inline static int32_t get_offset_of_m_recursiveCountA_245() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_recursiveCountA_245)); }
	inline int32_t get_m_recursiveCountA_245() const { return ___m_recursiveCountA_245; }
	inline int32_t* get_address_of_m_recursiveCountA_245() { return &___m_recursiveCountA_245; }
	inline void set_m_recursiveCountA_245(int32_t value)
	{
		___m_recursiveCountA_245 = value;
	}

	inline static int32_t get_offset_of_loopCountA_246() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___loopCountA_246)); }
	inline int32_t get_loopCountA_246() const { return ___loopCountA_246; }
	inline int32_t* get_address_of_loopCountA_246() { return &___loopCountA_246; }
	inline void set_loopCountA_246(int32_t value)
	{
		___loopCountA_246 = value;
	}

	inline static int32_t get_offset_of_m_isRebuildingLayout_247() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_isRebuildingLayout_247)); }
	inline bool get_m_isRebuildingLayout_247() const { return ___m_isRebuildingLayout_247; }
	inline bool* get_address_of_m_isRebuildingLayout_247() { return &___m_isRebuildingLayout_247; }
	inline void set_m_isRebuildingLayout_247(bool value)
	{
		___m_isRebuildingLayout_247 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESHPROUGUI_T529313277_H
#ifndef TEXTMESHPRO_T2393593166_H
#define TEXTMESHPRO_T2393593166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextMeshPro
struct  TextMeshPro_t2393593166  : public TMP_Text_t2599618874
{
public:
	// System.Boolean TMPro.TextMeshPro::m_hasFontAssetChanged
	bool ___m_hasFontAssetChanged_230;
	// System.Single TMPro.TextMeshPro::m_previousLossyScaleY
	float ___m_previousLossyScaleY_231;
	// UnityEngine.Renderer TMPro.TextMeshPro::m_renderer
	Renderer_t2627027031 * ___m_renderer_232;
	// UnityEngine.MeshFilter TMPro.TextMeshPro::m_meshFilter
	MeshFilter_t3523625662 * ___m_meshFilter_233;
	// System.Boolean TMPro.TextMeshPro::m_isFirstAllocation
	bool ___m_isFirstAllocation_234;
	// System.Int32 TMPro.TextMeshPro::m_max_characters
	int32_t ___m_max_characters_235;
	// System.Int32 TMPro.TextMeshPro::m_max_numberOfLines
	int32_t ___m_max_numberOfLines_236;
	// UnityEngine.Bounds TMPro.TextMeshPro::m_default_bounds
	Bounds_t2266837910  ___m_default_bounds_237;
	// TMPro.TMP_SubMesh[] TMPro.TextMeshPro::m_subTextObjects
	TMP_SubMeshU5BU5D_t1860744960* ___m_subTextObjects_238;
	// System.Boolean TMPro.TextMeshPro::m_isMaskingEnabled
	bool ___m_isMaskingEnabled_239;
	// System.Boolean TMPro.TextMeshPro::isMaskUpdateRequired
	bool ___isMaskUpdateRequired_240;
	// TMPro.MaskingTypes TMPro.TextMeshPro::m_maskType
	int32_t ___m_maskType_241;
	// UnityEngine.Matrix4x4 TMPro.TextMeshPro::m_EnvMapMatrix
	Matrix4x4_t1817901843  ___m_EnvMapMatrix_242;
	// UnityEngine.Vector3[] TMPro.TextMeshPro::m_RectTransformCorners
	Vector3U5BU5D_t1718750761* ___m_RectTransformCorners_243;
	// System.Boolean TMPro.TextMeshPro::m_isRegisteredForEvents
	bool ___m_isRegisteredForEvents_244;
	// System.Int32 TMPro.TextMeshPro::loopCountA
	int32_t ___loopCountA_245;
	// System.Boolean TMPro.TextMeshPro::m_currentAutoSizeMode
	bool ___m_currentAutoSizeMode_246;

public:
	inline static int32_t get_offset_of_m_hasFontAssetChanged_230() { return static_cast<int32_t>(offsetof(TextMeshPro_t2393593166, ___m_hasFontAssetChanged_230)); }
	inline bool get_m_hasFontAssetChanged_230() const { return ___m_hasFontAssetChanged_230; }
	inline bool* get_address_of_m_hasFontAssetChanged_230() { return &___m_hasFontAssetChanged_230; }
	inline void set_m_hasFontAssetChanged_230(bool value)
	{
		___m_hasFontAssetChanged_230 = value;
	}

	inline static int32_t get_offset_of_m_previousLossyScaleY_231() { return static_cast<int32_t>(offsetof(TextMeshPro_t2393593166, ___m_previousLossyScaleY_231)); }
	inline float get_m_previousLossyScaleY_231() const { return ___m_previousLossyScaleY_231; }
	inline float* get_address_of_m_previousLossyScaleY_231() { return &___m_previousLossyScaleY_231; }
	inline void set_m_previousLossyScaleY_231(float value)
	{
		___m_previousLossyScaleY_231 = value;
	}

	inline static int32_t get_offset_of_m_renderer_232() { return static_cast<int32_t>(offsetof(TextMeshPro_t2393593166, ___m_renderer_232)); }
	inline Renderer_t2627027031 * get_m_renderer_232() const { return ___m_renderer_232; }
	inline Renderer_t2627027031 ** get_address_of_m_renderer_232() { return &___m_renderer_232; }
	inline void set_m_renderer_232(Renderer_t2627027031 * value)
	{
		___m_renderer_232 = value;
		Il2CppCodeGenWriteBarrier((&___m_renderer_232), value);
	}

	inline static int32_t get_offset_of_m_meshFilter_233() { return static_cast<int32_t>(offsetof(TextMeshPro_t2393593166, ___m_meshFilter_233)); }
	inline MeshFilter_t3523625662 * get_m_meshFilter_233() const { return ___m_meshFilter_233; }
	inline MeshFilter_t3523625662 ** get_address_of_m_meshFilter_233() { return &___m_meshFilter_233; }
	inline void set_m_meshFilter_233(MeshFilter_t3523625662 * value)
	{
		___m_meshFilter_233 = value;
		Il2CppCodeGenWriteBarrier((&___m_meshFilter_233), value);
	}

	inline static int32_t get_offset_of_m_isFirstAllocation_234() { return static_cast<int32_t>(offsetof(TextMeshPro_t2393593166, ___m_isFirstAllocation_234)); }
	inline bool get_m_isFirstAllocation_234() const { return ___m_isFirstAllocation_234; }
	inline bool* get_address_of_m_isFirstAllocation_234() { return &___m_isFirstAllocation_234; }
	inline void set_m_isFirstAllocation_234(bool value)
	{
		___m_isFirstAllocation_234 = value;
	}

	inline static int32_t get_offset_of_m_max_characters_235() { return static_cast<int32_t>(offsetof(TextMeshPro_t2393593166, ___m_max_characters_235)); }
	inline int32_t get_m_max_characters_235() const { return ___m_max_characters_235; }
	inline int32_t* get_address_of_m_max_characters_235() { return &___m_max_characters_235; }
	inline void set_m_max_characters_235(int32_t value)
	{
		___m_max_characters_235 = value;
	}

	inline static int32_t get_offset_of_m_max_numberOfLines_236() { return static_cast<int32_t>(offsetof(TextMeshPro_t2393593166, ___m_max_numberOfLines_236)); }
	inline int32_t get_m_max_numberOfLines_236() const { return ___m_max_numberOfLines_236; }
	inline int32_t* get_address_of_m_max_numberOfLines_236() { return &___m_max_numberOfLines_236; }
	inline void set_m_max_numberOfLines_236(int32_t value)
	{
		___m_max_numberOfLines_236 = value;
	}

	inline static int32_t get_offset_of_m_default_bounds_237() { return static_cast<int32_t>(offsetof(TextMeshPro_t2393593166, ___m_default_bounds_237)); }
	inline Bounds_t2266837910  get_m_default_bounds_237() const { return ___m_default_bounds_237; }
	inline Bounds_t2266837910 * get_address_of_m_default_bounds_237() { return &___m_default_bounds_237; }
	inline void set_m_default_bounds_237(Bounds_t2266837910  value)
	{
		___m_default_bounds_237 = value;
	}

	inline static int32_t get_offset_of_m_subTextObjects_238() { return static_cast<int32_t>(offsetof(TextMeshPro_t2393593166, ___m_subTextObjects_238)); }
	inline TMP_SubMeshU5BU5D_t1860744960* get_m_subTextObjects_238() const { return ___m_subTextObjects_238; }
	inline TMP_SubMeshU5BU5D_t1860744960** get_address_of_m_subTextObjects_238() { return &___m_subTextObjects_238; }
	inline void set_m_subTextObjects_238(TMP_SubMeshU5BU5D_t1860744960* value)
	{
		___m_subTextObjects_238 = value;
		Il2CppCodeGenWriteBarrier((&___m_subTextObjects_238), value);
	}

	inline static int32_t get_offset_of_m_isMaskingEnabled_239() { return static_cast<int32_t>(offsetof(TextMeshPro_t2393593166, ___m_isMaskingEnabled_239)); }
	inline bool get_m_isMaskingEnabled_239() const { return ___m_isMaskingEnabled_239; }
	inline bool* get_address_of_m_isMaskingEnabled_239() { return &___m_isMaskingEnabled_239; }
	inline void set_m_isMaskingEnabled_239(bool value)
	{
		___m_isMaskingEnabled_239 = value;
	}

	inline static int32_t get_offset_of_isMaskUpdateRequired_240() { return static_cast<int32_t>(offsetof(TextMeshPro_t2393593166, ___isMaskUpdateRequired_240)); }
	inline bool get_isMaskUpdateRequired_240() const { return ___isMaskUpdateRequired_240; }
	inline bool* get_address_of_isMaskUpdateRequired_240() { return &___isMaskUpdateRequired_240; }
	inline void set_isMaskUpdateRequired_240(bool value)
	{
		___isMaskUpdateRequired_240 = value;
	}

	inline static int32_t get_offset_of_m_maskType_241() { return static_cast<int32_t>(offsetof(TextMeshPro_t2393593166, ___m_maskType_241)); }
	inline int32_t get_m_maskType_241() const { return ___m_maskType_241; }
	inline int32_t* get_address_of_m_maskType_241() { return &___m_maskType_241; }
	inline void set_m_maskType_241(int32_t value)
	{
		___m_maskType_241 = value;
	}

	inline static int32_t get_offset_of_m_EnvMapMatrix_242() { return static_cast<int32_t>(offsetof(TextMeshPro_t2393593166, ___m_EnvMapMatrix_242)); }
	inline Matrix4x4_t1817901843  get_m_EnvMapMatrix_242() const { return ___m_EnvMapMatrix_242; }
	inline Matrix4x4_t1817901843 * get_address_of_m_EnvMapMatrix_242() { return &___m_EnvMapMatrix_242; }
	inline void set_m_EnvMapMatrix_242(Matrix4x4_t1817901843  value)
	{
		___m_EnvMapMatrix_242 = value;
	}

	inline static int32_t get_offset_of_m_RectTransformCorners_243() { return static_cast<int32_t>(offsetof(TextMeshPro_t2393593166, ___m_RectTransformCorners_243)); }
	inline Vector3U5BU5D_t1718750761* get_m_RectTransformCorners_243() const { return ___m_RectTransformCorners_243; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_RectTransformCorners_243() { return &___m_RectTransformCorners_243; }
	inline void set_m_RectTransformCorners_243(Vector3U5BU5D_t1718750761* value)
	{
		___m_RectTransformCorners_243 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransformCorners_243), value);
	}

	inline static int32_t get_offset_of_m_isRegisteredForEvents_244() { return static_cast<int32_t>(offsetof(TextMeshPro_t2393593166, ___m_isRegisteredForEvents_244)); }
	inline bool get_m_isRegisteredForEvents_244() const { return ___m_isRegisteredForEvents_244; }
	inline bool* get_address_of_m_isRegisteredForEvents_244() { return &___m_isRegisteredForEvents_244; }
	inline void set_m_isRegisteredForEvents_244(bool value)
	{
		___m_isRegisteredForEvents_244 = value;
	}

	inline static int32_t get_offset_of_loopCountA_245() { return static_cast<int32_t>(offsetof(TextMeshPro_t2393593166, ___loopCountA_245)); }
	inline int32_t get_loopCountA_245() const { return ___loopCountA_245; }
	inline int32_t* get_address_of_loopCountA_245() { return &___loopCountA_245; }
	inline void set_loopCountA_245(int32_t value)
	{
		___loopCountA_245 = value;
	}

	inline static int32_t get_offset_of_m_currentAutoSizeMode_246() { return static_cast<int32_t>(offsetof(TextMeshPro_t2393593166, ___m_currentAutoSizeMode_246)); }
	inline bool get_m_currentAutoSizeMode_246() const { return ___m_currentAutoSizeMode_246; }
	inline bool* get_address_of_m_currentAutoSizeMode_246() { return &___m_currentAutoSizeMode_246; }
	inline void set_m_currentAutoSizeMode_246(bool value)
	{
		___m_currentAutoSizeMode_246 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESHPRO_T2393593166_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (FontWeights_t3122883458)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2100[10] = 
{
	FontWeights_t3122883458::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (TagUnits_t1169424683)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2101[4] = 
{
	TagUnits_t1169424683::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (TagType_t123236451)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2102[5] = 
{
	TagType_t123236451::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (TMP_Text_t2599618874), -1, sizeof(TMP_Text_t2599618874_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2103[202] = 
{
	TMP_Text_t2599618874::get_offset_of_m_text_28(),
	TMP_Text_t2599618874::get_offset_of_m_isRightToLeft_29(),
	TMP_Text_t2599618874::get_offset_of_m_fontAsset_30(),
	TMP_Text_t2599618874::get_offset_of_m_currentFontAsset_31(),
	TMP_Text_t2599618874::get_offset_of_m_isSDFShader_32(),
	TMP_Text_t2599618874::get_offset_of_m_sharedMaterial_33(),
	TMP_Text_t2599618874::get_offset_of_m_currentMaterial_34(),
	TMP_Text_t2599618874::get_offset_of_m_materialReferences_35(),
	TMP_Text_t2599618874::get_offset_of_m_materialReferenceIndexLookup_36(),
	TMP_Text_t2599618874::get_offset_of_m_materialReferenceStack_37(),
	TMP_Text_t2599618874::get_offset_of_m_currentMaterialIndex_38(),
	TMP_Text_t2599618874::get_offset_of_m_fontSharedMaterials_39(),
	TMP_Text_t2599618874::get_offset_of_m_fontMaterial_40(),
	TMP_Text_t2599618874::get_offset_of_m_fontMaterials_41(),
	TMP_Text_t2599618874::get_offset_of_m_isMaterialDirty_42(),
	TMP_Text_t2599618874::get_offset_of_m_fontColor32_43(),
	TMP_Text_t2599618874::get_offset_of_m_fontColor_44(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_s_colorWhite_45(),
	TMP_Text_t2599618874::get_offset_of_m_underlineColor_46(),
	TMP_Text_t2599618874::get_offset_of_m_strikethroughColor_47(),
	TMP_Text_t2599618874::get_offset_of_m_highlightColor_48(),
	TMP_Text_t2599618874::get_offset_of_m_enableVertexGradient_49(),
	TMP_Text_t2599618874::get_offset_of_m_fontColorGradient_50(),
	TMP_Text_t2599618874::get_offset_of_m_fontColorGradientPreset_51(),
	TMP_Text_t2599618874::get_offset_of_m_spriteAsset_52(),
	TMP_Text_t2599618874::get_offset_of_m_tintAllSprites_53(),
	TMP_Text_t2599618874::get_offset_of_m_tintSprite_54(),
	TMP_Text_t2599618874::get_offset_of_m_spriteColor_55(),
	TMP_Text_t2599618874::get_offset_of_m_overrideHtmlColors_56(),
	TMP_Text_t2599618874::get_offset_of_m_faceColor_57(),
	TMP_Text_t2599618874::get_offset_of_m_outlineColor_58(),
	TMP_Text_t2599618874::get_offset_of_m_outlineWidth_59(),
	TMP_Text_t2599618874::get_offset_of_m_fontSize_60(),
	TMP_Text_t2599618874::get_offset_of_m_currentFontSize_61(),
	TMP_Text_t2599618874::get_offset_of_m_fontSizeBase_62(),
	TMP_Text_t2599618874::get_offset_of_m_sizeStack_63(),
	TMP_Text_t2599618874::get_offset_of_m_fontWeight_64(),
	TMP_Text_t2599618874::get_offset_of_m_fontWeightInternal_65(),
	TMP_Text_t2599618874::get_offset_of_m_fontWeightStack_66(),
	TMP_Text_t2599618874::get_offset_of_m_enableAutoSizing_67(),
	TMP_Text_t2599618874::get_offset_of_m_maxFontSize_68(),
	TMP_Text_t2599618874::get_offset_of_m_minFontSize_69(),
	TMP_Text_t2599618874::get_offset_of_m_fontSizeMin_70(),
	TMP_Text_t2599618874::get_offset_of_m_fontSizeMax_71(),
	TMP_Text_t2599618874::get_offset_of_m_fontStyle_72(),
	TMP_Text_t2599618874::get_offset_of_m_style_73(),
	TMP_Text_t2599618874::get_offset_of_m_fontStyleStack_74(),
	TMP_Text_t2599618874::get_offset_of_m_isUsingBold_75(),
	TMP_Text_t2599618874::get_offset_of_m_textAlignment_76(),
	TMP_Text_t2599618874::get_offset_of_m_lineJustification_77(),
	TMP_Text_t2599618874::get_offset_of_m_lineJustificationStack_78(),
	TMP_Text_t2599618874::get_offset_of_m_textContainerLocalCorners_79(),
	TMP_Text_t2599618874::get_offset_of_m_isAlignmentEnumConverted_80(),
	TMP_Text_t2599618874::get_offset_of_m_characterSpacing_81(),
	TMP_Text_t2599618874::get_offset_of_m_cSpacing_82(),
	TMP_Text_t2599618874::get_offset_of_m_monoSpacing_83(),
	TMP_Text_t2599618874::get_offset_of_m_wordSpacing_84(),
	TMP_Text_t2599618874::get_offset_of_m_lineSpacing_85(),
	TMP_Text_t2599618874::get_offset_of_m_lineSpacingDelta_86(),
	TMP_Text_t2599618874::get_offset_of_m_lineHeight_87(),
	TMP_Text_t2599618874::get_offset_of_m_lineSpacingMax_88(),
	TMP_Text_t2599618874::get_offset_of_m_paragraphSpacing_89(),
	TMP_Text_t2599618874::get_offset_of_m_charWidthMaxAdj_90(),
	TMP_Text_t2599618874::get_offset_of_m_charWidthAdjDelta_91(),
	TMP_Text_t2599618874::get_offset_of_m_enableWordWrapping_92(),
	TMP_Text_t2599618874::get_offset_of_m_isCharacterWrappingEnabled_93(),
	TMP_Text_t2599618874::get_offset_of_m_isNonBreakingSpace_94(),
	TMP_Text_t2599618874::get_offset_of_m_isIgnoringAlignment_95(),
	TMP_Text_t2599618874::get_offset_of_m_wordWrappingRatios_96(),
	TMP_Text_t2599618874::get_offset_of_m_overflowMode_97(),
	TMP_Text_t2599618874::get_offset_of_m_firstOverflowCharacterIndex_98(),
	TMP_Text_t2599618874::get_offset_of_m_linkedTextComponent_99(),
	TMP_Text_t2599618874::get_offset_of_m_isLinkedTextComponent_100(),
	TMP_Text_t2599618874::get_offset_of_m_isTextTruncated_101(),
	TMP_Text_t2599618874::get_offset_of_m_enableKerning_102(),
	TMP_Text_t2599618874::get_offset_of_m_enableExtraPadding_103(),
	TMP_Text_t2599618874::get_offset_of_checkPaddingRequired_104(),
	TMP_Text_t2599618874::get_offset_of_m_isRichText_105(),
	TMP_Text_t2599618874::get_offset_of_m_parseCtrlCharacters_106(),
	TMP_Text_t2599618874::get_offset_of_m_isOverlay_107(),
	TMP_Text_t2599618874::get_offset_of_m_isOrthographic_108(),
	TMP_Text_t2599618874::get_offset_of_m_isCullingEnabled_109(),
	TMP_Text_t2599618874::get_offset_of_m_ignoreRectMaskCulling_110(),
	TMP_Text_t2599618874::get_offset_of_m_ignoreCulling_111(),
	TMP_Text_t2599618874::get_offset_of_m_horizontalMapping_112(),
	TMP_Text_t2599618874::get_offset_of_m_verticalMapping_113(),
	TMP_Text_t2599618874::get_offset_of_m_uvLineOffset_114(),
	TMP_Text_t2599618874::get_offset_of_m_renderMode_115(),
	TMP_Text_t2599618874::get_offset_of_m_geometrySortingOrder_116(),
	TMP_Text_t2599618874::get_offset_of_m_firstVisibleCharacter_117(),
	TMP_Text_t2599618874::get_offset_of_m_maxVisibleCharacters_118(),
	TMP_Text_t2599618874::get_offset_of_m_maxVisibleWords_119(),
	TMP_Text_t2599618874::get_offset_of_m_maxVisibleLines_120(),
	TMP_Text_t2599618874::get_offset_of_m_useMaxVisibleDescender_121(),
	TMP_Text_t2599618874::get_offset_of_m_pageToDisplay_122(),
	TMP_Text_t2599618874::get_offset_of_m_isNewPage_123(),
	TMP_Text_t2599618874::get_offset_of_m_margin_124(),
	TMP_Text_t2599618874::get_offset_of_m_marginLeft_125(),
	TMP_Text_t2599618874::get_offset_of_m_marginRight_126(),
	TMP_Text_t2599618874::get_offset_of_m_marginWidth_127(),
	TMP_Text_t2599618874::get_offset_of_m_marginHeight_128(),
	TMP_Text_t2599618874::get_offset_of_m_width_129(),
	TMP_Text_t2599618874::get_offset_of_m_textInfo_130(),
	TMP_Text_t2599618874::get_offset_of_m_havePropertiesChanged_131(),
	TMP_Text_t2599618874::get_offset_of_m_isUsingLegacyAnimationComponent_132(),
	TMP_Text_t2599618874::get_offset_of_m_transform_133(),
	TMP_Text_t2599618874::get_offset_of_m_rectTransform_134(),
	TMP_Text_t2599618874::get_offset_of_U3CautoSizeTextContainerU3Ek__BackingField_135(),
	TMP_Text_t2599618874::get_offset_of_m_autoSizeTextContainer_136(),
	TMP_Text_t2599618874::get_offset_of_m_mesh_137(),
	TMP_Text_t2599618874::get_offset_of_m_isVolumetricText_138(),
	TMP_Text_t2599618874::get_offset_of_m_spriteAnimator_139(),
	TMP_Text_t2599618874::get_offset_of_m_flexibleHeight_140(),
	TMP_Text_t2599618874::get_offset_of_m_flexibleWidth_141(),
	TMP_Text_t2599618874::get_offset_of_m_minWidth_142(),
	TMP_Text_t2599618874::get_offset_of_m_minHeight_143(),
	TMP_Text_t2599618874::get_offset_of_m_maxWidth_144(),
	TMP_Text_t2599618874::get_offset_of_m_maxHeight_145(),
	TMP_Text_t2599618874::get_offset_of_m_LayoutElement_146(),
	TMP_Text_t2599618874::get_offset_of_m_preferredWidth_147(),
	TMP_Text_t2599618874::get_offset_of_m_renderedWidth_148(),
	TMP_Text_t2599618874::get_offset_of_m_isPreferredWidthDirty_149(),
	TMP_Text_t2599618874::get_offset_of_m_preferredHeight_150(),
	TMP_Text_t2599618874::get_offset_of_m_renderedHeight_151(),
	TMP_Text_t2599618874::get_offset_of_m_isPreferredHeightDirty_152(),
	TMP_Text_t2599618874::get_offset_of_m_isCalculatingPreferredValues_153(),
	TMP_Text_t2599618874::get_offset_of_m_recursiveCount_154(),
	TMP_Text_t2599618874::get_offset_of_m_layoutPriority_155(),
	TMP_Text_t2599618874::get_offset_of_m_isCalculateSizeRequired_156(),
	TMP_Text_t2599618874::get_offset_of_m_isLayoutDirty_157(),
	TMP_Text_t2599618874::get_offset_of_m_verticesAlreadyDirty_158(),
	TMP_Text_t2599618874::get_offset_of_m_layoutAlreadyDirty_159(),
	TMP_Text_t2599618874::get_offset_of_m_isAwake_160(),
	TMP_Text_t2599618874::get_offset_of_m_isInputParsingRequired_161(),
	TMP_Text_t2599618874::get_offset_of_m_inputSource_162(),
	TMP_Text_t2599618874::get_offset_of_old_text_163(),
	TMP_Text_t2599618874::get_offset_of_m_fontScale_164(),
	TMP_Text_t2599618874::get_offset_of_m_fontScaleMultiplier_165(),
	TMP_Text_t2599618874::get_offset_of_m_htmlTag_166(),
	TMP_Text_t2599618874::get_offset_of_m_xmlAttribute_167(),
	TMP_Text_t2599618874::get_offset_of_m_attributeParameterValues_168(),
	TMP_Text_t2599618874::get_offset_of_tag_LineIndent_169(),
	TMP_Text_t2599618874::get_offset_of_tag_Indent_170(),
	TMP_Text_t2599618874::get_offset_of_m_indentStack_171(),
	TMP_Text_t2599618874::get_offset_of_tag_NoParsing_172(),
	TMP_Text_t2599618874::get_offset_of_m_isParsingText_173(),
	TMP_Text_t2599618874::get_offset_of_m_FXMatrix_174(),
	TMP_Text_t2599618874::get_offset_of_m_isFXMatrixSet_175(),
	TMP_Text_t2599618874::get_offset_of_m_char_buffer_176(),
	TMP_Text_t2599618874::get_offset_of_m_internalCharacterInfo_177(),
	TMP_Text_t2599618874::get_offset_of_m_input_CharArray_178(),
	TMP_Text_t2599618874::get_offset_of_m_charArray_Length_179(),
	TMP_Text_t2599618874::get_offset_of_m_totalCharacterCount_180(),
	TMP_Text_t2599618874::get_offset_of_m_SavedWordWrapState_181(),
	TMP_Text_t2599618874::get_offset_of_m_SavedLineState_182(),
	TMP_Text_t2599618874::get_offset_of_m_characterCount_183(),
	TMP_Text_t2599618874::get_offset_of_m_firstCharacterOfLine_184(),
	TMP_Text_t2599618874::get_offset_of_m_firstVisibleCharacterOfLine_185(),
	TMP_Text_t2599618874::get_offset_of_m_lastCharacterOfLine_186(),
	TMP_Text_t2599618874::get_offset_of_m_lastVisibleCharacterOfLine_187(),
	TMP_Text_t2599618874::get_offset_of_m_lineNumber_188(),
	TMP_Text_t2599618874::get_offset_of_m_lineVisibleCharacterCount_189(),
	TMP_Text_t2599618874::get_offset_of_m_pageNumber_190(),
	TMP_Text_t2599618874::get_offset_of_m_maxAscender_191(),
	TMP_Text_t2599618874::get_offset_of_m_maxCapHeight_192(),
	TMP_Text_t2599618874::get_offset_of_m_maxDescender_193(),
	TMP_Text_t2599618874::get_offset_of_m_maxLineAscender_194(),
	TMP_Text_t2599618874::get_offset_of_m_maxLineDescender_195(),
	TMP_Text_t2599618874::get_offset_of_m_startOfLineAscender_196(),
	TMP_Text_t2599618874::get_offset_of_m_lineOffset_197(),
	TMP_Text_t2599618874::get_offset_of_m_meshExtents_198(),
	TMP_Text_t2599618874::get_offset_of_m_htmlColor_199(),
	TMP_Text_t2599618874::get_offset_of_m_colorStack_200(),
	TMP_Text_t2599618874::get_offset_of_m_underlineColorStack_201(),
	TMP_Text_t2599618874::get_offset_of_m_strikethroughColorStack_202(),
	TMP_Text_t2599618874::get_offset_of_m_highlightColorStack_203(),
	TMP_Text_t2599618874::get_offset_of_m_tabSpacing_204(),
	TMP_Text_t2599618874::get_offset_of_m_spacing_205(),
	TMP_Text_t2599618874::get_offset_of_m_styleStack_206(),
	TMP_Text_t2599618874::get_offset_of_m_actionStack_207(),
	TMP_Text_t2599618874::get_offset_of_m_padding_208(),
	TMP_Text_t2599618874::get_offset_of_m_baselineOffset_209(),
	TMP_Text_t2599618874::get_offset_of_m_baselineOffsetStack_210(),
	TMP_Text_t2599618874::get_offset_of_m_xAdvance_211(),
	TMP_Text_t2599618874::get_offset_of_m_textElementType_212(),
	TMP_Text_t2599618874::get_offset_of_m_cached_TextElement_213(),
	TMP_Text_t2599618874::get_offset_of_m_cached_Underline_GlyphInfo_214(),
	TMP_Text_t2599618874::get_offset_of_m_cached_Ellipsis_GlyphInfo_215(),
	TMP_Text_t2599618874::get_offset_of_m_defaultSpriteAsset_216(),
	TMP_Text_t2599618874::get_offset_of_m_currentSpriteAsset_217(),
	TMP_Text_t2599618874::get_offset_of_m_spriteCount_218(),
	TMP_Text_t2599618874::get_offset_of_m_spriteIndex_219(),
	TMP_Text_t2599618874::get_offset_of_m_inlineGraphics_220(),
	TMP_Text_t2599618874::get_offset_of_m_spriteAnimationID_221(),
	TMP_Text_t2599618874::get_offset_of_m_ignoreActiveState_222(),
	TMP_Text_t2599618874::get_offset_of_k_Power_223(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargePositiveVector2_224(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargeNegativeVector2_225(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargePositiveFloat_226(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargeNegativeFloat_227(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargePositiveInt_228(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargeNegativeInt_229(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (TextInputSources_t1522115805)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2104[5] = 
{
	TextInputSources_t1522115805::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (TMP_TextElement_t129727469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2105[9] = 
{
	TMP_TextElement_t129727469::get_offset_of_id_0(),
	TMP_TextElement_t129727469::get_offset_of_x_1(),
	TMP_TextElement_t129727469::get_offset_of_y_2(),
	TMP_TextElement_t129727469::get_offset_of_width_3(),
	TMP_TextElement_t129727469::get_offset_of_height_4(),
	TMP_TextElement_t129727469::get_offset_of_xOffset_5(),
	TMP_TextElement_t129727469::get_offset_of_yOffset_6(),
	TMP_TextElement_t129727469::get_offset_of_xAdvance_7(),
	TMP_TextElement_t129727469::get_offset_of_scale_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (TMP_TextInfo_t3598145122), -1, sizeof(TMP_TextInfo_t3598145122_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2106[18] = 
{
	TMP_TextInfo_t3598145122_StaticFields::get_offset_of_k_InfinityVectorPositive_0(),
	TMP_TextInfo_t3598145122_StaticFields::get_offset_of_k_InfinityVectorNegative_1(),
	TMP_TextInfo_t3598145122::get_offset_of_textComponent_2(),
	TMP_TextInfo_t3598145122::get_offset_of_characterCount_3(),
	TMP_TextInfo_t3598145122::get_offset_of_spriteCount_4(),
	TMP_TextInfo_t3598145122::get_offset_of_spaceCount_5(),
	TMP_TextInfo_t3598145122::get_offset_of_wordCount_6(),
	TMP_TextInfo_t3598145122::get_offset_of_linkCount_7(),
	TMP_TextInfo_t3598145122::get_offset_of_lineCount_8(),
	TMP_TextInfo_t3598145122::get_offset_of_pageCount_9(),
	TMP_TextInfo_t3598145122::get_offset_of_materialCount_10(),
	TMP_TextInfo_t3598145122::get_offset_of_characterInfo_11(),
	TMP_TextInfo_t3598145122::get_offset_of_wordInfo_12(),
	TMP_TextInfo_t3598145122::get_offset_of_linkInfo_13(),
	TMP_TextInfo_t3598145122::get_offset_of_lineInfo_14(),
	TMP_TextInfo_t3598145122::get_offset_of_pageInfo_15(),
	TMP_TextInfo_t3598145122::get_offset_of_meshInfo_16(),
	TMP_TextInfo_t3598145122::get_offset_of_m_CachedMeshInfo_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (CaretPosition_t3997512201)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2107[4] = 
{
	CaretPosition_t3997512201::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (CaretInfo_t841780893)+ sizeof (RuntimeObject), sizeof(CaretInfo_t841780893 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2108[2] = 
{
	CaretInfo_t841780893::get_offset_of_index_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CaretInfo_t841780893::get_offset_of_position_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (TMP_TextUtilities_t2105690005), -1, sizeof(TMP_TextUtilities_t2105690005_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2109[3] = 
{
	TMP_TextUtilities_t2105690005_StaticFields::get_offset_of_m_rectWorldCorners_0(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (LineSegment_t1526544958)+ sizeof (RuntimeObject), sizeof(LineSegment_t1526544958 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2110[2] = 
{
	LineSegment_t1526544958::get_offset_of_Point1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LineSegment_t1526544958::get_offset_of_Point2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (TMP_UpdateManager_t4114267509), -1, sizeof(TMP_UpdateManager_t4114267509_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2111[5] = 
{
	TMP_UpdateManager_t4114267509_StaticFields::get_offset_of_s_Instance_0(),
	TMP_UpdateManager_t4114267509::get_offset_of_m_LayoutRebuildQueue_1(),
	TMP_UpdateManager_t4114267509::get_offset_of_m_LayoutQueueLookup_2(),
	TMP_UpdateManager_t4114267509::get_offset_of_m_GraphicRebuildQueue_3(),
	TMP_UpdateManager_t4114267509::get_offset_of_m_GraphicQueueLookup_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (TMP_UpdateRegistry_t461608481), -1, sizeof(TMP_UpdateRegistry_t461608481_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2112[5] = 
{
	TMP_UpdateRegistry_t461608481_StaticFields::get_offset_of_s_Instance_0(),
	TMP_UpdateRegistry_t461608481::get_offset_of_m_LayoutRebuildQueue_1(),
	TMP_UpdateRegistry_t461608481::get_offset_of_m_LayoutQueueLookup_2(),
	TMP_UpdateRegistry_t461608481::get_offset_of_m_GraphicRebuildQueue_3(),
	TMP_UpdateRegistry_t461608481::get_offset_of_m_GraphicQueueLookup_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (TMP_BasicXmlTagStack_t2962628096)+ sizeof (RuntimeObject), sizeof(TMP_BasicXmlTagStack_t2962628096 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2113[10] = 
{
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_bold_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_italic_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_underline_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_strikethrough_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_highlight_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_superscript_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_subscript_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_uppercase_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_lowercase_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_smallcaps_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2114[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (Compute_DistanceTransform_EventTypes_t2554612104)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2115[3] = 
{
	Compute_DistanceTransform_EventTypes_t2554612104::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (TMPro_EventManager_t712497257), -1, sizeof(TMPro_EventManager_t712497257_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2116[12] = 
{
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_COMPUTE_DT_EVENT_0(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_MATERIAL_PROPERTY_EVENT_1(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_FONT_PROPERTY_EVENT_2(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_SPRITE_ASSET_PROPERTY_EVENT_3(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_TEXTMESHPRO_PROPERTY_EVENT_4(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_DRAG_AND_DROP_MATERIAL_EVENT_5(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_TEXT_STYLE_PROPERTY_EVENT_6(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_COLOR_GRADIENT_PROPERTY_EVENT_7(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_TMP_SETTINGS_PROPERTY_EVENT_8(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_TEXTMESHPRO_UGUI_PROPERTY_EVENT_9(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_OnPreRenderObject_Event_10(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_TEXT_CHANGED_EVENT_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (Compute_DT_EventArgs_t1071353166), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2117[3] = 
{
	Compute_DT_EventArgs_t1071353166::get_offset_of_EventType_0(),
	Compute_DT_EventArgs_t1071353166::get_offset_of_ProgressPercentage_1(),
	Compute_DT_EventArgs_t1071353166::get_offset_of_Colors_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (TMPro_ExtensionMethods_t1453208966), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (TMP_Math_t624304809), -1, sizeof(TMP_Math_t624304809_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2119[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	TMP_Math_t624304809_StaticFields::get_offset_of_MAX_16BIT_6(),
	TMP_Math_t624304809_StaticFields::get_offset_of_MIN_16BIT_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (FaceInfo_t2243299176), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2120[21] = 
{
	FaceInfo_t2243299176::get_offset_of_Name_0(),
	FaceInfo_t2243299176::get_offset_of_PointSize_1(),
	FaceInfo_t2243299176::get_offset_of_Scale_2(),
	FaceInfo_t2243299176::get_offset_of_CharacterCount_3(),
	FaceInfo_t2243299176::get_offset_of_LineHeight_4(),
	FaceInfo_t2243299176::get_offset_of_Baseline_5(),
	FaceInfo_t2243299176::get_offset_of_Ascender_6(),
	FaceInfo_t2243299176::get_offset_of_CapHeight_7(),
	FaceInfo_t2243299176::get_offset_of_Descender_8(),
	FaceInfo_t2243299176::get_offset_of_CenterLine_9(),
	FaceInfo_t2243299176::get_offset_of_SuperscriptOffset_10(),
	FaceInfo_t2243299176::get_offset_of_SubscriptOffset_11(),
	FaceInfo_t2243299176::get_offset_of_SubSize_12(),
	FaceInfo_t2243299176::get_offset_of_Underline_13(),
	FaceInfo_t2243299176::get_offset_of_UnderlineThickness_14(),
	FaceInfo_t2243299176::get_offset_of_strikethrough_15(),
	FaceInfo_t2243299176::get_offset_of_strikethroughThickness_16(),
	FaceInfo_t2243299176::get_offset_of_TabWidth_17(),
	FaceInfo_t2243299176::get_offset_of_Padding_18(),
	FaceInfo_t2243299176::get_offset_of_AtlasWidth_19(),
	FaceInfo_t2243299176::get_offset_of_AtlasHeight_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (TMP_Glyph_t581847833), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (FontCreationSetting_t628772060)+ sizeof (RuntimeObject), sizeof(FontCreationSetting_t628772060_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2122[12] = 
{
	FontCreationSetting_t628772060::get_offset_of_fontSourcePath_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t628772060::get_offset_of_fontSizingMode_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t628772060::get_offset_of_fontSize_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t628772060::get_offset_of_fontPadding_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t628772060::get_offset_of_fontPackingMode_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t628772060::get_offset_of_fontAtlasWidth_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t628772060::get_offset_of_fontAtlasHeight_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t628772060::get_offset_of_fontCharacterSet_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t628772060::get_offset_of_fontStyle_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t628772060::get_offset_of_fontStlyeModifier_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t628772060::get_offset_of_fontRenderMode_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t628772060::get_offset_of_fontKerning_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (Glyph2D_t1260586688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2123[8] = 
{
	Glyph2D_t1260586688::get_offset_of_bottomLeft_0(),
	Glyph2D_t1260586688::get_offset_of_topLeft_1(),
	Glyph2D_t1260586688::get_offset_of_bottomRight_2(),
	Glyph2D_t1260586688::get_offset_of_topRight_3(),
	Glyph2D_t1260586688::get_offset_of_uv0_4(),
	Glyph2D_t1260586688::get_offset_of_uv1_5(),
	Glyph2D_t1260586688::get_offset_of_uv2_6(),
	Glyph2D_t1260586688::get_offset_of_uv3_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (KerningPairKey_t536493877)+ sizeof (RuntimeObject), sizeof(KerningPairKey_t536493877 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2124[3] = 
{
	KerningPairKey_t536493877::get_offset_of_ascii_Left_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	KerningPairKey_t536493877::get_offset_of_ascii_Right_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	KerningPairKey_t536493877::get_offset_of_key_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (KerningPair_t2270855589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2125[3] = 
{
	KerningPair_t2270855589::get_offset_of_AscII_Left_0(),
	KerningPair_t2270855589::get_offset_of_AscII_Right_1(),
	KerningPair_t2270855589::get_offset_of_XadvanceOffset_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (KerningTable_t2322366871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2126[1] = 
{
	KerningTable_t2322366871::get_offset_of_kerningPairs_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (U3CU3Ec__DisplayClass3_0_t2918531336), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2127[2] = 
{
	U3CU3Ec__DisplayClass3_0_t2918531336::get_offset_of_left_0(),
	U3CU3Ec__DisplayClass3_0_t2918531336::get_offset_of_right_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (U3CU3Ec__DisplayClass4_0_t2918531329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2128[2] = 
{
	U3CU3Ec__DisplayClass4_0_t2918531329::get_offset_of_left_0(),
	U3CU3Ec__DisplayClass4_0_t2918531329::get_offset_of_right_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (U3CU3Ec_t4077839018), -1, sizeof(U3CU3Ec_t4077839018_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2129[3] = 
{
	U3CU3Ec_t4077839018_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t4077839018_StaticFields::get_offset_of_U3CU3E9__6_0_1(),
	U3CU3Ec_t4077839018_StaticFields::get_offset_of_U3CU3E9__6_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (TMP_FontUtilities_t2599150238), -1, sizeof(TMP_FontUtilities_t2599150238_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2130[1] = 
{
	TMP_FontUtilities_t2599150238_StaticFields::get_offset_of_k_searchedFontAssets_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (TMP_VertexDataUpdateFlags_t388000256)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2131[8] = 
{
	TMP_VertexDataUpdateFlags_t388000256::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (TMP_CharacterInfo_t3185626797)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2132[35] = 
{
	TMP_CharacterInfo_t3185626797::get_offset_of_character_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_index_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_elementType_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_textElement_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_fontAsset_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_spriteAsset_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_spriteIndex_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_material_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_materialReferenceIndex_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_isUsingAlternateTypeface_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_pointSize_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_lineNumber_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_pageNumber_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertexIndex_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertex_TL_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertex_BL_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertex_TR_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertex_BR_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_topLeft_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_bottomLeft_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_topRight_20() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_bottomRight_21() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_origin_22() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_ascender_23() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_baseLine_24() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_descender_25() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_xAdvance_26() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_aspectRatio_27() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_scale_28() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_color_29() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_underlineColor_30() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_strikethroughColor_31() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_highlightColor_32() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_style_33() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_isVisible_34() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (TMP_Vertex_t2404176824)+ sizeof (RuntimeObject), sizeof(TMP_Vertex_t2404176824 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2133[5] = 
{
	TMP_Vertex_t2404176824::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t2404176824::get_offset_of_uv_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t2404176824::get_offset_of_uv2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t2404176824::get_offset_of_uv4_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t2404176824::get_offset_of_color_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (VertexGradient_t345148380)+ sizeof (RuntimeObject), sizeof(VertexGradient_t345148380 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2134[4] = 
{
	VertexGradient_t345148380::get_offset_of_topLeft_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_t345148380::get_offset_of_topRight_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_t345148380::get_offset_of_bottomLeft_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_t345148380::get_offset_of_bottomRight_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (TMP_PageInfo_t2608430633)+ sizeof (RuntimeObject), sizeof(TMP_PageInfo_t2608430633 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2135[5] = 
{
	TMP_PageInfo_t2608430633::get_offset_of_firstCharacterIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t2608430633::get_offset_of_lastCharacterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t2608430633::get_offset_of_ascender_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t2608430633::get_offset_of_baseLine_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t2608430633::get_offset_of_descender_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (TMP_LinkInfo_t1092083476)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2136[7] = 
{
	TMP_LinkInfo_t1092083476::get_offset_of_textComponent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_hashCode_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkIdFirstCharacterIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkIdLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkTextfirstCharacterIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkTextLength_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkID_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (TMP_WordInfo_t3331066303)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2137[4] = 
{
	TMP_WordInfo_t3331066303::get_offset_of_textComponent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t3331066303::get_offset_of_firstCharacterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t3331066303::get_offset_of_lastCharacterIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t3331066303::get_offset_of_characterCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (TMP_SpriteInfo_t2726321384)+ sizeof (RuntimeObject), sizeof(TMP_SpriteInfo_t2726321384 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2138[3] = 
{
	TMP_SpriteInfo_t2726321384::get_offset_of_spriteIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_SpriteInfo_t2726321384::get_offset_of_characterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_SpriteInfo_t2726321384::get_offset_of_vertexIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (Extents_t3837212874)+ sizeof (RuntimeObject), sizeof(Extents_t3837212874 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2139[2] = 
{
	Extents_t3837212874::get_offset_of_min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Extents_t3837212874::get_offset_of_max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (Mesh_Extents_t3388355125)+ sizeof (RuntimeObject), sizeof(Mesh_Extents_t3388355125 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2140[2] = 
{
	Mesh_Extents_t3388355125::get_offset_of_min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Mesh_Extents_t3388355125::get_offset_of_max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (WordWrapState_t341939652)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2141[53] = 
{
	WordWrapState_t341939652::get_offset_of_previous_WordBreak_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_total_CharacterCount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_visible_CharacterCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_visible_SpriteCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_visible_LinkCount_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_firstCharacterIndex_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_firstVisibleCharacterIndex_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lastCharacterIndex_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lastVisibleCharIndex_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lineNumber_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_maxCapHeight_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_maxAscender_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_maxDescender_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_maxLineAscender_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_maxLineDescender_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_previousLineAscender_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_xAdvance_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_preferredWidth_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_preferredHeight_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_previousLineScale_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_wordCount_20() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_fontStyle_21() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_fontScale_22() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_fontScaleMultiplier_23() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_currentFontSize_24() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_baselineOffset_25() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lineOffset_26() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_textInfo_27() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lineInfo_28() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_vertexColor_29() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_underlineColor_30() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_strikethroughColor_31() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_highlightColor_32() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_basicStyleStack_33() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_colorStack_34() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_underlineColorStack_35() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_strikethroughColorStack_36() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_highlightColorStack_37() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_sizeStack_38() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_indentStack_39() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_fontWeightStack_40() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_styleStack_41() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_baselineStack_42() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_actionStack_43() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_materialReferenceStack_44() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lineJustificationStack_45() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_spriteAnimationID_46() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_currentFontAsset_47() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_currentSpriteAsset_48() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_currentMaterial_49() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_currentMaterialIndex_50() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_meshExtents_51() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_tagNoParsing_52() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (TagAttribute_t688278634)+ sizeof (RuntimeObject), sizeof(TagAttribute_t688278634 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2142[3] = 
{
	TagAttribute_t688278634::get_offset_of_startIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagAttribute_t688278634::get_offset_of_length_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagAttribute_t688278634::get_offset_of_hashCode_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (XML_TagAttribute_t1174424309)+ sizeof (RuntimeObject), sizeof(XML_TagAttribute_t1174424309 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2143[5] = 
{
	XML_TagAttribute_t1174424309::get_offset_of_nameHashCode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_t1174424309::get_offset_of_valueType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_t1174424309::get_offset_of_valueStartIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_t1174424309::get_offset_of_valueLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_t1174424309::get_offset_of_valueHashCode_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (TextMeshPro_t2393593166), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2144[17] = 
{
	TextMeshPro_t2393593166::get_offset_of_m_hasFontAssetChanged_230(),
	TextMeshPro_t2393593166::get_offset_of_m_previousLossyScaleY_231(),
	TextMeshPro_t2393593166::get_offset_of_m_renderer_232(),
	TextMeshPro_t2393593166::get_offset_of_m_meshFilter_233(),
	TextMeshPro_t2393593166::get_offset_of_m_isFirstAllocation_234(),
	TextMeshPro_t2393593166::get_offset_of_m_max_characters_235(),
	TextMeshPro_t2393593166::get_offset_of_m_max_numberOfLines_236(),
	TextMeshPro_t2393593166::get_offset_of_m_default_bounds_237(),
	TextMeshPro_t2393593166::get_offset_of_m_subTextObjects_238(),
	TextMeshPro_t2393593166::get_offset_of_m_isMaskingEnabled_239(),
	TextMeshPro_t2393593166::get_offset_of_isMaskUpdateRequired_240(),
	TextMeshPro_t2393593166::get_offset_of_m_maskType_241(),
	TextMeshPro_t2393593166::get_offset_of_m_EnvMapMatrix_242(),
	TextMeshPro_t2393593166::get_offset_of_m_RectTransformCorners_243(),
	TextMeshPro_t2393593166::get_offset_of_m_isRegisteredForEvents_244(),
	TextMeshPro_t2393593166::get_offset_of_loopCountA_245(),
	TextMeshPro_t2393593166::get_offset_of_m_currentAutoSizeMode_246(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (ShaderUtilities_t714255158), -1, sizeof(ShaderUtilities_t714255158_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2145[59] = 
{
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_MainTex_0(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_FaceTex_1(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_FaceColor_2(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_FaceDilate_3(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_Shininess_4(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlayColor_5(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlayOffsetX_6(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlayOffsetY_7(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlayDilate_8(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlaySoftness_9(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_WeightNormal_10(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_WeightBold_11(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_OutlineTex_12(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_OutlineWidth_13(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_OutlineSoftness_14(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_OutlineColor_15(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GradientScale_16(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleX_17(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleY_18(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_PerspectiveFilter_19(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_TextureWidth_20(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_TextureHeight_21(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_BevelAmount_22(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GlowColor_23(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GlowOffset_24(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GlowPower_25(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GlowOuter_26(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_LightAngle_27(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_EnvMap_28(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_EnvMatrix_29(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_EnvMatrixRotation_30(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_MaskCoord_31(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ClipRect_32(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_MaskSoftnessX_33(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_MaskSoftnessY_34(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_VertexOffsetX_35(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_VertexOffsetY_36(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UseClipRect_37(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilID_38(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilOp_39(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilComp_40(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilReadMask_41(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilWriteMask_42(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ShaderFlags_43(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleRatio_A_44(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleRatio_B_45(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleRatio_C_46(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Bevel_47(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Glow_48(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Underlay_49(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Ratios_50(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_MASK_SOFT_51(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_MASK_HARD_52(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_MASK_TEX_53(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Outline_54(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ShaderTag_ZTestMode_55(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ShaderTag_CullMode_56(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_m_clamp_57(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_isInitialized_58(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (TextMeshProUGUI_t529313277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2146[18] = 
{
	TextMeshProUGUI_t529313277::get_offset_of_m_hasFontAssetChanged_230(),
	TextMeshProUGUI_t529313277::get_offset_of_m_subTextObjects_231(),
	TextMeshProUGUI_t529313277::get_offset_of_m_previousLossyScaleY_232(),
	TextMeshProUGUI_t529313277::get_offset_of_m_RectTransformCorners_233(),
	TextMeshProUGUI_t529313277::get_offset_of_m_canvasRenderer_234(),
	TextMeshProUGUI_t529313277::get_offset_of_m_canvas_235(),
	TextMeshProUGUI_t529313277::get_offset_of_m_isFirstAllocation_236(),
	TextMeshProUGUI_t529313277::get_offset_of_m_max_characters_237(),
	TextMeshProUGUI_t529313277::get_offset_of_m_isMaskingEnabled_238(),
	TextMeshProUGUI_t529313277::get_offset_of_m_baseMaterial_239(),
	TextMeshProUGUI_t529313277::get_offset_of_m_isScrollRegionSet_240(),
	TextMeshProUGUI_t529313277::get_offset_of_m_stencilID_241(),
	TextMeshProUGUI_t529313277::get_offset_of_m_maskOffset_242(),
	TextMeshProUGUI_t529313277::get_offset_of_m_EnvMapMatrix_243(),
	TextMeshProUGUI_t529313277::get_offset_of_m_isRegisteredForEvents_244(),
	TextMeshProUGUI_t529313277::get_offset_of_m_recursiveCountA_245(),
	TextMeshProUGUI_t529313277::get_offset_of_loopCountA_246(),
	TextMeshProUGUI_t529313277::get_offset_of_m_isRebuildingLayout_247(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (TextContainerAnchors_t945851193)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2147[11] = 
{
	TextContainerAnchors_t945851193::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (TextContainer_t97923372), -1, sizeof(TextContainer_t97923372_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2148[13] = 
{
	TextContainer_t97923372::get_offset_of_m_hasChanged_2(),
	TextContainer_t97923372::get_offset_of_m_pivot_3(),
	TextContainer_t97923372::get_offset_of_m_anchorPosition_4(),
	TextContainer_t97923372::get_offset_of_m_rect_5(),
	TextContainer_t97923372::get_offset_of_m_isDefaultWidth_6(),
	TextContainer_t97923372::get_offset_of_m_isDefaultHeight_7(),
	TextContainer_t97923372::get_offset_of_m_isAutoFitting_8(),
	TextContainer_t97923372::get_offset_of_m_corners_9(),
	TextContainer_t97923372::get_offset_of_m_worldCorners_10(),
	TextContainer_t97923372::get_offset_of_m_margins_11(),
	TextContainer_t97923372::get_offset_of_m_rectTransform_12(),
	TextContainer_t97923372_StaticFields::get_offset_of_k_defaultSize_13(),
	TextContainer_t97923372::get_offset_of_m_textMeshPro_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (SpriteAssetImportFormats_t116390639)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2149[3] = 
{
	SpriteAssetImportFormats_t116390639::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (TexturePacker_t3148178657), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (SpriteFrame_t3912389194)+ sizeof (RuntimeObject), sizeof(SpriteFrame_t3912389194 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2151[4] = 
{
	SpriteFrame_t3912389194::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_t3912389194::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_t3912389194::get_offset_of_w_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_t3912389194::get_offset_of_h_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (SpriteSize_t3355290999)+ sizeof (RuntimeObject), sizeof(SpriteSize_t3355290999 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2152[2] = 
{
	SpriteSize_t3355290999::get_offset_of_w_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteSize_t3355290999::get_offset_of_h_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (SpriteData_t3048397587)+ sizeof (RuntimeObject), sizeof(SpriteData_t3048397587_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2153[7] = 
{
	SpriteData_t3048397587::get_offset_of_filename_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t3048397587::get_offset_of_frame_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t3048397587::get_offset_of_rotated_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t3048397587::get_offset_of_trimmed_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t3048397587::get_offset_of_spriteSourceSize_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t3048397587::get_offset_of_sourceSize_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t3048397587::get_offset_of_pivot_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (SpriteDataObject_t308163541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2154[1] = 
{
	SpriteDataObject_t308163541::get_offset_of_frames_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255366), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2155[2] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (__StaticArrayInitTypeSizeU3D12_t2710994317)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_t2710994317 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (__StaticArrayInitTypeSizeU3D40_t1547998295)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D40_t1547998295 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (U3CModuleU3E_t692745548), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (AnalyticsEvent_t4058973021), -1, sizeof(AnalyticsEvent_t4058973021_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2159[4] = 
{
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of_k_SdkVersion_0(),
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of_m_EventData_1(),
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of__debugMode_2(),
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of_enumRenameTable_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (U3CModuleU3E_t692745549), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (AnalyticsEventTracker_t2285229262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2161[2] = 
{
	AnalyticsEventTracker_t2285229262::get_offset_of_m_Trigger_2(),
	AnalyticsEventTracker_t2285229262::get_offset_of_m_EventPayload_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (U3CTimedTriggerU3Ec__Iterator0_t3813435494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2162[4] = 
{
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24this_0(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24current_1(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24disposing_2(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (AnalyticsEventTrackerSettings_t480422680), -1, sizeof(AnalyticsEventTrackerSettings_t480422680_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2163[2] = 
{
	AnalyticsEventTrackerSettings_t480422680_StaticFields::get_offset_of_paramCountMax_0(),
	AnalyticsEventTrackerSettings_t480422680_StaticFields::get_offset_of_triggerRuleCountMax_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (AnalyticsEventParam_t2480121928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2164[5] = 
{
	AnalyticsEventParam_t2480121928::get_offset_of_m_RequirementType_0(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_GroupID_1(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Tooltip_2(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Name_3(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (RequirementType_t3584265503)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2165[4] = 
{
	RequirementType_t3584265503::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (AnalyticsEventParamListContainer_t587083383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2166[1] = 
{
	AnalyticsEventParamListContainer_t587083383::get_offset_of_m_Parameters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (StandardEventPayload_t1629891255), -1, sizeof(StandardEventPayload_t1629891255_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2167[6] = 
{
	StandardEventPayload_t1629891255::get_offset_of_m_IsEventExpanded_0(),
	StandardEventPayload_t1629891255::get_offset_of_m_StandardEventType_1(),
	StandardEventPayload_t1629891255::get_offset_of_standardEventType_2(),
	StandardEventPayload_t1629891255::get_offset_of_m_Parameters_3(),
	StandardEventPayload_t1629891255_StaticFields::get_offset_of_m_EventData_4(),
	StandardEventPayload_t1629891255::get_offset_of_m_Name_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (TrackableField_t1772682203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2168[3] = 
{
	TrackableField_t1772682203::get_offset_of_m_ValidTypeNames_2(),
	TrackableField_t1772682203::get_offset_of_m_Type_3(),
	TrackableField_t1772682203::get_offset_of_m_EnumType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (TrackablePropertyBase_t2121532948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2169[2] = 
{
	TrackablePropertyBase_t2121532948::get_offset_of_m_Target_0(),
	TrackablePropertyBase_t2121532948::get_offset_of_m_Path_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (ValueProperty_t1868393739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2170[11] = 
{
	ValueProperty_t1868393739::get_offset_of_m_EditingCustomValue_0(),
	ValueProperty_t1868393739::get_offset_of_m_PopupIndex_1(),
	ValueProperty_t1868393739::get_offset_of_m_CustomValue_2(),
	ValueProperty_t1868393739::get_offset_of_m_FixedType_3(),
	ValueProperty_t1868393739::get_offset_of_m_EnumType_4(),
	ValueProperty_t1868393739::get_offset_of_m_EnumTypeIsCustomizable_5(),
	ValueProperty_t1868393739::get_offset_of_m_CanDisable_6(),
	ValueProperty_t1868393739::get_offset_of_m_PropertyType_7(),
	ValueProperty_t1868393739::get_offset_of_m_ValueType_8(),
	ValueProperty_t1868393739::get_offset_of_m_Value_9(),
	ValueProperty_t1868393739::get_offset_of_m_Target_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (PropertyType_t4040930247)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2171[4] = 
{
	PropertyType_t4040930247::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (AnalyticsTracker_t731021378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2172[5] = 
{
	AnalyticsTracker_t731021378::get_offset_of_m_EventName_2(),
	AnalyticsTracker_t731021378::get_offset_of_m_Dict_3(),
	AnalyticsTracker_t731021378::get_offset_of_m_PrevDictHash_4(),
	AnalyticsTracker_t731021378::get_offset_of_m_TrackableProperty_5(),
	AnalyticsTracker_t731021378::get_offset_of_m_Trigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (Trigger_t4199345191)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2173[8] = 
{
	Trigger_t4199345191::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (TrackableProperty_t3943537984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2174[2] = 
{
	0,
	TrackableProperty_t3943537984::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (FieldWithTarget_t3058750293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2175[6] = 
{
	FieldWithTarget_t3058750293::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t3058750293::get_offset_of_m_Target_1(),
	FieldWithTarget_t3058750293::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t3058750293::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t3058750293::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t3058750293::get_offset_of_m_StaticString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (TriggerBool_t501031542)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2176[4] = 
{
	TriggerBool_t501031542::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (TriggerLifecycleEvent_t3193146760)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2177[9] = 
{
	TriggerLifecycleEvent_t3193146760::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (TriggerOperator_t3611898925)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2178[9] = 
{
	TriggerOperator_t3611898925::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (TriggerType_t105272677)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2179[5] = 
{
	TriggerType_t105272677::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (TriggerListContainer_t2032715483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2180[1] = 
{
	TriggerListContainer_t2032715483::get_offset_of_m_Rules_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (EventTrigger_t2527451695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2181[12] = 
{
	EventTrigger_t2527451695::get_offset_of_m_IsTriggerExpanded_0(),
	EventTrigger_t2527451695::get_offset_of_m_Type_1(),
	EventTrigger_t2527451695::get_offset_of_m_LifecycleEvent_2(),
	EventTrigger_t2527451695::get_offset_of_m_ApplyRules_3(),
	EventTrigger_t2527451695::get_offset_of_m_Rules_4(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerBool_5(),
	EventTrigger_t2527451695::get_offset_of_m_InitTime_6(),
	EventTrigger_t2527451695::get_offset_of_m_RepeatTime_7(),
	EventTrigger_t2527451695::get_offset_of_m_Repetitions_8(),
	EventTrigger_t2527451695::get_offset_of_repetitionCount_9(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerFunction_10(),
	EventTrigger_t2527451695::get_offset_of_m_Method_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (OnTrigger_t4184125570), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (TrackableTrigger_t621205209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2183[2] = 
{
	TrackableTrigger_t621205209::get_offset_of_m_Target_0(),
	TrackableTrigger_t621205209::get_offset_of_m_MethodPath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (TriggerMethod_t582536534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (TriggerRule_t1946298321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2185[4] = 
{
	TriggerRule_t1946298321::get_offset_of_m_Target_0(),
	TriggerRule_t1946298321::get_offset_of_m_Operator_1(),
	TriggerRule_t1946298321::get_offset_of_m_Value_2(),
	TriggerRule_t1946298321::get_offset_of_m_Value2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (U3CModuleU3E_t692745550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (Presentation_t669212981), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2187[5] = 
{
	Presentation_t669212981::get_offset_of_left_2(),
	Presentation_t669212981::get_offset_of_rotated_3(),
	Presentation_t669212981::get_offset_of_move_4(),
	Presentation_t669212981::get_offset_of_speed_5(),
	Presentation_t669212981::get_offset_of_maxX_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (Clip_t774875397), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2188[1] = 
{
	Clip_t774875397::get_offset_of_animationClip_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (ImageColor_t691475714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2189[3] = 
{
	ImageColor_t691475714::get_offset_of_from_9(),
	ImageColor_t691475714::get_offset_of_to_10(),
	ImageColor_t691475714::get_offset_of_image_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (Position2D_t1995187901), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2190[3] = 
{
	Position2D_t1995187901::get_offset_of_v0_9(),
	Position2D_t1995187901::get_offset_of_v1_10(),
	Position2D_t1995187901::get_offset_of_useGlobalValues_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (Position3D_t1995122365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2191[4] = 
{
	Position3D_t1995122365::get_offset_of_v0_9(),
	Position3D_t1995122365::get_offset_of_v1_10(),
	Position3D_t1995122365::get_offset_of_v1Func_11(),
	Position3D_t1995122365::get_offset_of_useGlobalValues_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (Rotation_t2635690123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2192[2] = 
{
	Rotation_t2635690123::get_offset_of_q0_9(),
	Rotation_t2635690123::get_offset_of_q1_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (Scale2D_t1489313937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2193[2] = 
{
	Scale2D_t1489313937::get_offset_of_v0_9(),
	Scale2D_t1489313937::get_offset_of_v1_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (Shake2D_t1926097863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2194[2] = 
{
	Shake2D_t1926097863::get_offset_of_magnitude_9(),
	Shake2D_t1926097863::get_offset_of_times_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (Curve_t1446801592), -1, sizeof(Curve_t1446801592_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2195[28] = 
{
	Curve_t1446801592_StaticFields::get_offset_of_Linear_0(),
	Curve_t1446801592_StaticFields::get_offset_of_CubicEaseIn_1(),
	Curve_t1446801592_StaticFields::get_offset_of_CubicEaseOut_2(),
	Curve_t1446801592_StaticFields::get_offset_of_CubicEaseInOut_3(),
	Curve_t1446801592_StaticFields::get_offset_of_QuarticEaseIn_4(),
	Curve_t1446801592_StaticFields::get_offset_of_QuarticEaseOut_5(),
	Curve_t1446801592_StaticFields::get_offset_of_QuarticEaseInOut_6(),
	Curve_t1446801592_StaticFields::get_offset_of_QuadricEaseIn_7(),
	Curve_t1446801592_StaticFields::get_offset_of_QuadricEaseOut_8(),
	Curve_t1446801592_StaticFields::get_offset_of_QuadricEaseInOut_9(),
	Curve_t1446801592_StaticFields::get_offset_of_BounceEaseOut_10(),
	Curve_t1446801592_StaticFields::get_offset_of_BounceEaseIn_11(),
	Curve_t1446801592_StaticFields::get_offset_of_BounceEaseInOut_12(),
	Curve_t1446801592_StaticFields::get_offset_of_BackEaseIn_13(),
	Curve_t1446801592_StaticFields::get_offset_of_BackEaseOut_14(),
	Curve_t1446801592_StaticFields::get_offset_of_BackEaseInOut_15(),
	Curve_t1446801592_StaticFields::get_offset_of_SineEaseIn_16(),
	Curve_t1446801592_StaticFields::get_offset_of_SineEaseOut_17(),
	Curve_t1446801592_StaticFields::get_offset_of_SineEaseInOut_18(),
	Curve_t1446801592_StaticFields::get_offset_of_ElasticEaseIn_19(),
	Curve_t1446801592_StaticFields::get_offset_of_ElasticEaseOut_20(),
	Curve_t1446801592_StaticFields::get_offset_of_ElasticEaseInOut_21(),
	Curve_t1446801592_StaticFields::get_offset_of_CircularEaseIn_22(),
	Curve_t1446801592_StaticFields::get_offset_of_CircularEaseOut_23(),
	Curve_t1446801592_StaticFields::get_offset_of_CircularEaseInOut_24(),
	Curve_t1446801592_StaticFields::get_offset_of_ExponentialEaseIn_25(),
	Curve_t1446801592_StaticFields::get_offset_of_ExponentialEaseOut_26(),
	Curve_t1446801592_StaticFields::get_offset_of_ExponentialEaseInOut_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (LinearCurve_t731416727), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (CubicEaseInCurve_t4146432715), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (CubicEaseInOutCurve_t574754590), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (CubicEaseOutCurve_t2195481624), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
