﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Func`2<TMPro.TMP_Glyph,System.Int32>
struct Func_2_t3920652434;
// System.Collections.Generic.List`1<TMPro.TMP_Dropdown/OptionData>
struct List_1_t2586715010;
// UnityEngine.Sprite
struct Sprite_t280657092;
// TMPro.TMP_Dropdown
struct TMP_Dropdown_t3024694699;
// TMPro.TMP_Dropdown/DropdownItem
struct DropdownItem_t1842596711;
// UnityEngine.Material
struct Material_t340375123;
// TMPro.TMP_InputField
struct TMP_InputField_t1099764886;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Char>
struct Dictionary_2_t2523173801;
// System.Collections.Generic.List`1<TMPro.TMP_MaterialManager/MaskingMaterial>
struct List_1_t1488971017;
// System.Collections.Generic.Dictionary`2<System.Int64,TMPro.TMP_MaterialManager/FallbackMaterial>
struct Dictionary_2_t939051307;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int64>
struct Dictionary_2_t2625280635;
// System.Collections.Generic.List`1<TMPro.TMP_MaterialManager/FallbackMaterial>
struct List_1_t1348486265;
// System.Collections.Generic.LinkedList`1<System.Action>
struct LinkedList_1_t104023486;
// System.Collections.Generic.Dictionary`2<System.Action,System.Collections.Generic.LinkedListNode`1<System.Action>>
struct Dictionary_2_t4196256347;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Material>
struct Dictionary_2_t3524055750;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_FontAsset>
struct Dictionary_2_t3548062253;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_SpriteAsset>
struct Dictionary_2_t3668501260;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Byte
struct Byte_t1134296376;
// System.Double
struct Double_t594665363;
// System.UInt16
struct UInt16_t2177724958;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t364381626;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_t484820633;
// TMPro.FloatTween/FloatTweenCallback
struct FloatTweenCallback_t556094317;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t934056436;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// UnityEngine.Color32[]
struct Color32U5BU5D_t3850468773;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t4072576034;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t496136383;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3807901092;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// TMPro.ColorTween/ColorTweenCallback
struct ColorTweenCallback_t824784811;
// TMPro.TMP_TextElement
struct TMP_TextElement_t129727469;
// System.Collections.Generic.List`1<TMPro.TMP_Style>
struct List_1_t656488210;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_Style>
struct Dictionary_2_t2368094095;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4286651560;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// TMPro.TMP_SpriteAnimator
struct TMP_SpriteAnimator_t2836635477;
// TMPro.TMP_Sprite
struct TMP_Sprite_t554067146;
// System.Collections.Generic.List`1<TMPro.TMP_FontAsset>
struct List_1_t1836456368;
// TMPro.TMP_StyleSheet
struct TMP_StyleSheet_t917564226;
// UnityEngine.TextAsset
struct TextAsset_t3022178571;
// TMPro.TMP_Settings/LineBreakingTable
struct LineBreakingTable_t1457697482;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1690781147;
// TMPro.FaceInfo
struct FaceInfo_t2243299176;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// System.Collections.Generic.List`1<TMPro.TMP_Glyph>
struct List_1_t2053922575;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_Glyph>
struct Dictionary_2_t3765528460;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.KerningPair>
struct Dictionary_2_t1159568920;
// TMPro.KerningTable
struct KerningTable_t2322366871;
// TMPro.KerningPair
struct KerningPair_t2270855589;
// TMPro.TMP_FontWeights[]
struct TMP_FontWeightsU5BU5D_t3691718250;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1839659084;
// UnityEngine.Texture
struct Texture_t3661962703;
// System.Collections.Generic.List`1<TMPro.TMP_Sprite>
struct List_1_t2026141888;
// System.Collections.Generic.List`1<TMPro.TMP_SpriteAsset>
struct List_1_t1956895375;
// TMPro.TMP_Text
struct TMP_Text_t2599618874;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.UI.Toggle
struct Toggle_t2735377061;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.MeshFilter
struct MeshFilter_t3523625662;
// UnityEngine.BoxCollider
struct BoxCollider_t1640800422;
// TMPro.TextMeshPro
struct TextMeshPro_t2393593166;
// TMPro.InlineGraphic
struct InlineGraphic_t2901727699;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>
struct Dictionary_2_t3280968592;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t427135887;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2532145056;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1260619206;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t731888065;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t1494447233;
// TMPro.TMP_ScrollbarEventHandler
struct TMP_ScrollbarEventHandler_t374199898;
// TMPro.TMP_InputField/SubmitEvent
struct SubmitEvent_t1343580625;
// TMPro.TMP_InputField/SelectionEvent
struct SelectionEvent_t4268942288;
// TMPro.TMP_InputField/TextSelectionEvent
struct TextSelectionEvent_t1023421581;
// TMPro.TMP_InputField/OnChangeEvent
struct OnChangeEvent_t4257774360;
// TMPro.TMP_InputField/OnValidateInput
struct OnValidateInput_t373909109;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// TMPro.TMP_InputValidator
struct TMP_InputValidator_t1385053824;
// UnityEngine.Event
struct Event_t2956885303;
// TMPro.TMP_Dropdown/OptionDataList
struct OptionDataList_t2293557512;
// TMPro.TMP_Dropdown/DropdownEvent
struct DropdownEvent_t1704673280;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Collections.Generic.List`1<TMPro.TMP_Dropdown/DropdownItem>
struct List_1_t3314671453;
// TMPro.TweenRunner`1<TMPro.FloatTween>
struct TweenRunner_1_t3214153611;
// TMPro.TMP_Dropdown/OptionData
struct OptionData_t1114640268;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// TMPro.InlineGraphicManager
struct InlineGraphicManager_t2871008645;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t529313277;

struct Vector3_t3722313464 ;
struct Vector4_t3319028937 ;
struct Vector2_t2156229523 ;
struct Color32_t2600501292 ;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745547_H
#define U3CMODULEU3E_T692745547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745547 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745547_H
#ifndef TMP_STYLE_T3479380764_H
#define TMP_STYLE_T3479380764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Style
struct  TMP_Style_t3479380764  : public RuntimeObject
{
public:
	// System.String TMPro.TMP_Style::m_Name
	String_t* ___m_Name_0;
	// System.Int32 TMPro.TMP_Style::m_HashCode
	int32_t ___m_HashCode_1;
	// System.String TMPro.TMP_Style::m_OpeningDefinition
	String_t* ___m_OpeningDefinition_2;
	// System.String TMPro.TMP_Style::m_ClosingDefinition
	String_t* ___m_ClosingDefinition_3;
	// System.Int32[] TMPro.TMP_Style::m_OpeningTagArray
	Int32U5BU5D_t385246372* ___m_OpeningTagArray_4;
	// System.Int32[] TMPro.TMP_Style::m_ClosingTagArray
	Int32U5BU5D_t385246372* ___m_ClosingTagArray_5;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(TMP_Style_t3479380764, ___m_Name_0)); }
	inline String_t* get_m_Name_0() const { return ___m_Name_0; }
	inline String_t** get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(String_t* value)
	{
		___m_Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_0), value);
	}

	inline static int32_t get_offset_of_m_HashCode_1() { return static_cast<int32_t>(offsetof(TMP_Style_t3479380764, ___m_HashCode_1)); }
	inline int32_t get_m_HashCode_1() const { return ___m_HashCode_1; }
	inline int32_t* get_address_of_m_HashCode_1() { return &___m_HashCode_1; }
	inline void set_m_HashCode_1(int32_t value)
	{
		___m_HashCode_1 = value;
	}

	inline static int32_t get_offset_of_m_OpeningDefinition_2() { return static_cast<int32_t>(offsetof(TMP_Style_t3479380764, ___m_OpeningDefinition_2)); }
	inline String_t* get_m_OpeningDefinition_2() const { return ___m_OpeningDefinition_2; }
	inline String_t** get_address_of_m_OpeningDefinition_2() { return &___m_OpeningDefinition_2; }
	inline void set_m_OpeningDefinition_2(String_t* value)
	{
		___m_OpeningDefinition_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_OpeningDefinition_2), value);
	}

	inline static int32_t get_offset_of_m_ClosingDefinition_3() { return static_cast<int32_t>(offsetof(TMP_Style_t3479380764, ___m_ClosingDefinition_3)); }
	inline String_t* get_m_ClosingDefinition_3() const { return ___m_ClosingDefinition_3; }
	inline String_t** get_address_of_m_ClosingDefinition_3() { return &___m_ClosingDefinition_3; }
	inline void set_m_ClosingDefinition_3(String_t* value)
	{
		___m_ClosingDefinition_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClosingDefinition_3), value);
	}

	inline static int32_t get_offset_of_m_OpeningTagArray_4() { return static_cast<int32_t>(offsetof(TMP_Style_t3479380764, ___m_OpeningTagArray_4)); }
	inline Int32U5BU5D_t385246372* get_m_OpeningTagArray_4() const { return ___m_OpeningTagArray_4; }
	inline Int32U5BU5D_t385246372** get_address_of_m_OpeningTagArray_4() { return &___m_OpeningTagArray_4; }
	inline void set_m_OpeningTagArray_4(Int32U5BU5D_t385246372* value)
	{
		___m_OpeningTagArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_OpeningTagArray_4), value);
	}

	inline static int32_t get_offset_of_m_ClosingTagArray_5() { return static_cast<int32_t>(offsetof(TMP_Style_t3479380764, ___m_ClosingTagArray_5)); }
	inline Int32U5BU5D_t385246372* get_m_ClosingTagArray_5() const { return ___m_ClosingTagArray_5; }
	inline Int32U5BU5D_t385246372** get_address_of_m_ClosingTagArray_5() { return &___m_ClosingTagArray_5; }
	inline void set_m_ClosingTagArray_5(Int32U5BU5D_t385246372* value)
	{
		___m_ClosingTagArray_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClosingTagArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_STYLE_T3479380764_H
#ifndef TMP_COMPATIBILITY_T2923561388_H
#define TMP_COMPATIBILITY_T2923561388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Compatibility
struct  TMP_Compatibility_t2923561388  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_COMPATIBILITY_T2923561388_H
#ifndef TMP_TEXTELEMENT_T129727469_H
#define TMP_TEXTELEMENT_T129727469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElement
struct  TMP_TextElement_t129727469  : public RuntimeObject
{
public:
	// System.Int32 TMPro.TMP_TextElement::id
	int32_t ___id_0;
	// System.Single TMPro.TMP_TextElement::x
	float ___x_1;
	// System.Single TMPro.TMP_TextElement::y
	float ___y_2;
	// System.Single TMPro.TMP_TextElement::width
	float ___width_3;
	// System.Single TMPro.TMP_TextElement::height
	float ___height_4;
	// System.Single TMPro.TMP_TextElement::xOffset
	float ___xOffset_5;
	// System.Single TMPro.TMP_TextElement::yOffset
	float ___yOffset_6;
	// System.Single TMPro.TMP_TextElement::xAdvance
	float ___xAdvance_7;
	// System.Single TMPro.TMP_TextElement::scale
	float ___scale_8;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___width_3)); }
	inline float get_width_3() const { return ___width_3; }
	inline float* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(float value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___height_4)); }
	inline float get_height_4() const { return ___height_4; }
	inline float* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(float value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_xOffset_5() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___xOffset_5)); }
	inline float get_xOffset_5() const { return ___xOffset_5; }
	inline float* get_address_of_xOffset_5() { return &___xOffset_5; }
	inline void set_xOffset_5(float value)
	{
		___xOffset_5 = value;
	}

	inline static int32_t get_offset_of_yOffset_6() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___yOffset_6)); }
	inline float get_yOffset_6() const { return ___yOffset_6; }
	inline float* get_address_of_yOffset_6() { return &___yOffset_6; }
	inline void set_yOffset_6(float value)
	{
		___yOffset_6 = value;
	}

	inline static int32_t get_offset_of_xAdvance_7() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___xAdvance_7)); }
	inline float get_xAdvance_7() const { return ___xAdvance_7; }
	inline float* get_address_of_xAdvance_7() { return &___xAdvance_7; }
	inline void set_xAdvance_7(float value)
	{
		___xAdvance_7 = value;
	}

	inline static int32_t get_offset_of_scale_8() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___scale_8)); }
	inline float get_scale_8() const { return ___scale_8; }
	inline float* get_address_of_scale_8() { return &___scale_8; }
	inline void set_scale_8(float value)
	{
		___scale_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENT_T129727469_H
#ifndef U3CU3EC_T2662116725_H
#define U3CU3EC_T2662116725_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontAsset/<>c
struct  U3CU3Ec_t2662116725  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t2662116725_StaticFields
{
public:
	// TMPro.TMP_FontAsset/<>c TMPro.TMP_FontAsset/<>c::<>9
	U3CU3Ec_t2662116725 * ___U3CU3E9_0;
	// System.Func`2<TMPro.TMP_Glyph,System.Int32> TMPro.TMP_FontAsset/<>c::<>9__34_0
	Func_2_t3920652434 * ___U3CU3E9__34_0_1;
	// System.Func`2<TMPro.TMP_Glyph,System.Int32> TMPro.TMP_FontAsset/<>c::<>9__37_0
	Func_2_t3920652434 * ___U3CU3E9__37_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2662116725_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2662116725 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2662116725 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2662116725 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__34_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2662116725_StaticFields, ___U3CU3E9__34_0_1)); }
	inline Func_2_t3920652434 * get_U3CU3E9__34_0_1() const { return ___U3CU3E9__34_0_1; }
	inline Func_2_t3920652434 ** get_address_of_U3CU3E9__34_0_1() { return &___U3CU3E9__34_0_1; }
	inline void set_U3CU3E9__34_0_1(Func_2_t3920652434 * value)
	{
		___U3CU3E9__34_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__34_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__37_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2662116725_StaticFields, ___U3CU3E9__37_0_2)); }
	inline Func_2_t3920652434 * get_U3CU3E9__37_0_2() const { return ___U3CU3E9__37_0_2; }
	inline Func_2_t3920652434 ** get_address_of_U3CU3E9__37_0_2() { return &___U3CU3E9__37_0_2; }
	inline void set_U3CU3E9__37_0_2(Func_2_t3920652434 * value)
	{
		___U3CU3E9__37_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__37_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T2662116725_H
#ifndef OPTIONDATALIST_T2293557512_H
#define OPTIONDATALIST_T2293557512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Dropdown/OptionDataList
struct  OptionDataList_t2293557512  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.TMP_Dropdown/OptionData> TMPro.TMP_Dropdown/OptionDataList::m_Options
	List_1_t2586715010 * ___m_Options_0;

public:
	inline static int32_t get_offset_of_m_Options_0() { return static_cast<int32_t>(offsetof(OptionDataList_t2293557512, ___m_Options_0)); }
	inline List_1_t2586715010 * get_m_Options_0() const { return ___m_Options_0; }
	inline List_1_t2586715010 ** get_address_of_m_Options_0() { return &___m_Options_0; }
	inline void set_m_Options_0(List_1_t2586715010 * value)
	{
		___m_Options_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Options_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPTIONDATALIST_T2293557512_H
#ifndef OPTIONDATA_T1114640268_H
#define OPTIONDATA_T1114640268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Dropdown/OptionData
struct  OptionData_t1114640268  : public RuntimeObject
{
public:
	// System.String TMPro.TMP_Dropdown/OptionData::m_Text
	String_t* ___m_Text_0;
	// UnityEngine.Sprite TMPro.TMP_Dropdown/OptionData::m_Image
	Sprite_t280657092 * ___m_Image_1;

public:
	inline static int32_t get_offset_of_m_Text_0() { return static_cast<int32_t>(offsetof(OptionData_t1114640268, ___m_Text_0)); }
	inline String_t* get_m_Text_0() const { return ___m_Text_0; }
	inline String_t** get_address_of_m_Text_0() { return &___m_Text_0; }
	inline void set_m_Text_0(String_t* value)
	{
		___m_Text_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_0), value);
	}

	inline static int32_t get_offset_of_m_Image_1() { return static_cast<int32_t>(offsetof(OptionData_t1114640268, ___m_Image_1)); }
	inline Sprite_t280657092 * get_m_Image_1() const { return ___m_Image_1; }
	inline Sprite_t280657092 ** get_address_of_m_Image_1() { return &___m_Image_1; }
	inline void set_m_Image_1(Sprite_t280657092 * value)
	{
		___m_Image_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Image_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPTIONDATA_T1114640268_H
#ifndef U3CDELAYEDDESTROYDROPDOWNLISTU3ED__68_T3448141726_H
#define U3CDELAYEDDESTROYDROPDOWNLISTU3ED__68_T3448141726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Dropdown/<DelayedDestroyDropdownList>d__68
struct  U3CDelayedDestroyDropdownListU3Ed__68_t3448141726  : public RuntimeObject
{
public:
	// System.Int32 TMPro.TMP_Dropdown/<DelayedDestroyDropdownList>d__68::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.TMP_Dropdown/<DelayedDestroyDropdownList>d__68::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single TMPro.TMP_Dropdown/<DelayedDestroyDropdownList>d__68::delay
	float ___delay_2;
	// TMPro.TMP_Dropdown TMPro.TMP_Dropdown/<DelayedDestroyDropdownList>d__68::<>4__this
	TMP_Dropdown_t3024694699 * ___U3CU3E4__this_3;
	// System.Int32 TMPro.TMP_Dropdown/<DelayedDestroyDropdownList>d__68::<i>5__1
	int32_t ___U3CiU3E5__1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDelayedDestroyDropdownListU3Ed__68_t3448141726, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDelayedDestroyDropdownListU3Ed__68_t3448141726, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CDelayedDestroyDropdownListU3Ed__68_t3448141726, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CDelayedDestroyDropdownListU3Ed__68_t3448141726, ___U3CU3E4__this_3)); }
	inline TMP_Dropdown_t3024694699 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline TMP_Dropdown_t3024694699 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(TMP_Dropdown_t3024694699 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CDelayedDestroyDropdownListU3Ed__68_t3448141726, ___U3CiU3E5__1_4)); }
	inline int32_t get_U3CiU3E5__1_4() const { return ___U3CiU3E5__1_4; }
	inline int32_t* get_address_of_U3CiU3E5__1_4() { return &___U3CiU3E5__1_4; }
	inline void set_U3CiU3E5__1_4(int32_t value)
	{
		___U3CiU3E5__1_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYEDDESTROYDROPDOWNLISTU3ED__68_T3448141726_H
#ifndef U3CU3EC__DISPLAYCLASS56_0_T2378633237_H
#define U3CU3EC__DISPLAYCLASS56_0_T2378633237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Dropdown/<>c__DisplayClass56_0
struct  U3CU3Ec__DisplayClass56_0_t2378633237  : public RuntimeObject
{
public:
	// TMPro.TMP_Dropdown/DropdownItem TMPro.TMP_Dropdown/<>c__DisplayClass56_0::item
	DropdownItem_t1842596711 * ___item_0;
	// TMPro.TMP_Dropdown TMPro.TMP_Dropdown/<>c__DisplayClass56_0::<>4__this
	TMP_Dropdown_t3024694699 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass56_0_t2378633237, ___item_0)); }
	inline DropdownItem_t1842596711 * get_item_0() const { return ___item_0; }
	inline DropdownItem_t1842596711 ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(DropdownItem_t1842596711 * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier((&___item_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass56_0_t2378633237, ___U3CU3E4__this_1)); }
	inline TMP_Dropdown_t3024694699 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline TMP_Dropdown_t3024694699 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(TMP_Dropdown_t3024694699 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS56_0_T2378633237_H
#ifndef U3CU3EC__DISPLAYCLASS10_0_T173182959_H
#define U3CU3EC__DISPLAYCLASS10_0_T173182959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MaterialManager/<>c__DisplayClass10_0
struct  U3CU3Ec__DisplayClass10_0_t173182959  : public RuntimeObject
{
public:
	// UnityEngine.Material TMPro.TMP_MaterialManager/<>c__DisplayClass10_0::stencilMaterial
	Material_t340375123 * ___stencilMaterial_0;

public:
	inline static int32_t get_offset_of_stencilMaterial_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t173182959, ___stencilMaterial_0)); }
	inline Material_t340375123 * get_stencilMaterial_0() const { return ___stencilMaterial_0; }
	inline Material_t340375123 ** get_address_of_stencilMaterial_0() { return &___stencilMaterial_0; }
	inline void set_stencilMaterial_0(Material_t340375123 * value)
	{
		___stencilMaterial_0 = value;
		Il2CppCodeGenWriteBarrier((&___stencilMaterial_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS10_0_T173182959_H
#ifndef MASKINGMATERIAL_T16896275_H
#define MASKINGMATERIAL_T16896275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MaterialManager/MaskingMaterial
struct  MaskingMaterial_t16896275  : public RuntimeObject
{
public:
	// UnityEngine.Material TMPro.TMP_MaterialManager/MaskingMaterial::baseMaterial
	Material_t340375123 * ___baseMaterial_0;
	// UnityEngine.Material TMPro.TMP_MaterialManager/MaskingMaterial::stencilMaterial
	Material_t340375123 * ___stencilMaterial_1;
	// System.Int32 TMPro.TMP_MaterialManager/MaskingMaterial::count
	int32_t ___count_2;
	// System.Int32 TMPro.TMP_MaterialManager/MaskingMaterial::stencilID
	int32_t ___stencilID_3;

public:
	inline static int32_t get_offset_of_baseMaterial_0() { return static_cast<int32_t>(offsetof(MaskingMaterial_t16896275, ___baseMaterial_0)); }
	inline Material_t340375123 * get_baseMaterial_0() const { return ___baseMaterial_0; }
	inline Material_t340375123 ** get_address_of_baseMaterial_0() { return &___baseMaterial_0; }
	inline void set_baseMaterial_0(Material_t340375123 * value)
	{
		___baseMaterial_0 = value;
		Il2CppCodeGenWriteBarrier((&___baseMaterial_0), value);
	}

	inline static int32_t get_offset_of_stencilMaterial_1() { return static_cast<int32_t>(offsetof(MaskingMaterial_t16896275, ___stencilMaterial_1)); }
	inline Material_t340375123 * get_stencilMaterial_1() const { return ___stencilMaterial_1; }
	inline Material_t340375123 ** get_address_of_stencilMaterial_1() { return &___stencilMaterial_1; }
	inline void set_stencilMaterial_1(Material_t340375123 * value)
	{
		___stencilMaterial_1 = value;
		Il2CppCodeGenWriteBarrier((&___stencilMaterial_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(MaskingMaterial_t16896275, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_stencilID_3() { return static_cast<int32_t>(offsetof(MaskingMaterial_t16896275, ___stencilID_3)); }
	inline int32_t get_stencilID_3() const { return ___stencilID_3; }
	inline int32_t* get_address_of_stencilID_3() { return &___stencilID_3; }
	inline void set_stencilID_3(int32_t value)
	{
		___stencilID_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKINGMATERIAL_T16896275_H
#ifndef U3CU3EC__DISPLAYCLASS12_0_T173182957_H
#define U3CU3EC__DISPLAYCLASS12_0_T173182957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MaterialManager/<>c__DisplayClass12_0
struct  U3CU3Ec__DisplayClass12_0_t173182957  : public RuntimeObject
{
public:
	// UnityEngine.Material TMPro.TMP_MaterialManager/<>c__DisplayClass12_0::stencilMaterial
	Material_t340375123 * ___stencilMaterial_0;

public:
	inline static int32_t get_offset_of_stencilMaterial_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t173182957, ___stencilMaterial_0)); }
	inline Material_t340375123 * get_stencilMaterial_0() const { return ___stencilMaterial_0; }
	inline Material_t340375123 ** get_address_of_stencilMaterial_0() { return &___stencilMaterial_0; }
	inline void set_stencilMaterial_0(Material_t340375123 * value)
	{
		___stencilMaterial_0 = value;
		Il2CppCodeGenWriteBarrier((&___stencilMaterial_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS12_0_T173182957_H
#ifndef U3CU3EC__DISPLAYCLASS14_0_T173182963_H
#define U3CU3EC__DISPLAYCLASS14_0_T173182963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MaterialManager/<>c__DisplayClass14_0
struct  U3CU3Ec__DisplayClass14_0_t173182963  : public RuntimeObject
{
public:
	// UnityEngine.Material TMPro.TMP_MaterialManager/<>c__DisplayClass14_0::baseMaterial
	Material_t340375123 * ___baseMaterial_0;

public:
	inline static int32_t get_offset_of_baseMaterial_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass14_0_t173182963, ___baseMaterial_0)); }
	inline Material_t340375123 * get_baseMaterial_0() const { return ___baseMaterial_0; }
	inline Material_t340375123 ** get_address_of_baseMaterial_0() { return &___baseMaterial_0; }
	inline void set_baseMaterial_0(Material_t340375123 * value)
	{
		___baseMaterial_0 = value;
		Il2CppCodeGenWriteBarrier((&___baseMaterial_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS14_0_T173182963_H
#ifndef U3CU3EC__DISPLAYCLASS13_0_T173182956_H
#define U3CU3EC__DISPLAYCLASS13_0_T173182956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MaterialManager/<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_t173182956  : public RuntimeObject
{
public:
	// UnityEngine.Material TMPro.TMP_MaterialManager/<>c__DisplayClass13_0::stencilMaterial
	Material_t340375123 * ___stencilMaterial_0;

public:
	inline static int32_t get_offset_of_stencilMaterial_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t173182956, ___stencilMaterial_0)); }
	inline Material_t340375123 * get_stencilMaterial_0() const { return ___stencilMaterial_0; }
	inline Material_t340375123 ** get_address_of_stencilMaterial_0() { return &___stencilMaterial_0; }
	inline void set_stencilMaterial_0(Material_t340375123 * value)
	{
		___stencilMaterial_0 = value;
		Il2CppCodeGenWriteBarrier((&___stencilMaterial_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_0_T173182956_H
#ifndef U3CCARETBLINKU3ED__238_T3270037332_H
#define U3CCARETBLINKU3ED__238_T3270037332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/<CaretBlink>d__238
struct  U3CCaretBlinkU3Ed__238_t3270037332  : public RuntimeObject
{
public:
	// System.Int32 TMPro.TMP_InputField/<CaretBlink>d__238::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.TMP_InputField/<CaretBlink>d__238::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TMPro.TMP_InputField TMPro.TMP_InputField/<CaretBlink>d__238::<>4__this
	TMP_InputField_t1099764886 * ___U3CU3E4__this_2;
	// System.Single TMPro.TMP_InputField/<CaretBlink>d__238::<blinkPeriod>5__1
	float ___U3CblinkPeriodU3E5__1_3;
	// System.Boolean TMPro.TMP_InputField/<CaretBlink>d__238::<blinkState>5__2
	bool ___U3CblinkStateU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCaretBlinkU3Ed__238_t3270037332, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCaretBlinkU3Ed__238_t3270037332, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CCaretBlinkU3Ed__238_t3270037332, ___U3CU3E4__this_2)); }
	inline TMP_InputField_t1099764886 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TMP_InputField_t1099764886 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TMP_InputField_t1099764886 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CblinkPeriodU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CCaretBlinkU3Ed__238_t3270037332, ___U3CblinkPeriodU3E5__1_3)); }
	inline float get_U3CblinkPeriodU3E5__1_3() const { return ___U3CblinkPeriodU3E5__1_3; }
	inline float* get_address_of_U3CblinkPeriodU3E5__1_3() { return &___U3CblinkPeriodU3E5__1_3; }
	inline void set_U3CblinkPeriodU3E5__1_3(float value)
	{
		___U3CblinkPeriodU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CblinkStateU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CCaretBlinkU3Ed__238_t3270037332, ___U3CblinkStateU3E5__2_4)); }
	inline bool get_U3CblinkStateU3E5__2_4() const { return ___U3CblinkStateU3E5__2_4; }
	inline bool* get_address_of_U3CblinkStateU3E5__2_4() { return &___U3CblinkStateU3E5__2_4; }
	inline void set_U3CblinkStateU3E5__2_4(bool value)
	{
		___U3CblinkStateU3E5__2_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCARETBLINKU3ED__238_T3270037332_H
#ifndef LINEBREAKINGTABLE_T1457697482_H
#define LINEBREAKINGTABLE_T1457697482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Settings/LineBreakingTable
struct  LineBreakingTable_t1457697482  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Char> TMPro.TMP_Settings/LineBreakingTable::leadingCharacters
	Dictionary_2_t2523173801 * ___leadingCharacters_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Char> TMPro.TMP_Settings/LineBreakingTable::followingCharacters
	Dictionary_2_t2523173801 * ___followingCharacters_1;

public:
	inline static int32_t get_offset_of_leadingCharacters_0() { return static_cast<int32_t>(offsetof(LineBreakingTable_t1457697482, ___leadingCharacters_0)); }
	inline Dictionary_2_t2523173801 * get_leadingCharacters_0() const { return ___leadingCharacters_0; }
	inline Dictionary_2_t2523173801 ** get_address_of_leadingCharacters_0() { return &___leadingCharacters_0; }
	inline void set_leadingCharacters_0(Dictionary_2_t2523173801 * value)
	{
		___leadingCharacters_0 = value;
		Il2CppCodeGenWriteBarrier((&___leadingCharacters_0), value);
	}

	inline static int32_t get_offset_of_followingCharacters_1() { return static_cast<int32_t>(offsetof(LineBreakingTable_t1457697482, ___followingCharacters_1)); }
	inline Dictionary_2_t2523173801 * get_followingCharacters_1() const { return ___followingCharacters_1; }
	inline Dictionary_2_t2523173801 ** get_address_of_followingCharacters_1() { return &___followingCharacters_1; }
	inline void set_followingCharacters_1(Dictionary_2_t2523173801 * value)
	{
		___followingCharacters_1 = value;
		Il2CppCodeGenWriteBarrier((&___followingCharacters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEBREAKINGTABLE_T1457697482_H
#ifndef SETPROPERTYUTILITY_T2931591262_H
#define SETPROPERTYUTILITY_T2931591262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SetPropertyUtility
struct  SetPropertyUtility_t2931591262  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETPROPERTYUTILITY_T2931591262_H
#ifndef FALLBACKMATERIAL_T4171378819_H
#define FALLBACKMATERIAL_T4171378819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MaterialManager/FallbackMaterial
struct  FallbackMaterial_t4171378819  : public RuntimeObject
{
public:
	// System.Int32 TMPro.TMP_MaterialManager/FallbackMaterial::baseID
	int32_t ___baseID_0;
	// UnityEngine.Material TMPro.TMP_MaterialManager/FallbackMaterial::baseMaterial
	Material_t340375123 * ___baseMaterial_1;
	// System.Int64 TMPro.TMP_MaterialManager/FallbackMaterial::fallbackID
	int64_t ___fallbackID_2;
	// UnityEngine.Material TMPro.TMP_MaterialManager/FallbackMaterial::fallbackMaterial
	Material_t340375123 * ___fallbackMaterial_3;
	// System.Int32 TMPro.TMP_MaterialManager/FallbackMaterial::count
	int32_t ___count_4;

public:
	inline static int32_t get_offset_of_baseID_0() { return static_cast<int32_t>(offsetof(FallbackMaterial_t4171378819, ___baseID_0)); }
	inline int32_t get_baseID_0() const { return ___baseID_0; }
	inline int32_t* get_address_of_baseID_0() { return &___baseID_0; }
	inline void set_baseID_0(int32_t value)
	{
		___baseID_0 = value;
	}

	inline static int32_t get_offset_of_baseMaterial_1() { return static_cast<int32_t>(offsetof(FallbackMaterial_t4171378819, ___baseMaterial_1)); }
	inline Material_t340375123 * get_baseMaterial_1() const { return ___baseMaterial_1; }
	inline Material_t340375123 ** get_address_of_baseMaterial_1() { return &___baseMaterial_1; }
	inline void set_baseMaterial_1(Material_t340375123 * value)
	{
		___baseMaterial_1 = value;
		Il2CppCodeGenWriteBarrier((&___baseMaterial_1), value);
	}

	inline static int32_t get_offset_of_fallbackID_2() { return static_cast<int32_t>(offsetof(FallbackMaterial_t4171378819, ___fallbackID_2)); }
	inline int64_t get_fallbackID_2() const { return ___fallbackID_2; }
	inline int64_t* get_address_of_fallbackID_2() { return &___fallbackID_2; }
	inline void set_fallbackID_2(int64_t value)
	{
		___fallbackID_2 = value;
	}

	inline static int32_t get_offset_of_fallbackMaterial_3() { return static_cast<int32_t>(offsetof(FallbackMaterial_t4171378819, ___fallbackMaterial_3)); }
	inline Material_t340375123 * get_fallbackMaterial_3() const { return ___fallbackMaterial_3; }
	inline Material_t340375123 ** get_address_of_fallbackMaterial_3() { return &___fallbackMaterial_3; }
	inline void set_fallbackMaterial_3(Material_t340375123 * value)
	{
		___fallbackMaterial_3 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackMaterial_3), value);
	}

	inline static int32_t get_offset_of_count_4() { return static_cast<int32_t>(offsetof(FallbackMaterial_t4171378819, ___count_4)); }
	inline int32_t get_count_4() const { return ___count_4; }
	inline int32_t* get_address_of_count_4() { return &___count_4; }
	inline void set_count_4(int32_t value)
	{
		___count_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FALLBACKMATERIAL_T4171378819_H
#ifndef TMP_MATERIALMANAGER_T2944071966_H
#define TMP_MATERIALMANAGER_T2944071966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MaterialManager
struct  TMP_MaterialManager_t2944071966  : public RuntimeObject
{
public:

public:
};

struct TMP_MaterialManager_t2944071966_StaticFields
{
public:
	// System.Collections.Generic.List`1<TMPro.TMP_MaterialManager/MaskingMaterial> TMPro.TMP_MaterialManager::m_materialList
	List_1_t1488971017 * ___m_materialList_0;
	// System.Collections.Generic.Dictionary`2<System.Int64,TMPro.TMP_MaterialManager/FallbackMaterial> TMPro.TMP_MaterialManager::m_fallbackMaterials
	Dictionary_2_t939051307 * ___m_fallbackMaterials_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int64> TMPro.TMP_MaterialManager::m_fallbackMaterialLookup
	Dictionary_2_t2625280635 * ___m_fallbackMaterialLookup_2;
	// System.Collections.Generic.List`1<TMPro.TMP_MaterialManager/FallbackMaterial> TMPro.TMP_MaterialManager::m_fallbackCleanupList
	List_1_t1348486265 * ___m_fallbackCleanupList_3;
	// System.Boolean TMPro.TMP_MaterialManager::isFallbackListDirty
	bool ___isFallbackListDirty_4;

public:
	inline static int32_t get_offset_of_m_materialList_0() { return static_cast<int32_t>(offsetof(TMP_MaterialManager_t2944071966_StaticFields, ___m_materialList_0)); }
	inline List_1_t1488971017 * get_m_materialList_0() const { return ___m_materialList_0; }
	inline List_1_t1488971017 ** get_address_of_m_materialList_0() { return &___m_materialList_0; }
	inline void set_m_materialList_0(List_1_t1488971017 * value)
	{
		___m_materialList_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialList_0), value);
	}

	inline static int32_t get_offset_of_m_fallbackMaterials_1() { return static_cast<int32_t>(offsetof(TMP_MaterialManager_t2944071966_StaticFields, ___m_fallbackMaterials_1)); }
	inline Dictionary_2_t939051307 * get_m_fallbackMaterials_1() const { return ___m_fallbackMaterials_1; }
	inline Dictionary_2_t939051307 ** get_address_of_m_fallbackMaterials_1() { return &___m_fallbackMaterials_1; }
	inline void set_m_fallbackMaterials_1(Dictionary_2_t939051307 * value)
	{
		___m_fallbackMaterials_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackMaterials_1), value);
	}

	inline static int32_t get_offset_of_m_fallbackMaterialLookup_2() { return static_cast<int32_t>(offsetof(TMP_MaterialManager_t2944071966_StaticFields, ___m_fallbackMaterialLookup_2)); }
	inline Dictionary_2_t2625280635 * get_m_fallbackMaterialLookup_2() const { return ___m_fallbackMaterialLookup_2; }
	inline Dictionary_2_t2625280635 ** get_address_of_m_fallbackMaterialLookup_2() { return &___m_fallbackMaterialLookup_2; }
	inline void set_m_fallbackMaterialLookup_2(Dictionary_2_t2625280635 * value)
	{
		___m_fallbackMaterialLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackMaterialLookup_2), value);
	}

	inline static int32_t get_offset_of_m_fallbackCleanupList_3() { return static_cast<int32_t>(offsetof(TMP_MaterialManager_t2944071966_StaticFields, ___m_fallbackCleanupList_3)); }
	inline List_1_t1348486265 * get_m_fallbackCleanupList_3() const { return ___m_fallbackCleanupList_3; }
	inline List_1_t1348486265 ** get_address_of_m_fallbackCleanupList_3() { return &___m_fallbackCleanupList_3; }
	inline void set_m_fallbackCleanupList_3(List_1_t1348486265 * value)
	{
		___m_fallbackCleanupList_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackCleanupList_3), value);
	}

	inline static int32_t get_offset_of_isFallbackListDirty_4() { return static_cast<int32_t>(offsetof(TMP_MaterialManager_t2944071966_StaticFields, ___isFallbackListDirty_4)); }
	inline bool get_isFallbackListDirty_4() const { return ___isFallbackListDirty_4; }
	inline bool* get_address_of_isFallbackListDirty_4() { return &___isFallbackListDirty_4; }
	inline void set_isFallbackListDirty_4(bool value)
	{
		___isFallbackListDirty_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_MATERIALMANAGER_T2944071966_H
#ifndef FASTACTION_T3491443480_H
#define FASTACTION_T3491443480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FastAction
struct  FastAction_t3491443480  : public RuntimeObject
{
public:
	// System.Collections.Generic.LinkedList`1<System.Action> TMPro.FastAction::delegates
	LinkedList_1_t104023486 * ___delegates_0;
	// System.Collections.Generic.Dictionary`2<System.Action,System.Collections.Generic.LinkedListNode`1<System.Action>> TMPro.FastAction::lookup
	Dictionary_2_t4196256347 * ___lookup_1;

public:
	inline static int32_t get_offset_of_delegates_0() { return static_cast<int32_t>(offsetof(FastAction_t3491443480, ___delegates_0)); }
	inline LinkedList_1_t104023486 * get_delegates_0() const { return ___delegates_0; }
	inline LinkedList_1_t104023486 ** get_address_of_delegates_0() { return &___delegates_0; }
	inline void set_delegates_0(LinkedList_1_t104023486 * value)
	{
		___delegates_0 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_0), value);
	}

	inline static int32_t get_offset_of_lookup_1() { return static_cast<int32_t>(offsetof(FastAction_t3491443480, ___lookup_1)); }
	inline Dictionary_2_t4196256347 * get_lookup_1() const { return ___lookup_1; }
	inline Dictionary_2_t4196256347 ** get_address_of_lookup_1() { return &___lookup_1; }
	inline void set_lookup_1(Dictionary_2_t4196256347 * value)
	{
		___lookup_1 = value;
		Il2CppCodeGenWriteBarrier((&___lookup_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FASTACTION_T3491443480_H
#ifndef U3CU3EC__DISPLAYCLASS28_0_T1214464005_H
#define U3CU3EC__DISPLAYCLASS28_0_T1214464005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.InlineGraphicManager/<>c__DisplayClass28_0
struct  U3CU3Ec__DisplayClass28_0_t1214464005  : public RuntimeObject
{
public:
	// System.Int32 TMPro.InlineGraphicManager/<>c__DisplayClass28_0::hashCode
	int32_t ___hashCode_0;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass28_0_t1214464005, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS28_0_T1214464005_H
#ifndef MATERIALREFERENCEMANAGER_T2114976864_H
#define MATERIALREFERENCEMANAGER_T2114976864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaterialReferenceManager
struct  MaterialReferenceManager_t2114976864  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Material> TMPro.MaterialReferenceManager::m_FontMaterialReferenceLookup
	Dictionary_2_t3524055750 * ___m_FontMaterialReferenceLookup_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_FontAsset> TMPro.MaterialReferenceManager::m_FontAssetReferenceLookup
	Dictionary_2_t3548062253 * ___m_FontAssetReferenceLookup_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_SpriteAsset> TMPro.MaterialReferenceManager::m_SpriteAssetReferenceLookup
	Dictionary_2_t3668501260 * ___m_SpriteAssetReferenceLookup_3;

public:
	inline static int32_t get_offset_of_m_FontMaterialReferenceLookup_1() { return static_cast<int32_t>(offsetof(MaterialReferenceManager_t2114976864, ___m_FontMaterialReferenceLookup_1)); }
	inline Dictionary_2_t3524055750 * get_m_FontMaterialReferenceLookup_1() const { return ___m_FontMaterialReferenceLookup_1; }
	inline Dictionary_2_t3524055750 ** get_address_of_m_FontMaterialReferenceLookup_1() { return &___m_FontMaterialReferenceLookup_1; }
	inline void set_m_FontMaterialReferenceLookup_1(Dictionary_2_t3524055750 * value)
	{
		___m_FontMaterialReferenceLookup_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontMaterialReferenceLookup_1), value);
	}

	inline static int32_t get_offset_of_m_FontAssetReferenceLookup_2() { return static_cast<int32_t>(offsetof(MaterialReferenceManager_t2114976864, ___m_FontAssetReferenceLookup_2)); }
	inline Dictionary_2_t3548062253 * get_m_FontAssetReferenceLookup_2() const { return ___m_FontAssetReferenceLookup_2; }
	inline Dictionary_2_t3548062253 ** get_address_of_m_FontAssetReferenceLookup_2() { return &___m_FontAssetReferenceLookup_2; }
	inline void set_m_FontAssetReferenceLookup_2(Dictionary_2_t3548062253 * value)
	{
		___m_FontAssetReferenceLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontAssetReferenceLookup_2), value);
	}

	inline static int32_t get_offset_of_m_SpriteAssetReferenceLookup_3() { return static_cast<int32_t>(offsetof(MaterialReferenceManager_t2114976864, ___m_SpriteAssetReferenceLookup_3)); }
	inline Dictionary_2_t3668501260 * get_m_SpriteAssetReferenceLookup_3() const { return ___m_SpriteAssetReferenceLookup_3; }
	inline Dictionary_2_t3668501260 ** get_address_of_m_SpriteAssetReferenceLookup_3() { return &___m_SpriteAssetReferenceLookup_3; }
	inline void set_m_SpriteAssetReferenceLookup_3(Dictionary_2_t3668501260 * value)
	{
		___m_SpriteAssetReferenceLookup_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpriteAssetReferenceLookup_3), value);
	}
};

struct MaterialReferenceManager_t2114976864_StaticFields
{
public:
	// TMPro.MaterialReferenceManager TMPro.MaterialReferenceManager::s_Instance
	MaterialReferenceManager_t2114976864 * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(MaterialReferenceManager_t2114976864_StaticFields, ___s_Instance_0)); }
	inline MaterialReferenceManager_t2114976864 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline MaterialReferenceManager_t2114976864 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(MaterialReferenceManager_t2114976864 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALREFERENCEMANAGER_T2114976864_H
#ifndef U3CU3EC__DISPLAYCLASS29_0_T1214529541_H
#define U3CU3EC__DISPLAYCLASS29_0_T1214529541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.InlineGraphicManager/<>c__DisplayClass29_0
struct  U3CU3Ec__DisplayClass29_0_t1214529541  : public RuntimeObject
{
public:
	// System.Int32 TMPro.InlineGraphicManager/<>c__DisplayClass29_0::index
	int32_t ___index_0;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_t1214529541, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS29_0_T1214529541_H
#ifndef BASEVERTEXEFFECT_T2675891272_H
#define BASEVERTEXEFFECT_T2675891272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseVertexEffect
struct  BaseVertexEffect_t2675891272  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVERTEXEFFECT_T2675891272_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef UNITYEVENT_1_T2729110193_H
#define UNITYEVENT_1_T2729110193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.String>
struct  UnityEvent_1_t2729110193  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2729110193, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2729110193_H
#ifndef U24ARRAYTYPEU3D12_T2488454196_H
#define U24ARRAYTYPEU3D12_T2488454196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t2488454196 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t2488454196__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T2488454196_H
#ifndef FONTCREATIONSETTING_T628772060_H
#define FONTCREATIONSETTING_T628772060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontCreationSetting
struct  FontCreationSetting_t628772060 
{
public:
	// System.String TMPro.FontCreationSetting::fontSourcePath
	String_t* ___fontSourcePath_0;
	// System.Int32 TMPro.FontCreationSetting::fontSizingMode
	int32_t ___fontSizingMode_1;
	// System.Int32 TMPro.FontCreationSetting::fontSize
	int32_t ___fontSize_2;
	// System.Int32 TMPro.FontCreationSetting::fontPadding
	int32_t ___fontPadding_3;
	// System.Int32 TMPro.FontCreationSetting::fontPackingMode
	int32_t ___fontPackingMode_4;
	// System.Int32 TMPro.FontCreationSetting::fontAtlasWidth
	int32_t ___fontAtlasWidth_5;
	// System.Int32 TMPro.FontCreationSetting::fontAtlasHeight
	int32_t ___fontAtlasHeight_6;
	// System.Int32 TMPro.FontCreationSetting::fontCharacterSet
	int32_t ___fontCharacterSet_7;
	// System.Int32 TMPro.FontCreationSetting::fontStyle
	int32_t ___fontStyle_8;
	// System.Single TMPro.FontCreationSetting::fontStlyeModifier
	float ___fontStlyeModifier_9;
	// System.Int32 TMPro.FontCreationSetting::fontRenderMode
	int32_t ___fontRenderMode_10;
	// System.Boolean TMPro.FontCreationSetting::fontKerning
	bool ___fontKerning_11;

public:
	inline static int32_t get_offset_of_fontSourcePath_0() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontSourcePath_0)); }
	inline String_t* get_fontSourcePath_0() const { return ___fontSourcePath_0; }
	inline String_t** get_address_of_fontSourcePath_0() { return &___fontSourcePath_0; }
	inline void set_fontSourcePath_0(String_t* value)
	{
		___fontSourcePath_0 = value;
		Il2CppCodeGenWriteBarrier((&___fontSourcePath_0), value);
	}

	inline static int32_t get_offset_of_fontSizingMode_1() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontSizingMode_1)); }
	inline int32_t get_fontSizingMode_1() const { return ___fontSizingMode_1; }
	inline int32_t* get_address_of_fontSizingMode_1() { return &___fontSizingMode_1; }
	inline void set_fontSizingMode_1(int32_t value)
	{
		___fontSizingMode_1 = value;
	}

	inline static int32_t get_offset_of_fontSize_2() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontSize_2)); }
	inline int32_t get_fontSize_2() const { return ___fontSize_2; }
	inline int32_t* get_address_of_fontSize_2() { return &___fontSize_2; }
	inline void set_fontSize_2(int32_t value)
	{
		___fontSize_2 = value;
	}

	inline static int32_t get_offset_of_fontPadding_3() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontPadding_3)); }
	inline int32_t get_fontPadding_3() const { return ___fontPadding_3; }
	inline int32_t* get_address_of_fontPadding_3() { return &___fontPadding_3; }
	inline void set_fontPadding_3(int32_t value)
	{
		___fontPadding_3 = value;
	}

	inline static int32_t get_offset_of_fontPackingMode_4() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontPackingMode_4)); }
	inline int32_t get_fontPackingMode_4() const { return ___fontPackingMode_4; }
	inline int32_t* get_address_of_fontPackingMode_4() { return &___fontPackingMode_4; }
	inline void set_fontPackingMode_4(int32_t value)
	{
		___fontPackingMode_4 = value;
	}

	inline static int32_t get_offset_of_fontAtlasWidth_5() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontAtlasWidth_5)); }
	inline int32_t get_fontAtlasWidth_5() const { return ___fontAtlasWidth_5; }
	inline int32_t* get_address_of_fontAtlasWidth_5() { return &___fontAtlasWidth_5; }
	inline void set_fontAtlasWidth_5(int32_t value)
	{
		___fontAtlasWidth_5 = value;
	}

	inline static int32_t get_offset_of_fontAtlasHeight_6() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontAtlasHeight_6)); }
	inline int32_t get_fontAtlasHeight_6() const { return ___fontAtlasHeight_6; }
	inline int32_t* get_address_of_fontAtlasHeight_6() { return &___fontAtlasHeight_6; }
	inline void set_fontAtlasHeight_6(int32_t value)
	{
		___fontAtlasHeight_6 = value;
	}

	inline static int32_t get_offset_of_fontCharacterSet_7() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontCharacterSet_7)); }
	inline int32_t get_fontCharacterSet_7() const { return ___fontCharacterSet_7; }
	inline int32_t* get_address_of_fontCharacterSet_7() { return &___fontCharacterSet_7; }
	inline void set_fontCharacterSet_7(int32_t value)
	{
		___fontCharacterSet_7 = value;
	}

	inline static int32_t get_offset_of_fontStyle_8() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontStyle_8)); }
	inline int32_t get_fontStyle_8() const { return ___fontStyle_8; }
	inline int32_t* get_address_of_fontStyle_8() { return &___fontStyle_8; }
	inline void set_fontStyle_8(int32_t value)
	{
		___fontStyle_8 = value;
	}

	inline static int32_t get_offset_of_fontStlyeModifier_9() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontStlyeModifier_9)); }
	inline float get_fontStlyeModifier_9() const { return ___fontStlyeModifier_9; }
	inline float* get_address_of_fontStlyeModifier_9() { return &___fontStlyeModifier_9; }
	inline void set_fontStlyeModifier_9(float value)
	{
		___fontStlyeModifier_9 = value;
	}

	inline static int32_t get_offset_of_fontRenderMode_10() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontRenderMode_10)); }
	inline int32_t get_fontRenderMode_10() const { return ___fontRenderMode_10; }
	inline int32_t* get_address_of_fontRenderMode_10() { return &___fontRenderMode_10; }
	inline void set_fontRenderMode_10(int32_t value)
	{
		___fontRenderMode_10 = value;
	}

	inline static int32_t get_offset_of_fontKerning_11() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontKerning_11)); }
	inline bool get_fontKerning_11() const { return ___fontKerning_11; }
	inline bool* get_address_of_fontKerning_11() { return &___fontKerning_11; }
	inline void set_fontKerning_11(bool value)
	{
		___fontKerning_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.FontCreationSetting
struct FontCreationSetting_t628772060_marshaled_pinvoke
{
	char* ___fontSourcePath_0;
	int32_t ___fontSizingMode_1;
	int32_t ___fontSize_2;
	int32_t ___fontPadding_3;
	int32_t ___fontPackingMode_4;
	int32_t ___fontAtlasWidth_5;
	int32_t ___fontAtlasHeight_6;
	int32_t ___fontCharacterSet_7;
	int32_t ___fontStyle_8;
	float ___fontStlyeModifier_9;
	int32_t ___fontRenderMode_10;
	int32_t ___fontKerning_11;
};
// Native definition for COM marshalling of TMPro.FontCreationSetting
struct FontCreationSetting_t628772060_marshaled_com
{
	Il2CppChar* ___fontSourcePath_0;
	int32_t ___fontSizingMode_1;
	int32_t ___fontSize_2;
	int32_t ___fontPadding_3;
	int32_t ___fontPackingMode_4;
	int32_t ___fontAtlasWidth_5;
	int32_t ___fontAtlasHeight_6;
	int32_t ___fontCharacterSet_7;
	int32_t ___fontStyle_8;
	float ___fontStlyeModifier_9;
	int32_t ___fontRenderMode_10;
	int32_t ___fontKerning_11;
};
#endif // FONTCREATIONSETTING_T628772060_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef CHAR_T3634460470_H
#define CHAR_T3634460470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3634460470 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t3634460470, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t3634460470_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3634460470_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef SPRITESTATE_T1362986479_H
#define SPRITESTATE_T1362986479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1362986479 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t280657092 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t280657092 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_HighlightedSprite_0)); }
	inline Sprite_t280657092 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t280657092 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t280657092 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_PressedSprite_1)); }
	inline Sprite_t280657092 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t280657092 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t280657092 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_DisabledSprite_2)); }
	inline Sprite_t280657092 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t280657092 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t280657092 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_pinvoke
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_com
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1362986479_H
#ifndef UNITYEVENT_3_T1597070127_H
#define UNITYEVENT_3_T1597070127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.String,System.Int32,System.Int32>
struct  UnityEvent_3_t1597070127  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t1597070127, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T1597070127_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef COLOR32_T2600501292_H
#define COLOR32_T2600501292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t2600501292 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2600501292_H
#ifndef UNITYEVENT_1_T3832605257_H
#define UNITYEVENT_1_T3832605257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Int32>
struct  UnityEvent_1_t3832605257  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3832605257, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3832605257_H
#ifndef MATERIALREFERENCE_T1952344632_H
#define MATERIALREFERENCE_T1952344632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaterialReference
struct  MaterialReference_t1952344632 
{
public:
	// System.Int32 TMPro.MaterialReference::index
	int32_t ___index_0;
	// TMPro.TMP_FontAsset TMPro.MaterialReference::fontAsset
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	// TMPro.TMP_SpriteAsset TMPro.MaterialReference::spriteAsset
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	// UnityEngine.Material TMPro.MaterialReference::material
	Material_t340375123 * ___material_3;
	// System.Boolean TMPro.MaterialReference::isDefaultMaterial
	bool ___isDefaultMaterial_4;
	// System.Boolean TMPro.MaterialReference::isFallbackMaterial
	bool ___isFallbackMaterial_5;
	// UnityEngine.Material TMPro.MaterialReference::fallbackMaterial
	Material_t340375123 * ___fallbackMaterial_6;
	// System.Single TMPro.MaterialReference::padding
	float ___padding_7;
	// System.Int32 TMPro.MaterialReference::referenceCount
	int32_t ___referenceCount_8;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_fontAsset_1() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___fontAsset_1)); }
	inline TMP_FontAsset_t364381626 * get_fontAsset_1() const { return ___fontAsset_1; }
	inline TMP_FontAsset_t364381626 ** get_address_of_fontAsset_1() { return &___fontAsset_1; }
	inline void set_fontAsset_1(TMP_FontAsset_t364381626 * value)
	{
		___fontAsset_1 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_1), value);
	}

	inline static int32_t get_offset_of_spriteAsset_2() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___spriteAsset_2)); }
	inline TMP_SpriteAsset_t484820633 * get_spriteAsset_2() const { return ___spriteAsset_2; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_spriteAsset_2() { return &___spriteAsset_2; }
	inline void set_spriteAsset_2(TMP_SpriteAsset_t484820633 * value)
	{
		___spriteAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_2), value);
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___material_3)); }
	inline Material_t340375123 * get_material_3() const { return ___material_3; }
	inline Material_t340375123 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_t340375123 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_isDefaultMaterial_4() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___isDefaultMaterial_4)); }
	inline bool get_isDefaultMaterial_4() const { return ___isDefaultMaterial_4; }
	inline bool* get_address_of_isDefaultMaterial_4() { return &___isDefaultMaterial_4; }
	inline void set_isDefaultMaterial_4(bool value)
	{
		___isDefaultMaterial_4 = value;
	}

	inline static int32_t get_offset_of_isFallbackMaterial_5() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___isFallbackMaterial_5)); }
	inline bool get_isFallbackMaterial_5() const { return ___isFallbackMaterial_5; }
	inline bool* get_address_of_isFallbackMaterial_5() { return &___isFallbackMaterial_5; }
	inline void set_isFallbackMaterial_5(bool value)
	{
		___isFallbackMaterial_5 = value;
	}

	inline static int32_t get_offset_of_fallbackMaterial_6() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___fallbackMaterial_6)); }
	inline Material_t340375123 * get_fallbackMaterial_6() const { return ___fallbackMaterial_6; }
	inline Material_t340375123 ** get_address_of_fallbackMaterial_6() { return &___fallbackMaterial_6; }
	inline void set_fallbackMaterial_6(Material_t340375123 * value)
	{
		___fallbackMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackMaterial_6), value);
	}

	inline static int32_t get_offset_of_padding_7() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___padding_7)); }
	inline float get_padding_7() const { return ___padding_7; }
	inline float* get_address_of_padding_7() { return &___padding_7; }
	inline void set_padding_7(float value)
	{
		___padding_7 = value;
	}

	inline static int32_t get_offset_of_referenceCount_8() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___referenceCount_8)); }
	inline int32_t get_referenceCount_8() const { return ___referenceCount_8; }
	inline int32_t* get_address_of_referenceCount_8() { return &___referenceCount_8; }
	inline void set_referenceCount_8(int32_t value)
	{
		___referenceCount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.MaterialReference
struct MaterialReference_t1952344632_marshaled_pinvoke
{
	int32_t ___index_0;
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	Material_t340375123 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t340375123 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
// Native definition for COM marshalling of TMPro.MaterialReference
struct MaterialReference_t1952344632_marshaled_com
{
	int32_t ___index_0;
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	Material_t340375123 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t340375123 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
#endif // MATERIALREFERENCE_T1952344632_H
#ifndef TMP_FONTWEIGHTS_T916301067_H
#define TMP_FONTWEIGHTS_T916301067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontWeights
struct  TMP_FontWeights_t916301067 
{
public:
	// TMPro.TMP_FontAsset TMPro.TMP_FontWeights::regularTypeface
	TMP_FontAsset_t364381626 * ___regularTypeface_0;
	// TMPro.TMP_FontAsset TMPro.TMP_FontWeights::italicTypeface
	TMP_FontAsset_t364381626 * ___italicTypeface_1;

public:
	inline static int32_t get_offset_of_regularTypeface_0() { return static_cast<int32_t>(offsetof(TMP_FontWeights_t916301067, ___regularTypeface_0)); }
	inline TMP_FontAsset_t364381626 * get_regularTypeface_0() const { return ___regularTypeface_0; }
	inline TMP_FontAsset_t364381626 ** get_address_of_regularTypeface_0() { return &___regularTypeface_0; }
	inline void set_regularTypeface_0(TMP_FontAsset_t364381626 * value)
	{
		___regularTypeface_0 = value;
		Il2CppCodeGenWriteBarrier((&___regularTypeface_0), value);
	}

	inline static int32_t get_offset_of_italicTypeface_1() { return static_cast<int32_t>(offsetof(TMP_FontWeights_t916301067, ___italicTypeface_1)); }
	inline TMP_FontAsset_t364381626 * get_italicTypeface_1() const { return ___italicTypeface_1; }
	inline TMP_FontAsset_t364381626 ** get_address_of_italicTypeface_1() { return &___italicTypeface_1; }
	inline void set_italicTypeface_1(TMP_FontAsset_t364381626 * value)
	{
		___italicTypeface_1 = value;
		Il2CppCodeGenWriteBarrier((&___italicTypeface_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_FontWeights
struct TMP_FontWeights_t916301067_marshaled_pinvoke
{
	TMP_FontAsset_t364381626 * ___regularTypeface_0;
	TMP_FontAsset_t364381626 * ___italicTypeface_1;
};
// Native definition for COM marshalling of TMPro.TMP_FontWeights
struct TMP_FontWeights_t916301067_marshaled_com
{
	TMP_FontAsset_t364381626 * ___regularTypeface_0;
	TMP_FontAsset_t364381626 * ___italicTypeface_1;
};
#endif // TMP_FONTWEIGHTS_T916301067_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef RESOURCES_T2155109485_H
#define RESOURCES_T2155109485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_DefaultControls/Resources
struct  Resources_t2155109485 
{
public:
	// UnityEngine.Sprite TMPro.TMP_DefaultControls/Resources::standard
	Sprite_t280657092 * ___standard_0;
	// UnityEngine.Sprite TMPro.TMP_DefaultControls/Resources::background
	Sprite_t280657092 * ___background_1;
	// UnityEngine.Sprite TMPro.TMP_DefaultControls/Resources::inputField
	Sprite_t280657092 * ___inputField_2;
	// UnityEngine.Sprite TMPro.TMP_DefaultControls/Resources::knob
	Sprite_t280657092 * ___knob_3;
	// UnityEngine.Sprite TMPro.TMP_DefaultControls/Resources::checkmark
	Sprite_t280657092 * ___checkmark_4;
	// UnityEngine.Sprite TMPro.TMP_DefaultControls/Resources::dropdown
	Sprite_t280657092 * ___dropdown_5;
	// UnityEngine.Sprite TMPro.TMP_DefaultControls/Resources::mask
	Sprite_t280657092 * ___mask_6;

public:
	inline static int32_t get_offset_of_standard_0() { return static_cast<int32_t>(offsetof(Resources_t2155109485, ___standard_0)); }
	inline Sprite_t280657092 * get_standard_0() const { return ___standard_0; }
	inline Sprite_t280657092 ** get_address_of_standard_0() { return &___standard_0; }
	inline void set_standard_0(Sprite_t280657092 * value)
	{
		___standard_0 = value;
		Il2CppCodeGenWriteBarrier((&___standard_0), value);
	}

	inline static int32_t get_offset_of_background_1() { return static_cast<int32_t>(offsetof(Resources_t2155109485, ___background_1)); }
	inline Sprite_t280657092 * get_background_1() const { return ___background_1; }
	inline Sprite_t280657092 ** get_address_of_background_1() { return &___background_1; }
	inline void set_background_1(Sprite_t280657092 * value)
	{
		___background_1 = value;
		Il2CppCodeGenWriteBarrier((&___background_1), value);
	}

	inline static int32_t get_offset_of_inputField_2() { return static_cast<int32_t>(offsetof(Resources_t2155109485, ___inputField_2)); }
	inline Sprite_t280657092 * get_inputField_2() const { return ___inputField_2; }
	inline Sprite_t280657092 ** get_address_of_inputField_2() { return &___inputField_2; }
	inline void set_inputField_2(Sprite_t280657092 * value)
	{
		___inputField_2 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_2), value);
	}

	inline static int32_t get_offset_of_knob_3() { return static_cast<int32_t>(offsetof(Resources_t2155109485, ___knob_3)); }
	inline Sprite_t280657092 * get_knob_3() const { return ___knob_3; }
	inline Sprite_t280657092 ** get_address_of_knob_3() { return &___knob_3; }
	inline void set_knob_3(Sprite_t280657092 * value)
	{
		___knob_3 = value;
		Il2CppCodeGenWriteBarrier((&___knob_3), value);
	}

	inline static int32_t get_offset_of_checkmark_4() { return static_cast<int32_t>(offsetof(Resources_t2155109485, ___checkmark_4)); }
	inline Sprite_t280657092 * get_checkmark_4() const { return ___checkmark_4; }
	inline Sprite_t280657092 ** get_address_of_checkmark_4() { return &___checkmark_4; }
	inline void set_checkmark_4(Sprite_t280657092 * value)
	{
		___checkmark_4 = value;
		Il2CppCodeGenWriteBarrier((&___checkmark_4), value);
	}

	inline static int32_t get_offset_of_dropdown_5() { return static_cast<int32_t>(offsetof(Resources_t2155109485, ___dropdown_5)); }
	inline Sprite_t280657092 * get_dropdown_5() const { return ___dropdown_5; }
	inline Sprite_t280657092 ** get_address_of_dropdown_5() { return &___dropdown_5; }
	inline void set_dropdown_5(Sprite_t280657092 * value)
	{
		___dropdown_5 = value;
		Il2CppCodeGenWriteBarrier((&___dropdown_5), value);
	}

	inline static int32_t get_offset_of_mask_6() { return static_cast<int32_t>(offsetof(Resources_t2155109485, ___mask_6)); }
	inline Sprite_t280657092 * get_mask_6() const { return ___mask_6; }
	inline Sprite_t280657092 ** get_address_of_mask_6() { return &___mask_6; }
	inline void set_mask_6(Sprite_t280657092 * value)
	{
		___mask_6 = value;
		Il2CppCodeGenWriteBarrier((&___mask_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_DefaultControls/Resources
struct Resources_t2155109485_marshaled_pinvoke
{
	Sprite_t280657092 * ___standard_0;
	Sprite_t280657092 * ___background_1;
	Sprite_t280657092 * ___inputField_2;
	Sprite_t280657092 * ___knob_3;
	Sprite_t280657092 * ___checkmark_4;
	Sprite_t280657092 * ___dropdown_5;
	Sprite_t280657092 * ___mask_6;
};
// Native definition for COM marshalling of TMPro.TMP_DefaultControls/Resources
struct Resources_t2155109485_marshaled_com
{
	Sprite_t280657092 * ___standard_0;
	Sprite_t280657092 * ___background_1;
	Sprite_t280657092 * ___inputField_2;
	Sprite_t280657092 * ___knob_3;
	Sprite_t280657092 * ___checkmark_4;
	Sprite_t280657092 * ___dropdown_5;
	Sprite_t280657092 * ___mask_6;
};
#endif // RESOURCES_T2155109485_H
#ifndef FLOATTWEEN_T3783157226_H
#define FLOATTWEEN_T3783157226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FloatTween
struct  FloatTween_t3783157226 
{
public:
	// TMPro.FloatTween/FloatTweenCallback TMPro.FloatTween::m_Target
	FloatTweenCallback_t556094317 * ___m_Target_0;
	// System.Single TMPro.FloatTween::m_StartValue
	float ___m_StartValue_1;
	// System.Single TMPro.FloatTween::m_TargetValue
	float ___m_TargetValue_2;
	// System.Single TMPro.FloatTween::m_Duration
	float ___m_Duration_3;
	// System.Boolean TMPro.FloatTween::m_IgnoreTimeScale
	bool ___m_IgnoreTimeScale_4;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(FloatTween_t3783157226, ___m_Target_0)); }
	inline FloatTweenCallback_t556094317 * get_m_Target_0() const { return ___m_Target_0; }
	inline FloatTweenCallback_t556094317 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(FloatTweenCallback_t556094317 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_StartValue_1() { return static_cast<int32_t>(offsetof(FloatTween_t3783157226, ___m_StartValue_1)); }
	inline float get_m_StartValue_1() const { return ___m_StartValue_1; }
	inline float* get_address_of_m_StartValue_1() { return &___m_StartValue_1; }
	inline void set_m_StartValue_1(float value)
	{
		___m_StartValue_1 = value;
	}

	inline static int32_t get_offset_of_m_TargetValue_2() { return static_cast<int32_t>(offsetof(FloatTween_t3783157226, ___m_TargetValue_2)); }
	inline float get_m_TargetValue_2() const { return ___m_TargetValue_2; }
	inline float* get_address_of_m_TargetValue_2() { return &___m_TargetValue_2; }
	inline void set_m_TargetValue_2(float value)
	{
		___m_TargetValue_2 = value;
	}

	inline static int32_t get_offset_of_m_Duration_3() { return static_cast<int32_t>(offsetof(FloatTween_t3783157226, ___m_Duration_3)); }
	inline float get_m_Duration_3() const { return ___m_Duration_3; }
	inline float* get_address_of_m_Duration_3() { return &___m_Duration_3; }
	inline void set_m_Duration_3(float value)
	{
		___m_Duration_3 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreTimeScale_4() { return static_cast<int32_t>(offsetof(FloatTween_t3783157226, ___m_IgnoreTimeScale_4)); }
	inline bool get_m_IgnoreTimeScale_4() const { return ___m_IgnoreTimeScale_4; }
	inline bool* get_address_of_m_IgnoreTimeScale_4() { return &___m_IgnoreTimeScale_4; }
	inline void set_m_IgnoreTimeScale_4(bool value)
	{
		___m_IgnoreTimeScale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.FloatTween
struct FloatTween_t3783157226_marshaled_pinvoke
{
	FloatTweenCallback_t556094317 * ___m_Target_0;
	float ___m_StartValue_1;
	float ___m_TargetValue_2;
	float ___m_Duration_3;
	int32_t ___m_IgnoreTimeScale_4;
};
// Native definition for COM marshalling of TMPro.FloatTween
struct FloatTween_t3783157226_marshaled_com
{
	FloatTweenCallback_t556094317 * ___m_Target_0;
	float ___m_StartValue_1;
	float ___m_TargetValue_2;
	float ___m_Duration_3;
	int32_t ___m_IgnoreTimeScale_4;
};
#endif // FLOATTWEEN_T3783157226_H
#ifndef UNITYEVENT_1_T3437345828_H
#define UNITYEVENT_1_T3437345828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct  UnityEvent_1_t3437345828  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3437345828, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3437345828_H
#ifndef UNITYEVENT_1_T2278926278_H
#define UNITYEVENT_1_T2278926278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Single>
struct  UnityEvent_1_t2278926278  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2278926278, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2278926278_H
#ifndef TEXTUREMAPPINGOPTIONS_T270963663_H
#define TEXTUREMAPPINGOPTIONS_T270963663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextureMappingOptions
struct  TextureMappingOptions_t270963663 
{
public:
	// System.Int32 TMPro.TextureMappingOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureMappingOptions_t270963663, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREMAPPINGOPTIONS_T270963663_H
#ifndef TEXTOVERFLOWMODES_T1430035314_H
#define TEXTOVERFLOWMODES_T1430035314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextOverflowModes
struct  TextOverflowModes_t1430035314 
{
public:
	// System.Int32 TMPro.TextOverflowModes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextOverflowModes_t1430035314, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTOVERFLOWMODES_T1430035314_H
#ifndef EXTENTS_T3837212874_H
#define EXTENTS_T3837212874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Extents
struct  Extents_t3837212874 
{
public:
	// UnityEngine.Vector2 TMPro.Extents::min
	Vector2_t2156229523  ___min_0;
	// UnityEngine.Vector2 TMPro.Extents::max
	Vector2_t2156229523  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Extents_t3837212874, ___min_0)); }
	inline Vector2_t2156229523  get_min_0() const { return ___min_0; }
	inline Vector2_t2156229523 * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_t2156229523  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Extents_t3837212874, ___max_1)); }
	inline Vector2_t2156229523  get_max_1() const { return ___max_1; }
	inline Vector2_t2156229523 * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_t2156229523  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENTS_T3837212874_H
#ifndef FONTSTYLES_T3828945032_H
#define FONTSTYLES_T3828945032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontStyles
struct  FontStyles_t3828945032 
{
public:
	// System.Int32 TMPro.FontStyles::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FontStyles_t3828945032, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLES_T3828945032_H
#ifndef MASKINGOFFSETMODE_T2266644590_H
#define MASKINGOFFSETMODE_T2266644590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaskingOffsetMode
struct  MaskingOffsetMode_t2266644590 
{
public:
	// System.Int32 TMPro.MaskingOffsetMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MaskingOffsetMode_t2266644590, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKINGOFFSETMODE_T2266644590_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef MASKINGTYPES_T3687969768_H
#define MASKINGTYPES_T3687969768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaskingTypes
struct  MaskingTypes_t3687969768 
{
public:
	// System.Int32 TMPro.MaskingTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MaskingTypes_t3687969768, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKINGTYPES_T3687969768_H
#ifndef _HORIZONTALALIGNMENTOPTIONS_T2379014378_H
#define _HORIZONTALALIGNMENTOPTIONS_T2379014378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro._HorizontalAlignmentOptions
struct  _HorizontalAlignmentOptions_t2379014378 
{
public:
	// System.Int32 TMPro._HorizontalAlignmentOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(_HorizontalAlignmentOptions_t2379014378, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // _HORIZONTALALIGNMENTOPTIONS_T2379014378_H
#ifndef TOUCHSCREENKEYBOARDTYPE_T1530597702_H
#define TOUCHSCREENKEYBOARDTYPE_T1530597702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchScreenKeyboardType
struct  TouchScreenKeyboardType_t1530597702 
{
public:
	// System.Int32 UnityEngine.TouchScreenKeyboardType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchScreenKeyboardType_t1530597702, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSCREENKEYBOARDTYPE_T1530597702_H
#ifndef TEXTALIGNMENTOPTIONS_T4036791236_H
#define TEXTALIGNMENTOPTIONS_T4036791236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextAlignmentOptions
struct  TextAlignmentOptions_t4036791236 
{
public:
	// System.Int32 TMPro.TextAlignmentOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAlignmentOptions_t4036791236, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTALIGNMENTOPTIONS_T4036791236_H
#ifndef TMP_SPRITE_T554067146_H
#define TMP_SPRITE_T554067146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Sprite
struct  TMP_Sprite_t554067146  : public TMP_TextElement_t129727469
{
public:
	// System.String TMPro.TMP_Sprite::name
	String_t* ___name_9;
	// System.Int32 TMPro.TMP_Sprite::hashCode
	int32_t ___hashCode_10;
	// System.Int32 TMPro.TMP_Sprite::unicode
	int32_t ___unicode_11;
	// UnityEngine.Vector2 TMPro.TMP_Sprite::pivot
	Vector2_t2156229523  ___pivot_12;
	// UnityEngine.Sprite TMPro.TMP_Sprite::sprite
	Sprite_t280657092 * ___sprite_13;

public:
	inline static int32_t get_offset_of_name_9() { return static_cast<int32_t>(offsetof(TMP_Sprite_t554067146, ___name_9)); }
	inline String_t* get_name_9() const { return ___name_9; }
	inline String_t** get_address_of_name_9() { return &___name_9; }
	inline void set_name_9(String_t* value)
	{
		___name_9 = value;
		Il2CppCodeGenWriteBarrier((&___name_9), value);
	}

	inline static int32_t get_offset_of_hashCode_10() { return static_cast<int32_t>(offsetof(TMP_Sprite_t554067146, ___hashCode_10)); }
	inline int32_t get_hashCode_10() const { return ___hashCode_10; }
	inline int32_t* get_address_of_hashCode_10() { return &___hashCode_10; }
	inline void set_hashCode_10(int32_t value)
	{
		___hashCode_10 = value;
	}

	inline static int32_t get_offset_of_unicode_11() { return static_cast<int32_t>(offsetof(TMP_Sprite_t554067146, ___unicode_11)); }
	inline int32_t get_unicode_11() const { return ___unicode_11; }
	inline int32_t* get_address_of_unicode_11() { return &___unicode_11; }
	inline void set_unicode_11(int32_t value)
	{
		___unicode_11 = value;
	}

	inline static int32_t get_offset_of_pivot_12() { return static_cast<int32_t>(offsetof(TMP_Sprite_t554067146, ___pivot_12)); }
	inline Vector2_t2156229523  get_pivot_12() const { return ___pivot_12; }
	inline Vector2_t2156229523 * get_address_of_pivot_12() { return &___pivot_12; }
	inline void set_pivot_12(Vector2_t2156229523  value)
	{
		___pivot_12 = value;
	}

	inline static int32_t get_offset_of_sprite_13() { return static_cast<int32_t>(offsetof(TMP_Sprite_t554067146, ___sprite_13)); }
	inline Sprite_t280657092 * get_sprite_13() const { return ___sprite_13; }
	inline Sprite_t280657092 ** get_address_of_sprite_13() { return &___sprite_13; }
	inline void set_sprite_13(Sprite_t280657092 * value)
	{
		___sprite_13 = value;
		Il2CppCodeGenWriteBarrier((&___sprite_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SPRITE_T554067146_H
#ifndef TMP_TEXTELEMENTTYPE_T1276645592_H
#define TMP_TEXTELEMENTTYPE_T1276645592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElementType
struct  TMP_TextElementType_t1276645592 
{
public:
	// System.Int32 TMPro.TMP_TextElementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TMP_TextElementType_t1276645592, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENTTYPE_T1276645592_H
#ifndef TEXTRENDERFLAGS_T2418684345_H
#define TEXTRENDERFLAGS_T2418684345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextRenderFlags
struct  TextRenderFlags_t2418684345 
{
public:
	// System.Int32 TMPro.TextRenderFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextRenderFlags_t2418684345, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTRENDERFLAGS_T2418684345_H
#ifndef _VERTICALALIGNMENTOPTIONS_T2825528260_H
#define _VERTICALALIGNMENTOPTIONS_T2825528260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro._VerticalAlignmentOptions
struct  _VerticalAlignmentOptions_t2825528260 
{
public:
	// System.Int32 TMPro._VerticalAlignmentOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(_VerticalAlignmentOptions_t2825528260, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // _VERTICALALIGNMENTOPTIONS_T2825528260_H
#ifndef TMP_MESHINFO_T2771747634_H
#define TMP_MESHINFO_T2771747634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MeshInfo
struct  TMP_MeshInfo_t2771747634 
{
public:
	// UnityEngine.Mesh TMPro.TMP_MeshInfo::mesh
	Mesh_t3648964284 * ___mesh_3;
	// System.Int32 TMPro.TMP_MeshInfo::vertexCount
	int32_t ___vertexCount_4;
	// UnityEngine.Vector3[] TMPro.TMP_MeshInfo::vertices
	Vector3U5BU5D_t1718750761* ___vertices_5;
	// UnityEngine.Vector3[] TMPro.TMP_MeshInfo::normals
	Vector3U5BU5D_t1718750761* ___normals_6;
	// UnityEngine.Vector4[] TMPro.TMP_MeshInfo::tangents
	Vector4U5BU5D_t934056436* ___tangents_7;
	// UnityEngine.Vector2[] TMPro.TMP_MeshInfo::uvs0
	Vector2U5BU5D_t1457185986* ___uvs0_8;
	// UnityEngine.Vector2[] TMPro.TMP_MeshInfo::uvs2
	Vector2U5BU5D_t1457185986* ___uvs2_9;
	// UnityEngine.Color32[] TMPro.TMP_MeshInfo::colors32
	Color32U5BU5D_t3850468773* ___colors32_10;
	// System.Int32[] TMPro.TMP_MeshInfo::triangles
	Int32U5BU5D_t385246372* ___triangles_11;

public:
	inline static int32_t get_offset_of_mesh_3() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t2771747634, ___mesh_3)); }
	inline Mesh_t3648964284 * get_mesh_3() const { return ___mesh_3; }
	inline Mesh_t3648964284 ** get_address_of_mesh_3() { return &___mesh_3; }
	inline void set_mesh_3(Mesh_t3648964284 * value)
	{
		___mesh_3 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_3), value);
	}

	inline static int32_t get_offset_of_vertexCount_4() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t2771747634, ___vertexCount_4)); }
	inline int32_t get_vertexCount_4() const { return ___vertexCount_4; }
	inline int32_t* get_address_of_vertexCount_4() { return &___vertexCount_4; }
	inline void set_vertexCount_4(int32_t value)
	{
		___vertexCount_4 = value;
	}

	inline static int32_t get_offset_of_vertices_5() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t2771747634, ___vertices_5)); }
	inline Vector3U5BU5D_t1718750761* get_vertices_5() const { return ___vertices_5; }
	inline Vector3U5BU5D_t1718750761** get_address_of_vertices_5() { return &___vertices_5; }
	inline void set_vertices_5(Vector3U5BU5D_t1718750761* value)
	{
		___vertices_5 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_5), value);
	}

	inline static int32_t get_offset_of_normals_6() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t2771747634, ___normals_6)); }
	inline Vector3U5BU5D_t1718750761* get_normals_6() const { return ___normals_6; }
	inline Vector3U5BU5D_t1718750761** get_address_of_normals_6() { return &___normals_6; }
	inline void set_normals_6(Vector3U5BU5D_t1718750761* value)
	{
		___normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___normals_6), value);
	}

	inline static int32_t get_offset_of_tangents_7() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t2771747634, ___tangents_7)); }
	inline Vector4U5BU5D_t934056436* get_tangents_7() const { return ___tangents_7; }
	inline Vector4U5BU5D_t934056436** get_address_of_tangents_7() { return &___tangents_7; }
	inline void set_tangents_7(Vector4U5BU5D_t934056436* value)
	{
		___tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___tangents_7), value);
	}

	inline static int32_t get_offset_of_uvs0_8() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t2771747634, ___uvs0_8)); }
	inline Vector2U5BU5D_t1457185986* get_uvs0_8() const { return ___uvs0_8; }
	inline Vector2U5BU5D_t1457185986** get_address_of_uvs0_8() { return &___uvs0_8; }
	inline void set_uvs0_8(Vector2U5BU5D_t1457185986* value)
	{
		___uvs0_8 = value;
		Il2CppCodeGenWriteBarrier((&___uvs0_8), value);
	}

	inline static int32_t get_offset_of_uvs2_9() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t2771747634, ___uvs2_9)); }
	inline Vector2U5BU5D_t1457185986* get_uvs2_9() const { return ___uvs2_9; }
	inline Vector2U5BU5D_t1457185986** get_address_of_uvs2_9() { return &___uvs2_9; }
	inline void set_uvs2_9(Vector2U5BU5D_t1457185986* value)
	{
		___uvs2_9 = value;
		Il2CppCodeGenWriteBarrier((&___uvs2_9), value);
	}

	inline static int32_t get_offset_of_colors32_10() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t2771747634, ___colors32_10)); }
	inline Color32U5BU5D_t3850468773* get_colors32_10() const { return ___colors32_10; }
	inline Color32U5BU5D_t3850468773** get_address_of_colors32_10() { return &___colors32_10; }
	inline void set_colors32_10(Color32U5BU5D_t3850468773* value)
	{
		___colors32_10 = value;
		Il2CppCodeGenWriteBarrier((&___colors32_10), value);
	}

	inline static int32_t get_offset_of_triangles_11() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t2771747634, ___triangles_11)); }
	inline Int32U5BU5D_t385246372* get_triangles_11() const { return ___triangles_11; }
	inline Int32U5BU5D_t385246372** get_address_of_triangles_11() { return &___triangles_11; }
	inline void set_triangles_11(Int32U5BU5D_t385246372* value)
	{
		___triangles_11 = value;
		Il2CppCodeGenWriteBarrier((&___triangles_11), value);
	}
};

struct TMP_MeshInfo_t2771747634_StaticFields
{
public:
	// UnityEngine.Color32 TMPro.TMP_MeshInfo::s_DefaultColor
	Color32_t2600501292  ___s_DefaultColor_0;
	// UnityEngine.Vector3 TMPro.TMP_MeshInfo::s_DefaultNormal
	Vector3_t3722313464  ___s_DefaultNormal_1;
	// UnityEngine.Vector4 TMPro.TMP_MeshInfo::s_DefaultTangent
	Vector4_t3319028937  ___s_DefaultTangent_2;

public:
	inline static int32_t get_offset_of_s_DefaultColor_0() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t2771747634_StaticFields, ___s_DefaultColor_0)); }
	inline Color32_t2600501292  get_s_DefaultColor_0() const { return ___s_DefaultColor_0; }
	inline Color32_t2600501292 * get_address_of_s_DefaultColor_0() { return &___s_DefaultColor_0; }
	inline void set_s_DefaultColor_0(Color32_t2600501292  value)
	{
		___s_DefaultColor_0 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_1() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t2771747634_StaticFields, ___s_DefaultNormal_1)); }
	inline Vector3_t3722313464  get_s_DefaultNormal_1() const { return ___s_DefaultNormal_1; }
	inline Vector3_t3722313464 * get_address_of_s_DefaultNormal_1() { return &___s_DefaultNormal_1; }
	inline void set_s_DefaultNormal_1(Vector3_t3722313464  value)
	{
		___s_DefaultNormal_1 = value;
	}

	inline static int32_t get_offset_of_s_DefaultTangent_2() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t2771747634_StaticFields, ___s_DefaultTangent_2)); }
	inline Vector4_t3319028937  get_s_DefaultTangent_2() const { return ___s_DefaultTangent_2; }
	inline Vector4_t3319028937 * get_address_of_s_DefaultTangent_2() { return &___s_DefaultTangent_2; }
	inline void set_s_DefaultTangent_2(Vector4_t3319028937  value)
	{
		___s_DefaultTangent_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_MeshInfo
struct TMP_MeshInfo_t2771747634_marshaled_pinvoke
{
	Mesh_t3648964284 * ___mesh_3;
	int32_t ___vertexCount_4;
	Vector3_t3722313464 * ___vertices_5;
	Vector3_t3722313464 * ___normals_6;
	Vector4_t3319028937 * ___tangents_7;
	Vector2_t2156229523 * ___uvs0_8;
	Vector2_t2156229523 * ___uvs2_9;
	Color32_t2600501292 * ___colors32_10;
	int32_t* ___triangles_11;
};
// Native definition for COM marshalling of TMPro.TMP_MeshInfo
struct TMP_MeshInfo_t2771747634_marshaled_com
{
	Mesh_t3648964284 * ___mesh_3;
	int32_t ___vertexCount_4;
	Vector3_t3722313464 * ___vertices_5;
	Vector3_t3722313464 * ___normals_6;
	Vector4_t3319028937 * ___tangents_7;
	Vector2_t2156229523 * ___uvs0_8;
	Vector2_t2156229523 * ___uvs2_9;
	Color32_t2600501292 * ___colors32_10;
	int32_t* ___triangles_11;
};
#endif // TMP_MESHINFO_T2771747634_H
#ifndef COLORTWEENCALLBACK_T824784811_H
#define COLORTWEENCALLBACK_T824784811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.ColorTween/ColorTweenCallback
struct  ColorTweenCallback_t824784811  : public UnityEvent_1_t3437345828
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORTWEENCALLBACK_T824784811_H
#ifndef COLORTWEENMODE_T4194101034_H
#define COLORTWEENMODE_T4194101034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.ColorTween/ColorTweenMode
struct  ColorTweenMode_t4194101034 
{
public:
	// System.Int32 TMPro.ColorTween/ColorTweenMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorTweenMode_t4194101034, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORTWEENMODE_T4194101034_H
#ifndef ANCHORPOSITIONS_T1063569770_H
#define ANCHORPOSITIONS_T1063569770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Compatibility/AnchorPositions
struct  AnchorPositions_t1063569770 
{
public:
	// System.Int32 TMPro.TMP_Compatibility/AnchorPositions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AnchorPositions_t1063569770, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHORPOSITIONS_T1063569770_H
#ifndef FLOATTWEENCALLBACK_T556094317_H
#define FLOATTWEENCALLBACK_T556094317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FloatTween/FloatTweenCallback
struct  FloatTweenCallback_t556094317  : public UnityEvent_1_t2278926278
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATTWEENCALLBACK_T556094317_H
#ifndef DROPDOWNEVENT_T1704673280_H
#define DROPDOWNEVENT_T1704673280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Dropdown/DropdownEvent
struct  DropdownEvent_t1704673280  : public UnityEvent_1_t3832605257
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DROPDOWNEVENT_T1704673280_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef TMP_DEFAULTCONTROLS_T2837752704_H
#define TMP_DEFAULTCONTROLS_T2837752704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_DefaultControls
struct  TMP_DefaultControls_t2837752704  : public RuntimeObject
{
public:

public:
};

struct TMP_DefaultControls_t2837752704_StaticFields
{
public:
	// UnityEngine.Vector2 TMPro.TMP_DefaultControls::s_ThickElementSize
	Vector2_t2156229523  ___s_ThickElementSize_3;
	// UnityEngine.Vector2 TMPro.TMP_DefaultControls::s_ThinElementSize
	Vector2_t2156229523  ___s_ThinElementSize_4;
	// UnityEngine.Color TMPro.TMP_DefaultControls::s_DefaultSelectableColor
	Color_t2555686324  ___s_DefaultSelectableColor_5;
	// UnityEngine.Color TMPro.TMP_DefaultControls::s_TextColor
	Color_t2555686324  ___s_TextColor_6;

public:
	inline static int32_t get_offset_of_s_ThickElementSize_3() { return static_cast<int32_t>(offsetof(TMP_DefaultControls_t2837752704_StaticFields, ___s_ThickElementSize_3)); }
	inline Vector2_t2156229523  get_s_ThickElementSize_3() const { return ___s_ThickElementSize_3; }
	inline Vector2_t2156229523 * get_address_of_s_ThickElementSize_3() { return &___s_ThickElementSize_3; }
	inline void set_s_ThickElementSize_3(Vector2_t2156229523  value)
	{
		___s_ThickElementSize_3 = value;
	}

	inline static int32_t get_offset_of_s_ThinElementSize_4() { return static_cast<int32_t>(offsetof(TMP_DefaultControls_t2837752704_StaticFields, ___s_ThinElementSize_4)); }
	inline Vector2_t2156229523  get_s_ThinElementSize_4() const { return ___s_ThinElementSize_4; }
	inline Vector2_t2156229523 * get_address_of_s_ThinElementSize_4() { return &___s_ThinElementSize_4; }
	inline void set_s_ThinElementSize_4(Vector2_t2156229523  value)
	{
		___s_ThinElementSize_4 = value;
	}

	inline static int32_t get_offset_of_s_DefaultSelectableColor_5() { return static_cast<int32_t>(offsetof(TMP_DefaultControls_t2837752704_StaticFields, ___s_DefaultSelectableColor_5)); }
	inline Color_t2555686324  get_s_DefaultSelectableColor_5() const { return ___s_DefaultSelectableColor_5; }
	inline Color_t2555686324 * get_address_of_s_DefaultSelectableColor_5() { return &___s_DefaultSelectableColor_5; }
	inline void set_s_DefaultSelectableColor_5(Color_t2555686324  value)
	{
		___s_DefaultSelectableColor_5 = value;
	}

	inline static int32_t get_offset_of_s_TextColor_6() { return static_cast<int32_t>(offsetof(TMP_DefaultControls_t2837752704_StaticFields, ___s_TextColor_6)); }
	inline Color_t2555686324  get_s_TextColor_6() const { return ___s_TextColor_6; }
	inline Color_t2555686324 * get_address_of_s_TextColor_6() { return &___s_TextColor_6; }
	inline void set_s_TextColor_6(Color_t2555686324  value)
	{
		___s_TextColor_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_DEFAULTCONTROLS_T2837752704_H
#ifndef TMP_VERTEX_T2404176824_H
#define TMP_VERTEX_T2404176824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Vertex
struct  TMP_Vertex_t2404176824 
{
public:
	// UnityEngine.Vector3 TMPro.TMP_Vertex::position
	Vector3_t3722313464  ___position_0;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv
	Vector2_t2156229523  ___uv_1;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv2
	Vector2_t2156229523  ___uv2_2;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv4
	Vector2_t2156229523  ___uv4_3;
	// UnityEngine.Color32 TMPro.TMP_Vertex::color
	Color32_t2600501292  ___color_4;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___position_0)); }
	inline Vector3_t3722313464  get_position_0() const { return ___position_0; }
	inline Vector3_t3722313464 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t3722313464  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_uv_1() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___uv_1)); }
	inline Vector2_t2156229523  get_uv_1() const { return ___uv_1; }
	inline Vector2_t2156229523 * get_address_of_uv_1() { return &___uv_1; }
	inline void set_uv_1(Vector2_t2156229523  value)
	{
		___uv_1 = value;
	}

	inline static int32_t get_offset_of_uv2_2() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___uv2_2)); }
	inline Vector2_t2156229523  get_uv2_2() const { return ___uv2_2; }
	inline Vector2_t2156229523 * get_address_of_uv2_2() { return &___uv2_2; }
	inline void set_uv2_2(Vector2_t2156229523  value)
	{
		___uv2_2 = value;
	}

	inline static int32_t get_offset_of_uv4_3() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___uv4_3)); }
	inline Vector2_t2156229523  get_uv4_3() const { return ___uv4_3; }
	inline Vector2_t2156229523 * get_address_of_uv4_3() { return &___uv4_3; }
	inline void set_uv4_3(Vector2_t2156229523  value)
	{
		___uv4_3 = value;
	}

	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___color_4)); }
	inline Color32_t2600501292  get_color_4() const { return ___color_4; }
	inline Color32_t2600501292 * get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(Color32_t2600501292  value)
	{
		___color_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_VERTEX_T2404176824_H
#ifndef VERTEXHELPER_T2453304189_H
#define VERTEXHELPER_T2453304189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VertexHelper
struct  VertexHelper_t2453304189  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_t899420910 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_t4072576034 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t3628304265 * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t3628304265 * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t3628304265 * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t3628304265 * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_t899420910 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_t496136383 * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_t128053199 * ___m_Indices_8;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Positions_0)); }
	inline List_1_t899420910 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_t899420910 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_t899420910 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Positions_0), value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Colors_1)); }
	inline List_1_t4072576034 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_t4072576034 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_t4072576034 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Colors_1), value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv0S_2)); }
	inline List_1_t3628304265 * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t3628304265 ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t3628304265 * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv0S_2), value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv1S_3)); }
	inline List_1_t3628304265 * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t3628304265 ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t3628304265 * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv1S_3), value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv2S_4)); }
	inline List_1_t3628304265 * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t3628304265 ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t3628304265 * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv2S_4), value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv3S_5)); }
	inline List_1_t3628304265 * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t3628304265 ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t3628304265 * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv3S_5), value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Normals_6)); }
	inline List_1_t899420910 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_t899420910 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_t899420910 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normals_6), value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Tangents_7)); }
	inline List_1_t496136383 * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_t496136383 ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_t496136383 * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tangents_7), value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Indices_8)); }
	inline List_1_t128053199 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_t128053199 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_t128053199 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Indices_8), value);
	}
};

struct VertexHelper_t2453304189_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_t3319028937  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_t3722313464  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_t3319028937  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_t3319028937 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_t3319028937  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_t3722313464  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_t3722313464 * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_t3722313464  value)
	{
		___s_DefaultNormal_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPER_T2453304189_H
#ifndef MODE_T1066900953_H
#define MODE_T1066900953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1066900953 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1066900953, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1066900953_H
#ifndef SELECTIONSTATE_T2656606514_H
#define SELECTIONSTATE_T2656606514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2656606514 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectionState_t2656606514, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2656606514_H
#ifndef TRANSITION_T1769908631_H
#define TRANSITION_T1769908631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1769908631 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t1769908631, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1769908631_H
#ifndef COLORBLOCK_T2139031574_H
#define COLORBLOCK_T2139031574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2139031574 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2555686324  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2555686324  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2555686324  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_NormalColor_0)); }
	inline Color_t2555686324  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2555686324 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2555686324  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_HighlightedColor_1)); }
	inline Color_t2555686324  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2555686324 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2555686324  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_PressedColor_2)); }
	inline Color_t2555686324  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2555686324 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2555686324  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_DisabledColor_3)); }
	inline Color_t2555686324  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2555686324  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2139031574_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255365  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t2488454196  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t2488454196  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t2488454196 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t2488454196  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef SELECTIONEVENT_T4268942288_H
#define SELECTIONEVENT_T4268942288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/SelectionEvent
struct  SelectionEvent_t4268942288  : public UnityEvent_1_t2729110193
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONEVENT_T4268942288_H
#ifndef ONCHANGEEVENT_T4257774360_H
#define ONCHANGEEVENT_T4257774360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/OnChangeEvent
struct  OnChangeEvent_t4257774360  : public UnityEvent_1_t2729110193
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCHANGEEVENT_T4257774360_H
#ifndef SUBMITEVENT_T1343580625_H
#define SUBMITEVENT_T1343580625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/SubmitEvent
struct  SubmitEvent_t1343580625  : public UnityEvent_1_t2729110193
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBMITEVENT_T1343580625_H
#ifndef TEXTSELECTIONEVENT_T1023421581_H
#define TEXTSELECTIONEVENT_T1023421581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/TextSelectionEvent
struct  TextSelectionEvent_t1023421581  : public UnityEvent_3_t1597070127
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTSELECTIONEVENT_T1023421581_H
#ifndef VERTEXSORTINGORDER_T2659893934_H
#define VERTEXSORTINGORDER_T2659893934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexSortingOrder
struct  VertexSortingOrder_t2659893934 
{
public:
	// System.Int32 TMPro.VertexSortingOrder::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VertexSortingOrder_t2659893934, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXSORTINGORDER_T2659893934_H
#ifndef U3CMOUSEDRAGOUTSIDERECTU3ED__255_T2073557366_H
#define U3CMOUSEDRAGOUTSIDERECTU3ED__255_T2073557366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/<MouseDragOutsideRect>d__255
struct  U3CMouseDragOutsideRectU3Ed__255_t2073557366  : public RuntimeObject
{
public:
	// System.Int32 TMPro.TMP_InputField/<MouseDragOutsideRect>d__255::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.TMP_InputField/<MouseDragOutsideRect>d__255::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.EventSystems.PointerEventData TMPro.TMP_InputField/<MouseDragOutsideRect>d__255::eventData
	PointerEventData_t3807901092 * ___eventData_2;
	// TMPro.TMP_InputField TMPro.TMP_InputField/<MouseDragOutsideRect>d__255::<>4__this
	TMP_InputField_t1099764886 * ___U3CU3E4__this_3;
	// UnityEngine.Vector2 TMPro.TMP_InputField/<MouseDragOutsideRect>d__255::<localMousePos>5__1
	Vector2_t2156229523  ___U3ClocalMousePosU3E5__1_4;
	// UnityEngine.Rect TMPro.TMP_InputField/<MouseDragOutsideRect>d__255::<rect>5__2
	Rect_t2360479859  ___U3CrectU3E5__2_5;
	// System.Single TMPro.TMP_InputField/<MouseDragOutsideRect>d__255::<delay>5__3
	float ___U3CdelayU3E5__3_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ed__255_t2073557366, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ed__255_t2073557366, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_eventData_2() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ed__255_t2073557366, ___eventData_2)); }
	inline PointerEventData_t3807901092 * get_eventData_2() const { return ___eventData_2; }
	inline PointerEventData_t3807901092 ** get_address_of_eventData_2() { return &___eventData_2; }
	inline void set_eventData_2(PointerEventData_t3807901092 * value)
	{
		___eventData_2 = value;
		Il2CppCodeGenWriteBarrier((&___eventData_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ed__255_t2073557366, ___U3CU3E4__this_3)); }
	inline TMP_InputField_t1099764886 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline TMP_InputField_t1099764886 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(TMP_InputField_t1099764886 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3ClocalMousePosU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ed__255_t2073557366, ___U3ClocalMousePosU3E5__1_4)); }
	inline Vector2_t2156229523  get_U3ClocalMousePosU3E5__1_4() const { return ___U3ClocalMousePosU3E5__1_4; }
	inline Vector2_t2156229523 * get_address_of_U3ClocalMousePosU3E5__1_4() { return &___U3ClocalMousePosU3E5__1_4; }
	inline void set_U3ClocalMousePosU3E5__1_4(Vector2_t2156229523  value)
	{
		___U3ClocalMousePosU3E5__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CrectU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ed__255_t2073557366, ___U3CrectU3E5__2_5)); }
	inline Rect_t2360479859  get_U3CrectU3E5__2_5() const { return ___U3CrectU3E5__2_5; }
	inline Rect_t2360479859 * get_address_of_U3CrectU3E5__2_5() { return &___U3CrectU3E5__2_5; }
	inline void set_U3CrectU3E5__2_5(Rect_t2360479859  value)
	{
		___U3CrectU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CdelayU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ed__255_t2073557366, ___U3CdelayU3E5__3_6)); }
	inline float get_U3CdelayU3E5__3_6() const { return ___U3CdelayU3E5__3_6; }
	inline float* get_address_of_U3CdelayU3E5__3_6() { return &___U3CdelayU3E5__3_6; }
	inline void set_U3CdelayU3E5__3_6(float value)
	{
		___U3CdelayU3E5__3_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMOUSEDRAGOUTSIDERECTU3ED__255_T2073557366_H
#ifndef EDITSTATE_T2966235475_H
#define EDITSTATE_T2966235475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/EditState
struct  EditState_t2966235475 
{
public:
	// System.Int32 TMPro.TMP_InputField/EditState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EditState_t2966235475, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITSTATE_T2966235475_H
#ifndef FONTASSETTYPES_T1222456209_H
#define FONTASSETTYPES_T1222456209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontAsset/FontAssetTypes
struct  FontAssetTypes_t1222456209 
{
public:
	// System.Int32 TMPro.TMP_FontAsset/FontAssetTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FontAssetTypes_t1222456209, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTASSETTYPES_T1222456209_H
#ifndef CHARACTERVALIDATION_T3801396403_H
#define CHARACTERVALIDATION_T3801396403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/CharacterValidation
struct  CharacterValidation_t3801396403 
{
public:
	// System.Int32 TMPro.TMP_InputField/CharacterValidation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CharacterValidation_t3801396403, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERVALIDATION_T3801396403_H
#ifndef INPUTTYPE_T2510134850_H
#define INPUTTYPE_T2510134850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/InputType
struct  InputType_t2510134850 
{
public:
	// System.Int32 TMPro.TMP_InputField/InputType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputType_t2510134850, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTTYPE_T2510134850_H
#ifndef CONTENTTYPE_T1128941285_H
#define CONTENTTYPE_T1128941285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/ContentType
struct  ContentType_t1128941285 
{
public:
	// System.Int32 TMPro.TMP_InputField/ContentType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ContentType_t1128941285, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTTYPE_T1128941285_H
#ifndef LINETYPE_T2817627283_H
#define LINETYPE_T2817627283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/LineType
struct  LineType_t2817627283 
{
public:
	// System.Int32 TMPro.TMP_InputField/LineType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LineType_t2817627283, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINETYPE_T2817627283_H
#ifndef TMP_LINEINFO_T1079631636_H
#define TMP_LINEINFO_T1079631636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LineInfo
struct  TMP_LineInfo_t1079631636 
{
public:
	// System.Int32 TMPro.TMP_LineInfo::characterCount
	int32_t ___characterCount_0;
	// System.Int32 TMPro.TMP_LineInfo::visibleCharacterCount
	int32_t ___visibleCharacterCount_1;
	// System.Int32 TMPro.TMP_LineInfo::spaceCount
	int32_t ___spaceCount_2;
	// System.Int32 TMPro.TMP_LineInfo::wordCount
	int32_t ___wordCount_3;
	// System.Int32 TMPro.TMP_LineInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_4;
	// System.Int32 TMPro.TMP_LineInfo::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_5;
	// System.Int32 TMPro.TMP_LineInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_6;
	// System.Int32 TMPro.TMP_LineInfo::lastVisibleCharacterIndex
	int32_t ___lastVisibleCharacterIndex_7;
	// System.Single TMPro.TMP_LineInfo::length
	float ___length_8;
	// System.Single TMPro.TMP_LineInfo::lineHeight
	float ___lineHeight_9;
	// System.Single TMPro.TMP_LineInfo::ascender
	float ___ascender_10;
	// System.Single TMPro.TMP_LineInfo::baseline
	float ___baseline_11;
	// System.Single TMPro.TMP_LineInfo::descender
	float ___descender_12;
	// System.Single TMPro.TMP_LineInfo::maxAdvance
	float ___maxAdvance_13;
	// System.Single TMPro.TMP_LineInfo::width
	float ___width_14;
	// System.Single TMPro.TMP_LineInfo::marginLeft
	float ___marginLeft_15;
	// System.Single TMPro.TMP_LineInfo::marginRight
	float ___marginRight_16;
	// TMPro.TextAlignmentOptions TMPro.TMP_LineInfo::alignment
	int32_t ___alignment_17;
	// TMPro.Extents TMPro.TMP_LineInfo::lineExtents
	Extents_t3837212874  ___lineExtents_18;

public:
	inline static int32_t get_offset_of_characterCount_0() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___characterCount_0)); }
	inline int32_t get_characterCount_0() const { return ___characterCount_0; }
	inline int32_t* get_address_of_characterCount_0() { return &___characterCount_0; }
	inline void set_characterCount_0(int32_t value)
	{
		___characterCount_0 = value;
	}

	inline static int32_t get_offset_of_visibleCharacterCount_1() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___visibleCharacterCount_1)); }
	inline int32_t get_visibleCharacterCount_1() const { return ___visibleCharacterCount_1; }
	inline int32_t* get_address_of_visibleCharacterCount_1() { return &___visibleCharacterCount_1; }
	inline void set_visibleCharacterCount_1(int32_t value)
	{
		___visibleCharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_spaceCount_2() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___spaceCount_2)); }
	inline int32_t get_spaceCount_2() const { return ___spaceCount_2; }
	inline int32_t* get_address_of_spaceCount_2() { return &___spaceCount_2; }
	inline void set_spaceCount_2(int32_t value)
	{
		___spaceCount_2 = value;
	}

	inline static int32_t get_offset_of_wordCount_3() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___wordCount_3)); }
	inline int32_t get_wordCount_3() const { return ___wordCount_3; }
	inline int32_t* get_address_of_wordCount_3() { return &___wordCount_3; }
	inline void set_wordCount_3(int32_t value)
	{
		___wordCount_3 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_4() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___firstCharacterIndex_4)); }
	inline int32_t get_firstCharacterIndex_4() const { return ___firstCharacterIndex_4; }
	inline int32_t* get_address_of_firstCharacterIndex_4() { return &___firstCharacterIndex_4; }
	inline void set_firstCharacterIndex_4(int32_t value)
	{
		___firstCharacterIndex_4 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_5() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___firstVisibleCharacterIndex_5)); }
	inline int32_t get_firstVisibleCharacterIndex_5() const { return ___firstVisibleCharacterIndex_5; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_5() { return &___firstVisibleCharacterIndex_5; }
	inline void set_firstVisibleCharacterIndex_5(int32_t value)
	{
		___firstVisibleCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_6() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lastCharacterIndex_6)); }
	inline int32_t get_lastCharacterIndex_6() const { return ___lastCharacterIndex_6; }
	inline int32_t* get_address_of_lastCharacterIndex_6() { return &___lastCharacterIndex_6; }
	inline void set_lastCharacterIndex_6(int32_t value)
	{
		___lastCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharacterIndex_7() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lastVisibleCharacterIndex_7)); }
	inline int32_t get_lastVisibleCharacterIndex_7() const { return ___lastVisibleCharacterIndex_7; }
	inline int32_t* get_address_of_lastVisibleCharacterIndex_7() { return &___lastVisibleCharacterIndex_7; }
	inline void set_lastVisibleCharacterIndex_7(int32_t value)
	{
		___lastVisibleCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_length_8() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___length_8)); }
	inline float get_length_8() const { return ___length_8; }
	inline float* get_address_of_length_8() { return &___length_8; }
	inline void set_length_8(float value)
	{
		___length_8 = value;
	}

	inline static int32_t get_offset_of_lineHeight_9() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lineHeight_9)); }
	inline float get_lineHeight_9() const { return ___lineHeight_9; }
	inline float* get_address_of_lineHeight_9() { return &___lineHeight_9; }
	inline void set_lineHeight_9(float value)
	{
		___lineHeight_9 = value;
	}

	inline static int32_t get_offset_of_ascender_10() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___ascender_10)); }
	inline float get_ascender_10() const { return ___ascender_10; }
	inline float* get_address_of_ascender_10() { return &___ascender_10; }
	inline void set_ascender_10(float value)
	{
		___ascender_10 = value;
	}

	inline static int32_t get_offset_of_baseline_11() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___baseline_11)); }
	inline float get_baseline_11() const { return ___baseline_11; }
	inline float* get_address_of_baseline_11() { return &___baseline_11; }
	inline void set_baseline_11(float value)
	{
		___baseline_11 = value;
	}

	inline static int32_t get_offset_of_descender_12() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___descender_12)); }
	inline float get_descender_12() const { return ___descender_12; }
	inline float* get_address_of_descender_12() { return &___descender_12; }
	inline void set_descender_12(float value)
	{
		___descender_12 = value;
	}

	inline static int32_t get_offset_of_maxAdvance_13() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___maxAdvance_13)); }
	inline float get_maxAdvance_13() const { return ___maxAdvance_13; }
	inline float* get_address_of_maxAdvance_13() { return &___maxAdvance_13; }
	inline void set_maxAdvance_13(float value)
	{
		___maxAdvance_13 = value;
	}

	inline static int32_t get_offset_of_width_14() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___width_14)); }
	inline float get_width_14() const { return ___width_14; }
	inline float* get_address_of_width_14() { return &___width_14; }
	inline void set_width_14(float value)
	{
		___width_14 = value;
	}

	inline static int32_t get_offset_of_marginLeft_15() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___marginLeft_15)); }
	inline float get_marginLeft_15() const { return ___marginLeft_15; }
	inline float* get_address_of_marginLeft_15() { return &___marginLeft_15; }
	inline void set_marginLeft_15(float value)
	{
		___marginLeft_15 = value;
	}

	inline static int32_t get_offset_of_marginRight_16() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___marginRight_16)); }
	inline float get_marginRight_16() const { return ___marginRight_16; }
	inline float* get_address_of_marginRight_16() { return &___marginRight_16; }
	inline void set_marginRight_16(float value)
	{
		___marginRight_16 = value;
	}

	inline static int32_t get_offset_of_alignment_17() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___alignment_17)); }
	inline int32_t get_alignment_17() const { return ___alignment_17; }
	inline int32_t* get_address_of_alignment_17() { return &___alignment_17; }
	inline void set_alignment_17(int32_t value)
	{
		___alignment_17 = value;
	}

	inline static int32_t get_offset_of_lineExtents_18() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lineExtents_18)); }
	inline Extents_t3837212874  get_lineExtents_18() const { return ___lineExtents_18; }
	inline Extents_t3837212874 * get_address_of_lineExtents_18() { return &___lineExtents_18; }
	inline void set_lineExtents_18(Extents_t3837212874  value)
	{
		___lineExtents_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_LINEINFO_T1079631636_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef NAVIGATION_T3049316579_H
#define NAVIGATION_T3049316579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t3049316579 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3250028441 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnUp_1)); }
	inline Selectable_t3250028441 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3250028441 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnDown_2)); }
	inline Selectable_t3250028441 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3250028441 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnLeft_3)); }
	inline Selectable_t3250028441 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3250028441 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnRight_4)); }
	inline Selectable_t3250028441 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3250028441 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T3049316579_H
#ifndef COLORTWEEN_T378116136_H
#define COLORTWEEN_T378116136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.ColorTween
struct  ColorTween_t378116136 
{
public:
	// TMPro.ColorTween/ColorTweenCallback TMPro.ColorTween::m_Target
	ColorTweenCallback_t824784811 * ___m_Target_0;
	// UnityEngine.Color TMPro.ColorTween::m_StartColor
	Color_t2555686324  ___m_StartColor_1;
	// UnityEngine.Color TMPro.ColorTween::m_TargetColor
	Color_t2555686324  ___m_TargetColor_2;
	// TMPro.ColorTween/ColorTweenMode TMPro.ColorTween::m_TweenMode
	int32_t ___m_TweenMode_3;
	// System.Single TMPro.ColorTween::m_Duration
	float ___m_Duration_4;
	// System.Boolean TMPro.ColorTween::m_IgnoreTimeScale
	bool ___m_IgnoreTimeScale_5;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(ColorTween_t378116136, ___m_Target_0)); }
	inline ColorTweenCallback_t824784811 * get_m_Target_0() const { return ___m_Target_0; }
	inline ColorTweenCallback_t824784811 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(ColorTweenCallback_t824784811 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_StartColor_1() { return static_cast<int32_t>(offsetof(ColorTween_t378116136, ___m_StartColor_1)); }
	inline Color_t2555686324  get_m_StartColor_1() const { return ___m_StartColor_1; }
	inline Color_t2555686324 * get_address_of_m_StartColor_1() { return &___m_StartColor_1; }
	inline void set_m_StartColor_1(Color_t2555686324  value)
	{
		___m_StartColor_1 = value;
	}

	inline static int32_t get_offset_of_m_TargetColor_2() { return static_cast<int32_t>(offsetof(ColorTween_t378116136, ___m_TargetColor_2)); }
	inline Color_t2555686324  get_m_TargetColor_2() const { return ___m_TargetColor_2; }
	inline Color_t2555686324 * get_address_of_m_TargetColor_2() { return &___m_TargetColor_2; }
	inline void set_m_TargetColor_2(Color_t2555686324  value)
	{
		___m_TargetColor_2 = value;
	}

	inline static int32_t get_offset_of_m_TweenMode_3() { return static_cast<int32_t>(offsetof(ColorTween_t378116136, ___m_TweenMode_3)); }
	inline int32_t get_m_TweenMode_3() const { return ___m_TweenMode_3; }
	inline int32_t* get_address_of_m_TweenMode_3() { return &___m_TweenMode_3; }
	inline void set_m_TweenMode_3(int32_t value)
	{
		___m_TweenMode_3 = value;
	}

	inline static int32_t get_offset_of_m_Duration_4() { return static_cast<int32_t>(offsetof(ColorTween_t378116136, ___m_Duration_4)); }
	inline float get_m_Duration_4() const { return ___m_Duration_4; }
	inline float* get_address_of_m_Duration_4() { return &___m_Duration_4; }
	inline void set_m_Duration_4(float value)
	{
		___m_Duration_4 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreTimeScale_5() { return static_cast<int32_t>(offsetof(ColorTween_t378116136, ___m_IgnoreTimeScale_5)); }
	inline bool get_m_IgnoreTimeScale_5() const { return ___m_IgnoreTimeScale_5; }
	inline bool* get_address_of_m_IgnoreTimeScale_5() { return &___m_IgnoreTimeScale_5; }
	inline void set_m_IgnoreTimeScale_5(bool value)
	{
		___m_IgnoreTimeScale_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.ColorTween
struct ColorTween_t378116136_marshaled_pinvoke
{
	ColorTweenCallback_t824784811 * ___m_Target_0;
	Color_t2555686324  ___m_StartColor_1;
	Color_t2555686324  ___m_TargetColor_2;
	int32_t ___m_TweenMode_3;
	float ___m_Duration_4;
	int32_t ___m_IgnoreTimeScale_5;
};
// Native definition for COM marshalling of TMPro.ColorTween
struct ColorTween_t378116136_marshaled_com
{
	ColorTweenCallback_t824784811 * ___m_Target_0;
	Color_t2555686324  ___m_StartColor_1;
	Color_t2555686324  ___m_TargetColor_2;
	int32_t ___m_TweenMode_3;
	float ___m_Duration_4;
	int32_t ___m_IgnoreTimeScale_5;
};
#endif // COLORTWEEN_T378116136_H
#ifndef TMP_CHARACTERINFO_T3185626797_H
#define TMP_CHARACTERINFO_T3185626797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_CharacterInfo
struct  TMP_CharacterInfo_t3185626797 
{
public:
	// System.Char TMPro.TMP_CharacterInfo::character
	Il2CppChar ___character_0;
	// System.Int16 TMPro.TMP_CharacterInfo::index
	int16_t ___index_1;
	// TMPro.TMP_TextElementType TMPro.TMP_CharacterInfo::elementType
	int32_t ___elementType_2;
	// TMPro.TMP_TextElement TMPro.TMP_CharacterInfo::textElement
	TMP_TextElement_t129727469 * ___textElement_3;
	// TMPro.TMP_FontAsset TMPro.TMP_CharacterInfo::fontAsset
	TMP_FontAsset_t364381626 * ___fontAsset_4;
	// TMPro.TMP_SpriteAsset TMPro.TMP_CharacterInfo::spriteAsset
	TMP_SpriteAsset_t484820633 * ___spriteAsset_5;
	// System.Int32 TMPro.TMP_CharacterInfo::spriteIndex
	int32_t ___spriteIndex_6;
	// UnityEngine.Material TMPro.TMP_CharacterInfo::material
	Material_t340375123 * ___material_7;
	// System.Int32 TMPro.TMP_CharacterInfo::materialReferenceIndex
	int32_t ___materialReferenceIndex_8;
	// System.Boolean TMPro.TMP_CharacterInfo::isUsingAlternateTypeface
	bool ___isUsingAlternateTypeface_9;
	// System.Single TMPro.TMP_CharacterInfo::pointSize
	float ___pointSize_10;
	// System.Int16 TMPro.TMP_CharacterInfo::lineNumber
	int16_t ___lineNumber_11;
	// System.Int16 TMPro.TMP_CharacterInfo::pageNumber
	int16_t ___pageNumber_12;
	// System.Int32 TMPro.TMP_CharacterInfo::vertexIndex
	int32_t ___vertexIndex_13;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_TL
	TMP_Vertex_t2404176824  ___vertex_TL_14;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_BL
	TMP_Vertex_t2404176824  ___vertex_BL_15;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_TR
	TMP_Vertex_t2404176824  ___vertex_TR_16;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_BR
	TMP_Vertex_t2404176824  ___vertex_BR_17;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::topLeft
	Vector3_t3722313464  ___topLeft_18;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::bottomLeft
	Vector3_t3722313464  ___bottomLeft_19;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::topRight
	Vector3_t3722313464  ___topRight_20;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::bottomRight
	Vector3_t3722313464  ___bottomRight_21;
	// System.Single TMPro.TMP_CharacterInfo::origin
	float ___origin_22;
	// System.Single TMPro.TMP_CharacterInfo::ascender
	float ___ascender_23;
	// System.Single TMPro.TMP_CharacterInfo::baseLine
	float ___baseLine_24;
	// System.Single TMPro.TMP_CharacterInfo::descender
	float ___descender_25;
	// System.Single TMPro.TMP_CharacterInfo::xAdvance
	float ___xAdvance_26;
	// System.Single TMPro.TMP_CharacterInfo::aspectRatio
	float ___aspectRatio_27;
	// System.Single TMPro.TMP_CharacterInfo::scale
	float ___scale_28;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::color
	Color32_t2600501292  ___color_29;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::underlineColor
	Color32_t2600501292  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::strikethroughColor
	Color32_t2600501292  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::highlightColor
	Color32_t2600501292  ___highlightColor_32;
	// TMPro.FontStyles TMPro.TMP_CharacterInfo::style
	int32_t ___style_33;
	// System.Boolean TMPro.TMP_CharacterInfo::isVisible
	bool ___isVisible_34;

public:
	inline static int32_t get_offset_of_character_0() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___character_0)); }
	inline Il2CppChar get_character_0() const { return ___character_0; }
	inline Il2CppChar* get_address_of_character_0() { return &___character_0; }
	inline void set_character_0(Il2CppChar value)
	{
		___character_0 = value;
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___index_1)); }
	inline int16_t get_index_1() const { return ___index_1; }
	inline int16_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int16_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_elementType_2() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___elementType_2)); }
	inline int32_t get_elementType_2() const { return ___elementType_2; }
	inline int32_t* get_address_of_elementType_2() { return &___elementType_2; }
	inline void set_elementType_2(int32_t value)
	{
		___elementType_2 = value;
	}

	inline static int32_t get_offset_of_textElement_3() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___textElement_3)); }
	inline TMP_TextElement_t129727469 * get_textElement_3() const { return ___textElement_3; }
	inline TMP_TextElement_t129727469 ** get_address_of_textElement_3() { return &___textElement_3; }
	inline void set_textElement_3(TMP_TextElement_t129727469 * value)
	{
		___textElement_3 = value;
		Il2CppCodeGenWriteBarrier((&___textElement_3), value);
	}

	inline static int32_t get_offset_of_fontAsset_4() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___fontAsset_4)); }
	inline TMP_FontAsset_t364381626 * get_fontAsset_4() const { return ___fontAsset_4; }
	inline TMP_FontAsset_t364381626 ** get_address_of_fontAsset_4() { return &___fontAsset_4; }
	inline void set_fontAsset_4(TMP_FontAsset_t364381626 * value)
	{
		___fontAsset_4 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_4), value);
	}

	inline static int32_t get_offset_of_spriteAsset_5() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___spriteAsset_5)); }
	inline TMP_SpriteAsset_t484820633 * get_spriteAsset_5() const { return ___spriteAsset_5; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_spriteAsset_5() { return &___spriteAsset_5; }
	inline void set_spriteAsset_5(TMP_SpriteAsset_t484820633 * value)
	{
		___spriteAsset_5 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_5), value);
	}

	inline static int32_t get_offset_of_spriteIndex_6() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___spriteIndex_6)); }
	inline int32_t get_spriteIndex_6() const { return ___spriteIndex_6; }
	inline int32_t* get_address_of_spriteIndex_6() { return &___spriteIndex_6; }
	inline void set_spriteIndex_6(int32_t value)
	{
		___spriteIndex_6 = value;
	}

	inline static int32_t get_offset_of_material_7() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___material_7)); }
	inline Material_t340375123 * get_material_7() const { return ___material_7; }
	inline Material_t340375123 ** get_address_of_material_7() { return &___material_7; }
	inline void set_material_7(Material_t340375123 * value)
	{
		___material_7 = value;
		Il2CppCodeGenWriteBarrier((&___material_7), value);
	}

	inline static int32_t get_offset_of_materialReferenceIndex_8() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___materialReferenceIndex_8)); }
	inline int32_t get_materialReferenceIndex_8() const { return ___materialReferenceIndex_8; }
	inline int32_t* get_address_of_materialReferenceIndex_8() { return &___materialReferenceIndex_8; }
	inline void set_materialReferenceIndex_8(int32_t value)
	{
		___materialReferenceIndex_8 = value;
	}

	inline static int32_t get_offset_of_isUsingAlternateTypeface_9() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___isUsingAlternateTypeface_9)); }
	inline bool get_isUsingAlternateTypeface_9() const { return ___isUsingAlternateTypeface_9; }
	inline bool* get_address_of_isUsingAlternateTypeface_9() { return &___isUsingAlternateTypeface_9; }
	inline void set_isUsingAlternateTypeface_9(bool value)
	{
		___isUsingAlternateTypeface_9 = value;
	}

	inline static int32_t get_offset_of_pointSize_10() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___pointSize_10)); }
	inline float get_pointSize_10() const { return ___pointSize_10; }
	inline float* get_address_of_pointSize_10() { return &___pointSize_10; }
	inline void set_pointSize_10(float value)
	{
		___pointSize_10 = value;
	}

	inline static int32_t get_offset_of_lineNumber_11() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___lineNumber_11)); }
	inline int16_t get_lineNumber_11() const { return ___lineNumber_11; }
	inline int16_t* get_address_of_lineNumber_11() { return &___lineNumber_11; }
	inline void set_lineNumber_11(int16_t value)
	{
		___lineNumber_11 = value;
	}

	inline static int32_t get_offset_of_pageNumber_12() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___pageNumber_12)); }
	inline int16_t get_pageNumber_12() const { return ___pageNumber_12; }
	inline int16_t* get_address_of_pageNumber_12() { return &___pageNumber_12; }
	inline void set_pageNumber_12(int16_t value)
	{
		___pageNumber_12 = value;
	}

	inline static int32_t get_offset_of_vertexIndex_13() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertexIndex_13)); }
	inline int32_t get_vertexIndex_13() const { return ___vertexIndex_13; }
	inline int32_t* get_address_of_vertexIndex_13() { return &___vertexIndex_13; }
	inline void set_vertexIndex_13(int32_t value)
	{
		___vertexIndex_13 = value;
	}

	inline static int32_t get_offset_of_vertex_TL_14() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_TL_14)); }
	inline TMP_Vertex_t2404176824  get_vertex_TL_14() const { return ___vertex_TL_14; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_TL_14() { return &___vertex_TL_14; }
	inline void set_vertex_TL_14(TMP_Vertex_t2404176824  value)
	{
		___vertex_TL_14 = value;
	}

	inline static int32_t get_offset_of_vertex_BL_15() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_BL_15)); }
	inline TMP_Vertex_t2404176824  get_vertex_BL_15() const { return ___vertex_BL_15; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_BL_15() { return &___vertex_BL_15; }
	inline void set_vertex_BL_15(TMP_Vertex_t2404176824  value)
	{
		___vertex_BL_15 = value;
	}

	inline static int32_t get_offset_of_vertex_TR_16() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_TR_16)); }
	inline TMP_Vertex_t2404176824  get_vertex_TR_16() const { return ___vertex_TR_16; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_TR_16() { return &___vertex_TR_16; }
	inline void set_vertex_TR_16(TMP_Vertex_t2404176824  value)
	{
		___vertex_TR_16 = value;
	}

	inline static int32_t get_offset_of_vertex_BR_17() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_BR_17)); }
	inline TMP_Vertex_t2404176824  get_vertex_BR_17() const { return ___vertex_BR_17; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_BR_17() { return &___vertex_BR_17; }
	inline void set_vertex_BR_17(TMP_Vertex_t2404176824  value)
	{
		___vertex_BR_17 = value;
	}

	inline static int32_t get_offset_of_topLeft_18() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___topLeft_18)); }
	inline Vector3_t3722313464  get_topLeft_18() const { return ___topLeft_18; }
	inline Vector3_t3722313464 * get_address_of_topLeft_18() { return &___topLeft_18; }
	inline void set_topLeft_18(Vector3_t3722313464  value)
	{
		___topLeft_18 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_19() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___bottomLeft_19)); }
	inline Vector3_t3722313464  get_bottomLeft_19() const { return ___bottomLeft_19; }
	inline Vector3_t3722313464 * get_address_of_bottomLeft_19() { return &___bottomLeft_19; }
	inline void set_bottomLeft_19(Vector3_t3722313464  value)
	{
		___bottomLeft_19 = value;
	}

	inline static int32_t get_offset_of_topRight_20() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___topRight_20)); }
	inline Vector3_t3722313464  get_topRight_20() const { return ___topRight_20; }
	inline Vector3_t3722313464 * get_address_of_topRight_20() { return &___topRight_20; }
	inline void set_topRight_20(Vector3_t3722313464  value)
	{
		___topRight_20 = value;
	}

	inline static int32_t get_offset_of_bottomRight_21() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___bottomRight_21)); }
	inline Vector3_t3722313464  get_bottomRight_21() const { return ___bottomRight_21; }
	inline Vector3_t3722313464 * get_address_of_bottomRight_21() { return &___bottomRight_21; }
	inline void set_bottomRight_21(Vector3_t3722313464  value)
	{
		___bottomRight_21 = value;
	}

	inline static int32_t get_offset_of_origin_22() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___origin_22)); }
	inline float get_origin_22() const { return ___origin_22; }
	inline float* get_address_of_origin_22() { return &___origin_22; }
	inline void set_origin_22(float value)
	{
		___origin_22 = value;
	}

	inline static int32_t get_offset_of_ascender_23() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___ascender_23)); }
	inline float get_ascender_23() const { return ___ascender_23; }
	inline float* get_address_of_ascender_23() { return &___ascender_23; }
	inline void set_ascender_23(float value)
	{
		___ascender_23 = value;
	}

	inline static int32_t get_offset_of_baseLine_24() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___baseLine_24)); }
	inline float get_baseLine_24() const { return ___baseLine_24; }
	inline float* get_address_of_baseLine_24() { return &___baseLine_24; }
	inline void set_baseLine_24(float value)
	{
		___baseLine_24 = value;
	}

	inline static int32_t get_offset_of_descender_25() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___descender_25)); }
	inline float get_descender_25() const { return ___descender_25; }
	inline float* get_address_of_descender_25() { return &___descender_25; }
	inline void set_descender_25(float value)
	{
		___descender_25 = value;
	}

	inline static int32_t get_offset_of_xAdvance_26() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___xAdvance_26)); }
	inline float get_xAdvance_26() const { return ___xAdvance_26; }
	inline float* get_address_of_xAdvance_26() { return &___xAdvance_26; }
	inline void set_xAdvance_26(float value)
	{
		___xAdvance_26 = value;
	}

	inline static int32_t get_offset_of_aspectRatio_27() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___aspectRatio_27)); }
	inline float get_aspectRatio_27() const { return ___aspectRatio_27; }
	inline float* get_address_of_aspectRatio_27() { return &___aspectRatio_27; }
	inline void set_aspectRatio_27(float value)
	{
		___aspectRatio_27 = value;
	}

	inline static int32_t get_offset_of_scale_28() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___scale_28)); }
	inline float get_scale_28() const { return ___scale_28; }
	inline float* get_address_of_scale_28() { return &___scale_28; }
	inline void set_scale_28(float value)
	{
		___scale_28 = value;
	}

	inline static int32_t get_offset_of_color_29() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___color_29)); }
	inline Color32_t2600501292  get_color_29() const { return ___color_29; }
	inline Color32_t2600501292 * get_address_of_color_29() { return &___color_29; }
	inline void set_color_29(Color32_t2600501292  value)
	{
		___color_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___underlineColor_30)); }
	inline Color32_t2600501292  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t2600501292 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t2600501292  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___strikethroughColor_31)); }
	inline Color32_t2600501292  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t2600501292 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t2600501292  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___highlightColor_32)); }
	inline Color32_t2600501292  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t2600501292 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t2600501292  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_style_33() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___style_33)); }
	inline int32_t get_style_33() const { return ___style_33; }
	inline int32_t* get_address_of_style_33() { return &___style_33; }
	inline void set_style_33(int32_t value)
	{
		___style_33 = value;
	}

	inline static int32_t get_offset_of_isVisible_34() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___isVisible_34)); }
	inline bool get_isVisible_34() const { return ___isVisible_34; }
	inline bool* get_address_of_isVisible_34() { return &___isVisible_34; }
	inline void set_isVisible_34(bool value)
	{
		___isVisible_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_CharacterInfo
struct TMP_CharacterInfo_t3185626797_marshaled_pinvoke
{
	uint8_t ___character_0;
	int16_t ___index_1;
	int32_t ___elementType_2;
	TMP_TextElement_t129727469 * ___textElement_3;
	TMP_FontAsset_t364381626 * ___fontAsset_4;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_5;
	int32_t ___spriteIndex_6;
	Material_t340375123 * ___material_7;
	int32_t ___materialReferenceIndex_8;
	int32_t ___isUsingAlternateTypeface_9;
	float ___pointSize_10;
	int16_t ___lineNumber_11;
	int16_t ___pageNumber_12;
	int32_t ___vertexIndex_13;
	TMP_Vertex_t2404176824  ___vertex_TL_14;
	TMP_Vertex_t2404176824  ___vertex_BL_15;
	TMP_Vertex_t2404176824  ___vertex_TR_16;
	TMP_Vertex_t2404176824  ___vertex_BR_17;
	Vector3_t3722313464  ___topLeft_18;
	Vector3_t3722313464  ___bottomLeft_19;
	Vector3_t3722313464  ___topRight_20;
	Vector3_t3722313464  ___bottomRight_21;
	float ___origin_22;
	float ___ascender_23;
	float ___baseLine_24;
	float ___descender_25;
	float ___xAdvance_26;
	float ___aspectRatio_27;
	float ___scale_28;
	Color32_t2600501292  ___color_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	int32_t ___style_33;
	int32_t ___isVisible_34;
};
// Native definition for COM marshalling of TMPro.TMP_CharacterInfo
struct TMP_CharacterInfo_t3185626797_marshaled_com
{
	uint8_t ___character_0;
	int16_t ___index_1;
	int32_t ___elementType_2;
	TMP_TextElement_t129727469 * ___textElement_3;
	TMP_FontAsset_t364381626 * ___fontAsset_4;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_5;
	int32_t ___spriteIndex_6;
	Material_t340375123 * ___material_7;
	int32_t ___materialReferenceIndex_8;
	int32_t ___isUsingAlternateTypeface_9;
	float ___pointSize_10;
	int16_t ___lineNumber_11;
	int16_t ___pageNumber_12;
	int32_t ___vertexIndex_13;
	TMP_Vertex_t2404176824  ___vertex_TL_14;
	TMP_Vertex_t2404176824  ___vertex_BL_15;
	TMP_Vertex_t2404176824  ___vertex_TR_16;
	TMP_Vertex_t2404176824  ___vertex_BR_17;
	Vector3_t3722313464  ___topLeft_18;
	Vector3_t3722313464  ___bottomLeft_19;
	Vector3_t3722313464  ___topRight_20;
	Vector3_t3722313464  ___bottomRight_21;
	float ___origin_22;
	float ___ascender_23;
	float ___baseLine_24;
	float ___descender_25;
	float ___xAdvance_26;
	float ___aspectRatio_27;
	float ___scale_28;
	Color32_t2600501292  ___color_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	int32_t ___style_33;
	int32_t ___isVisible_34;
};
#endif // TMP_CHARACTERINFO_T3185626797_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef TMP_STYLESHEET_T917564226_H
#define TMP_STYLESHEET_T917564226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_StyleSheet
struct  TMP_StyleSheet_t917564226  : public ScriptableObject_t2528358522
{
public:
	// System.Collections.Generic.List`1<TMPro.TMP_Style> TMPro.TMP_StyleSheet::m_StyleList
	List_1_t656488210 * ___m_StyleList_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_Style> TMPro.TMP_StyleSheet::m_StyleDictionary
	Dictionary_2_t2368094095 * ___m_StyleDictionary_4;

public:
	inline static int32_t get_offset_of_m_StyleList_3() { return static_cast<int32_t>(offsetof(TMP_StyleSheet_t917564226, ___m_StyleList_3)); }
	inline List_1_t656488210 * get_m_StyleList_3() const { return ___m_StyleList_3; }
	inline List_1_t656488210 ** get_address_of_m_StyleList_3() { return &___m_StyleList_3; }
	inline void set_m_StyleList_3(List_1_t656488210 * value)
	{
		___m_StyleList_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_StyleList_3), value);
	}

	inline static int32_t get_offset_of_m_StyleDictionary_4() { return static_cast<int32_t>(offsetof(TMP_StyleSheet_t917564226, ___m_StyleDictionary_4)); }
	inline Dictionary_2_t2368094095 * get_m_StyleDictionary_4() const { return ___m_StyleDictionary_4; }
	inline Dictionary_2_t2368094095 ** get_address_of_m_StyleDictionary_4() { return &___m_StyleDictionary_4; }
	inline void set_m_StyleDictionary_4(Dictionary_2_t2368094095 * value)
	{
		___m_StyleDictionary_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_StyleDictionary_4), value);
	}
};

struct TMP_StyleSheet_t917564226_StaticFields
{
public:
	// TMPro.TMP_StyleSheet TMPro.TMP_StyleSheet::s_Instance
	TMP_StyleSheet_t917564226 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(TMP_StyleSheet_t917564226_StaticFields, ___s_Instance_2)); }
	inline TMP_StyleSheet_t917564226 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline TMP_StyleSheet_t917564226 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(TMP_StyleSheet_t917564226 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_STYLESHEET_T917564226_H
#ifndef GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#define GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct  GetRayIntersectionAllNonAllocCallback_t2311174851  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#ifndef U3CDOSPRITEANIMATIONINTERNALU3ED__7_T549195582_H
#define U3CDOSPRITEANIMATIONINTERNALU3ED__7_T549195582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7
struct  U3CDoSpriteAnimationInternalU3Ed__7_t549195582  : public RuntimeObject
{
public:
	// System.Int32 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Int32 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::currentCharacter
	int32_t ___currentCharacter_2;
	// TMPro.TMP_SpriteAsset TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::spriteAsset
	TMP_SpriteAsset_t484820633 * ___spriteAsset_3;
	// System.Int32 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::start
	int32_t ___start_4;
	// System.Int32 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::end
	int32_t ___end_5;
	// System.Int32 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::framerate
	int32_t ___framerate_6;
	// TMPro.TMP_SpriteAnimator TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::<>4__this
	TMP_SpriteAnimator_t2836635477 * ___U3CU3E4__this_7;
	// System.Int32 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::<currentFrame>5__1
	int32_t ___U3CcurrentFrameU3E5__1_8;
	// TMPro.TMP_CharacterInfo TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::<charInfo>5__2
	TMP_CharacterInfo_t3185626797  ___U3CcharInfoU3E5__2_9;
	// System.Int32 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::<materialIndex>5__3
	int32_t ___U3CmaterialIndexU3E5__3_10;
	// System.Int32 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::<vertexIndex>5__4
	int32_t ___U3CvertexIndexU3E5__4_11;
	// TMPro.TMP_MeshInfo TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::<meshInfo>5__5
	TMP_MeshInfo_t2771747634  ___U3CmeshInfoU3E5__5_12;
	// System.Single TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::<elapsedTime>5__6
	float ___U3CelapsedTimeU3E5__6_13;
	// System.Single TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::<targetTime>5__7
	float ___U3CtargetTimeU3E5__7_14;
	// TMPro.TMP_Sprite TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::<sprite>5__8
	TMP_Sprite_t554067146 * ___U3CspriteU3E5__8_15;
	// UnityEngine.Vector3[] TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::<vertices>5__9
	Vector3U5BU5D_t1718750761* ___U3CverticesU3E5__9_16;
	// UnityEngine.Vector2 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::<origin>5__10
	Vector2_t2156229523  ___U3CoriginU3E5__10_17;
	// System.Single TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::<spriteScale>5__11
	float ___U3CspriteScaleU3E5__11_18;
	// UnityEngine.Vector3 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::<bl>5__12
	Vector3_t3722313464  ___U3CblU3E5__12_19;
	// UnityEngine.Vector3 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::<tl>5__13
	Vector3_t3722313464  ___U3CtlU3E5__13_20;
	// UnityEngine.Vector3 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::<tr>5__14
	Vector3_t3722313464  ___U3CtrU3E5__14_21;
	// UnityEngine.Vector3 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::<br>5__15
	Vector3_t3722313464  ___U3CbrU3E5__15_22;
	// UnityEngine.Vector2[] TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::<uvs0>5__16
	Vector2U5BU5D_t1457185986* ___U3Cuvs0U3E5__16_23;
	// UnityEngine.Vector2 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::<uv0>5__17
	Vector2_t2156229523  ___U3Cuv0U3E5__17_24;
	// UnityEngine.Vector2 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::<uv1>5__18
	Vector2_t2156229523  ___U3Cuv1U3E5__18_25;
	// UnityEngine.Vector2 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::<uv2>5__19
	Vector2_t2156229523  ___U3Cuv2U3E5__19_26;
	// UnityEngine.Vector2 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>d__7::<uv3>5__20
	Vector2_t2156229523  ___U3Cuv3U3E5__20_27;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_currentCharacter_2() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___currentCharacter_2)); }
	inline int32_t get_currentCharacter_2() const { return ___currentCharacter_2; }
	inline int32_t* get_address_of_currentCharacter_2() { return &___currentCharacter_2; }
	inline void set_currentCharacter_2(int32_t value)
	{
		___currentCharacter_2 = value;
	}

	inline static int32_t get_offset_of_spriteAsset_3() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___spriteAsset_3)); }
	inline TMP_SpriteAsset_t484820633 * get_spriteAsset_3() const { return ___spriteAsset_3; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_spriteAsset_3() { return &___spriteAsset_3; }
	inline void set_spriteAsset_3(TMP_SpriteAsset_t484820633 * value)
	{
		___spriteAsset_3 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_3), value);
	}

	inline static int32_t get_offset_of_start_4() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___start_4)); }
	inline int32_t get_start_4() const { return ___start_4; }
	inline int32_t* get_address_of_start_4() { return &___start_4; }
	inline void set_start_4(int32_t value)
	{
		___start_4 = value;
	}

	inline static int32_t get_offset_of_end_5() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___end_5)); }
	inline int32_t get_end_5() const { return ___end_5; }
	inline int32_t* get_address_of_end_5() { return &___end_5; }
	inline void set_end_5(int32_t value)
	{
		___end_5 = value;
	}

	inline static int32_t get_offset_of_framerate_6() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___framerate_6)); }
	inline int32_t get_framerate_6() const { return ___framerate_6; }
	inline int32_t* get_address_of_framerate_6() { return &___framerate_6; }
	inline void set_framerate_6(int32_t value)
	{
		___framerate_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_7() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___U3CU3E4__this_7)); }
	inline TMP_SpriteAnimator_t2836635477 * get_U3CU3E4__this_7() const { return ___U3CU3E4__this_7; }
	inline TMP_SpriteAnimator_t2836635477 ** get_address_of_U3CU3E4__this_7() { return &___U3CU3E4__this_7; }
	inline void set_U3CU3E4__this_7(TMP_SpriteAnimator_t2836635477 * value)
	{
		___U3CU3E4__this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_7), value);
	}

	inline static int32_t get_offset_of_U3CcurrentFrameU3E5__1_8() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___U3CcurrentFrameU3E5__1_8)); }
	inline int32_t get_U3CcurrentFrameU3E5__1_8() const { return ___U3CcurrentFrameU3E5__1_8; }
	inline int32_t* get_address_of_U3CcurrentFrameU3E5__1_8() { return &___U3CcurrentFrameU3E5__1_8; }
	inline void set_U3CcurrentFrameU3E5__1_8(int32_t value)
	{
		___U3CcurrentFrameU3E5__1_8 = value;
	}

	inline static int32_t get_offset_of_U3CcharInfoU3E5__2_9() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___U3CcharInfoU3E5__2_9)); }
	inline TMP_CharacterInfo_t3185626797  get_U3CcharInfoU3E5__2_9() const { return ___U3CcharInfoU3E5__2_9; }
	inline TMP_CharacterInfo_t3185626797 * get_address_of_U3CcharInfoU3E5__2_9() { return &___U3CcharInfoU3E5__2_9; }
	inline void set_U3CcharInfoU3E5__2_9(TMP_CharacterInfo_t3185626797  value)
	{
		___U3CcharInfoU3E5__2_9 = value;
	}

	inline static int32_t get_offset_of_U3CmaterialIndexU3E5__3_10() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___U3CmaterialIndexU3E5__3_10)); }
	inline int32_t get_U3CmaterialIndexU3E5__3_10() const { return ___U3CmaterialIndexU3E5__3_10; }
	inline int32_t* get_address_of_U3CmaterialIndexU3E5__3_10() { return &___U3CmaterialIndexU3E5__3_10; }
	inline void set_U3CmaterialIndexU3E5__3_10(int32_t value)
	{
		___U3CmaterialIndexU3E5__3_10 = value;
	}

	inline static int32_t get_offset_of_U3CvertexIndexU3E5__4_11() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___U3CvertexIndexU3E5__4_11)); }
	inline int32_t get_U3CvertexIndexU3E5__4_11() const { return ___U3CvertexIndexU3E5__4_11; }
	inline int32_t* get_address_of_U3CvertexIndexU3E5__4_11() { return &___U3CvertexIndexU3E5__4_11; }
	inline void set_U3CvertexIndexU3E5__4_11(int32_t value)
	{
		___U3CvertexIndexU3E5__4_11 = value;
	}

	inline static int32_t get_offset_of_U3CmeshInfoU3E5__5_12() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___U3CmeshInfoU3E5__5_12)); }
	inline TMP_MeshInfo_t2771747634  get_U3CmeshInfoU3E5__5_12() const { return ___U3CmeshInfoU3E5__5_12; }
	inline TMP_MeshInfo_t2771747634 * get_address_of_U3CmeshInfoU3E5__5_12() { return &___U3CmeshInfoU3E5__5_12; }
	inline void set_U3CmeshInfoU3E5__5_12(TMP_MeshInfo_t2771747634  value)
	{
		___U3CmeshInfoU3E5__5_12 = value;
	}

	inline static int32_t get_offset_of_U3CelapsedTimeU3E5__6_13() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___U3CelapsedTimeU3E5__6_13)); }
	inline float get_U3CelapsedTimeU3E5__6_13() const { return ___U3CelapsedTimeU3E5__6_13; }
	inline float* get_address_of_U3CelapsedTimeU3E5__6_13() { return &___U3CelapsedTimeU3E5__6_13; }
	inline void set_U3CelapsedTimeU3E5__6_13(float value)
	{
		___U3CelapsedTimeU3E5__6_13 = value;
	}

	inline static int32_t get_offset_of_U3CtargetTimeU3E5__7_14() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___U3CtargetTimeU3E5__7_14)); }
	inline float get_U3CtargetTimeU3E5__7_14() const { return ___U3CtargetTimeU3E5__7_14; }
	inline float* get_address_of_U3CtargetTimeU3E5__7_14() { return &___U3CtargetTimeU3E5__7_14; }
	inline void set_U3CtargetTimeU3E5__7_14(float value)
	{
		___U3CtargetTimeU3E5__7_14 = value;
	}

	inline static int32_t get_offset_of_U3CspriteU3E5__8_15() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___U3CspriteU3E5__8_15)); }
	inline TMP_Sprite_t554067146 * get_U3CspriteU3E5__8_15() const { return ___U3CspriteU3E5__8_15; }
	inline TMP_Sprite_t554067146 ** get_address_of_U3CspriteU3E5__8_15() { return &___U3CspriteU3E5__8_15; }
	inline void set_U3CspriteU3E5__8_15(TMP_Sprite_t554067146 * value)
	{
		___U3CspriteU3E5__8_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CspriteU3E5__8_15), value);
	}

	inline static int32_t get_offset_of_U3CverticesU3E5__9_16() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___U3CverticesU3E5__9_16)); }
	inline Vector3U5BU5D_t1718750761* get_U3CverticesU3E5__9_16() const { return ___U3CverticesU3E5__9_16; }
	inline Vector3U5BU5D_t1718750761** get_address_of_U3CverticesU3E5__9_16() { return &___U3CverticesU3E5__9_16; }
	inline void set_U3CverticesU3E5__9_16(Vector3U5BU5D_t1718750761* value)
	{
		___U3CverticesU3E5__9_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CverticesU3E5__9_16), value);
	}

	inline static int32_t get_offset_of_U3CoriginU3E5__10_17() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___U3CoriginU3E5__10_17)); }
	inline Vector2_t2156229523  get_U3CoriginU3E5__10_17() const { return ___U3CoriginU3E5__10_17; }
	inline Vector2_t2156229523 * get_address_of_U3CoriginU3E5__10_17() { return &___U3CoriginU3E5__10_17; }
	inline void set_U3CoriginU3E5__10_17(Vector2_t2156229523  value)
	{
		___U3CoriginU3E5__10_17 = value;
	}

	inline static int32_t get_offset_of_U3CspriteScaleU3E5__11_18() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___U3CspriteScaleU3E5__11_18)); }
	inline float get_U3CspriteScaleU3E5__11_18() const { return ___U3CspriteScaleU3E5__11_18; }
	inline float* get_address_of_U3CspriteScaleU3E5__11_18() { return &___U3CspriteScaleU3E5__11_18; }
	inline void set_U3CspriteScaleU3E5__11_18(float value)
	{
		___U3CspriteScaleU3E5__11_18 = value;
	}

	inline static int32_t get_offset_of_U3CblU3E5__12_19() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___U3CblU3E5__12_19)); }
	inline Vector3_t3722313464  get_U3CblU3E5__12_19() const { return ___U3CblU3E5__12_19; }
	inline Vector3_t3722313464 * get_address_of_U3CblU3E5__12_19() { return &___U3CblU3E5__12_19; }
	inline void set_U3CblU3E5__12_19(Vector3_t3722313464  value)
	{
		___U3CblU3E5__12_19 = value;
	}

	inline static int32_t get_offset_of_U3CtlU3E5__13_20() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___U3CtlU3E5__13_20)); }
	inline Vector3_t3722313464  get_U3CtlU3E5__13_20() const { return ___U3CtlU3E5__13_20; }
	inline Vector3_t3722313464 * get_address_of_U3CtlU3E5__13_20() { return &___U3CtlU3E5__13_20; }
	inline void set_U3CtlU3E5__13_20(Vector3_t3722313464  value)
	{
		___U3CtlU3E5__13_20 = value;
	}

	inline static int32_t get_offset_of_U3CtrU3E5__14_21() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___U3CtrU3E5__14_21)); }
	inline Vector3_t3722313464  get_U3CtrU3E5__14_21() const { return ___U3CtrU3E5__14_21; }
	inline Vector3_t3722313464 * get_address_of_U3CtrU3E5__14_21() { return &___U3CtrU3E5__14_21; }
	inline void set_U3CtrU3E5__14_21(Vector3_t3722313464  value)
	{
		___U3CtrU3E5__14_21 = value;
	}

	inline static int32_t get_offset_of_U3CbrU3E5__15_22() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___U3CbrU3E5__15_22)); }
	inline Vector3_t3722313464  get_U3CbrU3E5__15_22() const { return ___U3CbrU3E5__15_22; }
	inline Vector3_t3722313464 * get_address_of_U3CbrU3E5__15_22() { return &___U3CbrU3E5__15_22; }
	inline void set_U3CbrU3E5__15_22(Vector3_t3722313464  value)
	{
		___U3CbrU3E5__15_22 = value;
	}

	inline static int32_t get_offset_of_U3Cuvs0U3E5__16_23() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___U3Cuvs0U3E5__16_23)); }
	inline Vector2U5BU5D_t1457185986* get_U3Cuvs0U3E5__16_23() const { return ___U3Cuvs0U3E5__16_23; }
	inline Vector2U5BU5D_t1457185986** get_address_of_U3Cuvs0U3E5__16_23() { return &___U3Cuvs0U3E5__16_23; }
	inline void set_U3Cuvs0U3E5__16_23(Vector2U5BU5D_t1457185986* value)
	{
		___U3Cuvs0U3E5__16_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cuvs0U3E5__16_23), value);
	}

	inline static int32_t get_offset_of_U3Cuv0U3E5__17_24() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___U3Cuv0U3E5__17_24)); }
	inline Vector2_t2156229523  get_U3Cuv0U3E5__17_24() const { return ___U3Cuv0U3E5__17_24; }
	inline Vector2_t2156229523 * get_address_of_U3Cuv0U3E5__17_24() { return &___U3Cuv0U3E5__17_24; }
	inline void set_U3Cuv0U3E5__17_24(Vector2_t2156229523  value)
	{
		___U3Cuv0U3E5__17_24 = value;
	}

	inline static int32_t get_offset_of_U3Cuv1U3E5__18_25() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___U3Cuv1U3E5__18_25)); }
	inline Vector2_t2156229523  get_U3Cuv1U3E5__18_25() const { return ___U3Cuv1U3E5__18_25; }
	inline Vector2_t2156229523 * get_address_of_U3Cuv1U3E5__18_25() { return &___U3Cuv1U3E5__18_25; }
	inline void set_U3Cuv1U3E5__18_25(Vector2_t2156229523  value)
	{
		___U3Cuv1U3E5__18_25 = value;
	}

	inline static int32_t get_offset_of_U3Cuv2U3E5__19_26() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___U3Cuv2U3E5__19_26)); }
	inline Vector2_t2156229523  get_U3Cuv2U3E5__19_26() const { return ___U3Cuv2U3E5__19_26; }
	inline Vector2_t2156229523 * get_address_of_U3Cuv2U3E5__19_26() { return &___U3Cuv2U3E5__19_26; }
	inline void set_U3Cuv2U3E5__19_26(Vector2_t2156229523  value)
	{
		___U3Cuv2U3E5__19_26 = value;
	}

	inline static int32_t get_offset_of_U3Cuv3U3E5__20_27() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t549195582, ___U3Cuv3U3E5__20_27)); }
	inline Vector2_t2156229523  get_U3Cuv3U3E5__20_27() const { return ___U3Cuv3U3E5__20_27; }
	inline Vector2_t2156229523 * get_address_of_U3Cuv3U3E5__20_27() { return &___U3Cuv3U3E5__20_27; }
	inline void set_U3Cuv3U3E5__20_27(Vector2_t2156229523  value)
	{
		___U3Cuv3U3E5__20_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOSPRITEANIMATIONINTERNALU3ED__7_T549195582_H
#ifndef TMP_SETTINGS_T2071156274_H
#define TMP_SETTINGS_T2071156274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Settings
struct  TMP_Settings_t2071156274  : public ScriptableObject_t2528358522
{
public:
	// System.Boolean TMPro.TMP_Settings::m_enableWordWrapping
	bool ___m_enableWordWrapping_3;
	// System.Boolean TMPro.TMP_Settings::m_enableKerning
	bool ___m_enableKerning_4;
	// System.Boolean TMPro.TMP_Settings::m_enableExtraPadding
	bool ___m_enableExtraPadding_5;
	// System.Boolean TMPro.TMP_Settings::m_enableTintAllSprites
	bool ___m_enableTintAllSprites_6;
	// System.Boolean TMPro.TMP_Settings::m_enableParseEscapeCharacters
	bool ___m_enableParseEscapeCharacters_7;
	// System.Int32 TMPro.TMP_Settings::m_missingGlyphCharacter
	int32_t ___m_missingGlyphCharacter_8;
	// System.Boolean TMPro.TMP_Settings::m_warningsDisabled
	bool ___m_warningsDisabled_9;
	// TMPro.TMP_FontAsset TMPro.TMP_Settings::m_defaultFontAsset
	TMP_FontAsset_t364381626 * ___m_defaultFontAsset_10;
	// System.String TMPro.TMP_Settings::m_defaultFontAssetPath
	String_t* ___m_defaultFontAssetPath_11;
	// System.Single TMPro.TMP_Settings::m_defaultFontSize
	float ___m_defaultFontSize_12;
	// System.Single TMPro.TMP_Settings::m_defaultAutoSizeMinRatio
	float ___m_defaultAutoSizeMinRatio_13;
	// System.Single TMPro.TMP_Settings::m_defaultAutoSizeMaxRatio
	float ___m_defaultAutoSizeMaxRatio_14;
	// UnityEngine.Vector2 TMPro.TMP_Settings::m_defaultTextMeshProTextContainerSize
	Vector2_t2156229523  ___m_defaultTextMeshProTextContainerSize_15;
	// UnityEngine.Vector2 TMPro.TMP_Settings::m_defaultTextMeshProUITextContainerSize
	Vector2_t2156229523  ___m_defaultTextMeshProUITextContainerSize_16;
	// System.Boolean TMPro.TMP_Settings::m_autoSizeTextContainer
	bool ___m_autoSizeTextContainer_17;
	// System.Collections.Generic.List`1<TMPro.TMP_FontAsset> TMPro.TMP_Settings::m_fallbackFontAssets
	List_1_t1836456368 * ___m_fallbackFontAssets_18;
	// System.Boolean TMPro.TMP_Settings::m_matchMaterialPreset
	bool ___m_matchMaterialPreset_19;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Settings::m_defaultSpriteAsset
	TMP_SpriteAsset_t484820633 * ___m_defaultSpriteAsset_20;
	// System.String TMPro.TMP_Settings::m_defaultSpriteAssetPath
	String_t* ___m_defaultSpriteAssetPath_21;
	// System.Boolean TMPro.TMP_Settings::m_enableEmojiSupport
	bool ___m_enableEmojiSupport_22;
	// TMPro.TMP_StyleSheet TMPro.TMP_Settings::m_defaultStyleSheet
	TMP_StyleSheet_t917564226 * ___m_defaultStyleSheet_23;
	// UnityEngine.TextAsset TMPro.TMP_Settings::m_leadingCharacters
	TextAsset_t3022178571 * ___m_leadingCharacters_24;
	// UnityEngine.TextAsset TMPro.TMP_Settings::m_followingCharacters
	TextAsset_t3022178571 * ___m_followingCharacters_25;
	// TMPro.TMP_Settings/LineBreakingTable TMPro.TMP_Settings::m_linebreakingRules
	LineBreakingTable_t1457697482 * ___m_linebreakingRules_26;

public:
	inline static int32_t get_offset_of_m_enableWordWrapping_3() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_enableWordWrapping_3)); }
	inline bool get_m_enableWordWrapping_3() const { return ___m_enableWordWrapping_3; }
	inline bool* get_address_of_m_enableWordWrapping_3() { return &___m_enableWordWrapping_3; }
	inline void set_m_enableWordWrapping_3(bool value)
	{
		___m_enableWordWrapping_3 = value;
	}

	inline static int32_t get_offset_of_m_enableKerning_4() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_enableKerning_4)); }
	inline bool get_m_enableKerning_4() const { return ___m_enableKerning_4; }
	inline bool* get_address_of_m_enableKerning_4() { return &___m_enableKerning_4; }
	inline void set_m_enableKerning_4(bool value)
	{
		___m_enableKerning_4 = value;
	}

	inline static int32_t get_offset_of_m_enableExtraPadding_5() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_enableExtraPadding_5)); }
	inline bool get_m_enableExtraPadding_5() const { return ___m_enableExtraPadding_5; }
	inline bool* get_address_of_m_enableExtraPadding_5() { return &___m_enableExtraPadding_5; }
	inline void set_m_enableExtraPadding_5(bool value)
	{
		___m_enableExtraPadding_5 = value;
	}

	inline static int32_t get_offset_of_m_enableTintAllSprites_6() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_enableTintAllSprites_6)); }
	inline bool get_m_enableTintAllSprites_6() const { return ___m_enableTintAllSprites_6; }
	inline bool* get_address_of_m_enableTintAllSprites_6() { return &___m_enableTintAllSprites_6; }
	inline void set_m_enableTintAllSprites_6(bool value)
	{
		___m_enableTintAllSprites_6 = value;
	}

	inline static int32_t get_offset_of_m_enableParseEscapeCharacters_7() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_enableParseEscapeCharacters_7)); }
	inline bool get_m_enableParseEscapeCharacters_7() const { return ___m_enableParseEscapeCharacters_7; }
	inline bool* get_address_of_m_enableParseEscapeCharacters_7() { return &___m_enableParseEscapeCharacters_7; }
	inline void set_m_enableParseEscapeCharacters_7(bool value)
	{
		___m_enableParseEscapeCharacters_7 = value;
	}

	inline static int32_t get_offset_of_m_missingGlyphCharacter_8() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_missingGlyphCharacter_8)); }
	inline int32_t get_m_missingGlyphCharacter_8() const { return ___m_missingGlyphCharacter_8; }
	inline int32_t* get_address_of_m_missingGlyphCharacter_8() { return &___m_missingGlyphCharacter_8; }
	inline void set_m_missingGlyphCharacter_8(int32_t value)
	{
		___m_missingGlyphCharacter_8 = value;
	}

	inline static int32_t get_offset_of_m_warningsDisabled_9() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_warningsDisabled_9)); }
	inline bool get_m_warningsDisabled_9() const { return ___m_warningsDisabled_9; }
	inline bool* get_address_of_m_warningsDisabled_9() { return &___m_warningsDisabled_9; }
	inline void set_m_warningsDisabled_9(bool value)
	{
		___m_warningsDisabled_9 = value;
	}

	inline static int32_t get_offset_of_m_defaultFontAsset_10() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_defaultFontAsset_10)); }
	inline TMP_FontAsset_t364381626 * get_m_defaultFontAsset_10() const { return ___m_defaultFontAsset_10; }
	inline TMP_FontAsset_t364381626 ** get_address_of_m_defaultFontAsset_10() { return &___m_defaultFontAsset_10; }
	inline void set_m_defaultFontAsset_10(TMP_FontAsset_t364381626 * value)
	{
		___m_defaultFontAsset_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultFontAsset_10), value);
	}

	inline static int32_t get_offset_of_m_defaultFontAssetPath_11() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_defaultFontAssetPath_11)); }
	inline String_t* get_m_defaultFontAssetPath_11() const { return ___m_defaultFontAssetPath_11; }
	inline String_t** get_address_of_m_defaultFontAssetPath_11() { return &___m_defaultFontAssetPath_11; }
	inline void set_m_defaultFontAssetPath_11(String_t* value)
	{
		___m_defaultFontAssetPath_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultFontAssetPath_11), value);
	}

	inline static int32_t get_offset_of_m_defaultFontSize_12() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_defaultFontSize_12)); }
	inline float get_m_defaultFontSize_12() const { return ___m_defaultFontSize_12; }
	inline float* get_address_of_m_defaultFontSize_12() { return &___m_defaultFontSize_12; }
	inline void set_m_defaultFontSize_12(float value)
	{
		___m_defaultFontSize_12 = value;
	}

	inline static int32_t get_offset_of_m_defaultAutoSizeMinRatio_13() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_defaultAutoSizeMinRatio_13)); }
	inline float get_m_defaultAutoSizeMinRatio_13() const { return ___m_defaultAutoSizeMinRatio_13; }
	inline float* get_address_of_m_defaultAutoSizeMinRatio_13() { return &___m_defaultAutoSizeMinRatio_13; }
	inline void set_m_defaultAutoSizeMinRatio_13(float value)
	{
		___m_defaultAutoSizeMinRatio_13 = value;
	}

	inline static int32_t get_offset_of_m_defaultAutoSizeMaxRatio_14() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_defaultAutoSizeMaxRatio_14)); }
	inline float get_m_defaultAutoSizeMaxRatio_14() const { return ___m_defaultAutoSizeMaxRatio_14; }
	inline float* get_address_of_m_defaultAutoSizeMaxRatio_14() { return &___m_defaultAutoSizeMaxRatio_14; }
	inline void set_m_defaultAutoSizeMaxRatio_14(float value)
	{
		___m_defaultAutoSizeMaxRatio_14 = value;
	}

	inline static int32_t get_offset_of_m_defaultTextMeshProTextContainerSize_15() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_defaultTextMeshProTextContainerSize_15)); }
	inline Vector2_t2156229523  get_m_defaultTextMeshProTextContainerSize_15() const { return ___m_defaultTextMeshProTextContainerSize_15; }
	inline Vector2_t2156229523 * get_address_of_m_defaultTextMeshProTextContainerSize_15() { return &___m_defaultTextMeshProTextContainerSize_15; }
	inline void set_m_defaultTextMeshProTextContainerSize_15(Vector2_t2156229523  value)
	{
		___m_defaultTextMeshProTextContainerSize_15 = value;
	}

	inline static int32_t get_offset_of_m_defaultTextMeshProUITextContainerSize_16() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_defaultTextMeshProUITextContainerSize_16)); }
	inline Vector2_t2156229523  get_m_defaultTextMeshProUITextContainerSize_16() const { return ___m_defaultTextMeshProUITextContainerSize_16; }
	inline Vector2_t2156229523 * get_address_of_m_defaultTextMeshProUITextContainerSize_16() { return &___m_defaultTextMeshProUITextContainerSize_16; }
	inline void set_m_defaultTextMeshProUITextContainerSize_16(Vector2_t2156229523  value)
	{
		___m_defaultTextMeshProUITextContainerSize_16 = value;
	}

	inline static int32_t get_offset_of_m_autoSizeTextContainer_17() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_autoSizeTextContainer_17)); }
	inline bool get_m_autoSizeTextContainer_17() const { return ___m_autoSizeTextContainer_17; }
	inline bool* get_address_of_m_autoSizeTextContainer_17() { return &___m_autoSizeTextContainer_17; }
	inline void set_m_autoSizeTextContainer_17(bool value)
	{
		___m_autoSizeTextContainer_17 = value;
	}

	inline static int32_t get_offset_of_m_fallbackFontAssets_18() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_fallbackFontAssets_18)); }
	inline List_1_t1836456368 * get_m_fallbackFontAssets_18() const { return ___m_fallbackFontAssets_18; }
	inline List_1_t1836456368 ** get_address_of_m_fallbackFontAssets_18() { return &___m_fallbackFontAssets_18; }
	inline void set_m_fallbackFontAssets_18(List_1_t1836456368 * value)
	{
		___m_fallbackFontAssets_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackFontAssets_18), value);
	}

	inline static int32_t get_offset_of_m_matchMaterialPreset_19() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_matchMaterialPreset_19)); }
	inline bool get_m_matchMaterialPreset_19() const { return ___m_matchMaterialPreset_19; }
	inline bool* get_address_of_m_matchMaterialPreset_19() { return &___m_matchMaterialPreset_19; }
	inline void set_m_matchMaterialPreset_19(bool value)
	{
		___m_matchMaterialPreset_19 = value;
	}

	inline static int32_t get_offset_of_m_defaultSpriteAsset_20() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_defaultSpriteAsset_20)); }
	inline TMP_SpriteAsset_t484820633 * get_m_defaultSpriteAsset_20() const { return ___m_defaultSpriteAsset_20; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_defaultSpriteAsset_20() { return &___m_defaultSpriteAsset_20; }
	inline void set_m_defaultSpriteAsset_20(TMP_SpriteAsset_t484820633 * value)
	{
		___m_defaultSpriteAsset_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultSpriteAsset_20), value);
	}

	inline static int32_t get_offset_of_m_defaultSpriteAssetPath_21() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_defaultSpriteAssetPath_21)); }
	inline String_t* get_m_defaultSpriteAssetPath_21() const { return ___m_defaultSpriteAssetPath_21; }
	inline String_t** get_address_of_m_defaultSpriteAssetPath_21() { return &___m_defaultSpriteAssetPath_21; }
	inline void set_m_defaultSpriteAssetPath_21(String_t* value)
	{
		___m_defaultSpriteAssetPath_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultSpriteAssetPath_21), value);
	}

	inline static int32_t get_offset_of_m_enableEmojiSupport_22() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_enableEmojiSupport_22)); }
	inline bool get_m_enableEmojiSupport_22() const { return ___m_enableEmojiSupport_22; }
	inline bool* get_address_of_m_enableEmojiSupport_22() { return &___m_enableEmojiSupport_22; }
	inline void set_m_enableEmojiSupport_22(bool value)
	{
		___m_enableEmojiSupport_22 = value;
	}

	inline static int32_t get_offset_of_m_defaultStyleSheet_23() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_defaultStyleSheet_23)); }
	inline TMP_StyleSheet_t917564226 * get_m_defaultStyleSheet_23() const { return ___m_defaultStyleSheet_23; }
	inline TMP_StyleSheet_t917564226 ** get_address_of_m_defaultStyleSheet_23() { return &___m_defaultStyleSheet_23; }
	inline void set_m_defaultStyleSheet_23(TMP_StyleSheet_t917564226 * value)
	{
		___m_defaultStyleSheet_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultStyleSheet_23), value);
	}

	inline static int32_t get_offset_of_m_leadingCharacters_24() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_leadingCharacters_24)); }
	inline TextAsset_t3022178571 * get_m_leadingCharacters_24() const { return ___m_leadingCharacters_24; }
	inline TextAsset_t3022178571 ** get_address_of_m_leadingCharacters_24() { return &___m_leadingCharacters_24; }
	inline void set_m_leadingCharacters_24(TextAsset_t3022178571 * value)
	{
		___m_leadingCharacters_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_leadingCharacters_24), value);
	}

	inline static int32_t get_offset_of_m_followingCharacters_25() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_followingCharacters_25)); }
	inline TextAsset_t3022178571 * get_m_followingCharacters_25() const { return ___m_followingCharacters_25; }
	inline TextAsset_t3022178571 ** get_address_of_m_followingCharacters_25() { return &___m_followingCharacters_25; }
	inline void set_m_followingCharacters_25(TextAsset_t3022178571 * value)
	{
		___m_followingCharacters_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_followingCharacters_25), value);
	}

	inline static int32_t get_offset_of_m_linebreakingRules_26() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_linebreakingRules_26)); }
	inline LineBreakingTable_t1457697482 * get_m_linebreakingRules_26() const { return ___m_linebreakingRules_26; }
	inline LineBreakingTable_t1457697482 ** get_address_of_m_linebreakingRules_26() { return &___m_linebreakingRules_26; }
	inline void set_m_linebreakingRules_26(LineBreakingTable_t1457697482 * value)
	{
		___m_linebreakingRules_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_linebreakingRules_26), value);
	}
};

struct TMP_Settings_t2071156274_StaticFields
{
public:
	// TMPro.TMP_Settings TMPro.TMP_Settings::s_Instance
	TMP_Settings_t2071156274 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274_StaticFields, ___s_Instance_2)); }
	inline TMP_Settings_t2071156274 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline TMP_Settings_t2071156274 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(TMP_Settings_t2071156274 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SETTINGS_T2071156274_H
#ifndef TMP_INPUTVALIDATOR_T1385053824_H
#define TMP_INPUTVALIDATOR_T1385053824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputValidator
struct  TMP_InputValidator_t1385053824  : public ScriptableObject_t2528358522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_INPUTVALIDATOR_T1385053824_H
#ifndef GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#define GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct  GetRaycastNonAllocCallback_t3841783507  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#ifndef ONVALIDATEINPUT_T373909109_H
#define ONVALIDATEINPUT_T373909109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/OnValidateInput
struct  OnValidateInput_t373909109  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONVALIDATEINPUT_T373909109_H
#ifndef TMP_COLORGRADIENT_T3678055768_H
#define TMP_COLORGRADIENT_T3678055768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_ColorGradient
struct  TMP_ColorGradient_t3678055768  : public ScriptableObject_t2528358522
{
public:
	// UnityEngine.Color TMPro.TMP_ColorGradient::topLeft
	Color_t2555686324  ___topLeft_2;
	// UnityEngine.Color TMPro.TMP_ColorGradient::topRight
	Color_t2555686324  ___topRight_3;
	// UnityEngine.Color TMPro.TMP_ColorGradient::bottomLeft
	Color_t2555686324  ___bottomLeft_4;
	// UnityEngine.Color TMPro.TMP_ColorGradient::bottomRight
	Color_t2555686324  ___bottomRight_5;

public:
	inline static int32_t get_offset_of_topLeft_2() { return static_cast<int32_t>(offsetof(TMP_ColorGradient_t3678055768, ___topLeft_2)); }
	inline Color_t2555686324  get_topLeft_2() const { return ___topLeft_2; }
	inline Color_t2555686324 * get_address_of_topLeft_2() { return &___topLeft_2; }
	inline void set_topLeft_2(Color_t2555686324  value)
	{
		___topLeft_2 = value;
	}

	inline static int32_t get_offset_of_topRight_3() { return static_cast<int32_t>(offsetof(TMP_ColorGradient_t3678055768, ___topRight_3)); }
	inline Color_t2555686324  get_topRight_3() const { return ___topRight_3; }
	inline Color_t2555686324 * get_address_of_topRight_3() { return &___topRight_3; }
	inline void set_topRight_3(Color_t2555686324  value)
	{
		___topRight_3 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_4() { return static_cast<int32_t>(offsetof(TMP_ColorGradient_t3678055768, ___bottomLeft_4)); }
	inline Color_t2555686324  get_bottomLeft_4() const { return ___bottomLeft_4; }
	inline Color_t2555686324 * get_address_of_bottomLeft_4() { return &___bottomLeft_4; }
	inline void set_bottomLeft_4(Color_t2555686324  value)
	{
		___bottomLeft_4 = value;
	}

	inline static int32_t get_offset_of_bottomRight_5() { return static_cast<int32_t>(offsetof(TMP_ColorGradient_t3678055768, ___bottomRight_5)); }
	inline Color_t2555686324  get_bottomRight_5() const { return ___bottomRight_5; }
	inline Color_t2555686324 * get_address_of_bottomRight_5() { return &___bottomRight_5; }
	inline void set_bottomRight_5(Color_t2555686324  value)
	{
		___bottomRight_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_COLORGRADIENT_T3678055768_H
#ifndef TMP_ASSET_T2469957285_H
#define TMP_ASSET_T2469957285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Asset
struct  TMP_Asset_t2469957285  : public ScriptableObject_t2528358522
{
public:
	// System.Int32 TMPro.TMP_Asset::hashCode
	int32_t ___hashCode_2;
	// UnityEngine.Material TMPro.TMP_Asset::material
	Material_t340375123 * ___material_3;
	// System.Int32 TMPro.TMP_Asset::materialHashCode
	int32_t ___materialHashCode_4;

public:
	inline static int32_t get_offset_of_hashCode_2() { return static_cast<int32_t>(offsetof(TMP_Asset_t2469957285, ___hashCode_2)); }
	inline int32_t get_hashCode_2() const { return ___hashCode_2; }
	inline int32_t* get_address_of_hashCode_2() { return &___hashCode_2; }
	inline void set_hashCode_2(int32_t value)
	{
		___hashCode_2 = value;
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(TMP_Asset_t2469957285, ___material_3)); }
	inline Material_t340375123 * get_material_3() const { return ___material_3; }
	inline Material_t340375123 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_t340375123 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_materialHashCode_4() { return static_cast<int32_t>(offsetof(TMP_Asset_t2469957285, ___materialHashCode_4)); }
	inline int32_t get_materialHashCode_4() const { return ___materialHashCode_4; }
	inline int32_t* get_address_of_materialHashCode_4() { return &___materialHashCode_4; }
	inline void set_materialHashCode_4(int32_t value)
	{
		___materialHashCode_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_ASSET_T2469957285_H
#ifndef TMP_FONTASSET_T364381626_H
#define TMP_FONTASSET_T364381626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontAsset
struct  TMP_FontAsset_t364381626  : public TMP_Asset_t2469957285
{
public:
	// TMPro.TMP_FontAsset/FontAssetTypes TMPro.TMP_FontAsset::fontAssetType
	int32_t ___fontAssetType_6;
	// TMPro.FaceInfo TMPro.TMP_FontAsset::m_fontInfo
	FaceInfo_t2243299176 * ___m_fontInfo_7;
	// UnityEngine.Texture2D TMPro.TMP_FontAsset::atlas
	Texture2D_t3840446185 * ___atlas_8;
	// System.Collections.Generic.List`1<TMPro.TMP_Glyph> TMPro.TMP_FontAsset::m_glyphInfoList
	List_1_t2053922575 * ___m_glyphInfoList_9;
	// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_Glyph> TMPro.TMP_FontAsset::m_characterDictionary
	Dictionary_2_t3765528460 * ___m_characterDictionary_10;
	// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.KerningPair> TMPro.TMP_FontAsset::m_kerningDictionary
	Dictionary_2_t1159568920 * ___m_kerningDictionary_11;
	// TMPro.KerningTable TMPro.TMP_FontAsset::m_kerningInfo
	KerningTable_t2322366871 * ___m_kerningInfo_12;
	// TMPro.KerningPair TMPro.TMP_FontAsset::m_kerningPair
	KerningPair_t2270855589 * ___m_kerningPair_13;
	// System.Collections.Generic.List`1<TMPro.TMP_FontAsset> TMPro.TMP_FontAsset::fallbackFontAssets
	List_1_t1836456368 * ___fallbackFontAssets_14;
	// TMPro.FontCreationSetting TMPro.TMP_FontAsset::fontCreationSettings
	FontCreationSetting_t628772060  ___fontCreationSettings_15;
	// TMPro.TMP_FontWeights[] TMPro.TMP_FontAsset::fontWeights
	TMP_FontWeightsU5BU5D_t3691718250* ___fontWeights_16;
	// System.Int32[] TMPro.TMP_FontAsset::m_characterSet
	Int32U5BU5D_t385246372* ___m_characterSet_17;
	// System.Single TMPro.TMP_FontAsset::normalStyle
	float ___normalStyle_18;
	// System.Single TMPro.TMP_FontAsset::normalSpacingOffset
	float ___normalSpacingOffset_19;
	// System.Single TMPro.TMP_FontAsset::boldStyle
	float ___boldStyle_20;
	// System.Single TMPro.TMP_FontAsset::boldSpacing
	float ___boldSpacing_21;
	// System.Byte TMPro.TMP_FontAsset::italicStyle
	uint8_t ___italicStyle_22;
	// System.Byte TMPro.TMP_FontAsset::tabSize
	uint8_t ___tabSize_23;
	// System.Byte TMPro.TMP_FontAsset::m_oldTabSize
	uint8_t ___m_oldTabSize_24;

public:
	inline static int32_t get_offset_of_fontAssetType_6() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t364381626, ___fontAssetType_6)); }
	inline int32_t get_fontAssetType_6() const { return ___fontAssetType_6; }
	inline int32_t* get_address_of_fontAssetType_6() { return &___fontAssetType_6; }
	inline void set_fontAssetType_6(int32_t value)
	{
		___fontAssetType_6 = value;
	}

	inline static int32_t get_offset_of_m_fontInfo_7() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t364381626, ___m_fontInfo_7)); }
	inline FaceInfo_t2243299176 * get_m_fontInfo_7() const { return ___m_fontInfo_7; }
	inline FaceInfo_t2243299176 ** get_address_of_m_fontInfo_7() { return &___m_fontInfo_7; }
	inline void set_m_fontInfo_7(FaceInfo_t2243299176 * value)
	{
		___m_fontInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontInfo_7), value);
	}

	inline static int32_t get_offset_of_atlas_8() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t364381626, ___atlas_8)); }
	inline Texture2D_t3840446185 * get_atlas_8() const { return ___atlas_8; }
	inline Texture2D_t3840446185 ** get_address_of_atlas_8() { return &___atlas_8; }
	inline void set_atlas_8(Texture2D_t3840446185 * value)
	{
		___atlas_8 = value;
		Il2CppCodeGenWriteBarrier((&___atlas_8), value);
	}

	inline static int32_t get_offset_of_m_glyphInfoList_9() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t364381626, ___m_glyphInfoList_9)); }
	inline List_1_t2053922575 * get_m_glyphInfoList_9() const { return ___m_glyphInfoList_9; }
	inline List_1_t2053922575 ** get_address_of_m_glyphInfoList_9() { return &___m_glyphInfoList_9; }
	inline void set_m_glyphInfoList_9(List_1_t2053922575 * value)
	{
		___m_glyphInfoList_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_glyphInfoList_9), value);
	}

	inline static int32_t get_offset_of_m_characterDictionary_10() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t364381626, ___m_characterDictionary_10)); }
	inline Dictionary_2_t3765528460 * get_m_characterDictionary_10() const { return ___m_characterDictionary_10; }
	inline Dictionary_2_t3765528460 ** get_address_of_m_characterDictionary_10() { return &___m_characterDictionary_10; }
	inline void set_m_characterDictionary_10(Dictionary_2_t3765528460 * value)
	{
		___m_characterDictionary_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_characterDictionary_10), value);
	}

	inline static int32_t get_offset_of_m_kerningDictionary_11() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t364381626, ___m_kerningDictionary_11)); }
	inline Dictionary_2_t1159568920 * get_m_kerningDictionary_11() const { return ___m_kerningDictionary_11; }
	inline Dictionary_2_t1159568920 ** get_address_of_m_kerningDictionary_11() { return &___m_kerningDictionary_11; }
	inline void set_m_kerningDictionary_11(Dictionary_2_t1159568920 * value)
	{
		___m_kerningDictionary_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_kerningDictionary_11), value);
	}

	inline static int32_t get_offset_of_m_kerningInfo_12() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t364381626, ___m_kerningInfo_12)); }
	inline KerningTable_t2322366871 * get_m_kerningInfo_12() const { return ___m_kerningInfo_12; }
	inline KerningTable_t2322366871 ** get_address_of_m_kerningInfo_12() { return &___m_kerningInfo_12; }
	inline void set_m_kerningInfo_12(KerningTable_t2322366871 * value)
	{
		___m_kerningInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_kerningInfo_12), value);
	}

	inline static int32_t get_offset_of_m_kerningPair_13() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t364381626, ___m_kerningPair_13)); }
	inline KerningPair_t2270855589 * get_m_kerningPair_13() const { return ___m_kerningPair_13; }
	inline KerningPair_t2270855589 ** get_address_of_m_kerningPair_13() { return &___m_kerningPair_13; }
	inline void set_m_kerningPair_13(KerningPair_t2270855589 * value)
	{
		___m_kerningPair_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_kerningPair_13), value);
	}

	inline static int32_t get_offset_of_fallbackFontAssets_14() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t364381626, ___fallbackFontAssets_14)); }
	inline List_1_t1836456368 * get_fallbackFontAssets_14() const { return ___fallbackFontAssets_14; }
	inline List_1_t1836456368 ** get_address_of_fallbackFontAssets_14() { return &___fallbackFontAssets_14; }
	inline void set_fallbackFontAssets_14(List_1_t1836456368 * value)
	{
		___fallbackFontAssets_14 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackFontAssets_14), value);
	}

	inline static int32_t get_offset_of_fontCreationSettings_15() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t364381626, ___fontCreationSettings_15)); }
	inline FontCreationSetting_t628772060  get_fontCreationSettings_15() const { return ___fontCreationSettings_15; }
	inline FontCreationSetting_t628772060 * get_address_of_fontCreationSettings_15() { return &___fontCreationSettings_15; }
	inline void set_fontCreationSettings_15(FontCreationSetting_t628772060  value)
	{
		___fontCreationSettings_15 = value;
	}

	inline static int32_t get_offset_of_fontWeights_16() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t364381626, ___fontWeights_16)); }
	inline TMP_FontWeightsU5BU5D_t3691718250* get_fontWeights_16() const { return ___fontWeights_16; }
	inline TMP_FontWeightsU5BU5D_t3691718250** get_address_of_fontWeights_16() { return &___fontWeights_16; }
	inline void set_fontWeights_16(TMP_FontWeightsU5BU5D_t3691718250* value)
	{
		___fontWeights_16 = value;
		Il2CppCodeGenWriteBarrier((&___fontWeights_16), value);
	}

	inline static int32_t get_offset_of_m_characterSet_17() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t364381626, ___m_characterSet_17)); }
	inline Int32U5BU5D_t385246372* get_m_characterSet_17() const { return ___m_characterSet_17; }
	inline Int32U5BU5D_t385246372** get_address_of_m_characterSet_17() { return &___m_characterSet_17; }
	inline void set_m_characterSet_17(Int32U5BU5D_t385246372* value)
	{
		___m_characterSet_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_characterSet_17), value);
	}

	inline static int32_t get_offset_of_normalStyle_18() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t364381626, ___normalStyle_18)); }
	inline float get_normalStyle_18() const { return ___normalStyle_18; }
	inline float* get_address_of_normalStyle_18() { return &___normalStyle_18; }
	inline void set_normalStyle_18(float value)
	{
		___normalStyle_18 = value;
	}

	inline static int32_t get_offset_of_normalSpacingOffset_19() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t364381626, ___normalSpacingOffset_19)); }
	inline float get_normalSpacingOffset_19() const { return ___normalSpacingOffset_19; }
	inline float* get_address_of_normalSpacingOffset_19() { return &___normalSpacingOffset_19; }
	inline void set_normalSpacingOffset_19(float value)
	{
		___normalSpacingOffset_19 = value;
	}

	inline static int32_t get_offset_of_boldStyle_20() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t364381626, ___boldStyle_20)); }
	inline float get_boldStyle_20() const { return ___boldStyle_20; }
	inline float* get_address_of_boldStyle_20() { return &___boldStyle_20; }
	inline void set_boldStyle_20(float value)
	{
		___boldStyle_20 = value;
	}

	inline static int32_t get_offset_of_boldSpacing_21() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t364381626, ___boldSpacing_21)); }
	inline float get_boldSpacing_21() const { return ___boldSpacing_21; }
	inline float* get_address_of_boldSpacing_21() { return &___boldSpacing_21; }
	inline void set_boldSpacing_21(float value)
	{
		___boldSpacing_21 = value;
	}

	inline static int32_t get_offset_of_italicStyle_22() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t364381626, ___italicStyle_22)); }
	inline uint8_t get_italicStyle_22() const { return ___italicStyle_22; }
	inline uint8_t* get_address_of_italicStyle_22() { return &___italicStyle_22; }
	inline void set_italicStyle_22(uint8_t value)
	{
		___italicStyle_22 = value;
	}

	inline static int32_t get_offset_of_tabSize_23() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t364381626, ___tabSize_23)); }
	inline uint8_t get_tabSize_23() const { return ___tabSize_23; }
	inline uint8_t* get_address_of_tabSize_23() { return &___tabSize_23; }
	inline void set_tabSize_23(uint8_t value)
	{
		___tabSize_23 = value;
	}

	inline static int32_t get_offset_of_m_oldTabSize_24() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t364381626, ___m_oldTabSize_24)); }
	inline uint8_t get_m_oldTabSize_24() const { return ___m_oldTabSize_24; }
	inline uint8_t* get_address_of_m_oldTabSize_24() { return &___m_oldTabSize_24; }
	inline void set_m_oldTabSize_24(uint8_t value)
	{
		___m_oldTabSize_24 = value;
	}
};

struct TMP_FontAsset_t364381626_StaticFields
{
public:
	// TMPro.TMP_FontAsset TMPro.TMP_FontAsset::s_defaultFontAsset
	TMP_FontAsset_t364381626 * ___s_defaultFontAsset_5;

public:
	inline static int32_t get_offset_of_s_defaultFontAsset_5() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t364381626_StaticFields, ___s_defaultFontAsset_5)); }
	inline TMP_FontAsset_t364381626 * get_s_defaultFontAsset_5() const { return ___s_defaultFontAsset_5; }
	inline TMP_FontAsset_t364381626 ** get_address_of_s_defaultFontAsset_5() { return &___s_defaultFontAsset_5; }
	inline void set_s_defaultFontAsset_5(TMP_FontAsset_t364381626 * value)
	{
		___s_defaultFontAsset_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_defaultFontAsset_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_FONTASSET_T364381626_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef TMP_PHONENUMBERVALIDATOR_T743649728_H
#define TMP_PHONENUMBERVALIDATOR_T743649728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_PhoneNumberValidator
struct  TMP_PhoneNumberValidator_t743649728  : public TMP_InputValidator_t1385053824
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_PHONENUMBERVALIDATOR_T743649728_H
#ifndef TMP_SPRITEASSET_T484820633_H
#define TMP_SPRITEASSET_T484820633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteAsset
struct  TMP_SpriteAsset_t484820633  : public TMP_Asset_t2469957285
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_SpriteAsset::m_UnicodeLookup
	Dictionary_2_t1839659084 * ___m_UnicodeLookup_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_SpriteAsset::m_NameLookup
	Dictionary_2_t1839659084 * ___m_NameLookup_6;
	// UnityEngine.Texture TMPro.TMP_SpriteAsset::spriteSheet
	Texture_t3661962703 * ___spriteSheet_8;
	// System.Collections.Generic.List`1<TMPro.TMP_Sprite> TMPro.TMP_SpriteAsset::spriteInfoList
	List_1_t2026141888 * ___spriteInfoList_9;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_SpriteAsset::m_SpriteUnicodeLookup
	Dictionary_2_t1839659084 * ___m_SpriteUnicodeLookup_10;
	// System.Collections.Generic.List`1<TMPro.TMP_SpriteAsset> TMPro.TMP_SpriteAsset::fallbackSpriteAssets
	List_1_t1956895375 * ___fallbackSpriteAssets_11;

public:
	inline static int32_t get_offset_of_m_UnicodeLookup_5() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t484820633, ___m_UnicodeLookup_5)); }
	inline Dictionary_2_t1839659084 * get_m_UnicodeLookup_5() const { return ___m_UnicodeLookup_5; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_UnicodeLookup_5() { return &___m_UnicodeLookup_5; }
	inline void set_m_UnicodeLookup_5(Dictionary_2_t1839659084 * value)
	{
		___m_UnicodeLookup_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_UnicodeLookup_5), value);
	}

	inline static int32_t get_offset_of_m_NameLookup_6() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t484820633, ___m_NameLookup_6)); }
	inline Dictionary_2_t1839659084 * get_m_NameLookup_6() const { return ___m_NameLookup_6; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_NameLookup_6() { return &___m_NameLookup_6; }
	inline void set_m_NameLookup_6(Dictionary_2_t1839659084 * value)
	{
		___m_NameLookup_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_NameLookup_6), value);
	}

	inline static int32_t get_offset_of_spriteSheet_8() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t484820633, ___spriteSheet_8)); }
	inline Texture_t3661962703 * get_spriteSheet_8() const { return ___spriteSheet_8; }
	inline Texture_t3661962703 ** get_address_of_spriteSheet_8() { return &___spriteSheet_8; }
	inline void set_spriteSheet_8(Texture_t3661962703 * value)
	{
		___spriteSheet_8 = value;
		Il2CppCodeGenWriteBarrier((&___spriteSheet_8), value);
	}

	inline static int32_t get_offset_of_spriteInfoList_9() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t484820633, ___spriteInfoList_9)); }
	inline List_1_t2026141888 * get_spriteInfoList_9() const { return ___spriteInfoList_9; }
	inline List_1_t2026141888 ** get_address_of_spriteInfoList_9() { return &___spriteInfoList_9; }
	inline void set_spriteInfoList_9(List_1_t2026141888 * value)
	{
		___spriteInfoList_9 = value;
		Il2CppCodeGenWriteBarrier((&___spriteInfoList_9), value);
	}

	inline static int32_t get_offset_of_m_SpriteUnicodeLookup_10() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t484820633, ___m_SpriteUnicodeLookup_10)); }
	inline Dictionary_2_t1839659084 * get_m_SpriteUnicodeLookup_10() const { return ___m_SpriteUnicodeLookup_10; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_SpriteUnicodeLookup_10() { return &___m_SpriteUnicodeLookup_10; }
	inline void set_m_SpriteUnicodeLookup_10(Dictionary_2_t1839659084 * value)
	{
		___m_SpriteUnicodeLookup_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpriteUnicodeLookup_10), value);
	}

	inline static int32_t get_offset_of_fallbackSpriteAssets_11() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t484820633, ___fallbackSpriteAssets_11)); }
	inline List_1_t1956895375 * get_fallbackSpriteAssets_11() const { return ___fallbackSpriteAssets_11; }
	inline List_1_t1956895375 ** get_address_of_fallbackSpriteAssets_11() { return &___fallbackSpriteAssets_11; }
	inline void set_fallbackSpriteAssets_11(List_1_t1956895375 * value)
	{
		___fallbackSpriteAssets_11 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackSpriteAssets_11), value);
	}
};

struct TMP_SpriteAsset_t484820633_StaticFields
{
public:
	// TMPro.TMP_SpriteAsset TMPro.TMP_SpriteAsset::m_defaultSpriteAsset
	TMP_SpriteAsset_t484820633 * ___m_defaultSpriteAsset_7;

public:
	inline static int32_t get_offset_of_m_defaultSpriteAsset_7() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t484820633_StaticFields, ___m_defaultSpriteAsset_7)); }
	inline TMP_SpriteAsset_t484820633 * get_m_defaultSpriteAsset_7() const { return ___m_defaultSpriteAsset_7; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_defaultSpriteAsset_7() { return &___m_defaultSpriteAsset_7; }
	inline void set_m_defaultSpriteAsset_7(TMP_SpriteAsset_t484820633 * value)
	{
		___m_defaultSpriteAsset_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultSpriteAsset_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SPRITEASSET_T484820633_H
#ifndef DROPDOWNITEM_T1842596711_H
#define DROPDOWNITEM_T1842596711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Dropdown/DropdownItem
struct  DropdownItem_t1842596711  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_Text TMPro.TMP_Dropdown/DropdownItem::m_Text
	TMP_Text_t2599618874 * ___m_Text_2;
	// UnityEngine.UI.Image TMPro.TMP_Dropdown/DropdownItem::m_Image
	Image_t2670269651 * ___m_Image_3;
	// UnityEngine.RectTransform TMPro.TMP_Dropdown/DropdownItem::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_4;
	// UnityEngine.UI.Toggle TMPro.TMP_Dropdown/DropdownItem::m_Toggle
	Toggle_t2735377061 * ___m_Toggle_5;

public:
	inline static int32_t get_offset_of_m_Text_2() { return static_cast<int32_t>(offsetof(DropdownItem_t1842596711, ___m_Text_2)); }
	inline TMP_Text_t2599618874 * get_m_Text_2() const { return ___m_Text_2; }
	inline TMP_Text_t2599618874 ** get_address_of_m_Text_2() { return &___m_Text_2; }
	inline void set_m_Text_2(TMP_Text_t2599618874 * value)
	{
		___m_Text_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_2), value);
	}

	inline static int32_t get_offset_of_m_Image_3() { return static_cast<int32_t>(offsetof(DropdownItem_t1842596711, ___m_Image_3)); }
	inline Image_t2670269651 * get_m_Image_3() const { return ___m_Image_3; }
	inline Image_t2670269651 ** get_address_of_m_Image_3() { return &___m_Image_3; }
	inline void set_m_Image_3(Image_t2670269651 * value)
	{
		___m_Image_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Image_3), value);
	}

	inline static int32_t get_offset_of_m_RectTransform_4() { return static_cast<int32_t>(offsetof(DropdownItem_t1842596711, ___m_RectTransform_4)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_4() const { return ___m_RectTransform_4; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_4() { return &___m_RectTransform_4; }
	inline void set_m_RectTransform_4(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_4), value);
	}

	inline static int32_t get_offset_of_m_Toggle_5() { return static_cast<int32_t>(offsetof(DropdownItem_t1842596711, ___m_Toggle_5)); }
	inline Toggle_t2735377061 * get_m_Toggle_5() const { return ___m_Toggle_5; }
	inline Toggle_t2735377061 ** get_address_of_m_Toggle_5() { return &___m_Toggle_5; }
	inline void set_m_Toggle_5(Toggle_t2735377061 * value)
	{
		___m_Toggle_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Toggle_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DROPDOWNITEM_T1842596711_H
#ifndef TMP_SUBMESH_T2613037997_H
#define TMP_SUBMESH_T2613037997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SubMesh
struct  TMP_SubMesh_t2613037997  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_FontAsset TMPro.TMP_SubMesh::m_fontAsset
	TMP_FontAsset_t364381626 * ___m_fontAsset_2;
	// TMPro.TMP_SpriteAsset TMPro.TMP_SubMesh::m_spriteAsset
	TMP_SpriteAsset_t484820633 * ___m_spriteAsset_3;
	// UnityEngine.Material TMPro.TMP_SubMesh::m_material
	Material_t340375123 * ___m_material_4;
	// UnityEngine.Material TMPro.TMP_SubMesh::m_sharedMaterial
	Material_t340375123 * ___m_sharedMaterial_5;
	// UnityEngine.Material TMPro.TMP_SubMesh::m_fallbackMaterial
	Material_t340375123 * ___m_fallbackMaterial_6;
	// UnityEngine.Material TMPro.TMP_SubMesh::m_fallbackSourceMaterial
	Material_t340375123 * ___m_fallbackSourceMaterial_7;
	// System.Boolean TMPro.TMP_SubMesh::m_isDefaultMaterial
	bool ___m_isDefaultMaterial_8;
	// System.Single TMPro.TMP_SubMesh::m_padding
	float ___m_padding_9;
	// UnityEngine.Renderer TMPro.TMP_SubMesh::m_renderer
	Renderer_t2627027031 * ___m_renderer_10;
	// UnityEngine.MeshFilter TMPro.TMP_SubMesh::m_meshFilter
	MeshFilter_t3523625662 * ___m_meshFilter_11;
	// UnityEngine.Mesh TMPro.TMP_SubMesh::m_mesh
	Mesh_t3648964284 * ___m_mesh_12;
	// UnityEngine.BoxCollider TMPro.TMP_SubMesh::m_boxCollider
	BoxCollider_t1640800422 * ___m_boxCollider_13;
	// TMPro.TextMeshPro TMPro.TMP_SubMesh::m_TextComponent
	TextMeshPro_t2393593166 * ___m_TextComponent_14;
	// System.Boolean TMPro.TMP_SubMesh::m_isRegisteredForEvents
	bool ___m_isRegisteredForEvents_15;

public:
	inline static int32_t get_offset_of_m_fontAsset_2() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_fontAsset_2)); }
	inline TMP_FontAsset_t364381626 * get_m_fontAsset_2() const { return ___m_fontAsset_2; }
	inline TMP_FontAsset_t364381626 ** get_address_of_m_fontAsset_2() { return &___m_fontAsset_2; }
	inline void set_m_fontAsset_2(TMP_FontAsset_t364381626 * value)
	{
		___m_fontAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontAsset_2), value);
	}

	inline static int32_t get_offset_of_m_spriteAsset_3() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_spriteAsset_3)); }
	inline TMP_SpriteAsset_t484820633 * get_m_spriteAsset_3() const { return ___m_spriteAsset_3; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_spriteAsset_3() { return &___m_spriteAsset_3; }
	inline void set_m_spriteAsset_3(TMP_SpriteAsset_t484820633 * value)
	{
		___m_spriteAsset_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAsset_3), value);
	}

	inline static int32_t get_offset_of_m_material_4() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_material_4)); }
	inline Material_t340375123 * get_m_material_4() const { return ___m_material_4; }
	inline Material_t340375123 ** get_address_of_m_material_4() { return &___m_material_4; }
	inline void set_m_material_4(Material_t340375123 * value)
	{
		___m_material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_material_4), value);
	}

	inline static int32_t get_offset_of_m_sharedMaterial_5() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_sharedMaterial_5)); }
	inline Material_t340375123 * get_m_sharedMaterial_5() const { return ___m_sharedMaterial_5; }
	inline Material_t340375123 ** get_address_of_m_sharedMaterial_5() { return &___m_sharedMaterial_5; }
	inline void set_m_sharedMaterial_5(Material_t340375123 * value)
	{
		___m_sharedMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_sharedMaterial_5), value);
	}

	inline static int32_t get_offset_of_m_fallbackMaterial_6() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_fallbackMaterial_6)); }
	inline Material_t340375123 * get_m_fallbackMaterial_6() const { return ___m_fallbackMaterial_6; }
	inline Material_t340375123 ** get_address_of_m_fallbackMaterial_6() { return &___m_fallbackMaterial_6; }
	inline void set_m_fallbackMaterial_6(Material_t340375123 * value)
	{
		___m_fallbackMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackMaterial_6), value);
	}

	inline static int32_t get_offset_of_m_fallbackSourceMaterial_7() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_fallbackSourceMaterial_7)); }
	inline Material_t340375123 * get_m_fallbackSourceMaterial_7() const { return ___m_fallbackSourceMaterial_7; }
	inline Material_t340375123 ** get_address_of_m_fallbackSourceMaterial_7() { return &___m_fallbackSourceMaterial_7; }
	inline void set_m_fallbackSourceMaterial_7(Material_t340375123 * value)
	{
		___m_fallbackSourceMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackSourceMaterial_7), value);
	}

	inline static int32_t get_offset_of_m_isDefaultMaterial_8() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_isDefaultMaterial_8)); }
	inline bool get_m_isDefaultMaterial_8() const { return ___m_isDefaultMaterial_8; }
	inline bool* get_address_of_m_isDefaultMaterial_8() { return &___m_isDefaultMaterial_8; }
	inline void set_m_isDefaultMaterial_8(bool value)
	{
		___m_isDefaultMaterial_8 = value;
	}

	inline static int32_t get_offset_of_m_padding_9() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_padding_9)); }
	inline float get_m_padding_9() const { return ___m_padding_9; }
	inline float* get_address_of_m_padding_9() { return &___m_padding_9; }
	inline void set_m_padding_9(float value)
	{
		___m_padding_9 = value;
	}

	inline static int32_t get_offset_of_m_renderer_10() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_renderer_10)); }
	inline Renderer_t2627027031 * get_m_renderer_10() const { return ___m_renderer_10; }
	inline Renderer_t2627027031 ** get_address_of_m_renderer_10() { return &___m_renderer_10; }
	inline void set_m_renderer_10(Renderer_t2627027031 * value)
	{
		___m_renderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_renderer_10), value);
	}

	inline static int32_t get_offset_of_m_meshFilter_11() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_meshFilter_11)); }
	inline MeshFilter_t3523625662 * get_m_meshFilter_11() const { return ___m_meshFilter_11; }
	inline MeshFilter_t3523625662 ** get_address_of_m_meshFilter_11() { return &___m_meshFilter_11; }
	inline void set_m_meshFilter_11(MeshFilter_t3523625662 * value)
	{
		___m_meshFilter_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_meshFilter_11), value);
	}

	inline static int32_t get_offset_of_m_mesh_12() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_mesh_12)); }
	inline Mesh_t3648964284 * get_m_mesh_12() const { return ___m_mesh_12; }
	inline Mesh_t3648964284 ** get_address_of_m_mesh_12() { return &___m_mesh_12; }
	inline void set_m_mesh_12(Mesh_t3648964284 * value)
	{
		___m_mesh_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_12), value);
	}

	inline static int32_t get_offset_of_m_boxCollider_13() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_boxCollider_13)); }
	inline BoxCollider_t1640800422 * get_m_boxCollider_13() const { return ___m_boxCollider_13; }
	inline BoxCollider_t1640800422 ** get_address_of_m_boxCollider_13() { return &___m_boxCollider_13; }
	inline void set_m_boxCollider_13(BoxCollider_t1640800422 * value)
	{
		___m_boxCollider_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_boxCollider_13), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_14() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_TextComponent_14)); }
	inline TextMeshPro_t2393593166 * get_m_TextComponent_14() const { return ___m_TextComponent_14; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_TextComponent_14() { return &___m_TextComponent_14; }
	inline void set_m_TextComponent_14(TextMeshPro_t2393593166 * value)
	{
		___m_TextComponent_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_14), value);
	}

	inline static int32_t get_offset_of_m_isRegisteredForEvents_15() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_isRegisteredForEvents_15)); }
	inline bool get_m_isRegisteredForEvents_15() const { return ___m_isRegisteredForEvents_15; }
	inline bool* get_address_of_m_isRegisteredForEvents_15() { return &___m_isRegisteredForEvents_15; }
	inline void set_m_isRegisteredForEvents_15(bool value)
	{
		___m_isRegisteredForEvents_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SUBMESH_T2613037997_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef INLINEGRAPHICMANAGER_T2871008645_H
#define INLINEGRAPHICMANAGER_T2871008645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.InlineGraphicManager
struct  InlineGraphicManager_t2871008645  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_SpriteAsset TMPro.InlineGraphicManager::m_spriteAsset
	TMP_SpriteAsset_t484820633 * ___m_spriteAsset_2;
	// TMPro.InlineGraphic TMPro.InlineGraphicManager::m_inlineGraphic
	InlineGraphic_t2901727699 * ___m_inlineGraphic_3;
	// UnityEngine.CanvasRenderer TMPro.InlineGraphicManager::m_inlineGraphicCanvasRenderer
	CanvasRenderer_t2598313366 * ___m_inlineGraphicCanvasRenderer_4;
	// UnityEngine.UIVertex[] TMPro.InlineGraphicManager::m_uiVertex
	UIVertexU5BU5D_t1981460040* ___m_uiVertex_5;
	// UnityEngine.RectTransform TMPro.InlineGraphicManager::m_inlineGraphicRectTransform
	RectTransform_t3704657025 * ___m_inlineGraphicRectTransform_6;
	// TMPro.TMP_Text TMPro.InlineGraphicManager::m_textComponent
	TMP_Text_t2599618874 * ___m_textComponent_7;
	// System.Boolean TMPro.InlineGraphicManager::m_isInitialized
	bool ___m_isInitialized_8;

public:
	inline static int32_t get_offset_of_m_spriteAsset_2() { return static_cast<int32_t>(offsetof(InlineGraphicManager_t2871008645, ___m_spriteAsset_2)); }
	inline TMP_SpriteAsset_t484820633 * get_m_spriteAsset_2() const { return ___m_spriteAsset_2; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_spriteAsset_2() { return &___m_spriteAsset_2; }
	inline void set_m_spriteAsset_2(TMP_SpriteAsset_t484820633 * value)
	{
		___m_spriteAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAsset_2), value);
	}

	inline static int32_t get_offset_of_m_inlineGraphic_3() { return static_cast<int32_t>(offsetof(InlineGraphicManager_t2871008645, ___m_inlineGraphic_3)); }
	inline InlineGraphic_t2901727699 * get_m_inlineGraphic_3() const { return ___m_inlineGraphic_3; }
	inline InlineGraphic_t2901727699 ** get_address_of_m_inlineGraphic_3() { return &___m_inlineGraphic_3; }
	inline void set_m_inlineGraphic_3(InlineGraphic_t2901727699 * value)
	{
		___m_inlineGraphic_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_inlineGraphic_3), value);
	}

	inline static int32_t get_offset_of_m_inlineGraphicCanvasRenderer_4() { return static_cast<int32_t>(offsetof(InlineGraphicManager_t2871008645, ___m_inlineGraphicCanvasRenderer_4)); }
	inline CanvasRenderer_t2598313366 * get_m_inlineGraphicCanvasRenderer_4() const { return ___m_inlineGraphicCanvasRenderer_4; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_inlineGraphicCanvasRenderer_4() { return &___m_inlineGraphicCanvasRenderer_4; }
	inline void set_m_inlineGraphicCanvasRenderer_4(CanvasRenderer_t2598313366 * value)
	{
		___m_inlineGraphicCanvasRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_inlineGraphicCanvasRenderer_4), value);
	}

	inline static int32_t get_offset_of_m_uiVertex_5() { return static_cast<int32_t>(offsetof(InlineGraphicManager_t2871008645, ___m_uiVertex_5)); }
	inline UIVertexU5BU5D_t1981460040* get_m_uiVertex_5() const { return ___m_uiVertex_5; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_uiVertex_5() { return &___m_uiVertex_5; }
	inline void set_m_uiVertex_5(UIVertexU5BU5D_t1981460040* value)
	{
		___m_uiVertex_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_uiVertex_5), value);
	}

	inline static int32_t get_offset_of_m_inlineGraphicRectTransform_6() { return static_cast<int32_t>(offsetof(InlineGraphicManager_t2871008645, ___m_inlineGraphicRectTransform_6)); }
	inline RectTransform_t3704657025 * get_m_inlineGraphicRectTransform_6() const { return ___m_inlineGraphicRectTransform_6; }
	inline RectTransform_t3704657025 ** get_address_of_m_inlineGraphicRectTransform_6() { return &___m_inlineGraphicRectTransform_6; }
	inline void set_m_inlineGraphicRectTransform_6(RectTransform_t3704657025 * value)
	{
		___m_inlineGraphicRectTransform_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_inlineGraphicRectTransform_6), value);
	}

	inline static int32_t get_offset_of_m_textComponent_7() { return static_cast<int32_t>(offsetof(InlineGraphicManager_t2871008645, ___m_textComponent_7)); }
	inline TMP_Text_t2599618874 * get_m_textComponent_7() const { return ___m_textComponent_7; }
	inline TMP_Text_t2599618874 ** get_address_of_m_textComponent_7() { return &___m_textComponent_7; }
	inline void set_m_textComponent_7(TMP_Text_t2599618874 * value)
	{
		___m_textComponent_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textComponent_7), value);
	}

	inline static int32_t get_offset_of_m_isInitialized_8() { return static_cast<int32_t>(offsetof(InlineGraphicManager_t2871008645, ___m_isInitialized_8)); }
	inline bool get_m_isInitialized_8() const { return ___m_isInitialized_8; }
	inline bool* get_address_of_m_isInitialized_8() { return &___m_isInitialized_8; }
	inline void set_m_isInitialized_8(bool value)
	{
		___m_isInitialized_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INLINEGRAPHICMANAGER_T2871008645_H
#ifndef TMP_SCROLLBAREVENTHANDLER_T374199898_H
#define TMP_SCROLLBAREVENTHANDLER_T374199898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_ScrollbarEventHandler
struct  TMP_ScrollbarEventHandler_t374199898  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean TMPro.TMP_ScrollbarEventHandler::isSelected
	bool ___isSelected_2;

public:
	inline static int32_t get_offset_of_isSelected_2() { return static_cast<int32_t>(offsetof(TMP_ScrollbarEventHandler_t374199898, ___isSelected_2)); }
	inline bool get_isSelected_2() const { return ___isSelected_2; }
	inline bool* get_address_of_isSelected_2() { return &___isSelected_2; }
	inline void set_isSelected_2(bool value)
	{
		___isSelected_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SCROLLBAREVENTHANDLER_T374199898_H
#ifndef TMP_SPRITEANIMATOR_T2836635477_H
#define TMP_SPRITEANIMATOR_T2836635477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteAnimator
struct  TMP_SpriteAnimator_t2836635477  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean> TMPro.TMP_SpriteAnimator::m_animations
	Dictionary_2_t3280968592 * ___m_animations_2;
	// TMPro.TMP_Text TMPro.TMP_SpriteAnimator::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_3;

public:
	inline static int32_t get_offset_of_m_animations_2() { return static_cast<int32_t>(offsetof(TMP_SpriteAnimator_t2836635477, ___m_animations_2)); }
	inline Dictionary_2_t3280968592 * get_m_animations_2() const { return ___m_animations_2; }
	inline Dictionary_2_t3280968592 ** get_address_of_m_animations_2() { return &___m_animations_2; }
	inline void set_m_animations_2(Dictionary_2_t3280968592 * value)
	{
		___m_animations_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_animations_2), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_3() { return static_cast<int32_t>(offsetof(TMP_SpriteAnimator_t2836635477, ___m_TextComponent_3)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_3() const { return ___m_TextComponent_3; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_3() { return &___m_TextComponent_3; }
	inline void set_m_TextComponent_3(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SPRITEANIMATOR_T2836635477_H
#ifndef BASEMESHEFFECT_T2440176439_H
#define BASEMESHEFFECT_T2440176439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t2440176439  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_t1660335611 * ___m_Graphic_2;

public:
	inline static int32_t get_offset_of_m_Graphic_2() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t2440176439, ___m_Graphic_2)); }
	inline Graphic_t1660335611 * get_m_Graphic_2() const { return ___m_Graphic_2; }
	inline Graphic_t1660335611 ** get_address_of_m_Graphic_2() { return &___m_Graphic_2; }
	inline void set_m_Graphic_2(Graphic_t1660335611 * value)
	{
		___m_Graphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T2440176439_H
#ifndef SELECTABLE_T3250028441_H
#define SELECTABLE_T3250028441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3250028441  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t3049316579  ___m_Navigation_3;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_4;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2139031574  ___m_Colors_5;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1362986479  ___m_SpriteState_6;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2532145056 * ___m_AnimationTriggers_7;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_8;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1660335611 * ___m_TargetGraphic_9;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_10;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_11;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_12;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_13;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_14;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1260619206 * ___m_CanvasGroupCache_15;

public:
	inline static int32_t get_offset_of_m_Navigation_3() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Navigation_3)); }
	inline Navigation_t3049316579  get_m_Navigation_3() const { return ___m_Navigation_3; }
	inline Navigation_t3049316579 * get_address_of_m_Navigation_3() { return &___m_Navigation_3; }
	inline void set_m_Navigation_3(Navigation_t3049316579  value)
	{
		___m_Navigation_3 = value;
	}

	inline static int32_t get_offset_of_m_Transition_4() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Transition_4)); }
	inline int32_t get_m_Transition_4() const { return ___m_Transition_4; }
	inline int32_t* get_address_of_m_Transition_4() { return &___m_Transition_4; }
	inline void set_m_Transition_4(int32_t value)
	{
		___m_Transition_4 = value;
	}

	inline static int32_t get_offset_of_m_Colors_5() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Colors_5)); }
	inline ColorBlock_t2139031574  get_m_Colors_5() const { return ___m_Colors_5; }
	inline ColorBlock_t2139031574 * get_address_of_m_Colors_5() { return &___m_Colors_5; }
	inline void set_m_Colors_5(ColorBlock_t2139031574  value)
	{
		___m_Colors_5 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_6() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_SpriteState_6)); }
	inline SpriteState_t1362986479  get_m_SpriteState_6() const { return ___m_SpriteState_6; }
	inline SpriteState_t1362986479 * get_address_of_m_SpriteState_6() { return &___m_SpriteState_6; }
	inline void set_m_SpriteState_6(SpriteState_t1362986479  value)
	{
		___m_SpriteState_6 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_7() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_AnimationTriggers_7)); }
	inline AnimationTriggers_t2532145056 * get_m_AnimationTriggers_7() const { return ___m_AnimationTriggers_7; }
	inline AnimationTriggers_t2532145056 ** get_address_of_m_AnimationTriggers_7() { return &___m_AnimationTriggers_7; }
	inline void set_m_AnimationTriggers_7(AnimationTriggers_t2532145056 * value)
	{
		___m_AnimationTriggers_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_7), value);
	}

	inline static int32_t get_offset_of_m_Interactable_8() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Interactable_8)); }
	inline bool get_m_Interactable_8() const { return ___m_Interactable_8; }
	inline bool* get_address_of_m_Interactable_8() { return &___m_Interactable_8; }
	inline void set_m_Interactable_8(bool value)
	{
		___m_Interactable_8 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_9() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_TargetGraphic_9)); }
	inline Graphic_t1660335611 * get_m_TargetGraphic_9() const { return ___m_TargetGraphic_9; }
	inline Graphic_t1660335611 ** get_address_of_m_TargetGraphic_9() { return &___m_TargetGraphic_9; }
	inline void set_m_TargetGraphic_9(Graphic_t1660335611 * value)
	{
		___m_TargetGraphic_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_9), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_10() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_GroupsAllowInteraction_10)); }
	inline bool get_m_GroupsAllowInteraction_10() const { return ___m_GroupsAllowInteraction_10; }
	inline bool* get_address_of_m_GroupsAllowInteraction_10() { return &___m_GroupsAllowInteraction_10; }
	inline void set_m_GroupsAllowInteraction_10(bool value)
	{
		___m_GroupsAllowInteraction_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_11() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CurrentSelectionState_11)); }
	inline int32_t get_m_CurrentSelectionState_11() const { return ___m_CurrentSelectionState_11; }
	inline int32_t* get_address_of_m_CurrentSelectionState_11() { return &___m_CurrentSelectionState_11; }
	inline void set_m_CurrentSelectionState_11(int32_t value)
	{
		___m_CurrentSelectionState_11 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerInsideU3Ek__BackingField_12)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_12() const { return ___U3CisPointerInsideU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_12() { return &___U3CisPointerInsideU3Ek__BackingField_12; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_12(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerDownU3Ek__BackingField_13)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_13() const { return ___U3CisPointerDownU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_13() { return &___U3CisPointerDownU3Ek__BackingField_13; }
	inline void set_U3CisPointerDownU3Ek__BackingField_13(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3ChasSelectionU3Ek__BackingField_14)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_14() const { return ___U3ChasSelectionU3Ek__BackingField_14; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_14() { return &___U3ChasSelectionU3Ek__BackingField_14; }
	inline void set_U3ChasSelectionU3Ek__BackingField_14(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_15() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CanvasGroupCache_15)); }
	inline List_1_t1260619206 * get_m_CanvasGroupCache_15() const { return ___m_CanvasGroupCache_15; }
	inline List_1_t1260619206 ** get_address_of_m_CanvasGroupCache_15() { return &___m_CanvasGroupCache_15; }
	inline void set_m_CanvasGroupCache_15(List_1_t1260619206 * value)
	{
		___m_CanvasGroupCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_15), value);
	}
};

struct Selectable_t3250028441_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t427135887 * ___s_List_2;

public:
	inline static int32_t get_offset_of_s_List_2() { return static_cast<int32_t>(offsetof(Selectable_t3250028441_StaticFields, ___s_List_2)); }
	inline List_1_t427135887 * get_s_List_2() const { return ___s_List_2; }
	inline List_1_t427135887 ** get_address_of_s_List_2() { return &___s_List_2; }
	inline void set_s_List_2(List_1_t427135887 * value)
	{
		___s_List_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3250028441_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_t2598313366 * ___m_CanvasRenderer_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_4)); }
	inline Material_t340375123 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t340375123 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t340375123 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_5)); }
	inline Color_t2555686324  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2555686324 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2555686324  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_7)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRenderer_8)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRenderer_8() const { return ___m_CanvasRenderer_8; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRenderer_8() { return &___m_CanvasRenderer_8; }
	inline void set_m_CanvasRenderer_8(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRenderer_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_9)); }
	inline Canvas_t3310196443 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t3310196443 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t340375123 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t340375123 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t3648964284 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t3648964284 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef SHADOW_T773074319_H
#define SHADOW_T773074319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Shadow
struct  Shadow_t773074319  : public BaseMeshEffect_t2440176439
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t2555686324  ___m_EffectColor_3;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_t2156229523  ___m_EffectDistance_4;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_5;

public:
	inline static int32_t get_offset_of_m_EffectColor_3() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_EffectColor_3)); }
	inline Color_t2555686324  get_m_EffectColor_3() const { return ___m_EffectColor_3; }
	inline Color_t2555686324 * get_address_of_m_EffectColor_3() { return &___m_EffectColor_3; }
	inline void set_m_EffectColor_3(Color_t2555686324  value)
	{
		___m_EffectColor_3 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_4() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_EffectDistance_4)); }
	inline Vector2_t2156229523  get_m_EffectDistance_4() const { return ___m_EffectDistance_4; }
	inline Vector2_t2156229523 * get_address_of_m_EffectDistance_4() { return &___m_EffectDistance_4; }
	inline void set_m_EffectDistance_4(Vector2_t2156229523  value)
	{
		___m_EffectDistance_4 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_5() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_UseGraphicAlpha_5)); }
	inline bool get_m_UseGraphicAlpha_5() const { return ___m_UseGraphicAlpha_5; }
	inline bool* get_address_of_m_UseGraphicAlpha_5() { return &___m_UseGraphicAlpha_5; }
	inline void set_m_UseGraphicAlpha_5(bool value)
	{
		___m_UseGraphicAlpha_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOW_T773074319_H
#ifndef POSITIONASUV1_T3991086357_H
#define POSITIONASUV1_T3991086357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.PositionAsUV1
struct  PositionAsUV1_t3991086357  : public BaseMeshEffect_t2440176439
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONASUV1_T3991086357_H
#ifndef TMP_INPUTFIELD_T1099764886_H
#define TMP_INPUTFIELD_T1099764886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField
struct  TMP_InputField_t1099764886  : public Selectable_t3250028441
{
public:
	// UnityEngine.TouchScreenKeyboard TMPro.TMP_InputField::m_Keyboard
	TouchScreenKeyboard_t731888065 * ___m_Keyboard_16;
	// UnityEngine.RectTransform TMPro.TMP_InputField::m_TextViewport
	RectTransform_t3704657025 * ___m_TextViewport_18;
	// TMPro.TMP_Text TMPro.TMP_InputField::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_19;
	// UnityEngine.RectTransform TMPro.TMP_InputField::m_TextComponentRectTransform
	RectTransform_t3704657025 * ___m_TextComponentRectTransform_20;
	// UnityEngine.UI.Graphic TMPro.TMP_InputField::m_Placeholder
	Graphic_t1660335611 * ___m_Placeholder_21;
	// UnityEngine.UI.Scrollbar TMPro.TMP_InputField::m_VerticalScrollbar
	Scrollbar_t1494447233 * ___m_VerticalScrollbar_22;
	// TMPro.TMP_ScrollbarEventHandler TMPro.TMP_InputField::m_VerticalScrollbarEventHandler
	TMP_ScrollbarEventHandler_t374199898 * ___m_VerticalScrollbarEventHandler_23;
	// System.Single TMPro.TMP_InputField::m_ScrollPosition
	float ___m_ScrollPosition_24;
	// System.Single TMPro.TMP_InputField::m_ScrollSensitivity
	float ___m_ScrollSensitivity_25;
	// TMPro.TMP_InputField/ContentType TMPro.TMP_InputField::m_ContentType
	int32_t ___m_ContentType_26;
	// TMPro.TMP_InputField/InputType TMPro.TMP_InputField::m_InputType
	int32_t ___m_InputType_27;
	// System.Char TMPro.TMP_InputField::m_AsteriskChar
	Il2CppChar ___m_AsteriskChar_28;
	// UnityEngine.TouchScreenKeyboardType TMPro.TMP_InputField::m_KeyboardType
	int32_t ___m_KeyboardType_29;
	// TMPro.TMP_InputField/LineType TMPro.TMP_InputField::m_LineType
	int32_t ___m_LineType_30;
	// System.Boolean TMPro.TMP_InputField::m_HideMobileInput
	bool ___m_HideMobileInput_31;
	// TMPro.TMP_InputField/CharacterValidation TMPro.TMP_InputField::m_CharacterValidation
	int32_t ___m_CharacterValidation_32;
	// System.String TMPro.TMP_InputField::m_RegexValue
	String_t* ___m_RegexValue_33;
	// System.Single TMPro.TMP_InputField::m_GlobalPointSize
	float ___m_GlobalPointSize_34;
	// System.Int32 TMPro.TMP_InputField::m_CharacterLimit
	int32_t ___m_CharacterLimit_35;
	// TMPro.TMP_InputField/SubmitEvent TMPro.TMP_InputField::m_OnEndEdit
	SubmitEvent_t1343580625 * ___m_OnEndEdit_36;
	// TMPro.TMP_InputField/SubmitEvent TMPro.TMP_InputField::m_OnSubmit
	SubmitEvent_t1343580625 * ___m_OnSubmit_37;
	// TMPro.TMP_InputField/SelectionEvent TMPro.TMP_InputField::m_OnSelect
	SelectionEvent_t4268942288 * ___m_OnSelect_38;
	// TMPro.TMP_InputField/SelectionEvent TMPro.TMP_InputField::m_OnDeselect
	SelectionEvent_t4268942288 * ___m_OnDeselect_39;
	// TMPro.TMP_InputField/TextSelectionEvent TMPro.TMP_InputField::m_OnTextSelection
	TextSelectionEvent_t1023421581 * ___m_OnTextSelection_40;
	// TMPro.TMP_InputField/TextSelectionEvent TMPro.TMP_InputField::m_OnEndTextSelection
	TextSelectionEvent_t1023421581 * ___m_OnEndTextSelection_41;
	// TMPro.TMP_InputField/OnChangeEvent TMPro.TMP_InputField::m_OnValueChanged
	OnChangeEvent_t4257774360 * ___m_OnValueChanged_42;
	// TMPro.TMP_InputField/OnValidateInput TMPro.TMP_InputField::m_OnValidateInput
	OnValidateInput_t373909109 * ___m_OnValidateInput_43;
	// UnityEngine.Color TMPro.TMP_InputField::m_CaretColor
	Color_t2555686324  ___m_CaretColor_44;
	// System.Boolean TMPro.TMP_InputField::m_CustomCaretColor
	bool ___m_CustomCaretColor_45;
	// UnityEngine.Color TMPro.TMP_InputField::m_SelectionColor
	Color_t2555686324  ___m_SelectionColor_46;
	// System.String TMPro.TMP_InputField::m_Text
	String_t* ___m_Text_47;
	// System.Single TMPro.TMP_InputField::m_CaretBlinkRate
	float ___m_CaretBlinkRate_48;
	// System.Int32 TMPro.TMP_InputField::m_CaretWidth
	int32_t ___m_CaretWidth_49;
	// System.Boolean TMPro.TMP_InputField::m_ReadOnly
	bool ___m_ReadOnly_50;
	// System.Boolean TMPro.TMP_InputField::m_RichText
	bool ___m_RichText_51;
	// System.Int32 TMPro.TMP_InputField::m_StringPosition
	int32_t ___m_StringPosition_52;
	// System.Int32 TMPro.TMP_InputField::m_StringSelectPosition
	int32_t ___m_StringSelectPosition_53;
	// System.Int32 TMPro.TMP_InputField::m_CaretPosition
	int32_t ___m_CaretPosition_54;
	// System.Int32 TMPro.TMP_InputField::m_CaretSelectPosition
	int32_t ___m_CaretSelectPosition_55;
	// UnityEngine.RectTransform TMPro.TMP_InputField::caretRectTrans
	RectTransform_t3704657025 * ___caretRectTrans_56;
	// UnityEngine.UIVertex[] TMPro.TMP_InputField::m_CursorVerts
	UIVertexU5BU5D_t1981460040* ___m_CursorVerts_57;
	// UnityEngine.CanvasRenderer TMPro.TMP_InputField::m_CachedInputRenderer
	CanvasRenderer_t2598313366 * ___m_CachedInputRenderer_58;
	// UnityEngine.Vector2 TMPro.TMP_InputField::m_DefaultTransformPosition
	Vector2_t2156229523  ___m_DefaultTransformPosition_59;
	// UnityEngine.Vector2 TMPro.TMP_InputField::m_LastPosition
	Vector2_t2156229523  ___m_LastPosition_60;
	// UnityEngine.Mesh TMPro.TMP_InputField::m_Mesh
	Mesh_t3648964284 * ___m_Mesh_61;
	// System.Boolean TMPro.TMP_InputField::m_AllowInput
	bool ___m_AllowInput_62;
	// System.Boolean TMPro.TMP_InputField::m_ShouldActivateNextUpdate
	bool ___m_ShouldActivateNextUpdate_63;
	// System.Boolean TMPro.TMP_InputField::m_UpdateDrag
	bool ___m_UpdateDrag_64;
	// System.Boolean TMPro.TMP_InputField::m_DragPositionOutOfBounds
	bool ___m_DragPositionOutOfBounds_65;
	// System.Boolean TMPro.TMP_InputField::m_CaretVisible
	bool ___m_CaretVisible_68;
	// UnityEngine.Coroutine TMPro.TMP_InputField::m_BlinkCoroutine
	Coroutine_t3829159415 * ___m_BlinkCoroutine_69;
	// System.Single TMPro.TMP_InputField::m_BlinkStartTime
	float ___m_BlinkStartTime_70;
	// UnityEngine.Coroutine TMPro.TMP_InputField::m_DragCoroutine
	Coroutine_t3829159415 * ___m_DragCoroutine_71;
	// System.String TMPro.TMP_InputField::m_OriginalText
	String_t* ___m_OriginalText_72;
	// System.Boolean TMPro.TMP_InputField::m_WasCanceled
	bool ___m_WasCanceled_73;
	// System.Boolean TMPro.TMP_InputField::m_HasDoneFocusTransition
	bool ___m_HasDoneFocusTransition_74;
	// System.Boolean TMPro.TMP_InputField::m_IsScrollbarUpdateRequired
	bool ___m_IsScrollbarUpdateRequired_75;
	// System.Boolean TMPro.TMP_InputField::m_IsUpdatingScrollbarValues
	bool ___m_IsUpdatingScrollbarValues_76;
	// System.Boolean TMPro.TMP_InputField::m_isLastKeyBackspace
	bool ___m_isLastKeyBackspace_77;
	// System.Single TMPro.TMP_InputField::m_ClickStartTime
	float ___m_ClickStartTime_78;
	// System.Single TMPro.TMP_InputField::m_DoubleClickDelay
	float ___m_DoubleClickDelay_79;
	// TMPro.TMP_FontAsset TMPro.TMP_InputField::m_GlobalFontAsset
	TMP_FontAsset_t364381626 * ___m_GlobalFontAsset_81;
	// System.Boolean TMPro.TMP_InputField::m_OnFocusSelectAll
	bool ___m_OnFocusSelectAll_82;
	// System.Boolean TMPro.TMP_InputField::m_isSelectAll
	bool ___m_isSelectAll_83;
	// System.Boolean TMPro.TMP_InputField::m_ResetOnDeActivation
	bool ___m_ResetOnDeActivation_84;
	// System.Boolean TMPro.TMP_InputField::m_RestoreOriginalTextOnEscape
	bool ___m_RestoreOriginalTextOnEscape_85;
	// System.Boolean TMPro.TMP_InputField::m_isRichTextEditingAllowed
	bool ___m_isRichTextEditingAllowed_86;
	// TMPro.TMP_InputValidator TMPro.TMP_InputField::m_InputValidator
	TMP_InputValidator_t1385053824 * ___m_InputValidator_87;
	// System.Boolean TMPro.TMP_InputField::m_isSelected
	bool ___m_isSelected_88;
	// System.Boolean TMPro.TMP_InputField::isStringPositionDirty
	bool ___isStringPositionDirty_89;
	// System.Boolean TMPro.TMP_InputField::m_forceRectTransformAdjustment
	bool ___m_forceRectTransformAdjustment_90;
	// UnityEngine.Event TMPro.TMP_InputField::m_ProcessingEvent
	Event_t2956885303 * ___m_ProcessingEvent_91;

public:
	inline static int32_t get_offset_of_m_Keyboard_16() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_Keyboard_16)); }
	inline TouchScreenKeyboard_t731888065 * get_m_Keyboard_16() const { return ___m_Keyboard_16; }
	inline TouchScreenKeyboard_t731888065 ** get_address_of_m_Keyboard_16() { return &___m_Keyboard_16; }
	inline void set_m_Keyboard_16(TouchScreenKeyboard_t731888065 * value)
	{
		___m_Keyboard_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_Keyboard_16), value);
	}

	inline static int32_t get_offset_of_m_TextViewport_18() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_TextViewport_18)); }
	inline RectTransform_t3704657025 * get_m_TextViewport_18() const { return ___m_TextViewport_18; }
	inline RectTransform_t3704657025 ** get_address_of_m_TextViewport_18() { return &___m_TextViewport_18; }
	inline void set_m_TextViewport_18(RectTransform_t3704657025 * value)
	{
		___m_TextViewport_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextViewport_18), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_19() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_TextComponent_19)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_19() const { return ___m_TextComponent_19; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_19() { return &___m_TextComponent_19; }
	inline void set_m_TextComponent_19(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_19), value);
	}

	inline static int32_t get_offset_of_m_TextComponentRectTransform_20() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_TextComponentRectTransform_20)); }
	inline RectTransform_t3704657025 * get_m_TextComponentRectTransform_20() const { return ___m_TextComponentRectTransform_20; }
	inline RectTransform_t3704657025 ** get_address_of_m_TextComponentRectTransform_20() { return &___m_TextComponentRectTransform_20; }
	inline void set_m_TextComponentRectTransform_20(RectTransform_t3704657025 * value)
	{
		___m_TextComponentRectTransform_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponentRectTransform_20), value);
	}

	inline static int32_t get_offset_of_m_Placeholder_21() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_Placeholder_21)); }
	inline Graphic_t1660335611 * get_m_Placeholder_21() const { return ___m_Placeholder_21; }
	inline Graphic_t1660335611 ** get_address_of_m_Placeholder_21() { return &___m_Placeholder_21; }
	inline void set_m_Placeholder_21(Graphic_t1660335611 * value)
	{
		___m_Placeholder_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_Placeholder_21), value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbar_22() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_VerticalScrollbar_22)); }
	inline Scrollbar_t1494447233 * get_m_VerticalScrollbar_22() const { return ___m_VerticalScrollbar_22; }
	inline Scrollbar_t1494447233 ** get_address_of_m_VerticalScrollbar_22() { return &___m_VerticalScrollbar_22; }
	inline void set_m_VerticalScrollbar_22(Scrollbar_t1494447233 * value)
	{
		___m_VerticalScrollbar_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalScrollbar_22), value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarEventHandler_23() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_VerticalScrollbarEventHandler_23)); }
	inline TMP_ScrollbarEventHandler_t374199898 * get_m_VerticalScrollbarEventHandler_23() const { return ___m_VerticalScrollbarEventHandler_23; }
	inline TMP_ScrollbarEventHandler_t374199898 ** get_address_of_m_VerticalScrollbarEventHandler_23() { return &___m_VerticalScrollbarEventHandler_23; }
	inline void set_m_VerticalScrollbarEventHandler_23(TMP_ScrollbarEventHandler_t374199898 * value)
	{
		___m_VerticalScrollbarEventHandler_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalScrollbarEventHandler_23), value);
	}

	inline static int32_t get_offset_of_m_ScrollPosition_24() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_ScrollPosition_24)); }
	inline float get_m_ScrollPosition_24() const { return ___m_ScrollPosition_24; }
	inline float* get_address_of_m_ScrollPosition_24() { return &___m_ScrollPosition_24; }
	inline void set_m_ScrollPosition_24(float value)
	{
		___m_ScrollPosition_24 = value;
	}

	inline static int32_t get_offset_of_m_ScrollSensitivity_25() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_ScrollSensitivity_25)); }
	inline float get_m_ScrollSensitivity_25() const { return ___m_ScrollSensitivity_25; }
	inline float* get_address_of_m_ScrollSensitivity_25() { return &___m_ScrollSensitivity_25; }
	inline void set_m_ScrollSensitivity_25(float value)
	{
		___m_ScrollSensitivity_25 = value;
	}

	inline static int32_t get_offset_of_m_ContentType_26() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_ContentType_26)); }
	inline int32_t get_m_ContentType_26() const { return ___m_ContentType_26; }
	inline int32_t* get_address_of_m_ContentType_26() { return &___m_ContentType_26; }
	inline void set_m_ContentType_26(int32_t value)
	{
		___m_ContentType_26 = value;
	}

	inline static int32_t get_offset_of_m_InputType_27() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_InputType_27)); }
	inline int32_t get_m_InputType_27() const { return ___m_InputType_27; }
	inline int32_t* get_address_of_m_InputType_27() { return &___m_InputType_27; }
	inline void set_m_InputType_27(int32_t value)
	{
		___m_InputType_27 = value;
	}

	inline static int32_t get_offset_of_m_AsteriskChar_28() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_AsteriskChar_28)); }
	inline Il2CppChar get_m_AsteriskChar_28() const { return ___m_AsteriskChar_28; }
	inline Il2CppChar* get_address_of_m_AsteriskChar_28() { return &___m_AsteriskChar_28; }
	inline void set_m_AsteriskChar_28(Il2CppChar value)
	{
		___m_AsteriskChar_28 = value;
	}

	inline static int32_t get_offset_of_m_KeyboardType_29() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_KeyboardType_29)); }
	inline int32_t get_m_KeyboardType_29() const { return ___m_KeyboardType_29; }
	inline int32_t* get_address_of_m_KeyboardType_29() { return &___m_KeyboardType_29; }
	inline void set_m_KeyboardType_29(int32_t value)
	{
		___m_KeyboardType_29 = value;
	}

	inline static int32_t get_offset_of_m_LineType_30() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_LineType_30)); }
	inline int32_t get_m_LineType_30() const { return ___m_LineType_30; }
	inline int32_t* get_address_of_m_LineType_30() { return &___m_LineType_30; }
	inline void set_m_LineType_30(int32_t value)
	{
		___m_LineType_30 = value;
	}

	inline static int32_t get_offset_of_m_HideMobileInput_31() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_HideMobileInput_31)); }
	inline bool get_m_HideMobileInput_31() const { return ___m_HideMobileInput_31; }
	inline bool* get_address_of_m_HideMobileInput_31() { return &___m_HideMobileInput_31; }
	inline void set_m_HideMobileInput_31(bool value)
	{
		___m_HideMobileInput_31 = value;
	}

	inline static int32_t get_offset_of_m_CharacterValidation_32() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_CharacterValidation_32)); }
	inline int32_t get_m_CharacterValidation_32() const { return ___m_CharacterValidation_32; }
	inline int32_t* get_address_of_m_CharacterValidation_32() { return &___m_CharacterValidation_32; }
	inline void set_m_CharacterValidation_32(int32_t value)
	{
		___m_CharacterValidation_32 = value;
	}

	inline static int32_t get_offset_of_m_RegexValue_33() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_RegexValue_33)); }
	inline String_t* get_m_RegexValue_33() const { return ___m_RegexValue_33; }
	inline String_t** get_address_of_m_RegexValue_33() { return &___m_RegexValue_33; }
	inline void set_m_RegexValue_33(String_t* value)
	{
		___m_RegexValue_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_RegexValue_33), value);
	}

	inline static int32_t get_offset_of_m_GlobalPointSize_34() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_GlobalPointSize_34)); }
	inline float get_m_GlobalPointSize_34() const { return ___m_GlobalPointSize_34; }
	inline float* get_address_of_m_GlobalPointSize_34() { return &___m_GlobalPointSize_34; }
	inline void set_m_GlobalPointSize_34(float value)
	{
		___m_GlobalPointSize_34 = value;
	}

	inline static int32_t get_offset_of_m_CharacterLimit_35() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_CharacterLimit_35)); }
	inline int32_t get_m_CharacterLimit_35() const { return ___m_CharacterLimit_35; }
	inline int32_t* get_address_of_m_CharacterLimit_35() { return &___m_CharacterLimit_35; }
	inline void set_m_CharacterLimit_35(int32_t value)
	{
		___m_CharacterLimit_35 = value;
	}

	inline static int32_t get_offset_of_m_OnEndEdit_36() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_OnEndEdit_36)); }
	inline SubmitEvent_t1343580625 * get_m_OnEndEdit_36() const { return ___m_OnEndEdit_36; }
	inline SubmitEvent_t1343580625 ** get_address_of_m_OnEndEdit_36() { return &___m_OnEndEdit_36; }
	inline void set_m_OnEndEdit_36(SubmitEvent_t1343580625 * value)
	{
		___m_OnEndEdit_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnEndEdit_36), value);
	}

	inline static int32_t get_offset_of_m_OnSubmit_37() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_OnSubmit_37)); }
	inline SubmitEvent_t1343580625 * get_m_OnSubmit_37() const { return ___m_OnSubmit_37; }
	inline SubmitEvent_t1343580625 ** get_address_of_m_OnSubmit_37() { return &___m_OnSubmit_37; }
	inline void set_m_OnSubmit_37(SubmitEvent_t1343580625 * value)
	{
		___m_OnSubmit_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnSubmit_37), value);
	}

	inline static int32_t get_offset_of_m_OnSelect_38() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_OnSelect_38)); }
	inline SelectionEvent_t4268942288 * get_m_OnSelect_38() const { return ___m_OnSelect_38; }
	inline SelectionEvent_t4268942288 ** get_address_of_m_OnSelect_38() { return &___m_OnSelect_38; }
	inline void set_m_OnSelect_38(SelectionEvent_t4268942288 * value)
	{
		___m_OnSelect_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnSelect_38), value);
	}

	inline static int32_t get_offset_of_m_OnDeselect_39() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_OnDeselect_39)); }
	inline SelectionEvent_t4268942288 * get_m_OnDeselect_39() const { return ___m_OnDeselect_39; }
	inline SelectionEvent_t4268942288 ** get_address_of_m_OnDeselect_39() { return &___m_OnDeselect_39; }
	inline void set_m_OnDeselect_39(SelectionEvent_t4268942288 * value)
	{
		___m_OnDeselect_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDeselect_39), value);
	}

	inline static int32_t get_offset_of_m_OnTextSelection_40() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_OnTextSelection_40)); }
	inline TextSelectionEvent_t1023421581 * get_m_OnTextSelection_40() const { return ___m_OnTextSelection_40; }
	inline TextSelectionEvent_t1023421581 ** get_address_of_m_OnTextSelection_40() { return &___m_OnTextSelection_40; }
	inline void set_m_OnTextSelection_40(TextSelectionEvent_t1023421581 * value)
	{
		___m_OnTextSelection_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnTextSelection_40), value);
	}

	inline static int32_t get_offset_of_m_OnEndTextSelection_41() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_OnEndTextSelection_41)); }
	inline TextSelectionEvent_t1023421581 * get_m_OnEndTextSelection_41() const { return ___m_OnEndTextSelection_41; }
	inline TextSelectionEvent_t1023421581 ** get_address_of_m_OnEndTextSelection_41() { return &___m_OnEndTextSelection_41; }
	inline void set_m_OnEndTextSelection_41(TextSelectionEvent_t1023421581 * value)
	{
		___m_OnEndTextSelection_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnEndTextSelection_41), value);
	}

	inline static int32_t get_offset_of_m_OnValueChanged_42() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_OnValueChanged_42)); }
	inline OnChangeEvent_t4257774360 * get_m_OnValueChanged_42() const { return ___m_OnValueChanged_42; }
	inline OnChangeEvent_t4257774360 ** get_address_of_m_OnValueChanged_42() { return &___m_OnValueChanged_42; }
	inline void set_m_OnValueChanged_42(OnChangeEvent_t4257774360 * value)
	{
		___m_OnValueChanged_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_42), value);
	}

	inline static int32_t get_offset_of_m_OnValidateInput_43() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_OnValidateInput_43)); }
	inline OnValidateInput_t373909109 * get_m_OnValidateInput_43() const { return ___m_OnValidateInput_43; }
	inline OnValidateInput_t373909109 ** get_address_of_m_OnValidateInput_43() { return &___m_OnValidateInput_43; }
	inline void set_m_OnValidateInput_43(OnValidateInput_t373909109 * value)
	{
		___m_OnValidateInput_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValidateInput_43), value);
	}

	inline static int32_t get_offset_of_m_CaretColor_44() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_CaretColor_44)); }
	inline Color_t2555686324  get_m_CaretColor_44() const { return ___m_CaretColor_44; }
	inline Color_t2555686324 * get_address_of_m_CaretColor_44() { return &___m_CaretColor_44; }
	inline void set_m_CaretColor_44(Color_t2555686324  value)
	{
		___m_CaretColor_44 = value;
	}

	inline static int32_t get_offset_of_m_CustomCaretColor_45() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_CustomCaretColor_45)); }
	inline bool get_m_CustomCaretColor_45() const { return ___m_CustomCaretColor_45; }
	inline bool* get_address_of_m_CustomCaretColor_45() { return &___m_CustomCaretColor_45; }
	inline void set_m_CustomCaretColor_45(bool value)
	{
		___m_CustomCaretColor_45 = value;
	}

	inline static int32_t get_offset_of_m_SelectionColor_46() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_SelectionColor_46)); }
	inline Color_t2555686324  get_m_SelectionColor_46() const { return ___m_SelectionColor_46; }
	inline Color_t2555686324 * get_address_of_m_SelectionColor_46() { return &___m_SelectionColor_46; }
	inline void set_m_SelectionColor_46(Color_t2555686324  value)
	{
		___m_SelectionColor_46 = value;
	}

	inline static int32_t get_offset_of_m_Text_47() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_Text_47)); }
	inline String_t* get_m_Text_47() const { return ___m_Text_47; }
	inline String_t** get_address_of_m_Text_47() { return &___m_Text_47; }
	inline void set_m_Text_47(String_t* value)
	{
		___m_Text_47 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_47), value);
	}

	inline static int32_t get_offset_of_m_CaretBlinkRate_48() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_CaretBlinkRate_48)); }
	inline float get_m_CaretBlinkRate_48() const { return ___m_CaretBlinkRate_48; }
	inline float* get_address_of_m_CaretBlinkRate_48() { return &___m_CaretBlinkRate_48; }
	inline void set_m_CaretBlinkRate_48(float value)
	{
		___m_CaretBlinkRate_48 = value;
	}

	inline static int32_t get_offset_of_m_CaretWidth_49() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_CaretWidth_49)); }
	inline int32_t get_m_CaretWidth_49() const { return ___m_CaretWidth_49; }
	inline int32_t* get_address_of_m_CaretWidth_49() { return &___m_CaretWidth_49; }
	inline void set_m_CaretWidth_49(int32_t value)
	{
		___m_CaretWidth_49 = value;
	}

	inline static int32_t get_offset_of_m_ReadOnly_50() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_ReadOnly_50)); }
	inline bool get_m_ReadOnly_50() const { return ___m_ReadOnly_50; }
	inline bool* get_address_of_m_ReadOnly_50() { return &___m_ReadOnly_50; }
	inline void set_m_ReadOnly_50(bool value)
	{
		___m_ReadOnly_50 = value;
	}

	inline static int32_t get_offset_of_m_RichText_51() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_RichText_51)); }
	inline bool get_m_RichText_51() const { return ___m_RichText_51; }
	inline bool* get_address_of_m_RichText_51() { return &___m_RichText_51; }
	inline void set_m_RichText_51(bool value)
	{
		___m_RichText_51 = value;
	}

	inline static int32_t get_offset_of_m_StringPosition_52() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_StringPosition_52)); }
	inline int32_t get_m_StringPosition_52() const { return ___m_StringPosition_52; }
	inline int32_t* get_address_of_m_StringPosition_52() { return &___m_StringPosition_52; }
	inline void set_m_StringPosition_52(int32_t value)
	{
		___m_StringPosition_52 = value;
	}

	inline static int32_t get_offset_of_m_StringSelectPosition_53() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_StringSelectPosition_53)); }
	inline int32_t get_m_StringSelectPosition_53() const { return ___m_StringSelectPosition_53; }
	inline int32_t* get_address_of_m_StringSelectPosition_53() { return &___m_StringSelectPosition_53; }
	inline void set_m_StringSelectPosition_53(int32_t value)
	{
		___m_StringSelectPosition_53 = value;
	}

	inline static int32_t get_offset_of_m_CaretPosition_54() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_CaretPosition_54)); }
	inline int32_t get_m_CaretPosition_54() const { return ___m_CaretPosition_54; }
	inline int32_t* get_address_of_m_CaretPosition_54() { return &___m_CaretPosition_54; }
	inline void set_m_CaretPosition_54(int32_t value)
	{
		___m_CaretPosition_54 = value;
	}

	inline static int32_t get_offset_of_m_CaretSelectPosition_55() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_CaretSelectPosition_55)); }
	inline int32_t get_m_CaretSelectPosition_55() const { return ___m_CaretSelectPosition_55; }
	inline int32_t* get_address_of_m_CaretSelectPosition_55() { return &___m_CaretSelectPosition_55; }
	inline void set_m_CaretSelectPosition_55(int32_t value)
	{
		___m_CaretSelectPosition_55 = value;
	}

	inline static int32_t get_offset_of_caretRectTrans_56() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___caretRectTrans_56)); }
	inline RectTransform_t3704657025 * get_caretRectTrans_56() const { return ___caretRectTrans_56; }
	inline RectTransform_t3704657025 ** get_address_of_caretRectTrans_56() { return &___caretRectTrans_56; }
	inline void set_caretRectTrans_56(RectTransform_t3704657025 * value)
	{
		___caretRectTrans_56 = value;
		Il2CppCodeGenWriteBarrier((&___caretRectTrans_56), value);
	}

	inline static int32_t get_offset_of_m_CursorVerts_57() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_CursorVerts_57)); }
	inline UIVertexU5BU5D_t1981460040* get_m_CursorVerts_57() const { return ___m_CursorVerts_57; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_CursorVerts_57() { return &___m_CursorVerts_57; }
	inline void set_m_CursorVerts_57(UIVertexU5BU5D_t1981460040* value)
	{
		___m_CursorVerts_57 = value;
		Il2CppCodeGenWriteBarrier((&___m_CursorVerts_57), value);
	}

	inline static int32_t get_offset_of_m_CachedInputRenderer_58() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_CachedInputRenderer_58)); }
	inline CanvasRenderer_t2598313366 * get_m_CachedInputRenderer_58() const { return ___m_CachedInputRenderer_58; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CachedInputRenderer_58() { return &___m_CachedInputRenderer_58; }
	inline void set_m_CachedInputRenderer_58(CanvasRenderer_t2598313366 * value)
	{
		___m_CachedInputRenderer_58 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachedInputRenderer_58), value);
	}

	inline static int32_t get_offset_of_m_DefaultTransformPosition_59() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_DefaultTransformPosition_59)); }
	inline Vector2_t2156229523  get_m_DefaultTransformPosition_59() const { return ___m_DefaultTransformPosition_59; }
	inline Vector2_t2156229523 * get_address_of_m_DefaultTransformPosition_59() { return &___m_DefaultTransformPosition_59; }
	inline void set_m_DefaultTransformPosition_59(Vector2_t2156229523  value)
	{
		___m_DefaultTransformPosition_59 = value;
	}

	inline static int32_t get_offset_of_m_LastPosition_60() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_LastPosition_60)); }
	inline Vector2_t2156229523  get_m_LastPosition_60() const { return ___m_LastPosition_60; }
	inline Vector2_t2156229523 * get_address_of_m_LastPosition_60() { return &___m_LastPosition_60; }
	inline void set_m_LastPosition_60(Vector2_t2156229523  value)
	{
		___m_LastPosition_60 = value;
	}

	inline static int32_t get_offset_of_m_Mesh_61() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_Mesh_61)); }
	inline Mesh_t3648964284 * get_m_Mesh_61() const { return ___m_Mesh_61; }
	inline Mesh_t3648964284 ** get_address_of_m_Mesh_61() { return &___m_Mesh_61; }
	inline void set_m_Mesh_61(Mesh_t3648964284 * value)
	{
		___m_Mesh_61 = value;
		Il2CppCodeGenWriteBarrier((&___m_Mesh_61), value);
	}

	inline static int32_t get_offset_of_m_AllowInput_62() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_AllowInput_62)); }
	inline bool get_m_AllowInput_62() const { return ___m_AllowInput_62; }
	inline bool* get_address_of_m_AllowInput_62() { return &___m_AllowInput_62; }
	inline void set_m_AllowInput_62(bool value)
	{
		___m_AllowInput_62 = value;
	}

	inline static int32_t get_offset_of_m_ShouldActivateNextUpdate_63() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_ShouldActivateNextUpdate_63)); }
	inline bool get_m_ShouldActivateNextUpdate_63() const { return ___m_ShouldActivateNextUpdate_63; }
	inline bool* get_address_of_m_ShouldActivateNextUpdate_63() { return &___m_ShouldActivateNextUpdate_63; }
	inline void set_m_ShouldActivateNextUpdate_63(bool value)
	{
		___m_ShouldActivateNextUpdate_63 = value;
	}

	inline static int32_t get_offset_of_m_UpdateDrag_64() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_UpdateDrag_64)); }
	inline bool get_m_UpdateDrag_64() const { return ___m_UpdateDrag_64; }
	inline bool* get_address_of_m_UpdateDrag_64() { return &___m_UpdateDrag_64; }
	inline void set_m_UpdateDrag_64(bool value)
	{
		___m_UpdateDrag_64 = value;
	}

	inline static int32_t get_offset_of_m_DragPositionOutOfBounds_65() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_DragPositionOutOfBounds_65)); }
	inline bool get_m_DragPositionOutOfBounds_65() const { return ___m_DragPositionOutOfBounds_65; }
	inline bool* get_address_of_m_DragPositionOutOfBounds_65() { return &___m_DragPositionOutOfBounds_65; }
	inline void set_m_DragPositionOutOfBounds_65(bool value)
	{
		___m_DragPositionOutOfBounds_65 = value;
	}

	inline static int32_t get_offset_of_m_CaretVisible_68() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_CaretVisible_68)); }
	inline bool get_m_CaretVisible_68() const { return ___m_CaretVisible_68; }
	inline bool* get_address_of_m_CaretVisible_68() { return &___m_CaretVisible_68; }
	inline void set_m_CaretVisible_68(bool value)
	{
		___m_CaretVisible_68 = value;
	}

	inline static int32_t get_offset_of_m_BlinkCoroutine_69() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_BlinkCoroutine_69)); }
	inline Coroutine_t3829159415 * get_m_BlinkCoroutine_69() const { return ___m_BlinkCoroutine_69; }
	inline Coroutine_t3829159415 ** get_address_of_m_BlinkCoroutine_69() { return &___m_BlinkCoroutine_69; }
	inline void set_m_BlinkCoroutine_69(Coroutine_t3829159415 * value)
	{
		___m_BlinkCoroutine_69 = value;
		Il2CppCodeGenWriteBarrier((&___m_BlinkCoroutine_69), value);
	}

	inline static int32_t get_offset_of_m_BlinkStartTime_70() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_BlinkStartTime_70)); }
	inline float get_m_BlinkStartTime_70() const { return ___m_BlinkStartTime_70; }
	inline float* get_address_of_m_BlinkStartTime_70() { return &___m_BlinkStartTime_70; }
	inline void set_m_BlinkStartTime_70(float value)
	{
		___m_BlinkStartTime_70 = value;
	}

	inline static int32_t get_offset_of_m_DragCoroutine_71() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_DragCoroutine_71)); }
	inline Coroutine_t3829159415 * get_m_DragCoroutine_71() const { return ___m_DragCoroutine_71; }
	inline Coroutine_t3829159415 ** get_address_of_m_DragCoroutine_71() { return &___m_DragCoroutine_71; }
	inline void set_m_DragCoroutine_71(Coroutine_t3829159415 * value)
	{
		___m_DragCoroutine_71 = value;
		Il2CppCodeGenWriteBarrier((&___m_DragCoroutine_71), value);
	}

	inline static int32_t get_offset_of_m_OriginalText_72() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_OriginalText_72)); }
	inline String_t* get_m_OriginalText_72() const { return ___m_OriginalText_72; }
	inline String_t** get_address_of_m_OriginalText_72() { return &___m_OriginalText_72; }
	inline void set_m_OriginalText_72(String_t* value)
	{
		___m_OriginalText_72 = value;
		Il2CppCodeGenWriteBarrier((&___m_OriginalText_72), value);
	}

	inline static int32_t get_offset_of_m_WasCanceled_73() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_WasCanceled_73)); }
	inline bool get_m_WasCanceled_73() const { return ___m_WasCanceled_73; }
	inline bool* get_address_of_m_WasCanceled_73() { return &___m_WasCanceled_73; }
	inline void set_m_WasCanceled_73(bool value)
	{
		___m_WasCanceled_73 = value;
	}

	inline static int32_t get_offset_of_m_HasDoneFocusTransition_74() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_HasDoneFocusTransition_74)); }
	inline bool get_m_HasDoneFocusTransition_74() const { return ___m_HasDoneFocusTransition_74; }
	inline bool* get_address_of_m_HasDoneFocusTransition_74() { return &___m_HasDoneFocusTransition_74; }
	inline void set_m_HasDoneFocusTransition_74(bool value)
	{
		___m_HasDoneFocusTransition_74 = value;
	}

	inline static int32_t get_offset_of_m_IsScrollbarUpdateRequired_75() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_IsScrollbarUpdateRequired_75)); }
	inline bool get_m_IsScrollbarUpdateRequired_75() const { return ___m_IsScrollbarUpdateRequired_75; }
	inline bool* get_address_of_m_IsScrollbarUpdateRequired_75() { return &___m_IsScrollbarUpdateRequired_75; }
	inline void set_m_IsScrollbarUpdateRequired_75(bool value)
	{
		___m_IsScrollbarUpdateRequired_75 = value;
	}

	inline static int32_t get_offset_of_m_IsUpdatingScrollbarValues_76() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_IsUpdatingScrollbarValues_76)); }
	inline bool get_m_IsUpdatingScrollbarValues_76() const { return ___m_IsUpdatingScrollbarValues_76; }
	inline bool* get_address_of_m_IsUpdatingScrollbarValues_76() { return &___m_IsUpdatingScrollbarValues_76; }
	inline void set_m_IsUpdatingScrollbarValues_76(bool value)
	{
		___m_IsUpdatingScrollbarValues_76 = value;
	}

	inline static int32_t get_offset_of_m_isLastKeyBackspace_77() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_isLastKeyBackspace_77)); }
	inline bool get_m_isLastKeyBackspace_77() const { return ___m_isLastKeyBackspace_77; }
	inline bool* get_address_of_m_isLastKeyBackspace_77() { return &___m_isLastKeyBackspace_77; }
	inline void set_m_isLastKeyBackspace_77(bool value)
	{
		___m_isLastKeyBackspace_77 = value;
	}

	inline static int32_t get_offset_of_m_ClickStartTime_78() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_ClickStartTime_78)); }
	inline float get_m_ClickStartTime_78() const { return ___m_ClickStartTime_78; }
	inline float* get_address_of_m_ClickStartTime_78() { return &___m_ClickStartTime_78; }
	inline void set_m_ClickStartTime_78(float value)
	{
		___m_ClickStartTime_78 = value;
	}

	inline static int32_t get_offset_of_m_DoubleClickDelay_79() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_DoubleClickDelay_79)); }
	inline float get_m_DoubleClickDelay_79() const { return ___m_DoubleClickDelay_79; }
	inline float* get_address_of_m_DoubleClickDelay_79() { return &___m_DoubleClickDelay_79; }
	inline void set_m_DoubleClickDelay_79(float value)
	{
		___m_DoubleClickDelay_79 = value;
	}

	inline static int32_t get_offset_of_m_GlobalFontAsset_81() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_GlobalFontAsset_81)); }
	inline TMP_FontAsset_t364381626 * get_m_GlobalFontAsset_81() const { return ___m_GlobalFontAsset_81; }
	inline TMP_FontAsset_t364381626 ** get_address_of_m_GlobalFontAsset_81() { return &___m_GlobalFontAsset_81; }
	inline void set_m_GlobalFontAsset_81(TMP_FontAsset_t364381626 * value)
	{
		___m_GlobalFontAsset_81 = value;
		Il2CppCodeGenWriteBarrier((&___m_GlobalFontAsset_81), value);
	}

	inline static int32_t get_offset_of_m_OnFocusSelectAll_82() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_OnFocusSelectAll_82)); }
	inline bool get_m_OnFocusSelectAll_82() const { return ___m_OnFocusSelectAll_82; }
	inline bool* get_address_of_m_OnFocusSelectAll_82() { return &___m_OnFocusSelectAll_82; }
	inline void set_m_OnFocusSelectAll_82(bool value)
	{
		___m_OnFocusSelectAll_82 = value;
	}

	inline static int32_t get_offset_of_m_isSelectAll_83() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_isSelectAll_83)); }
	inline bool get_m_isSelectAll_83() const { return ___m_isSelectAll_83; }
	inline bool* get_address_of_m_isSelectAll_83() { return &___m_isSelectAll_83; }
	inline void set_m_isSelectAll_83(bool value)
	{
		___m_isSelectAll_83 = value;
	}

	inline static int32_t get_offset_of_m_ResetOnDeActivation_84() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_ResetOnDeActivation_84)); }
	inline bool get_m_ResetOnDeActivation_84() const { return ___m_ResetOnDeActivation_84; }
	inline bool* get_address_of_m_ResetOnDeActivation_84() { return &___m_ResetOnDeActivation_84; }
	inline void set_m_ResetOnDeActivation_84(bool value)
	{
		___m_ResetOnDeActivation_84 = value;
	}

	inline static int32_t get_offset_of_m_RestoreOriginalTextOnEscape_85() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_RestoreOriginalTextOnEscape_85)); }
	inline bool get_m_RestoreOriginalTextOnEscape_85() const { return ___m_RestoreOriginalTextOnEscape_85; }
	inline bool* get_address_of_m_RestoreOriginalTextOnEscape_85() { return &___m_RestoreOriginalTextOnEscape_85; }
	inline void set_m_RestoreOriginalTextOnEscape_85(bool value)
	{
		___m_RestoreOriginalTextOnEscape_85 = value;
	}

	inline static int32_t get_offset_of_m_isRichTextEditingAllowed_86() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_isRichTextEditingAllowed_86)); }
	inline bool get_m_isRichTextEditingAllowed_86() const { return ___m_isRichTextEditingAllowed_86; }
	inline bool* get_address_of_m_isRichTextEditingAllowed_86() { return &___m_isRichTextEditingAllowed_86; }
	inline void set_m_isRichTextEditingAllowed_86(bool value)
	{
		___m_isRichTextEditingAllowed_86 = value;
	}

	inline static int32_t get_offset_of_m_InputValidator_87() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_InputValidator_87)); }
	inline TMP_InputValidator_t1385053824 * get_m_InputValidator_87() const { return ___m_InputValidator_87; }
	inline TMP_InputValidator_t1385053824 ** get_address_of_m_InputValidator_87() { return &___m_InputValidator_87; }
	inline void set_m_InputValidator_87(TMP_InputValidator_t1385053824 * value)
	{
		___m_InputValidator_87 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputValidator_87), value);
	}

	inline static int32_t get_offset_of_m_isSelected_88() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_isSelected_88)); }
	inline bool get_m_isSelected_88() const { return ___m_isSelected_88; }
	inline bool* get_address_of_m_isSelected_88() { return &___m_isSelected_88; }
	inline void set_m_isSelected_88(bool value)
	{
		___m_isSelected_88 = value;
	}

	inline static int32_t get_offset_of_isStringPositionDirty_89() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___isStringPositionDirty_89)); }
	inline bool get_isStringPositionDirty_89() const { return ___isStringPositionDirty_89; }
	inline bool* get_address_of_isStringPositionDirty_89() { return &___isStringPositionDirty_89; }
	inline void set_isStringPositionDirty_89(bool value)
	{
		___isStringPositionDirty_89 = value;
	}

	inline static int32_t get_offset_of_m_forceRectTransformAdjustment_90() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_forceRectTransformAdjustment_90)); }
	inline bool get_m_forceRectTransformAdjustment_90() const { return ___m_forceRectTransformAdjustment_90; }
	inline bool* get_address_of_m_forceRectTransformAdjustment_90() { return &___m_forceRectTransformAdjustment_90; }
	inline void set_m_forceRectTransformAdjustment_90(bool value)
	{
		___m_forceRectTransformAdjustment_90 = value;
	}

	inline static int32_t get_offset_of_m_ProcessingEvent_91() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_ProcessingEvent_91)); }
	inline Event_t2956885303 * get_m_ProcessingEvent_91() const { return ___m_ProcessingEvent_91; }
	inline Event_t2956885303 ** get_address_of_m_ProcessingEvent_91() { return &___m_ProcessingEvent_91; }
	inline void set_m_ProcessingEvent_91(Event_t2956885303 * value)
	{
		___m_ProcessingEvent_91 = value;
		Il2CppCodeGenWriteBarrier((&___m_ProcessingEvent_91), value);
	}
};

struct TMP_InputField_t1099764886_StaticFields
{
public:
	// System.Char[] TMPro.TMP_InputField::kSeparators
	CharU5BU5D_t3528271667* ___kSeparators_17;

public:
	inline static int32_t get_offset_of_kSeparators_17() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886_StaticFields, ___kSeparators_17)); }
	inline CharU5BU5D_t3528271667* get_kSeparators_17() const { return ___kSeparators_17; }
	inline CharU5BU5D_t3528271667** get_address_of_kSeparators_17() { return &___kSeparators_17; }
	inline void set_kSeparators_17(CharU5BU5D_t3528271667* value)
	{
		___kSeparators_17 = value;
		Il2CppCodeGenWriteBarrier((&___kSeparators_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_INPUTFIELD_T1099764886_H
#ifndef TMP_DROPDOWN_T3024694699_H
#define TMP_DROPDOWN_T3024694699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Dropdown
struct  TMP_Dropdown_t3024694699  : public Selectable_t3250028441
{
public:
	// UnityEngine.RectTransform TMPro.TMP_Dropdown::m_Template
	RectTransform_t3704657025 * ___m_Template_16;
	// TMPro.TMP_Text TMPro.TMP_Dropdown::m_CaptionText
	TMP_Text_t2599618874 * ___m_CaptionText_17;
	// UnityEngine.UI.Image TMPro.TMP_Dropdown::m_CaptionImage
	Image_t2670269651 * ___m_CaptionImage_18;
	// TMPro.TMP_Text TMPro.TMP_Dropdown::m_ItemText
	TMP_Text_t2599618874 * ___m_ItemText_19;
	// UnityEngine.UI.Image TMPro.TMP_Dropdown::m_ItemImage
	Image_t2670269651 * ___m_ItemImage_20;
	// System.Int32 TMPro.TMP_Dropdown::m_Value
	int32_t ___m_Value_21;
	// TMPro.TMP_Dropdown/OptionDataList TMPro.TMP_Dropdown::m_Options
	OptionDataList_t2293557512 * ___m_Options_22;
	// TMPro.TMP_Dropdown/DropdownEvent TMPro.TMP_Dropdown::m_OnValueChanged
	DropdownEvent_t1704673280 * ___m_OnValueChanged_23;
	// UnityEngine.GameObject TMPro.TMP_Dropdown::m_Dropdown
	GameObject_t1113636619 * ___m_Dropdown_24;
	// UnityEngine.GameObject TMPro.TMP_Dropdown::m_Blocker
	GameObject_t1113636619 * ___m_Blocker_25;
	// System.Collections.Generic.List`1<TMPro.TMP_Dropdown/DropdownItem> TMPro.TMP_Dropdown::m_Items
	List_1_t3314671453 * ___m_Items_26;
	// TMPro.TweenRunner`1<TMPro.FloatTween> TMPro.TMP_Dropdown::m_AlphaTweenRunner
	TweenRunner_1_t3214153611 * ___m_AlphaTweenRunner_27;
	// System.Boolean TMPro.TMP_Dropdown::validTemplate
	bool ___validTemplate_28;

public:
	inline static int32_t get_offset_of_m_Template_16() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t3024694699, ___m_Template_16)); }
	inline RectTransform_t3704657025 * get_m_Template_16() const { return ___m_Template_16; }
	inline RectTransform_t3704657025 ** get_address_of_m_Template_16() { return &___m_Template_16; }
	inline void set_m_Template_16(RectTransform_t3704657025 * value)
	{
		___m_Template_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_Template_16), value);
	}

	inline static int32_t get_offset_of_m_CaptionText_17() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t3024694699, ___m_CaptionText_17)); }
	inline TMP_Text_t2599618874 * get_m_CaptionText_17() const { return ___m_CaptionText_17; }
	inline TMP_Text_t2599618874 ** get_address_of_m_CaptionText_17() { return &___m_CaptionText_17; }
	inline void set_m_CaptionText_17(TMP_Text_t2599618874 * value)
	{
		___m_CaptionText_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CaptionText_17), value);
	}

	inline static int32_t get_offset_of_m_CaptionImage_18() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t3024694699, ___m_CaptionImage_18)); }
	inline Image_t2670269651 * get_m_CaptionImage_18() const { return ___m_CaptionImage_18; }
	inline Image_t2670269651 ** get_address_of_m_CaptionImage_18() { return &___m_CaptionImage_18; }
	inline void set_m_CaptionImage_18(Image_t2670269651 * value)
	{
		___m_CaptionImage_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_CaptionImage_18), value);
	}

	inline static int32_t get_offset_of_m_ItemText_19() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t3024694699, ___m_ItemText_19)); }
	inline TMP_Text_t2599618874 * get_m_ItemText_19() const { return ___m_ItemText_19; }
	inline TMP_Text_t2599618874 ** get_address_of_m_ItemText_19() { return &___m_ItemText_19; }
	inline void set_m_ItemText_19(TMP_Text_t2599618874 * value)
	{
		___m_ItemText_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemText_19), value);
	}

	inline static int32_t get_offset_of_m_ItemImage_20() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t3024694699, ___m_ItemImage_20)); }
	inline Image_t2670269651 * get_m_ItemImage_20() const { return ___m_ItemImage_20; }
	inline Image_t2670269651 ** get_address_of_m_ItemImage_20() { return &___m_ItemImage_20; }
	inline void set_m_ItemImage_20(Image_t2670269651 * value)
	{
		___m_ItemImage_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemImage_20), value);
	}

	inline static int32_t get_offset_of_m_Value_21() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t3024694699, ___m_Value_21)); }
	inline int32_t get_m_Value_21() const { return ___m_Value_21; }
	inline int32_t* get_address_of_m_Value_21() { return &___m_Value_21; }
	inline void set_m_Value_21(int32_t value)
	{
		___m_Value_21 = value;
	}

	inline static int32_t get_offset_of_m_Options_22() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t3024694699, ___m_Options_22)); }
	inline OptionDataList_t2293557512 * get_m_Options_22() const { return ___m_Options_22; }
	inline OptionDataList_t2293557512 ** get_address_of_m_Options_22() { return &___m_Options_22; }
	inline void set_m_Options_22(OptionDataList_t2293557512 * value)
	{
		___m_Options_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_Options_22), value);
	}

	inline static int32_t get_offset_of_m_OnValueChanged_23() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t3024694699, ___m_OnValueChanged_23)); }
	inline DropdownEvent_t1704673280 * get_m_OnValueChanged_23() const { return ___m_OnValueChanged_23; }
	inline DropdownEvent_t1704673280 ** get_address_of_m_OnValueChanged_23() { return &___m_OnValueChanged_23; }
	inline void set_m_OnValueChanged_23(DropdownEvent_t1704673280 * value)
	{
		___m_OnValueChanged_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_23), value);
	}

	inline static int32_t get_offset_of_m_Dropdown_24() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t3024694699, ___m_Dropdown_24)); }
	inline GameObject_t1113636619 * get_m_Dropdown_24() const { return ___m_Dropdown_24; }
	inline GameObject_t1113636619 ** get_address_of_m_Dropdown_24() { return &___m_Dropdown_24; }
	inline void set_m_Dropdown_24(GameObject_t1113636619 * value)
	{
		___m_Dropdown_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dropdown_24), value);
	}

	inline static int32_t get_offset_of_m_Blocker_25() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t3024694699, ___m_Blocker_25)); }
	inline GameObject_t1113636619 * get_m_Blocker_25() const { return ___m_Blocker_25; }
	inline GameObject_t1113636619 ** get_address_of_m_Blocker_25() { return &___m_Blocker_25; }
	inline void set_m_Blocker_25(GameObject_t1113636619 * value)
	{
		___m_Blocker_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_Blocker_25), value);
	}

	inline static int32_t get_offset_of_m_Items_26() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t3024694699, ___m_Items_26)); }
	inline List_1_t3314671453 * get_m_Items_26() const { return ___m_Items_26; }
	inline List_1_t3314671453 ** get_address_of_m_Items_26() { return &___m_Items_26; }
	inline void set_m_Items_26(List_1_t3314671453 * value)
	{
		___m_Items_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_Items_26), value);
	}

	inline static int32_t get_offset_of_m_AlphaTweenRunner_27() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t3024694699, ___m_AlphaTweenRunner_27)); }
	inline TweenRunner_1_t3214153611 * get_m_AlphaTweenRunner_27() const { return ___m_AlphaTweenRunner_27; }
	inline TweenRunner_1_t3214153611 ** get_address_of_m_AlphaTweenRunner_27() { return &___m_AlphaTweenRunner_27; }
	inline void set_m_AlphaTweenRunner_27(TweenRunner_1_t3214153611 * value)
	{
		___m_AlphaTweenRunner_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_AlphaTweenRunner_27), value);
	}

	inline static int32_t get_offset_of_validTemplate_28() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t3024694699, ___validTemplate_28)); }
	inline bool get_validTemplate_28() const { return ___validTemplate_28; }
	inline bool* get_address_of_validTemplate_28() { return &___validTemplate_28; }
	inline void set_validTemplate_28(bool value)
	{
		___validTemplate_28 = value;
	}
};

struct TMP_Dropdown_t3024694699_StaticFields
{
public:
	// TMPro.TMP_Dropdown/OptionData TMPro.TMP_Dropdown::s_NoOptionData
	OptionData_t1114640268 * ___s_NoOptionData_29;

public:
	inline static int32_t get_offset_of_s_NoOptionData_29() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t3024694699_StaticFields, ___s_NoOptionData_29)); }
	inline OptionData_t1114640268 * get_s_NoOptionData_29() const { return ___s_NoOptionData_29; }
	inline OptionData_t1114640268 ** get_address_of_s_NoOptionData_29() { return &___s_NoOptionData_29; }
	inline void set_s_NoOptionData_29(OptionData_t1114640268 * value)
	{
		___s_NoOptionData_29 = value;
		Il2CppCodeGenWriteBarrier((&___s_NoOptionData_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_DROPDOWN_T3024694699_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_20)); }
	inline Material_t340375123 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t340375123 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_21)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef TMP_SELECTIONCARET_T407257285_H
#define TMP_SELECTIONCARET_T407257285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SelectionCaret
struct  TMP_SelectionCaret_t407257285  : public MaskableGraphic_t3839221559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SELECTIONCARET_T407257285_H
#ifndef INLINEGRAPHIC_T2901727699_H
#define INLINEGRAPHIC_T2901727699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.InlineGraphic
struct  InlineGraphic_t2901727699  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.Texture TMPro.InlineGraphic::texture
	Texture_t3661962703 * ___texture_28;
	// TMPro.InlineGraphicManager TMPro.InlineGraphic::m_manager
	InlineGraphicManager_t2871008645 * ___m_manager_29;
	// UnityEngine.RectTransform TMPro.InlineGraphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_30;
	// UnityEngine.RectTransform TMPro.InlineGraphic::m_ParentRectTransform
	RectTransform_t3704657025 * ___m_ParentRectTransform_31;

public:
	inline static int32_t get_offset_of_texture_28() { return static_cast<int32_t>(offsetof(InlineGraphic_t2901727699, ___texture_28)); }
	inline Texture_t3661962703 * get_texture_28() const { return ___texture_28; }
	inline Texture_t3661962703 ** get_address_of_texture_28() { return &___texture_28; }
	inline void set_texture_28(Texture_t3661962703 * value)
	{
		___texture_28 = value;
		Il2CppCodeGenWriteBarrier((&___texture_28), value);
	}

	inline static int32_t get_offset_of_m_manager_29() { return static_cast<int32_t>(offsetof(InlineGraphic_t2901727699, ___m_manager_29)); }
	inline InlineGraphicManager_t2871008645 * get_m_manager_29() const { return ___m_manager_29; }
	inline InlineGraphicManager_t2871008645 ** get_address_of_m_manager_29() { return &___m_manager_29; }
	inline void set_m_manager_29(InlineGraphicManager_t2871008645 * value)
	{
		___m_manager_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_manager_29), value);
	}

	inline static int32_t get_offset_of_m_RectTransform_30() { return static_cast<int32_t>(offsetof(InlineGraphic_t2901727699, ___m_RectTransform_30)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_30() const { return ___m_RectTransform_30; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_30() { return &___m_RectTransform_30; }
	inline void set_m_RectTransform_30(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_30), value);
	}

	inline static int32_t get_offset_of_m_ParentRectTransform_31() { return static_cast<int32_t>(offsetof(InlineGraphic_t2901727699, ___m_ParentRectTransform_31)); }
	inline RectTransform_t3704657025 * get_m_ParentRectTransform_31() const { return ___m_ParentRectTransform_31; }
	inline RectTransform_t3704657025 ** get_address_of_m_ParentRectTransform_31() { return &___m_ParentRectTransform_31; }
	inline void set_m_ParentRectTransform_31(RectTransform_t3704657025 * value)
	{
		___m_ParentRectTransform_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentRectTransform_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INLINEGRAPHIC_T2901727699_H
#ifndef TMP_SUBMESHUI_T1578871311_H
#define TMP_SUBMESHUI_T1578871311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SubMeshUI
struct  TMP_SubMeshUI_t1578871311  : public MaskableGraphic_t3839221559
{
public:
	// TMPro.TMP_FontAsset TMPro.TMP_SubMeshUI::m_fontAsset
	TMP_FontAsset_t364381626 * ___m_fontAsset_28;
	// TMPro.TMP_SpriteAsset TMPro.TMP_SubMeshUI::m_spriteAsset
	TMP_SpriteAsset_t484820633 * ___m_spriteAsset_29;
	// UnityEngine.Material TMPro.TMP_SubMeshUI::m_material
	Material_t340375123 * ___m_material_30;
	// UnityEngine.Material TMPro.TMP_SubMeshUI::m_sharedMaterial
	Material_t340375123 * ___m_sharedMaterial_31;
	// UnityEngine.Material TMPro.TMP_SubMeshUI::m_fallbackMaterial
	Material_t340375123 * ___m_fallbackMaterial_32;
	// UnityEngine.Material TMPro.TMP_SubMeshUI::m_fallbackSourceMaterial
	Material_t340375123 * ___m_fallbackSourceMaterial_33;
	// System.Boolean TMPro.TMP_SubMeshUI::m_isDefaultMaterial
	bool ___m_isDefaultMaterial_34;
	// System.Single TMPro.TMP_SubMeshUI::m_padding
	float ___m_padding_35;
	// UnityEngine.CanvasRenderer TMPro.TMP_SubMeshUI::m_canvasRenderer
	CanvasRenderer_t2598313366 * ___m_canvasRenderer_36;
	// UnityEngine.Mesh TMPro.TMP_SubMeshUI::m_mesh
	Mesh_t3648964284 * ___m_mesh_37;
	// TMPro.TextMeshProUGUI TMPro.TMP_SubMeshUI::m_TextComponent
	TextMeshProUGUI_t529313277 * ___m_TextComponent_38;
	// System.Boolean TMPro.TMP_SubMeshUI::m_isRegisteredForEvents
	bool ___m_isRegisteredForEvents_39;
	// System.Boolean TMPro.TMP_SubMeshUI::m_materialDirty
	bool ___m_materialDirty_40;
	// System.Int32 TMPro.TMP_SubMeshUI::m_materialReferenceIndex
	int32_t ___m_materialReferenceIndex_41;

public:
	inline static int32_t get_offset_of_m_fontAsset_28() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_fontAsset_28)); }
	inline TMP_FontAsset_t364381626 * get_m_fontAsset_28() const { return ___m_fontAsset_28; }
	inline TMP_FontAsset_t364381626 ** get_address_of_m_fontAsset_28() { return &___m_fontAsset_28; }
	inline void set_m_fontAsset_28(TMP_FontAsset_t364381626 * value)
	{
		___m_fontAsset_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontAsset_28), value);
	}

	inline static int32_t get_offset_of_m_spriteAsset_29() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_spriteAsset_29)); }
	inline TMP_SpriteAsset_t484820633 * get_m_spriteAsset_29() const { return ___m_spriteAsset_29; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_spriteAsset_29() { return &___m_spriteAsset_29; }
	inline void set_m_spriteAsset_29(TMP_SpriteAsset_t484820633 * value)
	{
		___m_spriteAsset_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAsset_29), value);
	}

	inline static int32_t get_offset_of_m_material_30() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_material_30)); }
	inline Material_t340375123 * get_m_material_30() const { return ___m_material_30; }
	inline Material_t340375123 ** get_address_of_m_material_30() { return &___m_material_30; }
	inline void set_m_material_30(Material_t340375123 * value)
	{
		___m_material_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_material_30), value);
	}

	inline static int32_t get_offset_of_m_sharedMaterial_31() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_sharedMaterial_31)); }
	inline Material_t340375123 * get_m_sharedMaterial_31() const { return ___m_sharedMaterial_31; }
	inline Material_t340375123 ** get_address_of_m_sharedMaterial_31() { return &___m_sharedMaterial_31; }
	inline void set_m_sharedMaterial_31(Material_t340375123 * value)
	{
		___m_sharedMaterial_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_sharedMaterial_31), value);
	}

	inline static int32_t get_offset_of_m_fallbackMaterial_32() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_fallbackMaterial_32)); }
	inline Material_t340375123 * get_m_fallbackMaterial_32() const { return ___m_fallbackMaterial_32; }
	inline Material_t340375123 ** get_address_of_m_fallbackMaterial_32() { return &___m_fallbackMaterial_32; }
	inline void set_m_fallbackMaterial_32(Material_t340375123 * value)
	{
		___m_fallbackMaterial_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackMaterial_32), value);
	}

	inline static int32_t get_offset_of_m_fallbackSourceMaterial_33() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_fallbackSourceMaterial_33)); }
	inline Material_t340375123 * get_m_fallbackSourceMaterial_33() const { return ___m_fallbackSourceMaterial_33; }
	inline Material_t340375123 ** get_address_of_m_fallbackSourceMaterial_33() { return &___m_fallbackSourceMaterial_33; }
	inline void set_m_fallbackSourceMaterial_33(Material_t340375123 * value)
	{
		___m_fallbackSourceMaterial_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackSourceMaterial_33), value);
	}

	inline static int32_t get_offset_of_m_isDefaultMaterial_34() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_isDefaultMaterial_34)); }
	inline bool get_m_isDefaultMaterial_34() const { return ___m_isDefaultMaterial_34; }
	inline bool* get_address_of_m_isDefaultMaterial_34() { return &___m_isDefaultMaterial_34; }
	inline void set_m_isDefaultMaterial_34(bool value)
	{
		___m_isDefaultMaterial_34 = value;
	}

	inline static int32_t get_offset_of_m_padding_35() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_padding_35)); }
	inline float get_m_padding_35() const { return ___m_padding_35; }
	inline float* get_address_of_m_padding_35() { return &___m_padding_35; }
	inline void set_m_padding_35(float value)
	{
		___m_padding_35 = value;
	}

	inline static int32_t get_offset_of_m_canvasRenderer_36() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_canvasRenderer_36)); }
	inline CanvasRenderer_t2598313366 * get_m_canvasRenderer_36() const { return ___m_canvasRenderer_36; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_canvasRenderer_36() { return &___m_canvasRenderer_36; }
	inline void set_m_canvasRenderer_36(CanvasRenderer_t2598313366 * value)
	{
		___m_canvasRenderer_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvasRenderer_36), value);
	}

	inline static int32_t get_offset_of_m_mesh_37() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_mesh_37)); }
	inline Mesh_t3648964284 * get_m_mesh_37() const { return ___m_mesh_37; }
	inline Mesh_t3648964284 ** get_address_of_m_mesh_37() { return &___m_mesh_37; }
	inline void set_m_mesh_37(Mesh_t3648964284 * value)
	{
		___m_mesh_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_37), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_38() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_TextComponent_38)); }
	inline TextMeshProUGUI_t529313277 * get_m_TextComponent_38() const { return ___m_TextComponent_38; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_m_TextComponent_38() { return &___m_TextComponent_38; }
	inline void set_m_TextComponent_38(TextMeshProUGUI_t529313277 * value)
	{
		___m_TextComponent_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_38), value);
	}

	inline static int32_t get_offset_of_m_isRegisteredForEvents_39() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_isRegisteredForEvents_39)); }
	inline bool get_m_isRegisteredForEvents_39() const { return ___m_isRegisteredForEvents_39; }
	inline bool* get_address_of_m_isRegisteredForEvents_39() { return &___m_isRegisteredForEvents_39; }
	inline void set_m_isRegisteredForEvents_39(bool value)
	{
		___m_isRegisteredForEvents_39 = value;
	}

	inline static int32_t get_offset_of_m_materialDirty_40() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_materialDirty_40)); }
	inline bool get_m_materialDirty_40() const { return ___m_materialDirty_40; }
	inline bool* get_address_of_m_materialDirty_40() { return &___m_materialDirty_40; }
	inline void set_m_materialDirty_40(bool value)
	{
		___m_materialDirty_40 = value;
	}

	inline static int32_t get_offset_of_m_materialReferenceIndex_41() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_materialReferenceIndex_41)); }
	inline int32_t get_m_materialReferenceIndex_41() const { return ___m_materialReferenceIndex_41; }
	inline int32_t* get_address_of_m_materialReferenceIndex_41() { return &___m_materialReferenceIndex_41; }
	inline void set_m_materialReferenceIndex_41(int32_t value)
	{
		___m_materialReferenceIndex_41 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SUBMESHUI_T1578871311_H
#ifndef OUTLINE_T2536100125_H
#define OUTLINE_T2536100125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Outline
struct  Outline_t2536100125  : public Shadow_t773074319
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINE_T2536100125_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (GetRayIntersectionAllNonAllocCallback_t2311174851), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (GetRaycastNonAllocCallback_t3841783507), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (VertexHelper_t2453304189), -1, sizeof(VertexHelper_t2453304189_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2002[11] = 
{
	VertexHelper_t2453304189::get_offset_of_m_Positions_0(),
	VertexHelper_t2453304189::get_offset_of_m_Colors_1(),
	VertexHelper_t2453304189::get_offset_of_m_Uv0S_2(),
	VertexHelper_t2453304189::get_offset_of_m_Uv1S_3(),
	VertexHelper_t2453304189::get_offset_of_m_Uv2S_4(),
	VertexHelper_t2453304189::get_offset_of_m_Uv3S_5(),
	VertexHelper_t2453304189::get_offset_of_m_Normals_6(),
	VertexHelper_t2453304189::get_offset_of_m_Tangents_7(),
	VertexHelper_t2453304189::get_offset_of_m_Indices_8(),
	VertexHelper_t2453304189_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t2453304189_StaticFields::get_offset_of_s_DefaultNormal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (BaseVertexEffect_t2675891272), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (BaseMeshEffect_t2440176439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2004[1] = 
{
	BaseMeshEffect_t2440176439::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (Outline_t2536100125), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (PositionAsUV1_t3991086357), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (Shadow_t773074319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2009[4] = 
{
	Shadow_t773074319::get_offset_of_m_EffectColor_3(),
	Shadow_t773074319::get_offset_of_m_EffectDistance_4(),
	Shadow_t773074319::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255365), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2010[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (U24ArrayTypeU3D12_t2488454196)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t2488454196 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (U3CModuleU3E_t692745547), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (FastAction_t3491443480), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2013[2] = 
{
	FastAction_t3491443480::get_offset_of_delegates_0(),
	FastAction_t3491443480::get_offset_of_lookup_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2014[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2015[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2016[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (InlineGraphic_t2901727699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2017[4] = 
{
	InlineGraphic_t2901727699::get_offset_of_texture_28(),
	InlineGraphic_t2901727699::get_offset_of_m_manager_29(),
	InlineGraphic_t2901727699::get_offset_of_m_RectTransform_30(),
	InlineGraphic_t2901727699::get_offset_of_m_ParentRectTransform_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (InlineGraphicManager_t2871008645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2018[7] = 
{
	InlineGraphicManager_t2871008645::get_offset_of_m_spriteAsset_2(),
	InlineGraphicManager_t2871008645::get_offset_of_m_inlineGraphic_3(),
	InlineGraphicManager_t2871008645::get_offset_of_m_inlineGraphicCanvasRenderer_4(),
	InlineGraphicManager_t2871008645::get_offset_of_m_uiVertex_5(),
	InlineGraphicManager_t2871008645::get_offset_of_m_inlineGraphicRectTransform_6(),
	InlineGraphicManager_t2871008645::get_offset_of_m_textComponent_7(),
	InlineGraphicManager_t2871008645::get_offset_of_m_isInitialized_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (U3CU3Ec__DisplayClass28_0_t1214464005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2019[1] = 
{
	U3CU3Ec__DisplayClass28_0_t1214464005::get_offset_of_hashCode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (U3CU3Ec__DisplayClass29_0_t1214529541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2020[1] = 
{
	U3CU3Ec__DisplayClass29_0_t1214529541::get_offset_of_index_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (MaterialReferenceManager_t2114976864), -1, sizeof(MaterialReferenceManager_t2114976864_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2021[4] = 
{
	MaterialReferenceManager_t2114976864_StaticFields::get_offset_of_s_Instance_0(),
	MaterialReferenceManager_t2114976864::get_offset_of_m_FontMaterialReferenceLookup_1(),
	MaterialReferenceManager_t2114976864::get_offset_of_m_FontAssetReferenceLookup_2(),
	MaterialReferenceManager_t2114976864::get_offset_of_m_SpriteAssetReferenceLookup_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (MaterialReference_t1952344632)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2022[9] = 
{
	MaterialReference_t1952344632::get_offset_of_index_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_t1952344632::get_offset_of_fontAsset_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_t1952344632::get_offset_of_spriteAsset_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_t1952344632::get_offset_of_material_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_t1952344632::get_offset_of_isDefaultMaterial_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_t1952344632::get_offset_of_isFallbackMaterial_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_t1952344632::get_offset_of_fallbackMaterial_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_t1952344632::get_offset_of_padding_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_t1952344632::get_offset_of_referenceCount_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (TMP_Asset_t2469957285), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2023[3] = 
{
	TMP_Asset_t2469957285::get_offset_of_hashCode_2(),
	TMP_Asset_t2469957285::get_offset_of_material_3(),
	TMP_Asset_t2469957285::get_offset_of_materialHashCode_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (TMP_ColorGradient_t3678055768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2024[4] = 
{
	TMP_ColorGradient_t3678055768::get_offset_of_topLeft_2(),
	TMP_ColorGradient_t3678055768::get_offset_of_topRight_3(),
	TMP_ColorGradient_t3678055768::get_offset_of_bottomLeft_4(),
	TMP_ColorGradient_t3678055768::get_offset_of_bottomRight_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (TMP_Compatibility_t2923561388), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (AnchorPositions_t1063569770)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2026[12] = 
{
	AnchorPositions_t1063569770::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (ColorTween_t378116136)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2028[6] = 
{
	ColorTween_t378116136::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t378116136::get_offset_of_m_StartColor_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t378116136::get_offset_of_m_TargetColor_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t378116136::get_offset_of_m_TweenMode_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t378116136::get_offset_of_m_Duration_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t378116136::get_offset_of_m_IgnoreTimeScale_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (ColorTweenMode_t4194101034)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2029[4] = 
{
	ColorTweenMode_t4194101034::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (ColorTweenCallback_t824784811), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (FloatTween_t3783157226)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2031[5] = 
{
	FloatTween_t3783157226::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_t3783157226::get_offset_of_m_StartValue_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_t3783157226::get_offset_of_m_TargetValue_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_t3783157226::get_offset_of_m_Duration_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_t3783157226::get_offset_of_m_IgnoreTimeScale_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (FloatTweenCallback_t556094317), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2033[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2034[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (TMP_DefaultControls_t2837752704), -1, sizeof(TMP_DefaultControls_t2837752704_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2035[7] = 
{
	0,
	0,
	0,
	TMP_DefaultControls_t2837752704_StaticFields::get_offset_of_s_ThickElementSize_3(),
	TMP_DefaultControls_t2837752704_StaticFields::get_offset_of_s_ThinElementSize_4(),
	TMP_DefaultControls_t2837752704_StaticFields::get_offset_of_s_DefaultSelectableColor_5(),
	TMP_DefaultControls_t2837752704_StaticFields::get_offset_of_s_TextColor_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (Resources_t2155109485)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2036[7] = 
{
	Resources_t2155109485::get_offset_of_standard_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Resources_t2155109485::get_offset_of_background_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Resources_t2155109485::get_offset_of_inputField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Resources_t2155109485::get_offset_of_knob_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Resources_t2155109485::get_offset_of_checkmark_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Resources_t2155109485::get_offset_of_dropdown_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Resources_t2155109485::get_offset_of_mask_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (TMP_Dropdown_t3024694699), -1, sizeof(TMP_Dropdown_t3024694699_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2037[14] = 
{
	TMP_Dropdown_t3024694699::get_offset_of_m_Template_16(),
	TMP_Dropdown_t3024694699::get_offset_of_m_CaptionText_17(),
	TMP_Dropdown_t3024694699::get_offset_of_m_CaptionImage_18(),
	TMP_Dropdown_t3024694699::get_offset_of_m_ItemText_19(),
	TMP_Dropdown_t3024694699::get_offset_of_m_ItemImage_20(),
	TMP_Dropdown_t3024694699::get_offset_of_m_Value_21(),
	TMP_Dropdown_t3024694699::get_offset_of_m_Options_22(),
	TMP_Dropdown_t3024694699::get_offset_of_m_OnValueChanged_23(),
	TMP_Dropdown_t3024694699::get_offset_of_m_Dropdown_24(),
	TMP_Dropdown_t3024694699::get_offset_of_m_Blocker_25(),
	TMP_Dropdown_t3024694699::get_offset_of_m_Items_26(),
	TMP_Dropdown_t3024694699::get_offset_of_m_AlphaTweenRunner_27(),
	TMP_Dropdown_t3024694699::get_offset_of_validTemplate_28(),
	TMP_Dropdown_t3024694699_StaticFields::get_offset_of_s_NoOptionData_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (DropdownItem_t1842596711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2038[4] = 
{
	DropdownItem_t1842596711::get_offset_of_m_Text_2(),
	DropdownItem_t1842596711::get_offset_of_m_Image_3(),
	DropdownItem_t1842596711::get_offset_of_m_RectTransform_4(),
	DropdownItem_t1842596711::get_offset_of_m_Toggle_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (OptionData_t1114640268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2039[2] = 
{
	OptionData_t1114640268::get_offset_of_m_Text_0(),
	OptionData_t1114640268::get_offset_of_m_Image_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (OptionDataList_t2293557512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2040[1] = 
{
	OptionDataList_t2293557512::get_offset_of_m_Options_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (DropdownEvent_t1704673280), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (U3CU3Ec__DisplayClass56_0_t2378633237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2042[2] = 
{
	U3CU3Ec__DisplayClass56_0_t2378633237::get_offset_of_item_0(),
	U3CU3Ec__DisplayClass56_0_t2378633237::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (U3CDelayedDestroyDropdownListU3Ed__68_t3448141726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2043[5] = 
{
	U3CDelayedDestroyDropdownListU3Ed__68_t3448141726::get_offset_of_U3CU3E1__state_0(),
	U3CDelayedDestroyDropdownListU3Ed__68_t3448141726::get_offset_of_U3CU3E2__current_1(),
	U3CDelayedDestroyDropdownListU3Ed__68_t3448141726::get_offset_of_delay_2(),
	U3CDelayedDestroyDropdownListU3Ed__68_t3448141726::get_offset_of_U3CU3E4__this_3(),
	U3CDelayedDestroyDropdownListU3Ed__68_t3448141726::get_offset_of_U3CiU3E5__1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (TMP_FontWeights_t916301067)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2044[2] = 
{
	TMP_FontWeights_t916301067::get_offset_of_regularTypeface_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_FontWeights_t916301067::get_offset_of_italicTypeface_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (TMP_FontAsset_t364381626), -1, sizeof(TMP_FontAsset_t364381626_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2045[20] = 
{
	TMP_FontAsset_t364381626_StaticFields::get_offset_of_s_defaultFontAsset_5(),
	TMP_FontAsset_t364381626::get_offset_of_fontAssetType_6(),
	TMP_FontAsset_t364381626::get_offset_of_m_fontInfo_7(),
	TMP_FontAsset_t364381626::get_offset_of_atlas_8(),
	TMP_FontAsset_t364381626::get_offset_of_m_glyphInfoList_9(),
	TMP_FontAsset_t364381626::get_offset_of_m_characterDictionary_10(),
	TMP_FontAsset_t364381626::get_offset_of_m_kerningDictionary_11(),
	TMP_FontAsset_t364381626::get_offset_of_m_kerningInfo_12(),
	TMP_FontAsset_t364381626::get_offset_of_m_kerningPair_13(),
	TMP_FontAsset_t364381626::get_offset_of_fallbackFontAssets_14(),
	TMP_FontAsset_t364381626::get_offset_of_fontCreationSettings_15(),
	TMP_FontAsset_t364381626::get_offset_of_fontWeights_16(),
	TMP_FontAsset_t364381626::get_offset_of_m_characterSet_17(),
	TMP_FontAsset_t364381626::get_offset_of_normalStyle_18(),
	TMP_FontAsset_t364381626::get_offset_of_normalSpacingOffset_19(),
	TMP_FontAsset_t364381626::get_offset_of_boldStyle_20(),
	TMP_FontAsset_t364381626::get_offset_of_boldSpacing_21(),
	TMP_FontAsset_t364381626::get_offset_of_italicStyle_22(),
	TMP_FontAsset_t364381626::get_offset_of_tabSize_23(),
	TMP_FontAsset_t364381626::get_offset_of_m_oldTabSize_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (FontAssetTypes_t1222456209)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2046[4] = 
{
	FontAssetTypes_t1222456209::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (U3CU3Ec_t2662116725), -1, sizeof(U3CU3Ec_t2662116725_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2047[3] = 
{
	U3CU3Ec_t2662116725_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t2662116725_StaticFields::get_offset_of_U3CU3E9__34_0_1(),
	U3CU3Ec_t2662116725_StaticFields::get_offset_of_U3CU3E9__37_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (TMP_InputField_t1099764886), -1, sizeof(TMP_InputField_t1099764886_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2048[76] = 
{
	TMP_InputField_t1099764886::get_offset_of_m_Keyboard_16(),
	TMP_InputField_t1099764886_StaticFields::get_offset_of_kSeparators_17(),
	TMP_InputField_t1099764886::get_offset_of_m_TextViewport_18(),
	TMP_InputField_t1099764886::get_offset_of_m_TextComponent_19(),
	TMP_InputField_t1099764886::get_offset_of_m_TextComponentRectTransform_20(),
	TMP_InputField_t1099764886::get_offset_of_m_Placeholder_21(),
	TMP_InputField_t1099764886::get_offset_of_m_VerticalScrollbar_22(),
	TMP_InputField_t1099764886::get_offset_of_m_VerticalScrollbarEventHandler_23(),
	TMP_InputField_t1099764886::get_offset_of_m_ScrollPosition_24(),
	TMP_InputField_t1099764886::get_offset_of_m_ScrollSensitivity_25(),
	TMP_InputField_t1099764886::get_offset_of_m_ContentType_26(),
	TMP_InputField_t1099764886::get_offset_of_m_InputType_27(),
	TMP_InputField_t1099764886::get_offset_of_m_AsteriskChar_28(),
	TMP_InputField_t1099764886::get_offset_of_m_KeyboardType_29(),
	TMP_InputField_t1099764886::get_offset_of_m_LineType_30(),
	TMP_InputField_t1099764886::get_offset_of_m_HideMobileInput_31(),
	TMP_InputField_t1099764886::get_offset_of_m_CharacterValidation_32(),
	TMP_InputField_t1099764886::get_offset_of_m_RegexValue_33(),
	TMP_InputField_t1099764886::get_offset_of_m_GlobalPointSize_34(),
	TMP_InputField_t1099764886::get_offset_of_m_CharacterLimit_35(),
	TMP_InputField_t1099764886::get_offset_of_m_OnEndEdit_36(),
	TMP_InputField_t1099764886::get_offset_of_m_OnSubmit_37(),
	TMP_InputField_t1099764886::get_offset_of_m_OnSelect_38(),
	TMP_InputField_t1099764886::get_offset_of_m_OnDeselect_39(),
	TMP_InputField_t1099764886::get_offset_of_m_OnTextSelection_40(),
	TMP_InputField_t1099764886::get_offset_of_m_OnEndTextSelection_41(),
	TMP_InputField_t1099764886::get_offset_of_m_OnValueChanged_42(),
	TMP_InputField_t1099764886::get_offset_of_m_OnValidateInput_43(),
	TMP_InputField_t1099764886::get_offset_of_m_CaretColor_44(),
	TMP_InputField_t1099764886::get_offset_of_m_CustomCaretColor_45(),
	TMP_InputField_t1099764886::get_offset_of_m_SelectionColor_46(),
	TMP_InputField_t1099764886::get_offset_of_m_Text_47(),
	TMP_InputField_t1099764886::get_offset_of_m_CaretBlinkRate_48(),
	TMP_InputField_t1099764886::get_offset_of_m_CaretWidth_49(),
	TMP_InputField_t1099764886::get_offset_of_m_ReadOnly_50(),
	TMP_InputField_t1099764886::get_offset_of_m_RichText_51(),
	TMP_InputField_t1099764886::get_offset_of_m_StringPosition_52(),
	TMP_InputField_t1099764886::get_offset_of_m_StringSelectPosition_53(),
	TMP_InputField_t1099764886::get_offset_of_m_CaretPosition_54(),
	TMP_InputField_t1099764886::get_offset_of_m_CaretSelectPosition_55(),
	TMP_InputField_t1099764886::get_offset_of_caretRectTrans_56(),
	TMP_InputField_t1099764886::get_offset_of_m_CursorVerts_57(),
	TMP_InputField_t1099764886::get_offset_of_m_CachedInputRenderer_58(),
	TMP_InputField_t1099764886::get_offset_of_m_DefaultTransformPosition_59(),
	TMP_InputField_t1099764886::get_offset_of_m_LastPosition_60(),
	TMP_InputField_t1099764886::get_offset_of_m_Mesh_61(),
	TMP_InputField_t1099764886::get_offset_of_m_AllowInput_62(),
	TMP_InputField_t1099764886::get_offset_of_m_ShouldActivateNextUpdate_63(),
	TMP_InputField_t1099764886::get_offset_of_m_UpdateDrag_64(),
	TMP_InputField_t1099764886::get_offset_of_m_DragPositionOutOfBounds_65(),
	0,
	0,
	TMP_InputField_t1099764886::get_offset_of_m_CaretVisible_68(),
	TMP_InputField_t1099764886::get_offset_of_m_BlinkCoroutine_69(),
	TMP_InputField_t1099764886::get_offset_of_m_BlinkStartTime_70(),
	TMP_InputField_t1099764886::get_offset_of_m_DragCoroutine_71(),
	TMP_InputField_t1099764886::get_offset_of_m_OriginalText_72(),
	TMP_InputField_t1099764886::get_offset_of_m_WasCanceled_73(),
	TMP_InputField_t1099764886::get_offset_of_m_HasDoneFocusTransition_74(),
	TMP_InputField_t1099764886::get_offset_of_m_IsScrollbarUpdateRequired_75(),
	TMP_InputField_t1099764886::get_offset_of_m_IsUpdatingScrollbarValues_76(),
	TMP_InputField_t1099764886::get_offset_of_m_isLastKeyBackspace_77(),
	TMP_InputField_t1099764886::get_offset_of_m_ClickStartTime_78(),
	TMP_InputField_t1099764886::get_offset_of_m_DoubleClickDelay_79(),
	0,
	TMP_InputField_t1099764886::get_offset_of_m_GlobalFontAsset_81(),
	TMP_InputField_t1099764886::get_offset_of_m_OnFocusSelectAll_82(),
	TMP_InputField_t1099764886::get_offset_of_m_isSelectAll_83(),
	TMP_InputField_t1099764886::get_offset_of_m_ResetOnDeActivation_84(),
	TMP_InputField_t1099764886::get_offset_of_m_RestoreOriginalTextOnEscape_85(),
	TMP_InputField_t1099764886::get_offset_of_m_isRichTextEditingAllowed_86(),
	TMP_InputField_t1099764886::get_offset_of_m_InputValidator_87(),
	TMP_InputField_t1099764886::get_offset_of_m_isSelected_88(),
	TMP_InputField_t1099764886::get_offset_of_isStringPositionDirty_89(),
	TMP_InputField_t1099764886::get_offset_of_m_forceRectTransformAdjustment_90(),
	TMP_InputField_t1099764886::get_offset_of_m_ProcessingEvent_91(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (ContentType_t1128941285)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2049[11] = 
{
	ContentType_t1128941285::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (InputType_t2510134850)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2050[4] = 
{
	InputType_t2510134850::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (CharacterValidation_t3801396403)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2051[10] = 
{
	CharacterValidation_t3801396403::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (LineType_t2817627283)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2052[4] = 
{
	LineType_t2817627283::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (OnValidateInput_t373909109), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (SubmitEvent_t1343580625), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (OnChangeEvent_t4257774360), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (SelectionEvent_t4268942288), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (TextSelectionEvent_t1023421581), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (EditState_t2966235475)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2058[3] = 
{
	EditState_t2966235475::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (U3CCaretBlinkU3Ed__238_t3270037332), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2059[5] = 
{
	U3CCaretBlinkU3Ed__238_t3270037332::get_offset_of_U3CU3E1__state_0(),
	U3CCaretBlinkU3Ed__238_t3270037332::get_offset_of_U3CU3E2__current_1(),
	U3CCaretBlinkU3Ed__238_t3270037332::get_offset_of_U3CU3E4__this_2(),
	U3CCaretBlinkU3Ed__238_t3270037332::get_offset_of_U3CblinkPeriodU3E5__1_3(),
	U3CCaretBlinkU3Ed__238_t3270037332::get_offset_of_U3CblinkStateU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (U3CMouseDragOutsideRectU3Ed__255_t2073557366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2060[7] = 
{
	U3CMouseDragOutsideRectU3Ed__255_t2073557366::get_offset_of_U3CU3E1__state_0(),
	U3CMouseDragOutsideRectU3Ed__255_t2073557366::get_offset_of_U3CU3E2__current_1(),
	U3CMouseDragOutsideRectU3Ed__255_t2073557366::get_offset_of_eventData_2(),
	U3CMouseDragOutsideRectU3Ed__255_t2073557366::get_offset_of_U3CU3E4__this_3(),
	U3CMouseDragOutsideRectU3Ed__255_t2073557366::get_offset_of_U3ClocalMousePosU3E5__1_4(),
	U3CMouseDragOutsideRectU3Ed__255_t2073557366::get_offset_of_U3CrectU3E5__2_5(),
	U3CMouseDragOutsideRectU3Ed__255_t2073557366::get_offset_of_U3CdelayU3E5__3_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (SetPropertyUtility_t2931591262), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (TMP_InputValidator_t1385053824), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (TMP_LineInfo_t1079631636)+ sizeof (RuntimeObject), sizeof(TMP_LineInfo_t1079631636 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2063[19] = 
{
	TMP_LineInfo_t1079631636::get_offset_of_characterCount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_visibleCharacterCount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_spaceCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_wordCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_firstCharacterIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_firstVisibleCharacterIndex_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_lastCharacterIndex_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_lastVisibleCharacterIndex_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_length_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_lineHeight_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_ascender_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_baseline_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_descender_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_maxAdvance_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_width_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_marginLeft_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_marginRight_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_alignment_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_lineExtents_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2064[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2065[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (TMP_MaterialManager_t2944071966), -1, sizeof(TMP_MaterialManager_t2944071966_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2066[5] = 
{
	TMP_MaterialManager_t2944071966_StaticFields::get_offset_of_m_materialList_0(),
	TMP_MaterialManager_t2944071966_StaticFields::get_offset_of_m_fallbackMaterials_1(),
	TMP_MaterialManager_t2944071966_StaticFields::get_offset_of_m_fallbackMaterialLookup_2(),
	TMP_MaterialManager_t2944071966_StaticFields::get_offset_of_m_fallbackCleanupList_3(),
	TMP_MaterialManager_t2944071966_StaticFields::get_offset_of_isFallbackListDirty_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (FallbackMaterial_t4171378819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2067[5] = 
{
	FallbackMaterial_t4171378819::get_offset_of_baseID_0(),
	FallbackMaterial_t4171378819::get_offset_of_baseMaterial_1(),
	FallbackMaterial_t4171378819::get_offset_of_fallbackID_2(),
	FallbackMaterial_t4171378819::get_offset_of_fallbackMaterial_3(),
	FallbackMaterial_t4171378819::get_offset_of_count_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (MaskingMaterial_t16896275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2068[4] = 
{
	MaskingMaterial_t16896275::get_offset_of_baseMaterial_0(),
	MaskingMaterial_t16896275::get_offset_of_stencilMaterial_1(),
	MaskingMaterial_t16896275::get_offset_of_count_2(),
	MaskingMaterial_t16896275::get_offset_of_stencilID_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (U3CU3Ec__DisplayClass10_0_t173182959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2069[1] = 
{
	U3CU3Ec__DisplayClass10_0_t173182959::get_offset_of_stencilMaterial_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (U3CU3Ec__DisplayClass12_0_t173182957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2070[1] = 
{
	U3CU3Ec__DisplayClass12_0_t173182957::get_offset_of_stencilMaterial_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (U3CU3Ec__DisplayClass13_0_t173182956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2071[1] = 
{
	U3CU3Ec__DisplayClass13_0_t173182956::get_offset_of_stencilMaterial_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (U3CU3Ec__DisplayClass14_0_t173182963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2072[1] = 
{
	U3CU3Ec__DisplayClass14_0_t173182963::get_offset_of_baseMaterial_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (VertexSortingOrder_t2659893934)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2073[3] = 
{
	VertexSortingOrder_t2659893934::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (TMP_MeshInfo_t2771747634)+ sizeof (RuntimeObject), -1, sizeof(TMP_MeshInfo_t2771747634_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2074[12] = 
{
	TMP_MeshInfo_t2771747634_StaticFields::get_offset_of_s_DefaultColor_0(),
	TMP_MeshInfo_t2771747634_StaticFields::get_offset_of_s_DefaultNormal_1(),
	TMP_MeshInfo_t2771747634_StaticFields::get_offset_of_s_DefaultTangent_2(),
	TMP_MeshInfo_t2771747634::get_offset_of_mesh_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t2771747634::get_offset_of_vertexCount_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t2771747634::get_offset_of_vertices_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t2771747634::get_offset_of_normals_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t2771747634::get_offset_of_tangents_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t2771747634::get_offset_of_uvs0_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t2771747634::get_offset_of_uvs2_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t2771747634::get_offset_of_colors32_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t2771747634::get_offset_of_triangles_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2075[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (TMP_PhoneNumberValidator_t743649728), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (TMP_ScrollbarEventHandler_t374199898), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2077[1] = 
{
	TMP_ScrollbarEventHandler_t374199898::get_offset_of_isSelected_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (TMP_SelectionCaret_t407257285), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (TMP_Settings_t2071156274), -1, sizeof(TMP_Settings_t2071156274_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2079[25] = 
{
	TMP_Settings_t2071156274_StaticFields::get_offset_of_s_Instance_2(),
	TMP_Settings_t2071156274::get_offset_of_m_enableWordWrapping_3(),
	TMP_Settings_t2071156274::get_offset_of_m_enableKerning_4(),
	TMP_Settings_t2071156274::get_offset_of_m_enableExtraPadding_5(),
	TMP_Settings_t2071156274::get_offset_of_m_enableTintAllSprites_6(),
	TMP_Settings_t2071156274::get_offset_of_m_enableParseEscapeCharacters_7(),
	TMP_Settings_t2071156274::get_offset_of_m_missingGlyphCharacter_8(),
	TMP_Settings_t2071156274::get_offset_of_m_warningsDisabled_9(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultFontAsset_10(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultFontAssetPath_11(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultFontSize_12(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultAutoSizeMinRatio_13(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultAutoSizeMaxRatio_14(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultTextMeshProTextContainerSize_15(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultTextMeshProUITextContainerSize_16(),
	TMP_Settings_t2071156274::get_offset_of_m_autoSizeTextContainer_17(),
	TMP_Settings_t2071156274::get_offset_of_m_fallbackFontAssets_18(),
	TMP_Settings_t2071156274::get_offset_of_m_matchMaterialPreset_19(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultSpriteAsset_20(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultSpriteAssetPath_21(),
	TMP_Settings_t2071156274::get_offset_of_m_enableEmojiSupport_22(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultStyleSheet_23(),
	TMP_Settings_t2071156274::get_offset_of_m_leadingCharacters_24(),
	TMP_Settings_t2071156274::get_offset_of_m_followingCharacters_25(),
	TMP_Settings_t2071156274::get_offset_of_m_linebreakingRules_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (LineBreakingTable_t1457697482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2080[2] = 
{
	LineBreakingTable_t1457697482::get_offset_of_leadingCharacters_0(),
	LineBreakingTable_t1457697482::get_offset_of_followingCharacters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (TMP_Sprite_t554067146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2081[5] = 
{
	TMP_Sprite_t554067146::get_offset_of_name_9(),
	TMP_Sprite_t554067146::get_offset_of_hashCode_10(),
	TMP_Sprite_t554067146::get_offset_of_unicode_11(),
	TMP_Sprite_t554067146::get_offset_of_pivot_12(),
	TMP_Sprite_t554067146::get_offset_of_sprite_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (TMP_SpriteAnimator_t2836635477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2082[2] = 
{
	TMP_SpriteAnimator_t2836635477::get_offset_of_m_animations_2(),
	TMP_SpriteAnimator_t2836635477::get_offset_of_m_TextComponent_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (U3CDoSpriteAnimationInternalU3Ed__7_t549195582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2083[28] = 
{
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CU3E1__state_0(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CU3E2__current_1(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_currentCharacter_2(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_spriteAsset_3(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_start_4(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_end_5(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_framerate_6(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CU3E4__this_7(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CcurrentFrameU3E5__1_8(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CcharInfoU3E5__2_9(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CmaterialIndexU3E5__3_10(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CvertexIndexU3E5__4_11(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CmeshInfoU3E5__5_12(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CelapsedTimeU3E5__6_13(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CtargetTimeU3E5__7_14(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CspriteU3E5__8_15(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CverticesU3E5__9_16(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CoriginU3E5__10_17(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CspriteScaleU3E5__11_18(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CblU3E5__12_19(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CtlU3E5__13_20(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CtrU3E5__14_21(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CbrU3E5__15_22(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3Cuvs0U3E5__16_23(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3Cuv0U3E5__17_24(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3Cuv1U3E5__18_25(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3Cuv2U3E5__19_26(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3Cuv3U3E5__20_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (TMP_SpriteAsset_t484820633), -1, sizeof(TMP_SpriteAsset_t484820633_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2084[7] = 
{
	TMP_SpriteAsset_t484820633::get_offset_of_m_UnicodeLookup_5(),
	TMP_SpriteAsset_t484820633::get_offset_of_m_NameLookup_6(),
	TMP_SpriteAsset_t484820633_StaticFields::get_offset_of_m_defaultSpriteAsset_7(),
	TMP_SpriteAsset_t484820633::get_offset_of_spriteSheet_8(),
	TMP_SpriteAsset_t484820633::get_offset_of_spriteInfoList_9(),
	TMP_SpriteAsset_t484820633::get_offset_of_m_SpriteUnicodeLookup_10(),
	TMP_SpriteAsset_t484820633::get_offset_of_fallbackSpriteAssets_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (TMP_Style_t3479380764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2085[6] = 
{
	TMP_Style_t3479380764::get_offset_of_m_Name_0(),
	TMP_Style_t3479380764::get_offset_of_m_HashCode_1(),
	TMP_Style_t3479380764::get_offset_of_m_OpeningDefinition_2(),
	TMP_Style_t3479380764::get_offset_of_m_ClosingDefinition_3(),
	TMP_Style_t3479380764::get_offset_of_m_OpeningTagArray_4(),
	TMP_Style_t3479380764::get_offset_of_m_ClosingTagArray_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (TMP_StyleSheet_t917564226), -1, sizeof(TMP_StyleSheet_t917564226_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2086[3] = 
{
	TMP_StyleSheet_t917564226_StaticFields::get_offset_of_s_Instance_2(),
	TMP_StyleSheet_t917564226::get_offset_of_m_StyleList_3(),
	TMP_StyleSheet_t917564226::get_offset_of_m_StyleDictionary_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (TMP_SubMesh_t2613037997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2087[14] = 
{
	TMP_SubMesh_t2613037997::get_offset_of_m_fontAsset_2(),
	TMP_SubMesh_t2613037997::get_offset_of_m_spriteAsset_3(),
	TMP_SubMesh_t2613037997::get_offset_of_m_material_4(),
	TMP_SubMesh_t2613037997::get_offset_of_m_sharedMaterial_5(),
	TMP_SubMesh_t2613037997::get_offset_of_m_fallbackMaterial_6(),
	TMP_SubMesh_t2613037997::get_offset_of_m_fallbackSourceMaterial_7(),
	TMP_SubMesh_t2613037997::get_offset_of_m_isDefaultMaterial_8(),
	TMP_SubMesh_t2613037997::get_offset_of_m_padding_9(),
	TMP_SubMesh_t2613037997::get_offset_of_m_renderer_10(),
	TMP_SubMesh_t2613037997::get_offset_of_m_meshFilter_11(),
	TMP_SubMesh_t2613037997::get_offset_of_m_mesh_12(),
	TMP_SubMesh_t2613037997::get_offset_of_m_boxCollider_13(),
	TMP_SubMesh_t2613037997::get_offset_of_m_TextComponent_14(),
	TMP_SubMesh_t2613037997::get_offset_of_m_isRegisteredForEvents_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (TMP_SubMeshUI_t1578871311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2088[14] = 
{
	TMP_SubMeshUI_t1578871311::get_offset_of_m_fontAsset_28(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_spriteAsset_29(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_material_30(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_sharedMaterial_31(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_fallbackMaterial_32(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_fallbackSourceMaterial_33(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_isDefaultMaterial_34(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_padding_35(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_canvasRenderer_36(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_mesh_37(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_TextComponent_38(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_isRegisteredForEvents_39(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_materialDirty_40(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_materialReferenceIndex_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (TextAlignmentOptions_t4036791236)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2090[37] = 
{
	TextAlignmentOptions_t4036791236::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (_HorizontalAlignmentOptions_t2379014378)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2091[7] = 
{
	_HorizontalAlignmentOptions_t2379014378::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (_VerticalAlignmentOptions_t2825528260)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2092[7] = 
{
	_VerticalAlignmentOptions_t2825528260::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (TextRenderFlags_t2418684345)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2093[3] = 
{
	TextRenderFlags_t2418684345::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (TMP_TextElementType_t1276645592)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2094[3] = 
{
	TMP_TextElementType_t1276645592::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (MaskingTypes_t3687969768)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2095[4] = 
{
	MaskingTypes_t3687969768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (TextOverflowModes_t1430035314)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2096[8] = 
{
	TextOverflowModes_t1430035314::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (MaskingOffsetMode_t2266644590)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2097[3] = 
{
	MaskingOffsetMode_t2266644590::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (TextureMappingOptions_t270963663)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2098[5] = 
{
	TextureMappingOptions_t270963663::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (FontStyles_t3828945032)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2099[12] = 
{
	FontStyles_t3828945032::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
