﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Manager
struct Manager_t2928243666;
// System.String
struct String_t;
// TMPro.Examples.Benchmark01
struct Benchmark01_t1571072624;
// TMPro.TMP_Text
struct TMP_Text_t2599618874;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_t3598145122;
// TMPro.Examples.TextConsoleSimulator
struct TextConsoleSimulator_t3766250034;
// TMPro.Examples.TeleType
struct TeleType_t2409835159;
// TMPro.Examples.ShaderPropAnimator
struct ShaderPropAnimator_t3617420994;
// RPGCharacterControllerFREE
struct RPGCharacterControllerFREE_t3036052416;
// TweenAnimation.Models.Curve
struct Curve_t1446801592;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Action
struct Action_t1264377477;
// TMPro.Examples.Benchmark01_UGUI
struct Benchmark01_UGUI_t3264177817;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Void
struct Void_t1185182177;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Char[]
struct CharU5BU5D_t3528271667;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// TMPro.Examples.SkewTextExample
struct SkewTextExample_t3460249701;
// TMPro.Examples.TextMeshProFloatingText
struct TextMeshProFloatingText_t845872552;
// EnvMapAnimator
struct EnvMapAnimator_t1140999784;
// UnityEngine.Material
struct Material_t340375123;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t529313277;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Camera
struct Camera_t4157153871;
// TMPro.TMP_MeshInfo[]
struct TMP_MeshInfoU5BU5D_t3365986247;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.Material[]
struct MaterialU5BU5D_t561872642;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.Collections.Generic.List`1<TweenAnimation.Models.TweenAnimation>
struct List_1_t3050851472;
// System.Func`2<TweenAnimation.Models.TweenAnimation,System.Boolean>
struct Func_2_t1832049457;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.AI.NavMeshAgent
struct NavMeshAgent_t1276799816;
// UnityEngine.SphereCollider
struct SphereCollider_t2077223608;
// UnityEngine.Collider
struct Collider_t1773347010;
// TMPro.TextMeshPro
struct TextMeshPro_t2393593166;
// UnityEngine.Font
struct Font_t1956802104;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// TMPro.TMP_InputField
struct TMP_InputField_t1099764886;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t1494447233;
// UnityEngine.TextMesh
struct TextMesh_t1536577757;
// TMPro.TMP_TextEventHandler/CharacterSelectionEvent
struct CharacterSelectionEvent_t3109943174;
// TMPro.TMP_TextEventHandler/WordSelectionEvent
struct WordSelectionEvent_t1841909953;
// TMPro.TMP_TextEventHandler/LineSelectionEvent
struct LineSelectionEvent_t2868010532;
// TMPro.TMP_TextEventHandler/LinkSelectionEvent
struct LinkSelectionEvent_t1590929858;
// TMPro.TMP_TextEventHandler
struct TMP_TextEventHandler_t1869054637;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t898892918;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t364381626;
// TMPro.TextContainer
struct TextContainer_t97923372;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CSTARTANIMU3EC__ITERATOR0_T710289490_H
#define U3CSTARTANIMU3EC__ITERATOR0_T710289490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Manager/<startAnim>c__Iterator0
struct  U3CstartAnimU3Ec__Iterator0_t710289490  : public RuntimeObject
{
public:
	// System.Int32 Manager/<startAnim>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// System.Single Manager/<startAnim>c__Iterator0::<randSec>__2
	float ___U3CrandSecU3E__2_1;
	// Manager Manager/<startAnim>c__Iterator0::$this
	Manager_t2928243666 * ___U24this_2;
	// System.Object Manager/<startAnim>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Manager/<startAnim>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 Manager/<startAnim>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CstartAnimU3Ec__Iterator0_t710289490, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U3CrandSecU3E__2_1() { return static_cast<int32_t>(offsetof(U3CstartAnimU3Ec__Iterator0_t710289490, ___U3CrandSecU3E__2_1)); }
	inline float get_U3CrandSecU3E__2_1() const { return ___U3CrandSecU3E__2_1; }
	inline float* get_address_of_U3CrandSecU3E__2_1() { return &___U3CrandSecU3E__2_1; }
	inline void set_U3CrandSecU3E__2_1(float value)
	{
		___U3CrandSecU3E__2_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CstartAnimU3Ec__Iterator0_t710289490, ___U24this_2)); }
	inline Manager_t2928243666 * get_U24this_2() const { return ___U24this_2; }
	inline Manager_t2928243666 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(Manager_t2928243666 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CstartAnimU3Ec__Iterator0_t710289490, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CstartAnimU3Ec__Iterator0_t710289490, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CstartAnimU3Ec__Iterator0_t710289490, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTANIMU3EC__ITERATOR0_T710289490_H
#ifndef CONSTANTS_T701097383_H
#define CONSTANTS_T701097383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Constants
struct  Constants_t701097383  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTANTS_T701097383_H
#ifndef UIPANELS_T1090734791_H
#define UIPANELS_T1090734791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Constants/UIPanels
struct  UIPanels_t1090734791  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPANELS_T1090734791_H
#ifndef U3CSTARTU3EC__ITERATOR0_T2216151886_H
#define U3CSTARTU3EC__ITERATOR0_T2216151886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t2216151886  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.Benchmark01/<Start>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// TMPro.Examples.Benchmark01 TMPro.Examples.Benchmark01/<Start>c__Iterator0::$this
	Benchmark01_t1571072624 * ___U24this_1;
	// System.Object TMPro.Examples.Benchmark01/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.Examples.Benchmark01/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.Examples.Benchmark01/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24this_1)); }
	inline Benchmark01_t1571072624 * get_U24this_1() const { return ___U24this_1; }
	inline Benchmark01_t1571072624 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Benchmark01_t1571072624 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T2216151886_H
#ifndef U3CREVEALCHARACTERSU3EC__ITERATOR0_T860191687_H
#define U3CREVEALCHARACTERSU3EC__ITERATOR0_T860191687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0
struct  U3CRevealCharactersU3Ec__Iterator0_t860191687  : public RuntimeObject
{
public:
	// TMPro.TMP_Text TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::textComponent
	TMP_Text_t2599618874 * ___textComponent_0;
	// TMPro.TMP_TextInfo TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::<textInfo>__0
	TMP_TextInfo_t3598145122 * ___U3CtextInfoU3E__0_1;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::<totalVisibleCharacters>__0
	int32_t ___U3CtotalVisibleCharactersU3E__0_2;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::<visibleCount>__0
	int32_t ___U3CvisibleCountU3E__0_3;
	// TMPro.Examples.TextConsoleSimulator TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$this
	TextConsoleSimulator_t3766250034 * ___U24this_4;
	// System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___textComponent_0)); }
	inline TMP_Text_t2599618874 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t2599618874 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t2599618874 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U3CtextInfoU3E__0_1)); }
	inline TMP_TextInfo_t3598145122 * get_U3CtextInfoU3E__0_1() const { return ___U3CtextInfoU3E__0_1; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_U3CtextInfoU3E__0_1() { return &___U3CtextInfoU3E__0_1; }
	inline void set_U3CtextInfoU3E__0_1(TMP_TextInfo_t3598145122 * value)
	{
		___U3CtextInfoU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CtotalVisibleCharactersU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U3CtotalVisibleCharactersU3E__0_2)); }
	inline int32_t get_U3CtotalVisibleCharactersU3E__0_2() const { return ___U3CtotalVisibleCharactersU3E__0_2; }
	inline int32_t* get_address_of_U3CtotalVisibleCharactersU3E__0_2() { return &___U3CtotalVisibleCharactersU3E__0_2; }
	inline void set_U3CtotalVisibleCharactersU3E__0_2(int32_t value)
	{
		___U3CtotalVisibleCharactersU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CvisibleCountU3E__0_3() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U3CvisibleCountU3E__0_3)); }
	inline int32_t get_U3CvisibleCountU3E__0_3() const { return ___U3CvisibleCountU3E__0_3; }
	inline int32_t* get_address_of_U3CvisibleCountU3E__0_3() { return &___U3CvisibleCountU3E__0_3; }
	inline void set_U3CvisibleCountU3E__0_3(int32_t value)
	{
		___U3CvisibleCountU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U24this_4)); }
	inline TextConsoleSimulator_t3766250034 * get_U24this_4() const { return ___U24this_4; }
	inline TextConsoleSimulator_t3766250034 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(TextConsoleSimulator_t3766250034 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREVEALCHARACTERSU3EC__ITERATOR0_T860191687_H
#ifndef U3CSTARTU3EC__ITERATOR0_T3341539328_H
#define U3CSTARTU3EC__ITERATOR0_T3341539328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TeleType/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t3341539328  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::<totalVisibleCharacters>__0
	int32_t ___U3CtotalVisibleCharactersU3E__0_0;
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::<counter>__0
	int32_t ___U3CcounterU3E__0_1;
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::<visibleCount>__0
	int32_t ___U3CvisibleCountU3E__0_2;
	// TMPro.Examples.TeleType TMPro.Examples.TeleType/<Start>c__Iterator0::$this
	TeleType_t2409835159 * ___U24this_3;
	// System.Object TMPro.Examples.TeleType/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean TMPro.Examples.TeleType/<Start>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CtotalVisibleCharactersU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U3CtotalVisibleCharactersU3E__0_0)); }
	inline int32_t get_U3CtotalVisibleCharactersU3E__0_0() const { return ___U3CtotalVisibleCharactersU3E__0_0; }
	inline int32_t* get_address_of_U3CtotalVisibleCharactersU3E__0_0() { return &___U3CtotalVisibleCharactersU3E__0_0; }
	inline void set_U3CtotalVisibleCharactersU3E__0_0(int32_t value)
	{
		___U3CtotalVisibleCharactersU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CcounterU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U3CcounterU3E__0_1)); }
	inline int32_t get_U3CcounterU3E__0_1() const { return ___U3CcounterU3E__0_1; }
	inline int32_t* get_address_of_U3CcounterU3E__0_1() { return &___U3CcounterU3E__0_1; }
	inline void set_U3CcounterU3E__0_1(int32_t value)
	{
		___U3CcounterU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CvisibleCountU3E__0_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U3CvisibleCountU3E__0_2)); }
	inline int32_t get_U3CvisibleCountU3E__0_2() const { return ___U3CvisibleCountU3E__0_2; }
	inline int32_t* get_address_of_U3CvisibleCountU3E__0_2() { return &___U3CvisibleCountU3E__0_2; }
	inline void set_U3CvisibleCountU3E__0_2(int32_t value)
	{
		___U3CvisibleCountU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U24this_3)); }
	inline TeleType_t2409835159 * get_U24this_3() const { return ___U24this_3; }
	inline TeleType_t2409835159 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(TeleType_t2409835159 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T3341539328_H
#ifndef U3CANIMATEPROPERTIESU3EC__ITERATOR0_T4041402054_H
#define U3CANIMATEPROPERTIESU3EC__ITERATOR0_T4041402054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0
struct  U3CAnimatePropertiesU3Ec__Iterator0_t4041402054  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::<glowPower>__1
	float ___U3CglowPowerU3E__1_0;
	// TMPro.Examples.ShaderPropAnimator TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$this
	ShaderPropAnimator_t3617420994 * ___U24this_1;
	// System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CglowPowerU3E__1_0() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U3CglowPowerU3E__1_0)); }
	inline float get_U3CglowPowerU3E__1_0() const { return ___U3CglowPowerU3E__1_0; }
	inline float* get_address_of_U3CglowPowerU3E__1_0() { return &___U3CglowPowerU3E__1_0; }
	inline void set_U3CglowPowerU3E__1_0(float value)
	{
		___U3CglowPowerU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U24this_1)); }
	inline ShaderPropAnimator_t3617420994 * get_U24this_1() const { return ___U24this_1; }
	inline ShaderPropAnimator_t3617420994 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ShaderPropAnimator_t3617420994 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEPROPERTIESU3EC__ITERATOR0_T4041402054_H
#ifndef U3C_JUMPU3EC__ITERATOR0_T3419148950_H
#define U3C_JUMPU3EC__ITERATOR0_T3419148950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RPGCharacterControllerFREE/<_Jump>c__Iterator0
struct  U3C_JumpU3Ec__Iterator0_t3419148950  : public RuntimeObject
{
public:
	// RPGCharacterControllerFREE RPGCharacterControllerFREE/<_Jump>c__Iterator0::$this
	RPGCharacterControllerFREE_t3036052416 * ___U24this_0;
	// System.Object RPGCharacterControllerFREE/<_Jump>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean RPGCharacterControllerFREE/<_Jump>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 RPGCharacterControllerFREE/<_Jump>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3C_JumpU3Ec__Iterator0_t3419148950, ___U24this_0)); }
	inline RPGCharacterControllerFREE_t3036052416 * get_U24this_0() const { return ___U24this_0; }
	inline RPGCharacterControllerFREE_t3036052416 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(RPGCharacterControllerFREE_t3036052416 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3C_JumpU3Ec__Iterator0_t3419148950, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3C_JumpU3Ec__Iterator0_t3419148950, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3C_JumpU3Ec__Iterator0_t3419148950, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_JUMPU3EC__ITERATOR0_T3419148950_H
#ifndef U3C_DIRECTIONALROLLU3EC__ITERATOR5_T1563546577_H
#define U3C_DIRECTIONALROLLU3EC__ITERATOR5_T1563546577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RPGCharacterControllerFREE/<_DirectionalRoll>c__Iterator5
struct  U3C_DirectionalRollU3Ec__Iterator5_t1563546577  : public RuntimeObject
{
public:
	// System.Single RPGCharacterControllerFREE/<_DirectionalRoll>c__Iterator5::<angle>__0
	float ___U3CangleU3E__0_0;
	// System.Single RPGCharacterControllerFREE/<_DirectionalRoll>c__Iterator5::<sign>__0
	float ___U3CsignU3E__0_1;
	// System.Single RPGCharacterControllerFREE/<_DirectionalRoll>c__Iterator5::<signed_angle>__0
	float ___U3Csigned_angleU3E__0_2;
	// System.Single RPGCharacterControllerFREE/<_DirectionalRoll>c__Iterator5::<angle360>__0
	float ___U3Cangle360U3E__0_3;
	// RPGCharacterControllerFREE RPGCharacterControllerFREE/<_DirectionalRoll>c__Iterator5::$this
	RPGCharacterControllerFREE_t3036052416 * ___U24this_4;
	// System.Object RPGCharacterControllerFREE/<_DirectionalRoll>c__Iterator5::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean RPGCharacterControllerFREE/<_DirectionalRoll>c__Iterator5::$disposing
	bool ___U24disposing_6;
	// System.Int32 RPGCharacterControllerFREE/<_DirectionalRoll>c__Iterator5::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CangleU3E__0_0() { return static_cast<int32_t>(offsetof(U3C_DirectionalRollU3Ec__Iterator5_t1563546577, ___U3CangleU3E__0_0)); }
	inline float get_U3CangleU3E__0_0() const { return ___U3CangleU3E__0_0; }
	inline float* get_address_of_U3CangleU3E__0_0() { return &___U3CangleU3E__0_0; }
	inline void set_U3CangleU3E__0_0(float value)
	{
		___U3CangleU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CsignU3E__0_1() { return static_cast<int32_t>(offsetof(U3C_DirectionalRollU3Ec__Iterator5_t1563546577, ___U3CsignU3E__0_1)); }
	inline float get_U3CsignU3E__0_1() const { return ___U3CsignU3E__0_1; }
	inline float* get_address_of_U3CsignU3E__0_1() { return &___U3CsignU3E__0_1; }
	inline void set_U3CsignU3E__0_1(float value)
	{
		___U3CsignU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3Csigned_angleU3E__0_2() { return static_cast<int32_t>(offsetof(U3C_DirectionalRollU3Ec__Iterator5_t1563546577, ___U3Csigned_angleU3E__0_2)); }
	inline float get_U3Csigned_angleU3E__0_2() const { return ___U3Csigned_angleU3E__0_2; }
	inline float* get_address_of_U3Csigned_angleU3E__0_2() { return &___U3Csigned_angleU3E__0_2; }
	inline void set_U3Csigned_angleU3E__0_2(float value)
	{
		___U3Csigned_angleU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3Cangle360U3E__0_3() { return static_cast<int32_t>(offsetof(U3C_DirectionalRollU3Ec__Iterator5_t1563546577, ___U3Cangle360U3E__0_3)); }
	inline float get_U3Cangle360U3E__0_3() const { return ___U3Cangle360U3E__0_3; }
	inline float* get_address_of_U3Cangle360U3E__0_3() { return &___U3Cangle360U3E__0_3; }
	inline void set_U3Cangle360U3E__0_3(float value)
	{
		___U3Cangle360U3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3C_DirectionalRollU3Ec__Iterator5_t1563546577, ___U24this_4)); }
	inline RPGCharacterControllerFREE_t3036052416 * get_U24this_4() const { return ___U24this_4; }
	inline RPGCharacterControllerFREE_t3036052416 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(RPGCharacterControllerFREE_t3036052416 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3C_DirectionalRollU3Ec__Iterator5_t1563546577, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3C_DirectionalRollU3Ec__Iterator5_t1563546577, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3C_DirectionalRollU3Ec__Iterator5_t1563546577, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_DIRECTIONALROLLU3EC__ITERATOR5_T1563546577_H
#ifndef U3C_DEATHU3EC__ITERATOR3_T2107953850_H
#define U3C_DEATHU3EC__ITERATOR3_T2107953850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RPGCharacterControllerFREE/<_Death>c__Iterator3
struct  U3C_DeathU3Ec__Iterator3_t2107953850  : public RuntimeObject
{
public:
	// RPGCharacterControllerFREE RPGCharacterControllerFREE/<_Death>c__Iterator3::$this
	RPGCharacterControllerFREE_t3036052416 * ___U24this_0;
	// System.Object RPGCharacterControllerFREE/<_Death>c__Iterator3::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean RPGCharacterControllerFREE/<_Death>c__Iterator3::$disposing
	bool ___U24disposing_2;
	// System.Int32 RPGCharacterControllerFREE/<_Death>c__Iterator3::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3C_DeathU3Ec__Iterator3_t2107953850, ___U24this_0)); }
	inline RPGCharacterControllerFREE_t3036052416 * get_U24this_0() const { return ___U24this_0; }
	inline RPGCharacterControllerFREE_t3036052416 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(RPGCharacterControllerFREE_t3036052416 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3C_DeathU3Ec__Iterator3_t2107953850, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3C_DeathU3Ec__Iterator3_t2107953850, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3C_DeathU3Ec__Iterator3_t2107953850, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_DEATHU3EC__ITERATOR3_T2107953850_H
#ifndef U3C_REVIVEU3EC__ITERATOR4_T660281937_H
#define U3C_REVIVEU3EC__ITERATOR4_T660281937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RPGCharacterControllerFREE/<_Revive>c__Iterator4
struct  U3C_ReviveU3Ec__Iterator4_t660281937  : public RuntimeObject
{
public:
	// RPGCharacterControllerFREE RPGCharacterControllerFREE/<_Revive>c__Iterator4::$this
	RPGCharacterControllerFREE_t3036052416 * ___U24this_0;
	// System.Object RPGCharacterControllerFREE/<_Revive>c__Iterator4::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean RPGCharacterControllerFREE/<_Revive>c__Iterator4::$disposing
	bool ___U24disposing_2;
	// System.Int32 RPGCharacterControllerFREE/<_Revive>c__Iterator4::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3C_ReviveU3Ec__Iterator4_t660281937, ___U24this_0)); }
	inline RPGCharacterControllerFREE_t3036052416 * get_U24this_0() const { return ___U24this_0; }
	inline RPGCharacterControllerFREE_t3036052416 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(RPGCharacterControllerFREE_t3036052416 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3C_ReviveU3Ec__Iterator4_t660281937, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3C_ReviveU3Ec__Iterator4_t660281937, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3C_ReviveU3Ec__Iterator4_t660281937, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_REVIVEU3EC__ITERATOR4_T660281937_H
#ifndef APPLICATIONMODEL_T3549425075_H
#define APPLICATIONMODEL_T3549425075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApplicationModel
struct  ApplicationModel_t3549425075  : public RuntimeObject
{
public:

public:
};

struct ApplicationModel_t3549425075_StaticFields
{
public:
	// System.Boolean ApplicationModel::isPaused
	bool ___isPaused_0;

public:
	inline static int32_t get_offset_of_isPaused_0() { return static_cast<int32_t>(offsetof(ApplicationModel_t3549425075_StaticFields, ___isPaused_0)); }
	inline bool get_isPaused_0() const { return ___isPaused_0; }
	inline bool* get_address_of_isPaused_0() { return &___isPaused_0; }
	inline void set_isPaused_0(bool value)
	{
		___isPaused_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONMODEL_T3549425075_H
#ifndef TWEENANIMATION_T1578776730_H
#define TWEENANIMATION_T1578776730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.TweenAnimation
struct  TweenAnimation_t1578776730  : public RuntimeObject
{
public:
	// System.Int32 TweenAnimation.Models.TweenAnimation::frameCount
	int32_t ___frameCount_0;
	// TweenAnimation.Models.Curve TweenAnimation.Models.TweenAnimation::curve
	Curve_t1446801592 * ___curve_1;
	// System.Single TweenAnimation.Models.TweenAnimation::duration
	float ___duration_2;
	// System.Single TweenAnimation.Models.TweenAnimation::delay
	float ___delay_3;
	// System.Single TweenAnimation.Models.TweenAnimation::startTime
	float ___startTime_4;
	// UnityEngine.Transform TweenAnimation.Models.TweenAnimation::transform
	Transform_t3600365921 * ___transform_5;
	// System.Boolean TweenAnimation.Models.TweenAnimation::isFinished
	bool ___isFinished_6;
	// System.Action TweenAnimation.Models.TweenAnimation::finishCallback
	Action_t1264377477 * ___finishCallback_7;
	// System.Action TweenAnimation.Models.TweenAnimation::startCallback
	Action_t1264377477 * ___startCallback_8;

public:
	inline static int32_t get_offset_of_frameCount_0() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___frameCount_0)); }
	inline int32_t get_frameCount_0() const { return ___frameCount_0; }
	inline int32_t* get_address_of_frameCount_0() { return &___frameCount_0; }
	inline void set_frameCount_0(int32_t value)
	{
		___frameCount_0 = value;
	}

	inline static int32_t get_offset_of_curve_1() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___curve_1)); }
	inline Curve_t1446801592 * get_curve_1() const { return ___curve_1; }
	inline Curve_t1446801592 ** get_address_of_curve_1() { return &___curve_1; }
	inline void set_curve_1(Curve_t1446801592 * value)
	{
		___curve_1 = value;
		Il2CppCodeGenWriteBarrier((&___curve_1), value);
	}

	inline static int32_t get_offset_of_duration_2() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___duration_2)); }
	inline float get_duration_2() const { return ___duration_2; }
	inline float* get_address_of_duration_2() { return &___duration_2; }
	inline void set_duration_2(float value)
	{
		___duration_2 = value;
	}

	inline static int32_t get_offset_of_delay_3() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___delay_3)); }
	inline float get_delay_3() const { return ___delay_3; }
	inline float* get_address_of_delay_3() { return &___delay_3; }
	inline void set_delay_3(float value)
	{
		___delay_3 = value;
	}

	inline static int32_t get_offset_of_startTime_4() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___startTime_4)); }
	inline float get_startTime_4() const { return ___startTime_4; }
	inline float* get_address_of_startTime_4() { return &___startTime_4; }
	inline void set_startTime_4(float value)
	{
		___startTime_4 = value;
	}

	inline static int32_t get_offset_of_transform_5() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___transform_5)); }
	inline Transform_t3600365921 * get_transform_5() const { return ___transform_5; }
	inline Transform_t3600365921 ** get_address_of_transform_5() { return &___transform_5; }
	inline void set_transform_5(Transform_t3600365921 * value)
	{
		___transform_5 = value;
		Il2CppCodeGenWriteBarrier((&___transform_5), value);
	}

	inline static int32_t get_offset_of_isFinished_6() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___isFinished_6)); }
	inline bool get_isFinished_6() const { return ___isFinished_6; }
	inline bool* get_address_of_isFinished_6() { return &___isFinished_6; }
	inline void set_isFinished_6(bool value)
	{
		___isFinished_6 = value;
	}

	inline static int32_t get_offset_of_finishCallback_7() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___finishCallback_7)); }
	inline Action_t1264377477 * get_finishCallback_7() const { return ___finishCallback_7; }
	inline Action_t1264377477 ** get_address_of_finishCallback_7() { return &___finishCallback_7; }
	inline void set_finishCallback_7(Action_t1264377477 * value)
	{
		___finishCallback_7 = value;
		Il2CppCodeGenWriteBarrier((&___finishCallback_7), value);
	}

	inline static int32_t get_offset_of_startCallback_8() { return static_cast<int32_t>(offsetof(TweenAnimation_t1578776730, ___startCallback_8)); }
	inline Action_t1264377477 * get_startCallback_8() const { return ___startCallback_8; }
	inline Action_t1264377477 ** get_address_of_startCallback_8() { return &___startCallback_8; }
	inline void set_startCallback_8(Action_t1264377477 * value)
	{
		___startCallback_8 = value;
		Il2CppCodeGenWriteBarrier((&___startCallback_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENANIMATION_T1578776730_H
#ifndef U3C_ROLLU3EC__ITERATOR6_T1187791279_H
#define U3C_ROLLU3EC__ITERATOR6_T1187791279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RPGCharacterControllerFREE/<_Roll>c__Iterator6
struct  U3C_RollU3Ec__Iterator6_t1187791279  : public RuntimeObject
{
public:
	// System.Int32 RPGCharacterControllerFREE/<_Roll>c__Iterator6::rollNumber
	int32_t ___rollNumber_0;
	// RPGCharacterControllerFREE RPGCharacterControllerFREE/<_Roll>c__Iterator6::$this
	RPGCharacterControllerFREE_t3036052416 * ___U24this_1;
	// System.Object RPGCharacterControllerFREE/<_Roll>c__Iterator6::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean RPGCharacterControllerFREE/<_Roll>c__Iterator6::$disposing
	bool ___U24disposing_3;
	// System.Int32 RPGCharacterControllerFREE/<_Roll>c__Iterator6::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_rollNumber_0() { return static_cast<int32_t>(offsetof(U3C_RollU3Ec__Iterator6_t1187791279, ___rollNumber_0)); }
	inline int32_t get_rollNumber_0() const { return ___rollNumber_0; }
	inline int32_t* get_address_of_rollNumber_0() { return &___rollNumber_0; }
	inline void set_rollNumber_0(int32_t value)
	{
		___rollNumber_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3C_RollU3Ec__Iterator6_t1187791279, ___U24this_1)); }
	inline RPGCharacterControllerFREE_t3036052416 * get_U24this_1() const { return ___U24this_1; }
	inline RPGCharacterControllerFREE_t3036052416 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(RPGCharacterControllerFREE_t3036052416 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3C_RollU3Ec__Iterator6_t1187791279, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3C_RollU3Ec__Iterator6_t1187791279, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3C_RollU3Ec__Iterator6_t1187791279, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_ROLLU3EC__ITERATOR6_T1187791279_H
#ifndef U3C_LOCKMOVEMENTANDATTACKU3EC__ITERATOR7_T3589827545_H
#define U3C_LOCKMOVEMENTANDATTACKU3EC__ITERATOR7_T3589827545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RPGCharacterControllerFREE/<_LockMovementAndAttack>c__Iterator7
struct  U3C_LockMovementAndAttackU3Ec__Iterator7_t3589827545  : public RuntimeObject
{
public:
	// System.Single RPGCharacterControllerFREE/<_LockMovementAndAttack>c__Iterator7::delayTime
	float ___delayTime_0;
	// System.Single RPGCharacterControllerFREE/<_LockMovementAndAttack>c__Iterator7::lockTime
	float ___lockTime_1;
	// RPGCharacterControllerFREE RPGCharacterControllerFREE/<_LockMovementAndAttack>c__Iterator7::$this
	RPGCharacterControllerFREE_t3036052416 * ___U24this_2;
	// System.Object RPGCharacterControllerFREE/<_LockMovementAndAttack>c__Iterator7::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean RPGCharacterControllerFREE/<_LockMovementAndAttack>c__Iterator7::$disposing
	bool ___U24disposing_4;
	// System.Int32 RPGCharacterControllerFREE/<_LockMovementAndAttack>c__Iterator7::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_delayTime_0() { return static_cast<int32_t>(offsetof(U3C_LockMovementAndAttackU3Ec__Iterator7_t3589827545, ___delayTime_0)); }
	inline float get_delayTime_0() const { return ___delayTime_0; }
	inline float* get_address_of_delayTime_0() { return &___delayTime_0; }
	inline void set_delayTime_0(float value)
	{
		___delayTime_0 = value;
	}

	inline static int32_t get_offset_of_lockTime_1() { return static_cast<int32_t>(offsetof(U3C_LockMovementAndAttackU3Ec__Iterator7_t3589827545, ___lockTime_1)); }
	inline float get_lockTime_1() const { return ___lockTime_1; }
	inline float* get_address_of_lockTime_1() { return &___lockTime_1; }
	inline void set_lockTime_1(float value)
	{
		___lockTime_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3C_LockMovementAndAttackU3Ec__Iterator7_t3589827545, ___U24this_2)); }
	inline RPGCharacterControllerFREE_t3036052416 * get_U24this_2() const { return ___U24this_2; }
	inline RPGCharacterControllerFREE_t3036052416 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(RPGCharacterControllerFREE_t3036052416 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3C_LockMovementAndAttackU3Ec__Iterator7_t3589827545, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3C_LockMovementAndAttackU3Ec__Iterator7_t3589827545, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3C_LockMovementAndAttackU3Ec__Iterator7_t3589827545, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_LOCKMOVEMENTANDATTACKU3EC__ITERATOR7_T3589827545_H
#ifndef U3CSTARTU3EC__ITERATOR0_T2622988697_H
#define U3CSTARTU3EC__ITERATOR0_T2622988697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t2622988697  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// TMPro.Examples.Benchmark01_UGUI TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$this
	Benchmark01_UGUI_t3264177817 * ___U24this_1;
	// System.Object TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24this_1)); }
	inline Benchmark01_UGUI_t3264177817 * get_U24this_1() const { return ___U24this_1; }
	inline Benchmark01_UGUI_t3264177817 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Benchmark01_UGUI_t3264177817 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T2622988697_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef CURVE_T1446801592_H
#define CURVE_T1446801592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve
struct  Curve_t1446801592  : public RuntimeObject
{
public:

public:
};

struct Curve_t1446801592_StaticFields
{
public:
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::Linear
	Curve_t1446801592 * ___Linear_0;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::CubicEaseIn
	Curve_t1446801592 * ___CubicEaseIn_1;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::CubicEaseOut
	Curve_t1446801592 * ___CubicEaseOut_2;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::CubicEaseInOut
	Curve_t1446801592 * ___CubicEaseInOut_3;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::QuarticEaseIn
	Curve_t1446801592 * ___QuarticEaseIn_4;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::QuarticEaseOut
	Curve_t1446801592 * ___QuarticEaseOut_5;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::QuarticEaseInOut
	Curve_t1446801592 * ___QuarticEaseInOut_6;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::QuadricEaseIn
	Curve_t1446801592 * ___QuadricEaseIn_7;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::QuadricEaseOut
	Curve_t1446801592 * ___QuadricEaseOut_8;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::QuadricEaseInOut
	Curve_t1446801592 * ___QuadricEaseInOut_9;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::BounceEaseOut
	Curve_t1446801592 * ___BounceEaseOut_10;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::BounceEaseIn
	Curve_t1446801592 * ___BounceEaseIn_11;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::BounceEaseInOut
	Curve_t1446801592 * ___BounceEaseInOut_12;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::BackEaseIn
	Curve_t1446801592 * ___BackEaseIn_13;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::BackEaseOut
	Curve_t1446801592 * ___BackEaseOut_14;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::BackEaseInOut
	Curve_t1446801592 * ___BackEaseInOut_15;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::SineEaseIn
	Curve_t1446801592 * ___SineEaseIn_16;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::SineEaseOut
	Curve_t1446801592 * ___SineEaseOut_17;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::SineEaseInOut
	Curve_t1446801592 * ___SineEaseInOut_18;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::ElasticEaseIn
	Curve_t1446801592 * ___ElasticEaseIn_19;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::ElasticEaseOut
	Curve_t1446801592 * ___ElasticEaseOut_20;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::ElasticEaseInOut
	Curve_t1446801592 * ___ElasticEaseInOut_21;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::CircularEaseIn
	Curve_t1446801592 * ___CircularEaseIn_22;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::CircularEaseOut
	Curve_t1446801592 * ___CircularEaseOut_23;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::CircularEaseInOut
	Curve_t1446801592 * ___CircularEaseInOut_24;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::ExponentialEaseIn
	Curve_t1446801592 * ___ExponentialEaseIn_25;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::ExponentialEaseOut
	Curve_t1446801592 * ___ExponentialEaseOut_26;
	// TweenAnimation.Models.Curve TweenAnimation.Models.Curve::ExponentialEaseInOut
	Curve_t1446801592 * ___ExponentialEaseInOut_27;

public:
	inline static int32_t get_offset_of_Linear_0() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___Linear_0)); }
	inline Curve_t1446801592 * get_Linear_0() const { return ___Linear_0; }
	inline Curve_t1446801592 ** get_address_of_Linear_0() { return &___Linear_0; }
	inline void set_Linear_0(Curve_t1446801592 * value)
	{
		___Linear_0 = value;
		Il2CppCodeGenWriteBarrier((&___Linear_0), value);
	}

	inline static int32_t get_offset_of_CubicEaseIn_1() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___CubicEaseIn_1)); }
	inline Curve_t1446801592 * get_CubicEaseIn_1() const { return ___CubicEaseIn_1; }
	inline Curve_t1446801592 ** get_address_of_CubicEaseIn_1() { return &___CubicEaseIn_1; }
	inline void set_CubicEaseIn_1(Curve_t1446801592 * value)
	{
		___CubicEaseIn_1 = value;
		Il2CppCodeGenWriteBarrier((&___CubicEaseIn_1), value);
	}

	inline static int32_t get_offset_of_CubicEaseOut_2() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___CubicEaseOut_2)); }
	inline Curve_t1446801592 * get_CubicEaseOut_2() const { return ___CubicEaseOut_2; }
	inline Curve_t1446801592 ** get_address_of_CubicEaseOut_2() { return &___CubicEaseOut_2; }
	inline void set_CubicEaseOut_2(Curve_t1446801592 * value)
	{
		___CubicEaseOut_2 = value;
		Il2CppCodeGenWriteBarrier((&___CubicEaseOut_2), value);
	}

	inline static int32_t get_offset_of_CubicEaseInOut_3() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___CubicEaseInOut_3)); }
	inline Curve_t1446801592 * get_CubicEaseInOut_3() const { return ___CubicEaseInOut_3; }
	inline Curve_t1446801592 ** get_address_of_CubicEaseInOut_3() { return &___CubicEaseInOut_3; }
	inline void set_CubicEaseInOut_3(Curve_t1446801592 * value)
	{
		___CubicEaseInOut_3 = value;
		Il2CppCodeGenWriteBarrier((&___CubicEaseInOut_3), value);
	}

	inline static int32_t get_offset_of_QuarticEaseIn_4() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___QuarticEaseIn_4)); }
	inline Curve_t1446801592 * get_QuarticEaseIn_4() const { return ___QuarticEaseIn_4; }
	inline Curve_t1446801592 ** get_address_of_QuarticEaseIn_4() { return &___QuarticEaseIn_4; }
	inline void set_QuarticEaseIn_4(Curve_t1446801592 * value)
	{
		___QuarticEaseIn_4 = value;
		Il2CppCodeGenWriteBarrier((&___QuarticEaseIn_4), value);
	}

	inline static int32_t get_offset_of_QuarticEaseOut_5() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___QuarticEaseOut_5)); }
	inline Curve_t1446801592 * get_QuarticEaseOut_5() const { return ___QuarticEaseOut_5; }
	inline Curve_t1446801592 ** get_address_of_QuarticEaseOut_5() { return &___QuarticEaseOut_5; }
	inline void set_QuarticEaseOut_5(Curve_t1446801592 * value)
	{
		___QuarticEaseOut_5 = value;
		Il2CppCodeGenWriteBarrier((&___QuarticEaseOut_5), value);
	}

	inline static int32_t get_offset_of_QuarticEaseInOut_6() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___QuarticEaseInOut_6)); }
	inline Curve_t1446801592 * get_QuarticEaseInOut_6() const { return ___QuarticEaseInOut_6; }
	inline Curve_t1446801592 ** get_address_of_QuarticEaseInOut_6() { return &___QuarticEaseInOut_6; }
	inline void set_QuarticEaseInOut_6(Curve_t1446801592 * value)
	{
		___QuarticEaseInOut_6 = value;
		Il2CppCodeGenWriteBarrier((&___QuarticEaseInOut_6), value);
	}

	inline static int32_t get_offset_of_QuadricEaseIn_7() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___QuadricEaseIn_7)); }
	inline Curve_t1446801592 * get_QuadricEaseIn_7() const { return ___QuadricEaseIn_7; }
	inline Curve_t1446801592 ** get_address_of_QuadricEaseIn_7() { return &___QuadricEaseIn_7; }
	inline void set_QuadricEaseIn_7(Curve_t1446801592 * value)
	{
		___QuadricEaseIn_7 = value;
		Il2CppCodeGenWriteBarrier((&___QuadricEaseIn_7), value);
	}

	inline static int32_t get_offset_of_QuadricEaseOut_8() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___QuadricEaseOut_8)); }
	inline Curve_t1446801592 * get_QuadricEaseOut_8() const { return ___QuadricEaseOut_8; }
	inline Curve_t1446801592 ** get_address_of_QuadricEaseOut_8() { return &___QuadricEaseOut_8; }
	inline void set_QuadricEaseOut_8(Curve_t1446801592 * value)
	{
		___QuadricEaseOut_8 = value;
		Il2CppCodeGenWriteBarrier((&___QuadricEaseOut_8), value);
	}

	inline static int32_t get_offset_of_QuadricEaseInOut_9() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___QuadricEaseInOut_9)); }
	inline Curve_t1446801592 * get_QuadricEaseInOut_9() const { return ___QuadricEaseInOut_9; }
	inline Curve_t1446801592 ** get_address_of_QuadricEaseInOut_9() { return &___QuadricEaseInOut_9; }
	inline void set_QuadricEaseInOut_9(Curve_t1446801592 * value)
	{
		___QuadricEaseInOut_9 = value;
		Il2CppCodeGenWriteBarrier((&___QuadricEaseInOut_9), value);
	}

	inline static int32_t get_offset_of_BounceEaseOut_10() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___BounceEaseOut_10)); }
	inline Curve_t1446801592 * get_BounceEaseOut_10() const { return ___BounceEaseOut_10; }
	inline Curve_t1446801592 ** get_address_of_BounceEaseOut_10() { return &___BounceEaseOut_10; }
	inline void set_BounceEaseOut_10(Curve_t1446801592 * value)
	{
		___BounceEaseOut_10 = value;
		Il2CppCodeGenWriteBarrier((&___BounceEaseOut_10), value);
	}

	inline static int32_t get_offset_of_BounceEaseIn_11() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___BounceEaseIn_11)); }
	inline Curve_t1446801592 * get_BounceEaseIn_11() const { return ___BounceEaseIn_11; }
	inline Curve_t1446801592 ** get_address_of_BounceEaseIn_11() { return &___BounceEaseIn_11; }
	inline void set_BounceEaseIn_11(Curve_t1446801592 * value)
	{
		___BounceEaseIn_11 = value;
		Il2CppCodeGenWriteBarrier((&___BounceEaseIn_11), value);
	}

	inline static int32_t get_offset_of_BounceEaseInOut_12() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___BounceEaseInOut_12)); }
	inline Curve_t1446801592 * get_BounceEaseInOut_12() const { return ___BounceEaseInOut_12; }
	inline Curve_t1446801592 ** get_address_of_BounceEaseInOut_12() { return &___BounceEaseInOut_12; }
	inline void set_BounceEaseInOut_12(Curve_t1446801592 * value)
	{
		___BounceEaseInOut_12 = value;
		Il2CppCodeGenWriteBarrier((&___BounceEaseInOut_12), value);
	}

	inline static int32_t get_offset_of_BackEaseIn_13() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___BackEaseIn_13)); }
	inline Curve_t1446801592 * get_BackEaseIn_13() const { return ___BackEaseIn_13; }
	inline Curve_t1446801592 ** get_address_of_BackEaseIn_13() { return &___BackEaseIn_13; }
	inline void set_BackEaseIn_13(Curve_t1446801592 * value)
	{
		___BackEaseIn_13 = value;
		Il2CppCodeGenWriteBarrier((&___BackEaseIn_13), value);
	}

	inline static int32_t get_offset_of_BackEaseOut_14() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___BackEaseOut_14)); }
	inline Curve_t1446801592 * get_BackEaseOut_14() const { return ___BackEaseOut_14; }
	inline Curve_t1446801592 ** get_address_of_BackEaseOut_14() { return &___BackEaseOut_14; }
	inline void set_BackEaseOut_14(Curve_t1446801592 * value)
	{
		___BackEaseOut_14 = value;
		Il2CppCodeGenWriteBarrier((&___BackEaseOut_14), value);
	}

	inline static int32_t get_offset_of_BackEaseInOut_15() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___BackEaseInOut_15)); }
	inline Curve_t1446801592 * get_BackEaseInOut_15() const { return ___BackEaseInOut_15; }
	inline Curve_t1446801592 ** get_address_of_BackEaseInOut_15() { return &___BackEaseInOut_15; }
	inline void set_BackEaseInOut_15(Curve_t1446801592 * value)
	{
		___BackEaseInOut_15 = value;
		Il2CppCodeGenWriteBarrier((&___BackEaseInOut_15), value);
	}

	inline static int32_t get_offset_of_SineEaseIn_16() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___SineEaseIn_16)); }
	inline Curve_t1446801592 * get_SineEaseIn_16() const { return ___SineEaseIn_16; }
	inline Curve_t1446801592 ** get_address_of_SineEaseIn_16() { return &___SineEaseIn_16; }
	inline void set_SineEaseIn_16(Curve_t1446801592 * value)
	{
		___SineEaseIn_16 = value;
		Il2CppCodeGenWriteBarrier((&___SineEaseIn_16), value);
	}

	inline static int32_t get_offset_of_SineEaseOut_17() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___SineEaseOut_17)); }
	inline Curve_t1446801592 * get_SineEaseOut_17() const { return ___SineEaseOut_17; }
	inline Curve_t1446801592 ** get_address_of_SineEaseOut_17() { return &___SineEaseOut_17; }
	inline void set_SineEaseOut_17(Curve_t1446801592 * value)
	{
		___SineEaseOut_17 = value;
		Il2CppCodeGenWriteBarrier((&___SineEaseOut_17), value);
	}

	inline static int32_t get_offset_of_SineEaseInOut_18() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___SineEaseInOut_18)); }
	inline Curve_t1446801592 * get_SineEaseInOut_18() const { return ___SineEaseInOut_18; }
	inline Curve_t1446801592 ** get_address_of_SineEaseInOut_18() { return &___SineEaseInOut_18; }
	inline void set_SineEaseInOut_18(Curve_t1446801592 * value)
	{
		___SineEaseInOut_18 = value;
		Il2CppCodeGenWriteBarrier((&___SineEaseInOut_18), value);
	}

	inline static int32_t get_offset_of_ElasticEaseIn_19() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___ElasticEaseIn_19)); }
	inline Curve_t1446801592 * get_ElasticEaseIn_19() const { return ___ElasticEaseIn_19; }
	inline Curve_t1446801592 ** get_address_of_ElasticEaseIn_19() { return &___ElasticEaseIn_19; }
	inline void set_ElasticEaseIn_19(Curve_t1446801592 * value)
	{
		___ElasticEaseIn_19 = value;
		Il2CppCodeGenWriteBarrier((&___ElasticEaseIn_19), value);
	}

	inline static int32_t get_offset_of_ElasticEaseOut_20() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___ElasticEaseOut_20)); }
	inline Curve_t1446801592 * get_ElasticEaseOut_20() const { return ___ElasticEaseOut_20; }
	inline Curve_t1446801592 ** get_address_of_ElasticEaseOut_20() { return &___ElasticEaseOut_20; }
	inline void set_ElasticEaseOut_20(Curve_t1446801592 * value)
	{
		___ElasticEaseOut_20 = value;
		Il2CppCodeGenWriteBarrier((&___ElasticEaseOut_20), value);
	}

	inline static int32_t get_offset_of_ElasticEaseInOut_21() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___ElasticEaseInOut_21)); }
	inline Curve_t1446801592 * get_ElasticEaseInOut_21() const { return ___ElasticEaseInOut_21; }
	inline Curve_t1446801592 ** get_address_of_ElasticEaseInOut_21() { return &___ElasticEaseInOut_21; }
	inline void set_ElasticEaseInOut_21(Curve_t1446801592 * value)
	{
		___ElasticEaseInOut_21 = value;
		Il2CppCodeGenWriteBarrier((&___ElasticEaseInOut_21), value);
	}

	inline static int32_t get_offset_of_CircularEaseIn_22() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___CircularEaseIn_22)); }
	inline Curve_t1446801592 * get_CircularEaseIn_22() const { return ___CircularEaseIn_22; }
	inline Curve_t1446801592 ** get_address_of_CircularEaseIn_22() { return &___CircularEaseIn_22; }
	inline void set_CircularEaseIn_22(Curve_t1446801592 * value)
	{
		___CircularEaseIn_22 = value;
		Il2CppCodeGenWriteBarrier((&___CircularEaseIn_22), value);
	}

	inline static int32_t get_offset_of_CircularEaseOut_23() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___CircularEaseOut_23)); }
	inline Curve_t1446801592 * get_CircularEaseOut_23() const { return ___CircularEaseOut_23; }
	inline Curve_t1446801592 ** get_address_of_CircularEaseOut_23() { return &___CircularEaseOut_23; }
	inline void set_CircularEaseOut_23(Curve_t1446801592 * value)
	{
		___CircularEaseOut_23 = value;
		Il2CppCodeGenWriteBarrier((&___CircularEaseOut_23), value);
	}

	inline static int32_t get_offset_of_CircularEaseInOut_24() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___CircularEaseInOut_24)); }
	inline Curve_t1446801592 * get_CircularEaseInOut_24() const { return ___CircularEaseInOut_24; }
	inline Curve_t1446801592 ** get_address_of_CircularEaseInOut_24() { return &___CircularEaseInOut_24; }
	inline void set_CircularEaseInOut_24(Curve_t1446801592 * value)
	{
		___CircularEaseInOut_24 = value;
		Il2CppCodeGenWriteBarrier((&___CircularEaseInOut_24), value);
	}

	inline static int32_t get_offset_of_ExponentialEaseIn_25() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___ExponentialEaseIn_25)); }
	inline Curve_t1446801592 * get_ExponentialEaseIn_25() const { return ___ExponentialEaseIn_25; }
	inline Curve_t1446801592 ** get_address_of_ExponentialEaseIn_25() { return &___ExponentialEaseIn_25; }
	inline void set_ExponentialEaseIn_25(Curve_t1446801592 * value)
	{
		___ExponentialEaseIn_25 = value;
		Il2CppCodeGenWriteBarrier((&___ExponentialEaseIn_25), value);
	}

	inline static int32_t get_offset_of_ExponentialEaseOut_26() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___ExponentialEaseOut_26)); }
	inline Curve_t1446801592 * get_ExponentialEaseOut_26() const { return ___ExponentialEaseOut_26; }
	inline Curve_t1446801592 ** get_address_of_ExponentialEaseOut_26() { return &___ExponentialEaseOut_26; }
	inline void set_ExponentialEaseOut_26(Curve_t1446801592 * value)
	{
		___ExponentialEaseOut_26 = value;
		Il2CppCodeGenWriteBarrier((&___ExponentialEaseOut_26), value);
	}

	inline static int32_t get_offset_of_ExponentialEaseInOut_27() { return static_cast<int32_t>(offsetof(Curve_t1446801592_StaticFields, ___ExponentialEaseInOut_27)); }
	inline Curve_t1446801592 * get_ExponentialEaseInOut_27() const { return ___ExponentialEaseInOut_27; }
	inline Curve_t1446801592 ** get_address_of_ExponentialEaseInOut_27() { return &___ExponentialEaseInOut_27; }
	inline void set_ExponentialEaseInOut_27(Curve_t1446801592 * value)
	{
		___ExponentialEaseInOut_27 = value;
		Il2CppCodeGenWriteBarrier((&___ExponentialEaseInOut_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVE_T1446801592_H
#ifndef U3CREVEALWORDSU3EC__ITERATOR1_T1343183262_H
#define U3CREVEALWORDSU3EC__ITERATOR1_T1343183262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1
struct  U3CRevealWordsU3Ec__Iterator1_t1343183262  : public RuntimeObject
{
public:
	// TMPro.TMP_Text TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::textComponent
	TMP_Text_t2599618874 * ___textComponent_0;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<totalWordCount>__0
	int32_t ___U3CtotalWordCountU3E__0_1;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<totalVisibleCharacters>__0
	int32_t ___U3CtotalVisibleCharactersU3E__0_2;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<counter>__0
	int32_t ___U3CcounterU3E__0_3;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<currentWord>__0
	int32_t ___U3CcurrentWordU3E__0_4;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<visibleCount>__0
	int32_t ___U3CvisibleCountU3E__0_5;
	// System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___textComponent_0)); }
	inline TMP_Text_t2599618874 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t2599618874 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t2599618874 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_U3CtotalWordCountU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U3CtotalWordCountU3E__0_1)); }
	inline int32_t get_U3CtotalWordCountU3E__0_1() const { return ___U3CtotalWordCountU3E__0_1; }
	inline int32_t* get_address_of_U3CtotalWordCountU3E__0_1() { return &___U3CtotalWordCountU3E__0_1; }
	inline void set_U3CtotalWordCountU3E__0_1(int32_t value)
	{
		___U3CtotalWordCountU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtotalVisibleCharactersU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U3CtotalVisibleCharactersU3E__0_2)); }
	inline int32_t get_U3CtotalVisibleCharactersU3E__0_2() const { return ___U3CtotalVisibleCharactersU3E__0_2; }
	inline int32_t* get_address_of_U3CtotalVisibleCharactersU3E__0_2() { return &___U3CtotalVisibleCharactersU3E__0_2; }
	inline void set_U3CtotalVisibleCharactersU3E__0_2(int32_t value)
	{
		___U3CtotalVisibleCharactersU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CcounterU3E__0_3() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U3CcounterU3E__0_3)); }
	inline int32_t get_U3CcounterU3E__0_3() const { return ___U3CcounterU3E__0_3; }
	inline int32_t* get_address_of_U3CcounterU3E__0_3() { return &___U3CcounterU3E__0_3; }
	inline void set_U3CcounterU3E__0_3(int32_t value)
	{
		___U3CcounterU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentWordU3E__0_4() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U3CcurrentWordU3E__0_4)); }
	inline int32_t get_U3CcurrentWordU3E__0_4() const { return ___U3CcurrentWordU3E__0_4; }
	inline int32_t* get_address_of_U3CcurrentWordU3E__0_4() { return &___U3CcurrentWordU3E__0_4; }
	inline void set_U3CcurrentWordU3E__0_4(int32_t value)
	{
		___U3CcurrentWordU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CvisibleCountU3E__0_5() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U3CvisibleCountU3E__0_5)); }
	inline int32_t get_U3CvisibleCountU3E__0_5() const { return ___U3CvisibleCountU3E__0_5; }
	inline int32_t* get_address_of_U3CvisibleCountU3E__0_5() { return &___U3CvisibleCountU3E__0_5; }
	inline void set_U3CvisibleCountU3E__0_5(int32_t value)
	{
		___U3CvisibleCountU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREVEALWORDSU3EC__ITERATOR1_T1343183262_H
#ifndef UNITYEVENT_3_T1597070127_H
#define UNITYEVENT_3_T1597070127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.String,System.Int32,System.Int32>
struct  UnityEvent_3_t1597070127  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t1597070127, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T1597070127_H
#ifndef UNITYEVENT_3_T2493613095_H
#define UNITYEVENT_3_T2493613095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.String,System.String,System.Int32>
struct  UnityEvent_3_t2493613095  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t2493613095, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T2493613095_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR32_T2600501292_H
#define COLOR32_T2600501292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t2600501292 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2600501292_H
#ifndef UNITYEVENT_2_T1169440328_H
#define UNITYEVENT_2_T1169440328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.Char,System.Int32>
struct  UnityEvent_2_t1169440328  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t1169440328, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T1169440328_H
#ifndef KEYVALUEPAIR_2_T3296565085_H
#define KEYVALUEPAIR_2_T3296565085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject>
struct  KeyValuePair_2_t3296565085 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	GameObject_t1113636619 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3296565085, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3296565085, ___value_1)); }
	inline GameObject_t1113636619 * get_value_1() const { return ___value_1; }
	inline GameObject_t1113636619 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(GameObject_t1113636619 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3296565085_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef QUARTICEASEINCURVE_T3162537896_H
#define QUARTICEASEINCURVE_T3162537896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/QuarticEaseInCurve
struct  QuarticEaseInCurve_t3162537896  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUARTICEASEINCURVE_T3162537896_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef ELASTICEASEOUTCURVE_T2726315200_H
#define ELASTICEASEOUTCURVE_T2726315200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/ElasticEaseOutCurve
struct  ElasticEaseOutCurve_t2726315200  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELASTICEASEOUTCURVE_T2726315200_H
#ifndef ELASTICEASEINOUTCURVE_T3360119746_H
#define ELASTICEASEINOUTCURVE_T3360119746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/ElasticEaseInOutCurve
struct  ElasticEaseInOutCurve_t3360119746  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELASTICEASEINOUTCURVE_T3360119746_H
#ifndef EXPONENTIALEASEINCURVE_T168678934_H
#define EXPONENTIALEASEINCURVE_T168678934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/ExponentialEaseInCurve
struct  ExponentialEaseInCurve_t168678934  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPONENTIALEASEINCURVE_T168678934_H
#ifndef EXPONENTIALEASEOUTCURVE_T3338962529_H
#define EXPONENTIALEASEOUTCURVE_T3338962529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/ExponentialEaseOutCurve
struct  ExponentialEaseOutCurve_t3338962529  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPONENTIALEASEOUTCURVE_T3338962529_H
#ifndef BACKEASEINOUTCURVE_T1136716490_H
#define BACKEASEINOUTCURVE_T1136716490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/BackEaseInOutCurve
struct  BackEaseInOutCurve_t1136716490  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKEASEINOUTCURVE_T1136716490_H
#ifndef ELASTICEASEINCURVE_T3556350251_H
#define ELASTICEASEINCURVE_T3556350251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/ElasticEaseInCurve
struct  ElasticEaseInCurve_t3556350251  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELASTICEASEINCURVE_T3556350251_H
#ifndef SINEEASEOUTCURVE_T349540267_H
#define SINEEASEOUTCURVE_T349540267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/SineEaseOutCurve
struct  SineEaseOutCurve_t349540267  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINEEASEOUTCURVE_T349540267_H
#ifndef SINEEASEINOUTCURVE_T3024528697_H
#define SINEEASEINOUTCURVE_T3024528697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/SineEaseInOutCurve
struct  SineEaseInOutCurve_t3024528697  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINEEASEINOUTCURVE_T3024528697_H
#ifndef CIRCULAREASEINCURVE_T2784181567_H
#define CIRCULAREASEINCURVE_T2784181567_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/CircularEaseInCurve
struct  CircularEaseInCurve_t2784181567  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIRCULAREASEINCURVE_T2784181567_H
#ifndef CIRCULAREASEINOUTCURVE_T3032629829_H
#define CIRCULAREASEINOUTCURVE_T3032629829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/CircularEaseInOutCurve
struct  CircularEaseInOutCurve_t3032629829  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIRCULAREASEINOUTCURVE_T3032629829_H
#ifndef CIRCULAREASEOUTCURVE_T3998775700_H
#define CIRCULAREASEOUTCURVE_T3998775700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/CircularEaseOutCurve
struct  CircularEaseOutCurve_t3998775700  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIRCULAREASEOUTCURVE_T3998775700_H
#ifndef SINEEASEINCURVE_T3529713798_H
#define SINEEASEINCURVE_T3529713798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/SineEaseInCurve
struct  SineEaseInCurve_t3529713798  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINEEASEINCURVE_T3529713798_H
#ifndef EXPONENTIALEASEINOUTCURVE_T802286740_H
#define EXPONENTIALEASEINOUTCURVE_T802286740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/ExponentialEaseInOutCurve
struct  ExponentialEaseInOutCurve_t802286740  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPONENTIALEASEINOUTCURVE_T802286740_H
#ifndef QUARTICEASEINOUTCURVE_T1421613264_H
#define QUARTICEASEINOUTCURVE_T1421613264_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/QuarticEaseInOutCurve
struct  QuarticEaseInOutCurve_t1421613264  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUARTICEASEINOUTCURVE_T1421613264_H
#ifndef QUADRICEASEOUTCURVE_T3528798688_H
#define QUADRICEASEOUTCURVE_T3528798688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/QuadricEaseOutCurve
struct  QuadricEaseOutCurve_t3528798688  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUADRICEASEOUTCURVE_T3528798688_H
#ifndef QUARTICEASEOUTCURVE_T3969926341_H
#define QUARTICEASEOUTCURVE_T3969926341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/QuarticEaseOutCurve
struct  QuarticEaseOutCurve_t3969926341  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUARTICEASEOUTCURVE_T3969926341_H
#ifndef QUADRICEASEINOUTCURVE_T2286750237_H
#define QUADRICEASEINOUTCURVE_T2286750237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/QuadricEaseInOutCurve
struct  QuadricEaseInOutCurve_t2286750237  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUADRICEASEINOUTCURVE_T2286750237_H
#ifndef QUADRICEASEINCURVE_T566295371_H
#define QUADRICEASEINCURVE_T566295371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/QuadricEaseInCurve
struct  QuadricEaseInCurve_t566295371  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUADRICEASEINCURVE_T566295371_H
#ifndef BACKEASEOUTCURVE_T1141847129_H
#define BACKEASEOUTCURVE_T1141847129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/BackEaseOutCurve
struct  BackEaseOutCurve_t1141847129  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKEASEOUTCURVE_T1141847129_H
#ifndef BACKEASEINCURVE_T2629419558_H
#define BACKEASEINCURVE_T2629419558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/BackEaseInCurve
struct  BackEaseInCurve_t2629419558  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKEASEINCURVE_T2629419558_H
#ifndef BOUNCEEASEINOUTCURVE_T2324510746_H
#define BOUNCEEASEINOUTCURVE_T2324510746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/BounceEaseInOutCurve
struct  BounceEaseInOutCurve_t2324510746  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNCEEASEINOUTCURVE_T2324510746_H
#ifndef BOUNCEEASEINCURVE_T2000402526_H
#define BOUNCEEASEINCURVE_T2000402526_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/BounceEaseInCurve
struct  BounceEaseInCurve_t2000402526  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNCEEASEINCURVE_T2000402526_H
#ifndef BOUNCEEASEOUTCURVE_T357678703_H
#define BOUNCEEASEOUTCURVE_T357678703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAnimation.Models.Curve/BounceEaseOutCurve
struct  BounceEaseOutCurve_t357678703  : public Curve_t1446801592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNCEEASEOUTCURVE_T357678703_H
#ifndef FPSCOUNTERANCHORPOSITIONS_T2550331785_H
#define FPSCOUNTERANCHORPOSITIONS_T2550331785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions
struct  FpsCounterAnchorPositions_t2550331785 
{
public:
	// System.Int32 TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FpsCounterAnchorPositions_t2550331785, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTERANCHORPOSITIONS_T2550331785_H
#ifndef U3CWARPTEXTU3EC__ITERATOR0_T116130919_H
#define U3CWARPTEXTU3EC__ITERATOR0_T116130919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0
struct  U3CWarpTextU3Ec__Iterator0_t116130919  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<old_CurveScale>__0
	float ___U3Cold_CurveScaleU3E__0_0;
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<old_ShearValue>__0
	float ___U3Cold_ShearValueU3E__0_1;
	// UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<old_curve>__0
	AnimationCurve_t3046754366 * ___U3Cold_curveU3E__0_2;
	// TMPro.TMP_TextInfo TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<textInfo>__1
	TMP_TextInfo_t3598145122 * ___U3CtextInfoU3E__1_3;
	// System.Int32 TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_4;
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<boundsMinX>__1
	float ___U3CboundsMinXU3E__1_5;
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<boundsMaxX>__1
	float ___U3CboundsMaxXU3E__1_6;
	// UnityEngine.Vector3[] TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<vertices>__2
	Vector3U5BU5D_t1718750761* ___U3CverticesU3E__2_7;
	// UnityEngine.Matrix4x4 TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<matrix>__2
	Matrix4x4_t1817901843  ___U3CmatrixU3E__2_8;
	// TMPro.Examples.SkewTextExample TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$this
	SkewTextExample_t3460249701 * ___U24this_9;
	// System.Object TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$current
	RuntimeObject * ___U24current_10;
	// System.Boolean TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$disposing
	bool ___U24disposing_11;
	// System.Int32 TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$PC
	int32_t ___U24PC_12;

public:
	inline static int32_t get_offset_of_U3Cold_CurveScaleU3E__0_0() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3Cold_CurveScaleU3E__0_0)); }
	inline float get_U3Cold_CurveScaleU3E__0_0() const { return ___U3Cold_CurveScaleU3E__0_0; }
	inline float* get_address_of_U3Cold_CurveScaleU3E__0_0() { return &___U3Cold_CurveScaleU3E__0_0; }
	inline void set_U3Cold_CurveScaleU3E__0_0(float value)
	{
		___U3Cold_CurveScaleU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Cold_ShearValueU3E__0_1() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3Cold_ShearValueU3E__0_1)); }
	inline float get_U3Cold_ShearValueU3E__0_1() const { return ___U3Cold_ShearValueU3E__0_1; }
	inline float* get_address_of_U3Cold_ShearValueU3E__0_1() { return &___U3Cold_ShearValueU3E__0_1; }
	inline void set_U3Cold_ShearValueU3E__0_1(float value)
	{
		___U3Cold_ShearValueU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3Cold_curveU3E__0_2() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3Cold_curveU3E__0_2)); }
	inline AnimationCurve_t3046754366 * get_U3Cold_curveU3E__0_2() const { return ___U3Cold_curveU3E__0_2; }
	inline AnimationCurve_t3046754366 ** get_address_of_U3Cold_curveU3E__0_2() { return &___U3Cold_curveU3E__0_2; }
	inline void set_U3Cold_curveU3E__0_2(AnimationCurve_t3046754366 * value)
	{
		___U3Cold_curveU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cold_curveU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E__1_3() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CtextInfoU3E__1_3)); }
	inline TMP_TextInfo_t3598145122 * get_U3CtextInfoU3E__1_3() const { return ___U3CtextInfoU3E__1_3; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_U3CtextInfoU3E__1_3() { return &___U3CtextInfoU3E__1_3; }
	inline void set_U3CtextInfoU3E__1_3(TMP_TextInfo_t3598145122 * value)
	{
		___U3CtextInfoU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_4() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CcharacterCountU3E__1_4)); }
	inline int32_t get_U3CcharacterCountU3E__1_4() const { return ___U3CcharacterCountU3E__1_4; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_4() { return &___U3CcharacterCountU3E__1_4; }
	inline void set_U3CcharacterCountU3E__1_4(int32_t value)
	{
		___U3CcharacterCountU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CboundsMinXU3E__1_5() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CboundsMinXU3E__1_5)); }
	inline float get_U3CboundsMinXU3E__1_5() const { return ___U3CboundsMinXU3E__1_5; }
	inline float* get_address_of_U3CboundsMinXU3E__1_5() { return &___U3CboundsMinXU3E__1_5; }
	inline void set_U3CboundsMinXU3E__1_5(float value)
	{
		___U3CboundsMinXU3E__1_5 = value;
	}

	inline static int32_t get_offset_of_U3CboundsMaxXU3E__1_6() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CboundsMaxXU3E__1_6)); }
	inline float get_U3CboundsMaxXU3E__1_6() const { return ___U3CboundsMaxXU3E__1_6; }
	inline float* get_address_of_U3CboundsMaxXU3E__1_6() { return &___U3CboundsMaxXU3E__1_6; }
	inline void set_U3CboundsMaxXU3E__1_6(float value)
	{
		___U3CboundsMaxXU3E__1_6 = value;
	}

	inline static int32_t get_offset_of_U3CverticesU3E__2_7() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CverticesU3E__2_7)); }
	inline Vector3U5BU5D_t1718750761* get_U3CverticesU3E__2_7() const { return ___U3CverticesU3E__2_7; }
	inline Vector3U5BU5D_t1718750761** get_address_of_U3CverticesU3E__2_7() { return &___U3CverticesU3E__2_7; }
	inline void set_U3CverticesU3E__2_7(Vector3U5BU5D_t1718750761* value)
	{
		___U3CverticesU3E__2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CverticesU3E__2_7), value);
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_8() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CmatrixU3E__2_8)); }
	inline Matrix4x4_t1817901843  get_U3CmatrixU3E__2_8() const { return ___U3CmatrixU3E__2_8; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CmatrixU3E__2_8() { return &___U3CmatrixU3E__2_8; }
	inline void set_U3CmatrixU3E__2_8(Matrix4x4_t1817901843  value)
	{
		___U3CmatrixU3E__2_8 = value;
	}

	inline static int32_t get_offset_of_U24this_9() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U24this_9)); }
	inline SkewTextExample_t3460249701 * get_U24this_9() const { return ___U24this_9; }
	inline SkewTextExample_t3460249701 ** get_address_of_U24this_9() { return &___U24this_9; }
	inline void set_U24this_9(SkewTextExample_t3460249701 * value)
	{
		___U24this_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_9), value);
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U24current_10)); }
	inline RuntimeObject * get_U24current_10() const { return ___U24current_10; }
	inline RuntimeObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(RuntimeObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_10), value);
	}

	inline static int32_t get_offset_of_U24disposing_11() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U24disposing_11)); }
	inline bool get_U24disposing_11() const { return ___U24disposing_11; }
	inline bool* get_address_of_U24disposing_11() { return &___U24disposing_11; }
	inline void set_U24disposing_11(bool value)
	{
		___U24disposing_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWARPTEXTU3EC__ITERATOR0_T116130919_H
#ifndef MOTIONTYPE_T1905163921_H
#define MOTIONTYPE_T1905163921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ObjectSpin/MotionType
struct  MotionType_t1905163921 
{
public:
	// System.Int32 TMPro.Examples.ObjectSpin/MotionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MotionType_t1905163921, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONTYPE_T1905163921_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef U3C_KNOCKBACKFORCEU3EC__ITERATOR2_T1163480401_H
#define U3C_KNOCKBACKFORCEU3EC__ITERATOR2_T1163480401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RPGCharacterControllerFREE/<_KnockbackForce>c__Iterator2
struct  U3C_KnockbackForceU3Ec__Iterator2_t1163480401  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 RPGCharacterControllerFREE/<_KnockbackForce>c__Iterator2::knockDirection
	Vector3_t3722313464  ___knockDirection_0;
	// System.Int32 RPGCharacterControllerFREE/<_KnockbackForce>c__Iterator2::knockBackAmount
	int32_t ___knockBackAmount_1;
	// System.Int32 RPGCharacterControllerFREE/<_KnockbackForce>c__Iterator2::variableAmount
	int32_t ___variableAmount_2;
	// RPGCharacterControllerFREE RPGCharacterControllerFREE/<_KnockbackForce>c__Iterator2::$this
	RPGCharacterControllerFREE_t3036052416 * ___U24this_3;
	// System.Object RPGCharacterControllerFREE/<_KnockbackForce>c__Iterator2::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean RPGCharacterControllerFREE/<_KnockbackForce>c__Iterator2::$disposing
	bool ___U24disposing_5;
	// System.Int32 RPGCharacterControllerFREE/<_KnockbackForce>c__Iterator2::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_knockDirection_0() { return static_cast<int32_t>(offsetof(U3C_KnockbackForceU3Ec__Iterator2_t1163480401, ___knockDirection_0)); }
	inline Vector3_t3722313464  get_knockDirection_0() const { return ___knockDirection_0; }
	inline Vector3_t3722313464 * get_address_of_knockDirection_0() { return &___knockDirection_0; }
	inline void set_knockDirection_0(Vector3_t3722313464  value)
	{
		___knockDirection_0 = value;
	}

	inline static int32_t get_offset_of_knockBackAmount_1() { return static_cast<int32_t>(offsetof(U3C_KnockbackForceU3Ec__Iterator2_t1163480401, ___knockBackAmount_1)); }
	inline int32_t get_knockBackAmount_1() const { return ___knockBackAmount_1; }
	inline int32_t* get_address_of_knockBackAmount_1() { return &___knockBackAmount_1; }
	inline void set_knockBackAmount_1(int32_t value)
	{
		___knockBackAmount_1 = value;
	}

	inline static int32_t get_offset_of_variableAmount_2() { return static_cast<int32_t>(offsetof(U3C_KnockbackForceU3Ec__Iterator2_t1163480401, ___variableAmount_2)); }
	inline int32_t get_variableAmount_2() const { return ___variableAmount_2; }
	inline int32_t* get_address_of_variableAmount_2() { return &___variableAmount_2; }
	inline void set_variableAmount_2(int32_t value)
	{
		___variableAmount_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3C_KnockbackForceU3Ec__Iterator2_t1163480401, ___U24this_3)); }
	inline RPGCharacterControllerFREE_t3036052416 * get_U24this_3() const { return ___U24this_3; }
	inline RPGCharacterControllerFREE_t3036052416 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(RPGCharacterControllerFREE_t3036052416 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3C_KnockbackForceU3Ec__Iterator2_t1163480401, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3C_KnockbackForceU3Ec__Iterator2_t1163480401, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3C_KnockbackForceU3Ec__Iterator2_t1163480401, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_KNOCKBACKFORCEU3EC__ITERATOR2_T1163480401_H
#ifndef WORDSELECTIONEVENT_T1841909953_H
#define WORDSELECTIONEVENT_T1841909953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/WordSelectionEvent
struct  WordSelectionEvent_t1841909953  : public UnityEvent_3_t1597070127
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDSELECTIONEVENT_T1841909953_H
#ifndef CHARACTERSELECTIONEVENT_T3109943174_H
#define CHARACTERSELECTIONEVENT_T3109943174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/CharacterSelectionEvent
struct  CharacterSelectionEvent_t3109943174  : public UnityEvent_2_t1169440328
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERSELECTIONEVENT_T3109943174_H
#ifndef LINKSELECTIONEVENT_T1590929858_H
#define LINKSELECTIONEVENT_T1590929858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/LinkSelectionEvent
struct  LinkSelectionEvent_t1590929858  : public UnityEvent_3_t2493613095
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKSELECTIONEVENT_T1590929858_H
#ifndef LINESELECTIONEVENT_T2868010532_H
#define LINESELECTIONEVENT_T2868010532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/LineSelectionEvent
struct  LineSelectionEvent_t2868010532  : public UnityEvent_3_t1597070127
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINESELECTIONEVENT_T2868010532_H
#ifndef U3CDISPLAYTEXTMESHFLOATINGTEXTU3EC__ITERATOR1_T865582314_H
#define U3CDISPLAYTEXTMESHFLOATINGTEXTU3EC__ITERATOR1_T865582314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1
struct  U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<CountDuration>__0
	float ___U3CCountDurationU3E__0_0;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<starting_Count>__0
	float ___U3Cstarting_CountU3E__0_1;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<current_Count>__0
	float ___U3Ccurrent_CountU3E__0_2;
	// UnityEngine.Vector3 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<start_pos>__0
	Vector3_t3722313464  ___U3Cstart_posU3E__0_3;
	// UnityEngine.Color32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<start_color>__0
	Color32_t2600501292  ___U3Cstart_colorU3E__0_4;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<alpha>__0
	float ___U3CalphaU3E__0_5;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<int_counter>__0
	int32_t ___U3Cint_counterU3E__0_6;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<fadeDuration>__0
	float ___U3CfadeDurationU3E__0_7;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::$this
	TextMeshProFloatingText_t845872552 * ___U24this_8;
	// System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::$current
	RuntimeObject * ___U24current_9;
	// System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::$disposing
	bool ___U24disposing_10;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_U3CCountDurationU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3CCountDurationU3E__0_0)); }
	inline float get_U3CCountDurationU3E__0_0() const { return ___U3CCountDurationU3E__0_0; }
	inline float* get_address_of_U3CCountDurationU3E__0_0() { return &___U3CCountDurationU3E__0_0; }
	inline void set_U3CCountDurationU3E__0_0(float value)
	{
		___U3CCountDurationU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Cstarting_CountU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3Cstarting_CountU3E__0_1)); }
	inline float get_U3Cstarting_CountU3E__0_1() const { return ___U3Cstarting_CountU3E__0_1; }
	inline float* get_address_of_U3Cstarting_CountU3E__0_1() { return &___U3Cstarting_CountU3E__0_1; }
	inline void set_U3Cstarting_CountU3E__0_1(float value)
	{
		___U3Cstarting_CountU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3Ccurrent_CountU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3Ccurrent_CountU3E__0_2)); }
	inline float get_U3Ccurrent_CountU3E__0_2() const { return ___U3Ccurrent_CountU3E__0_2; }
	inline float* get_address_of_U3Ccurrent_CountU3E__0_2() { return &___U3Ccurrent_CountU3E__0_2; }
	inline void set_U3Ccurrent_CountU3E__0_2(float value)
	{
		___U3Ccurrent_CountU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_posU3E__0_3() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3Cstart_posU3E__0_3)); }
	inline Vector3_t3722313464  get_U3Cstart_posU3E__0_3() const { return ___U3Cstart_posU3E__0_3; }
	inline Vector3_t3722313464 * get_address_of_U3Cstart_posU3E__0_3() { return &___U3Cstart_posU3E__0_3; }
	inline void set_U3Cstart_posU3E__0_3(Vector3_t3722313464  value)
	{
		___U3Cstart_posU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_colorU3E__0_4() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3Cstart_colorU3E__0_4)); }
	inline Color32_t2600501292  get_U3Cstart_colorU3E__0_4() const { return ___U3Cstart_colorU3E__0_4; }
	inline Color32_t2600501292 * get_address_of_U3Cstart_colorU3E__0_4() { return &___U3Cstart_colorU3E__0_4; }
	inline void set_U3Cstart_colorU3E__0_4(Color32_t2600501292  value)
	{
		___U3Cstart_colorU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CalphaU3E__0_5() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3CalphaU3E__0_5)); }
	inline float get_U3CalphaU3E__0_5() const { return ___U3CalphaU3E__0_5; }
	inline float* get_address_of_U3CalphaU3E__0_5() { return &___U3CalphaU3E__0_5; }
	inline void set_U3CalphaU3E__0_5(float value)
	{
		___U3CalphaU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U3Cint_counterU3E__0_6() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3Cint_counterU3E__0_6)); }
	inline int32_t get_U3Cint_counterU3E__0_6() const { return ___U3Cint_counterU3E__0_6; }
	inline int32_t* get_address_of_U3Cint_counterU3E__0_6() { return &___U3Cint_counterU3E__0_6; }
	inline void set_U3Cint_counterU3E__0_6(int32_t value)
	{
		___U3Cint_counterU3E__0_6 = value;
	}

	inline static int32_t get_offset_of_U3CfadeDurationU3E__0_7() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3CfadeDurationU3E__0_7)); }
	inline float get_U3CfadeDurationU3E__0_7() const { return ___U3CfadeDurationU3E__0_7; }
	inline float* get_address_of_U3CfadeDurationU3E__0_7() { return &___U3CfadeDurationU3E__0_7; }
	inline void set_U3CfadeDurationU3E__0_7(float value)
	{
		___U3CfadeDurationU3E__0_7 = value;
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U24this_8)); }
	inline TextMeshProFloatingText_t845872552 * get_U24this_8() const { return ___U24this_8; }
	inline TextMeshProFloatingText_t845872552 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(TextMeshProFloatingText_t845872552 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_8), value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U24current_9)); }
	inline RuntimeObject * get_U24current_9() const { return ___U24current_9; }
	inline RuntimeObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(RuntimeObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDISPLAYTEXTMESHFLOATINGTEXTU3EC__ITERATOR1_T865582314_H
#ifndef U3CDISPLAYTEXTMESHPROFLOATINGTEXTU3EC__ITERATOR0_T2967292235_H
#define U3CDISPLAYTEXTMESHPROFLOATINGTEXTU3EC__ITERATOR0_T2967292235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0
struct  U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<CountDuration>__0
	float ___U3CCountDurationU3E__0_0;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<starting_Count>__0
	float ___U3Cstarting_CountU3E__0_1;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<current_Count>__0
	float ___U3Ccurrent_CountU3E__0_2;
	// UnityEngine.Vector3 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<start_pos>__0
	Vector3_t3722313464  ___U3Cstart_posU3E__0_3;
	// UnityEngine.Color32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<start_color>__0
	Color32_t2600501292  ___U3Cstart_colorU3E__0_4;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<alpha>__0
	float ___U3CalphaU3E__0_5;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<fadeDuration>__0
	float ___U3CfadeDurationU3E__0_6;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::$this
	TextMeshProFloatingText_t845872552 * ___U24this_7;
	// System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_U3CCountDurationU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3CCountDurationU3E__0_0)); }
	inline float get_U3CCountDurationU3E__0_0() const { return ___U3CCountDurationU3E__0_0; }
	inline float* get_address_of_U3CCountDurationU3E__0_0() { return &___U3CCountDurationU3E__0_0; }
	inline void set_U3CCountDurationU3E__0_0(float value)
	{
		___U3CCountDurationU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Cstarting_CountU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3Cstarting_CountU3E__0_1)); }
	inline float get_U3Cstarting_CountU3E__0_1() const { return ___U3Cstarting_CountU3E__0_1; }
	inline float* get_address_of_U3Cstarting_CountU3E__0_1() { return &___U3Cstarting_CountU3E__0_1; }
	inline void set_U3Cstarting_CountU3E__0_1(float value)
	{
		___U3Cstarting_CountU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3Ccurrent_CountU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3Ccurrent_CountU3E__0_2)); }
	inline float get_U3Ccurrent_CountU3E__0_2() const { return ___U3Ccurrent_CountU3E__0_2; }
	inline float* get_address_of_U3Ccurrent_CountU3E__0_2() { return &___U3Ccurrent_CountU3E__0_2; }
	inline void set_U3Ccurrent_CountU3E__0_2(float value)
	{
		___U3Ccurrent_CountU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_posU3E__0_3() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3Cstart_posU3E__0_3)); }
	inline Vector3_t3722313464  get_U3Cstart_posU3E__0_3() const { return ___U3Cstart_posU3E__0_3; }
	inline Vector3_t3722313464 * get_address_of_U3Cstart_posU3E__0_3() { return &___U3Cstart_posU3E__0_3; }
	inline void set_U3Cstart_posU3E__0_3(Vector3_t3722313464  value)
	{
		___U3Cstart_posU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_colorU3E__0_4() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3Cstart_colorU3E__0_4)); }
	inline Color32_t2600501292  get_U3Cstart_colorU3E__0_4() const { return ___U3Cstart_colorU3E__0_4; }
	inline Color32_t2600501292 * get_address_of_U3Cstart_colorU3E__0_4() { return &___U3Cstart_colorU3E__0_4; }
	inline void set_U3Cstart_colorU3E__0_4(Color32_t2600501292  value)
	{
		___U3Cstart_colorU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CalphaU3E__0_5() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3CalphaU3E__0_5)); }
	inline float get_U3CalphaU3E__0_5() const { return ___U3CalphaU3E__0_5; }
	inline float* get_address_of_U3CalphaU3E__0_5() { return &___U3CalphaU3E__0_5; }
	inline void set_U3CalphaU3E__0_5(float value)
	{
		___U3CalphaU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U3CfadeDurationU3E__0_6() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3CfadeDurationU3E__0_6)); }
	inline float get_U3CfadeDurationU3E__0_6() const { return ___U3CfadeDurationU3E__0_6; }
	inline float* get_address_of_U3CfadeDurationU3E__0_6() { return &___U3CfadeDurationU3E__0_6; }
	inline void set_U3CfadeDurationU3E__0_6(float value)
	{
		___U3CfadeDurationU3E__0_6 = value;
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U24this_7)); }
	inline TextMeshProFloatingText_t845872552 * get_U24this_7() const { return ___U24this_7; }
	inline TextMeshProFloatingText_t845872552 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(TextMeshProFloatingText_t845872552 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDISPLAYTEXTMESHPROFLOATINGTEXTU3EC__ITERATOR0_T2967292235_H
#ifndef FPSCOUNTERANCHORPOSITIONS_T1585798158_H
#define FPSCOUNTERANCHORPOSITIONS_T1585798158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions
struct  FpsCounterAnchorPositions_t1585798158 
{
public:
	// System.Int32 TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FpsCounterAnchorPositions_t1585798158, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTERANCHORPOSITIONS_T1585798158_H
#ifndef OBJECTTYPE_T4082700821_H
#define OBJECTTYPE_T4082700821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_ExampleScript_01/objectType
struct  objectType_t4082700821 
{
public:
	// System.Int32 TMPro.Examples.TMP_ExampleScript_01/objectType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(objectType_t4082700821, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTYPE_T4082700821_H
#ifndef U3CSTARTU3EC__ITERATOR0_T1520811813_H
#define U3CSTARTU3EC__ITERATOR0_T1520811813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnvMapAnimator/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t1520811813  : public RuntimeObject
{
public:
	// UnityEngine.Matrix4x4 EnvMapAnimator/<Start>c__Iterator0::<matrix>__0
	Matrix4x4_t1817901843  ___U3CmatrixU3E__0_0;
	// EnvMapAnimator EnvMapAnimator/<Start>c__Iterator0::$this
	EnvMapAnimator_t1140999784 * ___U24this_1;
	// System.Object EnvMapAnimator/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean EnvMapAnimator/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 EnvMapAnimator/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CmatrixU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U3CmatrixU3E__0_0)); }
	inline Matrix4x4_t1817901843  get_U3CmatrixU3E__0_0() const { return ___U3CmatrixU3E__0_0; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CmatrixU3E__0_0() { return &___U3CmatrixU3E__0_0; }
	inline void set_U3CmatrixU3E__0_0(Matrix4x4_t1817901843  value)
	{
		___U3CmatrixU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U24this_1)); }
	inline EnvMapAnimator_t1140999784 * get_U24this_1() const { return ___U24this_1; }
	inline EnvMapAnimator_t1140999784 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(EnvMapAnimator_t1140999784 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T1520811813_H
#ifndef U3CHIDEPANELU3EC__ANONSTOREY0_T4128008152_H
#define U3CHIDEPANELU3EC__ANONSTOREY0_T4128008152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuScript/<HidePanel>c__AnonStorey0
struct  U3CHidePanelU3Ec__AnonStorey0_t4128008152  : public RuntimeObject
{
public:
	// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject> MenuScript/<HidePanel>c__AnonStorey0::entry
	KeyValuePair_2_t3296565085  ___entry_0;

public:
	inline static int32_t get_offset_of_entry_0() { return static_cast<int32_t>(offsetof(U3CHidePanelU3Ec__AnonStorey0_t4128008152, ___entry_0)); }
	inline KeyValuePair_2_t3296565085  get_entry_0() const { return ___entry_0; }
	inline KeyValuePair_2_t3296565085 * get_address_of_entry_0() { return &___entry_0; }
	inline void set_entry_0(KeyValuePair_2_t3296565085  value)
	{
		___entry_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHIDEPANELU3EC__ANONSTOREY0_T4128008152_H
#ifndef WEAPON_T4063826929_H
#define WEAPON_T4063826929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Weapon
struct  Weapon_t4063826929 
{
public:
	// System.Int32 Weapon::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Weapon_t4063826929, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEAPON_T4063826929_H
#ifndef PLAYERSTATE_T2716384857_H
#define PLAYERSTATE_T2716384857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MovementScript/playerState
struct  playerState_t2716384857 
{
public:
	// System.Int32 MovementScript/playerState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(playerState_t2716384857, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERSTATE_T2716384857_H
#ifndef PLAYERSTATE_T89644523_H
#define PLAYERSTATE_T89644523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Manager/playerState
struct  playerState_t89644523 
{
public:
	// System.Int32 Manager/playerState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(playerState_t89644523, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERSTATE_T89644523_H
#ifndef U3C_KNOCKBACKU3EC__ITERATOR1_T4196005666_H
#define U3C_KNOCKBACKU3EC__ITERATOR1_T4196005666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RPGCharacterControllerFREE/<_Knockback>c__Iterator1
struct  U3C_KnockbackU3Ec__Iterator1_t4196005666  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 RPGCharacterControllerFREE/<_Knockback>c__Iterator1::knockDirection
	Vector3_t3722313464  ___knockDirection_0;
	// System.Int32 RPGCharacterControllerFREE/<_Knockback>c__Iterator1::knockBackAmount
	int32_t ___knockBackAmount_1;
	// System.Int32 RPGCharacterControllerFREE/<_Knockback>c__Iterator1::variableAmount
	int32_t ___variableAmount_2;
	// RPGCharacterControllerFREE RPGCharacterControllerFREE/<_Knockback>c__Iterator1::$this
	RPGCharacterControllerFREE_t3036052416 * ___U24this_3;
	// System.Object RPGCharacterControllerFREE/<_Knockback>c__Iterator1::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean RPGCharacterControllerFREE/<_Knockback>c__Iterator1::$disposing
	bool ___U24disposing_5;
	// System.Int32 RPGCharacterControllerFREE/<_Knockback>c__Iterator1::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_knockDirection_0() { return static_cast<int32_t>(offsetof(U3C_KnockbackU3Ec__Iterator1_t4196005666, ___knockDirection_0)); }
	inline Vector3_t3722313464  get_knockDirection_0() const { return ___knockDirection_0; }
	inline Vector3_t3722313464 * get_address_of_knockDirection_0() { return &___knockDirection_0; }
	inline void set_knockDirection_0(Vector3_t3722313464  value)
	{
		___knockDirection_0 = value;
	}

	inline static int32_t get_offset_of_knockBackAmount_1() { return static_cast<int32_t>(offsetof(U3C_KnockbackU3Ec__Iterator1_t4196005666, ___knockBackAmount_1)); }
	inline int32_t get_knockBackAmount_1() const { return ___knockBackAmount_1; }
	inline int32_t* get_address_of_knockBackAmount_1() { return &___knockBackAmount_1; }
	inline void set_knockBackAmount_1(int32_t value)
	{
		___knockBackAmount_1 = value;
	}

	inline static int32_t get_offset_of_variableAmount_2() { return static_cast<int32_t>(offsetof(U3C_KnockbackU3Ec__Iterator1_t4196005666, ___variableAmount_2)); }
	inline int32_t get_variableAmount_2() const { return ___variableAmount_2; }
	inline int32_t* get_address_of_variableAmount_2() { return &___variableAmount_2; }
	inline void set_variableAmount_2(int32_t value)
	{
		___variableAmount_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3C_KnockbackU3Ec__Iterator1_t4196005666, ___U24this_3)); }
	inline RPGCharacterControllerFREE_t3036052416 * get_U24this_3() const { return ___U24this_3; }
	inline RPGCharacterControllerFREE_t3036052416 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(RPGCharacterControllerFREE_t3036052416 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3C_KnockbackU3Ec__Iterator1_t4196005666, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3C_KnockbackU3Ec__Iterator1_t4196005666, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3C_KnockbackU3Ec__Iterator1_t4196005666, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_KNOCKBACKU3EC__ITERATOR1_T4196005666_H
#ifndef CAMERAMODES_T3200559075_H
#define CAMERAMODES_T3200559075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.CameraController/CameraModes
struct  CameraModes_t3200559075 
{
public:
	// System.Int32 TMPro.Examples.CameraController/CameraModes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraModes_t3200559075, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAMODES_T3200559075_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef TMP_INPUTVALIDATOR_T1385053824_H
#define TMP_INPUTVALIDATOR_T1385053824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputValidator
struct  TMP_InputValidator_t1385053824  : public ScriptableObject_t2528358522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_INPUTVALIDATOR_T1385053824_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef TMP_DIGITVALIDATOR_T573672104_H
#define TMP_DIGITVALIDATOR_T573672104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_DigitValidator
struct  TMP_DigitValidator_t573672104  : public TMP_InputValidator_t1385053824
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_DIGITVALIDATOR_T573672104_H
#ifndef DRAGSCRIPT_T1732324504_H
#define DRAGSCRIPT_T1732324504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DragScript
struct  DragScript_t1732324504  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean DragScript::isDraggable
	bool ___isDraggable_2;
	// UnityEngine.GameObject DragScript::outline
	GameObject_t1113636619 * ___outline_3;
	// UnityEngine.GameObject DragScript::Halo
	GameObject_t1113636619 * ___Halo_4;
	// Manager DragScript::gameManager
	Manager_t2928243666 * ___gameManager_5;
	// System.Single DragScript::distance
	float ___distance_6;
	// System.Single DragScript::originalY
	float ___originalY_7;
	// System.Boolean DragScript::hasHitWater
	bool ___hasHitWater_8;

public:
	inline static int32_t get_offset_of_isDraggable_2() { return static_cast<int32_t>(offsetof(DragScript_t1732324504, ___isDraggable_2)); }
	inline bool get_isDraggable_2() const { return ___isDraggable_2; }
	inline bool* get_address_of_isDraggable_2() { return &___isDraggable_2; }
	inline void set_isDraggable_2(bool value)
	{
		___isDraggable_2 = value;
	}

	inline static int32_t get_offset_of_outline_3() { return static_cast<int32_t>(offsetof(DragScript_t1732324504, ___outline_3)); }
	inline GameObject_t1113636619 * get_outline_3() const { return ___outline_3; }
	inline GameObject_t1113636619 ** get_address_of_outline_3() { return &___outline_3; }
	inline void set_outline_3(GameObject_t1113636619 * value)
	{
		___outline_3 = value;
		Il2CppCodeGenWriteBarrier((&___outline_3), value);
	}

	inline static int32_t get_offset_of_Halo_4() { return static_cast<int32_t>(offsetof(DragScript_t1732324504, ___Halo_4)); }
	inline GameObject_t1113636619 * get_Halo_4() const { return ___Halo_4; }
	inline GameObject_t1113636619 ** get_address_of_Halo_4() { return &___Halo_4; }
	inline void set_Halo_4(GameObject_t1113636619 * value)
	{
		___Halo_4 = value;
		Il2CppCodeGenWriteBarrier((&___Halo_4), value);
	}

	inline static int32_t get_offset_of_gameManager_5() { return static_cast<int32_t>(offsetof(DragScript_t1732324504, ___gameManager_5)); }
	inline Manager_t2928243666 * get_gameManager_5() const { return ___gameManager_5; }
	inline Manager_t2928243666 ** get_address_of_gameManager_5() { return &___gameManager_5; }
	inline void set_gameManager_5(Manager_t2928243666 * value)
	{
		___gameManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___gameManager_5), value);
	}

	inline static int32_t get_offset_of_distance_6() { return static_cast<int32_t>(offsetof(DragScript_t1732324504, ___distance_6)); }
	inline float get_distance_6() const { return ___distance_6; }
	inline float* get_address_of_distance_6() { return &___distance_6; }
	inline void set_distance_6(float value)
	{
		___distance_6 = value;
	}

	inline static int32_t get_offset_of_originalY_7() { return static_cast<int32_t>(offsetof(DragScript_t1732324504, ___originalY_7)); }
	inline float get_originalY_7() const { return ___originalY_7; }
	inline float* get_address_of_originalY_7() { return &___originalY_7; }
	inline void set_originalY_7(float value)
	{
		___originalY_7 = value;
	}

	inline static int32_t get_offset_of_hasHitWater_8() { return static_cast<int32_t>(offsetof(DragScript_t1732324504, ___hasHitWater_8)); }
	inline bool get_hasHitWater_8() const { return ___hasHitWater_8; }
	inline bool* get_address_of_hasHitWater_8() { return &___hasHitWater_8; }
	inline void set_hasHitWater_8(bool value)
	{
		___hasHitWater_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGSCRIPT_T1732324504_H
#ifndef GLOWSCRIPT_T446333960_H
#define GLOWSCRIPT_T446333960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlowScript
struct  GlowScript_t446333960  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Material GlowScript::red
	Material_t340375123 * ___red_2;
	// UnityEngine.Material GlowScript::green
	Material_t340375123 * ___green_3;

public:
	inline static int32_t get_offset_of_red_2() { return static_cast<int32_t>(offsetof(GlowScript_t446333960, ___red_2)); }
	inline Material_t340375123 * get_red_2() const { return ___red_2; }
	inline Material_t340375123 ** get_address_of_red_2() { return &___red_2; }
	inline void set_red_2(Material_t340375123 * value)
	{
		___red_2 = value;
		Il2CppCodeGenWriteBarrier((&___red_2), value);
	}

	inline static int32_t get_offset_of_green_3() { return static_cast<int32_t>(offsetof(GlowScript_t446333960, ___green_3)); }
	inline Material_t340375123 * get_green_3() const { return ___green_3; }
	inline Material_t340375123 ** get_address_of_green_3() { return &___green_3; }
	inline void set_green_3(Material_t340375123 * value)
	{
		___green_3 = value;
		Il2CppCodeGenWriteBarrier((&___green_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOWSCRIPT_T446333960_H
#ifndef TMP_UIFRAMERATECOUNTER_T811747397_H
#define TMP_UIFRAMERATECOUNTER_T811747397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_UiFrameRateCounter
struct  TMP_UiFrameRateCounter_t811747397  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TMPro.Examples.TMP_UiFrameRateCounter::UpdateInterval
	float ___UpdateInterval_2;
	// System.Single TMPro.Examples.TMP_UiFrameRateCounter::m_LastInterval
	float ___m_LastInterval_3;
	// System.Int32 TMPro.Examples.TMP_UiFrameRateCounter::m_Frames
	int32_t ___m_Frames_4;
	// TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_UiFrameRateCounter::AnchorPosition
	int32_t ___AnchorPosition_5;
	// System.String TMPro.Examples.TMP_UiFrameRateCounter::htmlColorTag
	String_t* ___htmlColorTag_6;
	// TMPro.TextMeshProUGUI TMPro.Examples.TMP_UiFrameRateCounter::m_TextMeshPro
	TextMeshProUGUI_t529313277 * ___m_TextMeshPro_8;
	// UnityEngine.RectTransform TMPro.Examples.TMP_UiFrameRateCounter::m_frameCounter_transform
	RectTransform_t3704657025 * ___m_frameCounter_transform_9;
	// TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_UiFrameRateCounter::last_AnchorPosition
	int32_t ___last_AnchorPosition_10;

public:
	inline static int32_t get_offset_of_UpdateInterval_2() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___UpdateInterval_2)); }
	inline float get_UpdateInterval_2() const { return ___UpdateInterval_2; }
	inline float* get_address_of_UpdateInterval_2() { return &___UpdateInterval_2; }
	inline void set_UpdateInterval_2(float value)
	{
		___UpdateInterval_2 = value;
	}

	inline static int32_t get_offset_of_m_LastInterval_3() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___m_LastInterval_3)); }
	inline float get_m_LastInterval_3() const { return ___m_LastInterval_3; }
	inline float* get_address_of_m_LastInterval_3() { return &___m_LastInterval_3; }
	inline void set_m_LastInterval_3(float value)
	{
		___m_LastInterval_3 = value;
	}

	inline static int32_t get_offset_of_m_Frames_4() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___m_Frames_4)); }
	inline int32_t get_m_Frames_4() const { return ___m_Frames_4; }
	inline int32_t* get_address_of_m_Frames_4() { return &___m_Frames_4; }
	inline void set_m_Frames_4(int32_t value)
	{
		___m_Frames_4 = value;
	}

	inline static int32_t get_offset_of_AnchorPosition_5() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___AnchorPosition_5)); }
	inline int32_t get_AnchorPosition_5() const { return ___AnchorPosition_5; }
	inline int32_t* get_address_of_AnchorPosition_5() { return &___AnchorPosition_5; }
	inline void set_AnchorPosition_5(int32_t value)
	{
		___AnchorPosition_5 = value;
	}

	inline static int32_t get_offset_of_htmlColorTag_6() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___htmlColorTag_6)); }
	inline String_t* get_htmlColorTag_6() const { return ___htmlColorTag_6; }
	inline String_t** get_address_of_htmlColorTag_6() { return &___htmlColorTag_6; }
	inline void set_htmlColorTag_6(String_t* value)
	{
		___htmlColorTag_6 = value;
		Il2CppCodeGenWriteBarrier((&___htmlColorTag_6), value);
	}

	inline static int32_t get_offset_of_m_TextMeshPro_8() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___m_TextMeshPro_8)); }
	inline TextMeshProUGUI_t529313277 * get_m_TextMeshPro_8() const { return ___m_TextMeshPro_8; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_m_TextMeshPro_8() { return &___m_TextMeshPro_8; }
	inline void set_m_TextMeshPro_8(TextMeshProUGUI_t529313277 * value)
	{
		___m_TextMeshPro_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_8), value);
	}

	inline static int32_t get_offset_of_m_frameCounter_transform_9() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___m_frameCounter_transform_9)); }
	inline RectTransform_t3704657025 * get_m_frameCounter_transform_9() const { return ___m_frameCounter_transform_9; }
	inline RectTransform_t3704657025 ** get_address_of_m_frameCounter_transform_9() { return &___m_frameCounter_transform_9; }
	inline void set_m_frameCounter_transform_9(RectTransform_t3704657025 * value)
	{
		___m_frameCounter_transform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_frameCounter_transform_9), value);
	}

	inline static int32_t get_offset_of_last_AnchorPosition_10() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___last_AnchorPosition_10)); }
	inline int32_t get_last_AnchorPosition_10() const { return ___last_AnchorPosition_10; }
	inline int32_t* get_address_of_last_AnchorPosition_10() { return &___last_AnchorPosition_10; }
	inline void set_last_AnchorPosition_10(int32_t value)
	{
		___last_AnchorPosition_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_UIFRAMERATECOUNTER_T811747397_H
#ifndef TMP_TEXTSELECTOR_B_T3982526505_H
#define TMP_TEXTSELECTOR_B_T3982526505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_TextSelector_B
struct  TMP_TextSelector_B_t3982526505  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform TMPro.Examples.TMP_TextSelector_B::TextPopup_Prefab_01
	RectTransform_t3704657025 * ___TextPopup_Prefab_01_2;
	// UnityEngine.RectTransform TMPro.Examples.TMP_TextSelector_B::m_TextPopup_RectTransform
	RectTransform_t3704657025 * ___m_TextPopup_RectTransform_3;
	// TMPro.TextMeshProUGUI TMPro.Examples.TMP_TextSelector_B::m_TextPopup_TMPComponent
	TextMeshProUGUI_t529313277 * ___m_TextPopup_TMPComponent_4;
	// TMPro.TextMeshProUGUI TMPro.Examples.TMP_TextSelector_B::m_TextMeshPro
	TextMeshProUGUI_t529313277 * ___m_TextMeshPro_7;
	// UnityEngine.Canvas TMPro.Examples.TMP_TextSelector_B::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_8;
	// UnityEngine.Camera TMPro.Examples.TMP_TextSelector_B::m_Camera
	Camera_t4157153871 * ___m_Camera_9;
	// System.Boolean TMPro.Examples.TMP_TextSelector_B::isHoveringObject
	bool ___isHoveringObject_10;
	// System.Int32 TMPro.Examples.TMP_TextSelector_B::m_selectedWord
	int32_t ___m_selectedWord_11;
	// System.Int32 TMPro.Examples.TMP_TextSelector_B::m_selectedLink
	int32_t ___m_selectedLink_12;
	// System.Int32 TMPro.Examples.TMP_TextSelector_B::m_lastIndex
	int32_t ___m_lastIndex_13;
	// UnityEngine.Matrix4x4 TMPro.Examples.TMP_TextSelector_B::m_matrix
	Matrix4x4_t1817901843  ___m_matrix_14;
	// TMPro.TMP_MeshInfo[] TMPro.Examples.TMP_TextSelector_B::m_cachedMeshInfoVertexData
	TMP_MeshInfoU5BU5D_t3365986247* ___m_cachedMeshInfoVertexData_15;

public:
	inline static int32_t get_offset_of_TextPopup_Prefab_01_2() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___TextPopup_Prefab_01_2)); }
	inline RectTransform_t3704657025 * get_TextPopup_Prefab_01_2() const { return ___TextPopup_Prefab_01_2; }
	inline RectTransform_t3704657025 ** get_address_of_TextPopup_Prefab_01_2() { return &___TextPopup_Prefab_01_2; }
	inline void set_TextPopup_Prefab_01_2(RectTransform_t3704657025 * value)
	{
		___TextPopup_Prefab_01_2 = value;
		Il2CppCodeGenWriteBarrier((&___TextPopup_Prefab_01_2), value);
	}

	inline static int32_t get_offset_of_m_TextPopup_RectTransform_3() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_TextPopup_RectTransform_3)); }
	inline RectTransform_t3704657025 * get_m_TextPopup_RectTransform_3() const { return ___m_TextPopup_RectTransform_3; }
	inline RectTransform_t3704657025 ** get_address_of_m_TextPopup_RectTransform_3() { return &___m_TextPopup_RectTransform_3; }
	inline void set_m_TextPopup_RectTransform_3(RectTransform_t3704657025 * value)
	{
		___m_TextPopup_RectTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextPopup_RectTransform_3), value);
	}

	inline static int32_t get_offset_of_m_TextPopup_TMPComponent_4() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_TextPopup_TMPComponent_4)); }
	inline TextMeshProUGUI_t529313277 * get_m_TextPopup_TMPComponent_4() const { return ___m_TextPopup_TMPComponent_4; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_m_TextPopup_TMPComponent_4() { return &___m_TextPopup_TMPComponent_4; }
	inline void set_m_TextPopup_TMPComponent_4(TextMeshProUGUI_t529313277 * value)
	{
		___m_TextPopup_TMPComponent_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextPopup_TMPComponent_4), value);
	}

	inline static int32_t get_offset_of_m_TextMeshPro_7() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_TextMeshPro_7)); }
	inline TextMeshProUGUI_t529313277 * get_m_TextMeshPro_7() const { return ___m_TextMeshPro_7; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_m_TextMeshPro_7() { return &___m_TextMeshPro_7; }
	inline void set_m_TextMeshPro_7(TextMeshProUGUI_t529313277 * value)
	{
		___m_TextMeshPro_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_7), value);
	}

	inline static int32_t get_offset_of_m_Canvas_8() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_Canvas_8)); }
	inline Canvas_t3310196443 * get_m_Canvas_8() const { return ___m_Canvas_8; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_8() { return &___m_Canvas_8; }
	inline void set_m_Canvas_8(Canvas_t3310196443 * value)
	{
		___m_Canvas_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_8), value);
	}

	inline static int32_t get_offset_of_m_Camera_9() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_Camera_9)); }
	inline Camera_t4157153871 * get_m_Camera_9() const { return ___m_Camera_9; }
	inline Camera_t4157153871 ** get_address_of_m_Camera_9() { return &___m_Camera_9; }
	inline void set_m_Camera_9(Camera_t4157153871 * value)
	{
		___m_Camera_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_9), value);
	}

	inline static int32_t get_offset_of_isHoveringObject_10() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___isHoveringObject_10)); }
	inline bool get_isHoveringObject_10() const { return ___isHoveringObject_10; }
	inline bool* get_address_of_isHoveringObject_10() { return &___isHoveringObject_10; }
	inline void set_isHoveringObject_10(bool value)
	{
		___isHoveringObject_10 = value;
	}

	inline static int32_t get_offset_of_m_selectedWord_11() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_selectedWord_11)); }
	inline int32_t get_m_selectedWord_11() const { return ___m_selectedWord_11; }
	inline int32_t* get_address_of_m_selectedWord_11() { return &___m_selectedWord_11; }
	inline void set_m_selectedWord_11(int32_t value)
	{
		___m_selectedWord_11 = value;
	}

	inline static int32_t get_offset_of_m_selectedLink_12() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_selectedLink_12)); }
	inline int32_t get_m_selectedLink_12() const { return ___m_selectedLink_12; }
	inline int32_t* get_address_of_m_selectedLink_12() { return &___m_selectedLink_12; }
	inline void set_m_selectedLink_12(int32_t value)
	{
		___m_selectedLink_12 = value;
	}

	inline static int32_t get_offset_of_m_lastIndex_13() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_lastIndex_13)); }
	inline int32_t get_m_lastIndex_13() const { return ___m_lastIndex_13; }
	inline int32_t* get_address_of_m_lastIndex_13() { return &___m_lastIndex_13; }
	inline void set_m_lastIndex_13(int32_t value)
	{
		___m_lastIndex_13 = value;
	}

	inline static int32_t get_offset_of_m_matrix_14() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_matrix_14)); }
	inline Matrix4x4_t1817901843  get_m_matrix_14() const { return ___m_matrix_14; }
	inline Matrix4x4_t1817901843 * get_address_of_m_matrix_14() { return &___m_matrix_14; }
	inline void set_m_matrix_14(Matrix4x4_t1817901843  value)
	{
		___m_matrix_14 = value;
	}

	inline static int32_t get_offset_of_m_cachedMeshInfoVertexData_15() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_cachedMeshInfoVertexData_15)); }
	inline TMP_MeshInfoU5BU5D_t3365986247* get_m_cachedMeshInfoVertexData_15() const { return ___m_cachedMeshInfoVertexData_15; }
	inline TMP_MeshInfoU5BU5D_t3365986247** get_address_of_m_cachedMeshInfoVertexData_15() { return &___m_cachedMeshInfoVertexData_15; }
	inline void set_m_cachedMeshInfoVertexData_15(TMP_MeshInfoU5BU5D_t3365986247* value)
	{
		___m_cachedMeshInfoVertexData_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_cachedMeshInfoVertexData_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTSELECTOR_B_T3982526505_H
#ifndef HOVERSCRIPT_T4095815920_H
#define HOVERSCRIPT_T4095815920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoverScript
struct  HoverScript_t4095815920  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioSource HoverScript::Sound
	AudioSource_t3935305588 * ___Sound_2;

public:
	inline static int32_t get_offset_of_Sound_2() { return static_cast<int32_t>(offsetof(HoverScript_t4095815920, ___Sound_2)); }
	inline AudioSource_t3935305588 * get_Sound_2() const { return ___Sound_2; }
	inline AudioSource_t3935305588 ** get_address_of_Sound_2() { return &___Sound_2; }
	inline void set_Sound_2(AudioSource_t3935305588 * value)
	{
		___Sound_2 = value;
		Il2CppCodeGenWriteBarrier((&___Sound_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOVERSCRIPT_T4095815920_H
#ifndef MANAGER_T2928243666_H
#define MANAGER_T2928243666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Manager
struct  Manager_t2928243666  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Manager::swimmerShownDuration
	float ___swimmerShownDuration_2;
	// System.Int32 Manager::scoreMultiplier
	int32_t ___scoreMultiplier_3;
	// UnityEngine.UI.Slider Manager::diffSlider
	Slider_t3903728902 * ___diffSlider_4;
	// UnityEngine.UI.Slider Manager::exposureSlider
	Slider_t3903728902 * ___exposureSlider_5;
	// UnityEngine.UI.Slider Manager::timeSlider
	Slider_t3903728902 * ___timeSlider_6;
	// UnityEngine.UI.Button Manager::pauseButton
	Button_t4055032469 * ___pauseButton_7;
	// UnityEngine.GameObject Manager::swimmer
	GameObject_t1113636619 * ___swimmer_8;
	// UnityEngine.GameObject Manager::cage
	GameObject_t1113636619 * ___cage_9;
	// UnityEngine.GameObject Manager::Beach
	GameObject_t1113636619 * ___Beach_10;
	// UnityEngine.GameObject Manager::cageImage
	GameObject_t1113636619 * ___cageImage_11;
	// UnityEngine.GameObject Manager::GlowSphere
	GameObject_t1113636619 * ___GlowSphere_12;
	// UnityEngine.Animator Manager::UIController
	Animator_t434523843 * ___UIController_13;
	// UnityEngine.Animator Manager::SoundController
	Animator_t434523843 * ___SoundController_14;
	// UnityEngine.Animator Manager::GameAnimator
	Animator_t434523843 * ___GameAnimator_15;
	// UnityEngine.UI.Text Manager::hintText
	Text_t1901882714 * ___hintText_16;
	// TMPro.TextMeshProUGUI Manager::scoreText
	TextMeshProUGUI_t529313277 * ___scoreText_17;
	// TMPro.TextMeshProUGUI Manager::totalScoreText
	TextMeshProUGUI_t529313277 * ___totalScoreText_18;
	// TMPro.TextMeshProUGUI Manager::cageTimerText
	TextMeshProUGUI_t529313277 * ___cageTimerText_19;
	// UnityEngine.Material[] Manager::colours
	MaterialU5BU5D_t561872642* ___colours_20;
	// UnityEngine.Vector3[] Manager::spawnValues
	Vector3U5BU5D_t1718750761* ___spawnValues_21;
	// UnityEngine.Vector3 Manager::cagePosition
	Vector3_t3722313464  ___cagePosition_22;
	// System.String Manager::cageColourText
	String_t* ___cageColourText_23;
	// System.Int32 Manager::diffCount
	int32_t ___diffCount_24;
	// System.Int32 Manager::cageType
	int32_t ___cageType_25;
	// System.Int32 Manager::cageSpawnCount
	int32_t ___cageSpawnCount_26;
	// System.Int32 Manager::usedCageCount
	int32_t ___usedCageCount_27;
	// System.Int32 Manager::correctCages
	int32_t ___correctCages_28;
	// System.Int32 Manager::startingScore
	int32_t ___startingScore_29;
	// System.Int32 Manager::currentScore
	int32_t ___currentScore_30;
	// System.Single Manager::totalTime
	float ___totalTime_31;
	// System.Single Manager::cageTimer
	float ___cageTimer_32;
	// System.Single Manager::totalCageTime
	float ___totalCageTime_33;
	// System.Boolean Manager::swimmersSpawned
	bool ___swimmersSpawned_34;
	// System.Boolean Manager::gameStarted
	bool ___gameStarted_35;
	// System.Boolean Manager::cagesActive
	bool ___cagesActive_36;
	// System.Boolean Manager::cageHintActive
	bool ___cageHintActive_37;
	// System.Boolean Manager::firstTimeTutorial
	bool ___firstTimeTutorial_38;
	// System.Int32 Manager::handStage
	int32_t ___handStage_39;
	// System.Boolean Manager::tutorialDone
	bool ___tutorialDone_40;
	// System.Single Manager::handSpeed
	float ___handSpeed_41;
	// UnityEngine.GameObject Manager::tutorialTarget
	GameObject_t1113636619 * ___tutorialTarget_42;
	// System.Int32 Manager::tutorialRepeat
	int32_t ___tutorialRepeat_43;
	// System.String Manager::hintTextType
	String_t* ___hintTextType_44;
	// System.Int32 Manager::textType
	int32_t ___textType_45;
	// UnityEngine.GameObject Manager::GreenCircle
	GameObject_t1113636619 * ___GreenCircle_46;
	// UnityEngine.GameObject Manager::GreenCicle2
	GameObject_t1113636619 * ___GreenCicle2_47;
	// UnityEngine.GameObject Manager::RedCircle
	GameObject_t1113636619 * ___RedCircle_48;
	// UnityEngine.GameObject[] Manager::beachPeople
	GameObjectU5BU5D_t3328599146* ___beachPeople_49;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Manager::swimmers
	List_1_t2585711361 * ___swimmers_50;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Manager::cages
	List_1_t2585711361 * ___cages_51;

public:
	inline static int32_t get_offset_of_swimmerShownDuration_2() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___swimmerShownDuration_2)); }
	inline float get_swimmerShownDuration_2() const { return ___swimmerShownDuration_2; }
	inline float* get_address_of_swimmerShownDuration_2() { return &___swimmerShownDuration_2; }
	inline void set_swimmerShownDuration_2(float value)
	{
		___swimmerShownDuration_2 = value;
	}

	inline static int32_t get_offset_of_scoreMultiplier_3() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___scoreMultiplier_3)); }
	inline int32_t get_scoreMultiplier_3() const { return ___scoreMultiplier_3; }
	inline int32_t* get_address_of_scoreMultiplier_3() { return &___scoreMultiplier_3; }
	inline void set_scoreMultiplier_3(int32_t value)
	{
		___scoreMultiplier_3 = value;
	}

	inline static int32_t get_offset_of_diffSlider_4() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___diffSlider_4)); }
	inline Slider_t3903728902 * get_diffSlider_4() const { return ___diffSlider_4; }
	inline Slider_t3903728902 ** get_address_of_diffSlider_4() { return &___diffSlider_4; }
	inline void set_diffSlider_4(Slider_t3903728902 * value)
	{
		___diffSlider_4 = value;
		Il2CppCodeGenWriteBarrier((&___diffSlider_4), value);
	}

	inline static int32_t get_offset_of_exposureSlider_5() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___exposureSlider_5)); }
	inline Slider_t3903728902 * get_exposureSlider_5() const { return ___exposureSlider_5; }
	inline Slider_t3903728902 ** get_address_of_exposureSlider_5() { return &___exposureSlider_5; }
	inline void set_exposureSlider_5(Slider_t3903728902 * value)
	{
		___exposureSlider_5 = value;
		Il2CppCodeGenWriteBarrier((&___exposureSlider_5), value);
	}

	inline static int32_t get_offset_of_timeSlider_6() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___timeSlider_6)); }
	inline Slider_t3903728902 * get_timeSlider_6() const { return ___timeSlider_6; }
	inline Slider_t3903728902 ** get_address_of_timeSlider_6() { return &___timeSlider_6; }
	inline void set_timeSlider_6(Slider_t3903728902 * value)
	{
		___timeSlider_6 = value;
		Il2CppCodeGenWriteBarrier((&___timeSlider_6), value);
	}

	inline static int32_t get_offset_of_pauseButton_7() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___pauseButton_7)); }
	inline Button_t4055032469 * get_pauseButton_7() const { return ___pauseButton_7; }
	inline Button_t4055032469 ** get_address_of_pauseButton_7() { return &___pauseButton_7; }
	inline void set_pauseButton_7(Button_t4055032469 * value)
	{
		___pauseButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___pauseButton_7), value);
	}

	inline static int32_t get_offset_of_swimmer_8() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___swimmer_8)); }
	inline GameObject_t1113636619 * get_swimmer_8() const { return ___swimmer_8; }
	inline GameObject_t1113636619 ** get_address_of_swimmer_8() { return &___swimmer_8; }
	inline void set_swimmer_8(GameObject_t1113636619 * value)
	{
		___swimmer_8 = value;
		Il2CppCodeGenWriteBarrier((&___swimmer_8), value);
	}

	inline static int32_t get_offset_of_cage_9() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___cage_9)); }
	inline GameObject_t1113636619 * get_cage_9() const { return ___cage_9; }
	inline GameObject_t1113636619 ** get_address_of_cage_9() { return &___cage_9; }
	inline void set_cage_9(GameObject_t1113636619 * value)
	{
		___cage_9 = value;
		Il2CppCodeGenWriteBarrier((&___cage_9), value);
	}

	inline static int32_t get_offset_of_Beach_10() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___Beach_10)); }
	inline GameObject_t1113636619 * get_Beach_10() const { return ___Beach_10; }
	inline GameObject_t1113636619 ** get_address_of_Beach_10() { return &___Beach_10; }
	inline void set_Beach_10(GameObject_t1113636619 * value)
	{
		___Beach_10 = value;
		Il2CppCodeGenWriteBarrier((&___Beach_10), value);
	}

	inline static int32_t get_offset_of_cageImage_11() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___cageImage_11)); }
	inline GameObject_t1113636619 * get_cageImage_11() const { return ___cageImage_11; }
	inline GameObject_t1113636619 ** get_address_of_cageImage_11() { return &___cageImage_11; }
	inline void set_cageImage_11(GameObject_t1113636619 * value)
	{
		___cageImage_11 = value;
		Il2CppCodeGenWriteBarrier((&___cageImage_11), value);
	}

	inline static int32_t get_offset_of_GlowSphere_12() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___GlowSphere_12)); }
	inline GameObject_t1113636619 * get_GlowSphere_12() const { return ___GlowSphere_12; }
	inline GameObject_t1113636619 ** get_address_of_GlowSphere_12() { return &___GlowSphere_12; }
	inline void set_GlowSphere_12(GameObject_t1113636619 * value)
	{
		___GlowSphere_12 = value;
		Il2CppCodeGenWriteBarrier((&___GlowSphere_12), value);
	}

	inline static int32_t get_offset_of_UIController_13() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___UIController_13)); }
	inline Animator_t434523843 * get_UIController_13() const { return ___UIController_13; }
	inline Animator_t434523843 ** get_address_of_UIController_13() { return &___UIController_13; }
	inline void set_UIController_13(Animator_t434523843 * value)
	{
		___UIController_13 = value;
		Il2CppCodeGenWriteBarrier((&___UIController_13), value);
	}

	inline static int32_t get_offset_of_SoundController_14() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___SoundController_14)); }
	inline Animator_t434523843 * get_SoundController_14() const { return ___SoundController_14; }
	inline Animator_t434523843 ** get_address_of_SoundController_14() { return &___SoundController_14; }
	inline void set_SoundController_14(Animator_t434523843 * value)
	{
		___SoundController_14 = value;
		Il2CppCodeGenWriteBarrier((&___SoundController_14), value);
	}

	inline static int32_t get_offset_of_GameAnimator_15() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___GameAnimator_15)); }
	inline Animator_t434523843 * get_GameAnimator_15() const { return ___GameAnimator_15; }
	inline Animator_t434523843 ** get_address_of_GameAnimator_15() { return &___GameAnimator_15; }
	inline void set_GameAnimator_15(Animator_t434523843 * value)
	{
		___GameAnimator_15 = value;
		Il2CppCodeGenWriteBarrier((&___GameAnimator_15), value);
	}

	inline static int32_t get_offset_of_hintText_16() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___hintText_16)); }
	inline Text_t1901882714 * get_hintText_16() const { return ___hintText_16; }
	inline Text_t1901882714 ** get_address_of_hintText_16() { return &___hintText_16; }
	inline void set_hintText_16(Text_t1901882714 * value)
	{
		___hintText_16 = value;
		Il2CppCodeGenWriteBarrier((&___hintText_16), value);
	}

	inline static int32_t get_offset_of_scoreText_17() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___scoreText_17)); }
	inline TextMeshProUGUI_t529313277 * get_scoreText_17() const { return ___scoreText_17; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_scoreText_17() { return &___scoreText_17; }
	inline void set_scoreText_17(TextMeshProUGUI_t529313277 * value)
	{
		___scoreText_17 = value;
		Il2CppCodeGenWriteBarrier((&___scoreText_17), value);
	}

	inline static int32_t get_offset_of_totalScoreText_18() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___totalScoreText_18)); }
	inline TextMeshProUGUI_t529313277 * get_totalScoreText_18() const { return ___totalScoreText_18; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_totalScoreText_18() { return &___totalScoreText_18; }
	inline void set_totalScoreText_18(TextMeshProUGUI_t529313277 * value)
	{
		___totalScoreText_18 = value;
		Il2CppCodeGenWriteBarrier((&___totalScoreText_18), value);
	}

	inline static int32_t get_offset_of_cageTimerText_19() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___cageTimerText_19)); }
	inline TextMeshProUGUI_t529313277 * get_cageTimerText_19() const { return ___cageTimerText_19; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_cageTimerText_19() { return &___cageTimerText_19; }
	inline void set_cageTimerText_19(TextMeshProUGUI_t529313277 * value)
	{
		___cageTimerText_19 = value;
		Il2CppCodeGenWriteBarrier((&___cageTimerText_19), value);
	}

	inline static int32_t get_offset_of_colours_20() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___colours_20)); }
	inline MaterialU5BU5D_t561872642* get_colours_20() const { return ___colours_20; }
	inline MaterialU5BU5D_t561872642** get_address_of_colours_20() { return &___colours_20; }
	inline void set_colours_20(MaterialU5BU5D_t561872642* value)
	{
		___colours_20 = value;
		Il2CppCodeGenWriteBarrier((&___colours_20), value);
	}

	inline static int32_t get_offset_of_spawnValues_21() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___spawnValues_21)); }
	inline Vector3U5BU5D_t1718750761* get_spawnValues_21() const { return ___spawnValues_21; }
	inline Vector3U5BU5D_t1718750761** get_address_of_spawnValues_21() { return &___spawnValues_21; }
	inline void set_spawnValues_21(Vector3U5BU5D_t1718750761* value)
	{
		___spawnValues_21 = value;
		Il2CppCodeGenWriteBarrier((&___spawnValues_21), value);
	}

	inline static int32_t get_offset_of_cagePosition_22() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___cagePosition_22)); }
	inline Vector3_t3722313464  get_cagePosition_22() const { return ___cagePosition_22; }
	inline Vector3_t3722313464 * get_address_of_cagePosition_22() { return &___cagePosition_22; }
	inline void set_cagePosition_22(Vector3_t3722313464  value)
	{
		___cagePosition_22 = value;
	}

	inline static int32_t get_offset_of_cageColourText_23() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___cageColourText_23)); }
	inline String_t* get_cageColourText_23() const { return ___cageColourText_23; }
	inline String_t** get_address_of_cageColourText_23() { return &___cageColourText_23; }
	inline void set_cageColourText_23(String_t* value)
	{
		___cageColourText_23 = value;
		Il2CppCodeGenWriteBarrier((&___cageColourText_23), value);
	}

	inline static int32_t get_offset_of_diffCount_24() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___diffCount_24)); }
	inline int32_t get_diffCount_24() const { return ___diffCount_24; }
	inline int32_t* get_address_of_diffCount_24() { return &___diffCount_24; }
	inline void set_diffCount_24(int32_t value)
	{
		___diffCount_24 = value;
	}

	inline static int32_t get_offset_of_cageType_25() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___cageType_25)); }
	inline int32_t get_cageType_25() const { return ___cageType_25; }
	inline int32_t* get_address_of_cageType_25() { return &___cageType_25; }
	inline void set_cageType_25(int32_t value)
	{
		___cageType_25 = value;
	}

	inline static int32_t get_offset_of_cageSpawnCount_26() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___cageSpawnCount_26)); }
	inline int32_t get_cageSpawnCount_26() const { return ___cageSpawnCount_26; }
	inline int32_t* get_address_of_cageSpawnCount_26() { return &___cageSpawnCount_26; }
	inline void set_cageSpawnCount_26(int32_t value)
	{
		___cageSpawnCount_26 = value;
	}

	inline static int32_t get_offset_of_usedCageCount_27() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___usedCageCount_27)); }
	inline int32_t get_usedCageCount_27() const { return ___usedCageCount_27; }
	inline int32_t* get_address_of_usedCageCount_27() { return &___usedCageCount_27; }
	inline void set_usedCageCount_27(int32_t value)
	{
		___usedCageCount_27 = value;
	}

	inline static int32_t get_offset_of_correctCages_28() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___correctCages_28)); }
	inline int32_t get_correctCages_28() const { return ___correctCages_28; }
	inline int32_t* get_address_of_correctCages_28() { return &___correctCages_28; }
	inline void set_correctCages_28(int32_t value)
	{
		___correctCages_28 = value;
	}

	inline static int32_t get_offset_of_startingScore_29() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___startingScore_29)); }
	inline int32_t get_startingScore_29() const { return ___startingScore_29; }
	inline int32_t* get_address_of_startingScore_29() { return &___startingScore_29; }
	inline void set_startingScore_29(int32_t value)
	{
		___startingScore_29 = value;
	}

	inline static int32_t get_offset_of_currentScore_30() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___currentScore_30)); }
	inline int32_t get_currentScore_30() const { return ___currentScore_30; }
	inline int32_t* get_address_of_currentScore_30() { return &___currentScore_30; }
	inline void set_currentScore_30(int32_t value)
	{
		___currentScore_30 = value;
	}

	inline static int32_t get_offset_of_totalTime_31() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___totalTime_31)); }
	inline float get_totalTime_31() const { return ___totalTime_31; }
	inline float* get_address_of_totalTime_31() { return &___totalTime_31; }
	inline void set_totalTime_31(float value)
	{
		___totalTime_31 = value;
	}

	inline static int32_t get_offset_of_cageTimer_32() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___cageTimer_32)); }
	inline float get_cageTimer_32() const { return ___cageTimer_32; }
	inline float* get_address_of_cageTimer_32() { return &___cageTimer_32; }
	inline void set_cageTimer_32(float value)
	{
		___cageTimer_32 = value;
	}

	inline static int32_t get_offset_of_totalCageTime_33() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___totalCageTime_33)); }
	inline float get_totalCageTime_33() const { return ___totalCageTime_33; }
	inline float* get_address_of_totalCageTime_33() { return &___totalCageTime_33; }
	inline void set_totalCageTime_33(float value)
	{
		___totalCageTime_33 = value;
	}

	inline static int32_t get_offset_of_swimmersSpawned_34() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___swimmersSpawned_34)); }
	inline bool get_swimmersSpawned_34() const { return ___swimmersSpawned_34; }
	inline bool* get_address_of_swimmersSpawned_34() { return &___swimmersSpawned_34; }
	inline void set_swimmersSpawned_34(bool value)
	{
		___swimmersSpawned_34 = value;
	}

	inline static int32_t get_offset_of_gameStarted_35() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___gameStarted_35)); }
	inline bool get_gameStarted_35() const { return ___gameStarted_35; }
	inline bool* get_address_of_gameStarted_35() { return &___gameStarted_35; }
	inline void set_gameStarted_35(bool value)
	{
		___gameStarted_35 = value;
	}

	inline static int32_t get_offset_of_cagesActive_36() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___cagesActive_36)); }
	inline bool get_cagesActive_36() const { return ___cagesActive_36; }
	inline bool* get_address_of_cagesActive_36() { return &___cagesActive_36; }
	inline void set_cagesActive_36(bool value)
	{
		___cagesActive_36 = value;
	}

	inline static int32_t get_offset_of_cageHintActive_37() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___cageHintActive_37)); }
	inline bool get_cageHintActive_37() const { return ___cageHintActive_37; }
	inline bool* get_address_of_cageHintActive_37() { return &___cageHintActive_37; }
	inline void set_cageHintActive_37(bool value)
	{
		___cageHintActive_37 = value;
	}

	inline static int32_t get_offset_of_firstTimeTutorial_38() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___firstTimeTutorial_38)); }
	inline bool get_firstTimeTutorial_38() const { return ___firstTimeTutorial_38; }
	inline bool* get_address_of_firstTimeTutorial_38() { return &___firstTimeTutorial_38; }
	inline void set_firstTimeTutorial_38(bool value)
	{
		___firstTimeTutorial_38 = value;
	}

	inline static int32_t get_offset_of_handStage_39() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___handStage_39)); }
	inline int32_t get_handStage_39() const { return ___handStage_39; }
	inline int32_t* get_address_of_handStage_39() { return &___handStage_39; }
	inline void set_handStage_39(int32_t value)
	{
		___handStage_39 = value;
	}

	inline static int32_t get_offset_of_tutorialDone_40() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___tutorialDone_40)); }
	inline bool get_tutorialDone_40() const { return ___tutorialDone_40; }
	inline bool* get_address_of_tutorialDone_40() { return &___tutorialDone_40; }
	inline void set_tutorialDone_40(bool value)
	{
		___tutorialDone_40 = value;
	}

	inline static int32_t get_offset_of_handSpeed_41() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___handSpeed_41)); }
	inline float get_handSpeed_41() const { return ___handSpeed_41; }
	inline float* get_address_of_handSpeed_41() { return &___handSpeed_41; }
	inline void set_handSpeed_41(float value)
	{
		___handSpeed_41 = value;
	}

	inline static int32_t get_offset_of_tutorialTarget_42() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___tutorialTarget_42)); }
	inline GameObject_t1113636619 * get_tutorialTarget_42() const { return ___tutorialTarget_42; }
	inline GameObject_t1113636619 ** get_address_of_tutorialTarget_42() { return &___tutorialTarget_42; }
	inline void set_tutorialTarget_42(GameObject_t1113636619 * value)
	{
		___tutorialTarget_42 = value;
		Il2CppCodeGenWriteBarrier((&___tutorialTarget_42), value);
	}

	inline static int32_t get_offset_of_tutorialRepeat_43() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___tutorialRepeat_43)); }
	inline int32_t get_tutorialRepeat_43() const { return ___tutorialRepeat_43; }
	inline int32_t* get_address_of_tutorialRepeat_43() { return &___tutorialRepeat_43; }
	inline void set_tutorialRepeat_43(int32_t value)
	{
		___tutorialRepeat_43 = value;
	}

	inline static int32_t get_offset_of_hintTextType_44() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___hintTextType_44)); }
	inline String_t* get_hintTextType_44() const { return ___hintTextType_44; }
	inline String_t** get_address_of_hintTextType_44() { return &___hintTextType_44; }
	inline void set_hintTextType_44(String_t* value)
	{
		___hintTextType_44 = value;
		Il2CppCodeGenWriteBarrier((&___hintTextType_44), value);
	}

	inline static int32_t get_offset_of_textType_45() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___textType_45)); }
	inline int32_t get_textType_45() const { return ___textType_45; }
	inline int32_t* get_address_of_textType_45() { return &___textType_45; }
	inline void set_textType_45(int32_t value)
	{
		___textType_45 = value;
	}

	inline static int32_t get_offset_of_GreenCircle_46() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___GreenCircle_46)); }
	inline GameObject_t1113636619 * get_GreenCircle_46() const { return ___GreenCircle_46; }
	inline GameObject_t1113636619 ** get_address_of_GreenCircle_46() { return &___GreenCircle_46; }
	inline void set_GreenCircle_46(GameObject_t1113636619 * value)
	{
		___GreenCircle_46 = value;
		Il2CppCodeGenWriteBarrier((&___GreenCircle_46), value);
	}

	inline static int32_t get_offset_of_GreenCicle2_47() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___GreenCicle2_47)); }
	inline GameObject_t1113636619 * get_GreenCicle2_47() const { return ___GreenCicle2_47; }
	inline GameObject_t1113636619 ** get_address_of_GreenCicle2_47() { return &___GreenCicle2_47; }
	inline void set_GreenCicle2_47(GameObject_t1113636619 * value)
	{
		___GreenCicle2_47 = value;
		Il2CppCodeGenWriteBarrier((&___GreenCicle2_47), value);
	}

	inline static int32_t get_offset_of_RedCircle_48() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___RedCircle_48)); }
	inline GameObject_t1113636619 * get_RedCircle_48() const { return ___RedCircle_48; }
	inline GameObject_t1113636619 ** get_address_of_RedCircle_48() { return &___RedCircle_48; }
	inline void set_RedCircle_48(GameObject_t1113636619 * value)
	{
		___RedCircle_48 = value;
		Il2CppCodeGenWriteBarrier((&___RedCircle_48), value);
	}

	inline static int32_t get_offset_of_beachPeople_49() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___beachPeople_49)); }
	inline GameObjectU5BU5D_t3328599146* get_beachPeople_49() const { return ___beachPeople_49; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_beachPeople_49() { return &___beachPeople_49; }
	inline void set_beachPeople_49(GameObjectU5BU5D_t3328599146* value)
	{
		___beachPeople_49 = value;
		Il2CppCodeGenWriteBarrier((&___beachPeople_49), value);
	}

	inline static int32_t get_offset_of_swimmers_50() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___swimmers_50)); }
	inline List_1_t2585711361 * get_swimmers_50() const { return ___swimmers_50; }
	inline List_1_t2585711361 ** get_address_of_swimmers_50() { return &___swimmers_50; }
	inline void set_swimmers_50(List_1_t2585711361 * value)
	{
		___swimmers_50 = value;
		Il2CppCodeGenWriteBarrier((&___swimmers_50), value);
	}

	inline static int32_t get_offset_of_cages_51() { return static_cast<int32_t>(offsetof(Manager_t2928243666, ___cages_51)); }
	inline List_1_t2585711361 * get_cages_51() const { return ___cages_51; }
	inline List_1_t2585711361 ** get_address_of_cages_51() { return &___cages_51; }
	inline void set_cages_51(List_1_t2585711361 * value)
	{
		___cages_51 = value;
		Il2CppCodeGenWriteBarrier((&___cages_51), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MANAGER_T2928243666_H
#ifndef SCOREMANAGER_T3621325103_H
#define SCOREMANAGER_T3621325103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoreManager
struct  ScoreManager_t3621325103  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TextMeshProUGUI ScoreManager::text
	TextMeshProUGUI_t529313277 * ___text_3;

public:
	inline static int32_t get_offset_of_text_3() { return static_cast<int32_t>(offsetof(ScoreManager_t3621325103, ___text_3)); }
	inline TextMeshProUGUI_t529313277 * get_text_3() const { return ___text_3; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_text_3() { return &___text_3; }
	inline void set_text_3(TextMeshProUGUI_t529313277 * value)
	{
		___text_3 = value;
		Il2CppCodeGenWriteBarrier((&___text_3), value);
	}
};

struct ScoreManager_t3621325103_StaticFields
{
public:
	// System.Int32 ScoreManager::score
	int32_t ___score_2;

public:
	inline static int32_t get_offset_of_score_2() { return static_cast<int32_t>(offsetof(ScoreManager_t3621325103_StaticFields, ___score_2)); }
	inline int32_t get_score_2() const { return ___score_2; }
	inline int32_t* get_address_of_score_2() { return &___score_2; }
	inline void set_score_2(int32_t value)
	{
		___score_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOREMANAGER_T3621325103_H
#ifndef TWEENSCRIPT_T2995776179_H
#define TWEENSCRIPT_T2995776179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenScript
struct  TweenScript_t2995776179  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TweenScript::timeScale
	float ___timeScale_2;
	// System.Collections.Generic.List`1<TweenAnimation.Models.TweenAnimation> TweenScript::animations
	List_1_t3050851472 * ___animations_3;
	// System.Collections.Generic.List`1<TweenAnimation.Models.TweenAnimation> TweenScript::animationsToEnqeue
	List_1_t3050851472 * ___animationsToEnqeue_4;

public:
	inline static int32_t get_offset_of_timeScale_2() { return static_cast<int32_t>(offsetof(TweenScript_t2995776179, ___timeScale_2)); }
	inline float get_timeScale_2() const { return ___timeScale_2; }
	inline float* get_address_of_timeScale_2() { return &___timeScale_2; }
	inline void set_timeScale_2(float value)
	{
		___timeScale_2 = value;
	}

	inline static int32_t get_offset_of_animations_3() { return static_cast<int32_t>(offsetof(TweenScript_t2995776179, ___animations_3)); }
	inline List_1_t3050851472 * get_animations_3() const { return ___animations_3; }
	inline List_1_t3050851472 ** get_address_of_animations_3() { return &___animations_3; }
	inline void set_animations_3(List_1_t3050851472 * value)
	{
		___animations_3 = value;
		Il2CppCodeGenWriteBarrier((&___animations_3), value);
	}

	inline static int32_t get_offset_of_animationsToEnqeue_4() { return static_cast<int32_t>(offsetof(TweenScript_t2995776179, ___animationsToEnqeue_4)); }
	inline List_1_t3050851472 * get_animationsToEnqeue_4() const { return ___animationsToEnqeue_4; }
	inline List_1_t3050851472 ** get_address_of_animationsToEnqeue_4() { return &___animationsToEnqeue_4; }
	inline void set_animationsToEnqeue_4(List_1_t3050851472 * value)
	{
		___animationsToEnqeue_4 = value;
		Il2CppCodeGenWriteBarrier((&___animationsToEnqeue_4), value);
	}
};

struct TweenScript_t2995776179_StaticFields
{
public:
	// TweenScript TweenScript::<Instance>k__BackingField
	TweenScript_t2995776179 * ___U3CInstanceU3Ek__BackingField_5;
	// System.Func`2<TweenAnimation.Models.TweenAnimation,System.Boolean> TweenScript::<>f__am$cache0
	Func_2_t1832049457 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TweenScript_t2995776179_StaticFields, ___U3CInstanceU3Ek__BackingField_5)); }
	inline TweenScript_t2995776179 * get_U3CInstanceU3Ek__BackingField_5() const { return ___U3CInstanceU3Ek__BackingField_5; }
	inline TweenScript_t2995776179 ** get_address_of_U3CInstanceU3Ek__BackingField_5() { return &___U3CInstanceU3Ek__BackingField_5; }
	inline void set_U3CInstanceU3Ek__BackingField_5(TweenScript_t2995776179 * value)
	{
		___U3CInstanceU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(TweenScript_t2995776179_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Func_2_t1832049457 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Func_2_t1832049457 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Func_2_t1832049457 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENSCRIPT_T2995776179_H
#ifndef CAMERACONTROLLER_T3346819214_H
#define CAMERACONTROLLER_T3346819214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraController
struct  CameraController_t3346819214  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CameraController::cameraTarget
	GameObject_t1113636619 * ___cameraTarget_2;
	// System.Single CameraController::rotateSpeed
	float ___rotateSpeed_3;
	// System.Single CameraController::rotate
	float ___rotate_4;
	// System.Single CameraController::offsetDistance
	float ___offsetDistance_5;
	// System.Single CameraController::offsetHeight
	float ___offsetHeight_6;
	// System.Single CameraController::smoothing
	float ___smoothing_7;
	// UnityEngine.Vector3 CameraController::offset
	Vector3_t3722313464  ___offset_8;
	// System.Boolean CameraController::following
	bool ___following_9;
	// UnityEngine.Vector3 CameraController::lastPosition
	Vector3_t3722313464  ___lastPosition_10;

public:
	inline static int32_t get_offset_of_cameraTarget_2() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___cameraTarget_2)); }
	inline GameObject_t1113636619 * get_cameraTarget_2() const { return ___cameraTarget_2; }
	inline GameObject_t1113636619 ** get_address_of_cameraTarget_2() { return &___cameraTarget_2; }
	inline void set_cameraTarget_2(GameObject_t1113636619 * value)
	{
		___cameraTarget_2 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTarget_2), value);
	}

	inline static int32_t get_offset_of_rotateSpeed_3() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___rotateSpeed_3)); }
	inline float get_rotateSpeed_3() const { return ___rotateSpeed_3; }
	inline float* get_address_of_rotateSpeed_3() { return &___rotateSpeed_3; }
	inline void set_rotateSpeed_3(float value)
	{
		___rotateSpeed_3 = value;
	}

	inline static int32_t get_offset_of_rotate_4() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___rotate_4)); }
	inline float get_rotate_4() const { return ___rotate_4; }
	inline float* get_address_of_rotate_4() { return &___rotate_4; }
	inline void set_rotate_4(float value)
	{
		___rotate_4 = value;
	}

	inline static int32_t get_offset_of_offsetDistance_5() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___offsetDistance_5)); }
	inline float get_offsetDistance_5() const { return ___offsetDistance_5; }
	inline float* get_address_of_offsetDistance_5() { return &___offsetDistance_5; }
	inline void set_offsetDistance_5(float value)
	{
		___offsetDistance_5 = value;
	}

	inline static int32_t get_offset_of_offsetHeight_6() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___offsetHeight_6)); }
	inline float get_offsetHeight_6() const { return ___offsetHeight_6; }
	inline float* get_address_of_offsetHeight_6() { return &___offsetHeight_6; }
	inline void set_offsetHeight_6(float value)
	{
		___offsetHeight_6 = value;
	}

	inline static int32_t get_offset_of_smoothing_7() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___smoothing_7)); }
	inline float get_smoothing_7() const { return ___smoothing_7; }
	inline float* get_address_of_smoothing_7() { return &___smoothing_7; }
	inline void set_smoothing_7(float value)
	{
		___smoothing_7 = value;
	}

	inline static int32_t get_offset_of_offset_8() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___offset_8)); }
	inline Vector3_t3722313464  get_offset_8() const { return ___offset_8; }
	inline Vector3_t3722313464 * get_address_of_offset_8() { return &___offset_8; }
	inline void set_offset_8(Vector3_t3722313464  value)
	{
		___offset_8 = value;
	}

	inline static int32_t get_offset_of_following_9() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___following_9)); }
	inline bool get_following_9() const { return ___following_9; }
	inline bool* get_address_of_following_9() { return &___following_9; }
	inline void set_following_9(bool value)
	{
		___following_9 = value;
	}

	inline static int32_t get_offset_of_lastPosition_10() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___lastPosition_10)); }
	inline Vector3_t3722313464  get_lastPosition_10() const { return ___lastPosition_10; }
	inline Vector3_t3722313464 * get_address_of_lastPosition_10() { return &___lastPosition_10; }
	inline void set_lastPosition_10(Vector3_t3722313464  value)
	{
		___lastPosition_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACONTROLLER_T3346819214_H
#ifndef TELETYPE_T2409835159_H
#define TELETYPE_T2409835159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TeleType
struct  TeleType_t2409835159  : public MonoBehaviour_t3962482529
{
public:
	// System.String TMPro.Examples.TeleType::label01
	String_t* ___label01_2;
	// System.String TMPro.Examples.TeleType::label02
	String_t* ___label02_3;
	// TMPro.TMP_Text TMPro.Examples.TeleType::m_textMeshPro
	TMP_Text_t2599618874 * ___m_textMeshPro_4;

public:
	inline static int32_t get_offset_of_label01_2() { return static_cast<int32_t>(offsetof(TeleType_t2409835159, ___label01_2)); }
	inline String_t* get_label01_2() const { return ___label01_2; }
	inline String_t** get_address_of_label01_2() { return &___label01_2; }
	inline void set_label01_2(String_t* value)
	{
		___label01_2 = value;
		Il2CppCodeGenWriteBarrier((&___label01_2), value);
	}

	inline static int32_t get_offset_of_label02_3() { return static_cast<int32_t>(offsetof(TeleType_t2409835159, ___label02_3)); }
	inline String_t* get_label02_3() const { return ___label02_3; }
	inline String_t** get_address_of_label02_3() { return &___label02_3; }
	inline void set_label02_3(String_t* value)
	{
		___label02_3 = value;
		Il2CppCodeGenWriteBarrier((&___label02_3), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_4() { return static_cast<int32_t>(offsetof(TeleType_t2409835159, ___m_textMeshPro_4)); }
	inline TMP_Text_t2599618874 * get_m_textMeshPro_4() const { return ___m_textMeshPro_4; }
	inline TMP_Text_t2599618874 ** get_address_of_m_textMeshPro_4() { return &___m_textMeshPro_4; }
	inline void set_m_textMeshPro_4(TMP_Text_t2599618874 * value)
	{
		___m_textMeshPro_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TELETYPE_T2409835159_H
#ifndef RPGCHARACTERCONTROLLERFREE_T3036052416_H
#define RPGCHARACTERCONTROLLERFREE_T3036052416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RPGCharacterControllerFREE
struct  RPGCharacterControllerFREE_t3036052416  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rigidbody RPGCharacterControllerFREE::rb
	Rigidbody_t3916780224 * ___rb_2;
	// UnityEngine.Animator RPGCharacterControllerFREE::animator
	Animator_t434523843 * ___animator_3;
	// UnityEngine.GameObject RPGCharacterControllerFREE::target
	GameObject_t1113636619 * ___target_4;
	// UnityEngine.Vector3 RPGCharacterControllerFREE::targetDashDirection
	Vector3_t3722313464  ___targetDashDirection_5;
	// UnityEngine.Camera RPGCharacterControllerFREE::sceneCamera
	Camera_t4157153871 * ___sceneCamera_6;
	// System.Boolean RPGCharacterControllerFREE::useNavMesh
	bool ___useNavMesh_7;
	// UnityEngine.AI.NavMeshAgent RPGCharacterControllerFREE::agent
	NavMeshAgent_t1276799816 * ___agent_8;
	// System.Single RPGCharacterControllerFREE::navMeshSpeed
	float ___navMeshSpeed_9;
	// UnityEngine.Transform RPGCharacterControllerFREE::goal
	Transform_t3600365921 * ___goal_10;
	// System.Single RPGCharacterControllerFREE::gravity
	float ___gravity_11;
	// System.Boolean RPGCharacterControllerFREE::canJump
	bool ___canJump_12;
	// System.Boolean RPGCharacterControllerFREE::isJumping
	bool ___isJumping_13;
	// System.Boolean RPGCharacterControllerFREE::isGrounded
	bool ___isGrounded_14;
	// System.Single RPGCharacterControllerFREE::jumpSpeed
	float ___jumpSpeed_15;
	// System.Single RPGCharacterControllerFREE::doublejumpSpeed
	float ___doublejumpSpeed_16;
	// System.Boolean RPGCharacterControllerFREE::doublejumping
	bool ___doublejumping_17;
	// System.Boolean RPGCharacterControllerFREE::canDoubleJump
	bool ___canDoubleJump_18;
	// System.Boolean RPGCharacterControllerFREE::isDoubleJumping
	bool ___isDoubleJumping_19;
	// System.Boolean RPGCharacterControllerFREE::doublejumped
	bool ___doublejumped_20;
	// System.Boolean RPGCharacterControllerFREE::isFalling
	bool ___isFalling_21;
	// System.Boolean RPGCharacterControllerFREE::startFall
	bool ___startFall_22;
	// System.Single RPGCharacterControllerFREE::fallingVelocity
	float ___fallingVelocity_23;
	// System.Single RPGCharacterControllerFREE::inAirSpeed
	float ___inAirSpeed_24;
	// System.Single RPGCharacterControllerFREE::maxVelocity
	float ___maxVelocity_25;
	// System.Single RPGCharacterControllerFREE::minVelocity
	float ___minVelocity_26;
	// System.Single RPGCharacterControllerFREE::rollSpeed
	float ___rollSpeed_27;
	// System.Boolean RPGCharacterControllerFREE::isRolling
	bool ___isRolling_28;
	// System.Single RPGCharacterControllerFREE::rollduration
	float ___rollduration_29;
	// System.Boolean RPGCharacterControllerFREE::canMove
	bool ___canMove_30;
	// System.Single RPGCharacterControllerFREE::walkSpeed
	float ___walkSpeed_31;
	// System.Single RPGCharacterControllerFREE::moveSpeed
	float ___moveSpeed_32;
	// System.Single RPGCharacterControllerFREE::runSpeed
	float ___runSpeed_33;
	// System.Single RPGCharacterControllerFREE::rotationSpeed
	float ___rotationSpeed_34;
	// System.Single RPGCharacterControllerFREE::x
	float ___x_35;
	// System.Single RPGCharacterControllerFREE::z
	float ___z_36;
	// System.Single RPGCharacterControllerFREE::dv
	float ___dv_37;
	// System.Single RPGCharacterControllerFREE::dh
	float ___dh_38;
	// UnityEngine.Vector3 RPGCharacterControllerFREE::inputVec
	Vector3_t3722313464  ___inputVec_39;
	// UnityEngine.Vector3 RPGCharacterControllerFREE::newVelocity
	Vector3_t3722313464  ___newVelocity_40;
	// Weapon RPGCharacterControllerFREE::weapon
	int32_t ___weapon_41;
	// System.Int32 RPGCharacterControllerFREE::rightWeapon
	int32_t ___rightWeapon_42;
	// System.Int32 RPGCharacterControllerFREE::leftWeapon
	int32_t ___leftWeapon_43;
	// System.Boolean RPGCharacterControllerFREE::isRelax
	bool ___isRelax_44;
	// System.Boolean RPGCharacterControllerFREE::canAction
	bool ___canAction_45;
	// System.Boolean RPGCharacterControllerFREE::isStrafing
	bool ___isStrafing_46;
	// System.Boolean RPGCharacterControllerFREE::isDead
	bool ___isDead_47;
	// System.Boolean RPGCharacterControllerFREE::isBlocking
	bool ___isBlocking_48;
	// System.Single RPGCharacterControllerFREE::knockbackMultiplier
	float ___knockbackMultiplier_49;
	// System.Boolean RPGCharacterControllerFREE::isKnockback
	bool ___isKnockback_50;

public:
	inline static int32_t get_offset_of_rb_2() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___rb_2)); }
	inline Rigidbody_t3916780224 * get_rb_2() const { return ___rb_2; }
	inline Rigidbody_t3916780224 ** get_address_of_rb_2() { return &___rb_2; }
	inline void set_rb_2(Rigidbody_t3916780224 * value)
	{
		___rb_2 = value;
		Il2CppCodeGenWriteBarrier((&___rb_2), value);
	}

	inline static int32_t get_offset_of_animator_3() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___animator_3)); }
	inline Animator_t434523843 * get_animator_3() const { return ___animator_3; }
	inline Animator_t434523843 ** get_address_of_animator_3() { return &___animator_3; }
	inline void set_animator_3(Animator_t434523843 * value)
	{
		___animator_3 = value;
		Il2CppCodeGenWriteBarrier((&___animator_3), value);
	}

	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___target_4)); }
	inline GameObject_t1113636619 * get_target_4() const { return ___target_4; }
	inline GameObject_t1113636619 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(GameObject_t1113636619 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_targetDashDirection_5() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___targetDashDirection_5)); }
	inline Vector3_t3722313464  get_targetDashDirection_5() const { return ___targetDashDirection_5; }
	inline Vector3_t3722313464 * get_address_of_targetDashDirection_5() { return &___targetDashDirection_5; }
	inline void set_targetDashDirection_5(Vector3_t3722313464  value)
	{
		___targetDashDirection_5 = value;
	}

	inline static int32_t get_offset_of_sceneCamera_6() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___sceneCamera_6)); }
	inline Camera_t4157153871 * get_sceneCamera_6() const { return ___sceneCamera_6; }
	inline Camera_t4157153871 ** get_address_of_sceneCamera_6() { return &___sceneCamera_6; }
	inline void set_sceneCamera_6(Camera_t4157153871 * value)
	{
		___sceneCamera_6 = value;
		Il2CppCodeGenWriteBarrier((&___sceneCamera_6), value);
	}

	inline static int32_t get_offset_of_useNavMesh_7() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___useNavMesh_7)); }
	inline bool get_useNavMesh_7() const { return ___useNavMesh_7; }
	inline bool* get_address_of_useNavMesh_7() { return &___useNavMesh_7; }
	inline void set_useNavMesh_7(bool value)
	{
		___useNavMesh_7 = value;
	}

	inline static int32_t get_offset_of_agent_8() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___agent_8)); }
	inline NavMeshAgent_t1276799816 * get_agent_8() const { return ___agent_8; }
	inline NavMeshAgent_t1276799816 ** get_address_of_agent_8() { return &___agent_8; }
	inline void set_agent_8(NavMeshAgent_t1276799816 * value)
	{
		___agent_8 = value;
		Il2CppCodeGenWriteBarrier((&___agent_8), value);
	}

	inline static int32_t get_offset_of_navMeshSpeed_9() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___navMeshSpeed_9)); }
	inline float get_navMeshSpeed_9() const { return ___navMeshSpeed_9; }
	inline float* get_address_of_navMeshSpeed_9() { return &___navMeshSpeed_9; }
	inline void set_navMeshSpeed_9(float value)
	{
		___navMeshSpeed_9 = value;
	}

	inline static int32_t get_offset_of_goal_10() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___goal_10)); }
	inline Transform_t3600365921 * get_goal_10() const { return ___goal_10; }
	inline Transform_t3600365921 ** get_address_of_goal_10() { return &___goal_10; }
	inline void set_goal_10(Transform_t3600365921 * value)
	{
		___goal_10 = value;
		Il2CppCodeGenWriteBarrier((&___goal_10), value);
	}

	inline static int32_t get_offset_of_gravity_11() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___gravity_11)); }
	inline float get_gravity_11() const { return ___gravity_11; }
	inline float* get_address_of_gravity_11() { return &___gravity_11; }
	inline void set_gravity_11(float value)
	{
		___gravity_11 = value;
	}

	inline static int32_t get_offset_of_canJump_12() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___canJump_12)); }
	inline bool get_canJump_12() const { return ___canJump_12; }
	inline bool* get_address_of_canJump_12() { return &___canJump_12; }
	inline void set_canJump_12(bool value)
	{
		___canJump_12 = value;
	}

	inline static int32_t get_offset_of_isJumping_13() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___isJumping_13)); }
	inline bool get_isJumping_13() const { return ___isJumping_13; }
	inline bool* get_address_of_isJumping_13() { return &___isJumping_13; }
	inline void set_isJumping_13(bool value)
	{
		___isJumping_13 = value;
	}

	inline static int32_t get_offset_of_isGrounded_14() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___isGrounded_14)); }
	inline bool get_isGrounded_14() const { return ___isGrounded_14; }
	inline bool* get_address_of_isGrounded_14() { return &___isGrounded_14; }
	inline void set_isGrounded_14(bool value)
	{
		___isGrounded_14 = value;
	}

	inline static int32_t get_offset_of_jumpSpeed_15() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___jumpSpeed_15)); }
	inline float get_jumpSpeed_15() const { return ___jumpSpeed_15; }
	inline float* get_address_of_jumpSpeed_15() { return &___jumpSpeed_15; }
	inline void set_jumpSpeed_15(float value)
	{
		___jumpSpeed_15 = value;
	}

	inline static int32_t get_offset_of_doublejumpSpeed_16() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___doublejumpSpeed_16)); }
	inline float get_doublejumpSpeed_16() const { return ___doublejumpSpeed_16; }
	inline float* get_address_of_doublejumpSpeed_16() { return &___doublejumpSpeed_16; }
	inline void set_doublejumpSpeed_16(float value)
	{
		___doublejumpSpeed_16 = value;
	}

	inline static int32_t get_offset_of_doublejumping_17() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___doublejumping_17)); }
	inline bool get_doublejumping_17() const { return ___doublejumping_17; }
	inline bool* get_address_of_doublejumping_17() { return &___doublejumping_17; }
	inline void set_doublejumping_17(bool value)
	{
		___doublejumping_17 = value;
	}

	inline static int32_t get_offset_of_canDoubleJump_18() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___canDoubleJump_18)); }
	inline bool get_canDoubleJump_18() const { return ___canDoubleJump_18; }
	inline bool* get_address_of_canDoubleJump_18() { return &___canDoubleJump_18; }
	inline void set_canDoubleJump_18(bool value)
	{
		___canDoubleJump_18 = value;
	}

	inline static int32_t get_offset_of_isDoubleJumping_19() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___isDoubleJumping_19)); }
	inline bool get_isDoubleJumping_19() const { return ___isDoubleJumping_19; }
	inline bool* get_address_of_isDoubleJumping_19() { return &___isDoubleJumping_19; }
	inline void set_isDoubleJumping_19(bool value)
	{
		___isDoubleJumping_19 = value;
	}

	inline static int32_t get_offset_of_doublejumped_20() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___doublejumped_20)); }
	inline bool get_doublejumped_20() const { return ___doublejumped_20; }
	inline bool* get_address_of_doublejumped_20() { return &___doublejumped_20; }
	inline void set_doublejumped_20(bool value)
	{
		___doublejumped_20 = value;
	}

	inline static int32_t get_offset_of_isFalling_21() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___isFalling_21)); }
	inline bool get_isFalling_21() const { return ___isFalling_21; }
	inline bool* get_address_of_isFalling_21() { return &___isFalling_21; }
	inline void set_isFalling_21(bool value)
	{
		___isFalling_21 = value;
	}

	inline static int32_t get_offset_of_startFall_22() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___startFall_22)); }
	inline bool get_startFall_22() const { return ___startFall_22; }
	inline bool* get_address_of_startFall_22() { return &___startFall_22; }
	inline void set_startFall_22(bool value)
	{
		___startFall_22 = value;
	}

	inline static int32_t get_offset_of_fallingVelocity_23() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___fallingVelocity_23)); }
	inline float get_fallingVelocity_23() const { return ___fallingVelocity_23; }
	inline float* get_address_of_fallingVelocity_23() { return &___fallingVelocity_23; }
	inline void set_fallingVelocity_23(float value)
	{
		___fallingVelocity_23 = value;
	}

	inline static int32_t get_offset_of_inAirSpeed_24() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___inAirSpeed_24)); }
	inline float get_inAirSpeed_24() const { return ___inAirSpeed_24; }
	inline float* get_address_of_inAirSpeed_24() { return &___inAirSpeed_24; }
	inline void set_inAirSpeed_24(float value)
	{
		___inAirSpeed_24 = value;
	}

	inline static int32_t get_offset_of_maxVelocity_25() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___maxVelocity_25)); }
	inline float get_maxVelocity_25() const { return ___maxVelocity_25; }
	inline float* get_address_of_maxVelocity_25() { return &___maxVelocity_25; }
	inline void set_maxVelocity_25(float value)
	{
		___maxVelocity_25 = value;
	}

	inline static int32_t get_offset_of_minVelocity_26() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___minVelocity_26)); }
	inline float get_minVelocity_26() const { return ___minVelocity_26; }
	inline float* get_address_of_minVelocity_26() { return &___minVelocity_26; }
	inline void set_minVelocity_26(float value)
	{
		___minVelocity_26 = value;
	}

	inline static int32_t get_offset_of_rollSpeed_27() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___rollSpeed_27)); }
	inline float get_rollSpeed_27() const { return ___rollSpeed_27; }
	inline float* get_address_of_rollSpeed_27() { return &___rollSpeed_27; }
	inline void set_rollSpeed_27(float value)
	{
		___rollSpeed_27 = value;
	}

	inline static int32_t get_offset_of_isRolling_28() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___isRolling_28)); }
	inline bool get_isRolling_28() const { return ___isRolling_28; }
	inline bool* get_address_of_isRolling_28() { return &___isRolling_28; }
	inline void set_isRolling_28(bool value)
	{
		___isRolling_28 = value;
	}

	inline static int32_t get_offset_of_rollduration_29() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___rollduration_29)); }
	inline float get_rollduration_29() const { return ___rollduration_29; }
	inline float* get_address_of_rollduration_29() { return &___rollduration_29; }
	inline void set_rollduration_29(float value)
	{
		___rollduration_29 = value;
	}

	inline static int32_t get_offset_of_canMove_30() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___canMove_30)); }
	inline bool get_canMove_30() const { return ___canMove_30; }
	inline bool* get_address_of_canMove_30() { return &___canMove_30; }
	inline void set_canMove_30(bool value)
	{
		___canMove_30 = value;
	}

	inline static int32_t get_offset_of_walkSpeed_31() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___walkSpeed_31)); }
	inline float get_walkSpeed_31() const { return ___walkSpeed_31; }
	inline float* get_address_of_walkSpeed_31() { return &___walkSpeed_31; }
	inline void set_walkSpeed_31(float value)
	{
		___walkSpeed_31 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_32() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___moveSpeed_32)); }
	inline float get_moveSpeed_32() const { return ___moveSpeed_32; }
	inline float* get_address_of_moveSpeed_32() { return &___moveSpeed_32; }
	inline void set_moveSpeed_32(float value)
	{
		___moveSpeed_32 = value;
	}

	inline static int32_t get_offset_of_runSpeed_33() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___runSpeed_33)); }
	inline float get_runSpeed_33() const { return ___runSpeed_33; }
	inline float* get_address_of_runSpeed_33() { return &___runSpeed_33; }
	inline void set_runSpeed_33(float value)
	{
		___runSpeed_33 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_34() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___rotationSpeed_34)); }
	inline float get_rotationSpeed_34() const { return ___rotationSpeed_34; }
	inline float* get_address_of_rotationSpeed_34() { return &___rotationSpeed_34; }
	inline void set_rotationSpeed_34(float value)
	{
		___rotationSpeed_34 = value;
	}

	inline static int32_t get_offset_of_x_35() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___x_35)); }
	inline float get_x_35() const { return ___x_35; }
	inline float* get_address_of_x_35() { return &___x_35; }
	inline void set_x_35(float value)
	{
		___x_35 = value;
	}

	inline static int32_t get_offset_of_z_36() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___z_36)); }
	inline float get_z_36() const { return ___z_36; }
	inline float* get_address_of_z_36() { return &___z_36; }
	inline void set_z_36(float value)
	{
		___z_36 = value;
	}

	inline static int32_t get_offset_of_dv_37() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___dv_37)); }
	inline float get_dv_37() const { return ___dv_37; }
	inline float* get_address_of_dv_37() { return &___dv_37; }
	inline void set_dv_37(float value)
	{
		___dv_37 = value;
	}

	inline static int32_t get_offset_of_dh_38() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___dh_38)); }
	inline float get_dh_38() const { return ___dh_38; }
	inline float* get_address_of_dh_38() { return &___dh_38; }
	inline void set_dh_38(float value)
	{
		___dh_38 = value;
	}

	inline static int32_t get_offset_of_inputVec_39() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___inputVec_39)); }
	inline Vector3_t3722313464  get_inputVec_39() const { return ___inputVec_39; }
	inline Vector3_t3722313464 * get_address_of_inputVec_39() { return &___inputVec_39; }
	inline void set_inputVec_39(Vector3_t3722313464  value)
	{
		___inputVec_39 = value;
	}

	inline static int32_t get_offset_of_newVelocity_40() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___newVelocity_40)); }
	inline Vector3_t3722313464  get_newVelocity_40() const { return ___newVelocity_40; }
	inline Vector3_t3722313464 * get_address_of_newVelocity_40() { return &___newVelocity_40; }
	inline void set_newVelocity_40(Vector3_t3722313464  value)
	{
		___newVelocity_40 = value;
	}

	inline static int32_t get_offset_of_weapon_41() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___weapon_41)); }
	inline int32_t get_weapon_41() const { return ___weapon_41; }
	inline int32_t* get_address_of_weapon_41() { return &___weapon_41; }
	inline void set_weapon_41(int32_t value)
	{
		___weapon_41 = value;
	}

	inline static int32_t get_offset_of_rightWeapon_42() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___rightWeapon_42)); }
	inline int32_t get_rightWeapon_42() const { return ___rightWeapon_42; }
	inline int32_t* get_address_of_rightWeapon_42() { return &___rightWeapon_42; }
	inline void set_rightWeapon_42(int32_t value)
	{
		___rightWeapon_42 = value;
	}

	inline static int32_t get_offset_of_leftWeapon_43() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___leftWeapon_43)); }
	inline int32_t get_leftWeapon_43() const { return ___leftWeapon_43; }
	inline int32_t* get_address_of_leftWeapon_43() { return &___leftWeapon_43; }
	inline void set_leftWeapon_43(int32_t value)
	{
		___leftWeapon_43 = value;
	}

	inline static int32_t get_offset_of_isRelax_44() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___isRelax_44)); }
	inline bool get_isRelax_44() const { return ___isRelax_44; }
	inline bool* get_address_of_isRelax_44() { return &___isRelax_44; }
	inline void set_isRelax_44(bool value)
	{
		___isRelax_44 = value;
	}

	inline static int32_t get_offset_of_canAction_45() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___canAction_45)); }
	inline bool get_canAction_45() const { return ___canAction_45; }
	inline bool* get_address_of_canAction_45() { return &___canAction_45; }
	inline void set_canAction_45(bool value)
	{
		___canAction_45 = value;
	}

	inline static int32_t get_offset_of_isStrafing_46() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___isStrafing_46)); }
	inline bool get_isStrafing_46() const { return ___isStrafing_46; }
	inline bool* get_address_of_isStrafing_46() { return &___isStrafing_46; }
	inline void set_isStrafing_46(bool value)
	{
		___isStrafing_46 = value;
	}

	inline static int32_t get_offset_of_isDead_47() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___isDead_47)); }
	inline bool get_isDead_47() const { return ___isDead_47; }
	inline bool* get_address_of_isDead_47() { return &___isDead_47; }
	inline void set_isDead_47(bool value)
	{
		___isDead_47 = value;
	}

	inline static int32_t get_offset_of_isBlocking_48() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___isBlocking_48)); }
	inline bool get_isBlocking_48() const { return ___isBlocking_48; }
	inline bool* get_address_of_isBlocking_48() { return &___isBlocking_48; }
	inline void set_isBlocking_48(bool value)
	{
		___isBlocking_48 = value;
	}

	inline static int32_t get_offset_of_knockbackMultiplier_49() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___knockbackMultiplier_49)); }
	inline float get_knockbackMultiplier_49() const { return ___knockbackMultiplier_49; }
	inline float* get_address_of_knockbackMultiplier_49() { return &___knockbackMultiplier_49; }
	inline void set_knockbackMultiplier_49(float value)
	{
		___knockbackMultiplier_49 = value;
	}

	inline static int32_t get_offset_of_isKnockback_50() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3036052416, ___isKnockback_50)); }
	inline bool get_isKnockback_50() const { return ___isKnockback_50; }
	inline bool* get_address_of_isKnockback_50() { return &___isKnockback_50; }
	inline void set_isKnockback_50(bool value)
	{
		___isKnockback_50 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RPGCHARACTERCONTROLLERFREE_T3036052416_H
#ifndef CLEANUPCAGE_T1266394266_H
#define CLEANUPCAGE_T1266394266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CleanUpCage
struct  CleanUpCage_t1266394266  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLEANUPCAGE_T1266394266_H
#ifndef COLOURGENERATOR_T3527398847_H
#define COLOURGENERATOR_T3527398847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColourGenerator
struct  ColourGenerator_t3527398847  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ColourGenerator::suit
	GameObject_t1113636619 * ___suit_2;
	// UnityEngine.GameObject ColourGenerator::shark
	GameObject_t1113636619 * ___shark_3;
	// UnityEngine.SphereCollider ColourGenerator::collider
	SphereCollider_t2077223608 * ___collider_4;

public:
	inline static int32_t get_offset_of_suit_2() { return static_cast<int32_t>(offsetof(ColourGenerator_t3527398847, ___suit_2)); }
	inline GameObject_t1113636619 * get_suit_2() const { return ___suit_2; }
	inline GameObject_t1113636619 ** get_address_of_suit_2() { return &___suit_2; }
	inline void set_suit_2(GameObject_t1113636619 * value)
	{
		___suit_2 = value;
		Il2CppCodeGenWriteBarrier((&___suit_2), value);
	}

	inline static int32_t get_offset_of_shark_3() { return static_cast<int32_t>(offsetof(ColourGenerator_t3527398847, ___shark_3)); }
	inline GameObject_t1113636619 * get_shark_3() const { return ___shark_3; }
	inline GameObject_t1113636619 ** get_address_of_shark_3() { return &___shark_3; }
	inline void set_shark_3(GameObject_t1113636619 * value)
	{
		___shark_3 = value;
		Il2CppCodeGenWriteBarrier((&___shark_3), value);
	}

	inline static int32_t get_offset_of_collider_4() { return static_cast<int32_t>(offsetof(ColourGenerator_t3527398847, ___collider_4)); }
	inline SphereCollider_t2077223608 * get_collider_4() const { return ___collider_4; }
	inline SphereCollider_t2077223608 ** get_address_of_collider_4() { return &___collider_4; }
	inline void set_collider_4(SphereCollider_t2077223608 * value)
	{
		___collider_4 = value;
		Il2CppCodeGenWriteBarrier((&___collider_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOURGENERATOR_T3527398847_H
#ifndef CAGECOLLISION_T1104701574_H
#define CAGECOLLISION_T1104701574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CageCollision
struct  CageCollision_t1104701574  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CageCollision::shark
	GameObject_t1113636619 * ___shark_2;
	// UnityEngine.Collider CageCollision::sphereCollider
	Collider_t1773347010 * ___sphereCollider_3;

public:
	inline static int32_t get_offset_of_shark_2() { return static_cast<int32_t>(offsetof(CageCollision_t1104701574, ___shark_2)); }
	inline GameObject_t1113636619 * get_shark_2() const { return ___shark_2; }
	inline GameObject_t1113636619 ** get_address_of_shark_2() { return &___shark_2; }
	inline void set_shark_2(GameObject_t1113636619 * value)
	{
		___shark_2 = value;
		Il2CppCodeGenWriteBarrier((&___shark_2), value);
	}

	inline static int32_t get_offset_of_sphereCollider_3() { return static_cast<int32_t>(offsetof(CageCollision_t1104701574, ___sphereCollider_3)); }
	inline Collider_t1773347010 * get_sphereCollider_3() const { return ___sphereCollider_3; }
	inline Collider_t1773347010 ** get_address_of_sphereCollider_3() { return &___sphereCollider_3; }
	inline void set_sphereCollider_3(Collider_t1773347010 * value)
	{
		___sphereCollider_3 = value;
		Il2CppCodeGenWriteBarrier((&___sphereCollider_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAGECOLLISION_T1104701574_H
#ifndef BEACHMEN_T363650106_H
#define BEACHMEN_T363650106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BeachMen
struct  BeachMen_t363650106  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animator BeachMen::animator
	Animator_t434523843 * ___animator_2;

public:
	inline static int32_t get_offset_of_animator_2() { return static_cast<int32_t>(offsetof(BeachMen_t363650106, ___animator_2)); }
	inline Animator_t434523843 * get_animator_2() const { return ___animator_2; }
	inline Animator_t434523843 ** get_address_of_animator_2() { return &___animator_2; }
	inline void set_animator_2(Animator_t434523843 * value)
	{
		___animator_2 = value;
		Il2CppCodeGenWriteBarrier((&___animator_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEACHMEN_T363650106_H
#ifndef CAGEAPPEARANCE_T1912774197_H
#define CAGEAPPEARANCE_T1912774197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CageAppearance
struct  CageAppearance_t1912774197  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Material[] CageAppearance::colours
	MaterialU5BU5D_t561872642* ___colours_2;
	// UnityEngine.GameObject CageAppearance::cageBody
	GameObject_t1113636619 * ___cageBody_3;
	// UnityEngine.GameObject CageAppearance::cageDoor
	GameObject_t1113636619 * ___cageDoor_4;

public:
	inline static int32_t get_offset_of_colours_2() { return static_cast<int32_t>(offsetof(CageAppearance_t1912774197, ___colours_2)); }
	inline MaterialU5BU5D_t561872642* get_colours_2() const { return ___colours_2; }
	inline MaterialU5BU5D_t561872642** get_address_of_colours_2() { return &___colours_2; }
	inline void set_colours_2(MaterialU5BU5D_t561872642* value)
	{
		___colours_2 = value;
		Il2CppCodeGenWriteBarrier((&___colours_2), value);
	}

	inline static int32_t get_offset_of_cageBody_3() { return static_cast<int32_t>(offsetof(CageAppearance_t1912774197, ___cageBody_3)); }
	inline GameObject_t1113636619 * get_cageBody_3() const { return ___cageBody_3; }
	inline GameObject_t1113636619 ** get_address_of_cageBody_3() { return &___cageBody_3; }
	inline void set_cageBody_3(GameObject_t1113636619 * value)
	{
		___cageBody_3 = value;
		Il2CppCodeGenWriteBarrier((&___cageBody_3), value);
	}

	inline static int32_t get_offset_of_cageDoor_4() { return static_cast<int32_t>(offsetof(CageAppearance_t1912774197, ___cageDoor_4)); }
	inline GameObject_t1113636619 * get_cageDoor_4() const { return ___cageDoor_4; }
	inline GameObject_t1113636619 ** get_address_of_cageDoor_4() { return &___cageDoor_4; }
	inline void set_cageDoor_4(GameObject_t1113636619 * value)
	{
		___cageDoor_4 = value;
		Il2CppCodeGenWriteBarrier((&___cageDoor_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAGEAPPEARANCE_T1912774197_H
#ifndef TMP_TEXTSELECTOR_A_T3982526506_H
#define TMP_TEXTSELECTOR_A_T3982526506_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_TextSelector_A
struct  TMP_TextSelector_A_t3982526506  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TextMeshPro TMPro.Examples.TMP_TextSelector_A::m_TextMeshPro
	TextMeshPro_t2393593166 * ___m_TextMeshPro_2;
	// UnityEngine.Camera TMPro.Examples.TMP_TextSelector_A::m_Camera
	Camera_t4157153871 * ___m_Camera_3;
	// System.Boolean TMPro.Examples.TMP_TextSelector_A::m_isHoveringObject
	bool ___m_isHoveringObject_4;
	// System.Int32 TMPro.Examples.TMP_TextSelector_A::m_selectedLink
	int32_t ___m_selectedLink_5;
	// System.Int32 TMPro.Examples.TMP_TextSelector_A::m_lastCharIndex
	int32_t ___m_lastCharIndex_6;
	// System.Int32 TMPro.Examples.TMP_TextSelector_A::m_lastWordIndex
	int32_t ___m_lastWordIndex_7;

public:
	inline static int32_t get_offset_of_m_TextMeshPro_2() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t3982526506, ___m_TextMeshPro_2)); }
	inline TextMeshPro_t2393593166 * get_m_TextMeshPro_2() const { return ___m_TextMeshPro_2; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_TextMeshPro_2() { return &___m_TextMeshPro_2; }
	inline void set_m_TextMeshPro_2(TextMeshPro_t2393593166 * value)
	{
		___m_TextMeshPro_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_2), value);
	}

	inline static int32_t get_offset_of_m_Camera_3() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t3982526506, ___m_Camera_3)); }
	inline Camera_t4157153871 * get_m_Camera_3() const { return ___m_Camera_3; }
	inline Camera_t4157153871 ** get_address_of_m_Camera_3() { return &___m_Camera_3; }
	inline void set_m_Camera_3(Camera_t4157153871 * value)
	{
		___m_Camera_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_3), value);
	}

	inline static int32_t get_offset_of_m_isHoveringObject_4() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t3982526506, ___m_isHoveringObject_4)); }
	inline bool get_m_isHoveringObject_4() const { return ___m_isHoveringObject_4; }
	inline bool* get_address_of_m_isHoveringObject_4() { return &___m_isHoveringObject_4; }
	inline void set_m_isHoveringObject_4(bool value)
	{
		___m_isHoveringObject_4 = value;
	}

	inline static int32_t get_offset_of_m_selectedLink_5() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t3982526506, ___m_selectedLink_5)); }
	inline int32_t get_m_selectedLink_5() const { return ___m_selectedLink_5; }
	inline int32_t* get_address_of_m_selectedLink_5() { return &___m_selectedLink_5; }
	inline void set_m_selectedLink_5(int32_t value)
	{
		___m_selectedLink_5 = value;
	}

	inline static int32_t get_offset_of_m_lastCharIndex_6() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t3982526506, ___m_lastCharIndex_6)); }
	inline int32_t get_m_lastCharIndex_6() const { return ___m_lastCharIndex_6; }
	inline int32_t* get_address_of_m_lastCharIndex_6() { return &___m_lastCharIndex_6; }
	inline void set_m_lastCharIndex_6(int32_t value)
	{
		___m_lastCharIndex_6 = value;
	}

	inline static int32_t get_offset_of_m_lastWordIndex_7() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t3982526506, ___m_lastWordIndex_7)); }
	inline int32_t get_m_lastWordIndex_7() const { return ___m_lastWordIndex_7; }
	inline int32_t* get_address_of_m_lastWordIndex_7() { return &___m_lastWordIndex_7; }
	inline void set_m_lastWordIndex_7(int32_t value)
	{
		___m_lastWordIndex_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTSELECTOR_A_T3982526506_H
#ifndef BENCHMARK04_T1570876016_H
#define BENCHMARK04_T1570876016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark04
struct  Benchmark04_t1570876016  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark04::SpawnType
	int32_t ___SpawnType_2;
	// System.Int32 TMPro.Examples.Benchmark04::MinPointSize
	int32_t ___MinPointSize_3;
	// System.Int32 TMPro.Examples.Benchmark04::MaxPointSize
	int32_t ___MaxPointSize_4;
	// System.Int32 TMPro.Examples.Benchmark04::Steps
	int32_t ___Steps_5;
	// UnityEngine.Transform TMPro.Examples.Benchmark04::m_Transform
	Transform_t3600365921 * ___m_Transform_6;

public:
	inline static int32_t get_offset_of_SpawnType_2() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___SpawnType_2)); }
	inline int32_t get_SpawnType_2() const { return ___SpawnType_2; }
	inline int32_t* get_address_of_SpawnType_2() { return &___SpawnType_2; }
	inline void set_SpawnType_2(int32_t value)
	{
		___SpawnType_2 = value;
	}

	inline static int32_t get_offset_of_MinPointSize_3() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___MinPointSize_3)); }
	inline int32_t get_MinPointSize_3() const { return ___MinPointSize_3; }
	inline int32_t* get_address_of_MinPointSize_3() { return &___MinPointSize_3; }
	inline void set_MinPointSize_3(int32_t value)
	{
		___MinPointSize_3 = value;
	}

	inline static int32_t get_offset_of_MaxPointSize_4() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___MaxPointSize_4)); }
	inline int32_t get_MaxPointSize_4() const { return ___MaxPointSize_4; }
	inline int32_t* get_address_of_MaxPointSize_4() { return &___MaxPointSize_4; }
	inline void set_MaxPointSize_4(int32_t value)
	{
		___MaxPointSize_4 = value;
	}

	inline static int32_t get_offset_of_Steps_5() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___Steps_5)); }
	inline int32_t get_Steps_5() const { return ___Steps_5; }
	inline int32_t* get_address_of_Steps_5() { return &___Steps_5; }
	inline void set_Steps_5(int32_t value)
	{
		___Steps_5 = value;
	}

	inline static int32_t get_offset_of_m_Transform_6() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___m_Transform_6)); }
	inline Transform_t3600365921 * get_m_Transform_6() const { return ___m_Transform_6; }
	inline Transform_t3600365921 ** get_address_of_m_Transform_6() { return &___m_Transform_6; }
	inline void set_m_Transform_6(Transform_t3600365921 * value)
	{
		___m_Transform_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Transform_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK04_T1570876016_H
#ifndef CAMERACONTROLLER_T2264742161_H
#define CAMERACONTROLLER_T2264742161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.CameraController
struct  CameraController_t2264742161  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform TMPro.Examples.CameraController::cameraTransform
	Transform_t3600365921 * ___cameraTransform_2;
	// UnityEngine.Transform TMPro.Examples.CameraController::dummyTarget
	Transform_t3600365921 * ___dummyTarget_3;
	// UnityEngine.Transform TMPro.Examples.CameraController::CameraTarget
	Transform_t3600365921 * ___CameraTarget_4;
	// System.Single TMPro.Examples.CameraController::FollowDistance
	float ___FollowDistance_5;
	// System.Single TMPro.Examples.CameraController::MaxFollowDistance
	float ___MaxFollowDistance_6;
	// System.Single TMPro.Examples.CameraController::MinFollowDistance
	float ___MinFollowDistance_7;
	// System.Single TMPro.Examples.CameraController::ElevationAngle
	float ___ElevationAngle_8;
	// System.Single TMPro.Examples.CameraController::MaxElevationAngle
	float ___MaxElevationAngle_9;
	// System.Single TMPro.Examples.CameraController::MinElevationAngle
	float ___MinElevationAngle_10;
	// System.Single TMPro.Examples.CameraController::OrbitalAngle
	float ___OrbitalAngle_11;
	// TMPro.Examples.CameraController/CameraModes TMPro.Examples.CameraController::CameraMode
	int32_t ___CameraMode_12;
	// System.Boolean TMPro.Examples.CameraController::MovementSmoothing
	bool ___MovementSmoothing_13;
	// System.Boolean TMPro.Examples.CameraController::RotationSmoothing
	bool ___RotationSmoothing_14;
	// System.Boolean TMPro.Examples.CameraController::previousSmoothing
	bool ___previousSmoothing_15;
	// System.Single TMPro.Examples.CameraController::MovementSmoothingValue
	float ___MovementSmoothingValue_16;
	// System.Single TMPro.Examples.CameraController::RotationSmoothingValue
	float ___RotationSmoothingValue_17;
	// System.Single TMPro.Examples.CameraController::MoveSensitivity
	float ___MoveSensitivity_18;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::currentVelocity
	Vector3_t3722313464  ___currentVelocity_19;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::desiredPosition
	Vector3_t3722313464  ___desiredPosition_20;
	// System.Single TMPro.Examples.CameraController::mouseX
	float ___mouseX_21;
	// System.Single TMPro.Examples.CameraController::mouseY
	float ___mouseY_22;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::moveVector
	Vector3_t3722313464  ___moveVector_23;
	// System.Single TMPro.Examples.CameraController::mouseWheel
	float ___mouseWheel_24;

public:
	inline static int32_t get_offset_of_cameraTransform_2() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___cameraTransform_2)); }
	inline Transform_t3600365921 * get_cameraTransform_2() const { return ___cameraTransform_2; }
	inline Transform_t3600365921 ** get_address_of_cameraTransform_2() { return &___cameraTransform_2; }
	inline void set_cameraTransform_2(Transform_t3600365921 * value)
	{
		___cameraTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTransform_2), value);
	}

	inline static int32_t get_offset_of_dummyTarget_3() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___dummyTarget_3)); }
	inline Transform_t3600365921 * get_dummyTarget_3() const { return ___dummyTarget_3; }
	inline Transform_t3600365921 ** get_address_of_dummyTarget_3() { return &___dummyTarget_3; }
	inline void set_dummyTarget_3(Transform_t3600365921 * value)
	{
		___dummyTarget_3 = value;
		Il2CppCodeGenWriteBarrier((&___dummyTarget_3), value);
	}

	inline static int32_t get_offset_of_CameraTarget_4() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___CameraTarget_4)); }
	inline Transform_t3600365921 * get_CameraTarget_4() const { return ___CameraTarget_4; }
	inline Transform_t3600365921 ** get_address_of_CameraTarget_4() { return &___CameraTarget_4; }
	inline void set_CameraTarget_4(Transform_t3600365921 * value)
	{
		___CameraTarget_4 = value;
		Il2CppCodeGenWriteBarrier((&___CameraTarget_4), value);
	}

	inline static int32_t get_offset_of_FollowDistance_5() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___FollowDistance_5)); }
	inline float get_FollowDistance_5() const { return ___FollowDistance_5; }
	inline float* get_address_of_FollowDistance_5() { return &___FollowDistance_5; }
	inline void set_FollowDistance_5(float value)
	{
		___FollowDistance_5 = value;
	}

	inline static int32_t get_offset_of_MaxFollowDistance_6() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MaxFollowDistance_6)); }
	inline float get_MaxFollowDistance_6() const { return ___MaxFollowDistance_6; }
	inline float* get_address_of_MaxFollowDistance_6() { return &___MaxFollowDistance_6; }
	inline void set_MaxFollowDistance_6(float value)
	{
		___MaxFollowDistance_6 = value;
	}

	inline static int32_t get_offset_of_MinFollowDistance_7() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MinFollowDistance_7)); }
	inline float get_MinFollowDistance_7() const { return ___MinFollowDistance_7; }
	inline float* get_address_of_MinFollowDistance_7() { return &___MinFollowDistance_7; }
	inline void set_MinFollowDistance_7(float value)
	{
		___MinFollowDistance_7 = value;
	}

	inline static int32_t get_offset_of_ElevationAngle_8() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___ElevationAngle_8)); }
	inline float get_ElevationAngle_8() const { return ___ElevationAngle_8; }
	inline float* get_address_of_ElevationAngle_8() { return &___ElevationAngle_8; }
	inline void set_ElevationAngle_8(float value)
	{
		___ElevationAngle_8 = value;
	}

	inline static int32_t get_offset_of_MaxElevationAngle_9() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MaxElevationAngle_9)); }
	inline float get_MaxElevationAngle_9() const { return ___MaxElevationAngle_9; }
	inline float* get_address_of_MaxElevationAngle_9() { return &___MaxElevationAngle_9; }
	inline void set_MaxElevationAngle_9(float value)
	{
		___MaxElevationAngle_9 = value;
	}

	inline static int32_t get_offset_of_MinElevationAngle_10() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MinElevationAngle_10)); }
	inline float get_MinElevationAngle_10() const { return ___MinElevationAngle_10; }
	inline float* get_address_of_MinElevationAngle_10() { return &___MinElevationAngle_10; }
	inline void set_MinElevationAngle_10(float value)
	{
		___MinElevationAngle_10 = value;
	}

	inline static int32_t get_offset_of_OrbitalAngle_11() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___OrbitalAngle_11)); }
	inline float get_OrbitalAngle_11() const { return ___OrbitalAngle_11; }
	inline float* get_address_of_OrbitalAngle_11() { return &___OrbitalAngle_11; }
	inline void set_OrbitalAngle_11(float value)
	{
		___OrbitalAngle_11 = value;
	}

	inline static int32_t get_offset_of_CameraMode_12() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___CameraMode_12)); }
	inline int32_t get_CameraMode_12() const { return ___CameraMode_12; }
	inline int32_t* get_address_of_CameraMode_12() { return &___CameraMode_12; }
	inline void set_CameraMode_12(int32_t value)
	{
		___CameraMode_12 = value;
	}

	inline static int32_t get_offset_of_MovementSmoothing_13() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MovementSmoothing_13)); }
	inline bool get_MovementSmoothing_13() const { return ___MovementSmoothing_13; }
	inline bool* get_address_of_MovementSmoothing_13() { return &___MovementSmoothing_13; }
	inline void set_MovementSmoothing_13(bool value)
	{
		___MovementSmoothing_13 = value;
	}

	inline static int32_t get_offset_of_RotationSmoothing_14() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___RotationSmoothing_14)); }
	inline bool get_RotationSmoothing_14() const { return ___RotationSmoothing_14; }
	inline bool* get_address_of_RotationSmoothing_14() { return &___RotationSmoothing_14; }
	inline void set_RotationSmoothing_14(bool value)
	{
		___RotationSmoothing_14 = value;
	}

	inline static int32_t get_offset_of_previousSmoothing_15() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___previousSmoothing_15)); }
	inline bool get_previousSmoothing_15() const { return ___previousSmoothing_15; }
	inline bool* get_address_of_previousSmoothing_15() { return &___previousSmoothing_15; }
	inline void set_previousSmoothing_15(bool value)
	{
		___previousSmoothing_15 = value;
	}

	inline static int32_t get_offset_of_MovementSmoothingValue_16() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MovementSmoothingValue_16)); }
	inline float get_MovementSmoothingValue_16() const { return ___MovementSmoothingValue_16; }
	inline float* get_address_of_MovementSmoothingValue_16() { return &___MovementSmoothingValue_16; }
	inline void set_MovementSmoothingValue_16(float value)
	{
		___MovementSmoothingValue_16 = value;
	}

	inline static int32_t get_offset_of_RotationSmoothingValue_17() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___RotationSmoothingValue_17)); }
	inline float get_RotationSmoothingValue_17() const { return ___RotationSmoothingValue_17; }
	inline float* get_address_of_RotationSmoothingValue_17() { return &___RotationSmoothingValue_17; }
	inline void set_RotationSmoothingValue_17(float value)
	{
		___RotationSmoothingValue_17 = value;
	}

	inline static int32_t get_offset_of_MoveSensitivity_18() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MoveSensitivity_18)); }
	inline float get_MoveSensitivity_18() const { return ___MoveSensitivity_18; }
	inline float* get_address_of_MoveSensitivity_18() { return &___MoveSensitivity_18; }
	inline void set_MoveSensitivity_18(float value)
	{
		___MoveSensitivity_18 = value;
	}

	inline static int32_t get_offset_of_currentVelocity_19() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___currentVelocity_19)); }
	inline Vector3_t3722313464  get_currentVelocity_19() const { return ___currentVelocity_19; }
	inline Vector3_t3722313464 * get_address_of_currentVelocity_19() { return &___currentVelocity_19; }
	inline void set_currentVelocity_19(Vector3_t3722313464  value)
	{
		___currentVelocity_19 = value;
	}

	inline static int32_t get_offset_of_desiredPosition_20() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___desiredPosition_20)); }
	inline Vector3_t3722313464  get_desiredPosition_20() const { return ___desiredPosition_20; }
	inline Vector3_t3722313464 * get_address_of_desiredPosition_20() { return &___desiredPosition_20; }
	inline void set_desiredPosition_20(Vector3_t3722313464  value)
	{
		___desiredPosition_20 = value;
	}

	inline static int32_t get_offset_of_mouseX_21() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___mouseX_21)); }
	inline float get_mouseX_21() const { return ___mouseX_21; }
	inline float* get_address_of_mouseX_21() { return &___mouseX_21; }
	inline void set_mouseX_21(float value)
	{
		___mouseX_21 = value;
	}

	inline static int32_t get_offset_of_mouseY_22() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___mouseY_22)); }
	inline float get_mouseY_22() const { return ___mouseY_22; }
	inline float* get_address_of_mouseY_22() { return &___mouseY_22; }
	inline void set_mouseY_22(float value)
	{
		___mouseY_22 = value;
	}

	inline static int32_t get_offset_of_moveVector_23() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___moveVector_23)); }
	inline Vector3_t3722313464  get_moveVector_23() const { return ___moveVector_23; }
	inline Vector3_t3722313464 * get_address_of_moveVector_23() { return &___moveVector_23; }
	inline void set_moveVector_23(Vector3_t3722313464  value)
	{
		___moveVector_23 = value;
	}

	inline static int32_t get_offset_of_mouseWheel_24() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___mouseWheel_24)); }
	inline float get_mouseWheel_24() const { return ___mouseWheel_24; }
	inline float* get_address_of_mouseWheel_24() { return &___mouseWheel_24; }
	inline void set_mouseWheel_24(float value)
	{
		___mouseWheel_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACONTROLLER_T2264742161_H
#ifndef TEXTCONSOLESIMULATOR_T3766250034_H
#define TEXTCONSOLESIMULATOR_T3766250034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextConsoleSimulator
struct  TextConsoleSimulator_t3766250034  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_Text TMPro.Examples.TextConsoleSimulator::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_2;
	// System.Boolean TMPro.Examples.TextConsoleSimulator::hasTextChanged
	bool ___hasTextChanged_3;

public:
	inline static int32_t get_offset_of_m_TextComponent_2() { return static_cast<int32_t>(offsetof(TextConsoleSimulator_t3766250034, ___m_TextComponent_2)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_2() const { return ___m_TextComponent_2; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_2() { return &___m_TextComponent_2; }
	inline void set_m_TextComponent_2(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_2), value);
	}

	inline static int32_t get_offset_of_hasTextChanged_3() { return static_cast<int32_t>(offsetof(TextConsoleSimulator_t3766250034, ___hasTextChanged_3)); }
	inline bool get_hasTextChanged_3() const { return ___hasTextChanged_3; }
	inline bool* get_address_of_hasTextChanged_3() { return &___hasTextChanged_3; }
	inline void set_hasTextChanged_3(bool value)
	{
		___hasTextChanged_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTCONSOLESIMULATOR_T3766250034_H
#ifndef BENCHMARK02_T1571269232_H
#define BENCHMARK02_T1571269232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark02
struct  Benchmark02_t1571269232  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark02::SpawnType
	int32_t ___SpawnType_2;
	// System.Int32 TMPro.Examples.Benchmark02::NumberOfNPC
	int32_t ___NumberOfNPC_3;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.Benchmark02::floatingText_Script
	TextMeshProFloatingText_t845872552 * ___floatingText_Script_4;

public:
	inline static int32_t get_offset_of_SpawnType_2() { return static_cast<int32_t>(offsetof(Benchmark02_t1571269232, ___SpawnType_2)); }
	inline int32_t get_SpawnType_2() const { return ___SpawnType_2; }
	inline int32_t* get_address_of_SpawnType_2() { return &___SpawnType_2; }
	inline void set_SpawnType_2(int32_t value)
	{
		___SpawnType_2 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_3() { return static_cast<int32_t>(offsetof(Benchmark02_t1571269232, ___NumberOfNPC_3)); }
	inline int32_t get_NumberOfNPC_3() const { return ___NumberOfNPC_3; }
	inline int32_t* get_address_of_NumberOfNPC_3() { return &___NumberOfNPC_3; }
	inline void set_NumberOfNPC_3(int32_t value)
	{
		___NumberOfNPC_3 = value;
	}

	inline static int32_t get_offset_of_floatingText_Script_4() { return static_cast<int32_t>(offsetof(Benchmark02_t1571269232, ___floatingText_Script_4)); }
	inline TextMeshProFloatingText_t845872552 * get_floatingText_Script_4() const { return ___floatingText_Script_4; }
	inline TextMeshProFloatingText_t845872552 ** get_address_of_floatingText_Script_4() { return &___floatingText_Script_4; }
	inline void set_floatingText_Script_4(TextMeshProFloatingText_t845872552 * value)
	{
		___floatingText_Script_4 = value;
		Il2CppCodeGenWriteBarrier((&___floatingText_Script_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK02_T1571269232_H
#ifndef BENCHMARK03_T1571203696_H
#define BENCHMARK03_T1571203696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark03
struct  Benchmark03_t1571203696  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark03::SpawnType
	int32_t ___SpawnType_2;
	// System.Int32 TMPro.Examples.Benchmark03::NumberOfNPC
	int32_t ___NumberOfNPC_3;
	// UnityEngine.Font TMPro.Examples.Benchmark03::TheFont
	Font_t1956802104 * ___TheFont_4;

public:
	inline static int32_t get_offset_of_SpawnType_2() { return static_cast<int32_t>(offsetof(Benchmark03_t1571203696, ___SpawnType_2)); }
	inline int32_t get_SpawnType_2() const { return ___SpawnType_2; }
	inline int32_t* get_address_of_SpawnType_2() { return &___SpawnType_2; }
	inline void set_SpawnType_2(int32_t value)
	{
		___SpawnType_2 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_3() { return static_cast<int32_t>(offsetof(Benchmark03_t1571203696, ___NumberOfNPC_3)); }
	inline int32_t get_NumberOfNPC_3() const { return ___NumberOfNPC_3; }
	inline int32_t* get_address_of_NumberOfNPC_3() { return &___NumberOfNPC_3; }
	inline void set_NumberOfNPC_3(int32_t value)
	{
		___NumberOfNPC_3 = value;
	}

	inline static int32_t get_offset_of_TheFont_4() { return static_cast<int32_t>(offsetof(Benchmark03_t1571203696, ___TheFont_4)); }
	inline Font_t1956802104 * get_TheFont_4() const { return ___TheFont_4; }
	inline Font_t1956802104 ** get_address_of_TheFont_4() { return &___TheFont_4; }
	inline void set_TheFont_4(Font_t1956802104 * value)
	{
		___TheFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___TheFont_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK03_T1571203696_H
#ifndef SKEWTEXTEXAMPLE_T3460249701_H
#define SKEWTEXTEXAMPLE_T3460249701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.SkewTextExample
struct  SkewTextExample_t3460249701  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_Text TMPro.Examples.SkewTextExample::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_2;
	// UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::VertexCurve
	AnimationCurve_t3046754366 * ___VertexCurve_3;
	// System.Single TMPro.Examples.SkewTextExample::CurveScale
	float ___CurveScale_4;
	// System.Single TMPro.Examples.SkewTextExample::ShearAmount
	float ___ShearAmount_5;

public:
	inline static int32_t get_offset_of_m_TextComponent_2() { return static_cast<int32_t>(offsetof(SkewTextExample_t3460249701, ___m_TextComponent_2)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_2() const { return ___m_TextComponent_2; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_2() { return &___m_TextComponent_2; }
	inline void set_m_TextComponent_2(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_2), value);
	}

	inline static int32_t get_offset_of_VertexCurve_3() { return static_cast<int32_t>(offsetof(SkewTextExample_t3460249701, ___VertexCurve_3)); }
	inline AnimationCurve_t3046754366 * get_VertexCurve_3() const { return ___VertexCurve_3; }
	inline AnimationCurve_t3046754366 ** get_address_of_VertexCurve_3() { return &___VertexCurve_3; }
	inline void set_VertexCurve_3(AnimationCurve_t3046754366 * value)
	{
		___VertexCurve_3 = value;
		Il2CppCodeGenWriteBarrier((&___VertexCurve_3), value);
	}

	inline static int32_t get_offset_of_CurveScale_4() { return static_cast<int32_t>(offsetof(SkewTextExample_t3460249701, ___CurveScale_4)); }
	inline float get_CurveScale_4() const { return ___CurveScale_4; }
	inline float* get_address_of_CurveScale_4() { return &___CurveScale_4; }
	inline void set_CurveScale_4(float value)
	{
		___CurveScale_4 = value;
	}

	inline static int32_t get_offset_of_ShearAmount_5() { return static_cast<int32_t>(offsetof(SkewTextExample_t3460249701, ___ShearAmount_5)); }
	inline float get_ShearAmount_5() const { return ___ShearAmount_5; }
	inline float* get_address_of_ShearAmount_5() { return &___ShearAmount_5; }
	inline void set_ShearAmount_5(float value)
	{
		___ShearAmount_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKEWTEXTEXAMPLE_T3460249701_H
#ifndef ENVMAPANIMATOR_T1140999784_H
#define ENVMAPANIMATOR_T1140999784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnvMapAnimator
struct  EnvMapAnimator_t1140999784  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 EnvMapAnimator::RotationSpeeds
	Vector3_t3722313464  ___RotationSpeeds_2;
	// TMPro.TMP_Text EnvMapAnimator::m_textMeshPro
	TMP_Text_t2599618874 * ___m_textMeshPro_3;
	// UnityEngine.Material EnvMapAnimator::m_material
	Material_t340375123 * ___m_material_4;

public:
	inline static int32_t get_offset_of_RotationSpeeds_2() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1140999784, ___RotationSpeeds_2)); }
	inline Vector3_t3722313464  get_RotationSpeeds_2() const { return ___RotationSpeeds_2; }
	inline Vector3_t3722313464 * get_address_of_RotationSpeeds_2() { return &___RotationSpeeds_2; }
	inline void set_RotationSpeeds_2(Vector3_t3722313464  value)
	{
		___RotationSpeeds_2 = value;
	}

	inline static int32_t get_offset_of_m_textMeshPro_3() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1140999784, ___m_textMeshPro_3)); }
	inline TMP_Text_t2599618874 * get_m_textMeshPro_3() const { return ___m_textMeshPro_3; }
	inline TMP_Text_t2599618874 ** get_address_of_m_textMeshPro_3() { return &___m_textMeshPro_3; }
	inline void set_m_textMeshPro_3(TMP_Text_t2599618874 * value)
	{
		___m_textMeshPro_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_3), value);
	}

	inline static int32_t get_offset_of_m_material_4() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1140999784, ___m_material_4)); }
	inline Material_t340375123 * get_m_material_4() const { return ___m_material_4; }
	inline Material_t340375123 ** get_address_of_m_material_4() { return &___m_material_4; }
	inline void set_m_material_4(Material_t340375123 * value)
	{
		___m_material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_material_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENVMAPANIMATOR_T1140999784_H
#ifndef OBJECTSPIN_T341713598_H
#define OBJECTSPIN_T341713598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ObjectSpin
struct  ObjectSpin_t341713598  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TMPro.Examples.ObjectSpin::SpinSpeed
	float ___SpinSpeed_2;
	// System.Int32 TMPro.Examples.ObjectSpin::RotationRange
	int32_t ___RotationRange_3;
	// UnityEngine.Transform TMPro.Examples.ObjectSpin::m_transform
	Transform_t3600365921 * ___m_transform_4;
	// System.Single TMPro.Examples.ObjectSpin::m_time
	float ___m_time_5;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_prevPOS
	Vector3_t3722313464  ___m_prevPOS_6;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_initial_Rotation
	Vector3_t3722313464  ___m_initial_Rotation_7;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_initial_Position
	Vector3_t3722313464  ___m_initial_Position_8;
	// UnityEngine.Color32 TMPro.Examples.ObjectSpin::m_lightColor
	Color32_t2600501292  ___m_lightColor_9;
	// System.Int32 TMPro.Examples.ObjectSpin::frames
	int32_t ___frames_10;
	// TMPro.Examples.ObjectSpin/MotionType TMPro.Examples.ObjectSpin::Motion
	int32_t ___Motion_11;

public:
	inline static int32_t get_offset_of_SpinSpeed_2() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___SpinSpeed_2)); }
	inline float get_SpinSpeed_2() const { return ___SpinSpeed_2; }
	inline float* get_address_of_SpinSpeed_2() { return &___SpinSpeed_2; }
	inline void set_SpinSpeed_2(float value)
	{
		___SpinSpeed_2 = value;
	}

	inline static int32_t get_offset_of_RotationRange_3() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___RotationRange_3)); }
	inline int32_t get_RotationRange_3() const { return ___RotationRange_3; }
	inline int32_t* get_address_of_RotationRange_3() { return &___RotationRange_3; }
	inline void set_RotationRange_3(int32_t value)
	{
		___RotationRange_3 = value;
	}

	inline static int32_t get_offset_of_m_transform_4() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_transform_4)); }
	inline Transform_t3600365921 * get_m_transform_4() const { return ___m_transform_4; }
	inline Transform_t3600365921 ** get_address_of_m_transform_4() { return &___m_transform_4; }
	inline void set_m_transform_4(Transform_t3600365921 * value)
	{
		___m_transform_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_4), value);
	}

	inline static int32_t get_offset_of_m_time_5() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_time_5)); }
	inline float get_m_time_5() const { return ___m_time_5; }
	inline float* get_address_of_m_time_5() { return &___m_time_5; }
	inline void set_m_time_5(float value)
	{
		___m_time_5 = value;
	}

	inline static int32_t get_offset_of_m_prevPOS_6() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_prevPOS_6)); }
	inline Vector3_t3722313464  get_m_prevPOS_6() const { return ___m_prevPOS_6; }
	inline Vector3_t3722313464 * get_address_of_m_prevPOS_6() { return &___m_prevPOS_6; }
	inline void set_m_prevPOS_6(Vector3_t3722313464  value)
	{
		___m_prevPOS_6 = value;
	}

	inline static int32_t get_offset_of_m_initial_Rotation_7() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_initial_Rotation_7)); }
	inline Vector3_t3722313464  get_m_initial_Rotation_7() const { return ___m_initial_Rotation_7; }
	inline Vector3_t3722313464 * get_address_of_m_initial_Rotation_7() { return &___m_initial_Rotation_7; }
	inline void set_m_initial_Rotation_7(Vector3_t3722313464  value)
	{
		___m_initial_Rotation_7 = value;
	}

	inline static int32_t get_offset_of_m_initial_Position_8() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_initial_Position_8)); }
	inline Vector3_t3722313464  get_m_initial_Position_8() const { return ___m_initial_Position_8; }
	inline Vector3_t3722313464 * get_address_of_m_initial_Position_8() { return &___m_initial_Position_8; }
	inline void set_m_initial_Position_8(Vector3_t3722313464  value)
	{
		___m_initial_Position_8 = value;
	}

	inline static int32_t get_offset_of_m_lightColor_9() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_lightColor_9)); }
	inline Color32_t2600501292  get_m_lightColor_9() const { return ___m_lightColor_9; }
	inline Color32_t2600501292 * get_address_of_m_lightColor_9() { return &___m_lightColor_9; }
	inline void set_m_lightColor_9(Color32_t2600501292  value)
	{
		___m_lightColor_9 = value;
	}

	inline static int32_t get_offset_of_frames_10() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___frames_10)); }
	inline int32_t get_frames_10() const { return ___frames_10; }
	inline int32_t* get_address_of_frames_10() { return &___frames_10; }
	inline void set_frames_10(int32_t value)
	{
		___frames_10 = value;
	}

	inline static int32_t get_offset_of_Motion_11() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___Motion_11)); }
	inline int32_t get_Motion_11() const { return ___Motion_11; }
	inline int32_t* get_address_of_Motion_11() { return &___Motion_11; }
	inline void set_Motion_11(int32_t value)
	{
		___Motion_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTSPIN_T341713598_H
#ifndef SHADERPROPANIMATOR_T3617420994_H
#define SHADERPROPANIMATOR_T3617420994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ShaderPropAnimator
struct  ShaderPropAnimator_t3617420994  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Renderer TMPro.Examples.ShaderPropAnimator::m_Renderer
	Renderer_t2627027031 * ___m_Renderer_2;
	// UnityEngine.Material TMPro.Examples.ShaderPropAnimator::m_Material
	Material_t340375123 * ___m_Material_3;
	// UnityEngine.AnimationCurve TMPro.Examples.ShaderPropAnimator::GlowCurve
	AnimationCurve_t3046754366 * ___GlowCurve_4;
	// System.Single TMPro.Examples.ShaderPropAnimator::m_frame
	float ___m_frame_5;

public:
	inline static int32_t get_offset_of_m_Renderer_2() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t3617420994, ___m_Renderer_2)); }
	inline Renderer_t2627027031 * get_m_Renderer_2() const { return ___m_Renderer_2; }
	inline Renderer_t2627027031 ** get_address_of_m_Renderer_2() { return &___m_Renderer_2; }
	inline void set_m_Renderer_2(Renderer_t2627027031 * value)
	{
		___m_Renderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Renderer_2), value);
	}

	inline static int32_t get_offset_of_m_Material_3() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t3617420994, ___m_Material_3)); }
	inline Material_t340375123 * get_m_Material_3() const { return ___m_Material_3; }
	inline Material_t340375123 ** get_address_of_m_Material_3() { return &___m_Material_3; }
	inline void set_m_Material_3(Material_t340375123 * value)
	{
		___m_Material_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_3), value);
	}

	inline static int32_t get_offset_of_GlowCurve_4() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t3617420994, ___GlowCurve_4)); }
	inline AnimationCurve_t3046754366 * get_GlowCurve_4() const { return ___GlowCurve_4; }
	inline AnimationCurve_t3046754366 ** get_address_of_GlowCurve_4() { return &___GlowCurve_4; }
	inline void set_GlowCurve_4(AnimationCurve_t3046754366 * value)
	{
		___GlowCurve_4 = value;
		Il2CppCodeGenWriteBarrier((&___GlowCurve_4), value);
	}

	inline static int32_t get_offset_of_m_frame_5() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t3617420994, ___m_frame_5)); }
	inline float get_m_frame_5() const { return ___m_frame_5; }
	inline float* get_address_of_m_frame_5() { return &___m_frame_5; }
	inline void set_m_frame_5(float value)
	{
		___m_frame_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERPROPANIMATOR_T3617420994_H
#ifndef SIMPLESCRIPT_T3279312205_H
#define SIMPLESCRIPT_T3279312205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.SimpleScript
struct  SimpleScript_t3279312205  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TextMeshPro TMPro.Examples.SimpleScript::m_textMeshPro
	TextMeshPro_t2393593166 * ___m_textMeshPro_2;
	// System.Single TMPro.Examples.SimpleScript::m_frame
	float ___m_frame_4;

public:
	inline static int32_t get_offset_of_m_textMeshPro_2() { return static_cast<int32_t>(offsetof(SimpleScript_t3279312205, ___m_textMeshPro_2)); }
	inline TextMeshPro_t2393593166 * get_m_textMeshPro_2() const { return ___m_textMeshPro_2; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_textMeshPro_2() { return &___m_textMeshPro_2; }
	inline void set_m_textMeshPro_2(TextMeshPro_t2393593166 * value)
	{
		___m_textMeshPro_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_2), value);
	}

	inline static int32_t get_offset_of_m_frame_4() { return static_cast<int32_t>(offsetof(SimpleScript_t3279312205, ___m_frame_4)); }
	inline float get_m_frame_4() const { return ___m_frame_4; }
	inline float* get_address_of_m_frame_4() { return &___m_frame_4; }
	inline void set_m_frame_4(float value)
	{
		___m_frame_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLESCRIPT_T3279312205_H
#ifndef CHATCONTROLLER_T3486202795_H
#define CHATCONTROLLER_T3486202795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatController
struct  ChatController_t3486202795  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_InputField ChatController::TMP_ChatInput
	TMP_InputField_t1099764886 * ___TMP_ChatInput_2;
	// TMPro.TMP_Text ChatController::TMP_ChatOutput
	TMP_Text_t2599618874 * ___TMP_ChatOutput_3;
	// UnityEngine.UI.Scrollbar ChatController::ChatScrollbar
	Scrollbar_t1494447233 * ___ChatScrollbar_4;

public:
	inline static int32_t get_offset_of_TMP_ChatInput_2() { return static_cast<int32_t>(offsetof(ChatController_t3486202795, ___TMP_ChatInput_2)); }
	inline TMP_InputField_t1099764886 * get_TMP_ChatInput_2() const { return ___TMP_ChatInput_2; }
	inline TMP_InputField_t1099764886 ** get_address_of_TMP_ChatInput_2() { return &___TMP_ChatInput_2; }
	inline void set_TMP_ChatInput_2(TMP_InputField_t1099764886 * value)
	{
		___TMP_ChatInput_2 = value;
		Il2CppCodeGenWriteBarrier((&___TMP_ChatInput_2), value);
	}

	inline static int32_t get_offset_of_TMP_ChatOutput_3() { return static_cast<int32_t>(offsetof(ChatController_t3486202795, ___TMP_ChatOutput_3)); }
	inline TMP_Text_t2599618874 * get_TMP_ChatOutput_3() const { return ___TMP_ChatOutput_3; }
	inline TMP_Text_t2599618874 ** get_address_of_TMP_ChatOutput_3() { return &___TMP_ChatOutput_3; }
	inline void set_TMP_ChatOutput_3(TMP_Text_t2599618874 * value)
	{
		___TMP_ChatOutput_3 = value;
		Il2CppCodeGenWriteBarrier((&___TMP_ChatOutput_3), value);
	}

	inline static int32_t get_offset_of_ChatScrollbar_4() { return static_cast<int32_t>(offsetof(ChatController_t3486202795, ___ChatScrollbar_4)); }
	inline Scrollbar_t1494447233 * get_ChatScrollbar_4() const { return ___ChatScrollbar_4; }
	inline Scrollbar_t1494447233 ** get_address_of_ChatScrollbar_4() { return &___ChatScrollbar_4; }
	inline void set_ChatScrollbar_4(Scrollbar_t1494447233 * value)
	{
		___ChatScrollbar_4 = value;
		Il2CppCodeGenWriteBarrier((&___ChatScrollbar_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATCONTROLLER_T3486202795_H
#ifndef TEXTMESHPROFLOATINGTEXT_T845872552_H
#define TEXTMESHPROFLOATINGTEXT_T845872552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextMeshProFloatingText
struct  TextMeshProFloatingText_t845872552  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Font TMPro.Examples.TextMeshProFloatingText::TheFont
	Font_t1956802104 * ___TheFont_2;
	// UnityEngine.GameObject TMPro.Examples.TextMeshProFloatingText::m_floatingText
	GameObject_t1113636619 * ___m_floatingText_3;
	// TMPro.TextMeshPro TMPro.Examples.TextMeshProFloatingText::m_textMeshPro
	TextMeshPro_t2393593166 * ___m_textMeshPro_4;
	// UnityEngine.TextMesh TMPro.Examples.TextMeshProFloatingText::m_textMesh
	TextMesh_t1536577757 * ___m_textMesh_5;
	// UnityEngine.Transform TMPro.Examples.TextMeshProFloatingText::m_transform
	Transform_t3600365921 * ___m_transform_6;
	// UnityEngine.Transform TMPro.Examples.TextMeshProFloatingText::m_floatingText_Transform
	Transform_t3600365921 * ___m_floatingText_Transform_7;
	// UnityEngine.Transform TMPro.Examples.TextMeshProFloatingText::m_cameraTransform
	Transform_t3600365921 * ___m_cameraTransform_8;
	// UnityEngine.Vector3 TMPro.Examples.TextMeshProFloatingText::lastPOS
	Vector3_t3722313464  ___lastPOS_9;
	// UnityEngine.Quaternion TMPro.Examples.TextMeshProFloatingText::lastRotation
	Quaternion_t2301928331  ___lastRotation_10;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText::SpawnType
	int32_t ___SpawnType_11;

public:
	inline static int32_t get_offset_of_TheFont_2() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___TheFont_2)); }
	inline Font_t1956802104 * get_TheFont_2() const { return ___TheFont_2; }
	inline Font_t1956802104 ** get_address_of_TheFont_2() { return &___TheFont_2; }
	inline void set_TheFont_2(Font_t1956802104 * value)
	{
		___TheFont_2 = value;
		Il2CppCodeGenWriteBarrier((&___TheFont_2), value);
	}

	inline static int32_t get_offset_of_m_floatingText_3() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_floatingText_3)); }
	inline GameObject_t1113636619 * get_m_floatingText_3() const { return ___m_floatingText_3; }
	inline GameObject_t1113636619 ** get_address_of_m_floatingText_3() { return &___m_floatingText_3; }
	inline void set_m_floatingText_3(GameObject_t1113636619 * value)
	{
		___m_floatingText_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_floatingText_3), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_4() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_textMeshPro_4)); }
	inline TextMeshPro_t2393593166 * get_m_textMeshPro_4() const { return ___m_textMeshPro_4; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_textMeshPro_4() { return &___m_textMeshPro_4; }
	inline void set_m_textMeshPro_4(TextMeshPro_t2393593166 * value)
	{
		___m_textMeshPro_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_4), value);
	}

	inline static int32_t get_offset_of_m_textMesh_5() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_textMesh_5)); }
	inline TextMesh_t1536577757 * get_m_textMesh_5() const { return ___m_textMesh_5; }
	inline TextMesh_t1536577757 ** get_address_of_m_textMesh_5() { return &___m_textMesh_5; }
	inline void set_m_textMesh_5(TextMesh_t1536577757 * value)
	{
		___m_textMesh_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_5), value);
	}

	inline static int32_t get_offset_of_m_transform_6() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_transform_6)); }
	inline Transform_t3600365921 * get_m_transform_6() const { return ___m_transform_6; }
	inline Transform_t3600365921 ** get_address_of_m_transform_6() { return &___m_transform_6; }
	inline void set_m_transform_6(Transform_t3600365921 * value)
	{
		___m_transform_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_6), value);
	}

	inline static int32_t get_offset_of_m_floatingText_Transform_7() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_floatingText_Transform_7)); }
	inline Transform_t3600365921 * get_m_floatingText_Transform_7() const { return ___m_floatingText_Transform_7; }
	inline Transform_t3600365921 ** get_address_of_m_floatingText_Transform_7() { return &___m_floatingText_Transform_7; }
	inline void set_m_floatingText_Transform_7(Transform_t3600365921 * value)
	{
		___m_floatingText_Transform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_floatingText_Transform_7), value);
	}

	inline static int32_t get_offset_of_m_cameraTransform_8() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_cameraTransform_8)); }
	inline Transform_t3600365921 * get_m_cameraTransform_8() const { return ___m_cameraTransform_8; }
	inline Transform_t3600365921 ** get_address_of_m_cameraTransform_8() { return &___m_cameraTransform_8; }
	inline void set_m_cameraTransform_8(Transform_t3600365921 * value)
	{
		___m_cameraTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_cameraTransform_8), value);
	}

	inline static int32_t get_offset_of_lastPOS_9() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___lastPOS_9)); }
	inline Vector3_t3722313464  get_lastPOS_9() const { return ___lastPOS_9; }
	inline Vector3_t3722313464 * get_address_of_lastPOS_9() { return &___lastPOS_9; }
	inline void set_lastPOS_9(Vector3_t3722313464  value)
	{
		___lastPOS_9 = value;
	}

	inline static int32_t get_offset_of_lastRotation_10() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___lastRotation_10)); }
	inline Quaternion_t2301928331  get_lastRotation_10() const { return ___lastRotation_10; }
	inline Quaternion_t2301928331 * get_address_of_lastRotation_10() { return &___lastRotation_10; }
	inline void set_lastRotation_10(Quaternion_t2301928331  value)
	{
		___lastRotation_10 = value;
	}

	inline static int32_t get_offset_of_SpawnType_11() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___SpawnType_11)); }
	inline int32_t get_SpawnType_11() const { return ___SpawnType_11; }
	inline int32_t* get_address_of_SpawnType_11() { return &___SpawnType_11; }
	inline void set_SpawnType_11(int32_t value)
	{
		___SpawnType_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESHPROFLOATINGTEXT_T845872552_H
#ifndef TMP_TEXTEVENTHANDLER_T1869054637_H
#define TMP_TEXTEVENTHANDLER_T1869054637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler
struct  TMP_TextEventHandler_t1869054637  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::m_OnCharacterSelection
	CharacterSelectionEvent_t3109943174 * ___m_OnCharacterSelection_2;
	// TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::m_OnWordSelection
	WordSelectionEvent_t1841909953 * ___m_OnWordSelection_3;
	// TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::m_OnLineSelection
	LineSelectionEvent_t2868010532 * ___m_OnLineSelection_4;
	// TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::m_OnLinkSelection
	LinkSelectionEvent_t1590929858 * ___m_OnLinkSelection_5;
	// TMPro.TMP_Text TMPro.TMP_TextEventHandler::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_6;
	// UnityEngine.Camera TMPro.TMP_TextEventHandler::m_Camera
	Camera_t4157153871 * ___m_Camera_7;
	// UnityEngine.Canvas TMPro.TMP_TextEventHandler::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_8;
	// System.Int32 TMPro.TMP_TextEventHandler::m_selectedLink
	int32_t ___m_selectedLink_9;
	// System.Int32 TMPro.TMP_TextEventHandler::m_lastCharIndex
	int32_t ___m_lastCharIndex_10;
	// System.Int32 TMPro.TMP_TextEventHandler::m_lastWordIndex
	int32_t ___m_lastWordIndex_11;
	// System.Int32 TMPro.TMP_TextEventHandler::m_lastLineIndex
	int32_t ___m_lastLineIndex_12;

public:
	inline static int32_t get_offset_of_m_OnCharacterSelection_2() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_OnCharacterSelection_2)); }
	inline CharacterSelectionEvent_t3109943174 * get_m_OnCharacterSelection_2() const { return ___m_OnCharacterSelection_2; }
	inline CharacterSelectionEvent_t3109943174 ** get_address_of_m_OnCharacterSelection_2() { return &___m_OnCharacterSelection_2; }
	inline void set_m_OnCharacterSelection_2(CharacterSelectionEvent_t3109943174 * value)
	{
		___m_OnCharacterSelection_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCharacterSelection_2), value);
	}

	inline static int32_t get_offset_of_m_OnWordSelection_3() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_OnWordSelection_3)); }
	inline WordSelectionEvent_t1841909953 * get_m_OnWordSelection_3() const { return ___m_OnWordSelection_3; }
	inline WordSelectionEvent_t1841909953 ** get_address_of_m_OnWordSelection_3() { return &___m_OnWordSelection_3; }
	inline void set_m_OnWordSelection_3(WordSelectionEvent_t1841909953 * value)
	{
		___m_OnWordSelection_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnWordSelection_3), value);
	}

	inline static int32_t get_offset_of_m_OnLineSelection_4() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_OnLineSelection_4)); }
	inline LineSelectionEvent_t2868010532 * get_m_OnLineSelection_4() const { return ___m_OnLineSelection_4; }
	inline LineSelectionEvent_t2868010532 ** get_address_of_m_OnLineSelection_4() { return &___m_OnLineSelection_4; }
	inline void set_m_OnLineSelection_4(LineSelectionEvent_t2868010532 * value)
	{
		___m_OnLineSelection_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnLineSelection_4), value);
	}

	inline static int32_t get_offset_of_m_OnLinkSelection_5() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_OnLinkSelection_5)); }
	inline LinkSelectionEvent_t1590929858 * get_m_OnLinkSelection_5() const { return ___m_OnLinkSelection_5; }
	inline LinkSelectionEvent_t1590929858 ** get_address_of_m_OnLinkSelection_5() { return &___m_OnLinkSelection_5; }
	inline void set_m_OnLinkSelection_5(LinkSelectionEvent_t1590929858 * value)
	{
		___m_OnLinkSelection_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnLinkSelection_5), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_6() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_TextComponent_6)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_6() const { return ___m_TextComponent_6; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_6() { return &___m_TextComponent_6; }
	inline void set_m_TextComponent_6(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_6), value);
	}

	inline static int32_t get_offset_of_m_Camera_7() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_Camera_7)); }
	inline Camera_t4157153871 * get_m_Camera_7() const { return ___m_Camera_7; }
	inline Camera_t4157153871 ** get_address_of_m_Camera_7() { return &___m_Camera_7; }
	inline void set_m_Camera_7(Camera_t4157153871 * value)
	{
		___m_Camera_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_7), value);
	}

	inline static int32_t get_offset_of_m_Canvas_8() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_Canvas_8)); }
	inline Canvas_t3310196443 * get_m_Canvas_8() const { return ___m_Canvas_8; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_8() { return &___m_Canvas_8; }
	inline void set_m_Canvas_8(Canvas_t3310196443 * value)
	{
		___m_Canvas_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_8), value);
	}

	inline static int32_t get_offset_of_m_selectedLink_9() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_selectedLink_9)); }
	inline int32_t get_m_selectedLink_9() const { return ___m_selectedLink_9; }
	inline int32_t* get_address_of_m_selectedLink_9() { return &___m_selectedLink_9; }
	inline void set_m_selectedLink_9(int32_t value)
	{
		___m_selectedLink_9 = value;
	}

	inline static int32_t get_offset_of_m_lastCharIndex_10() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_lastCharIndex_10)); }
	inline int32_t get_m_lastCharIndex_10() const { return ___m_lastCharIndex_10; }
	inline int32_t* get_address_of_m_lastCharIndex_10() { return &___m_lastCharIndex_10; }
	inline void set_m_lastCharIndex_10(int32_t value)
	{
		___m_lastCharIndex_10 = value;
	}

	inline static int32_t get_offset_of_m_lastWordIndex_11() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_lastWordIndex_11)); }
	inline int32_t get_m_lastWordIndex_11() const { return ___m_lastWordIndex_11; }
	inline int32_t* get_address_of_m_lastWordIndex_11() { return &___m_lastWordIndex_11; }
	inline void set_m_lastWordIndex_11(int32_t value)
	{
		___m_lastWordIndex_11 = value;
	}

	inline static int32_t get_offset_of_m_lastLineIndex_12() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_lastLineIndex_12)); }
	inline int32_t get_m_lastLineIndex_12() const { return ___m_lastLineIndex_12; }
	inline int32_t* get_address_of_m_lastLineIndex_12() { return &___m_lastLineIndex_12; }
	inline void set_m_lastLineIndex_12(int32_t value)
	{
		___m_lastLineIndex_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTEVENTHANDLER_T1869054637_H
#ifndef TMP_TEXTEVENTCHECK_T1103849140_H
#define TMP_TEXTEVENTCHECK_T1103849140_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_TextEventCheck
struct  TMP_TextEventCheck_t1103849140  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_TextEventHandler TMPro.Examples.TMP_TextEventCheck::TextEventHandler
	TMP_TextEventHandler_t1869054637 * ___TextEventHandler_2;

public:
	inline static int32_t get_offset_of_TextEventHandler_2() { return static_cast<int32_t>(offsetof(TMP_TextEventCheck_t1103849140, ___TextEventHandler_2)); }
	inline TMP_TextEventHandler_t1869054637 * get_TextEventHandler_2() const { return ___TextEventHandler_2; }
	inline TMP_TextEventHandler_t1869054637 ** get_address_of_TextEventHandler_2() { return &___TextEventHandler_2; }
	inline void set_TextEventHandler_2(TMP_TextEventHandler_t1869054637 * value)
	{
		___TextEventHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___TextEventHandler_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTEVENTCHECK_T1103849140_H
#ifndef MOVEMENTSCRIPT_T3129111103_H
#define MOVEMENTSCRIPT_T3129111103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MovementScript
struct  MovementScript_t3129111103  : public MonoBehaviour_t3962482529
{
public:
	// MovementScript/playerState MovementScript::state
	int32_t ___state_2;
	// UnityEngine.GameObject MovementScript::swimmer
	GameObject_t1113636619 * ___swimmer_3;
	// System.Boolean MovementScript::sharkCaptured
	bool ___sharkCaptured_4;
	// System.Single MovementScript::totalDive
	float ___totalDive_5;

public:
	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(MovementScript_t3129111103, ___state_2)); }
	inline int32_t get_state_2() const { return ___state_2; }
	inline int32_t* get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(int32_t value)
	{
		___state_2 = value;
	}

	inline static int32_t get_offset_of_swimmer_3() { return static_cast<int32_t>(offsetof(MovementScript_t3129111103, ___swimmer_3)); }
	inline GameObject_t1113636619 * get_swimmer_3() const { return ___swimmer_3; }
	inline GameObject_t1113636619 ** get_address_of_swimmer_3() { return &___swimmer_3; }
	inline void set_swimmer_3(GameObject_t1113636619 * value)
	{
		___swimmer_3 = value;
		Il2CppCodeGenWriteBarrier((&___swimmer_3), value);
	}

	inline static int32_t get_offset_of_sharkCaptured_4() { return static_cast<int32_t>(offsetof(MovementScript_t3129111103, ___sharkCaptured_4)); }
	inline bool get_sharkCaptured_4() const { return ___sharkCaptured_4; }
	inline bool* get_address_of_sharkCaptured_4() { return &___sharkCaptured_4; }
	inline void set_sharkCaptured_4(bool value)
	{
		___sharkCaptured_4 = value;
	}

	inline static int32_t get_offset_of_totalDive_5() { return static_cast<int32_t>(offsetof(MovementScript_t3129111103, ___totalDive_5)); }
	inline float get_totalDive_5() const { return ___totalDive_5; }
	inline float* get_address_of_totalDive_5() { return &___totalDive_5; }
	inline void set_totalDive_5(float value)
	{
		___totalDive_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENTSCRIPT_T3129111103_H
#ifndef TMP_TEXTINFODEBUGTOOL_T1868681444_H
#define TMP_TEXTINFODEBUGTOOL_T1868681444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_TextInfoDebugTool
struct  TMP_TextInfoDebugTool_t1868681444  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowCharacters
	bool ___ShowCharacters_2;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowWords
	bool ___ShowWords_3;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowLinks
	bool ___ShowLinks_4;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowLines
	bool ___ShowLines_5;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowMeshBounds
	bool ___ShowMeshBounds_6;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowTextBounds
	bool ___ShowTextBounds_7;
	// System.String TMPro.Examples.TMP_TextInfoDebugTool::ObjectStats
	String_t* ___ObjectStats_8;
	// TMPro.TMP_Text TMPro.Examples.TMP_TextInfoDebugTool::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_9;
	// UnityEngine.Transform TMPro.Examples.TMP_TextInfoDebugTool::m_Transform
	Transform_t3600365921 * ___m_Transform_10;

public:
	inline static int32_t get_offset_of_ShowCharacters_2() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ShowCharacters_2)); }
	inline bool get_ShowCharacters_2() const { return ___ShowCharacters_2; }
	inline bool* get_address_of_ShowCharacters_2() { return &___ShowCharacters_2; }
	inline void set_ShowCharacters_2(bool value)
	{
		___ShowCharacters_2 = value;
	}

	inline static int32_t get_offset_of_ShowWords_3() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ShowWords_3)); }
	inline bool get_ShowWords_3() const { return ___ShowWords_3; }
	inline bool* get_address_of_ShowWords_3() { return &___ShowWords_3; }
	inline void set_ShowWords_3(bool value)
	{
		___ShowWords_3 = value;
	}

	inline static int32_t get_offset_of_ShowLinks_4() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ShowLinks_4)); }
	inline bool get_ShowLinks_4() const { return ___ShowLinks_4; }
	inline bool* get_address_of_ShowLinks_4() { return &___ShowLinks_4; }
	inline void set_ShowLinks_4(bool value)
	{
		___ShowLinks_4 = value;
	}

	inline static int32_t get_offset_of_ShowLines_5() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ShowLines_5)); }
	inline bool get_ShowLines_5() const { return ___ShowLines_5; }
	inline bool* get_address_of_ShowLines_5() { return &___ShowLines_5; }
	inline void set_ShowLines_5(bool value)
	{
		___ShowLines_5 = value;
	}

	inline static int32_t get_offset_of_ShowMeshBounds_6() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ShowMeshBounds_6)); }
	inline bool get_ShowMeshBounds_6() const { return ___ShowMeshBounds_6; }
	inline bool* get_address_of_ShowMeshBounds_6() { return &___ShowMeshBounds_6; }
	inline void set_ShowMeshBounds_6(bool value)
	{
		___ShowMeshBounds_6 = value;
	}

	inline static int32_t get_offset_of_ShowTextBounds_7() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ShowTextBounds_7)); }
	inline bool get_ShowTextBounds_7() const { return ___ShowTextBounds_7; }
	inline bool* get_address_of_ShowTextBounds_7() { return &___ShowTextBounds_7; }
	inline void set_ShowTextBounds_7(bool value)
	{
		___ShowTextBounds_7 = value;
	}

	inline static int32_t get_offset_of_ObjectStats_8() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ObjectStats_8)); }
	inline String_t* get_ObjectStats_8() const { return ___ObjectStats_8; }
	inline String_t** get_address_of_ObjectStats_8() { return &___ObjectStats_8; }
	inline void set_ObjectStats_8(String_t* value)
	{
		___ObjectStats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectStats_8), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_9() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___m_TextComponent_9)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_9() const { return ___m_TextComponent_9; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_9() { return &___m_TextComponent_9; }
	inline void set_m_TextComponent_9(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_9), value);
	}

	inline static int32_t get_offset_of_m_Transform_10() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___m_Transform_10)); }
	inline Transform_t3600365921 * get_m_Transform_10() const { return ___m_Transform_10; }
	inline Transform_t3600365921 ** get_address_of_m_Transform_10() { return &___m_Transform_10; }
	inline void set_m_Transform_10(Transform_t3600365921 * value)
	{
		___m_Transform_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Transform_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTINFODEBUGTOOL_T1868681444_H
#ifndef MENUSCRIPT_T2744967707_H
#define MENUSCRIPT_T2744967707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuScript
struct  MenuScript_t2744967707  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject MenuScript::StartPanel
	GameObject_t1113636619 * ___StartPanel_2;
	// UnityEngine.GameObject MenuScript::StartPanelSelection
	GameObject_t1113636619 * ___StartPanelSelection_3;
	// UnityEngine.GameObject MenuScript::ExposurePanel
	GameObject_t1113636619 * ___ExposurePanel_4;
	// UnityEngine.GameObject MenuScript::ExposureSelection
	GameObject_t1113636619 * ___ExposureSelection_5;
	// UnityEngine.GameObject MenuScript::TimePanel
	GameObject_t1113636619 * ___TimePanel_6;
	// UnityEngine.GameObject MenuScript::TimePanelSelection
	GameObject_t1113636619 * ___TimePanelSelection_7;
	// UnityEngine.GameObject MenuScript::InstructionsPanel
	GameObject_t1113636619 * ___InstructionsPanel_8;
	// UnityEngine.GameObject MenuScript::SettingsPanel
	GameObject_t1113636619 * ___SettingsPanel_9;
	// UnityEngine.GameObject MenuScript::EndPanel
	GameObject_t1113636619 * ___EndPanel_10;
	// UnityEngine.GameObject MenuScript::ResetPanel
	GameObject_t1113636619 * ___ResetPanel_11;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> MenuScript::panels
	Dictionary_2_t898892918 * ___panels_12;
	// System.Collections.Generic.List`1<TweenAnimation.Models.TweenAnimation> MenuScript::animations
	List_1_t3050851472 * ___animations_13;

public:
	inline static int32_t get_offset_of_StartPanel_2() { return static_cast<int32_t>(offsetof(MenuScript_t2744967707, ___StartPanel_2)); }
	inline GameObject_t1113636619 * get_StartPanel_2() const { return ___StartPanel_2; }
	inline GameObject_t1113636619 ** get_address_of_StartPanel_2() { return &___StartPanel_2; }
	inline void set_StartPanel_2(GameObject_t1113636619 * value)
	{
		___StartPanel_2 = value;
		Il2CppCodeGenWriteBarrier((&___StartPanel_2), value);
	}

	inline static int32_t get_offset_of_StartPanelSelection_3() { return static_cast<int32_t>(offsetof(MenuScript_t2744967707, ___StartPanelSelection_3)); }
	inline GameObject_t1113636619 * get_StartPanelSelection_3() const { return ___StartPanelSelection_3; }
	inline GameObject_t1113636619 ** get_address_of_StartPanelSelection_3() { return &___StartPanelSelection_3; }
	inline void set_StartPanelSelection_3(GameObject_t1113636619 * value)
	{
		___StartPanelSelection_3 = value;
		Il2CppCodeGenWriteBarrier((&___StartPanelSelection_3), value);
	}

	inline static int32_t get_offset_of_ExposurePanel_4() { return static_cast<int32_t>(offsetof(MenuScript_t2744967707, ___ExposurePanel_4)); }
	inline GameObject_t1113636619 * get_ExposurePanel_4() const { return ___ExposurePanel_4; }
	inline GameObject_t1113636619 ** get_address_of_ExposurePanel_4() { return &___ExposurePanel_4; }
	inline void set_ExposurePanel_4(GameObject_t1113636619 * value)
	{
		___ExposurePanel_4 = value;
		Il2CppCodeGenWriteBarrier((&___ExposurePanel_4), value);
	}

	inline static int32_t get_offset_of_ExposureSelection_5() { return static_cast<int32_t>(offsetof(MenuScript_t2744967707, ___ExposureSelection_5)); }
	inline GameObject_t1113636619 * get_ExposureSelection_5() const { return ___ExposureSelection_5; }
	inline GameObject_t1113636619 ** get_address_of_ExposureSelection_5() { return &___ExposureSelection_5; }
	inline void set_ExposureSelection_5(GameObject_t1113636619 * value)
	{
		___ExposureSelection_5 = value;
		Il2CppCodeGenWriteBarrier((&___ExposureSelection_5), value);
	}

	inline static int32_t get_offset_of_TimePanel_6() { return static_cast<int32_t>(offsetof(MenuScript_t2744967707, ___TimePanel_6)); }
	inline GameObject_t1113636619 * get_TimePanel_6() const { return ___TimePanel_6; }
	inline GameObject_t1113636619 ** get_address_of_TimePanel_6() { return &___TimePanel_6; }
	inline void set_TimePanel_6(GameObject_t1113636619 * value)
	{
		___TimePanel_6 = value;
		Il2CppCodeGenWriteBarrier((&___TimePanel_6), value);
	}

	inline static int32_t get_offset_of_TimePanelSelection_7() { return static_cast<int32_t>(offsetof(MenuScript_t2744967707, ___TimePanelSelection_7)); }
	inline GameObject_t1113636619 * get_TimePanelSelection_7() const { return ___TimePanelSelection_7; }
	inline GameObject_t1113636619 ** get_address_of_TimePanelSelection_7() { return &___TimePanelSelection_7; }
	inline void set_TimePanelSelection_7(GameObject_t1113636619 * value)
	{
		___TimePanelSelection_7 = value;
		Il2CppCodeGenWriteBarrier((&___TimePanelSelection_7), value);
	}

	inline static int32_t get_offset_of_InstructionsPanel_8() { return static_cast<int32_t>(offsetof(MenuScript_t2744967707, ___InstructionsPanel_8)); }
	inline GameObject_t1113636619 * get_InstructionsPanel_8() const { return ___InstructionsPanel_8; }
	inline GameObject_t1113636619 ** get_address_of_InstructionsPanel_8() { return &___InstructionsPanel_8; }
	inline void set_InstructionsPanel_8(GameObject_t1113636619 * value)
	{
		___InstructionsPanel_8 = value;
		Il2CppCodeGenWriteBarrier((&___InstructionsPanel_8), value);
	}

	inline static int32_t get_offset_of_SettingsPanel_9() { return static_cast<int32_t>(offsetof(MenuScript_t2744967707, ___SettingsPanel_9)); }
	inline GameObject_t1113636619 * get_SettingsPanel_9() const { return ___SettingsPanel_9; }
	inline GameObject_t1113636619 ** get_address_of_SettingsPanel_9() { return &___SettingsPanel_9; }
	inline void set_SettingsPanel_9(GameObject_t1113636619 * value)
	{
		___SettingsPanel_9 = value;
		Il2CppCodeGenWriteBarrier((&___SettingsPanel_9), value);
	}

	inline static int32_t get_offset_of_EndPanel_10() { return static_cast<int32_t>(offsetof(MenuScript_t2744967707, ___EndPanel_10)); }
	inline GameObject_t1113636619 * get_EndPanel_10() const { return ___EndPanel_10; }
	inline GameObject_t1113636619 ** get_address_of_EndPanel_10() { return &___EndPanel_10; }
	inline void set_EndPanel_10(GameObject_t1113636619 * value)
	{
		___EndPanel_10 = value;
		Il2CppCodeGenWriteBarrier((&___EndPanel_10), value);
	}

	inline static int32_t get_offset_of_ResetPanel_11() { return static_cast<int32_t>(offsetof(MenuScript_t2744967707, ___ResetPanel_11)); }
	inline GameObject_t1113636619 * get_ResetPanel_11() const { return ___ResetPanel_11; }
	inline GameObject_t1113636619 ** get_address_of_ResetPanel_11() { return &___ResetPanel_11; }
	inline void set_ResetPanel_11(GameObject_t1113636619 * value)
	{
		___ResetPanel_11 = value;
		Il2CppCodeGenWriteBarrier((&___ResetPanel_11), value);
	}

	inline static int32_t get_offset_of_panels_12() { return static_cast<int32_t>(offsetof(MenuScript_t2744967707, ___panels_12)); }
	inline Dictionary_2_t898892918 * get_panels_12() const { return ___panels_12; }
	inline Dictionary_2_t898892918 ** get_address_of_panels_12() { return &___panels_12; }
	inline void set_panels_12(Dictionary_2_t898892918 * value)
	{
		___panels_12 = value;
		Il2CppCodeGenWriteBarrier((&___panels_12), value);
	}

	inline static int32_t get_offset_of_animations_13() { return static_cast<int32_t>(offsetof(MenuScript_t2744967707, ___animations_13)); }
	inline List_1_t3050851472 * get_animations_13() const { return ___animations_13; }
	inline List_1_t3050851472 ** get_address_of_animations_13() { return &___animations_13; }
	inline void set_animations_13(List_1_t3050851472 * value)
	{
		___animations_13 = value;
		Il2CppCodeGenWriteBarrier((&___animations_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUSCRIPT_T2744967707_H
#ifndef TMP_FRAMERATECOUNTER_T314972976_H
#define TMP_FRAMERATECOUNTER_T314972976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_FrameRateCounter
struct  TMP_FrameRateCounter_t314972976  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TMPro.Examples.TMP_FrameRateCounter::UpdateInterval
	float ___UpdateInterval_2;
	// System.Single TMPro.Examples.TMP_FrameRateCounter::m_LastInterval
	float ___m_LastInterval_3;
	// System.Int32 TMPro.Examples.TMP_FrameRateCounter::m_Frames
	int32_t ___m_Frames_4;
	// TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_FrameRateCounter::AnchorPosition
	int32_t ___AnchorPosition_5;
	// System.String TMPro.Examples.TMP_FrameRateCounter::htmlColorTag
	String_t* ___htmlColorTag_6;
	// TMPro.TextMeshPro TMPro.Examples.TMP_FrameRateCounter::m_TextMeshPro
	TextMeshPro_t2393593166 * ___m_TextMeshPro_8;
	// UnityEngine.Transform TMPro.Examples.TMP_FrameRateCounter::m_frameCounter_transform
	Transform_t3600365921 * ___m_frameCounter_transform_9;
	// UnityEngine.Camera TMPro.Examples.TMP_FrameRateCounter::m_camera
	Camera_t4157153871 * ___m_camera_10;
	// TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_FrameRateCounter::last_AnchorPosition
	int32_t ___last_AnchorPosition_11;

public:
	inline static int32_t get_offset_of_UpdateInterval_2() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___UpdateInterval_2)); }
	inline float get_UpdateInterval_2() const { return ___UpdateInterval_2; }
	inline float* get_address_of_UpdateInterval_2() { return &___UpdateInterval_2; }
	inline void set_UpdateInterval_2(float value)
	{
		___UpdateInterval_2 = value;
	}

	inline static int32_t get_offset_of_m_LastInterval_3() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___m_LastInterval_3)); }
	inline float get_m_LastInterval_3() const { return ___m_LastInterval_3; }
	inline float* get_address_of_m_LastInterval_3() { return &___m_LastInterval_3; }
	inline void set_m_LastInterval_3(float value)
	{
		___m_LastInterval_3 = value;
	}

	inline static int32_t get_offset_of_m_Frames_4() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___m_Frames_4)); }
	inline int32_t get_m_Frames_4() const { return ___m_Frames_4; }
	inline int32_t* get_address_of_m_Frames_4() { return &___m_Frames_4; }
	inline void set_m_Frames_4(int32_t value)
	{
		___m_Frames_4 = value;
	}

	inline static int32_t get_offset_of_AnchorPosition_5() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___AnchorPosition_5)); }
	inline int32_t get_AnchorPosition_5() const { return ___AnchorPosition_5; }
	inline int32_t* get_address_of_AnchorPosition_5() { return &___AnchorPosition_5; }
	inline void set_AnchorPosition_5(int32_t value)
	{
		___AnchorPosition_5 = value;
	}

	inline static int32_t get_offset_of_htmlColorTag_6() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___htmlColorTag_6)); }
	inline String_t* get_htmlColorTag_6() const { return ___htmlColorTag_6; }
	inline String_t** get_address_of_htmlColorTag_6() { return &___htmlColorTag_6; }
	inline void set_htmlColorTag_6(String_t* value)
	{
		___htmlColorTag_6 = value;
		Il2CppCodeGenWriteBarrier((&___htmlColorTag_6), value);
	}

	inline static int32_t get_offset_of_m_TextMeshPro_8() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___m_TextMeshPro_8)); }
	inline TextMeshPro_t2393593166 * get_m_TextMeshPro_8() const { return ___m_TextMeshPro_8; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_TextMeshPro_8() { return &___m_TextMeshPro_8; }
	inline void set_m_TextMeshPro_8(TextMeshPro_t2393593166 * value)
	{
		___m_TextMeshPro_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_8), value);
	}

	inline static int32_t get_offset_of_m_frameCounter_transform_9() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___m_frameCounter_transform_9)); }
	inline Transform_t3600365921 * get_m_frameCounter_transform_9() const { return ___m_frameCounter_transform_9; }
	inline Transform_t3600365921 ** get_address_of_m_frameCounter_transform_9() { return &___m_frameCounter_transform_9; }
	inline void set_m_frameCounter_transform_9(Transform_t3600365921 * value)
	{
		___m_frameCounter_transform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_frameCounter_transform_9), value);
	}

	inline static int32_t get_offset_of_m_camera_10() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___m_camera_10)); }
	inline Camera_t4157153871 * get_m_camera_10() const { return ___m_camera_10; }
	inline Camera_t4157153871 ** get_address_of_m_camera_10() { return &___m_camera_10; }
	inline void set_m_camera_10(Camera_t4157153871 * value)
	{
		___m_camera_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_camera_10), value);
	}

	inline static int32_t get_offset_of_last_AnchorPosition_11() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___last_AnchorPosition_11)); }
	inline int32_t get_last_AnchorPosition_11() const { return ___last_AnchorPosition_11; }
	inline int32_t* get_address_of_last_AnchorPosition_11() { return &___last_AnchorPosition_11; }
	inline void set_last_AnchorPosition_11(int32_t value)
	{
		___last_AnchorPosition_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_FRAMERATECOUNTER_T314972976_H
#ifndef BENCHMARK01_T1571072624_H
#define BENCHMARK01_T1571072624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01
struct  Benchmark01_t1571072624  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark01::BenchmarkType
	int32_t ___BenchmarkType_2;
	// TMPro.TMP_FontAsset TMPro.Examples.Benchmark01::TMProFont
	TMP_FontAsset_t364381626 * ___TMProFont_3;
	// UnityEngine.Font TMPro.Examples.Benchmark01::TextMeshFont
	Font_t1956802104 * ___TextMeshFont_4;
	// TMPro.TextMeshPro TMPro.Examples.Benchmark01::m_textMeshPro
	TextMeshPro_t2393593166 * ___m_textMeshPro_5;
	// TMPro.TextContainer TMPro.Examples.Benchmark01::m_textContainer
	TextContainer_t97923372 * ___m_textContainer_6;
	// UnityEngine.TextMesh TMPro.Examples.Benchmark01::m_textMesh
	TextMesh_t1536577757 * ___m_textMesh_7;
	// UnityEngine.Material TMPro.Examples.Benchmark01::m_material01
	Material_t340375123 * ___m_material01_10;
	// UnityEngine.Material TMPro.Examples.Benchmark01::m_material02
	Material_t340375123 * ___m_material02_11;

public:
	inline static int32_t get_offset_of_BenchmarkType_2() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___BenchmarkType_2)); }
	inline int32_t get_BenchmarkType_2() const { return ___BenchmarkType_2; }
	inline int32_t* get_address_of_BenchmarkType_2() { return &___BenchmarkType_2; }
	inline void set_BenchmarkType_2(int32_t value)
	{
		___BenchmarkType_2 = value;
	}

	inline static int32_t get_offset_of_TMProFont_3() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___TMProFont_3)); }
	inline TMP_FontAsset_t364381626 * get_TMProFont_3() const { return ___TMProFont_3; }
	inline TMP_FontAsset_t364381626 ** get_address_of_TMProFont_3() { return &___TMProFont_3; }
	inline void set_TMProFont_3(TMP_FontAsset_t364381626 * value)
	{
		___TMProFont_3 = value;
		Il2CppCodeGenWriteBarrier((&___TMProFont_3), value);
	}

	inline static int32_t get_offset_of_TextMeshFont_4() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___TextMeshFont_4)); }
	inline Font_t1956802104 * get_TextMeshFont_4() const { return ___TextMeshFont_4; }
	inline Font_t1956802104 ** get_address_of_TextMeshFont_4() { return &___TextMeshFont_4; }
	inline void set_TextMeshFont_4(Font_t1956802104 * value)
	{
		___TextMeshFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___TextMeshFont_4), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_5() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_textMeshPro_5)); }
	inline TextMeshPro_t2393593166 * get_m_textMeshPro_5() const { return ___m_textMeshPro_5; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_textMeshPro_5() { return &___m_textMeshPro_5; }
	inline void set_m_textMeshPro_5(TextMeshPro_t2393593166 * value)
	{
		___m_textMeshPro_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_5), value);
	}

	inline static int32_t get_offset_of_m_textContainer_6() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_textContainer_6)); }
	inline TextContainer_t97923372 * get_m_textContainer_6() const { return ___m_textContainer_6; }
	inline TextContainer_t97923372 ** get_address_of_m_textContainer_6() { return &___m_textContainer_6; }
	inline void set_m_textContainer_6(TextContainer_t97923372 * value)
	{
		___m_textContainer_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainer_6), value);
	}

	inline static int32_t get_offset_of_m_textMesh_7() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_textMesh_7)); }
	inline TextMesh_t1536577757 * get_m_textMesh_7() const { return ___m_textMesh_7; }
	inline TextMesh_t1536577757 ** get_address_of_m_textMesh_7() { return &___m_textMesh_7; }
	inline void set_m_textMesh_7(TextMesh_t1536577757 * value)
	{
		___m_textMesh_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_7), value);
	}

	inline static int32_t get_offset_of_m_material01_10() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_material01_10)); }
	inline Material_t340375123 * get_m_material01_10() const { return ___m_material01_10; }
	inline Material_t340375123 ** get_address_of_m_material01_10() { return &___m_material01_10; }
	inline void set_m_material01_10(Material_t340375123 * value)
	{
		___m_material01_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_material01_10), value);
	}

	inline static int32_t get_offset_of_m_material02_11() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_material02_11)); }
	inline Material_t340375123 * get_m_material02_11() const { return ___m_material02_11; }
	inline Material_t340375123 ** get_address_of_m_material02_11() { return &___m_material02_11; }
	inline void set_m_material02_11(Material_t340375123 * value)
	{
		___m_material02_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_material02_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK01_T1571072624_H
#ifndef BENCHMARK01_UGUI_T3264177817_H
#define BENCHMARK01_UGUI_T3264177817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01_UGUI
struct  Benchmark01_UGUI_t3264177817  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark01_UGUI::BenchmarkType
	int32_t ___BenchmarkType_2;
	// UnityEngine.Canvas TMPro.Examples.Benchmark01_UGUI::canvas
	Canvas_t3310196443 * ___canvas_3;
	// TMPro.TMP_FontAsset TMPro.Examples.Benchmark01_UGUI::TMProFont
	TMP_FontAsset_t364381626 * ___TMProFont_4;
	// UnityEngine.Font TMPro.Examples.Benchmark01_UGUI::TextMeshFont
	Font_t1956802104 * ___TextMeshFont_5;
	// TMPro.TextMeshProUGUI TMPro.Examples.Benchmark01_UGUI::m_textMeshPro
	TextMeshProUGUI_t529313277 * ___m_textMeshPro_6;
	// UnityEngine.UI.Text TMPro.Examples.Benchmark01_UGUI::m_textMesh
	Text_t1901882714 * ___m_textMesh_7;
	// UnityEngine.Material TMPro.Examples.Benchmark01_UGUI::m_material01
	Material_t340375123 * ___m_material01_10;
	// UnityEngine.Material TMPro.Examples.Benchmark01_UGUI::m_material02
	Material_t340375123 * ___m_material02_11;

public:
	inline static int32_t get_offset_of_BenchmarkType_2() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___BenchmarkType_2)); }
	inline int32_t get_BenchmarkType_2() const { return ___BenchmarkType_2; }
	inline int32_t* get_address_of_BenchmarkType_2() { return &___BenchmarkType_2; }
	inline void set_BenchmarkType_2(int32_t value)
	{
		___BenchmarkType_2 = value;
	}

	inline static int32_t get_offset_of_canvas_3() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___canvas_3)); }
	inline Canvas_t3310196443 * get_canvas_3() const { return ___canvas_3; }
	inline Canvas_t3310196443 ** get_address_of_canvas_3() { return &___canvas_3; }
	inline void set_canvas_3(Canvas_t3310196443 * value)
	{
		___canvas_3 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_3), value);
	}

	inline static int32_t get_offset_of_TMProFont_4() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___TMProFont_4)); }
	inline TMP_FontAsset_t364381626 * get_TMProFont_4() const { return ___TMProFont_4; }
	inline TMP_FontAsset_t364381626 ** get_address_of_TMProFont_4() { return &___TMProFont_4; }
	inline void set_TMProFont_4(TMP_FontAsset_t364381626 * value)
	{
		___TMProFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___TMProFont_4), value);
	}

	inline static int32_t get_offset_of_TextMeshFont_5() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___TextMeshFont_5)); }
	inline Font_t1956802104 * get_TextMeshFont_5() const { return ___TextMeshFont_5; }
	inline Font_t1956802104 ** get_address_of_TextMeshFont_5() { return &___TextMeshFont_5; }
	inline void set_TextMeshFont_5(Font_t1956802104 * value)
	{
		___TextMeshFont_5 = value;
		Il2CppCodeGenWriteBarrier((&___TextMeshFont_5), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_6() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_textMeshPro_6)); }
	inline TextMeshProUGUI_t529313277 * get_m_textMeshPro_6() const { return ___m_textMeshPro_6; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_m_textMeshPro_6() { return &___m_textMeshPro_6; }
	inline void set_m_textMeshPro_6(TextMeshProUGUI_t529313277 * value)
	{
		___m_textMeshPro_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_6), value);
	}

	inline static int32_t get_offset_of_m_textMesh_7() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_textMesh_7)); }
	inline Text_t1901882714 * get_m_textMesh_7() const { return ___m_textMesh_7; }
	inline Text_t1901882714 ** get_address_of_m_textMesh_7() { return &___m_textMesh_7; }
	inline void set_m_textMesh_7(Text_t1901882714 * value)
	{
		___m_textMesh_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_7), value);
	}

	inline static int32_t get_offset_of_m_material01_10() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_material01_10)); }
	inline Material_t340375123 * get_m_material01_10() const { return ___m_material01_10; }
	inline Material_t340375123 ** get_address_of_m_material01_10() { return &___m_material01_10; }
	inline void set_m_material01_10(Material_t340375123 * value)
	{
		___m_material01_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_material01_10), value);
	}

	inline static int32_t get_offset_of_m_material02_11() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_material02_11)); }
	inline Material_t340375123 * get_m_material02_11() const { return ___m_material02_11; }
	inline Material_t340375123 ** get_address_of_m_material02_11() { return &___m_material02_11; }
	inline void set_m_material02_11(Material_t340375123 * value)
	{
		___m_material02_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_material02_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK01_UGUI_T3264177817_H
#ifndef TEXTMESHSPAWNER_T177691618_H
#define TEXTMESHSPAWNER_T177691618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextMeshSpawner
struct  TextMeshSpawner_t177691618  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.TextMeshSpawner::SpawnType
	int32_t ___SpawnType_2;
	// System.Int32 TMPro.Examples.TextMeshSpawner::NumberOfNPC
	int32_t ___NumberOfNPC_3;
	// UnityEngine.Font TMPro.Examples.TextMeshSpawner::TheFont
	Font_t1956802104 * ___TheFont_4;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.TextMeshSpawner::floatingText_Script
	TextMeshProFloatingText_t845872552 * ___floatingText_Script_5;

public:
	inline static int32_t get_offset_of_SpawnType_2() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t177691618, ___SpawnType_2)); }
	inline int32_t get_SpawnType_2() const { return ___SpawnType_2; }
	inline int32_t* get_address_of_SpawnType_2() { return &___SpawnType_2; }
	inline void set_SpawnType_2(int32_t value)
	{
		___SpawnType_2 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_3() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t177691618, ___NumberOfNPC_3)); }
	inline int32_t get_NumberOfNPC_3() const { return ___NumberOfNPC_3; }
	inline int32_t* get_address_of_NumberOfNPC_3() { return &___NumberOfNPC_3; }
	inline void set_NumberOfNPC_3(int32_t value)
	{
		___NumberOfNPC_3 = value;
	}

	inline static int32_t get_offset_of_TheFont_4() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t177691618, ___TheFont_4)); }
	inline Font_t1956802104 * get_TheFont_4() const { return ___TheFont_4; }
	inline Font_t1956802104 ** get_address_of_TheFont_4() { return &___TheFont_4; }
	inline void set_TheFont_4(Font_t1956802104 * value)
	{
		___TheFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___TheFont_4), value);
	}

	inline static int32_t get_offset_of_floatingText_Script_5() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t177691618, ___floatingText_Script_5)); }
	inline TextMeshProFloatingText_t845872552 * get_floatingText_Script_5() const { return ___floatingText_Script_5; }
	inline TextMeshProFloatingText_t845872552 ** get_address_of_floatingText_Script_5() { return &___floatingText_Script_5; }
	inline void set_floatingText_Script_5(TextMeshProFloatingText_t845872552 * value)
	{
		___floatingText_Script_5 = value;
		Il2CppCodeGenWriteBarrier((&___floatingText_Script_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESHSPAWNER_T177691618_H
#ifndef SWIMMERGLOWSCRIPT_T3643536374_H
#define SWIMMERGLOWSCRIPT_T3643536374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SwimmerGlowScript
struct  SwimmerGlowScript_t3643536374  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject SwimmerGlowScript::GlowObject
	GameObject_t1113636619 * ___GlowObject_2;

public:
	inline static int32_t get_offset_of_GlowObject_2() { return static_cast<int32_t>(offsetof(SwimmerGlowScript_t3643536374, ___GlowObject_2)); }
	inline GameObject_t1113636619 * get_GlowObject_2() const { return ___GlowObject_2; }
	inline GameObject_t1113636619 ** get_address_of_GlowObject_2() { return &___GlowObject_2; }
	inline void set_GlowObject_2(GameObject_t1113636619 * value)
	{
		___GlowObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___GlowObject_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIMMERGLOWSCRIPT_T3643536374_H
#ifndef TMP_EXAMPLESCRIPT_01_T3051742005_H
#define TMP_EXAMPLESCRIPT_01_T3051742005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_ExampleScript_01
struct  TMP_ExampleScript_01_t3051742005  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.Examples.TMP_ExampleScript_01/objectType TMPro.Examples.TMP_ExampleScript_01::ObjectType
	int32_t ___ObjectType_2;
	// System.Boolean TMPro.Examples.TMP_ExampleScript_01::isStatic
	bool ___isStatic_3;
	// TMPro.TMP_Text TMPro.Examples.TMP_ExampleScript_01::m_text
	TMP_Text_t2599618874 * ___m_text_4;
	// System.Int32 TMPro.Examples.TMP_ExampleScript_01::count
	int32_t ___count_6;

public:
	inline static int32_t get_offset_of_ObjectType_2() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t3051742005, ___ObjectType_2)); }
	inline int32_t get_ObjectType_2() const { return ___ObjectType_2; }
	inline int32_t* get_address_of_ObjectType_2() { return &___ObjectType_2; }
	inline void set_ObjectType_2(int32_t value)
	{
		___ObjectType_2 = value;
	}

	inline static int32_t get_offset_of_isStatic_3() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t3051742005, ___isStatic_3)); }
	inline bool get_isStatic_3() const { return ___isStatic_3; }
	inline bool* get_address_of_isStatic_3() { return &___isStatic_3; }
	inline void set_isStatic_3(bool value)
	{
		___isStatic_3 = value;
	}

	inline static int32_t get_offset_of_m_text_4() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t3051742005, ___m_text_4)); }
	inline TMP_Text_t2599618874 * get_m_text_4() const { return ___m_text_4; }
	inline TMP_Text_t2599618874 ** get_address_of_m_text_4() { return &___m_text_4; }
	inline void set_m_text_4(TMP_Text_t2599618874 * value)
	{
		___m_text_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_text_4), value);
	}

	inline static int32_t get_offset_of_count_6() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t3051742005, ___count_6)); }
	inline int32_t get_count_6() const { return ___count_6; }
	inline int32_t* get_address_of_count_6() { return &___count_6; }
	inline void set_count_6(int32_t value)
	{
		___count_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_EXAMPLESCRIPT_01_T3051742005_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (QuarticEaseInCurve_t3162537896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (QuarticEaseInOutCurve_t1421613264), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (QuarticEaseOutCurve_t3969926341), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (QuadricEaseInCurve_t566295371), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (QuadricEaseInOutCurve_t2286750237), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (QuadricEaseOutCurve_t3528798688), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (BounceEaseOutCurve_t357678703), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (BounceEaseInCurve_t2000402526), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (BounceEaseInOutCurve_t2324510746), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (BackEaseInCurve_t2629419558), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (BackEaseOutCurve_t1141847129), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (BackEaseInOutCurve_t1136716490), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (ElasticEaseInCurve_t3556350251), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (ElasticEaseOutCurve_t2726315200), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (ElasticEaseInOutCurve_t3360119746), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (SineEaseInCurve_t3529713798), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (SineEaseOutCurve_t349540267), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (SineEaseInOutCurve_t3024528697), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (CircularEaseInCurve_t2784181567), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (CircularEaseOutCurve_t3998775700), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (CircularEaseInOutCurve_t3032629829), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (ExponentialEaseInCurve_t168678934), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (ExponentialEaseOutCurve_t3338962529), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (ExponentialEaseInOutCurve_t802286740), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (TweenAnimation_t1578776730), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2224[9] = 
{
	TweenAnimation_t1578776730::get_offset_of_frameCount_0(),
	TweenAnimation_t1578776730::get_offset_of_curve_1(),
	TweenAnimation_t1578776730::get_offset_of_duration_2(),
	TweenAnimation_t1578776730::get_offset_of_delay_3(),
	TweenAnimation_t1578776730::get_offset_of_startTime_4(),
	TweenAnimation_t1578776730::get_offset_of_transform_5(),
	TweenAnimation_t1578776730::get_offset_of_isFinished_6(),
	TweenAnimation_t1578776730::get_offset_of_finishCallback_7(),
	TweenAnimation_t1578776730::get_offset_of_startCallback_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (TweenScript_t2995776179), -1, sizeof(TweenScript_t2995776179_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2225[5] = 
{
	TweenScript_t2995776179::get_offset_of_timeScale_2(),
	TweenScript_t2995776179::get_offset_of_animations_3(),
	TweenScript_t2995776179::get_offset_of_animationsToEnqeue_4(),
	TweenScript_t2995776179_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_5(),
	TweenScript_t2995776179_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (CameraController_t3346819214), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2226[9] = 
{
	CameraController_t3346819214::get_offset_of_cameraTarget_2(),
	CameraController_t3346819214::get_offset_of_rotateSpeed_3(),
	CameraController_t3346819214::get_offset_of_rotate_4(),
	CameraController_t3346819214::get_offset_of_offsetDistance_5(),
	CameraController_t3346819214::get_offset_of_offsetHeight_6(),
	CameraController_t3346819214::get_offset_of_smoothing_7(),
	CameraController_t3346819214::get_offset_of_offset_8(),
	CameraController_t3346819214::get_offset_of_following_9(),
	CameraController_t3346819214::get_offset_of_lastPosition_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (Weapon_t4063826929)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2227[3] = 
{
	Weapon_t4063826929::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (RPGCharacterControllerFREE_t3036052416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2228[49] = 
{
	RPGCharacterControllerFREE_t3036052416::get_offset_of_rb_2(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_animator_3(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_target_4(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_targetDashDirection_5(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_sceneCamera_6(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_useNavMesh_7(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_agent_8(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_navMeshSpeed_9(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_goal_10(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_gravity_11(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_canJump_12(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_isJumping_13(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_isGrounded_14(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_jumpSpeed_15(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_doublejumpSpeed_16(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_doublejumping_17(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_canDoubleJump_18(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_isDoubleJumping_19(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_doublejumped_20(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_isFalling_21(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_startFall_22(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_fallingVelocity_23(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_inAirSpeed_24(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_maxVelocity_25(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_minVelocity_26(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_rollSpeed_27(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_isRolling_28(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_rollduration_29(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_canMove_30(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_walkSpeed_31(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_moveSpeed_32(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_runSpeed_33(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_rotationSpeed_34(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_x_35(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_z_36(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_dv_37(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_dh_38(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_inputVec_39(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_newVelocity_40(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_weapon_41(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_rightWeapon_42(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_leftWeapon_43(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_isRelax_44(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_canAction_45(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_isStrafing_46(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_isDead_47(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_isBlocking_48(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_knockbackMultiplier_49(),
	RPGCharacterControllerFREE_t3036052416::get_offset_of_isKnockback_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (U3C_JumpU3Ec__Iterator0_t3419148950), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2229[4] = 
{
	U3C_JumpU3Ec__Iterator0_t3419148950::get_offset_of_U24this_0(),
	U3C_JumpU3Ec__Iterator0_t3419148950::get_offset_of_U24current_1(),
	U3C_JumpU3Ec__Iterator0_t3419148950::get_offset_of_U24disposing_2(),
	U3C_JumpU3Ec__Iterator0_t3419148950::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (U3C_KnockbackU3Ec__Iterator1_t4196005666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2230[7] = 
{
	U3C_KnockbackU3Ec__Iterator1_t4196005666::get_offset_of_knockDirection_0(),
	U3C_KnockbackU3Ec__Iterator1_t4196005666::get_offset_of_knockBackAmount_1(),
	U3C_KnockbackU3Ec__Iterator1_t4196005666::get_offset_of_variableAmount_2(),
	U3C_KnockbackU3Ec__Iterator1_t4196005666::get_offset_of_U24this_3(),
	U3C_KnockbackU3Ec__Iterator1_t4196005666::get_offset_of_U24current_4(),
	U3C_KnockbackU3Ec__Iterator1_t4196005666::get_offset_of_U24disposing_5(),
	U3C_KnockbackU3Ec__Iterator1_t4196005666::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (U3C_KnockbackForceU3Ec__Iterator2_t1163480401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2231[7] = 
{
	U3C_KnockbackForceU3Ec__Iterator2_t1163480401::get_offset_of_knockDirection_0(),
	U3C_KnockbackForceU3Ec__Iterator2_t1163480401::get_offset_of_knockBackAmount_1(),
	U3C_KnockbackForceU3Ec__Iterator2_t1163480401::get_offset_of_variableAmount_2(),
	U3C_KnockbackForceU3Ec__Iterator2_t1163480401::get_offset_of_U24this_3(),
	U3C_KnockbackForceU3Ec__Iterator2_t1163480401::get_offset_of_U24current_4(),
	U3C_KnockbackForceU3Ec__Iterator2_t1163480401::get_offset_of_U24disposing_5(),
	U3C_KnockbackForceU3Ec__Iterator2_t1163480401::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (U3C_DeathU3Ec__Iterator3_t2107953850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2232[4] = 
{
	U3C_DeathU3Ec__Iterator3_t2107953850::get_offset_of_U24this_0(),
	U3C_DeathU3Ec__Iterator3_t2107953850::get_offset_of_U24current_1(),
	U3C_DeathU3Ec__Iterator3_t2107953850::get_offset_of_U24disposing_2(),
	U3C_DeathU3Ec__Iterator3_t2107953850::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (U3C_ReviveU3Ec__Iterator4_t660281937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2233[4] = 
{
	U3C_ReviveU3Ec__Iterator4_t660281937::get_offset_of_U24this_0(),
	U3C_ReviveU3Ec__Iterator4_t660281937::get_offset_of_U24current_1(),
	U3C_ReviveU3Ec__Iterator4_t660281937::get_offset_of_U24disposing_2(),
	U3C_ReviveU3Ec__Iterator4_t660281937::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (U3C_DirectionalRollU3Ec__Iterator5_t1563546577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2234[8] = 
{
	U3C_DirectionalRollU3Ec__Iterator5_t1563546577::get_offset_of_U3CangleU3E__0_0(),
	U3C_DirectionalRollU3Ec__Iterator5_t1563546577::get_offset_of_U3CsignU3E__0_1(),
	U3C_DirectionalRollU3Ec__Iterator5_t1563546577::get_offset_of_U3Csigned_angleU3E__0_2(),
	U3C_DirectionalRollU3Ec__Iterator5_t1563546577::get_offset_of_U3Cangle360U3E__0_3(),
	U3C_DirectionalRollU3Ec__Iterator5_t1563546577::get_offset_of_U24this_4(),
	U3C_DirectionalRollU3Ec__Iterator5_t1563546577::get_offset_of_U24current_5(),
	U3C_DirectionalRollU3Ec__Iterator5_t1563546577::get_offset_of_U24disposing_6(),
	U3C_DirectionalRollU3Ec__Iterator5_t1563546577::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (U3C_RollU3Ec__Iterator6_t1187791279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2235[5] = 
{
	U3C_RollU3Ec__Iterator6_t1187791279::get_offset_of_rollNumber_0(),
	U3C_RollU3Ec__Iterator6_t1187791279::get_offset_of_U24this_1(),
	U3C_RollU3Ec__Iterator6_t1187791279::get_offset_of_U24current_2(),
	U3C_RollU3Ec__Iterator6_t1187791279::get_offset_of_U24disposing_3(),
	U3C_RollU3Ec__Iterator6_t1187791279::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (U3C_LockMovementAndAttackU3Ec__Iterator7_t3589827545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2236[6] = 
{
	U3C_LockMovementAndAttackU3Ec__Iterator7_t3589827545::get_offset_of_delayTime_0(),
	U3C_LockMovementAndAttackU3Ec__Iterator7_t3589827545::get_offset_of_lockTime_1(),
	U3C_LockMovementAndAttackU3Ec__Iterator7_t3589827545::get_offset_of_U24this_2(),
	U3C_LockMovementAndAttackU3Ec__Iterator7_t3589827545::get_offset_of_U24current_3(),
	U3C_LockMovementAndAttackU3Ec__Iterator7_t3589827545::get_offset_of_U24disposing_4(),
	U3C_LockMovementAndAttackU3Ec__Iterator7_t3589827545::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (ScoreManager_t3621325103), -1, sizeof(ScoreManager_t3621325103_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2237[2] = 
{
	ScoreManager_t3621325103_StaticFields::get_offset_of_score_2(),
	ScoreManager_t3621325103::get_offset_of_text_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (ApplicationModel_t3549425075), -1, sizeof(ApplicationModel_t3549425075_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2238[1] = 
{
	ApplicationModel_t3549425075_StaticFields::get_offset_of_isPaused_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (BeachMen_t363650106), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2239[1] = 
{
	BeachMen_t363650106::get_offset_of_animator_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (CageAppearance_t1912774197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2240[3] = 
{
	CageAppearance_t1912774197::get_offset_of_colours_2(),
	CageAppearance_t1912774197::get_offset_of_cageBody_3(),
	CageAppearance_t1912774197::get_offset_of_cageDoor_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (CageCollision_t1104701574), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2241[2] = 
{
	CageCollision_t1104701574::get_offset_of_shark_2(),
	CageCollision_t1104701574::get_offset_of_sphereCollider_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (CleanUpCage_t1266394266), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (ColourGenerator_t3527398847), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2243[3] = 
{
	ColourGenerator_t3527398847::get_offset_of_suit_2(),
	ColourGenerator_t3527398847::get_offset_of_shark_3(),
	ColourGenerator_t3527398847::get_offset_of_collider_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (Constants_t701097383), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (UIPanels_t1090734791), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2245[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (DragScript_t1732324504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2246[7] = 
{
	DragScript_t1732324504::get_offset_of_isDraggable_2(),
	DragScript_t1732324504::get_offset_of_outline_3(),
	DragScript_t1732324504::get_offset_of_Halo_4(),
	DragScript_t1732324504::get_offset_of_gameManager_5(),
	DragScript_t1732324504::get_offset_of_distance_6(),
	DragScript_t1732324504::get_offset_of_originalY_7(),
	DragScript_t1732324504::get_offset_of_hasHitWater_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (GlowScript_t446333960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2247[2] = 
{
	GlowScript_t446333960::get_offset_of_red_2(),
	GlowScript_t446333960::get_offset_of_green_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (HoverScript_t4095815920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2248[1] = 
{
	HoverScript_t4095815920::get_offset_of_Sound_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (Manager_t2928243666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2249[50] = 
{
	Manager_t2928243666::get_offset_of_swimmerShownDuration_2(),
	Manager_t2928243666::get_offset_of_scoreMultiplier_3(),
	Manager_t2928243666::get_offset_of_diffSlider_4(),
	Manager_t2928243666::get_offset_of_exposureSlider_5(),
	Manager_t2928243666::get_offset_of_timeSlider_6(),
	Manager_t2928243666::get_offset_of_pauseButton_7(),
	Manager_t2928243666::get_offset_of_swimmer_8(),
	Manager_t2928243666::get_offset_of_cage_9(),
	Manager_t2928243666::get_offset_of_Beach_10(),
	Manager_t2928243666::get_offset_of_cageImage_11(),
	Manager_t2928243666::get_offset_of_GlowSphere_12(),
	Manager_t2928243666::get_offset_of_UIController_13(),
	Manager_t2928243666::get_offset_of_SoundController_14(),
	Manager_t2928243666::get_offset_of_GameAnimator_15(),
	Manager_t2928243666::get_offset_of_hintText_16(),
	Manager_t2928243666::get_offset_of_scoreText_17(),
	Manager_t2928243666::get_offset_of_totalScoreText_18(),
	Manager_t2928243666::get_offset_of_cageTimerText_19(),
	Manager_t2928243666::get_offset_of_colours_20(),
	Manager_t2928243666::get_offset_of_spawnValues_21(),
	Manager_t2928243666::get_offset_of_cagePosition_22(),
	Manager_t2928243666::get_offset_of_cageColourText_23(),
	Manager_t2928243666::get_offset_of_diffCount_24(),
	Manager_t2928243666::get_offset_of_cageType_25(),
	Manager_t2928243666::get_offset_of_cageSpawnCount_26(),
	Manager_t2928243666::get_offset_of_usedCageCount_27(),
	Manager_t2928243666::get_offset_of_correctCages_28(),
	Manager_t2928243666::get_offset_of_startingScore_29(),
	Manager_t2928243666::get_offset_of_currentScore_30(),
	Manager_t2928243666::get_offset_of_totalTime_31(),
	Manager_t2928243666::get_offset_of_cageTimer_32(),
	Manager_t2928243666::get_offset_of_totalCageTime_33(),
	Manager_t2928243666::get_offset_of_swimmersSpawned_34(),
	Manager_t2928243666::get_offset_of_gameStarted_35(),
	Manager_t2928243666::get_offset_of_cagesActive_36(),
	Manager_t2928243666::get_offset_of_cageHintActive_37(),
	Manager_t2928243666::get_offset_of_firstTimeTutorial_38(),
	Manager_t2928243666::get_offset_of_handStage_39(),
	Manager_t2928243666::get_offset_of_tutorialDone_40(),
	Manager_t2928243666::get_offset_of_handSpeed_41(),
	Manager_t2928243666::get_offset_of_tutorialTarget_42(),
	Manager_t2928243666::get_offset_of_tutorialRepeat_43(),
	Manager_t2928243666::get_offset_of_hintTextType_44(),
	Manager_t2928243666::get_offset_of_textType_45(),
	Manager_t2928243666::get_offset_of_GreenCircle_46(),
	Manager_t2928243666::get_offset_of_GreenCicle2_47(),
	Manager_t2928243666::get_offset_of_RedCircle_48(),
	Manager_t2928243666::get_offset_of_beachPeople_49(),
	Manager_t2928243666::get_offset_of_swimmers_50(),
	Manager_t2928243666::get_offset_of_cages_51(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (playerState_t89644523)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2250[5] = 
{
	playerState_t89644523::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (U3CstartAnimU3Ec__Iterator0_t710289490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2251[6] = 
{
	U3CstartAnimU3Ec__Iterator0_t710289490::get_offset_of_U3CiU3E__1_0(),
	U3CstartAnimU3Ec__Iterator0_t710289490::get_offset_of_U3CrandSecU3E__2_1(),
	U3CstartAnimU3Ec__Iterator0_t710289490::get_offset_of_U24this_2(),
	U3CstartAnimU3Ec__Iterator0_t710289490::get_offset_of_U24current_3(),
	U3CstartAnimU3Ec__Iterator0_t710289490::get_offset_of_U24disposing_4(),
	U3CstartAnimU3Ec__Iterator0_t710289490::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (MenuScript_t2744967707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2252[12] = 
{
	MenuScript_t2744967707::get_offset_of_StartPanel_2(),
	MenuScript_t2744967707::get_offset_of_StartPanelSelection_3(),
	MenuScript_t2744967707::get_offset_of_ExposurePanel_4(),
	MenuScript_t2744967707::get_offset_of_ExposureSelection_5(),
	MenuScript_t2744967707::get_offset_of_TimePanel_6(),
	MenuScript_t2744967707::get_offset_of_TimePanelSelection_7(),
	MenuScript_t2744967707::get_offset_of_InstructionsPanel_8(),
	MenuScript_t2744967707::get_offset_of_SettingsPanel_9(),
	MenuScript_t2744967707::get_offset_of_EndPanel_10(),
	MenuScript_t2744967707::get_offset_of_ResetPanel_11(),
	MenuScript_t2744967707::get_offset_of_panels_12(),
	MenuScript_t2744967707::get_offset_of_animations_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (U3CHidePanelU3Ec__AnonStorey0_t4128008152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2253[1] = 
{
	U3CHidePanelU3Ec__AnonStorey0_t4128008152::get_offset_of_entry_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (MovementScript_t3129111103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2254[4] = 
{
	MovementScript_t3129111103::get_offset_of_state_2(),
	MovementScript_t3129111103::get_offset_of_swimmer_3(),
	MovementScript_t3129111103::get_offset_of_sharkCaptured_4(),
	MovementScript_t3129111103::get_offset_of_totalDive_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (playerState_t2716384857)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2255[5] = 
{
	playerState_t2716384857::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (SwimmerGlowScript_t3643536374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2256[1] = 
{
	SwimmerGlowScript_t3643536374::get_offset_of_GlowObject_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (Benchmark01_t1571072624), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2257[10] = 
{
	Benchmark01_t1571072624::get_offset_of_BenchmarkType_2(),
	Benchmark01_t1571072624::get_offset_of_TMProFont_3(),
	Benchmark01_t1571072624::get_offset_of_TextMeshFont_4(),
	Benchmark01_t1571072624::get_offset_of_m_textMeshPro_5(),
	Benchmark01_t1571072624::get_offset_of_m_textContainer_6(),
	Benchmark01_t1571072624::get_offset_of_m_textMesh_7(),
	0,
	0,
	Benchmark01_t1571072624::get_offset_of_m_material01_10(),
	Benchmark01_t1571072624::get_offset_of_m_material02_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (U3CStartU3Ec__Iterator0_t2216151886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2258[5] = 
{
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U3CiU3E__1_0(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (Benchmark01_UGUI_t3264177817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2259[10] = 
{
	Benchmark01_UGUI_t3264177817::get_offset_of_BenchmarkType_2(),
	Benchmark01_UGUI_t3264177817::get_offset_of_canvas_3(),
	Benchmark01_UGUI_t3264177817::get_offset_of_TMProFont_4(),
	Benchmark01_UGUI_t3264177817::get_offset_of_TextMeshFont_5(),
	Benchmark01_UGUI_t3264177817::get_offset_of_m_textMeshPro_6(),
	Benchmark01_UGUI_t3264177817::get_offset_of_m_textMesh_7(),
	0,
	0,
	Benchmark01_UGUI_t3264177817::get_offset_of_m_material01_10(),
	Benchmark01_UGUI_t3264177817::get_offset_of_m_material02_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (U3CStartU3Ec__Iterator0_t2622988697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2260[5] = 
{
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U3CiU3E__1_0(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (Benchmark02_t1571269232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2261[3] = 
{
	Benchmark02_t1571269232::get_offset_of_SpawnType_2(),
	Benchmark02_t1571269232::get_offset_of_NumberOfNPC_3(),
	Benchmark02_t1571269232::get_offset_of_floatingText_Script_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (Benchmark03_t1571203696), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2262[3] = 
{
	Benchmark03_t1571203696::get_offset_of_SpawnType_2(),
	Benchmark03_t1571203696::get_offset_of_NumberOfNPC_3(),
	Benchmark03_t1571203696::get_offset_of_TheFont_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (Benchmark04_t1570876016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2263[5] = 
{
	Benchmark04_t1570876016::get_offset_of_SpawnType_2(),
	Benchmark04_t1570876016::get_offset_of_MinPointSize_3(),
	Benchmark04_t1570876016::get_offset_of_MaxPointSize_4(),
	Benchmark04_t1570876016::get_offset_of_Steps_5(),
	Benchmark04_t1570876016::get_offset_of_m_Transform_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (CameraController_t2264742161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2264[25] = 
{
	CameraController_t2264742161::get_offset_of_cameraTransform_2(),
	CameraController_t2264742161::get_offset_of_dummyTarget_3(),
	CameraController_t2264742161::get_offset_of_CameraTarget_4(),
	CameraController_t2264742161::get_offset_of_FollowDistance_5(),
	CameraController_t2264742161::get_offset_of_MaxFollowDistance_6(),
	CameraController_t2264742161::get_offset_of_MinFollowDistance_7(),
	CameraController_t2264742161::get_offset_of_ElevationAngle_8(),
	CameraController_t2264742161::get_offset_of_MaxElevationAngle_9(),
	CameraController_t2264742161::get_offset_of_MinElevationAngle_10(),
	CameraController_t2264742161::get_offset_of_OrbitalAngle_11(),
	CameraController_t2264742161::get_offset_of_CameraMode_12(),
	CameraController_t2264742161::get_offset_of_MovementSmoothing_13(),
	CameraController_t2264742161::get_offset_of_RotationSmoothing_14(),
	CameraController_t2264742161::get_offset_of_previousSmoothing_15(),
	CameraController_t2264742161::get_offset_of_MovementSmoothingValue_16(),
	CameraController_t2264742161::get_offset_of_RotationSmoothingValue_17(),
	CameraController_t2264742161::get_offset_of_MoveSensitivity_18(),
	CameraController_t2264742161::get_offset_of_currentVelocity_19(),
	CameraController_t2264742161::get_offset_of_desiredPosition_20(),
	CameraController_t2264742161::get_offset_of_mouseX_21(),
	CameraController_t2264742161::get_offset_of_mouseY_22(),
	CameraController_t2264742161::get_offset_of_moveVector_23(),
	CameraController_t2264742161::get_offset_of_mouseWheel_24(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (CameraModes_t3200559075)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2265[4] = 
{
	CameraModes_t3200559075::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (ChatController_t3486202795), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2266[3] = 
{
	ChatController_t3486202795::get_offset_of_TMP_ChatInput_2(),
	ChatController_t3486202795::get_offset_of_TMP_ChatOutput_3(),
	ChatController_t3486202795::get_offset_of_ChatScrollbar_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (EnvMapAnimator_t1140999784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2267[3] = 
{
	EnvMapAnimator_t1140999784::get_offset_of_RotationSpeeds_2(),
	EnvMapAnimator_t1140999784::get_offset_of_m_textMeshPro_3(),
	EnvMapAnimator_t1140999784::get_offset_of_m_material_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (U3CStartU3Ec__Iterator0_t1520811813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2268[5] = 
{
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U3CmatrixU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (ObjectSpin_t341713598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2269[10] = 
{
	ObjectSpin_t341713598::get_offset_of_SpinSpeed_2(),
	ObjectSpin_t341713598::get_offset_of_RotationRange_3(),
	ObjectSpin_t341713598::get_offset_of_m_transform_4(),
	ObjectSpin_t341713598::get_offset_of_m_time_5(),
	ObjectSpin_t341713598::get_offset_of_m_prevPOS_6(),
	ObjectSpin_t341713598::get_offset_of_m_initial_Rotation_7(),
	ObjectSpin_t341713598::get_offset_of_m_initial_Position_8(),
	ObjectSpin_t341713598::get_offset_of_m_lightColor_9(),
	ObjectSpin_t341713598::get_offset_of_frames_10(),
	ObjectSpin_t341713598::get_offset_of_Motion_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (MotionType_t1905163921)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2270[4] = 
{
	MotionType_t1905163921::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (ShaderPropAnimator_t3617420994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2271[4] = 
{
	ShaderPropAnimator_t3617420994::get_offset_of_m_Renderer_2(),
	ShaderPropAnimator_t3617420994::get_offset_of_m_Material_3(),
	ShaderPropAnimator_t3617420994::get_offset_of_GlowCurve_4(),
	ShaderPropAnimator_t3617420994::get_offset_of_m_frame_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (U3CAnimatePropertiesU3Ec__Iterator0_t4041402054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2272[5] = 
{
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U3CglowPowerU3E__1_0(),
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U24this_1(),
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U24current_2(),
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U24disposing_3(),
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (SimpleScript_t3279312205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2273[3] = 
{
	SimpleScript_t3279312205::get_offset_of_m_textMeshPro_2(),
	0,
	SimpleScript_t3279312205::get_offset_of_m_frame_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (SkewTextExample_t3460249701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2274[4] = 
{
	SkewTextExample_t3460249701::get_offset_of_m_TextComponent_2(),
	SkewTextExample_t3460249701::get_offset_of_VertexCurve_3(),
	SkewTextExample_t3460249701::get_offset_of_CurveScale_4(),
	SkewTextExample_t3460249701::get_offset_of_ShearAmount_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (U3CWarpTextU3Ec__Iterator0_t116130919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2275[13] = 
{
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3Cold_CurveScaleU3E__0_0(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3Cold_ShearValueU3E__0_1(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3Cold_curveU3E__0_2(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CtextInfoU3E__1_3(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CcharacterCountU3E__1_4(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CboundsMinXU3E__1_5(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CboundsMaxXU3E__1_6(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CverticesU3E__2_7(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CmatrixU3E__2_8(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U24this_9(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U24current_10(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U24disposing_11(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U24PC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (TeleType_t2409835159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2276[3] = 
{
	TeleType_t2409835159::get_offset_of_label01_2(),
	TeleType_t2409835159::get_offset_of_label02_3(),
	TeleType_t2409835159::get_offset_of_m_textMeshPro_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (U3CStartU3Ec__Iterator0_t3341539328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2277[7] = 
{
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U3CtotalVisibleCharactersU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U3CcounterU3E__0_1(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U3CvisibleCountU3E__0_2(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U24this_3(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U24current_4(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U24disposing_5(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (TextConsoleSimulator_t3766250034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2278[2] = 
{
	TextConsoleSimulator_t3766250034::get_offset_of_m_TextComponent_2(),
	TextConsoleSimulator_t3766250034::get_offset_of_hasTextChanged_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (U3CRevealCharactersU3Ec__Iterator0_t860191687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2279[8] = 
{
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_textComponent_0(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U3CtextInfoU3E__0_1(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U3CtotalVisibleCharactersU3E__0_2(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U3CvisibleCountU3E__0_3(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U24this_4(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U24current_5(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U24disposing_6(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (U3CRevealWordsU3Ec__Iterator1_t1343183262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2280[9] = 
{
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_textComponent_0(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U3CtotalWordCountU3E__0_1(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U3CtotalVisibleCharactersU3E__0_2(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U3CcounterU3E__0_3(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U3CcurrentWordU3E__0_4(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U3CvisibleCountU3E__0_5(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U24current_6(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U24disposing_7(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (TextMeshProFloatingText_t845872552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2281[10] = 
{
	TextMeshProFloatingText_t845872552::get_offset_of_TheFont_2(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_floatingText_3(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_textMeshPro_4(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_textMesh_5(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_transform_6(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_floatingText_Transform_7(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_cameraTransform_8(),
	TextMeshProFloatingText_t845872552::get_offset_of_lastPOS_9(),
	TextMeshProFloatingText_t845872552::get_offset_of_lastRotation_10(),
	TextMeshProFloatingText_t845872552::get_offset_of_SpawnType_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2282[11] = 
{
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3CCountDurationU3E__0_0(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3Cstarting_CountU3E__0_1(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3Ccurrent_CountU3E__0_2(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3Cstart_posU3E__0_3(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3Cstart_colorU3E__0_4(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3CalphaU3E__0_5(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3CfadeDurationU3E__0_6(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U24this_7(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U24current_8(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U24disposing_9(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2283[12] = 
{
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3CCountDurationU3E__0_0(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3Cstarting_CountU3E__0_1(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3Ccurrent_CountU3E__0_2(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3Cstart_posU3E__0_3(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3Cstart_colorU3E__0_4(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3CalphaU3E__0_5(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3Cint_counterU3E__0_6(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3CfadeDurationU3E__0_7(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U24this_8(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U24current_9(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U24disposing_10(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (TextMeshSpawner_t177691618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2284[4] = 
{
	TextMeshSpawner_t177691618::get_offset_of_SpawnType_2(),
	TextMeshSpawner_t177691618::get_offset_of_NumberOfNPC_3(),
	TextMeshSpawner_t177691618::get_offset_of_TheFont_4(),
	TextMeshSpawner_t177691618::get_offset_of_floatingText_Script_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (TMP_DigitValidator_t573672104), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (TMP_ExampleScript_01_t3051742005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2286[5] = 
{
	TMP_ExampleScript_01_t3051742005::get_offset_of_ObjectType_2(),
	TMP_ExampleScript_01_t3051742005::get_offset_of_isStatic_3(),
	TMP_ExampleScript_01_t3051742005::get_offset_of_m_text_4(),
	0,
	TMP_ExampleScript_01_t3051742005::get_offset_of_count_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (objectType_t4082700821)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2287[3] = 
{
	objectType_t4082700821::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (TMP_FrameRateCounter_t314972976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2288[10] = 
{
	TMP_FrameRateCounter_t314972976::get_offset_of_UpdateInterval_2(),
	TMP_FrameRateCounter_t314972976::get_offset_of_m_LastInterval_3(),
	TMP_FrameRateCounter_t314972976::get_offset_of_m_Frames_4(),
	TMP_FrameRateCounter_t314972976::get_offset_of_AnchorPosition_5(),
	TMP_FrameRateCounter_t314972976::get_offset_of_htmlColorTag_6(),
	0,
	TMP_FrameRateCounter_t314972976::get_offset_of_m_TextMeshPro_8(),
	TMP_FrameRateCounter_t314972976::get_offset_of_m_frameCounter_transform_9(),
	TMP_FrameRateCounter_t314972976::get_offset_of_m_camera_10(),
	TMP_FrameRateCounter_t314972976::get_offset_of_last_AnchorPosition_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (FpsCounterAnchorPositions_t1585798158)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2289[5] = 
{
	FpsCounterAnchorPositions_t1585798158::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (TMP_TextEventCheck_t1103849140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2290[1] = 
{
	TMP_TextEventCheck_t1103849140::get_offset_of_TextEventHandler_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (TMP_TextEventHandler_t1869054637), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2291[11] = 
{
	TMP_TextEventHandler_t1869054637::get_offset_of_m_OnCharacterSelection_2(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_OnWordSelection_3(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_OnLineSelection_4(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_OnLinkSelection_5(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_TextComponent_6(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_Camera_7(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_Canvas_8(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_selectedLink_9(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_lastCharIndex_10(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_lastWordIndex_11(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_lastLineIndex_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (CharacterSelectionEvent_t3109943174), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (WordSelectionEvent_t1841909953), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (LineSelectionEvent_t2868010532), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (LinkSelectionEvent_t1590929858), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (TMP_TextInfoDebugTool_t1868681444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2296[9] = 
{
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ShowCharacters_2(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ShowWords_3(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ShowLinks_4(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ShowLines_5(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ShowMeshBounds_6(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ShowTextBounds_7(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ObjectStats_8(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_m_TextComponent_9(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_m_Transform_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (TMP_TextSelector_A_t3982526506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2297[6] = 
{
	TMP_TextSelector_A_t3982526506::get_offset_of_m_TextMeshPro_2(),
	TMP_TextSelector_A_t3982526506::get_offset_of_m_Camera_3(),
	TMP_TextSelector_A_t3982526506::get_offset_of_m_isHoveringObject_4(),
	TMP_TextSelector_A_t3982526506::get_offset_of_m_selectedLink_5(),
	TMP_TextSelector_A_t3982526506::get_offset_of_m_lastCharIndex_6(),
	TMP_TextSelector_A_t3982526506::get_offset_of_m_lastWordIndex_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (TMP_TextSelector_B_t3982526505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2298[14] = 
{
	TMP_TextSelector_B_t3982526505::get_offset_of_TextPopup_Prefab_01_2(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_TextPopup_RectTransform_3(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_TextPopup_TMPComponent_4(),
	0,
	0,
	TMP_TextSelector_B_t3982526505::get_offset_of_m_TextMeshPro_7(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_Canvas_8(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_Camera_9(),
	TMP_TextSelector_B_t3982526505::get_offset_of_isHoveringObject_10(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_selectedWord_11(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_selectedLink_12(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_lastIndex_13(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_matrix_14(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_cachedMeshInfoVertexData_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (TMP_UiFrameRateCounter_t811747397), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2299[9] = 
{
	TMP_UiFrameRateCounter_t811747397::get_offset_of_UpdateInterval_2(),
	TMP_UiFrameRateCounter_t811747397::get_offset_of_m_LastInterval_3(),
	TMP_UiFrameRateCounter_t811747397::get_offset_of_m_Frames_4(),
	TMP_UiFrameRateCounter_t811747397::get_offset_of_AnchorPosition_5(),
	TMP_UiFrameRateCounter_t811747397::get_offset_of_htmlColorTag_6(),
	0,
	TMP_UiFrameRateCounter_t811747397::get_offset_of_m_TextMeshPro_8(),
	TMP_UiFrameRateCounter_t811747397::get_offset_of_m_frameCounter_transform_9(),
	TMP_UiFrameRateCounter_t811747397::get_offset_of_last_AnchorPosition_10(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
