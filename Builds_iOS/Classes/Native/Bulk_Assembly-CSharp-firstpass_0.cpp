﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityStandardAssets.Water.Displace
struct Displace_t1352130581;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.Behaviour
struct Behaviour_t1437897464;
// System.String
struct String_t;
// UnityStandardAssets.Water.GerstnerDisplace
struct GerstnerDisplace_t2537102777;
// UnityStandardAssets.Water.MeshContainer
struct MeshContainer_t140041298;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityStandardAssets.Water.PlanarReflection
struct PlanarReflection_t439636033;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Type
struct Type_t;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Object
struct Object_t631007953;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,System.Boolean>
struct Dictionary_2_t310742658;
// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t1444694249;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Skybox
struct Skybox_t2662837510;
// UnityStandardAssets.Water.SpecularLighting
struct SpecularLighting_t2163114238;
// UnityStandardAssets.Water.Water
struct Water_t936588004;
// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera>
struct Dictionary_2_t75641268;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t132545152;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// UnityEngine.FlareLayer
struct FlareLayer_t1739223323;
// UnityStandardAssets.Water.WaterBase
struct WaterBase_t3883863317;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityStandardAssets.Water.WaterTile
struct WaterTile_t2536246541;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t964245573;
// UnityEngine.Camera[]
struct CameraU5BU5D_t1709987734;
// System.Boolean[]
struct BooleanU5BU5D_t2897418192;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Camera>
struct IEqualityComparer_1_t1969518593;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Camera,System.Boolean,System.Collections.DictionaryEntry>
struct Transform_1_t2106966386;
// System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Camera,UnityEngine.Camera,System.Collections.DictionaryEntry>
struct Transform_1_t919535928;
// System.Void
struct Void_t1185182177;
// System.Reflection.MemberFilter
struct MemberFilter_t426314064;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t190067161;

extern String_t* _stringLiteral3533732601;
extern String_t* _stringLiteral240773206;
extern const uint32_t Displace_OnEnable_m2262001474_MetadataUsageId;
extern const uint32_t Displace_OnDisable_m3700756168_MetadataUsageId;
extern String_t* _stringLiteral253045501;
extern const uint32_t PlanarReflection__ctor_m4060724367_MetadataUsageId;
extern const RuntimeType* WaterBase_t3883863317_0_0_0_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern RuntimeClass* WaterBase_t3883863317_il2cpp_TypeInfo_var;
extern const uint32_t PlanarReflection_Start_m1671959525_MetadataUsageId;
extern const RuntimeType* Camera_t4157153871_0_0_0_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var;
extern RuntimeClass* GameObject_t1113636619_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_GetComponent_TisCamera_t4157153871_m1507399110_RuntimeMethod_var;
extern String_t* _stringLiteral2505626915;
extern const uint32_t PlanarReflection_CreateReflectionCameraFor_m898764438_MetadataUsageId;
extern String_t* _stringLiteral78692563;
extern const uint32_t PlanarReflection_SetStandardCameraParameter_m3135024219_MetadataUsageId;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern RuntimeClass* RenderTexture_t2108887433_il2cpp_TypeInfo_var;
extern const uint32_t PlanarReflection_CreateTextureFor_m3687076934_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t310742658_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m1064274257_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_ContainsKey_m1090609320_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m518130851_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_m3182360459_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_set_Item_m857652548_RuntimeMethod_var;
extern const uint32_t PlanarReflection_RenderHelpCameras_m2976503157_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_Clear_m4167034391_RuntimeMethod_var;
extern const uint32_t PlanarReflection_LateUpdate_m53228134_MetadataUsageId;
extern const uint32_t PlanarReflection_WaterTileBeingRendered_m1672873199_MetadataUsageId;
extern String_t* _stringLiteral2712144963;
extern String_t* _stringLiteral1050277699;
extern const uint32_t PlanarReflection_OnEnable_m2568423146_MetadataUsageId;
extern const uint32_t PlanarReflection_OnDisable_m1191548495_MetadataUsageId;
extern const RuntimeType* Skybox_t2662837510_0_0_0_var;
extern RuntimeClass* Skybox_t2662837510_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern RuntimeClass* Matrix4x4_t1817901843_il2cpp_TypeInfo_var;
extern const uint32_t PlanarReflection_RenderReflectionFor_m68969975_MetadataUsageId;
extern RuntimeClass* Vector4_t3319028937_il2cpp_TypeInfo_var;
extern const uint32_t PlanarReflection_CalculateObliqueMatrix_m3657792474_MetadataUsageId;
extern const uint32_t PlanarReflection_CameraSpacePlane_m3103867915_MetadataUsageId;
extern const uint32_t SpecularLighting_Start_m3290965022_MetadataUsageId;
extern String_t* _stringLiteral1648708994;
extern const uint32_t SpecularLighting_Update_m2066293166_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t75641268_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m579872301_RuntimeMethod_var;
extern const uint32_t Water__ctor_m1153371576_MetadataUsageId;
extern RuntimeClass* Water_t936588004_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisRenderer_t2627027031_m1858649706_RuntimeMethod_var;
extern String_t* _stringLiteral521248767;
extern String_t* _stringLiteral1727694679;
extern const uint32_t Water_OnWillRenderObject_m1537605840_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_GetEnumerator_m484651291_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m2791492809_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Value_m1196865026_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m2261968848_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m2390683763_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Clear_m1016216395_RuntimeMethod_var;
extern const uint32_t Water_OnDisable_m66027400_MetadataUsageId;
extern String_t* _stringLiteral49249872;
extern String_t* _stringLiteral3029421264;
extern String_t* _stringLiteral2268830102;
extern String_t* _stringLiteral2617886994;
extern const uint32_t Water_Update_m2292850110_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisSkybox_t2662837510_m2500560783_RuntimeMethod_var;
extern const uint32_t Water_UpdateCameraModes_m3658611570_MetadataUsageId;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern RuntimeClass* ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m3746080878_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisFlareLayer_t1739223323_m722533630_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_set_Item_m1091284778_RuntimeMethod_var;
extern String_t* _stringLiteral3277177482;
extern String_t* _stringLiteral1537969950;
extern String_t* _stringLiteral1505539685;
extern String_t* _stringLiteral1366986456;
extern String_t* _stringLiteral2541588253;
extern const uint32_t Water_CreateWaterObjects_m2240077532_MetadataUsageId;
extern String_t* _stringLiteral713880165;
extern String_t* _stringLiteral1103090832;
extern String_t* _stringLiteral2505954588;
extern const uint32_t Water_FindHardwareWaterSupport_m3785980915_MetadataUsageId;
extern const uint32_t Water_CameraSpacePlane_m2332942409_MetadataUsageId;
extern String_t* _stringLiteral1508871393;
extern String_t* _stringLiteral3272779580;
extern const uint32_t WaterBase_UpdateShader_m695718468_MetadataUsageId;
extern const uint32_t WaterBase_WaterTileBeingRendered_m3136753890_MetadataUsageId;
extern const uint32_t WaterBase_Update_m2764498022_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisPlanarReflection_t439636033_m1616117542_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisWaterBase_t3883863317_m2958773954_RuntimeMethod_var;
extern const uint32_t WaterTile_AcquireComponents_m1557480152_MetadataUsageId;
extern const uint32_t WaterTile_OnWillRenderObject_m21485850_MetadataUsageId;

struct Vector3U5BU5D_t1718750761;
struct TypeU5BU5D_t3940880105;
struct ObjectU5BU5D_t2843939325;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745551_H
#define U3CMODULEU3E_T692745551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745551 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745551_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef MESHCONTAINER_T140041298_H
#define MESHCONTAINER_T140041298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.MeshContainer
struct  MeshContainer_t140041298  : public RuntimeObject
{
public:
	// UnityEngine.Mesh UnityStandardAssets.Water.MeshContainer::mesh
	Mesh_t3648964284 * ___mesh_0;
	// UnityEngine.Vector3[] UnityStandardAssets.Water.MeshContainer::vertices
	Vector3U5BU5D_t1718750761* ___vertices_1;
	// UnityEngine.Vector3[] UnityStandardAssets.Water.MeshContainer::normals
	Vector3U5BU5D_t1718750761* ___normals_2;

public:
	inline static int32_t get_offset_of_mesh_0() { return static_cast<int32_t>(offsetof(MeshContainer_t140041298, ___mesh_0)); }
	inline Mesh_t3648964284 * get_mesh_0() const { return ___mesh_0; }
	inline Mesh_t3648964284 ** get_address_of_mesh_0() { return &___mesh_0; }
	inline void set_mesh_0(Mesh_t3648964284 * value)
	{
		___mesh_0 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_0), value);
	}

	inline static int32_t get_offset_of_vertices_1() { return static_cast<int32_t>(offsetof(MeshContainer_t140041298, ___vertices_1)); }
	inline Vector3U5BU5D_t1718750761* get_vertices_1() const { return ___vertices_1; }
	inline Vector3U5BU5D_t1718750761** get_address_of_vertices_1() { return &___vertices_1; }
	inline void set_vertices_1(Vector3U5BU5D_t1718750761* value)
	{
		___vertices_1 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_1), value);
	}

	inline static int32_t get_offset_of_normals_2() { return static_cast<int32_t>(offsetof(MeshContainer_t140041298, ___normals_2)); }
	inline Vector3U5BU5D_t1718750761* get_normals_2() const { return ___normals_2; }
	inline Vector3U5BU5D_t1718750761** get_address_of_normals_2() { return &___normals_2; }
	inline void set_normals_2(Vector3U5BU5D_t1718750761* value)
	{
		___normals_2 = value;
		Il2CppCodeGenWriteBarrier((&___normals_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHCONTAINER_T140041298_H
#ifndef DICTIONARY_2_T310742658_H
#define DICTIONARY_2_T310742658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,System.Boolean>
struct  Dictionary_2_t310742658  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	CameraU5BU5D_t1709987734* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	BooleanU5BU5D_t2897418192* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t310742658, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t310742658, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t310742658, ___keySlots_6)); }
	inline CameraU5BU5D_t1709987734* get_keySlots_6() const { return ___keySlots_6; }
	inline CameraU5BU5D_t1709987734** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(CameraU5BU5D_t1709987734* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t310742658, ___valueSlots_7)); }
	inline BooleanU5BU5D_t2897418192* get_valueSlots_7() const { return ___valueSlots_7; }
	inline BooleanU5BU5D_t2897418192** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(BooleanU5BU5D_t2897418192* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t310742658, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t310742658, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t310742658, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t310742658, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t310742658, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t310742658, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t310742658, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t310742658_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t2106966386 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t310742658_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t2106966386 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t2106966386 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t2106966386 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T310742658_H
#ifndef DICTIONARY_2_T75641268_H
#define DICTIONARY_2_T75641268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera>
struct  Dictionary_2_t75641268  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	CameraU5BU5D_t1709987734* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	CameraU5BU5D_t1709987734* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t75641268, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t75641268, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t75641268, ___keySlots_6)); }
	inline CameraU5BU5D_t1709987734* get_keySlots_6() const { return ___keySlots_6; }
	inline CameraU5BU5D_t1709987734** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(CameraU5BU5D_t1709987734* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t75641268, ___valueSlots_7)); }
	inline CameraU5BU5D_t1709987734* get_valueSlots_7() const { return ___valueSlots_7; }
	inline CameraU5BU5D_t1709987734** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(CameraU5BU5D_t1709987734* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t75641268, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t75641268, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t75641268, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t75641268, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t75641268, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t75641268, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t75641268, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t75641268_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t919535928 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t75641268_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t919535928 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t919535928 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t919535928 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T75641268_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef KEYVALUEPAIR_2_T2473313435_H
#define KEYVALUEPAIR_2_T2473313435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,UnityEngine.Camera>
struct  KeyValuePair_2_t2473313435 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	Camera_t4157153871 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	Camera_t4157153871 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2473313435, ___key_0)); }
	inline Camera_t4157153871 * get_key_0() const { return ___key_0; }
	inline Camera_t4157153871 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(Camera_t4157153871 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2473313435, ___value_1)); }
	inline Camera_t4157153871 * get_value_1() const { return ___value_1; }
	inline Camera_t4157153871 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Camera_t4157153871 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2473313435_H
#ifndef DOUBLE_T594665363_H
#define DOUBLE_T594665363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t594665363 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t594665363, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T594665363_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef KEYVALUEPAIR_2_T2530217319_H
#define KEYVALUEPAIR_2_T2530217319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct  KeyValuePair_2_t2530217319 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2530217319, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2530217319, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2530217319_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef ENUMERATOR_T2086727927_H
#define ENUMERATOR_T2086727927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
struct  Enumerator_t2086727927 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t132545152 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t2530217319  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t2086727927, ___dictionary_0)); }
	inline Dictionary_2_t132545152 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t132545152 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t132545152 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2086727927, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t2086727927, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2086727927, ___current_3)); }
	inline KeyValuePair_2_t2530217319  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2530217319 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2530217319  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2086727927_H
#ifndef WATERMODE_T293960580_H
#define WATERMODE_T293960580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.Water/WaterMode
struct  WaterMode_t293960580 
{
public:
	// System.Int32 UnityStandardAssets.Water.Water/WaterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WaterMode_t293960580, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERMODE_T293960580_H
#ifndef DEPTHTEXTUREMODE_T4161834719_H
#define DEPTHTEXTUREMODE_T4161834719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DepthTextureMode
struct  DepthTextureMode_t4161834719 
{
public:
	// System.Int32 UnityEngine.DepthTextureMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DepthTextureMode_t4161834719, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHTEXTUREMODE_T4161834719_H
#ifndef RENDERINGPATH_T883966888_H
#define RENDERINGPATH_T883966888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderingPath
struct  RenderingPath_t883966888 
{
public:
	// System.Int32 UnityEngine.RenderingPath::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderingPath_t883966888, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERINGPATH_T883966888_H
#ifndef ENUMERATOR_T2029824043_H
#define ENUMERATOR_T2029824043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,UnityEngine.Camera>
struct  Enumerator_t2029824043 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t75641268 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t2473313435  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t2029824043, ___dictionary_0)); }
	inline Dictionary_2_t75641268 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t75641268 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t75641268 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2029824043, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t2029824043, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2029824043, ___current_3)); }
	inline KeyValuePair_2_t2473313435  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2473313435 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2473313435  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2029824043_H
#ifndef HIDEFLAGS_T4250555765_H
#define HIDEFLAGS_T4250555765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HideFlags
struct  HideFlags_t4250555765 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HideFlags_t4250555765, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEFLAGS_T4250555765_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef CAMERACLEARFLAGS_T2362496923_H
#define CAMERACLEARFLAGS_T2362496923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CameraClearFlags
struct  CameraClearFlags_t2362496923 
{
public:
	// System.Int32 UnityEngine.CameraClearFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraClearFlags_t2362496923, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACLEARFLAGS_T2362496923_H
#ifndef RUNTIMETYPEHANDLE_T3027515415_H
#define RUNTIMETYPEHANDLE_T3027515415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3027515415 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3027515415, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3027515415_H
#ifndef WATERQUALITY_T1541080576_H
#define WATERQUALITY_T1541080576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.WaterQuality
struct  WaterQuality_t1541080576 
{
public:
	// System.Int32 UnityStandardAssets.Water.WaterQuality::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WaterQuality_t1541080576, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERQUALITY_T1541080576_H
#ifndef RENDERTEXTUREFORMAT_T962350765_H
#define RENDERTEXTUREFORMAT_T962350765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureFormat
struct  RenderTextureFormat_t962350765 
{
public:
	// System.Int32 UnityEngine.RenderTextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderTextureFormat_t962350765, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREFORMAT_T962350765_H
#ifndef MESH_T3648964284_H
#define MESH_T3648964284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Mesh
struct  Mesh_t3648964284  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_T3648964284_H
#ifndef SHADER_T4151988712_H
#define SHADER_T4151988712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Shader
struct  Shader_t4151988712  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADER_T4151988712_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3027515415  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t3027515415  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t3027515415  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t426314064 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t426314064 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t426314064 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t426314064 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t426314064 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t426314064 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t426314064 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t426314064 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t426314064 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t426314064 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t426314064 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t426314064 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef MATERIAL_T340375123_H
#define MATERIAL_T340375123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t340375123  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T340375123_H
#ifndef TEXTURE_T3661962703_H
#define TEXTURE_T3661962703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t3661962703  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T3661962703_H
#ifndef RENDERTEXTURE_T2108887433_H
#define RENDERTEXTURE_T2108887433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTexture
struct  RenderTexture_t2108887433  : public Texture_t3661962703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTURE_T2108887433_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef RENDERER_T2627027031_H
#define RENDERER_T2627027031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t2627027031  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T2627027031_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef CAMERA_T4157153871_H
#define CAMERA_T4157153871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t4157153871  : public Behaviour_t1437897464
{
public:

public:
};

struct Camera_t4157153871_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t190067161 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t190067161 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t190067161 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t190067161 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t190067161 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t190067161 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t190067161 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t190067161 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t190067161 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t190067161 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t190067161 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t190067161 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T4157153871_H
#ifndef FLARELAYER_T1739223323_H
#define FLARELAYER_T1739223323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FlareLayer
struct  FlareLayer_t1739223323  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLARELAYER_T1739223323_H
#ifndef SKYBOX_T2662837510_H
#define SKYBOX_T2662837510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Skybox
struct  Skybox_t2662837510  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKYBOX_T2662837510_H
#ifndef DISPLACE_T1352130581_H
#define DISPLACE_T1352130581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.Displace
struct  Displace_t1352130581  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLACE_T1352130581_H
#ifndef WATER_T936588004_H
#define WATER_T936588004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.Water
struct  Water_t936588004  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Water.Water/WaterMode UnityStandardAssets.Water.Water::waterMode
	int32_t ___waterMode_2;
	// System.Boolean UnityStandardAssets.Water.Water::disablePixelLights
	bool ___disablePixelLights_3;
	// System.Int32 UnityStandardAssets.Water.Water::textureSize
	int32_t ___textureSize_4;
	// System.Single UnityStandardAssets.Water.Water::clipPlaneOffset
	float ___clipPlaneOffset_5;
	// UnityEngine.LayerMask UnityStandardAssets.Water.Water::reflectLayers
	LayerMask_t3493934918  ___reflectLayers_6;
	// UnityEngine.LayerMask UnityStandardAssets.Water.Water::refractLayers
	LayerMask_t3493934918  ___refractLayers_7;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera> UnityStandardAssets.Water.Water::m_ReflectionCameras
	Dictionary_2_t75641268 * ___m_ReflectionCameras_8;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera> UnityStandardAssets.Water.Water::m_RefractionCameras
	Dictionary_2_t75641268 * ___m_RefractionCameras_9;
	// UnityEngine.RenderTexture UnityStandardAssets.Water.Water::m_ReflectionTexture
	RenderTexture_t2108887433 * ___m_ReflectionTexture_10;
	// UnityEngine.RenderTexture UnityStandardAssets.Water.Water::m_RefractionTexture
	RenderTexture_t2108887433 * ___m_RefractionTexture_11;
	// UnityStandardAssets.Water.Water/WaterMode UnityStandardAssets.Water.Water::m_HardwareWaterSupport
	int32_t ___m_HardwareWaterSupport_12;
	// System.Int32 UnityStandardAssets.Water.Water::m_OldReflectionTextureSize
	int32_t ___m_OldReflectionTextureSize_13;
	// System.Int32 UnityStandardAssets.Water.Water::m_OldRefractionTextureSize
	int32_t ___m_OldRefractionTextureSize_14;

public:
	inline static int32_t get_offset_of_waterMode_2() { return static_cast<int32_t>(offsetof(Water_t936588004, ___waterMode_2)); }
	inline int32_t get_waterMode_2() const { return ___waterMode_2; }
	inline int32_t* get_address_of_waterMode_2() { return &___waterMode_2; }
	inline void set_waterMode_2(int32_t value)
	{
		___waterMode_2 = value;
	}

	inline static int32_t get_offset_of_disablePixelLights_3() { return static_cast<int32_t>(offsetof(Water_t936588004, ___disablePixelLights_3)); }
	inline bool get_disablePixelLights_3() const { return ___disablePixelLights_3; }
	inline bool* get_address_of_disablePixelLights_3() { return &___disablePixelLights_3; }
	inline void set_disablePixelLights_3(bool value)
	{
		___disablePixelLights_3 = value;
	}

	inline static int32_t get_offset_of_textureSize_4() { return static_cast<int32_t>(offsetof(Water_t936588004, ___textureSize_4)); }
	inline int32_t get_textureSize_4() const { return ___textureSize_4; }
	inline int32_t* get_address_of_textureSize_4() { return &___textureSize_4; }
	inline void set_textureSize_4(int32_t value)
	{
		___textureSize_4 = value;
	}

	inline static int32_t get_offset_of_clipPlaneOffset_5() { return static_cast<int32_t>(offsetof(Water_t936588004, ___clipPlaneOffset_5)); }
	inline float get_clipPlaneOffset_5() const { return ___clipPlaneOffset_5; }
	inline float* get_address_of_clipPlaneOffset_5() { return &___clipPlaneOffset_5; }
	inline void set_clipPlaneOffset_5(float value)
	{
		___clipPlaneOffset_5 = value;
	}

	inline static int32_t get_offset_of_reflectLayers_6() { return static_cast<int32_t>(offsetof(Water_t936588004, ___reflectLayers_6)); }
	inline LayerMask_t3493934918  get_reflectLayers_6() const { return ___reflectLayers_6; }
	inline LayerMask_t3493934918 * get_address_of_reflectLayers_6() { return &___reflectLayers_6; }
	inline void set_reflectLayers_6(LayerMask_t3493934918  value)
	{
		___reflectLayers_6 = value;
	}

	inline static int32_t get_offset_of_refractLayers_7() { return static_cast<int32_t>(offsetof(Water_t936588004, ___refractLayers_7)); }
	inline LayerMask_t3493934918  get_refractLayers_7() const { return ___refractLayers_7; }
	inline LayerMask_t3493934918 * get_address_of_refractLayers_7() { return &___refractLayers_7; }
	inline void set_refractLayers_7(LayerMask_t3493934918  value)
	{
		___refractLayers_7 = value;
	}

	inline static int32_t get_offset_of_m_ReflectionCameras_8() { return static_cast<int32_t>(offsetof(Water_t936588004, ___m_ReflectionCameras_8)); }
	inline Dictionary_2_t75641268 * get_m_ReflectionCameras_8() const { return ___m_ReflectionCameras_8; }
	inline Dictionary_2_t75641268 ** get_address_of_m_ReflectionCameras_8() { return &___m_ReflectionCameras_8; }
	inline void set_m_ReflectionCameras_8(Dictionary_2_t75641268 * value)
	{
		___m_ReflectionCameras_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReflectionCameras_8), value);
	}

	inline static int32_t get_offset_of_m_RefractionCameras_9() { return static_cast<int32_t>(offsetof(Water_t936588004, ___m_RefractionCameras_9)); }
	inline Dictionary_2_t75641268 * get_m_RefractionCameras_9() const { return ___m_RefractionCameras_9; }
	inline Dictionary_2_t75641268 ** get_address_of_m_RefractionCameras_9() { return &___m_RefractionCameras_9; }
	inline void set_m_RefractionCameras_9(Dictionary_2_t75641268 * value)
	{
		___m_RefractionCameras_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RefractionCameras_9), value);
	}

	inline static int32_t get_offset_of_m_ReflectionTexture_10() { return static_cast<int32_t>(offsetof(Water_t936588004, ___m_ReflectionTexture_10)); }
	inline RenderTexture_t2108887433 * get_m_ReflectionTexture_10() const { return ___m_ReflectionTexture_10; }
	inline RenderTexture_t2108887433 ** get_address_of_m_ReflectionTexture_10() { return &___m_ReflectionTexture_10; }
	inline void set_m_ReflectionTexture_10(RenderTexture_t2108887433 * value)
	{
		___m_ReflectionTexture_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReflectionTexture_10), value);
	}

	inline static int32_t get_offset_of_m_RefractionTexture_11() { return static_cast<int32_t>(offsetof(Water_t936588004, ___m_RefractionTexture_11)); }
	inline RenderTexture_t2108887433 * get_m_RefractionTexture_11() const { return ___m_RefractionTexture_11; }
	inline RenderTexture_t2108887433 ** get_address_of_m_RefractionTexture_11() { return &___m_RefractionTexture_11; }
	inline void set_m_RefractionTexture_11(RenderTexture_t2108887433 * value)
	{
		___m_RefractionTexture_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_RefractionTexture_11), value);
	}

	inline static int32_t get_offset_of_m_HardwareWaterSupport_12() { return static_cast<int32_t>(offsetof(Water_t936588004, ___m_HardwareWaterSupport_12)); }
	inline int32_t get_m_HardwareWaterSupport_12() const { return ___m_HardwareWaterSupport_12; }
	inline int32_t* get_address_of_m_HardwareWaterSupport_12() { return &___m_HardwareWaterSupport_12; }
	inline void set_m_HardwareWaterSupport_12(int32_t value)
	{
		___m_HardwareWaterSupport_12 = value;
	}

	inline static int32_t get_offset_of_m_OldReflectionTextureSize_13() { return static_cast<int32_t>(offsetof(Water_t936588004, ___m_OldReflectionTextureSize_13)); }
	inline int32_t get_m_OldReflectionTextureSize_13() const { return ___m_OldReflectionTextureSize_13; }
	inline int32_t* get_address_of_m_OldReflectionTextureSize_13() { return &___m_OldReflectionTextureSize_13; }
	inline void set_m_OldReflectionTextureSize_13(int32_t value)
	{
		___m_OldReflectionTextureSize_13 = value;
	}

	inline static int32_t get_offset_of_m_OldRefractionTextureSize_14() { return static_cast<int32_t>(offsetof(Water_t936588004, ___m_OldRefractionTextureSize_14)); }
	inline int32_t get_m_OldRefractionTextureSize_14() const { return ___m_OldRefractionTextureSize_14; }
	inline int32_t* get_address_of_m_OldRefractionTextureSize_14() { return &___m_OldRefractionTextureSize_14; }
	inline void set_m_OldRefractionTextureSize_14(int32_t value)
	{
		___m_OldRefractionTextureSize_14 = value;
	}
};

struct Water_t936588004_StaticFields
{
public:
	// System.Boolean UnityStandardAssets.Water.Water::s_InsideWater
	bool ___s_InsideWater_15;

public:
	inline static int32_t get_offset_of_s_InsideWater_15() { return static_cast<int32_t>(offsetof(Water_t936588004_StaticFields, ___s_InsideWater_15)); }
	inline bool get_s_InsideWater_15() const { return ___s_InsideWater_15; }
	inline bool* get_address_of_s_InsideWater_15() { return &___s_InsideWater_15; }
	inline void set_s_InsideWater_15(bool value)
	{
		___s_InsideWater_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATER_T936588004_H
#ifndef WATERBASE_T3883863317_H
#define WATERBASE_T3883863317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.WaterBase
struct  WaterBase_t3883863317  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Material UnityStandardAssets.Water.WaterBase::sharedMaterial
	Material_t340375123 * ___sharedMaterial_2;
	// UnityStandardAssets.Water.WaterQuality UnityStandardAssets.Water.WaterBase::waterQuality
	int32_t ___waterQuality_3;
	// System.Boolean UnityStandardAssets.Water.WaterBase::edgeBlend
	bool ___edgeBlend_4;

public:
	inline static int32_t get_offset_of_sharedMaterial_2() { return static_cast<int32_t>(offsetof(WaterBase_t3883863317, ___sharedMaterial_2)); }
	inline Material_t340375123 * get_sharedMaterial_2() const { return ___sharedMaterial_2; }
	inline Material_t340375123 ** get_address_of_sharedMaterial_2() { return &___sharedMaterial_2; }
	inline void set_sharedMaterial_2(Material_t340375123 * value)
	{
		___sharedMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___sharedMaterial_2), value);
	}

	inline static int32_t get_offset_of_waterQuality_3() { return static_cast<int32_t>(offsetof(WaterBase_t3883863317, ___waterQuality_3)); }
	inline int32_t get_waterQuality_3() const { return ___waterQuality_3; }
	inline int32_t* get_address_of_waterQuality_3() { return &___waterQuality_3; }
	inline void set_waterQuality_3(int32_t value)
	{
		___waterQuality_3 = value;
	}

	inline static int32_t get_offset_of_edgeBlend_4() { return static_cast<int32_t>(offsetof(WaterBase_t3883863317, ___edgeBlend_4)); }
	inline bool get_edgeBlend_4() const { return ___edgeBlend_4; }
	inline bool* get_address_of_edgeBlend_4() { return &___edgeBlend_4; }
	inline void set_edgeBlend_4(bool value)
	{
		___edgeBlend_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERBASE_T3883863317_H
#ifndef PLANARREFLECTION_T439636033_H
#define PLANARREFLECTION_T439636033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.PlanarReflection
struct  PlanarReflection_t439636033  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.LayerMask UnityStandardAssets.Water.PlanarReflection::reflectionMask
	LayerMask_t3493934918  ___reflectionMask_2;
	// System.Boolean UnityStandardAssets.Water.PlanarReflection::reflectSkybox
	bool ___reflectSkybox_3;
	// UnityEngine.Color UnityStandardAssets.Water.PlanarReflection::clearColor
	Color_t2555686324  ___clearColor_4;
	// System.String UnityStandardAssets.Water.PlanarReflection::reflectionSampler
	String_t* ___reflectionSampler_5;
	// System.Single UnityStandardAssets.Water.PlanarReflection::clipPlaneOffset
	float ___clipPlaneOffset_6;
	// UnityEngine.Vector3 UnityStandardAssets.Water.PlanarReflection::m_Oldpos
	Vector3_t3722313464  ___m_Oldpos_7;
	// UnityEngine.Camera UnityStandardAssets.Water.PlanarReflection::m_ReflectionCamera
	Camera_t4157153871 * ___m_ReflectionCamera_8;
	// UnityEngine.Material UnityStandardAssets.Water.PlanarReflection::m_SharedMaterial
	Material_t340375123 * ___m_SharedMaterial_9;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,System.Boolean> UnityStandardAssets.Water.PlanarReflection::m_HelperCameras
	Dictionary_2_t310742658 * ___m_HelperCameras_10;

public:
	inline static int32_t get_offset_of_reflectionMask_2() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___reflectionMask_2)); }
	inline LayerMask_t3493934918  get_reflectionMask_2() const { return ___reflectionMask_2; }
	inline LayerMask_t3493934918 * get_address_of_reflectionMask_2() { return &___reflectionMask_2; }
	inline void set_reflectionMask_2(LayerMask_t3493934918  value)
	{
		___reflectionMask_2 = value;
	}

	inline static int32_t get_offset_of_reflectSkybox_3() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___reflectSkybox_3)); }
	inline bool get_reflectSkybox_3() const { return ___reflectSkybox_3; }
	inline bool* get_address_of_reflectSkybox_3() { return &___reflectSkybox_3; }
	inline void set_reflectSkybox_3(bool value)
	{
		___reflectSkybox_3 = value;
	}

	inline static int32_t get_offset_of_clearColor_4() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___clearColor_4)); }
	inline Color_t2555686324  get_clearColor_4() const { return ___clearColor_4; }
	inline Color_t2555686324 * get_address_of_clearColor_4() { return &___clearColor_4; }
	inline void set_clearColor_4(Color_t2555686324  value)
	{
		___clearColor_4 = value;
	}

	inline static int32_t get_offset_of_reflectionSampler_5() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___reflectionSampler_5)); }
	inline String_t* get_reflectionSampler_5() const { return ___reflectionSampler_5; }
	inline String_t** get_address_of_reflectionSampler_5() { return &___reflectionSampler_5; }
	inline void set_reflectionSampler_5(String_t* value)
	{
		___reflectionSampler_5 = value;
		Il2CppCodeGenWriteBarrier((&___reflectionSampler_5), value);
	}

	inline static int32_t get_offset_of_clipPlaneOffset_6() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___clipPlaneOffset_6)); }
	inline float get_clipPlaneOffset_6() const { return ___clipPlaneOffset_6; }
	inline float* get_address_of_clipPlaneOffset_6() { return &___clipPlaneOffset_6; }
	inline void set_clipPlaneOffset_6(float value)
	{
		___clipPlaneOffset_6 = value;
	}

	inline static int32_t get_offset_of_m_Oldpos_7() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___m_Oldpos_7)); }
	inline Vector3_t3722313464  get_m_Oldpos_7() const { return ___m_Oldpos_7; }
	inline Vector3_t3722313464 * get_address_of_m_Oldpos_7() { return &___m_Oldpos_7; }
	inline void set_m_Oldpos_7(Vector3_t3722313464  value)
	{
		___m_Oldpos_7 = value;
	}

	inline static int32_t get_offset_of_m_ReflectionCamera_8() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___m_ReflectionCamera_8)); }
	inline Camera_t4157153871 * get_m_ReflectionCamera_8() const { return ___m_ReflectionCamera_8; }
	inline Camera_t4157153871 ** get_address_of_m_ReflectionCamera_8() { return &___m_ReflectionCamera_8; }
	inline void set_m_ReflectionCamera_8(Camera_t4157153871 * value)
	{
		___m_ReflectionCamera_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReflectionCamera_8), value);
	}

	inline static int32_t get_offset_of_m_SharedMaterial_9() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___m_SharedMaterial_9)); }
	inline Material_t340375123 * get_m_SharedMaterial_9() const { return ___m_SharedMaterial_9; }
	inline Material_t340375123 ** get_address_of_m_SharedMaterial_9() { return &___m_SharedMaterial_9; }
	inline void set_m_SharedMaterial_9(Material_t340375123 * value)
	{
		___m_SharedMaterial_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_SharedMaterial_9), value);
	}

	inline static int32_t get_offset_of_m_HelperCameras_10() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___m_HelperCameras_10)); }
	inline Dictionary_2_t310742658 * get_m_HelperCameras_10() const { return ___m_HelperCameras_10; }
	inline Dictionary_2_t310742658 ** get_address_of_m_HelperCameras_10() { return &___m_HelperCameras_10; }
	inline void set_m_HelperCameras_10(Dictionary_2_t310742658 * value)
	{
		___m_HelperCameras_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_HelperCameras_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANARREFLECTION_T439636033_H
#ifndef SPECULARLIGHTING_T2163114238_H
#define SPECULARLIGHTING_T2163114238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.SpecularLighting
struct  SpecularLighting_t2163114238  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UnityStandardAssets.Water.SpecularLighting::specularLight
	Transform_t3600365921 * ___specularLight_2;
	// UnityStandardAssets.Water.WaterBase UnityStandardAssets.Water.SpecularLighting::m_WaterBase
	WaterBase_t3883863317 * ___m_WaterBase_3;

public:
	inline static int32_t get_offset_of_specularLight_2() { return static_cast<int32_t>(offsetof(SpecularLighting_t2163114238, ___specularLight_2)); }
	inline Transform_t3600365921 * get_specularLight_2() const { return ___specularLight_2; }
	inline Transform_t3600365921 ** get_address_of_specularLight_2() { return &___specularLight_2; }
	inline void set_specularLight_2(Transform_t3600365921 * value)
	{
		___specularLight_2 = value;
		Il2CppCodeGenWriteBarrier((&___specularLight_2), value);
	}

	inline static int32_t get_offset_of_m_WaterBase_3() { return static_cast<int32_t>(offsetof(SpecularLighting_t2163114238, ___m_WaterBase_3)); }
	inline WaterBase_t3883863317 * get_m_WaterBase_3() const { return ___m_WaterBase_3; }
	inline WaterBase_t3883863317 ** get_address_of_m_WaterBase_3() { return &___m_WaterBase_3; }
	inline void set_m_WaterBase_3(WaterBase_t3883863317 * value)
	{
		___m_WaterBase_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_WaterBase_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECULARLIGHTING_T2163114238_H
#ifndef WATERTILE_T2536246541_H
#define WATERTILE_T2536246541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.WaterTile
struct  WaterTile_t2536246541  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Water.PlanarReflection UnityStandardAssets.Water.WaterTile::reflection
	PlanarReflection_t439636033 * ___reflection_2;
	// UnityStandardAssets.Water.WaterBase UnityStandardAssets.Water.WaterTile::waterBase
	WaterBase_t3883863317 * ___waterBase_3;

public:
	inline static int32_t get_offset_of_reflection_2() { return static_cast<int32_t>(offsetof(WaterTile_t2536246541, ___reflection_2)); }
	inline PlanarReflection_t439636033 * get_reflection_2() const { return ___reflection_2; }
	inline PlanarReflection_t439636033 ** get_address_of_reflection_2() { return &___reflection_2; }
	inline void set_reflection_2(PlanarReflection_t439636033 * value)
	{
		___reflection_2 = value;
		Il2CppCodeGenWriteBarrier((&___reflection_2), value);
	}

	inline static int32_t get_offset_of_waterBase_3() { return static_cast<int32_t>(offsetof(WaterTile_t2536246541, ___waterBase_3)); }
	inline WaterBase_t3883863317 * get_waterBase_3() const { return ___waterBase_3; }
	inline WaterBase_t3883863317 ** get_address_of_waterBase_3() { return &___waterBase_3; }
	inline void set_waterBase_3(WaterBase_t3883863317 * value)
	{
		___waterBase_3 = value;
		Il2CppCodeGenWriteBarrier((&___waterBase_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERTILE_T2536246541_H
#ifndef GERSTNERDISPLACE_T2537102777_H
#define GERSTNERDISPLACE_T2537102777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.GerstnerDisplace
struct  GerstnerDisplace_t2537102777  : public Displace_t1352130581
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GERSTNERDISPLACE_T2537102777_H
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t3722313464  m_Items[1];

public:
	inline Vector3_t3722313464  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t3722313464 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t3722313464  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t3722313464  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t3722313464 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t3722313464  value)
	{
		m_Items[index] = value;
	}
};
// System.Type[]
struct TypeU5BU5D_t3940880105  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t2843939325  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor()
extern "C"  void Dictionary_2__ctor_m236774955_gshared (Dictionary_2_t1444694249 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ContainsKey(!0)
extern "C"  bool Dictionary_2_ContainsKey_m1703962396_gshared (Dictionary_2_t1444694249 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m4262304220_gshared (Dictionary_2_t1444694249 * __this, RuntimeObject * p0, bool p1, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Item(!0)
extern "C"  bool Dictionary_2_get_Item_m872229925_gshared (Dictionary_2_t1444694249 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m351745166_gshared (Dictionary_2_t1444694249 * __this, RuntimeObject * p0, bool p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Clear()
extern "C"  void Dictionary_2_Clear_m3572306323_gshared (Dictionary_2_t1444694249 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m518943619_gshared (Dictionary_2_t132545152 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2086727927  Dictionary_2_GetEnumerator_m3278257048_gshared (Dictionary_2_t132545152 * __this, const RuntimeMethod* method);
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t2530217319  Enumerator_get_Current_m2655181939_gshared (Enumerator_t2086727927 * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m3464904234_gshared (KeyValuePair_2_t2530217319 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1107569389_gshared (Enumerator_t2086727927 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3885012575_gshared (Enumerator_t2086727927 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m1938428402_gshared (Dictionary_2_t132545152 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m3280774074_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, RuntimeObject ** p1, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m3474379962_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C"  bool Behaviour_get_enabled_m753527255 (Behaviour_t1437897464 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.Displace::OnEnable()
extern "C"  void Displace_OnEnable_m2262001474 (Displace_t1352130581 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.Displace::OnDisable()
extern "C"  void Displace_OnDisable_m3700756168 (Displace_t1352130581 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::EnableKeyword(System.String)
extern "C"  void Shader_EnableKeyword_m3103559844 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::DisableKeyword(System.String)
extern "C"  void Shader_DisableKeyword_m433641454 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.Displace::.ctor()
extern "C"  void Displace__ctor_m2244844273 (Displace_t1352130581 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine.Mesh::get_vertices()
extern "C"  Vector3U5BU5D_t1718750761* Mesh_get_vertices_m3585684815 (Mesh_t3648964284 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine.Mesh::get_normals()
extern "C"  Vector3U5BU5D_t1718750761* Mesh_get_normals_m4099615978 (Mesh_t3648964284 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])
extern "C"  void Mesh_set_vertices_m2084450642 (Mesh_t3648964284 * __this, Vector3U5BU5D_t1718750761* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_normals(UnityEngine.Vector3[])
extern "C"  void Mesh_set_normals_m332514528 (Mesh_t3648964284 * __this, Vector3U5BU5D_t1718750761* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_grey()
extern "C"  Color_t2555686324  Color_get_grey_m3440705476 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m1620074514 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
extern "C"  Component_t1923634451 * GameObject_GetComponent_m1027872079 (GameObject_t1113636619 * __this, Type_t * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m4211327027 (Object_t631007953 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m3755062657 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C"  GameObject_t1113636619 * GameObject_Find_m2032535176 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m3574996620 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::.ctor(System.String,System.Type[])
extern "C"  void GameObject__ctor_m1350607670 (GameObject_t1113636619 * __this, String_t* p0, TypeU5BU5D_t3940880105* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::AddComponent(System.Type)
extern "C"  Component_t1923634451 * GameObject_AddComponent_m136524825 (GameObject_t1113636619 * __this, Type_t * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Camera>()
#define GameObject_GetComponent_TisCamera_t4157153871_m1507399110(__this, method) ((  Camera_t4157153871 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void UnityEngine.Camera::set_backgroundColor(UnityEngine.Color)
extern "C"  void Camera_set_backgroundColor_m1332346802 (Camera_t4157153871 * __this, Color_t2555686324  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_clearFlags(UnityEngine.CameraClearFlags)
extern "C"  void Camera_set_clearFlags_m2207032996 (Camera_t4157153871 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.PlanarReflection::SetStandardCameraParameter(UnityEngine.Camera,UnityEngine.LayerMask)
extern "C"  void PlanarReflection_SetStandardCameraParameter_m3135024219 (PlanarReflection_t439636033 * __this, Camera_t4157153871 * ___cam0, LayerMask_t3493934918  ___mask1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
extern "C"  RenderTexture_t2108887433 * Camera_get_targetTexture_m2278634983 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderTexture UnityStandardAssets.Water.PlanarReflection::CreateTextureFor(UnityEngine.Camera)
extern "C"  RenderTexture_t2108887433 * PlanarReflection_CreateTextureFor_m3687076934 (PlanarReflection_t439636033 * __this, Camera_t4157153871 * ___cam0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_targetTexture(UnityEngine.RenderTexture)
extern "C"  void Camera_set_targetTexture_m3148311140 (Camera_t4157153871 * __this, RenderTexture_t2108887433 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.LayerMask::op_Implicit(UnityEngine.LayerMask)
extern "C"  int32_t LayerMask_op_Implicit_m3296792737 (RuntimeObject * __this /* static, unused */, LayerMask_t3493934918  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.LayerMask::NameToLayer(System.String)
extern "C"  int32_t LayerMask_NameToLayer_m2359665122 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_cullingMask(System.Int32)
extern "C"  void Camera_set_cullingMask_m1402455777 (Camera_t4157153871 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_black()
extern "C"  Color_t2555686324  Color_get_black_m719512684 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C"  void Behaviour_set_enabled_m20417929 (Behaviour_t1437897464 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Camera::get_pixelWidth()
extern "C"  int32_t Camera_get_pixelWidth_m1110053668 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mathf::FloorToInt(System.Single)
extern "C"  int32_t Mathf_FloorToInt_m1870542928 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Camera::get_pixelHeight()
extern "C"  int32_t Camera_get_pixelHeight_m722276884 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32)
extern "C"  void RenderTexture__ctor_m769234016 (RenderTexture_t2108887433 * __this, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C"  void Object_set_hideFlags_m1648752846 (Object_t631007953 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Camera,System.Boolean>::.ctor()
#define Dictionary_2__ctor_m1064274257(__this, method) ((  void (*) (Dictionary_2_t310742658 *, const RuntimeMethod*))Dictionary_2__ctor_m236774955_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Camera,System.Boolean>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m1090609320(__this, p0, method) ((  bool (*) (Dictionary_2_t310742658 *, Camera_t4157153871 *, const RuntimeMethod*))Dictionary_2_ContainsKey_m1703962396_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Camera,System.Boolean>::Add(!0,!1)
#define Dictionary_2_Add_m518130851(__this, p0, p1, method) ((  void (*) (Dictionary_2_t310742658 *, Camera_t4157153871 *, bool, const RuntimeMethod*))Dictionary_2_Add_m4262304220_gshared)(__this, p0, p1, method)
// !1 System.Collections.Generic.Dictionary`2<UnityEngine.Camera,System.Boolean>::get_Item(!0)
#define Dictionary_2_get_Item_m3182360459(__this, p0, method) ((  bool (*) (Dictionary_2_t310742658 *, Camera_t4157153871 *, const RuntimeMethod*))Dictionary_2_get_Item_m872229925_gshared)(__this, p0, method)
// UnityEngine.Camera UnityStandardAssets.Water.PlanarReflection::CreateReflectionCameraFor(UnityEngine.Camera)
extern "C"  Camera_t4157153871 * PlanarReflection_CreateReflectionCameraFor_m898764438 (PlanarReflection_t439636033 * __this, Camera_t4157153871 * ___cam0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.PlanarReflection::RenderReflectionFor(UnityEngine.Camera,UnityEngine.Camera)
extern "C"  void PlanarReflection_RenderReflectionFor_m68969975 (PlanarReflection_t439636033 * __this, Camera_t4157153871 * ___cam0, Camera_t4157153871 * ___reflectCamera1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Camera,System.Boolean>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m857652548(__this, p0, p1, method) ((  void (*) (Dictionary_2_t310742658 *, Camera_t4157153871 *, bool, const RuntimeMethod*))Dictionary_2_set_Item_m351745166_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Camera,System.Boolean>::Clear()
#define Dictionary_2_Clear_m4167034391(__this, method) ((  void (*) (Dictionary_2_t310742658 *, const RuntimeMethod*))Dictionary_2_Clear_m3572306323_gshared)(__this, method)
// System.Void UnityStandardAssets.Water.PlanarReflection::RenderHelpCameras(UnityEngine.Camera)
extern "C"  void PlanarReflection_RenderHelpCameras_m2976503157 (PlanarReflection_t439636033 * __this, Camera_t4157153871 * ___currentCam0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetTexture(System.String,UnityEngine.Texture)
extern "C"  void Material_SetTexture_m1829349465 (Material_t340375123 * __this, String_t* p0, Texture_t3661962703 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Material::HasProperty(System.String)
extern "C"  bool Material_HasProperty_m2704238996 (Material_t340375123 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.PlanarReflection::SaneCameraSettings(UnityEngine.Camera)
extern "C"  void PlanarReflection_SaneCameraSettings_m1844908166 (PlanarReflection_t439636033 * __this, Camera_t4157153871 * ___helperCam0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.Component::GetComponent(System.Type)
extern "C"  Component_t1923634451 * Component_GetComponent_m886226392 (Component_t1923634451 * __this, Type_t * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.Skybox::get_material()
extern "C"  Material_t340375123 * Skybox_get_material_m3789022787 (Skybox_t2662837510 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Skybox::set_material(UnityEngine.Material)
extern "C"  void Skybox_set_material_m3176166872 (Skybox_t2662837510 * __this, Material_t340375123 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::set_invertCulling(System.Boolean)
extern "C"  void GL_set_invertCulling_m2740667760 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_eulerAngles()
extern "C"  Vector3_t3722313464  Transform_get_eulerAngles_m2743581774 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m3353183577 (Vector3_t3722313464 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_eulerAngles(UnityEngine.Vector3)
extern "C"  void Transform_set_eulerAngles_m135219616 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t3722313464  Transform_get_position_m36019626 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m3387557959 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_up()
extern "C"  Vector3_t3722313464  Transform_get_up_m3972993886 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Dot_m606404487 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m2498754347 (Vector4_t3319028937 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_zero()
extern "C"  Matrix4x4_t1817901843  Matrix4x4_get_zero_m2898777066 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityStandardAssets.Water.PlanarReflection::CalculateReflectionMatrix(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern "C"  Matrix4x4_t1817901843  PlanarReflection_CalculateReflectionMatrix_m3712858887 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843  ___reflectionMat0, Vector4_t3319028937  ___plane1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Matrix4x4_MultiplyPoint_m1575665487 (Matrix4x4_t1817901843 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Camera::get_worldToCameraMatrix()
extern "C"  Matrix4x4_t1817901843  Camera_get_worldToCameraMatrix_m22661425 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern "C"  Matrix4x4_t1817901843  Matrix4x4_op_Multiply_m1876492807 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843  p0, Matrix4x4_t1817901843  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_worldToCameraMatrix(UnityEngine.Matrix4x4)
extern "C"  void Camera_set_worldToCameraMatrix_m2548466927 (Camera_t4157153871 * __this, Matrix4x4_t1817901843  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityStandardAssets.Water.PlanarReflection::CameraSpacePlane(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector4_t3319028937  PlanarReflection_CameraSpacePlane_m3103867915 (PlanarReflection_t439636033 * __this, Camera_t4157153871 * ___cam0, Vector3_t3722313464  ___pos1, Vector3_t3722313464  ___normal2, float ___sideSign3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Camera::get_projectionMatrix()
extern "C"  Matrix4x4_t1817901843  Camera_get_projectionMatrix_m667780853 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityStandardAssets.Water.PlanarReflection::CalculateObliqueMatrix(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern "C"  Matrix4x4_t1817901843  PlanarReflection_CalculateObliqueMatrix_m3657792474 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843  ___projection0, Vector4_t3319028937  ___clipPlane1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_projectionMatrix(UnityEngine.Matrix4x4)
extern "C"  void Camera_set_projectionMatrix_m3293177686 (Camera_t4157153871 * __this, Matrix4x4_t1817901843  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::Render()
extern "C"  void Camera_Render_m2813253190 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_depthTextureMode(UnityEngine.DepthTextureMode)
extern "C"  void Camera_set_depthTextureMode_m754977860 (Camera_t4157153871 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_renderingPath(UnityEngine.RenderingPath)
extern "C"  void Camera_set_renderingPath_m3563375443 (Camera_t4157153871 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_inverse()
extern "C"  Matrix4x4_t1817901843  Matrix4x4_get_inverse_m1870592360 (Matrix4x4_t1817901843 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityStandardAssets.Water.PlanarReflection::Sgn(System.Single)
extern "C"  float PlanarReflection_Sgn_m3256206255 (RuntimeObject * __this /* static, unused */, float ___a0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern "C"  Vector4_t3319028937  Matrix4x4_op_Multiply_m1839501195 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843  p0, Vector4_t3319028937  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::Dot(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  float Vector4_Dot_m3492158352 (RuntimeObject * __this /* static, unused */, Vector4_t3319028937  p0, Vector4_t3319028937  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::op_Multiply(UnityEngine.Vector4,System.Single)
extern "C"  Vector4_t3319028937  Vector4_op_Multiply_m213790997 (RuntimeObject * __this /* static, unused */, Vector4_t3319028937  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32)
extern "C"  float Matrix4x4_get_Item_m567451091 (Matrix4x4_t1817901843 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Single)
extern "C"  void Matrix4x4_set_Item_m1906605342 (Matrix4x4_t1817901843 * __this, int32_t p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::get_Item(System.Int32)
extern "C"  float Vector4_get_Item_m2380866393 (Vector4_t3319028937 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_op_Multiply_m3376773913 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_Addition_m779775034 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyVector(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Matrix4x4_MultiplyVector_m3808798942 (Matrix4x4_t1817901843 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C"  Vector3_t3722313464  Vector3_get_normalized_m2454957984 (Vector3_t3722313464 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern "C"  Vector3_t3722313464  Transform_get_forward_m747522392 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector4_t3319028937  Vector4_op_Implicit_m2966035112 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetVector(System.String,UnityEngine.Vector4)
extern "C"  void Material_SetVector_m4214217286 (Material_t340375123 * __this, String_t* p0, Vector4_t3319028937  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.LayerMask UnityEngine.LayerMask::op_Implicit(System.Int32)
extern "C"  LayerMask_t3493934918  LayerMask_op_Implicit_m90232283 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera>::.ctor()
#define Dictionary_2__ctor_m579872301(__this, method) ((  void (*) (Dictionary_2_t75641268 *, const RuntimeMethod*))Dictionary_2__ctor_m518943619_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t2627027031_m1858649706(__this, method) ((  Renderer_t2627027031 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// UnityEngine.Material UnityEngine.Renderer::get_sharedMaterial()
extern "C"  Material_t340375123 * Renderer_get_sharedMaterial_m1936632411 (Renderer_t2627027031 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Renderer::get_enabled()
extern "C"  bool Renderer_get_enabled_m3482452518 (Renderer_t2627027031 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_current()
extern "C"  Camera_t4157153871 * Camera_get_current_m929992396 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityStandardAssets.Water.Water/WaterMode UnityStandardAssets.Water.Water::FindHardwareWaterSupport()
extern "C"  int32_t Water_FindHardwareWaterSupport_m3785980915 (Water_t936588004 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityStandardAssets.Water.Water/WaterMode UnityStandardAssets.Water.Water::GetWaterMode()
extern "C"  int32_t Water_GetWaterMode_m1759043735 (Water_t936588004 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.Water::CreateWaterObjects(UnityEngine.Camera,UnityEngine.Camera&,UnityEngine.Camera&)
extern "C"  void Water_CreateWaterObjects_m2240077532 (Water_t936588004 * __this, Camera_t4157153871 * ___currentCamera0, Camera_t4157153871 ** ___reflectionCamera1, Camera_t4157153871 ** ___refractionCamera2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.QualitySettings::get_pixelLightCount()
extern "C"  int32_t QualitySettings_get_pixelLightCount_m3013306133 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::set_pixelLightCount(System.Int32)
extern "C"  void QualitySettings_set_pixelLightCount_m3523654033 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.Water::UpdateCameraModes(UnityEngine.Camera,UnityEngine.Camera)
extern "C"  void Water_UpdateCameraModes_m3658611570 (Water_t936588004 * __this, Camera_t4157153871 * ___src0, Camera_t4157153871 * ___dest1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.Water::CalculateReflectionMatrix(UnityEngine.Matrix4x4&,UnityEngine.Vector4)
extern "C"  void Water_CalculateReflectionMatrix_m175561076 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843 * ___reflectionMat0, Vector4_t3319028937  ___plane1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityStandardAssets.Water.Water::CameraSpacePlane(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector4_t3319028937  Water_CameraSpacePlane_m2332942409 (Water_t936588004 * __this, Camera_t4157153871 * ___cam0, Vector3_t3722313464  ___pos1, Vector3_t3722313464  ___normal2, float ___sideSign3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Camera::CalculateObliqueMatrix(UnityEngine.Vector4)
extern "C"  Matrix4x4_t1817901843  Camera_CalculateObliqueMatrix_m3018791782 (Camera_t4157153871 * __this, Vector4_t3319028937  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_cullingMatrix(UnityEngine.Matrix4x4)
extern "C"  void Camera_set_cullingMatrix_m2348869097 (Camera_t4157153871 * __this, Matrix4x4_t1817901843  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.LayerMask::get_value()
extern "C"  int32_t LayerMask_get_value_m1881709263 (LayerMask_t3493934918 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GL::get_invertCulling()
extern "C"  bool GL_get_invertCulling_m4270974637 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t2301928331  Transform_get_rotation_m3502953881 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_rotation_m3524318132 (Transform_t3600365921 * __this, Quaternion_t2301928331  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern "C"  void Object_DestroyImmediate_m3193525861 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m484651291(__this, method) ((  Enumerator_t2029824043  (*) (Dictionary_2_t75641268 *, const RuntimeMethod*))Dictionary_2_GetEnumerator_m3278257048_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,UnityEngine.Camera>::get_Current()
#define Enumerator_get_Current_m2791492809(__this, method) ((  KeyValuePair_2_t2473313435  (*) (Enumerator_t2029824043 *, const RuntimeMethod*))Enumerator_get_Current_m2655181939_gshared)(__this, method)
// !1 System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,UnityEngine.Camera>::get_Value()
#define KeyValuePair_2_get_Value_m1196865026(__this, method) ((  Camera_t4157153871 * (*) (KeyValuePair_2_t2473313435 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m3464904234_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,UnityEngine.Camera>::MoveNext()
#define Enumerator_MoveNext_m2261968848(__this, method) ((  bool (*) (Enumerator_t2029824043 *, const RuntimeMethod*))Enumerator_MoveNext_m1107569389_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,UnityEngine.Camera>::Dispose()
#define Enumerator_Dispose_m2390683763(__this, method) ((  void (*) (Enumerator_t2029824043 *, const RuntimeMethod*))Enumerator_Dispose_m3885012575_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera>::Clear()
#define Dictionary_2_Clear_m1016216395(__this, method) ((  void (*) (Dictionary_2_t75641268 *, const RuntimeMethod*))Dictionary_2_Clear_m1938428402_gshared)(__this, method)
// UnityEngine.Vector4 UnityEngine.Material::GetVector(System.String)
extern "C"  Vector4_t3319028937  Material_GetVector_m806950826 (Material_t340375123 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Material::GetFloat(System.String)
extern "C"  float Material_GetFloat_m2210875428 (Material_t340375123 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_timeSinceLevelLoad()
extern "C"  float Time_get_timeSinceLevelLoad_m2224611026 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::IEEERemainder(System.Double,System.Double)
extern "C"  double Math_IEEERemainder_m1747102664 (RuntimeObject * __this /* static, unused */, double p0, double p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
extern "C"  int32_t Camera_get_clearFlags_m992534691 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Camera::get_backgroundColor()
extern "C"  Color_t2555686324  Camera_get_backgroundColor_m3310993309 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Skybox>()
#define Component_GetComponent_TisSkybox_t2662837510_m2500560783(__this, method) ((  Skybox_t2662837510 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Single UnityEngine.Camera::get_farClipPlane()
extern "C"  float Camera_get_farClipPlane_m538536689 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_farClipPlane(System.Single)
extern "C"  void Camera_set_farClipPlane_m3828313665 (Camera_t4157153871 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_nearClipPlane()
extern "C"  float Camera_get_nearClipPlane_m837839537 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_nearClipPlane(System.Single)
extern "C"  void Camera_set_nearClipPlane_m3667419702 (Camera_t4157153871 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Camera::get_orthographic()
extern "C"  bool Camera_get_orthographic_m2831464531 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_orthographic(System.Boolean)
extern "C"  void Camera_set_orthographic_m2855749523 (Camera_t4157153871 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_fieldOfView()
extern "C"  float Camera_get_fieldOfView_m1018585504 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_fieldOfView(System.Single)
extern "C"  void Camera_set_fieldOfView_m1438246590 (Camera_t4157153871 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_aspect()
extern "C"  float Camera_get_aspect_m862507514 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_aspect(System.Single)
extern "C"  void Camera_set_aspect_m2625464181 (Camera_t4157153871 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_orthographicSize()
extern "C"  float Camera_get_orthographicSize_m3903216845 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_orthographicSize(System.Single)
extern "C"  void Camera_set_orthographicSize_m76971700 (Camera_t4157153871 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Object::GetInstanceID()
extern "C"  int32_t Object_GetInstanceID_m1255174761 (Object_t631007953 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m904156431 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::set_name(System.String)
extern "C"  void Object_set_name_m291480324 (Object_t631007953 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::set_isPowerOfTwo(System.Boolean)
extern "C"  void RenderTexture_set_isPowerOfTwo_m3873419893 (RenderTexture_t2108887433 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m3746080878(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t75641268 *, Camera_t4157153871 *, Camera_t4157153871 **, const RuntimeMethod*))Dictionary_2_TryGetValue_m3280774074_gshared)(__this, p0, p1, method)
// System.String System.String::Concat(System.Object[])
extern "C"  String_t* String_Concat_m2971454694 (RuntimeObject * __this /* static, unused */, ObjectU5BU5D_t2843939325* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.FlareLayer>()
#define GameObject_AddComponent_TisFlareLayer_t1739223323_m722533630(__this, method) ((  FlareLayer_t1739223323 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m1091284778(__this, p0, p1, method) ((  void (*) (Dictionary_2_t75641268 *, Camera_t4157153871 *, Camera_t4157153871 *, const RuntimeMethod*))Dictionary_2_set_Item_m3474379962_gshared)(__this, p0, p1, method)
// System.String UnityEngine.Material::GetTag(System.String,System.Boolean)
extern "C"  String_t* Material_GetTag_m2091721776 (Material_t340375123 * __this, String_t* p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m920492651 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Shader UnityEngine.Material::get_shader()
extern "C"  Shader_t4151988712 * Material_get_shader_m1331119247 (Material_t340375123 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::set_maximumLOD(System.Int32)
extern "C"  void Shader_set_maximumLOD_m3495846676 (Shader_t4151988712 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SystemInfo::SupportsRenderTextureFormat(UnityEngine.RenderTextureFormat)
extern "C"  bool SystemInfo_SupportsRenderTextureFormat_m1663449629 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t4157153871 * Camera_get_main_m3643453163 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.DepthTextureMode UnityEngine.Camera::get_depthTextureMode()
extern "C"  int32_t Camera_get_depthTextureMode_m871144641 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.WaterBase::UpdateShader()
extern "C"  void WaterBase_UpdateShader_m695718468 (WaterBase_t3883863317 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.WaterTile::AcquireComponents()
extern "C"  void WaterTile_AcquireComponents_m1557480152 (WaterTile_t2536246541 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t3600365921 * Transform_get_parent_m835071599 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityStandardAssets.Water.PlanarReflection>()
#define Component_GetComponent_TisPlanarReflection_t439636033_m1616117542(__this, method) ((  PlanarReflection_t439636033 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityStandardAssets.Water.WaterBase>()
#define Component_GetComponent_TisWaterBase_t3883863317_m2958773954(__this, method) ((  WaterBase_t3883863317 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void UnityStandardAssets.Water.PlanarReflection::WaterTileBeingRendered(UnityEngine.Transform,UnityEngine.Camera)
extern "C"  void PlanarReflection_WaterTileBeingRendered_m1672873199 (PlanarReflection_t439636033 * __this, Transform_t3600365921 * ___tr0, Camera_t4157153871 * ___currentCam1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.WaterBase::WaterTileBeingRendered(UnityEngine.Transform,UnityEngine.Camera)
extern "C"  void WaterBase_WaterTileBeingRendered_m3136753890 (WaterBase_t3883863317 * __this, Transform_t3600365921 * ___tr0, Camera_t4157153871 * ___currentCam1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets.Water.Displace::.ctor()
extern "C"  void Displace__ctor_m2244844273 (Displace_t1352130581 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.Displace::Awake()
extern "C"  void Displace_Awake_m1785564436 (Displace_t1352130581 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = Behaviour_get_enabled_m753527255(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Displace_OnEnable_m2262001474(__this, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_0016:
	{
		Displace_OnDisable_m3700756168(__this, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Water.Displace::OnEnable()
extern "C"  void Displace_OnEnable_m2262001474 (Displace_t1352130581 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Displace_OnEnable_m2262001474_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Shader_EnableKeyword_m3103559844(NULL /*static, unused*/, _stringLiteral3533732601, /*hidden argument*/NULL);
		Shader_DisableKeyword_m433641454(NULL /*static, unused*/, _stringLiteral240773206, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.Displace::OnDisable()
extern "C"  void Displace_OnDisable_m3700756168 (Displace_t1352130581 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Displace_OnDisable_m3700756168_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Shader_EnableKeyword_m3103559844(NULL /*static, unused*/, _stringLiteral240773206, /*hidden argument*/NULL);
		Shader_DisableKeyword_m433641454(NULL /*static, unused*/, _stringLiteral3533732601, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets.Water.GerstnerDisplace::.ctor()
extern "C"  void GerstnerDisplace__ctor_m940758337 (GerstnerDisplace_t2537102777 * __this, const RuntimeMethod* method)
{
	{
		Displace__ctor_m2244844273(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets.Water.MeshContainer::.ctor(UnityEngine.Mesh)
extern "C"  void MeshContainer__ctor_m2119568425 (MeshContainer_t140041298 * __this, Mesh_t3648964284 * ___m0, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		Mesh_t3648964284 * L_0 = ___m0;
		__this->set_mesh_0(L_0);
		Mesh_t3648964284 * L_1 = ___m0;
		NullCheck(L_1);
		Vector3U5BU5D_t1718750761* L_2 = Mesh_get_vertices_m3585684815(L_1, /*hidden argument*/NULL);
		__this->set_vertices_1(L_2);
		Mesh_t3648964284 * L_3 = ___m0;
		NullCheck(L_3);
		Vector3U5BU5D_t1718750761* L_4 = Mesh_get_normals_m4099615978(L_3, /*hidden argument*/NULL);
		__this->set_normals_2(L_4);
		return;
	}
}
// System.Void UnityStandardAssets.Water.MeshContainer::Update()
extern "C"  void MeshContainer_Update_m2904550259 (MeshContainer_t140041298 * __this, const RuntimeMethod* method)
{
	{
		Mesh_t3648964284 * L_0 = __this->get_mesh_0();
		Vector3U5BU5D_t1718750761* L_1 = __this->get_vertices_1();
		NullCheck(L_0);
		Mesh_set_vertices_m2084450642(L_0, L_1, /*hidden argument*/NULL);
		Mesh_t3648964284 * L_2 = __this->get_mesh_0();
		Vector3U5BU5D_t1718750761* L_3 = __this->get_normals_2();
		NullCheck(L_2);
		Mesh_set_normals_m332514528(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets.Water.PlanarReflection::.ctor()
extern "C"  void PlanarReflection__ctor_m4060724367 (PlanarReflection_t439636033 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlanarReflection__ctor_m4060724367_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color_t2555686324  L_0 = Color_get_grey_m3440705476(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_clearColor_4(L_0);
		__this->set_reflectionSampler_5(_stringLiteral253045501);
		__this->set_clipPlaneOffset_6((0.07f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.PlanarReflection::Start()
extern "C"  void PlanarReflection_Start_m1671959525 (PlanarReflection_t439636033 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlanarReflection_Start_m1671959525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		RuntimeTypeHandle_t3027515415  L_1 = { reinterpret_cast<intptr_t> (WaterBase_t3883863317_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t1923634451 * L_3 = GameObject_GetComponent_m1027872079(L_0, L_2, /*hidden argument*/NULL);
		NullCheck(((WaterBase_t3883863317 *)CastclassClass((RuntimeObject*)L_3, WaterBase_t3883863317_il2cpp_TypeInfo_var)));
		Material_t340375123 * L_4 = ((WaterBase_t3883863317 *)CastclassClass((RuntimeObject*)L_3, WaterBase_t3883863317_il2cpp_TypeInfo_var))->get_sharedMaterial_2();
		__this->set_m_SharedMaterial_9(L_4);
		return;
	}
}
// UnityEngine.Camera UnityStandardAssets.Water.PlanarReflection::CreateReflectionCameraFor(UnityEngine.Camera)
extern "C"  Camera_t4157153871 * PlanarReflection_CreateReflectionCameraFor_m898764438 (PlanarReflection_t439636033 * __this, Camera_t4157153871 * ___cam0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlanarReflection_CreateReflectionCameraFor_m898764438_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	GameObject_t1113636619 * V_1 = NULL;
	Camera_t4157153871 * V_2 = NULL;
	Camera_t4157153871 * G_B6_0 = NULL;
	Camera_t4157153871 * G_B5_0 = NULL;
	int32_t G_B7_0 = 0;
	Camera_t4157153871 * G_B7_1 = NULL;
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = Object_get_name_m4211327027(L_0, /*hidden argument*/NULL);
		Camera_t4157153871 * L_2 = ___cam0;
		NullCheck(L_2);
		String_t* L_3 = Object_get_name_m4211327027(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m3755062657(NULL /*static, unused*/, L_1, _stringLiteral2505626915, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		GameObject_t1113636619 * L_6 = GameObject_Find_m2032535176(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		GameObject_t1113636619 * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0048;
		}
	}
	{
		String_t* L_9 = V_0;
		TypeU5BU5D_t3940880105* L_10 = (TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)1);
		TypeU5BU5D_t3940880105* L_11 = L_10;
		RuntimeTypeHandle_t3027515415  L_12 = { reinterpret_cast<intptr_t> (Camera_t4157153871_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_13 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_13);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_13);
		GameObject_t1113636619 * L_14 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m1350607670(L_14, L_9, L_11, /*hidden argument*/NULL);
		V_1 = L_14;
	}

IL_0048:
	{
		GameObject_t1113636619 * L_15 = V_1;
		RuntimeTypeHandle_t3027515415  L_16 = { reinterpret_cast<intptr_t> (Camera_t4157153871_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_17 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		Component_t1923634451 * L_18 = GameObject_GetComponent_m1027872079(L_15, L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0073;
		}
	}
	{
		GameObject_t1113636619 * L_20 = V_1;
		RuntimeTypeHandle_t3027515415  L_21 = { reinterpret_cast<intptr_t> (Camera_t4157153871_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		GameObject_AddComponent_m136524825(L_20, L_22, /*hidden argument*/NULL);
	}

IL_0073:
	{
		GameObject_t1113636619 * L_23 = V_1;
		NullCheck(L_23);
		Camera_t4157153871 * L_24 = GameObject_GetComponent_TisCamera_t4157153871_m1507399110(L_23, /*hidden argument*/GameObject_GetComponent_TisCamera_t4157153871_m1507399110_RuntimeMethod_var);
		V_2 = L_24;
		Camera_t4157153871 * L_25 = V_2;
		Color_t2555686324  L_26 = __this->get_clearColor_4();
		NullCheck(L_25);
		Camera_set_backgroundColor_m1332346802(L_25, L_26, /*hidden argument*/NULL);
		Camera_t4157153871 * L_27 = V_2;
		bool L_28 = __this->get_reflectSkybox_3();
		G_B5_0 = L_27;
		if (!L_28)
		{
			G_B6_0 = L_27;
			goto IL_0098;
		}
	}
	{
		G_B7_0 = 1;
		G_B7_1 = G_B5_0;
		goto IL_0099;
	}

IL_0098:
	{
		G_B7_0 = 2;
		G_B7_1 = G_B6_0;
	}

IL_0099:
	{
		NullCheck(G_B7_1);
		Camera_set_clearFlags_m2207032996(G_B7_1, G_B7_0, /*hidden argument*/NULL);
		Camera_t4157153871 * L_29 = V_2;
		LayerMask_t3493934918  L_30 = __this->get_reflectionMask_2();
		PlanarReflection_SetStandardCameraParameter_m3135024219(__this, L_29, L_30, /*hidden argument*/NULL);
		Camera_t4157153871 * L_31 = V_2;
		NullCheck(L_31);
		RenderTexture_t2108887433 * L_32 = Camera_get_targetTexture_m2278634983(L_31, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_33 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		if (L_33)
		{
			goto IL_00c8;
		}
	}
	{
		Camera_t4157153871 * L_34 = V_2;
		Camera_t4157153871 * L_35 = ___cam0;
		RenderTexture_t2108887433 * L_36 = PlanarReflection_CreateTextureFor_m3687076934(__this, L_35, /*hidden argument*/NULL);
		NullCheck(L_34);
		Camera_set_targetTexture_m3148311140(L_34, L_36, /*hidden argument*/NULL);
	}

IL_00c8:
	{
		Camera_t4157153871 * L_37 = V_2;
		return L_37;
	}
}
// System.Void UnityStandardAssets.Water.PlanarReflection::SetStandardCameraParameter(UnityEngine.Camera,UnityEngine.LayerMask)
extern "C"  void PlanarReflection_SetStandardCameraParameter_m3135024219 (PlanarReflection_t439636033 * __this, Camera_t4157153871 * ___cam0, LayerMask_t3493934918  ___mask1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlanarReflection_SetStandardCameraParameter_m3135024219_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t4157153871 * L_0 = ___cam0;
		LayerMask_t3493934918  L_1 = ___mask1;
		int32_t L_2 = LayerMask_op_Implicit_m3296792737(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_3 = LayerMask_NameToLayer_m2359665122(NULL /*static, unused*/, _stringLiteral78692563, /*hidden argument*/NULL);
		NullCheck(L_0);
		Camera_set_cullingMask_m1402455777(L_0, ((int32_t)((int32_t)L_2&(int32_t)((~((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_3&(int32_t)((int32_t)31))))))))), /*hidden argument*/NULL);
		Camera_t4157153871 * L_4 = ___cam0;
		Color_t2555686324  L_5 = Color_get_black_m719512684(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		Camera_set_backgroundColor_m1332346802(L_4, L_5, /*hidden argument*/NULL);
		Camera_t4157153871 * L_6 = ___cam0;
		NullCheck(L_6);
		Behaviour_set_enabled_m20417929(L_6, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RenderTexture UnityStandardAssets.Water.PlanarReflection::CreateTextureFor(UnityEngine.Camera)
extern "C"  RenderTexture_t2108887433 * PlanarReflection_CreateTextureFor_m3687076934 (PlanarReflection_t439636033 * __this, Camera_t4157153871 * ___cam0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlanarReflection_CreateTextureFor_m3687076934_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RenderTexture_t2108887433 * V_0 = NULL;
	{
		Camera_t4157153871 * L_0 = ___cam0;
		NullCheck(L_0);
		int32_t L_1 = Camera_get_pixelWidth_m1110053668(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_FloorToInt_m1870542928(NULL /*static, unused*/, ((float)il2cpp_codegen_multiply((float)(((float)((float)L_1))), (float)(0.5f))), /*hidden argument*/NULL);
		Camera_t4157153871 * L_3 = ___cam0;
		NullCheck(L_3);
		int32_t L_4 = Camera_get_pixelHeight_m722276884(L_3, /*hidden argument*/NULL);
		int32_t L_5 = Mathf_FloorToInt_m1870542928(NULL /*static, unused*/, ((float)il2cpp_codegen_multiply((float)(((float)((float)L_4))), (float)(0.5f))), /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_6 = (RenderTexture_t2108887433 *)il2cpp_codegen_object_new(RenderTexture_t2108887433_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m769234016(L_6, L_2, L_5, ((int32_t)24), /*hidden argument*/NULL);
		V_0 = L_6;
		RenderTexture_t2108887433 * L_7 = V_0;
		NullCheck(L_7);
		Object_set_hideFlags_m1648752846(L_7, ((int32_t)52), /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_8 = V_0;
		return L_8;
	}
}
// System.Void UnityStandardAssets.Water.PlanarReflection::RenderHelpCameras(UnityEngine.Camera)
extern "C"  void PlanarReflection_RenderHelpCameras_m2976503157 (PlanarReflection_t439636033 * __this, Camera_t4157153871 * ___currentCam0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlanarReflection_RenderHelpCameras_m2976503157_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t310742658 * L_0 = __this->get_m_HelperCameras_10();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Dictionary_2_t310742658 * L_1 = (Dictionary_2_t310742658 *)il2cpp_codegen_object_new(Dictionary_2_t310742658_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1064274257(L_1, /*hidden argument*/Dictionary_2__ctor_m1064274257_RuntimeMethod_var);
		__this->set_m_HelperCameras_10(L_1);
	}

IL_0016:
	{
		Dictionary_2_t310742658 * L_2 = __this->get_m_HelperCameras_10();
		Camera_t4157153871 * L_3 = ___currentCam0;
		NullCheck(L_2);
		bool L_4 = Dictionary_2_ContainsKey_m1090609320(L_2, L_3, /*hidden argument*/Dictionary_2_ContainsKey_m1090609320_RuntimeMethod_var);
		if (L_4)
		{
			goto IL_0034;
		}
	}
	{
		Dictionary_2_t310742658 * L_5 = __this->get_m_HelperCameras_10();
		Camera_t4157153871 * L_6 = ___currentCam0;
		NullCheck(L_5);
		Dictionary_2_Add_m518130851(L_5, L_6, (bool)0, /*hidden argument*/Dictionary_2_Add_m518130851_RuntimeMethod_var);
	}

IL_0034:
	{
		Dictionary_2_t310742658 * L_7 = __this->get_m_HelperCameras_10();
		Camera_t4157153871 * L_8 = ___currentCam0;
		NullCheck(L_7);
		bool L_9 = Dictionary_2_get_Item_m3182360459(L_7, L_8, /*hidden argument*/Dictionary_2_get_Item_m3182360459_RuntimeMethod_var);
		if (!L_9)
		{
			goto IL_0046;
		}
	}
	{
		return;
	}

IL_0046:
	{
		Camera_t4157153871 * L_10 = __this->get_m_ReflectionCamera_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0063;
		}
	}
	{
		Camera_t4157153871 * L_12 = ___currentCam0;
		Camera_t4157153871 * L_13 = PlanarReflection_CreateReflectionCameraFor_m898764438(__this, L_12, /*hidden argument*/NULL);
		__this->set_m_ReflectionCamera_8(L_13);
	}

IL_0063:
	{
		Camera_t4157153871 * L_14 = ___currentCam0;
		Camera_t4157153871 * L_15 = __this->get_m_ReflectionCamera_8();
		PlanarReflection_RenderReflectionFor_m68969975(__this, L_14, L_15, /*hidden argument*/NULL);
		Dictionary_2_t310742658 * L_16 = __this->get_m_HelperCameras_10();
		Camera_t4157153871 * L_17 = ___currentCam0;
		NullCheck(L_16);
		Dictionary_2_set_Item_m857652548(L_16, L_17, (bool)1, /*hidden argument*/Dictionary_2_set_Item_m857652548_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityStandardAssets.Water.PlanarReflection::LateUpdate()
extern "C"  void PlanarReflection_LateUpdate_m53228134 (PlanarReflection_t439636033 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlanarReflection_LateUpdate_m53228134_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t310742658 * L_0 = __this->get_m_HelperCameras_10();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Dictionary_2_t310742658 * L_1 = __this->get_m_HelperCameras_10();
		NullCheck(L_1);
		Dictionary_2_Clear_m4167034391(L_1, /*hidden argument*/Dictionary_2_Clear_m4167034391_RuntimeMethod_var);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Water.PlanarReflection::WaterTileBeingRendered(UnityEngine.Transform,UnityEngine.Camera)
extern "C"  void PlanarReflection_WaterTileBeingRendered_m1672873199 (PlanarReflection_t439636033 * __this, Transform_t3600365921 * ___tr0, Camera_t4157153871 * ___currentCam1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlanarReflection_WaterTileBeingRendered_m1672873199_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t4157153871 * L_0 = ___currentCam1;
		PlanarReflection_RenderHelpCameras_m2976503157(__this, L_0, /*hidden argument*/NULL);
		Camera_t4157153871 * L_1 = __this->get_m_ReflectionCamera_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0043;
		}
	}
	{
		Material_t340375123 * L_3 = __this->get_m_SharedMaterial_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0043;
		}
	}
	{
		Material_t340375123 * L_5 = __this->get_m_SharedMaterial_9();
		String_t* L_6 = __this->get_reflectionSampler_5();
		Camera_t4157153871 * L_7 = __this->get_m_ReflectionCamera_8();
		NullCheck(L_7);
		RenderTexture_t2108887433 * L_8 = Camera_get_targetTexture_m2278634983(L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		Material_SetTexture_m1829349465(L_5, L_6, L_8, /*hidden argument*/NULL);
	}

IL_0043:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Water.PlanarReflection::OnEnable()
extern "C"  void PlanarReflection_OnEnable_m2568423146 (PlanarReflection_t439636033 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlanarReflection_OnEnable_m2568423146_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Shader_EnableKeyword_m3103559844(NULL /*static, unused*/, _stringLiteral2712144963, /*hidden argument*/NULL);
		Shader_DisableKeyword_m433641454(NULL /*static, unused*/, _stringLiteral1050277699, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.PlanarReflection::OnDisable()
extern "C"  void PlanarReflection_OnDisable_m1191548495 (PlanarReflection_t439636033 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlanarReflection_OnDisable_m1191548495_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Shader_EnableKeyword_m3103559844(NULL /*static, unused*/, _stringLiteral1050277699, /*hidden argument*/NULL);
		Shader_DisableKeyword_m433641454(NULL /*static, unused*/, _stringLiteral2712144963, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.PlanarReflection::RenderReflectionFor(UnityEngine.Camera,UnityEngine.Camera)
extern "C"  void PlanarReflection_RenderReflectionFor_m68969975 (PlanarReflection_t439636033 * __this, Camera_t4157153871 * ___cam0, Camera_t4157153871 * ___reflectCamera1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlanarReflection_RenderReflectionFor_m68969975_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Skybox_t2662837510 * V_0 = NULL;
	Transform_t3600365921 * V_1 = NULL;
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	Vector4_t3319028937  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Matrix4x4_t1817901843  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t3722313464  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector4_t3319028937  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Matrix4x4_t1817901843  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t3722313464  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Camera_t4157153871 * G_B7_0 = NULL;
	Camera_t4157153871 * G_B6_0 = NULL;
	int32_t G_B8_0 = 0;
	Camera_t4157153871 * G_B8_1 = NULL;
	{
		Camera_t4157153871 * L_0 = ___reflectCamera1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Material_t340375123 * L_2 = __this->get_m_SharedMaterial_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0033;
		}
	}
	{
		Material_t340375123 * L_4 = __this->get_m_SharedMaterial_9();
		String_t* L_5 = __this->get_reflectionSampler_5();
		NullCheck(L_4);
		bool L_6 = Material_HasProperty_m2704238996(L_4, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0033;
		}
	}
	{
		return;
	}

IL_0033:
	{
		Camera_t4157153871 * L_7 = ___reflectCamera1;
		LayerMask_t3493934918  L_8 = __this->get_reflectionMask_2();
		int32_t L_9 = LayerMask_op_Implicit_m3296792737(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		int32_t L_10 = LayerMask_NameToLayer_m2359665122(NULL /*static, unused*/, _stringLiteral78692563, /*hidden argument*/NULL);
		NullCheck(L_7);
		Camera_set_cullingMask_m1402455777(L_7, ((int32_t)((int32_t)L_9&(int32_t)((~((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_10&(int32_t)((int32_t)31))))))))), /*hidden argument*/NULL);
		Camera_t4157153871 * L_11 = ___reflectCamera1;
		PlanarReflection_SaneCameraSettings_m1844908166(__this, L_11, /*hidden argument*/NULL);
		Camera_t4157153871 * L_12 = ___reflectCamera1;
		Color_t2555686324  L_13 = __this->get_clearColor_4();
		NullCheck(L_12);
		Camera_set_backgroundColor_m1332346802(L_12, L_13, /*hidden argument*/NULL);
		Camera_t4157153871 * L_14 = ___reflectCamera1;
		bool L_15 = __this->get_reflectSkybox_3();
		G_B6_0 = L_14;
		if (!L_15)
		{
			G_B7_0 = L_14;
			goto IL_007a;
		}
	}
	{
		G_B8_0 = 1;
		G_B8_1 = G_B6_0;
		goto IL_007b;
	}

IL_007a:
	{
		G_B8_0 = 2;
		G_B8_1 = G_B7_0;
	}

IL_007b:
	{
		NullCheck(G_B8_1);
		Camera_set_clearFlags_m2207032996(G_B8_1, G_B8_0, /*hidden argument*/NULL);
		bool L_16 = __this->get_reflectSkybox_3();
		if (!L_16)
		{
			goto IL_010b;
		}
	}
	{
		Camera_t4157153871 * L_17 = ___cam0;
		NullCheck(L_17);
		GameObject_t1113636619 * L_18 = Component_get_gameObject_m442555142(L_17, /*hidden argument*/NULL);
		RuntimeTypeHandle_t3027515415  L_19 = { reinterpret_cast<intptr_t> (Skybox_t2662837510_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		Component_t1923634451 * L_21 = GameObject_GetComponent_m1027872079(L_18, L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_010b;
		}
	}
	{
		Camera_t4157153871 * L_23 = ___reflectCamera1;
		NullCheck(L_23);
		GameObject_t1113636619 * L_24 = Component_get_gameObject_m442555142(L_23, /*hidden argument*/NULL);
		RuntimeTypeHandle_t3027515415  L_25 = { reinterpret_cast<intptr_t> (Skybox_t2662837510_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		NullCheck(L_24);
		Component_t1923634451 * L_27 = GameObject_GetComponent_m1027872079(L_24, L_26, /*hidden argument*/NULL);
		V_0 = ((Skybox_t2662837510 *)CastclassSealed((RuntimeObject*)L_27, Skybox_t2662837510_il2cpp_TypeInfo_var));
		Skybox_t2662837510 * L_28 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_29 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		if (L_29)
		{
			goto IL_00eb;
		}
	}
	{
		Camera_t4157153871 * L_30 = ___reflectCamera1;
		NullCheck(L_30);
		GameObject_t1113636619 * L_31 = Component_get_gameObject_m442555142(L_30, /*hidden argument*/NULL);
		RuntimeTypeHandle_t3027515415  L_32 = { reinterpret_cast<intptr_t> (Skybox_t2662837510_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_33 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		NullCheck(L_31);
		Component_t1923634451 * L_34 = GameObject_AddComponent_m136524825(L_31, L_33, /*hidden argument*/NULL);
		V_0 = ((Skybox_t2662837510 *)CastclassSealed((RuntimeObject*)L_34, Skybox_t2662837510_il2cpp_TypeInfo_var));
	}

IL_00eb:
	{
		Skybox_t2662837510 * L_35 = V_0;
		Camera_t4157153871 * L_36 = ___cam0;
		RuntimeTypeHandle_t3027515415  L_37 = { reinterpret_cast<intptr_t> (Skybox_t2662837510_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_38 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		NullCheck(L_36);
		Component_t1923634451 * L_39 = Component_GetComponent_m886226392(L_36, L_38, /*hidden argument*/NULL);
		NullCheck(((Skybox_t2662837510 *)CastclassSealed((RuntimeObject*)L_39, Skybox_t2662837510_il2cpp_TypeInfo_var)));
		Material_t340375123 * L_40 = Skybox_get_material_m3789022787(((Skybox_t2662837510 *)CastclassSealed((RuntimeObject*)L_39, Skybox_t2662837510_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		NullCheck(L_35);
		Skybox_set_material_m3176166872(L_35, L_40, /*hidden argument*/NULL);
	}

IL_010b:
	{
		GL_set_invertCulling_m2740667760(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		Transform_t3600365921 * L_41 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		V_1 = L_41;
		Camera_t4157153871 * L_42 = ___cam0;
		NullCheck(L_42);
		Transform_t3600365921 * L_43 = Component_get_transform_m3162698980(L_42, /*hidden argument*/NULL);
		NullCheck(L_43);
		Vector3_t3722313464  L_44 = Transform_get_eulerAngles_m2743581774(L_43, /*hidden argument*/NULL);
		V_2 = L_44;
		Camera_t4157153871 * L_45 = ___reflectCamera1;
		NullCheck(L_45);
		Transform_t3600365921 * L_46 = Component_get_transform_m3162698980(L_45, /*hidden argument*/NULL);
		float L_47 = (&V_2)->get_x_1();
		float L_48 = (&V_2)->get_y_2();
		float L_49 = (&V_2)->get_z_3();
		Vector3_t3722313464  L_50;
		memset(&L_50, 0, sizeof(L_50));
		Vector3__ctor_m3353183577((&L_50), ((-L_47)), L_48, L_49, /*hidden argument*/NULL);
		NullCheck(L_46);
		Transform_set_eulerAngles_m135219616(L_46, L_50, /*hidden argument*/NULL);
		Camera_t4157153871 * L_51 = ___reflectCamera1;
		NullCheck(L_51);
		Transform_t3600365921 * L_52 = Component_get_transform_m3162698980(L_51, /*hidden argument*/NULL);
		Camera_t4157153871 * L_53 = ___cam0;
		NullCheck(L_53);
		Transform_t3600365921 * L_54 = Component_get_transform_m3162698980(L_53, /*hidden argument*/NULL);
		NullCheck(L_54);
		Vector3_t3722313464  L_55 = Transform_get_position_m36019626(L_54, /*hidden argument*/NULL);
		NullCheck(L_52);
		Transform_set_position_m3387557959(L_52, L_55, /*hidden argument*/NULL);
		Transform_t3600365921 * L_56 = V_1;
		NullCheck(L_56);
		Transform_t3600365921 * L_57 = Component_get_transform_m3162698980(L_56, /*hidden argument*/NULL);
		NullCheck(L_57);
		Vector3_t3722313464  L_58 = Transform_get_position_m36019626(L_57, /*hidden argument*/NULL);
		V_3 = L_58;
		Transform_t3600365921 * L_59 = V_1;
		NullCheck(L_59);
		Vector3_t3722313464  L_60 = Transform_get_position_m36019626(L_59, /*hidden argument*/NULL);
		V_4 = L_60;
		float L_61 = (&V_4)->get_y_2();
		(&V_3)->set_y_2(L_61);
		Transform_t3600365921 * L_62 = V_1;
		NullCheck(L_62);
		Transform_t3600365921 * L_63 = Component_get_transform_m3162698980(L_62, /*hidden argument*/NULL);
		NullCheck(L_63);
		Vector3_t3722313464  L_64 = Transform_get_up_m3972993886(L_63, /*hidden argument*/NULL);
		V_5 = L_64;
		Vector3_t3722313464  L_65 = V_5;
		Vector3_t3722313464  L_66 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_67 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_65, L_66, /*hidden argument*/NULL);
		float L_68 = __this->get_clipPlaneOffset_6();
		V_6 = ((float)il2cpp_codegen_subtract((float)((-L_67)), (float)L_68));
		float L_69 = (&V_5)->get_x_1();
		float L_70 = (&V_5)->get_y_2();
		float L_71 = (&V_5)->get_z_3();
		float L_72 = V_6;
		Vector4__ctor_m2498754347((Vector4_t3319028937 *)(&V_7), L_69, L_70, L_71, L_72, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t1817901843_il2cpp_TypeInfo_var);
		Matrix4x4_t1817901843  L_73 = Matrix4x4_get_zero_m2898777066(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_8 = L_73;
		Matrix4x4_t1817901843  L_74 = V_8;
		Vector4_t3319028937  L_75 = V_7;
		Matrix4x4_t1817901843  L_76 = PlanarReflection_CalculateReflectionMatrix_m3712858887(NULL /*static, unused*/, L_74, L_75, /*hidden argument*/NULL);
		V_8 = L_76;
		Camera_t4157153871 * L_77 = ___cam0;
		NullCheck(L_77);
		Transform_t3600365921 * L_78 = Component_get_transform_m3162698980(L_77, /*hidden argument*/NULL);
		NullCheck(L_78);
		Vector3_t3722313464  L_79 = Transform_get_position_m36019626(L_78, /*hidden argument*/NULL);
		__this->set_m_Oldpos_7(L_79);
		Vector3_t3722313464  L_80 = __this->get_m_Oldpos_7();
		Vector3_t3722313464  L_81 = Matrix4x4_MultiplyPoint_m1575665487((Matrix4x4_t1817901843 *)(&V_8), L_80, /*hidden argument*/NULL);
		V_9 = L_81;
		Camera_t4157153871 * L_82 = ___reflectCamera1;
		Camera_t4157153871 * L_83 = ___cam0;
		NullCheck(L_83);
		Matrix4x4_t1817901843  L_84 = Camera_get_worldToCameraMatrix_m22661425(L_83, /*hidden argument*/NULL);
		Matrix4x4_t1817901843  L_85 = V_8;
		Matrix4x4_t1817901843  L_86 = Matrix4x4_op_Multiply_m1876492807(NULL /*static, unused*/, L_84, L_85, /*hidden argument*/NULL);
		NullCheck(L_82);
		Camera_set_worldToCameraMatrix_m2548466927(L_82, L_86, /*hidden argument*/NULL);
		Camera_t4157153871 * L_87 = ___reflectCamera1;
		Vector3_t3722313464  L_88 = V_3;
		Vector3_t3722313464  L_89 = V_5;
		Vector4_t3319028937  L_90 = PlanarReflection_CameraSpacePlane_m3103867915(__this, L_87, L_88, L_89, (1.0f), /*hidden argument*/NULL);
		V_10 = L_90;
		Camera_t4157153871 * L_91 = ___cam0;
		NullCheck(L_91);
		Matrix4x4_t1817901843  L_92 = Camera_get_projectionMatrix_m667780853(L_91, /*hidden argument*/NULL);
		V_11 = L_92;
		Matrix4x4_t1817901843  L_93 = V_11;
		Vector4_t3319028937  L_94 = V_10;
		Matrix4x4_t1817901843  L_95 = PlanarReflection_CalculateObliqueMatrix_m3657792474(NULL /*static, unused*/, L_93, L_94, /*hidden argument*/NULL);
		V_11 = L_95;
		Camera_t4157153871 * L_96 = ___reflectCamera1;
		Matrix4x4_t1817901843  L_97 = V_11;
		NullCheck(L_96);
		Camera_set_projectionMatrix_m3293177686(L_96, L_97, /*hidden argument*/NULL);
		Camera_t4157153871 * L_98 = ___reflectCamera1;
		NullCheck(L_98);
		Transform_t3600365921 * L_99 = Component_get_transform_m3162698980(L_98, /*hidden argument*/NULL);
		Vector3_t3722313464  L_100 = V_9;
		NullCheck(L_99);
		Transform_set_position_m3387557959(L_99, L_100, /*hidden argument*/NULL);
		Camera_t4157153871 * L_101 = ___cam0;
		NullCheck(L_101);
		Transform_t3600365921 * L_102 = Component_get_transform_m3162698980(L_101, /*hidden argument*/NULL);
		NullCheck(L_102);
		Vector3_t3722313464  L_103 = Transform_get_eulerAngles_m2743581774(L_102, /*hidden argument*/NULL);
		V_12 = L_103;
		Camera_t4157153871 * L_104 = ___reflectCamera1;
		NullCheck(L_104);
		Transform_t3600365921 * L_105 = Component_get_transform_m3162698980(L_104, /*hidden argument*/NULL);
		float L_106 = (&V_12)->get_x_1();
		float L_107 = (&V_12)->get_y_2();
		float L_108 = (&V_12)->get_z_3();
		Vector3_t3722313464  L_109;
		memset(&L_109, 0, sizeof(L_109));
		Vector3__ctor_m3353183577((&L_109), ((-L_106)), L_107, L_108, /*hidden argument*/NULL);
		NullCheck(L_105);
		Transform_set_eulerAngles_m135219616(L_105, L_109, /*hidden argument*/NULL);
		Camera_t4157153871 * L_110 = ___reflectCamera1;
		NullCheck(L_110);
		Camera_Render_m2813253190(L_110, /*hidden argument*/NULL);
		GL_set_invertCulling_m2740667760(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.PlanarReflection::SaneCameraSettings(UnityEngine.Camera)
extern "C"  void PlanarReflection_SaneCameraSettings_m1844908166 (PlanarReflection_t439636033 * __this, Camera_t4157153871 * ___helperCam0, const RuntimeMethod* method)
{
	{
		Camera_t4157153871 * L_0 = ___helperCam0;
		NullCheck(L_0);
		Camera_set_depthTextureMode_m754977860(L_0, 0, /*hidden argument*/NULL);
		Camera_t4157153871 * L_1 = ___helperCam0;
		Color_t2555686324  L_2 = Color_get_black_m719512684(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Camera_set_backgroundColor_m1332346802(L_1, L_2, /*hidden argument*/NULL);
		Camera_t4157153871 * L_3 = ___helperCam0;
		NullCheck(L_3);
		Camera_set_clearFlags_m2207032996(L_3, 2, /*hidden argument*/NULL);
		Camera_t4157153871 * L_4 = ___helperCam0;
		NullCheck(L_4);
		Camera_set_renderingPath_m3563375443(L_4, 1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Matrix4x4 UnityStandardAssets.Water.PlanarReflection::CalculateObliqueMatrix(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern "C"  Matrix4x4_t1817901843  PlanarReflection_CalculateObliqueMatrix_m3657792474 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843  ___projection0, Vector4_t3319028937  ___clipPlane1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlanarReflection_CalculateObliqueMatrix_m3657792474_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector4_t3319028937  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t3319028937  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Matrix4x4_t1817901843  L_0 = Matrix4x4_get_inverse_m1870592360((Matrix4x4_t1817901843 *)(&___projection0), /*hidden argument*/NULL);
		float L_1 = (&___clipPlane1)->get_x_1();
		float L_2 = PlanarReflection_Sgn_m3256206255(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		float L_3 = (&___clipPlane1)->get_y_2();
		float L_4 = PlanarReflection_Sgn_m3256206255(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Vector4_t3319028937  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector4__ctor_m2498754347((&L_5), L_2, L_4, (1.0f), (1.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t1817901843_il2cpp_TypeInfo_var);
		Vector4_t3319028937  L_6 = Matrix4x4_op_Multiply_m1839501195(NULL /*static, unused*/, L_0, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Vector4_t3319028937  L_7 = ___clipPlane1;
		Vector4_t3319028937  L_8 = ___clipPlane1;
		Vector4_t3319028937  L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t3319028937_il2cpp_TypeInfo_var);
		float L_10 = Vector4_Dot_m3492158352(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		Vector4_t3319028937  L_11 = Vector4_op_Multiply_m213790997(NULL /*static, unused*/, L_7, ((float)((float)(2.0f)/(float)L_10)), /*hidden argument*/NULL);
		V_1 = L_11;
		float L_12 = (&V_1)->get_x_1();
		float L_13 = Matrix4x4_get_Item_m567451091((Matrix4x4_t1817901843 *)(&___projection0), 3, /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1906605342((Matrix4x4_t1817901843 *)(&___projection0), 2, ((float)il2cpp_codegen_subtract((float)L_12, (float)L_13)), /*hidden argument*/NULL);
		float L_14 = (&V_1)->get_y_2();
		float L_15 = Matrix4x4_get_Item_m567451091((Matrix4x4_t1817901843 *)(&___projection0), 7, /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1906605342((Matrix4x4_t1817901843 *)(&___projection0), 6, ((float)il2cpp_codegen_subtract((float)L_14, (float)L_15)), /*hidden argument*/NULL);
		float L_16 = (&V_1)->get_z_3();
		float L_17 = Matrix4x4_get_Item_m567451091((Matrix4x4_t1817901843 *)(&___projection0), ((int32_t)11), /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1906605342((Matrix4x4_t1817901843 *)(&___projection0), ((int32_t)10), ((float)il2cpp_codegen_subtract((float)L_16, (float)L_17)), /*hidden argument*/NULL);
		float L_18 = (&V_1)->get_w_4();
		float L_19 = Matrix4x4_get_Item_m567451091((Matrix4x4_t1817901843 *)(&___projection0), ((int32_t)15), /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1906605342((Matrix4x4_t1817901843 *)(&___projection0), ((int32_t)14), ((float)il2cpp_codegen_subtract((float)L_18, (float)L_19)), /*hidden argument*/NULL);
		Matrix4x4_t1817901843  L_20 = ___projection0;
		return L_20;
	}
}
// UnityEngine.Matrix4x4 UnityStandardAssets.Water.PlanarReflection::CalculateReflectionMatrix(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern "C"  Matrix4x4_t1817901843  PlanarReflection_CalculateReflectionMatrix_m3712858887 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843  ___reflectionMat0, Vector4_t3319028937  ___plane1, const RuntimeMethod* method)
{
	{
		float L_0 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 0, /*hidden argument*/NULL);
		float L_1 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 0, /*hidden argument*/NULL);
		(&___reflectionMat0)->set_m00_0(((float)il2cpp_codegen_subtract((float)(1.0f), (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(2.0f), (float)L_0)), (float)L_1)))));
		float L_2 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 0, /*hidden argument*/NULL);
		float L_3 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 1, /*hidden argument*/NULL);
		(&___reflectionMat0)->set_m01_4(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_2)), (float)L_3)));
		float L_4 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 0, /*hidden argument*/NULL);
		float L_5 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 2, /*hidden argument*/NULL);
		(&___reflectionMat0)->set_m02_8(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_4)), (float)L_5)));
		float L_6 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 3, /*hidden argument*/NULL);
		float L_7 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 0, /*hidden argument*/NULL);
		(&___reflectionMat0)->set_m03_12(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_6)), (float)L_7)));
		float L_8 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 1, /*hidden argument*/NULL);
		float L_9 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 0, /*hidden argument*/NULL);
		(&___reflectionMat0)->set_m10_1(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_8)), (float)L_9)));
		float L_10 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 1, /*hidden argument*/NULL);
		float L_11 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 1, /*hidden argument*/NULL);
		(&___reflectionMat0)->set_m11_5(((float)il2cpp_codegen_subtract((float)(1.0f), (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(2.0f), (float)L_10)), (float)L_11)))));
		float L_12 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 1, /*hidden argument*/NULL);
		float L_13 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 2, /*hidden argument*/NULL);
		(&___reflectionMat0)->set_m12_9(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_12)), (float)L_13)));
		float L_14 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 3, /*hidden argument*/NULL);
		float L_15 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 1, /*hidden argument*/NULL);
		(&___reflectionMat0)->set_m13_13(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_14)), (float)L_15)));
		float L_16 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 2, /*hidden argument*/NULL);
		float L_17 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 0, /*hidden argument*/NULL);
		(&___reflectionMat0)->set_m20_2(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_16)), (float)L_17)));
		float L_18 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 2, /*hidden argument*/NULL);
		float L_19 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 1, /*hidden argument*/NULL);
		(&___reflectionMat0)->set_m21_6(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_18)), (float)L_19)));
		float L_20 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 2, /*hidden argument*/NULL);
		float L_21 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 2, /*hidden argument*/NULL);
		(&___reflectionMat0)->set_m22_10(((float)il2cpp_codegen_subtract((float)(1.0f), (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(2.0f), (float)L_20)), (float)L_21)))));
		float L_22 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 3, /*hidden argument*/NULL);
		float L_23 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 2, /*hidden argument*/NULL);
		(&___reflectionMat0)->set_m23_14(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_22)), (float)L_23)));
		(&___reflectionMat0)->set_m30_3((0.0f));
		(&___reflectionMat0)->set_m31_7((0.0f));
		(&___reflectionMat0)->set_m32_11((0.0f));
		(&___reflectionMat0)->set_m33_15((1.0f));
		Matrix4x4_t1817901843  L_24 = ___reflectionMat0;
		return L_24;
	}
}
// System.Single UnityStandardAssets.Water.PlanarReflection::Sgn(System.Single)
extern "C"  float PlanarReflection_Sgn_m3256206255 (RuntimeObject * __this /* static, unused */, float ___a0, const RuntimeMethod* method)
{
	{
		float L_0 = ___a0;
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_0011;
		}
	}
	{
		return (1.0f);
	}

IL_0011:
	{
		float L_1 = ___a0;
		if ((!(((float)L_1) < ((float)(0.0f)))))
		{
			goto IL_0022;
		}
	}
	{
		return (-1.0f);
	}

IL_0022:
	{
		return (0.0f);
	}
}
// UnityEngine.Vector4 UnityStandardAssets.Water.PlanarReflection::CameraSpacePlane(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector4_t3319028937  PlanarReflection_CameraSpacePlane_m3103867915 (PlanarReflection_t439636033 * __this, Camera_t4157153871 * ___cam0, Vector3_t3722313464  ___pos1, Vector3_t3722313464  ___normal2, float ___sideSign3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlanarReflection_CameraSpacePlane_m3103867915_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t1817901843  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Vector3_t3722313464  L_0 = ___pos1;
		Vector3_t3722313464  L_1 = ___normal2;
		float L_2 = __this->get_clipPlaneOffset_6();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t3722313464  L_4 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Camera_t4157153871 * L_5 = ___cam0;
		NullCheck(L_5);
		Matrix4x4_t1817901843  L_6 = Camera_get_worldToCameraMatrix_m22661425(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		Vector3_t3722313464  L_7 = V_0;
		Vector3_t3722313464  L_8 = Matrix4x4_MultiplyPoint_m1575665487((Matrix4x4_t1817901843 *)(&V_1), L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		Vector3_t3722313464  L_9 = ___normal2;
		Vector3_t3722313464  L_10 = Matrix4x4_MultiplyVector_m3808798942((Matrix4x4_t1817901843 *)(&V_1), L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		Vector3_t3722313464  L_11 = Vector3_get_normalized_m2454957984((Vector3_t3722313464 *)(&V_4), /*hidden argument*/NULL);
		float L_12 = ___sideSign3;
		Vector3_t3722313464  L_13 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		float L_14 = (&V_3)->get_x_1();
		float L_15 = (&V_3)->get_y_2();
		float L_16 = (&V_3)->get_z_3();
		Vector3_t3722313464  L_17 = V_2;
		Vector3_t3722313464  L_18 = V_3;
		float L_19 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		Vector4_t3319028937  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector4__ctor_m2498754347((&L_20), L_14, L_15, L_16, ((-L_19)), /*hidden argument*/NULL);
		return L_20;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets.Water.SpecularLighting::.ctor()
extern "C"  void SpecularLighting__ctor_m3107193736 (SpecularLighting_t2163114238 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.SpecularLighting::Start()
extern "C"  void SpecularLighting_Start_m3290965022 (SpecularLighting_t2163114238 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpecularLighting_Start_m3290965022_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		RuntimeTypeHandle_t3027515415  L_1 = { reinterpret_cast<intptr_t> (WaterBase_t3883863317_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t1923634451 * L_3 = GameObject_GetComponent_m1027872079(L_0, L_2, /*hidden argument*/NULL);
		__this->set_m_WaterBase_3(((WaterBase_t3883863317 *)CastclassClass((RuntimeObject*)L_3, WaterBase_t3883863317_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityStandardAssets.Water.SpecularLighting::Update()
extern "C"  void SpecularLighting_Update_m2066293166 (SpecularLighting_t2163114238 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpecularLighting_Update_m2066293166_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WaterBase_t3883863317 * L_0 = __this->get_m_WaterBase_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0030;
		}
	}
	{
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		RuntimeTypeHandle_t3027515415  L_3 = { reinterpret_cast<intptr_t> (WaterBase_t3883863317_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Component_t1923634451 * L_5 = GameObject_GetComponent_m1027872079(L_2, L_4, /*hidden argument*/NULL);
		__this->set_m_WaterBase_3(((WaterBase_t3883863317 *)CastclassClass((RuntimeObject*)L_5, WaterBase_t3883863317_il2cpp_TypeInfo_var)));
	}

IL_0030:
	{
		Transform_t3600365921 * L_6 = __this->get_specularLight_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_007f;
		}
	}
	{
		WaterBase_t3883863317 * L_8 = __this->get_m_WaterBase_3();
		NullCheck(L_8);
		Material_t340375123 * L_9 = L_8->get_sharedMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_007f;
		}
	}
	{
		WaterBase_t3883863317 * L_11 = __this->get_m_WaterBase_3();
		NullCheck(L_11);
		Material_t340375123 * L_12 = L_11->get_sharedMaterial_2();
		Transform_t3600365921 * L_13 = __this->get_specularLight_2();
		NullCheck(L_13);
		Transform_t3600365921 * L_14 = Component_get_transform_m3162698980(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t3722313464  L_15 = Transform_get_forward_m747522392(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t3319028937_il2cpp_TypeInfo_var);
		Vector4_t3319028937  L_16 = Vector4_op_Implicit_m2966035112(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_12);
		Material_SetVector_m4214217286(L_12, _stringLiteral1648708994, L_16, /*hidden argument*/NULL);
	}

IL_007f:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets.Water.Water::.ctor()
extern "C"  void Water__ctor_m1153371576 (Water_t936588004 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water__ctor_m1153371576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_waterMode_2(2);
		__this->set_disablePixelLights_3((bool)1);
		__this->set_textureSize_4(((int32_t)256));
		__this->set_clipPlaneOffset_5((0.07f));
		LayerMask_t3493934918  L_0 = LayerMask_op_Implicit_m90232283(NULL /*static, unused*/, (-1), /*hidden argument*/NULL);
		__this->set_reflectLayers_6(L_0);
		LayerMask_t3493934918  L_1 = LayerMask_op_Implicit_m90232283(NULL /*static, unused*/, (-1), /*hidden argument*/NULL);
		__this->set_refractLayers_7(L_1);
		Dictionary_2_t75641268 * L_2 = (Dictionary_2_t75641268 *)il2cpp_codegen_object_new(Dictionary_2_t75641268_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m579872301(L_2, /*hidden argument*/Dictionary_2__ctor_m579872301_RuntimeMethod_var);
		__this->set_m_ReflectionCameras_8(L_2);
		Dictionary_2_t75641268 * L_3 = (Dictionary_2_t75641268 *)il2cpp_codegen_object_new(Dictionary_2_t75641268_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m579872301(L_3, /*hidden argument*/Dictionary_2__ctor_m579872301_RuntimeMethod_var);
		__this->set_m_RefractionCameras_9(L_3);
		__this->set_m_HardwareWaterSupport_12(2);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.Water::OnWillRenderObject()
extern "C"  void Water_OnWillRenderObject_m1537605840 (Water_t936588004 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water_OnWillRenderObject_m1537605840_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Camera_t4157153871 * V_0 = NULL;
	int32_t V_1 = 0;
	Camera_t4157153871 * V_2 = NULL;
	Camera_t4157153871 * V_3 = NULL;
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	int32_t V_6 = 0;
	float V_7 = 0.0f;
	Vector4_t3319028937  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Matrix4x4_t1817901843  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t3722313464  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t3722313464  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector4_t3319028937  V_12;
	memset(&V_12, 0, sizeof(V_12));
	bool V_13 = false;
	Vector3_t3722313464  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Vector4_t3319028937  V_15;
	memset(&V_15, 0, sizeof(V_15));
	{
		bool L_0 = Behaviour_get_enabled_m753527255(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0040;
		}
	}
	{
		Renderer_t2627027031 * L_1 = Component_GetComponent_TisRenderer_t2627027031_m1858649706(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m1858649706_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0040;
		}
	}
	{
		Renderer_t2627027031 * L_3 = Component_GetComponent_TisRenderer_t2627027031_m1858649706(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m1858649706_RuntimeMethod_var);
		NullCheck(L_3);
		Material_t340375123 * L_4 = Renderer_get_sharedMaterial_m1936632411(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0040;
		}
	}
	{
		Renderer_t2627027031 * L_6 = Component_GetComponent_TisRenderer_t2627027031_m1858649706(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m1858649706_RuntimeMethod_var);
		NullCheck(L_6);
		bool L_7 = Renderer_get_enabled_m3482452518(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0041;
		}
	}

IL_0040:
	{
		return;
	}

IL_0041:
	{
		Camera_t4157153871 * L_8 = Camera_get_current_m929992396(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_8;
		Camera_t4157153871 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0053;
		}
	}
	{
		return;
	}

IL_0053:
	{
		bool L_11 = ((Water_t936588004_StaticFields*)il2cpp_codegen_static_fields_for(Water_t936588004_il2cpp_TypeInfo_var))->get_s_InsideWater_15();
		if (!L_11)
		{
			goto IL_005e;
		}
	}
	{
		return;
	}

IL_005e:
	{
		((Water_t936588004_StaticFields*)il2cpp_codegen_static_fields_for(Water_t936588004_il2cpp_TypeInfo_var))->set_s_InsideWater_15((bool)1);
		int32_t L_12 = Water_FindHardwareWaterSupport_m3785980915(__this, /*hidden argument*/NULL);
		__this->set_m_HardwareWaterSupport_12(L_12);
		int32_t L_13 = Water_GetWaterMode_m1759043735(__this, /*hidden argument*/NULL);
		V_1 = L_13;
		Camera_t4157153871 * L_14 = V_0;
		Water_CreateWaterObjects_m2240077532(__this, L_14, (Camera_t4157153871 **)(&V_2), (Camera_t4157153871 **)(&V_3), /*hidden argument*/NULL);
		Transform_t3600365921 * L_15 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_t3722313464  L_16 = Transform_get_position_m36019626(L_15, /*hidden argument*/NULL);
		V_4 = L_16;
		Transform_t3600365921 * L_17 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t3722313464  L_18 = Transform_get_up_m3972993886(L_17, /*hidden argument*/NULL);
		V_5 = L_18;
		int32_t L_19 = QualitySettings_get_pixelLightCount_m3013306133(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_6 = L_19;
		bool L_20 = __this->get_disablePixelLights_3();
		if (!L_20)
		{
			goto IL_00b4;
		}
	}
	{
		QualitySettings_set_pixelLightCount_m3523654033(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_00b4:
	{
		Camera_t4157153871 * L_21 = V_0;
		Camera_t4157153871 * L_22 = V_2;
		Water_UpdateCameraModes_m3658611570(__this, L_21, L_22, /*hidden argument*/NULL);
		Camera_t4157153871 * L_23 = V_0;
		Camera_t4157153871 * L_24 = V_3;
		Water_UpdateCameraModes_m3658611570(__this, L_23, L_24, /*hidden argument*/NULL);
		int32_t L_25 = V_1;
		if ((((int32_t)L_25) < ((int32_t)1)))
		{
			goto IL_0214;
		}
	}
	{
		Vector3_t3722313464  L_26 = V_5;
		Vector3_t3722313464  L_27 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_28 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_26, L_27, /*hidden argument*/NULL);
		float L_29 = __this->get_clipPlaneOffset_5();
		V_7 = ((float)il2cpp_codegen_subtract((float)((-L_28)), (float)L_29));
		float L_30 = (&V_5)->get_x_1();
		float L_31 = (&V_5)->get_y_2();
		float L_32 = (&V_5)->get_z_3();
		float L_33 = V_7;
		Vector4__ctor_m2498754347((Vector4_t3319028937 *)(&V_8), L_30, L_31, L_32, L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t1817901843_il2cpp_TypeInfo_var);
		Matrix4x4_t1817901843  L_34 = Matrix4x4_get_zero_m2898777066(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_9 = L_34;
		Vector4_t3319028937  L_35 = V_8;
		Water_CalculateReflectionMatrix_m175561076(NULL /*static, unused*/, (Matrix4x4_t1817901843 *)(&V_9), L_35, /*hidden argument*/NULL);
		Camera_t4157153871 * L_36 = V_0;
		NullCheck(L_36);
		Transform_t3600365921 * L_37 = Component_get_transform_m3162698980(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		Vector3_t3722313464  L_38 = Transform_get_position_m36019626(L_37, /*hidden argument*/NULL);
		V_10 = L_38;
		Vector3_t3722313464  L_39 = V_10;
		Vector3_t3722313464  L_40 = Matrix4x4_MultiplyPoint_m1575665487((Matrix4x4_t1817901843 *)(&V_9), L_39, /*hidden argument*/NULL);
		V_11 = L_40;
		Camera_t4157153871 * L_41 = V_2;
		Camera_t4157153871 * L_42 = V_0;
		NullCheck(L_42);
		Matrix4x4_t1817901843  L_43 = Camera_get_worldToCameraMatrix_m22661425(L_42, /*hidden argument*/NULL);
		Matrix4x4_t1817901843  L_44 = V_9;
		Matrix4x4_t1817901843  L_45 = Matrix4x4_op_Multiply_m1876492807(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		NullCheck(L_41);
		Camera_set_worldToCameraMatrix_m2548466927(L_41, L_45, /*hidden argument*/NULL);
		Camera_t4157153871 * L_46 = V_2;
		Vector3_t3722313464  L_47 = V_4;
		Vector3_t3722313464  L_48 = V_5;
		Vector4_t3319028937  L_49 = Water_CameraSpacePlane_m2332942409(__this, L_46, L_47, L_48, (1.0f), /*hidden argument*/NULL);
		V_12 = L_49;
		Camera_t4157153871 * L_50 = V_2;
		Camera_t4157153871 * L_51 = V_0;
		Vector4_t3319028937  L_52 = V_12;
		NullCheck(L_51);
		Matrix4x4_t1817901843  L_53 = Camera_CalculateObliqueMatrix_m3018791782(L_51, L_52, /*hidden argument*/NULL);
		NullCheck(L_50);
		Camera_set_projectionMatrix_m3293177686(L_50, L_53, /*hidden argument*/NULL);
		Camera_t4157153871 * L_54 = V_2;
		Camera_t4157153871 * L_55 = V_0;
		NullCheck(L_55);
		Matrix4x4_t1817901843  L_56 = Camera_get_projectionMatrix_m667780853(L_55, /*hidden argument*/NULL);
		Camera_t4157153871 * L_57 = V_0;
		NullCheck(L_57);
		Matrix4x4_t1817901843  L_58 = Camera_get_worldToCameraMatrix_m22661425(L_57, /*hidden argument*/NULL);
		Matrix4x4_t1817901843  L_59 = Matrix4x4_op_Multiply_m1876492807(NULL /*static, unused*/, L_56, L_58, /*hidden argument*/NULL);
		NullCheck(L_54);
		Camera_set_cullingMatrix_m2348869097(L_54, L_59, /*hidden argument*/NULL);
		Camera_t4157153871 * L_60 = V_2;
		LayerMask_t3493934918 * L_61 = __this->get_address_of_reflectLayers_6();
		int32_t L_62 = LayerMask_get_value_m1881709263((LayerMask_t3493934918 *)L_61, /*hidden argument*/NULL);
		NullCheck(L_60);
		Camera_set_cullingMask_m1402455777(L_60, ((int32_t)((int32_t)((int32_t)-17)&(int32_t)L_62)), /*hidden argument*/NULL);
		Camera_t4157153871 * L_63 = V_2;
		RenderTexture_t2108887433 * L_64 = __this->get_m_ReflectionTexture_10();
		NullCheck(L_63);
		Camera_set_targetTexture_m3148311140(L_63, L_64, /*hidden argument*/NULL);
		bool L_65 = GL_get_invertCulling_m4270974637(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_13 = L_65;
		bool L_66 = V_13;
		GL_set_invertCulling_m2740667760(NULL /*static, unused*/, (bool)((((int32_t)L_66) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		Camera_t4157153871 * L_67 = V_2;
		NullCheck(L_67);
		Transform_t3600365921 * L_68 = Component_get_transform_m3162698980(L_67, /*hidden argument*/NULL);
		Vector3_t3722313464  L_69 = V_11;
		NullCheck(L_68);
		Transform_set_position_m3387557959(L_68, L_69, /*hidden argument*/NULL);
		Camera_t4157153871 * L_70 = V_0;
		NullCheck(L_70);
		Transform_t3600365921 * L_71 = Component_get_transform_m3162698980(L_70, /*hidden argument*/NULL);
		NullCheck(L_71);
		Vector3_t3722313464  L_72 = Transform_get_eulerAngles_m2743581774(L_71, /*hidden argument*/NULL);
		V_14 = L_72;
		Camera_t4157153871 * L_73 = V_2;
		NullCheck(L_73);
		Transform_t3600365921 * L_74 = Component_get_transform_m3162698980(L_73, /*hidden argument*/NULL);
		float L_75 = (&V_14)->get_x_1();
		float L_76 = (&V_14)->get_y_2();
		float L_77 = (&V_14)->get_z_3();
		Vector3_t3722313464  L_78;
		memset(&L_78, 0, sizeof(L_78));
		Vector3__ctor_m3353183577((&L_78), ((-L_75)), L_76, L_77, /*hidden argument*/NULL);
		NullCheck(L_74);
		Transform_set_eulerAngles_m135219616(L_74, L_78, /*hidden argument*/NULL);
		Camera_t4157153871 * L_79 = V_2;
		NullCheck(L_79);
		Camera_Render_m2813253190(L_79, /*hidden argument*/NULL);
		Camera_t4157153871 * L_80 = V_2;
		NullCheck(L_80);
		Transform_t3600365921 * L_81 = Component_get_transform_m3162698980(L_80, /*hidden argument*/NULL);
		Vector3_t3722313464  L_82 = V_10;
		NullCheck(L_81);
		Transform_set_position_m3387557959(L_81, L_82, /*hidden argument*/NULL);
		bool L_83 = V_13;
		GL_set_invertCulling_m2740667760(NULL /*static, unused*/, L_83, /*hidden argument*/NULL);
		Renderer_t2627027031 * L_84 = Component_GetComponent_TisRenderer_t2627027031_m1858649706(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m1858649706_RuntimeMethod_var);
		NullCheck(L_84);
		Material_t340375123 * L_85 = Renderer_get_sharedMaterial_m1936632411(L_84, /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_86 = __this->get_m_ReflectionTexture_10();
		NullCheck(L_85);
		Material_SetTexture_m1829349465(L_85, _stringLiteral253045501, L_86, /*hidden argument*/NULL);
	}

IL_0214:
	{
		int32_t L_87 = V_1;
		if ((((int32_t)L_87) < ((int32_t)2)))
		{
			goto IL_02cb;
		}
	}
	{
		Camera_t4157153871 * L_88 = V_3;
		Camera_t4157153871 * L_89 = V_0;
		NullCheck(L_89);
		Matrix4x4_t1817901843  L_90 = Camera_get_worldToCameraMatrix_m22661425(L_89, /*hidden argument*/NULL);
		NullCheck(L_88);
		Camera_set_worldToCameraMatrix_m2548466927(L_88, L_90, /*hidden argument*/NULL);
		Camera_t4157153871 * L_91 = V_3;
		Vector3_t3722313464  L_92 = V_4;
		Vector3_t3722313464  L_93 = V_5;
		Vector4_t3319028937  L_94 = Water_CameraSpacePlane_m2332942409(__this, L_91, L_92, L_93, (-1.0f), /*hidden argument*/NULL);
		V_15 = L_94;
		Camera_t4157153871 * L_95 = V_3;
		Camera_t4157153871 * L_96 = V_0;
		Vector4_t3319028937  L_97 = V_15;
		NullCheck(L_96);
		Matrix4x4_t1817901843  L_98 = Camera_CalculateObliqueMatrix_m3018791782(L_96, L_97, /*hidden argument*/NULL);
		NullCheck(L_95);
		Camera_set_projectionMatrix_m3293177686(L_95, L_98, /*hidden argument*/NULL);
		Camera_t4157153871 * L_99 = V_3;
		Camera_t4157153871 * L_100 = V_0;
		NullCheck(L_100);
		Matrix4x4_t1817901843  L_101 = Camera_get_projectionMatrix_m667780853(L_100, /*hidden argument*/NULL);
		Camera_t4157153871 * L_102 = V_0;
		NullCheck(L_102);
		Matrix4x4_t1817901843  L_103 = Camera_get_worldToCameraMatrix_m22661425(L_102, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t1817901843_il2cpp_TypeInfo_var);
		Matrix4x4_t1817901843  L_104 = Matrix4x4_op_Multiply_m1876492807(NULL /*static, unused*/, L_101, L_103, /*hidden argument*/NULL);
		NullCheck(L_99);
		Camera_set_cullingMatrix_m2348869097(L_99, L_104, /*hidden argument*/NULL);
		Camera_t4157153871 * L_105 = V_3;
		LayerMask_t3493934918 * L_106 = __this->get_address_of_refractLayers_7();
		int32_t L_107 = LayerMask_get_value_m1881709263((LayerMask_t3493934918 *)L_106, /*hidden argument*/NULL);
		NullCheck(L_105);
		Camera_set_cullingMask_m1402455777(L_105, ((int32_t)((int32_t)((int32_t)-17)&(int32_t)L_107)), /*hidden argument*/NULL);
		Camera_t4157153871 * L_108 = V_3;
		RenderTexture_t2108887433 * L_109 = __this->get_m_RefractionTexture_11();
		NullCheck(L_108);
		Camera_set_targetTexture_m3148311140(L_108, L_109, /*hidden argument*/NULL);
		Camera_t4157153871 * L_110 = V_3;
		NullCheck(L_110);
		Transform_t3600365921 * L_111 = Component_get_transform_m3162698980(L_110, /*hidden argument*/NULL);
		Camera_t4157153871 * L_112 = V_0;
		NullCheck(L_112);
		Transform_t3600365921 * L_113 = Component_get_transform_m3162698980(L_112, /*hidden argument*/NULL);
		NullCheck(L_113);
		Vector3_t3722313464  L_114 = Transform_get_position_m36019626(L_113, /*hidden argument*/NULL);
		NullCheck(L_111);
		Transform_set_position_m3387557959(L_111, L_114, /*hidden argument*/NULL);
		Camera_t4157153871 * L_115 = V_3;
		NullCheck(L_115);
		Transform_t3600365921 * L_116 = Component_get_transform_m3162698980(L_115, /*hidden argument*/NULL);
		Camera_t4157153871 * L_117 = V_0;
		NullCheck(L_117);
		Transform_t3600365921 * L_118 = Component_get_transform_m3162698980(L_117, /*hidden argument*/NULL);
		NullCheck(L_118);
		Quaternion_t2301928331  L_119 = Transform_get_rotation_m3502953881(L_118, /*hidden argument*/NULL);
		NullCheck(L_116);
		Transform_set_rotation_m3524318132(L_116, L_119, /*hidden argument*/NULL);
		Camera_t4157153871 * L_120 = V_3;
		NullCheck(L_120);
		Camera_Render_m2813253190(L_120, /*hidden argument*/NULL);
		Renderer_t2627027031 * L_121 = Component_GetComponent_TisRenderer_t2627027031_m1858649706(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m1858649706_RuntimeMethod_var);
		NullCheck(L_121);
		Material_t340375123 * L_122 = Renderer_get_sharedMaterial_m1936632411(L_121, /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_123 = __this->get_m_RefractionTexture_11();
		NullCheck(L_122);
		Material_SetTexture_m1829349465(L_122, _stringLiteral521248767, L_123, /*hidden argument*/NULL);
	}

IL_02cb:
	{
		bool L_124 = __this->get_disablePixelLights_3();
		if (!L_124)
		{
			goto IL_02dd;
		}
	}
	{
		int32_t L_125 = V_6;
		QualitySettings_set_pixelLightCount_m3523654033(NULL /*static, unused*/, L_125, /*hidden argument*/NULL);
	}

IL_02dd:
	{
		int32_t L_126 = V_1;
		if (!L_126)
		{
			goto IL_02f6;
		}
	}
	{
		int32_t L_127 = V_1;
		if ((((int32_t)L_127) == ((int32_t)1)))
		{
			goto IL_0319;
		}
	}
	{
		int32_t L_128 = V_1;
		if ((((int32_t)L_128) == ((int32_t)2)))
		{
			goto IL_033c;
		}
	}
	{
		goto IL_035f;
	}

IL_02f6:
	{
		Shader_EnableKeyword_m3103559844(NULL /*static, unused*/, _stringLiteral1050277699, /*hidden argument*/NULL);
		Shader_DisableKeyword_m433641454(NULL /*static, unused*/, _stringLiteral2712144963, /*hidden argument*/NULL);
		Shader_DisableKeyword_m433641454(NULL /*static, unused*/, _stringLiteral1727694679, /*hidden argument*/NULL);
		goto IL_035f;
	}

IL_0319:
	{
		Shader_DisableKeyword_m433641454(NULL /*static, unused*/, _stringLiteral1050277699, /*hidden argument*/NULL);
		Shader_EnableKeyword_m3103559844(NULL /*static, unused*/, _stringLiteral2712144963, /*hidden argument*/NULL);
		Shader_DisableKeyword_m433641454(NULL /*static, unused*/, _stringLiteral1727694679, /*hidden argument*/NULL);
		goto IL_035f;
	}

IL_033c:
	{
		Shader_DisableKeyword_m433641454(NULL /*static, unused*/, _stringLiteral1050277699, /*hidden argument*/NULL);
		Shader_DisableKeyword_m433641454(NULL /*static, unused*/, _stringLiteral2712144963, /*hidden argument*/NULL);
		Shader_EnableKeyword_m3103559844(NULL /*static, unused*/, _stringLiteral1727694679, /*hidden argument*/NULL);
		goto IL_035f;
	}

IL_035f:
	{
		((Water_t936588004_StaticFields*)il2cpp_codegen_static_fields_for(Water_t936588004_il2cpp_TypeInfo_var))->set_s_InsideWater_15((bool)0);
		return;
	}
}
// System.Void UnityStandardAssets.Water.Water::OnDisable()
extern "C"  void Water_OnDisable_m66027400 (Water_t936588004 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water_OnDisable_m66027400_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t2473313435  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t2029824043  V_1;
	memset(&V_1, 0, sizeof(V_1));
	KeyValuePair_2_t2473313435  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Enumerator_t2029824043  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RenderTexture_t2108887433 * L_0 = __this->get_m_ReflectionTexture_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		RenderTexture_t2108887433 * L_2 = __this->get_m_ReflectionTexture_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m3193525861(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_m_ReflectionTexture_10((RenderTexture_t2108887433 *)NULL);
	}

IL_0022:
	{
		RenderTexture_t2108887433 * L_3 = __this->get_m_RefractionTexture_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0044;
		}
	}
	{
		RenderTexture_t2108887433 * L_5 = __this->get_m_RefractionTexture_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m3193525861(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->set_m_RefractionTexture_11((RenderTexture_t2108887433 *)NULL);
	}

IL_0044:
	{
		Dictionary_2_t75641268 * L_6 = __this->get_m_ReflectionCameras_8();
		NullCheck(L_6);
		Enumerator_t2029824043  L_7 = Dictionary_2_GetEnumerator_m484651291(L_6, /*hidden argument*/Dictionary_2_GetEnumerator_m484651291_RuntimeMethod_var);
		V_1 = L_7;
	}

IL_0050:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006e;
		}

IL_0055:
		{
			KeyValuePair_2_t2473313435  L_8 = Enumerator_get_Current_m2791492809((Enumerator_t2029824043 *)(&V_1), /*hidden argument*/Enumerator_get_Current_m2791492809_RuntimeMethod_var);
			V_0 = L_8;
			Camera_t4157153871 * L_9 = KeyValuePair_2_get_Value_m1196865026((KeyValuePair_2_t2473313435 *)(&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m1196865026_RuntimeMethod_var);
			NullCheck(L_9);
			GameObject_t1113636619 * L_10 = Component_get_gameObject_m442555142(L_9, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
			Object_DestroyImmediate_m3193525861(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		}

IL_006e:
		{
			bool L_11 = Enumerator_MoveNext_m2261968848((Enumerator_t2029824043 *)(&V_1), /*hidden argument*/Enumerator_MoveNext_m2261968848_RuntimeMethod_var);
			if (L_11)
			{
				goto IL_0055;
			}
		}

IL_007a:
		{
			IL2CPP_LEAVE(0x8D, FINALLY_007f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_007f;
	}

FINALLY_007f:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2390683763((Enumerator_t2029824043 *)(&V_1), /*hidden argument*/Enumerator_Dispose_m2390683763_RuntimeMethod_var);
		IL2CPP_END_FINALLY(127)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(127)
	{
		IL2CPP_JUMP_TBL(0x8D, IL_008d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_008d:
	{
		Dictionary_2_t75641268 * L_12 = __this->get_m_ReflectionCameras_8();
		NullCheck(L_12);
		Dictionary_2_Clear_m1016216395(L_12, /*hidden argument*/Dictionary_2_Clear_m1016216395_RuntimeMethod_var);
		Dictionary_2_t75641268 * L_13 = __this->get_m_RefractionCameras_9();
		NullCheck(L_13);
		Enumerator_t2029824043  L_14 = Dictionary_2_GetEnumerator_m484651291(L_13, /*hidden argument*/Dictionary_2_GetEnumerator_m484651291_RuntimeMethod_var);
		V_3 = L_14;
	}

IL_00a4:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00c2;
		}

IL_00a9:
		{
			KeyValuePair_2_t2473313435  L_15 = Enumerator_get_Current_m2791492809((Enumerator_t2029824043 *)(&V_3), /*hidden argument*/Enumerator_get_Current_m2791492809_RuntimeMethod_var);
			V_2 = L_15;
			Camera_t4157153871 * L_16 = KeyValuePair_2_get_Value_m1196865026((KeyValuePair_2_t2473313435 *)(&V_2), /*hidden argument*/KeyValuePair_2_get_Value_m1196865026_RuntimeMethod_var);
			NullCheck(L_16);
			GameObject_t1113636619 * L_17 = Component_get_gameObject_m442555142(L_16, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
			Object_DestroyImmediate_m3193525861(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		}

IL_00c2:
		{
			bool L_18 = Enumerator_MoveNext_m2261968848((Enumerator_t2029824043 *)(&V_3), /*hidden argument*/Enumerator_MoveNext_m2261968848_RuntimeMethod_var);
			if (L_18)
			{
				goto IL_00a9;
			}
		}

IL_00ce:
		{
			IL2CPP_LEAVE(0xE1, FINALLY_00d3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00d3;
	}

FINALLY_00d3:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2390683763((Enumerator_t2029824043 *)(&V_3), /*hidden argument*/Enumerator_Dispose_m2390683763_RuntimeMethod_var);
		IL2CPP_END_FINALLY(211)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(211)
	{
		IL2CPP_JUMP_TBL(0xE1, IL_00e1)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00e1:
	{
		Dictionary_2_t75641268 * L_19 = __this->get_m_RefractionCameras_9();
		NullCheck(L_19);
		Dictionary_2_Clear_m1016216395(L_19, /*hidden argument*/Dictionary_2_Clear_m1016216395_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityStandardAssets.Water.Water::Update()
extern "C"  void Water_Update_m2292850110 (Water_t936588004 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water_Update_m2292850110_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Material_t340375123 * V_0 = NULL;
	Vector4_t3319028937  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector4_t3319028937  V_3;
	memset(&V_3, 0, sizeof(V_3));
	double V_4 = 0.0;
	Vector4_t3319028937  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		Renderer_t2627027031 * L_0 = Component_GetComponent_TisRenderer_t2627027031_m1858649706(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m1858649706_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Renderer_t2627027031 * L_2 = Component_GetComponent_TisRenderer_t2627027031_m1858649706(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m1858649706_RuntimeMethod_var);
		NullCheck(L_2);
		Material_t340375123 * L_3 = Renderer_get_sharedMaterial_m1936632411(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Material_t340375123 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		Material_t340375123 * L_6 = V_0;
		NullCheck(L_6);
		Vector4_t3319028937  L_7 = Material_GetVector_m806950826(L_6, _stringLiteral49249872, /*hidden argument*/NULL);
		V_1 = L_7;
		Material_t340375123 * L_8 = V_0;
		NullCheck(L_8);
		float L_9 = Material_GetFloat_m2210875428(L_8, _stringLiteral3029421264, /*hidden argument*/NULL);
		V_2 = L_9;
		float L_10 = V_2;
		float L_11 = V_2;
		float L_12 = V_2;
		float L_13 = V_2;
		Vector4__ctor_m2498754347((Vector4_t3319028937 *)(&V_3), L_10, L_11, ((float)il2cpp_codegen_multiply((float)L_12, (float)(0.4f))), ((float)il2cpp_codegen_multiply((float)L_13, (float)(0.45f))), /*hidden argument*/NULL);
		float L_14 = Time_get_timeSinceLevelLoad_m2224611026(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = ((double)((double)(((double)((double)L_14)))/(double)(20.0)));
		float L_15 = (&V_1)->get_x_1();
		float L_16 = (&V_3)->get_x_1();
		double L_17 = V_4;
		double L_18 = Math_IEEERemainder_m1747102664(NULL /*static, unused*/, ((double)il2cpp_codegen_multiply((double)(((double)((double)((float)il2cpp_codegen_multiply((float)L_15, (float)L_16))))), (double)L_17)), (1.0), /*hidden argument*/NULL);
		float L_19 = (&V_1)->get_y_2();
		float L_20 = (&V_3)->get_y_2();
		double L_21 = V_4;
		double L_22 = Math_IEEERemainder_m1747102664(NULL /*static, unused*/, ((double)il2cpp_codegen_multiply((double)(((double)((double)((float)il2cpp_codegen_multiply((float)L_19, (float)L_20))))), (double)L_21)), (1.0), /*hidden argument*/NULL);
		float L_23 = (&V_1)->get_z_3();
		float L_24 = (&V_3)->get_z_3();
		double L_25 = V_4;
		double L_26 = Math_IEEERemainder_m1747102664(NULL /*static, unused*/, ((double)il2cpp_codegen_multiply((double)(((double)((double)((float)il2cpp_codegen_multiply((float)L_23, (float)L_24))))), (double)L_25)), (1.0), /*hidden argument*/NULL);
		float L_27 = (&V_1)->get_w_4();
		float L_28 = (&V_3)->get_w_4();
		double L_29 = V_4;
		double L_30 = Math_IEEERemainder_m1747102664(NULL /*static, unused*/, ((double)il2cpp_codegen_multiply((double)(((double)((double)((float)il2cpp_codegen_multiply((float)L_27, (float)L_28))))), (double)L_29)), (1.0), /*hidden argument*/NULL);
		Vector4__ctor_m2498754347((Vector4_t3319028937 *)(&V_5), (((float)((float)L_18))), (((float)((float)L_22))), (((float)((float)L_26))), (((float)((float)L_30))), /*hidden argument*/NULL);
		Material_t340375123 * L_31 = V_0;
		Vector4_t3319028937  L_32 = V_5;
		NullCheck(L_31);
		Material_SetVector_m4214217286(L_31, _stringLiteral2268830102, L_32, /*hidden argument*/NULL);
		Material_t340375123 * L_33 = V_0;
		Vector4_t3319028937  L_34 = V_3;
		NullCheck(L_33);
		Material_SetVector_m4214217286(L_33, _stringLiteral2617886994, L_34, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.Water::UpdateCameraModes(UnityEngine.Camera,UnityEngine.Camera)
extern "C"  void Water_UpdateCameraModes_m3658611570 (Water_t936588004 * __this, Camera_t4157153871 * ___src0, Camera_t4157153871 * ___dest1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water_UpdateCameraModes_m3658611570_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Skybox_t2662837510 * V_0 = NULL;
	Skybox_t2662837510 * V_1 = NULL;
	{
		Camera_t4157153871 * L_0 = ___dest1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		Camera_t4157153871 * L_2 = ___dest1;
		Camera_t4157153871 * L_3 = ___src0;
		NullCheck(L_3);
		int32_t L_4 = Camera_get_clearFlags_m992534691(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Camera_set_clearFlags_m2207032996(L_2, L_4, /*hidden argument*/NULL);
		Camera_t4157153871 * L_5 = ___dest1;
		Camera_t4157153871 * L_6 = ___src0;
		NullCheck(L_6);
		Color_t2555686324  L_7 = Camera_get_backgroundColor_m3310993309(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Camera_set_backgroundColor_m1332346802(L_5, L_7, /*hidden argument*/NULL);
		Camera_t4157153871 * L_8 = ___src0;
		NullCheck(L_8);
		int32_t L_9 = Camera_get_clearFlags_m992534691(L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)1))))
		{
			goto IL_0079;
		}
	}
	{
		Camera_t4157153871 * L_10 = ___src0;
		NullCheck(L_10);
		Skybox_t2662837510 * L_11 = Component_GetComponent_TisSkybox_t2662837510_m2500560783(L_10, /*hidden argument*/Component_GetComponent_TisSkybox_t2662837510_m2500560783_RuntimeMethod_var);
		V_0 = L_11;
		Camera_t4157153871 * L_12 = ___dest1;
		NullCheck(L_12);
		Skybox_t2662837510 * L_13 = Component_GetComponent_TisSkybox_t2662837510_m2500560783(L_12, /*hidden argument*/Component_GetComponent_TisSkybox_t2662837510_m2500560783_RuntimeMethod_var);
		V_1 = L_13;
		Skybox_t2662837510 * L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_005a;
		}
	}
	{
		Skybox_t2662837510 * L_16 = V_0;
		NullCheck(L_16);
		Material_t340375123 * L_17 = Skybox_get_material_m3789022787(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_18 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_0066;
		}
	}

IL_005a:
	{
		Skybox_t2662837510 * L_19 = V_1;
		NullCheck(L_19);
		Behaviour_set_enabled_m20417929(L_19, (bool)0, /*hidden argument*/NULL);
		goto IL_0079;
	}

IL_0066:
	{
		Skybox_t2662837510 * L_20 = V_1;
		NullCheck(L_20);
		Behaviour_set_enabled_m20417929(L_20, (bool)1, /*hidden argument*/NULL);
		Skybox_t2662837510 * L_21 = V_1;
		Skybox_t2662837510 * L_22 = V_0;
		NullCheck(L_22);
		Material_t340375123 * L_23 = Skybox_get_material_m3789022787(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		Skybox_set_material_m3176166872(L_21, L_23, /*hidden argument*/NULL);
	}

IL_0079:
	{
		Camera_t4157153871 * L_24 = ___dest1;
		Camera_t4157153871 * L_25 = ___src0;
		NullCheck(L_25);
		float L_26 = Camera_get_farClipPlane_m538536689(L_25, /*hidden argument*/NULL);
		NullCheck(L_24);
		Camera_set_farClipPlane_m3828313665(L_24, L_26, /*hidden argument*/NULL);
		Camera_t4157153871 * L_27 = ___dest1;
		Camera_t4157153871 * L_28 = ___src0;
		NullCheck(L_28);
		float L_29 = Camera_get_nearClipPlane_m837839537(L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		Camera_set_nearClipPlane_m3667419702(L_27, L_29, /*hidden argument*/NULL);
		Camera_t4157153871 * L_30 = ___dest1;
		Camera_t4157153871 * L_31 = ___src0;
		NullCheck(L_31);
		bool L_32 = Camera_get_orthographic_m2831464531(L_31, /*hidden argument*/NULL);
		NullCheck(L_30);
		Camera_set_orthographic_m2855749523(L_30, L_32, /*hidden argument*/NULL);
		Camera_t4157153871 * L_33 = ___dest1;
		Camera_t4157153871 * L_34 = ___src0;
		NullCheck(L_34);
		float L_35 = Camera_get_fieldOfView_m1018585504(L_34, /*hidden argument*/NULL);
		NullCheck(L_33);
		Camera_set_fieldOfView_m1438246590(L_33, L_35, /*hidden argument*/NULL);
		Camera_t4157153871 * L_36 = ___dest1;
		Camera_t4157153871 * L_37 = ___src0;
		NullCheck(L_37);
		float L_38 = Camera_get_aspect_m862507514(L_37, /*hidden argument*/NULL);
		NullCheck(L_36);
		Camera_set_aspect_m2625464181(L_36, L_38, /*hidden argument*/NULL);
		Camera_t4157153871 * L_39 = ___dest1;
		Camera_t4157153871 * L_40 = ___src0;
		NullCheck(L_40);
		float L_41 = Camera_get_orthographicSize_m3903216845(L_40, /*hidden argument*/NULL);
		NullCheck(L_39);
		Camera_set_orthographicSize_m76971700(L_39, L_41, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.Water::CreateWaterObjects(UnityEngine.Camera,UnityEngine.Camera&,UnityEngine.Camera&)
extern "C"  void Water_CreateWaterObjects_m2240077532 (Water_t936588004 * __this, Camera_t4157153871 * ___currentCamera0, Camera_t4157153871 ** ___reflectionCamera1, Camera_t4157153871 ** ___refractionCamera2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water_CreateWaterObjects_m2240077532_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	GameObject_t1113636619 * V_1 = NULL;
	GameObject_t1113636619 * V_2 = NULL;
	{
		int32_t L_0 = Water_GetWaterMode_m1759043735(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Camera_t4157153871 ** L_1 = ___reflectionCamera1;
		*((RuntimeObject **)(L_1)) = (RuntimeObject *)NULL;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)(L_1), (RuntimeObject *)NULL);
		Camera_t4157153871 ** L_2 = ___refractionCamera2;
		*((RuntimeObject **)(L_2)) = (RuntimeObject *)NULL;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)(L_2), (RuntimeObject *)NULL);
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) < ((int32_t)1)))
		{
			goto IL_0186;
		}
	}
	{
		RenderTexture_t2108887433 * L_4 = __this->get_m_ReflectionTexture_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		int32_t L_6 = __this->get_m_OldReflectionTextureSize_13();
		int32_t L_7 = __this->get_textureSize_4();
		if ((((int32_t)L_6) == ((int32_t)L_7)))
		{
			goto IL_00ae;
		}
	}

IL_0035:
	{
		RenderTexture_t2108887433 * L_8 = __this->get_m_ReflectionTexture_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		RenderTexture_t2108887433 * L_10 = __this->get_m_ReflectionTexture_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m3193525861(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
	}

IL_0050:
	{
		int32_t L_11 = __this->get_textureSize_4();
		int32_t L_12 = __this->get_textureSize_4();
		RenderTexture_t2108887433 * L_13 = (RenderTexture_t2108887433 *)il2cpp_codegen_object_new(RenderTexture_t2108887433_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m769234016(L_13, L_11, L_12, ((int32_t)16), /*hidden argument*/NULL);
		__this->set_m_ReflectionTexture_10(L_13);
		RenderTexture_t2108887433 * L_14 = __this->get_m_ReflectionTexture_10();
		int32_t L_15 = Object_GetInstanceID_m1255174761(__this, /*hidden argument*/NULL);
		int32_t L_16 = L_15;
		RuntimeObject * L_17 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_16);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral3277177482, L_17, /*hidden argument*/NULL);
		NullCheck(L_14);
		Object_set_name_m291480324(L_14, L_18, /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_19 = __this->get_m_ReflectionTexture_10();
		NullCheck(L_19);
		RenderTexture_set_isPowerOfTwo_m3873419893(L_19, (bool)1, /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_20 = __this->get_m_ReflectionTexture_10();
		NullCheck(L_20);
		Object_set_hideFlags_m1648752846(L_20, ((int32_t)52), /*hidden argument*/NULL);
		int32_t L_21 = __this->get_textureSize_4();
		__this->set_m_OldReflectionTextureSize_13(L_21);
	}

IL_00ae:
	{
		Dictionary_2_t75641268 * L_22 = __this->get_m_ReflectionCameras_8();
		Camera_t4157153871 * L_23 = ___currentCamera0;
		Camera_t4157153871 ** L_24 = ___reflectionCamera1;
		NullCheck(L_22);
		Dictionary_2_TryGetValue_m3746080878(L_22, L_23, (Camera_t4157153871 **)L_24, /*hidden argument*/Dictionary_2_TryGetValue_m3746080878_RuntimeMethod_var);
		Camera_t4157153871 ** L_25 = ___reflectionCamera1;
		Camera_t4157153871 * L_26 = *((Camera_t4157153871 **)L_25);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_27 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_0186;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_28 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t2843939325* L_29 = L_28;
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, _stringLiteral1537969950);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral1537969950);
		ObjectU5BU5D_t2843939325* L_30 = L_29;
		int32_t L_31 = Object_GetInstanceID_m1255174761(__this, /*hidden argument*/NULL);
		int32_t L_32 = L_31;
		RuntimeObject * L_33 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_32);
		NullCheck(L_30);
		ArrayElementTypeCheck (L_30, L_33);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_33);
		ObjectU5BU5D_t2843939325* L_34 = L_30;
		NullCheck(L_34);
		ArrayElementTypeCheck (L_34, _stringLiteral1505539685);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral1505539685);
		ObjectU5BU5D_t2843939325* L_35 = L_34;
		Camera_t4157153871 * L_36 = ___currentCamera0;
		NullCheck(L_36);
		int32_t L_37 = Object_GetInstanceID_m1255174761(L_36, /*hidden argument*/NULL);
		int32_t L_38 = L_37;
		RuntimeObject * L_39 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_38);
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, L_39);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_39);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_40 = String_Concat_m2971454694(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		TypeU5BU5D_t3940880105* L_41 = (TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)2);
		TypeU5BU5D_t3940880105* L_42 = L_41;
		RuntimeTypeHandle_t3027515415  L_43 = { reinterpret_cast<intptr_t> (Camera_t4157153871_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		NullCheck(L_42);
		ArrayElementTypeCheck (L_42, L_44);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_44);
		TypeU5BU5D_t3940880105* L_45 = L_42;
		RuntimeTypeHandle_t3027515415  L_46 = { reinterpret_cast<intptr_t> (Skybox_t2662837510_0_0_0_var) };
		Type_t * L_47 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		NullCheck(L_45);
		ArrayElementTypeCheck (L_45, L_47);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_47);
		GameObject_t1113636619 * L_48 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m1350607670(L_48, L_40, L_45, /*hidden argument*/NULL);
		V_1 = L_48;
		Camera_t4157153871 ** L_49 = ___reflectionCamera1;
		GameObject_t1113636619 * L_50 = V_1;
		NullCheck(L_50);
		Camera_t4157153871 * L_51 = GameObject_GetComponent_TisCamera_t4157153871_m1507399110(L_50, /*hidden argument*/GameObject_GetComponent_TisCamera_t4157153871_m1507399110_RuntimeMethod_var);
		*((RuntimeObject **)(L_49)) = (RuntimeObject *)L_51;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)(L_49), (RuntimeObject *)L_51);
		Camera_t4157153871 ** L_52 = ___reflectionCamera1;
		Camera_t4157153871 * L_53 = *((Camera_t4157153871 **)L_52);
		NullCheck(L_53);
		Behaviour_set_enabled_m20417929(L_53, (bool)0, /*hidden argument*/NULL);
		Camera_t4157153871 ** L_54 = ___reflectionCamera1;
		Camera_t4157153871 * L_55 = *((Camera_t4157153871 **)L_54);
		NullCheck(L_55);
		Transform_t3600365921 * L_56 = Component_get_transform_m3162698980(L_55, /*hidden argument*/NULL);
		Transform_t3600365921 * L_57 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_57);
		Vector3_t3722313464  L_58 = Transform_get_position_m36019626(L_57, /*hidden argument*/NULL);
		NullCheck(L_56);
		Transform_set_position_m3387557959(L_56, L_58, /*hidden argument*/NULL);
		Camera_t4157153871 ** L_59 = ___reflectionCamera1;
		Camera_t4157153871 * L_60 = *((Camera_t4157153871 **)L_59);
		NullCheck(L_60);
		Transform_t3600365921 * L_61 = Component_get_transform_m3162698980(L_60, /*hidden argument*/NULL);
		Transform_t3600365921 * L_62 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_62);
		Quaternion_t2301928331  L_63 = Transform_get_rotation_m3502953881(L_62, /*hidden argument*/NULL);
		NullCheck(L_61);
		Transform_set_rotation_m3524318132(L_61, L_63, /*hidden argument*/NULL);
		Camera_t4157153871 ** L_64 = ___reflectionCamera1;
		Camera_t4157153871 * L_65 = *((Camera_t4157153871 **)L_64);
		NullCheck(L_65);
		GameObject_t1113636619 * L_66 = Component_get_gameObject_m442555142(L_65, /*hidden argument*/NULL);
		NullCheck(L_66);
		GameObject_AddComponent_TisFlareLayer_t1739223323_m722533630(L_66, /*hidden argument*/GameObject_AddComponent_TisFlareLayer_t1739223323_m722533630_RuntimeMethod_var);
		GameObject_t1113636619 * L_67 = V_1;
		NullCheck(L_67);
		Object_set_hideFlags_m1648752846(L_67, ((int32_t)61), /*hidden argument*/NULL);
		Dictionary_2_t75641268 * L_68 = __this->get_m_ReflectionCameras_8();
		Camera_t4157153871 * L_69 = ___currentCamera0;
		Camera_t4157153871 ** L_70 = ___reflectionCamera1;
		Camera_t4157153871 * L_71 = *((Camera_t4157153871 **)L_70);
		NullCheck(L_68);
		Dictionary_2_set_Item_m1091284778(L_68, L_69, L_71, /*hidden argument*/Dictionary_2_set_Item_m1091284778_RuntimeMethod_var);
	}

IL_0186:
	{
		int32_t L_72 = V_0;
		if ((((int32_t)L_72) < ((int32_t)2)))
		{
			goto IL_02ff;
		}
	}
	{
		RenderTexture_t2108887433 * L_73 = __this->get_m_RefractionTexture_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_74 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_73, /*hidden argument*/NULL);
		if (!L_74)
		{
			goto IL_01ae;
		}
	}
	{
		int32_t L_75 = __this->get_m_OldRefractionTextureSize_14();
		int32_t L_76 = __this->get_textureSize_4();
		if ((((int32_t)L_75) == ((int32_t)L_76)))
		{
			goto IL_0227;
		}
	}

IL_01ae:
	{
		RenderTexture_t2108887433 * L_77 = __this->get_m_RefractionTexture_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_78 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_77, /*hidden argument*/NULL);
		if (!L_78)
		{
			goto IL_01c9;
		}
	}
	{
		RenderTexture_t2108887433 * L_79 = __this->get_m_RefractionTexture_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m3193525861(NULL /*static, unused*/, L_79, /*hidden argument*/NULL);
	}

IL_01c9:
	{
		int32_t L_80 = __this->get_textureSize_4();
		int32_t L_81 = __this->get_textureSize_4();
		RenderTexture_t2108887433 * L_82 = (RenderTexture_t2108887433 *)il2cpp_codegen_object_new(RenderTexture_t2108887433_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m769234016(L_82, L_80, L_81, ((int32_t)16), /*hidden argument*/NULL);
		__this->set_m_RefractionTexture_11(L_82);
		RenderTexture_t2108887433 * L_83 = __this->get_m_RefractionTexture_11();
		int32_t L_84 = Object_GetInstanceID_m1255174761(__this, /*hidden argument*/NULL);
		int32_t L_85 = L_84;
		RuntimeObject * L_86 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_85);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_87 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral1366986456, L_86, /*hidden argument*/NULL);
		NullCheck(L_83);
		Object_set_name_m291480324(L_83, L_87, /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_88 = __this->get_m_RefractionTexture_11();
		NullCheck(L_88);
		RenderTexture_set_isPowerOfTwo_m3873419893(L_88, (bool)1, /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_89 = __this->get_m_RefractionTexture_11();
		NullCheck(L_89);
		Object_set_hideFlags_m1648752846(L_89, ((int32_t)52), /*hidden argument*/NULL);
		int32_t L_90 = __this->get_textureSize_4();
		__this->set_m_OldRefractionTextureSize_14(L_90);
	}

IL_0227:
	{
		Dictionary_2_t75641268 * L_91 = __this->get_m_RefractionCameras_9();
		Camera_t4157153871 * L_92 = ___currentCamera0;
		Camera_t4157153871 ** L_93 = ___refractionCamera2;
		NullCheck(L_91);
		Dictionary_2_TryGetValue_m3746080878(L_91, L_92, (Camera_t4157153871 **)L_93, /*hidden argument*/Dictionary_2_TryGetValue_m3746080878_RuntimeMethod_var);
		Camera_t4157153871 ** L_94 = ___refractionCamera2;
		Camera_t4157153871 * L_95 = *((Camera_t4157153871 **)L_94);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_96 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_95, /*hidden argument*/NULL);
		if (L_96)
		{
			goto IL_02ff;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_97 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t2843939325* L_98 = L_97;
		NullCheck(L_98);
		ArrayElementTypeCheck (L_98, _stringLiteral2541588253);
		(L_98)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral2541588253);
		ObjectU5BU5D_t2843939325* L_99 = L_98;
		int32_t L_100 = Object_GetInstanceID_m1255174761(__this, /*hidden argument*/NULL);
		int32_t L_101 = L_100;
		RuntimeObject * L_102 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_101);
		NullCheck(L_99);
		ArrayElementTypeCheck (L_99, L_102);
		(L_99)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_102);
		ObjectU5BU5D_t2843939325* L_103 = L_99;
		NullCheck(L_103);
		ArrayElementTypeCheck (L_103, _stringLiteral1505539685);
		(L_103)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral1505539685);
		ObjectU5BU5D_t2843939325* L_104 = L_103;
		Camera_t4157153871 * L_105 = ___currentCamera0;
		NullCheck(L_105);
		int32_t L_106 = Object_GetInstanceID_m1255174761(L_105, /*hidden argument*/NULL);
		int32_t L_107 = L_106;
		RuntimeObject * L_108 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_107);
		NullCheck(L_104);
		ArrayElementTypeCheck (L_104, L_108);
		(L_104)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_108);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_109 = String_Concat_m2971454694(NULL /*static, unused*/, L_104, /*hidden argument*/NULL);
		TypeU5BU5D_t3940880105* L_110 = (TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)2);
		TypeU5BU5D_t3940880105* L_111 = L_110;
		RuntimeTypeHandle_t3027515415  L_112 = { reinterpret_cast<intptr_t> (Camera_t4157153871_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_113 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_112, /*hidden argument*/NULL);
		NullCheck(L_111);
		ArrayElementTypeCheck (L_111, L_113);
		(L_111)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_113);
		TypeU5BU5D_t3940880105* L_114 = L_111;
		RuntimeTypeHandle_t3027515415  L_115 = { reinterpret_cast<intptr_t> (Skybox_t2662837510_0_0_0_var) };
		Type_t * L_116 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_115, /*hidden argument*/NULL);
		NullCheck(L_114);
		ArrayElementTypeCheck (L_114, L_116);
		(L_114)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_116);
		GameObject_t1113636619 * L_117 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m1350607670(L_117, L_109, L_114, /*hidden argument*/NULL);
		V_2 = L_117;
		Camera_t4157153871 ** L_118 = ___refractionCamera2;
		GameObject_t1113636619 * L_119 = V_2;
		NullCheck(L_119);
		Camera_t4157153871 * L_120 = GameObject_GetComponent_TisCamera_t4157153871_m1507399110(L_119, /*hidden argument*/GameObject_GetComponent_TisCamera_t4157153871_m1507399110_RuntimeMethod_var);
		*((RuntimeObject **)(L_118)) = (RuntimeObject *)L_120;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)(L_118), (RuntimeObject *)L_120);
		Camera_t4157153871 ** L_121 = ___refractionCamera2;
		Camera_t4157153871 * L_122 = *((Camera_t4157153871 **)L_121);
		NullCheck(L_122);
		Behaviour_set_enabled_m20417929(L_122, (bool)0, /*hidden argument*/NULL);
		Camera_t4157153871 ** L_123 = ___refractionCamera2;
		Camera_t4157153871 * L_124 = *((Camera_t4157153871 **)L_123);
		NullCheck(L_124);
		Transform_t3600365921 * L_125 = Component_get_transform_m3162698980(L_124, /*hidden argument*/NULL);
		Transform_t3600365921 * L_126 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_126);
		Vector3_t3722313464  L_127 = Transform_get_position_m36019626(L_126, /*hidden argument*/NULL);
		NullCheck(L_125);
		Transform_set_position_m3387557959(L_125, L_127, /*hidden argument*/NULL);
		Camera_t4157153871 ** L_128 = ___refractionCamera2;
		Camera_t4157153871 * L_129 = *((Camera_t4157153871 **)L_128);
		NullCheck(L_129);
		Transform_t3600365921 * L_130 = Component_get_transform_m3162698980(L_129, /*hidden argument*/NULL);
		Transform_t3600365921 * L_131 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_131);
		Quaternion_t2301928331  L_132 = Transform_get_rotation_m3502953881(L_131, /*hidden argument*/NULL);
		NullCheck(L_130);
		Transform_set_rotation_m3524318132(L_130, L_132, /*hidden argument*/NULL);
		Camera_t4157153871 ** L_133 = ___refractionCamera2;
		Camera_t4157153871 * L_134 = *((Camera_t4157153871 **)L_133);
		NullCheck(L_134);
		GameObject_t1113636619 * L_135 = Component_get_gameObject_m442555142(L_134, /*hidden argument*/NULL);
		NullCheck(L_135);
		GameObject_AddComponent_TisFlareLayer_t1739223323_m722533630(L_135, /*hidden argument*/GameObject_AddComponent_TisFlareLayer_t1739223323_m722533630_RuntimeMethod_var);
		GameObject_t1113636619 * L_136 = V_2;
		NullCheck(L_136);
		Object_set_hideFlags_m1648752846(L_136, ((int32_t)61), /*hidden argument*/NULL);
		Dictionary_2_t75641268 * L_137 = __this->get_m_RefractionCameras_9();
		Camera_t4157153871 * L_138 = ___currentCamera0;
		Camera_t4157153871 ** L_139 = ___refractionCamera2;
		Camera_t4157153871 * L_140 = *((Camera_t4157153871 **)L_139);
		NullCheck(L_137);
		Dictionary_2_set_Item_m1091284778(L_137, L_138, L_140, /*hidden argument*/Dictionary_2_set_Item_m1091284778_RuntimeMethod_var);
	}

IL_02ff:
	{
		return;
	}
}
// UnityStandardAssets.Water.Water/WaterMode UnityStandardAssets.Water.Water::GetWaterMode()
extern "C"  int32_t Water_GetWaterMode_m1759043735 (Water_t936588004 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_HardwareWaterSupport_12();
		int32_t L_1 = __this->get_waterMode_2();
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_2 = __this->get_m_HardwareWaterSupport_12();
		return L_2;
	}

IL_0018:
	{
		int32_t L_3 = __this->get_waterMode_2();
		return L_3;
	}
}
// UnityStandardAssets.Water.Water/WaterMode UnityStandardAssets.Water.Water::FindHardwareWaterSupport()
extern "C"  int32_t Water_FindHardwareWaterSupport_m3785980915 (Water_t936588004 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water_FindHardwareWaterSupport_m3785980915_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Material_t340375123 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		Renderer_t2627027031 * L_0 = Component_GetComponent_TisRenderer_t2627027031_m1858649706(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m1858649706_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		return (int32_t)(0);
	}

IL_0012:
	{
		Renderer_t2627027031 * L_2 = Component_GetComponent_TisRenderer_t2627027031_m1858649706(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m1858649706_RuntimeMethod_var);
		NullCheck(L_2);
		Material_t340375123 * L_3 = Renderer_get_sharedMaterial_m1936632411(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Material_t340375123 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_002b;
		}
	}
	{
		return (int32_t)(0);
	}

IL_002b:
	{
		Material_t340375123 * L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7 = Material_GetTag_m2091721776(L_6, _stringLiteral713880165, (bool)0, /*hidden argument*/NULL);
		V_1 = L_7;
		String_t* L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m920492651(NULL /*static, unused*/, L_8, _stringLiteral1103090832, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_004a;
		}
	}
	{
		return (int32_t)(2);
	}

IL_004a:
	{
		String_t* L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m920492651(NULL /*static, unused*/, L_10, _stringLiteral2505954588, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_005c;
		}
	}
	{
		return (int32_t)(1);
	}

IL_005c:
	{
		return (int32_t)(0);
	}
}
// UnityEngine.Vector4 UnityStandardAssets.Water.Water::CameraSpacePlane(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector4_t3319028937  Water_CameraSpacePlane_m2332942409 (Water_t936588004 * __this, Camera_t4157153871 * ___cam0, Vector3_t3722313464  ___pos1, Vector3_t3722313464  ___normal2, float ___sideSign3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water_CameraSpacePlane_m2332942409_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t1817901843  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Vector3_t3722313464  L_0 = ___pos1;
		Vector3_t3722313464  L_1 = ___normal2;
		float L_2 = __this->get_clipPlaneOffset_5();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t3722313464  L_4 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Camera_t4157153871 * L_5 = ___cam0;
		NullCheck(L_5);
		Matrix4x4_t1817901843  L_6 = Camera_get_worldToCameraMatrix_m22661425(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		Vector3_t3722313464  L_7 = V_0;
		Vector3_t3722313464  L_8 = Matrix4x4_MultiplyPoint_m1575665487((Matrix4x4_t1817901843 *)(&V_1), L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		Vector3_t3722313464  L_9 = ___normal2;
		Vector3_t3722313464  L_10 = Matrix4x4_MultiplyVector_m3808798942((Matrix4x4_t1817901843 *)(&V_1), L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		Vector3_t3722313464  L_11 = Vector3_get_normalized_m2454957984((Vector3_t3722313464 *)(&V_4), /*hidden argument*/NULL);
		float L_12 = ___sideSign3;
		Vector3_t3722313464  L_13 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		float L_14 = (&V_3)->get_x_1();
		float L_15 = (&V_3)->get_y_2();
		float L_16 = (&V_3)->get_z_3();
		Vector3_t3722313464  L_17 = V_2;
		Vector3_t3722313464  L_18 = V_3;
		float L_19 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		Vector4_t3319028937  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector4__ctor_m2498754347((&L_20), L_14, L_15, L_16, ((-L_19)), /*hidden argument*/NULL);
		return L_20;
	}
}
// System.Void UnityStandardAssets.Water.Water::CalculateReflectionMatrix(UnityEngine.Matrix4x4&,UnityEngine.Vector4)
extern "C"  void Water_CalculateReflectionMatrix_m175561076 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843 * ___reflectionMat0, Vector4_t3319028937  ___plane1, const RuntimeMethod* method)
{
	{
		Matrix4x4_t1817901843 * L_0 = ___reflectionMat0;
		float L_1 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 0, /*hidden argument*/NULL);
		float L_2 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 0, /*hidden argument*/NULL);
		L_0->set_m00_0(((float)il2cpp_codegen_subtract((float)(1.0f), (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(2.0f), (float)L_1)), (float)L_2)))));
		Matrix4x4_t1817901843 * L_3 = ___reflectionMat0;
		float L_4 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 0, /*hidden argument*/NULL);
		float L_5 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 1, /*hidden argument*/NULL);
		L_3->set_m01_4(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_4)), (float)L_5)));
		Matrix4x4_t1817901843 * L_6 = ___reflectionMat0;
		float L_7 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 0, /*hidden argument*/NULL);
		float L_8 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 2, /*hidden argument*/NULL);
		L_6->set_m02_8(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_7)), (float)L_8)));
		Matrix4x4_t1817901843 * L_9 = ___reflectionMat0;
		float L_10 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 3, /*hidden argument*/NULL);
		float L_11 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 0, /*hidden argument*/NULL);
		L_9->set_m03_12(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_10)), (float)L_11)));
		Matrix4x4_t1817901843 * L_12 = ___reflectionMat0;
		float L_13 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 1, /*hidden argument*/NULL);
		float L_14 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 0, /*hidden argument*/NULL);
		L_12->set_m10_1(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_13)), (float)L_14)));
		Matrix4x4_t1817901843 * L_15 = ___reflectionMat0;
		float L_16 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 1, /*hidden argument*/NULL);
		float L_17 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 1, /*hidden argument*/NULL);
		L_15->set_m11_5(((float)il2cpp_codegen_subtract((float)(1.0f), (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(2.0f), (float)L_16)), (float)L_17)))));
		Matrix4x4_t1817901843 * L_18 = ___reflectionMat0;
		float L_19 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 1, /*hidden argument*/NULL);
		float L_20 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 2, /*hidden argument*/NULL);
		L_18->set_m12_9(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_19)), (float)L_20)));
		Matrix4x4_t1817901843 * L_21 = ___reflectionMat0;
		float L_22 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 3, /*hidden argument*/NULL);
		float L_23 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 1, /*hidden argument*/NULL);
		L_21->set_m13_13(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_22)), (float)L_23)));
		Matrix4x4_t1817901843 * L_24 = ___reflectionMat0;
		float L_25 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 2, /*hidden argument*/NULL);
		float L_26 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 0, /*hidden argument*/NULL);
		L_24->set_m20_2(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_25)), (float)L_26)));
		Matrix4x4_t1817901843 * L_27 = ___reflectionMat0;
		float L_28 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 2, /*hidden argument*/NULL);
		float L_29 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 1, /*hidden argument*/NULL);
		L_27->set_m21_6(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_28)), (float)L_29)));
		Matrix4x4_t1817901843 * L_30 = ___reflectionMat0;
		float L_31 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 2, /*hidden argument*/NULL);
		float L_32 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 2, /*hidden argument*/NULL);
		L_30->set_m22_10(((float)il2cpp_codegen_subtract((float)(1.0f), (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(2.0f), (float)L_31)), (float)L_32)))));
		Matrix4x4_t1817901843 * L_33 = ___reflectionMat0;
		float L_34 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 3, /*hidden argument*/NULL);
		float L_35 = Vector4_get_Item_m2380866393((Vector4_t3319028937 *)(&___plane1), 2, /*hidden argument*/NULL);
		L_33->set_m23_14(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_34)), (float)L_35)));
		Matrix4x4_t1817901843 * L_36 = ___reflectionMat0;
		L_36->set_m30_3((0.0f));
		Matrix4x4_t1817901843 * L_37 = ___reflectionMat0;
		L_37->set_m31_7((0.0f));
		Matrix4x4_t1817901843 * L_38 = ___reflectionMat0;
		L_38->set_m32_11((0.0f));
		Matrix4x4_t1817901843 * L_39 = ___reflectionMat0;
		L_39->set_m33_15((1.0f));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets.Water.WaterBase::.ctor()
extern "C"  void WaterBase__ctor_m2007567740 (WaterBase_t3883863317 * __this, const RuntimeMethod* method)
{
	{
		__this->set_waterQuality_3(2);
		__this->set_edgeBlend_4((bool)1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.WaterBase::UpdateShader()
extern "C"  void WaterBase_UpdateShader_m695718468 (WaterBase_t3883863317 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaterBase_UpdateShader_m695718468_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_waterQuality_3();
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0026;
		}
	}
	{
		Material_t340375123 * L_1 = __this->get_sharedMaterial_2();
		NullCheck(L_1);
		Shader_t4151988712 * L_2 = Material_get_shader_m1331119247(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Shader_set_maximumLOD_m3495846676(L_2, ((int32_t)501), /*hidden argument*/NULL);
		goto IL_0061;
	}

IL_0026:
	{
		int32_t L_3 = __this->get_waterQuality_3();
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_004c;
		}
	}
	{
		Material_t340375123 * L_4 = __this->get_sharedMaterial_2();
		NullCheck(L_4);
		Shader_t4151988712 * L_5 = Material_get_shader_m1331119247(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Shader_set_maximumLOD_m3495846676(L_5, ((int32_t)301), /*hidden argument*/NULL);
		goto IL_0061;
	}

IL_004c:
	{
		Material_t340375123 * L_6 = __this->get_sharedMaterial_2();
		NullCheck(L_6);
		Shader_t4151988712 * L_7 = Material_get_shader_m1331119247(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Shader_set_maximumLOD_m3495846676(L_7, ((int32_t)201), /*hidden argument*/NULL);
	}

IL_0061:
	{
		bool L_8 = SystemInfo_SupportsRenderTextureFormat_m1663449629(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0073;
		}
	}
	{
		__this->set_edgeBlend_4((bool)0);
	}

IL_0073:
	{
		bool L_9 = __this->get_edgeBlend_4();
		if (!L_9)
		{
			goto IL_00b8;
		}
	}
	{
		Shader_EnableKeyword_m3103559844(NULL /*static, unused*/, _stringLiteral1508871393, /*hidden argument*/NULL);
		Shader_DisableKeyword_m433641454(NULL /*static, unused*/, _stringLiteral3272779580, /*hidden argument*/NULL);
		Camera_t4157153871 * L_10 = Camera_get_main_m3643453163(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00b3;
		}
	}
	{
		Camera_t4157153871 * L_12 = Camera_get_main_m3643453163(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t4157153871 * L_13 = L_12;
		NullCheck(L_13);
		int32_t L_14 = Camera_get_depthTextureMode_m871144641(L_13, /*hidden argument*/NULL);
		NullCheck(L_13);
		Camera_set_depthTextureMode_m754977860(L_13, ((int32_t)((int32_t)L_14|(int32_t)1)), /*hidden argument*/NULL);
	}

IL_00b3:
	{
		goto IL_00cc;
	}

IL_00b8:
	{
		Shader_EnableKeyword_m3103559844(NULL /*static, unused*/, _stringLiteral3272779580, /*hidden argument*/NULL);
		Shader_DisableKeyword_m433641454(NULL /*static, unused*/, _stringLiteral1508871393, /*hidden argument*/NULL);
	}

IL_00cc:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Water.WaterBase::WaterTileBeingRendered(UnityEngine.Transform,UnityEngine.Camera)
extern "C"  void WaterBase_WaterTileBeingRendered_m3136753890 (WaterBase_t3883863317 * __this, Transform_t3600365921 * ___tr0, Camera_t4157153871 * ___currentCam1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaterBase_WaterTileBeingRendered_m3136753890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t4157153871 * L_0 = ___currentCam1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		bool L_2 = __this->get_edgeBlend_4();
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		Camera_t4157153871 * L_3 = ___currentCam1;
		Camera_t4157153871 * L_4 = L_3;
		NullCheck(L_4);
		int32_t L_5 = Camera_get_depthTextureMode_m871144641(L_4, /*hidden argument*/NULL);
		NullCheck(L_4);
		Camera_set_depthTextureMode_m754977860(L_4, ((int32_t)((int32_t)L_5|(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Water.WaterBase::Update()
extern "C"  void WaterBase_Update_m2764498022 (WaterBase_t3883863317 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaterBase_Update_m2764498022_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t340375123 * L_0 = __this->get_sharedMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		WaterBase_UpdateShader_m695718468(__this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets.Water.WaterTile::.ctor()
extern "C"  void WaterTile__ctor_m222892649 (WaterTile_t2536246541 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.WaterTile::Start()
extern "C"  void WaterTile_Start_m4023456665 (WaterTile_t2536246541 * __this, const RuntimeMethod* method)
{
	{
		WaterTile_AcquireComponents_m1557480152(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.WaterTile::AcquireComponents()
extern "C"  void WaterTile_AcquireComponents_m1557480152 (WaterTile_t2536246541 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaterTile_AcquireComponents_m1557480152_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlanarReflection_t439636033 * L_0 = __this->get_reflection_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0051;
		}
	}
	{
		Transform_t3600365921 * L_2 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = Transform_get_parent_m835071599(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0040;
		}
	}
	{
		Transform_t3600365921 * L_5 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3600365921 * L_6 = Transform_get_parent_m835071599(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		PlanarReflection_t439636033 * L_7 = Component_GetComponent_TisPlanarReflection_t439636033_m1616117542(L_6, /*hidden argument*/Component_GetComponent_TisPlanarReflection_t439636033_m1616117542_RuntimeMethod_var);
		__this->set_reflection_2(L_7);
		goto IL_0051;
	}

IL_0040:
	{
		Transform_t3600365921 * L_8 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		PlanarReflection_t439636033 * L_9 = Component_GetComponent_TisPlanarReflection_t439636033_m1616117542(L_8, /*hidden argument*/Component_GetComponent_TisPlanarReflection_t439636033_m1616117542_RuntimeMethod_var);
		__this->set_reflection_2(L_9);
	}

IL_0051:
	{
		WaterBase_t3883863317 * L_10 = __this->get_waterBase_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_00a2;
		}
	}
	{
		Transform_t3600365921 * L_12 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_t3600365921 * L_13 = Transform_get_parent_m835071599(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0091;
		}
	}
	{
		Transform_t3600365921 * L_15 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_t3600365921 * L_16 = Transform_get_parent_m835071599(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		WaterBase_t3883863317 * L_17 = Component_GetComponent_TisWaterBase_t3883863317_m2958773954(L_16, /*hidden argument*/Component_GetComponent_TisWaterBase_t3883863317_m2958773954_RuntimeMethod_var);
		__this->set_waterBase_3(L_17);
		goto IL_00a2;
	}

IL_0091:
	{
		Transform_t3600365921 * L_18 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		WaterBase_t3883863317 * L_19 = Component_GetComponent_TisWaterBase_t3883863317_m2958773954(L_18, /*hidden argument*/Component_GetComponent_TisWaterBase_t3883863317_m2958773954_RuntimeMethod_var);
		__this->set_waterBase_3(L_19);
	}

IL_00a2:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Water.WaterTile::OnWillRenderObject()
extern "C"  void WaterTile_OnWillRenderObject_m21485850 (WaterTile_t2536246541 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaterTile_OnWillRenderObject_m21485850_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlanarReflection_t439636033 * L_0 = __this->get_reflection_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		PlanarReflection_t439636033 * L_2 = __this->get_reflection_2();
		Transform_t3600365921 * L_3 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Camera_t4157153871 * L_4 = Camera_get_current_m929992396(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		PlanarReflection_WaterTileBeingRendered_m1672873199(L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0026:
	{
		WaterBase_t3883863317 * L_5 = __this->get_waterBase_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004c;
		}
	}
	{
		WaterBase_t3883863317 * L_7 = __this->get_waterBase_3();
		Transform_t3600365921 * L_8 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Camera_t4157153871 * L_9 = Camera_get_current_m929992396(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		WaterBase_WaterTileBeingRendered_m3136753890(L_7, L_8, L_9, /*hidden argument*/NULL);
	}

IL_004c:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
